package es.wingsmobile.guardian.ui.home;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.wingsmobile.guardian.db.DatabaseHelper;

public class HomeViewModelFactory implements ViewModelProvider.Factory {
    private DatabaseHelper db;

    public HomeViewModelFactory(Context context) {
        db = new DatabaseHelper(context);
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new HomeViewModel(db);
    }
}
