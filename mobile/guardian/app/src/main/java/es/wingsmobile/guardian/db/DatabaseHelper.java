package es.wingsmobile.guardian.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import es.wingsmobile.guardian.R;
import es.wingsmobile.guardian.domain.Config;
import es.wingsmobile.guardian.domain.Record;
import es.wingsmobile.guardian.util.FormatUtil;

/**
 * Created by seba on 19/06/17.
 */

public class DatabaseHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "guardian.db";
    private static final int DATABASE_VERSION = 5;
    private SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void insertEvents(Record r){
        try{
            insert(r);
        }
        catch (Exception ex){
            Log.d("ERROR DB insertEvents",ex.getMessage());
            try{
                createUpdate();
                insert(r);
            }catch(Exception ex2) {
                Log.d("ERROR DB insert",ex.getMessage());
            }
        }
    }

    private  void insert(Record r){
        setDataCleaning();
        db = getWritableDatabase();

        db.execSQL("DELETE FROM log where pid = '"+r.getPid()+"';");

        if (!r.isRecording()){
            db.execSQL("UPDATE log set recording = 0;");//sirve para marcar el freno de varias apps grabando al mismo tiempo, sino quedan colgadas
        }

        ContentValues c = new ContentValues();
        c.put("pid",r.getPid());
        c.put("start", FormatUtil.fromDateToString(r.getStart()));
        c.put("stop",FormatUtil.fromDateToString(r.getEnd()));
        c.put("source",r.getSource());
        c.put("pkg",r.getAppName());
        c.put("duration",r.getDuration());
        c.put("image","");
        c.put("show",r.isShow());
        c.put("type",r.getType().toUpperCase());
        c.put("section",r.getSection().toUpperCase());
        c.put("recording",r.isRecording());
        db.insertWithOnConflict("log",null, c,SQLiteDatabase.CONFLICT_IGNORE);
        db.close();
    }

    public List<Record> getEvents(){
        List<Record> ret = new LinkedList<>();
        try{
            db = getReadableDatabase();
            Cursor c = db.rawQuery("select * from log order by start desc, stop desc",null);
            if(c != null){
                c.moveToPosition(-1);
                while(c.moveToNext()){
                    Record r = new Record();
                    r.setPid(c.getString(0));
                    r.setStart(FormatUtil.fromStringToDate(c.getString(1)));
                    r.setEnd(FormatUtil.fromStringToDate(c.getString(2)));
                    r.setSource(c.getString(3));
                    r.setAppName(c.getString(4));
                    r.setShow(c.getInt(7) == 1);
                    r.setType(c.getString(8));
                    r.setSection(c.getString(9));
                    r.setRecording(c.getInt(10)== 1);
                    ret.add(r);
                }
            }
            c.close();
            db.close();
            return ret;
        }
        catch (Exception ex){
            Log.d("ERROR DB getEvents",ex.getMessage());
            createUpdate();
            return ret;
        }
    }

    public void clearLogs(){
        try{
            db = getWritableDatabase();
            db.execSQL("delete from log;");
            db.close();
        }
        catch (Exception ex){
            Log.d("ERROR DB clearLogs",ex.getMessage());
        }
    }

    private Config config = new Config();

    public Config getConfig(){
        try{
            db = getReadableDatabase();
            Cursor c = db.rawQuery("select * from config",null);
            c.moveToPosition(-1);
            while(c.moveToNext()){
                config.put(c.getString(0),c.getString(1));
            }
            c.close();
            db.close();
            return config;
        }
        catch (Exception ex){
            Log.d("ERROR DB getConfig",ex.getMessage());
            return  null;
        }
    }

    public void setDataCleaning(){
        try{
            Config cfRows = new Config();
            Config cfWeek = new Config();

            cfRows.get("checkRows");
            cfWeek.get("checkWeek");

            if (cfRows != null && cfRows.equals("true")) {
                db.execSQL("delete from log where pid in (select pid from log Order By start LIMIT 100);");
            }

            if (cfWeek != null && cfWeek.equals("true")) {
                db.execSQL("delete from log where start > DATE('now','+7 day');");
            }
            db.close();
        }
        catch (Exception ex){
            Log.d("ERROR DB setDataCleaning",ex.getMessage());
        }
    }

    public void saveConfig(Config config) {
        try{
            this.config = config;
            db = getWritableDatabase();
            String sql = "INSERT OR REPLACE INTO config ([key],value) VALUES (?,?);";
            String[] values = new String[2];
            for (Map.Entry<String, String> entry : config.entrySet()) {
                values[0] = entry.getKey();
                values[1] = entry.getValue();
                db.execSQL(sql, values);
            }
            db.close();
        }
        catch (Exception ex){
            Log.d("ERROR DB saveConfig",ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        createUpdate();
    }

    public void createUpdate(){
        try{
            db = getWritableDatabase();
            db.execSQL("DROP TABLE log;");
            db.execSQL("CREATE TABLE IF NOT EXISTS log( \n" +
                    "pid TEXT PRIMARY KEY,"+
                    "start TEXT,"+
                    "stop TEXT,"+
                    "source TEXT,"+
                    "pkg TEXT,"+
                    "duration TEXT,"+
                    "image TEXT,"+
                    "show TEXT,"+
                    "type TEXT,"+
                    "section TEXT,"+
                    "recording TEXT"+
                    ");");
            saveConfig(config);
            onCreate(db);
        }
        catch (Exception ex){
            Log.d("ERROR DB create/upgrade",ex.getMessage());
        }
    }
}
