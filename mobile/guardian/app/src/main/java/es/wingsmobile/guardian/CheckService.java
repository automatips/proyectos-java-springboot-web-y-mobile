package es.wingsmobile.guardian;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.wingsmobile.guardian.db.DatabaseHelper;
import es.wingsmobile.guardian.domain.Config;
import es.wingsmobile.guardian.domain.Record;

public class CheckService extends Service {


    public final static String TAG = "GUARDIAN_LOG";

    private BroadcastReceiver onOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                sleeping = true;
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                sleeping = false;
            }
        }
    };

    private final Timer t = new Timer(true);
    private Process dumpAudio;
    private Process dumpCamera;
    private Process dumpLocation;
    private boolean nextLine;
    private boolean sleeping = false;
    private Map<String, String> pids = new HashMap<>();

    private DatabaseHelper db;
    private Config config;

    private boolean show = true;

    private boolean trackMic=false;
    private boolean trackCam=false;
    private boolean trackLoc=false;
    private boolean alterNotofication = false;
    private boolean vContinue = false;
    private Record currentRecord = null;
    private int startID = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = new DatabaseHelper(this);
        config = db.getConfig();
        registerReceiver(onOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
        registerReceiver(onOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
        listInCache = new LinkedList();
    }

    private void startMyOwnForeground(int id) {

        String NOTIFICATION_CHANNEL_ID = "es.wingsmobile.guardian";
        String channelName = "Guardian Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        Intent appIntent = new Intent(CheckService.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, id, appIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder notificationBuilder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.service_running))
                //.setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(id, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        myStart(startId);
        Bundle data = intent.getBundleExtra("data");
        if(data!=null){
            alterNotofication =  data.getBoolean("alert",false);
        }

        return START_STICKY;
    }

    private List<Record> listInCache;

    private boolean inCache(String type, String appName) {
        for (Record recordCached : listInCache) {
            if (recordCached.getAppName().equals(appName) && recordCached.getType().equals(type)) {
                Date h = new Date();
                long difference = h.getTime() - recordCached.getStart().getTime();
                if (TimeUnit.MILLISECONDS.toSeconds(difference) > 59) {
                    listInCache.remove(recordCached);
                }
                return true;
            }
        }
        return false;
    }

    private void createRecord(String type, String appName, String section) {
        if (!inCache(type, appName) && !type.equals("")) {
            String key = UUID.randomUUID().toString();
            Record r = new Record();
            r.setPid(key);
            r.setStart(new Date());
            r.setEnd(new Date());
            r.setType(type);
            r.setAppName(appName);
            r.setSection(section.toUpperCase());
            if(appName!=null&&!appName.equals("")){
                db.insertEvents(r);
                listInCache.add(r);
            }
        }
    }

    private void myStart(int id) {

        startID = id;

        t.schedule(new TimerTask() {
            @Override
            public void run() {
                //region SETTINGS
                config = db.getConfig();
                String val = config.get("only_sleep");

                String sTrackMic = config.get("trackMic");
                String sTrackCam = config.get("trackCam");
                String sTrackLoc = config.get("trackLoc");

                if (val != null && val.equals("true")) {
                    show = sleeping;
                } else {
                    show = true;
                }

                if (sTrackMic != null && sTrackMic.equals("true")) {
                    trackMic = true;
                } else {
                    trackMic = false;
                }

                if (sTrackCam != null && sTrackCam.equals("true")) {
                    trackCam = true;
                } else {
                    trackCam = false;
                }

                if (sTrackLoc != null && sTrackLoc.equals("true")) {
                    trackLoc = true;
                } else {
                    trackLoc = false;
                }
                //endregion SETTINGS

                //region LOCATION
                if (trackLoc) {
                    try {
                        dumpLocation = Runtime.getRuntime().exec("dumpsys location");
                        String streamLocation = readInputStream(dumpLocation.getInputStream()).toString();
                        if (!streamLocation.contains("mStarted=false")) {
                            String dumpsysResult = streamLocation.substring(streamLocation.indexOf("Historical Records by Provider:"), streamLocation.length()).replace("Historical Records by Provider:", "");
                            Pattern pCurrentlyActive = Pattern.compile(", .*?\\: .*?\\: .*?Duration requested.*?out of the last.*?\\: Currently active.*?");
                            Matcher mCurrentlyActive = pCurrentlyActive.matcher(dumpsysResult);
                            while (mCurrentlyActive.find()) {
                                String sCurrentlyActiveFilter = mCurrentlyActive.group(0);
                                Pattern pCurrentlyActiveFilter = Pattern.compile(", .*?\\: .*?\\: .*?Duration requested.*?out of the last.*?");
                                Matcher mCurrentlyActiveFilter = pCurrentlyActiveFilter.matcher(sCurrentlyActiveFilter);
                                String sCurrently = "";
                                //Itera sobre si misma con menos filtros, ya que los activos tienen al final "Currently active", los demás son histórico
                                while (mCurrentlyActiveFilter.find()) {
                                    sCurrently = mCurrentlyActiveFilter.group(0);
                                }
                                if (sCurrently != "") {
                                    String appName = sCurrently.substring(2, sCurrently.indexOf(":"));
                                    String sCurrentlySection = sCurrently.replace(", " + appName + ": ", "");
                                    String section = sCurrentlySection.substring(0, sCurrentlySection.indexOf(":"));
                                    createRecord("LOCATION", appName, section);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "Error en dumpsys location", ex);
                    }
                }
                //endregion LOCATION

                //region CAMERA
                if (trackCam) {
                    try {
                        dumpCamera = Runtime.getRuntime().exec("dumpsys media.camera");
                        String streamCamera = readInputStream(dumpCamera.getInputStream()).toString();
                        String[] arr = streamCamera.split("== ");
                        for (String linea : arr) {
                            Pattern p = Pattern.compile("\\(Camera ID:.*\\{\\}\\)");
                            Matcher m = p.matcher(linea);
                            if (m.find()) {
                                String[] arrOutput = m.group(0).split(", ");
                                for (String lineaOutput : arrOutput) {
                                    if (lineaOutput.contains("Client Package Name: ")) {
                                        String packageName = lineaOutput.replace("Client Package Name: ", "");
                                        String section = "NORMAL";
                                        if (lineaOutput.replace(" ", "").trim().contains("CameraID:1")) {
                                            section = "FRONTAL";
                                        }
                                        createRecord("CAMERA", packageName, section);
                                    }
                                }
                            }
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "Error en dumpsys media.camera", ex);
                    }
                }
                //endregion CAMERA

                //region AUDIO_FLINGER
                if (trackMic) {
                    try {
                        dumpAudio = Runtime.getRuntime().exec("dumpsys media.audio_flinger");
                        String audioDumpString = readInputStream(dumpAudio.getInputStream()).toString();
                        Pattern p = Pattern.compile(" +?Input thread.+");
                        Matcher m = p.matcher(audioDumpString);

                        if (m.find()) {
                            if (currentRecord==null) {
                                currentRecord = new Record();
                                currentRecord.setStart (new Date ());
                                vContinue = true;

                                String currentGroup = m.group(0);
                                String device = "";
                                String pid = "";
                                String appNAme = "";
                                boolean nextIsInfo = false;
                                for(String line : currentGroup.split(", +?")){
                                    if(line.contains("Input device: ")){
                                        device = line.split(":")[1].trim();
                                    }
                                    if(line.contains("Active Client")){
                                        nextIsInfo=true;
                                        continue;
                                    }
                                    if(nextIsInfo){
                                        //yes 4237 5097 A 0x000 00000001 00000010 16000 00016F80 960 n
                                        String[] infoLine = line.split(" +");
                                        pid = infoLine[1];
                                        nextIsInfo = false;
                                    }
                                }
                                currentRecord.setPid(pid+currentRecord.getStart ());
                                try{ processPids(pid); } catch (Exception e){}
                                appNAme = pids.get(pid);

                                if (appNAme == null) {
                                    try{ processPids(pid); } catch (Exception e){}
                                    appNAme = pids.get(pid);
                                }
                                Record r = new Record();
                                device = device.substring(device.indexOf("(") + 1, device.length() - 1).trim();
                                if (currentRecord != null) {
                                    currentRecord.setSource(device);
                                }

                                if(appNAme!=null){
                                    currentRecord.setAppName(appNAme);
                                }

                                currentRecord.setEnd(new Date());
                                currentRecord.setSection("");
                                currentRecord.setRecording(true);
                                currentRecord.setType("MICROPHONE");
                                db.insertEvents(currentRecord);
                            }
                            else {
                                currentRecord.setEnd(new Date());
                                currentRecord.setRecording(true);
                                currentRecord.setType("MICROPHONE");
                                db.insertEvents(currentRecord);
                            }

                            } //Si no se está grabando
                            else {
                                if (currentRecord!=null){
                                    currentRecord.setEnd(new Date());
                                    currentRecord.setRecording ( false );
                                    db.insertEvents ( currentRecord );

                                    Intent i= new Intent();
                                    i.setAction("com.wings.phone");
                                    i.setPackage("com.wings.phone");
                                    Bundle data = new Bundle();
                                    data.putString("appNAme",currentRecord.getAppName ());
                                    data.putString("device",currentRecord.getSource ());
                                    data.putString("start",currentRecord.getStart().toString());
                                    i.putExtra("data",data);
                                    getApplicationContext().sendBroadcast(i);

                                    Intent appIntent = new Intent(CheckService.this, MainActivity.class);
                                    final int id = startID;
                                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), id, appIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    Notification.Builder notificationBuilder = new Notification.Builder(getApplicationContext (), "es.wingsmobile.guardian");
                                    Notification notification = notificationBuilder.setOngoing(true)
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setContentTitle(getString(R.string.alert_recording))
                                            .setCategory(Notification.CATEGORY_SERVICE)
                                            .setContentIntent(pendingIntent)
                                            .build();
                                    startForeground(id, notification);
                                    currentRecord = null;
                                }
                            }
                        } catch (IOException e) {
                        e.printStackTrace ( );
                    }

                    //endregion
                }
            }
        }, 1000, 1000);

        startMyOwnForeground(id);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        t.cancel();
        unregisterReceiver(onOffReceiver);
    }

    private void processPids(String pid) throws Exception {

        String[] cmd = {
                "dumpsys",
                "input",
                pid
        };
        Process ps = Runtime.getRuntime().exec(cmd);
        List<String> psOut = readInputStream(ps.getInputStream());

        for (String str : psOut) {
            if (str.contains("ownerPid=" + pid)) {
                String name = str.substring(str.indexOf("{") + 1, str.indexOf("}"));
                String[] arr = name.split(" ");
                try{ name = arr[2].substring(0, arr[2].indexOf("/")); } catch (StringIndexOutOfBoundsException e){}
                pids.put(pid, name);
                break;
            }
        }
    }

    private List<String> readInputStream(InputStream is) {
        List<String> ret = new LinkedList<>();
        try {
            BufferedReader bf = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = bf.readLine()) != null) {
                ret.add(line.trim());
            }
            bf.close();
        } catch (Exception e) {
            Log.d("Process", "Error>> :" + e.toString());
        }
        return ret;
    }
}
