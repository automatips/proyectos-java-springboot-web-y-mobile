package com.wings.phone.data.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;

import com.wings.phone.R;
import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.util.ContactUtils;
import com.wings.phone.util.Settings;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.contact.Number;
import com.wings.voip.model.requests.SyncContactRequest;
import com.wings.voip.model.requests.SyncContactResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;


public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private ContentResolver mContentResolver;

    private String apiUrl;
    private String appId;
    private String appToken;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();

        apiUrl = getContext().getString(R.string.settings_api_url);
        appId = getContext().getString(R.string.settings_api_app_id);
        appToken = getContext().getString(R.string.settings_api_app_token);
    }


    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        if(Settings.isRegistered()){
            //get all contacts
            String lastUpdated = Settings.getPreference("wings.lastUpdated", null, getContext());


            List<Contact> contacts = ContactsManager.getAllContacts(getContext());

            //sync even if there's no contact here, might be changes
            //on the server side, new services etc
            //create request
            SyncContactRequest contactRequest = new SyncContactRequest();
            contactRequest.setLogin(Settings.getRegisteredNumber());
            contactRequest.setPassword(Settings.getToken());
            contactRequest.setContacts(contacts);
            contactRequest.setFirstTime(lastUpdated == null);

            //make request
            Call<SyncContactResponse> call = ApiClientFactory.getClient(apiUrl).syncContacts(appId, appToken, contactRequest);
            try {
                Response<SyncContactResponse> response = call.execute();
                if (response != null && response.body() != null) {
                    List<Contact> toCheck = response.body().getContacts();
                    ContactsManager.deleteAllMimeTypes(getContext());
                    List<String> processedNumbers = new LinkedList<>();
                    for (Contact contact : toCheck) {
                        //if(shouldProcess(contact,processedNumbers)){
                        if(true){
                            ContactsManager.createAllMimeTypes(getContext(), contact);
                        }
                    }
                } else {
                }
            } catch (IOException e) {
                syncResult.stats.numAuthExceptions++;
                Timber.e(e);
            }
            //update lastUpdate timestamp
            Settings.setPreference("wings.lastUpdated", String.valueOf(System.currentTimeMillis()), getContext());
        }



    }

    private boolean shouldProcess(Contact contact, List<String> lista) {
        for(Number n : contact.getNumbers()){
            if(!lista.contains(n.getRawNumber()) && !lista.contains(n.getFormattedNumber())){
                lista.add(n.getRawNumber());
                lista.add(n.getFormattedNumber());
                return true;
            }
        }

        return false;
    }

}
