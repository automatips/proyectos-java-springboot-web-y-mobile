package com.wings.phone.views;

import android.view.View;

import net.frakbot.glowpadbackport.GlowPadView;

public class GlowPadViewListenerStub implements GlowPadView.OnTriggerListener {

    @Override
    public void onGrabbed(View v, int handle) {}

    @Override
    public void onReleased(View v, int handle) {}

    @Override
    public void onTrigger(View v, int target) {}

    @Override
    public void onGrabbedStateChange(View v, int handle) {}

    @Override
    public void onFinishFinalAnimation() {}
}
