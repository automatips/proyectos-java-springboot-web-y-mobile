package com.wings.phone.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;

import com.wings.phone.R;
import com.wings.phone.adapters.ContactsAdapter;
import com.wings.phone.gsm.MakeGsmCallHelper;
import com.wings.phone.telecom.DtmfCallback;
import com.wings.phone.util.DialogUtils;
import com.wings.phone.util.InternetCheck;
import com.wings.phone.util.NetworkUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;
import com.wings.phone.util.WingsConst;
import com.wings.phone.views.DialButtonView;

import timber.log.Timber;

/**
 * Created by seba on 26/04/17.
 */

public class DialpadFragment extends Fragment {

    public static final String TAG = "DialpadFragment";
    public View.OnClickListener hideDialerButtonClickListener;

    //static final ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_DTMF, 100);

    private StringBuilder stringNumber = new StringBuilder();
    private static TextView textNumber;
    private static EditText textNumberEdit;

    private boolean showCallButtons = true;
    private boolean showWhiteBackground = false;
    private boolean showDialedNumberText = true;
    private boolean enableContactSearch = true;

    private View hideDialerButton;
    private View.OnClickListener clickListener;
    private DtmfCallback dtmfCallback;
    private boolean edited = false;
    private boolean initialized = false;


    private static ListView listV;
    private static ContactsAdapter contactsAdapter;
    private Integer transferFromId;
    private int mPreviousLength;
    private static View view;


    public void init(boolean showCallButtons, boolean showWhiteBackground, boolean showDialedNumberText, boolean enableContactSearch,DtmfCallback dtmfCallback, View.OnClickListener listener) {
        this.showCallButtons = showCallButtons;
        this.showWhiteBackground = showWhiteBackground;
        this.showDialedNumberText = showDialedNumberText;
        this.clickListener = listener;
        this.dtmfCallback = dtmfCallback;
        this.enableContactSearch = enableContactSearch;
        initialized = true;
    }

    public void init(boolean showCallButtons, boolean showWhiteBackground, boolean showDialedNumberText, View.OnClickListener listener) {
        this.showCallButtons = showCallButtons;
        this.showWhiteBackground = showWhiteBackground;
        this.showDialedNumberText = showDialedNumberText;
        this.clickListener = listener;
        initialized = true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactsAdapter = new ContactsAdapter(getContext(), R.layout.view_contact_item, false, "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dialpad, null);

        hideDialerButton = view.findViewById(R.id.hide_dialer_button);

        if(hideDialerButtonClickListener != null){
            hideDialerButton.setOnClickListener(hideDialerButtonClickListener);
        }else{
            hideDialerButton.setOnClickListener(clickListener);
        }


        if (showWhiteBackground) {
            view.setBackgroundColor(getResources().getColor(R.color.colorWhite, null));
        }

        textNumber = view.findViewById(R.id.dial_number);
        textNumberEdit = (EditText) view.findViewById(R.id.dial_number_edit);

        // Set to TYPE_NULL on all Android API versions
        textNumberEdit.setInputType(InputType.TYPE_NULL);
        textNumberEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);



        listV = (ListView) view.findViewById(R.id.list_contacts);
        listV.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                TextView tv = ((Activity) getContext()).findViewById(R.id.tvTLC);
                if(listV.getCount() < 10){
                    tv.setVisibility(View.INVISIBLE);
                }else{
                    tv.setVisibility(View.VISIBLE);
                    tv.setText(listV.getCount()+" "+getString(R.string.contactTV));
                }

            }
        });
        if(!showDialedNumberText){
            listV.setVisibility(View.GONE);
        }


        if(enableContactSearch){
            contactsAdapter.setShowExtraButtons(true);
            listV.setFastScrollEnabled(true);
            listV.setAdapter(contactsAdapter);

            TextWatcher textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                    //listV.clearAnimation();
                    String t = charSequence.toString();
                    if(t.length()>0){
                        refreshList(t);
                    }
                    //contactsAdapter.setFilterQuery(charSequence.toString());

                }

                @Override
                public void afterTextChanged(Editable charSequence) {

                }
            };
            ((EditText) view.findViewById(R.id.dial_number_edit)).addTextChangedListener(textWatcher);
            ((TextView) view.findViewById(R.id.dial_number)).addTextChangedListener(textWatcher);
        }


        View.OnClickListener clickListener = new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //goTextView();
                        dial(v);
                    }});
            }
        };
        if (getArguments() != null) {
            String number = getArguments().getString("number");
            textNumber.setText(number);
            stringNumber = new StringBuilder(number);
        }

        if (showCallButtons) {
            view.findViewById(R.id.call_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    goTextView();

                    if (!stringNumber.toString().trim().isEmpty()) {
                        String number = stringNumber.toString().trim();
                        MakeGsmCallHelper.makeIntent(getContext(), number);
                    }
                }
            });


            if (Settings.isRegistered()) {

                ImageView wingsButton = view.findViewById(R.id.call_button_wings);

                ImageViewCompat.setImageTintList(wingsButton, ColorStateList.valueOf(getResources().getColor(R.color.colorBlue, null)));
                //wingsButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorBlue), android.graphics.PorterDuff.Mode.MULTIPLY);

                wingsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        goTextView();
                        if(textNumber.getText().toString().trim().isEmpty()){
                            return;
                        }
                        if (NetworkUtils.isOnline(getContext())) {

                            NetworkUtils.hasInternet(new InternetCheck.Consumer() {
                                @Override
                                public void accept(Boolean internet) {
                                    if (internet) {

                                        String number = textNumber.getText().toString().trim();
                                        number = StringUtils.cleanUpNumber(number);
                                        String country = Settings.getCountry();
                                        String numberFormatted = PhoneNumberUtils.formatNumberToE164(number, country.toUpperCase());
                                        Uri uri = Uri.parse("wings:" + (numberFormatted != null ? numberFormatted : number));
                                        Intent intent = new Intent(WingsConst.INTENT_WINGS_WINGS_CALL, uri);

                                        getContext().startActivity(intent);

                                    } else {
                                        NetworkUtils.showNoInternetDialog(getContext());
                                    }
                                }
                            });
                        } else {
                            NetworkUtils.showNoInternetDialog(getContext());
                        }
                    }
                });

            }

        } else {
            view.findViewById(R.id.call_button).setVisibility(View.GONE);
            view.findViewById(R.id.call_button_wings).setVisibility(View.GONE);
        }
        textNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
                textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);
                textNumber.setVisibility(View.GONE);
                textNumberEdit.setVisibility(View.VISIBLE);
                textNumberEdit.setText("");
                textNumberEdit.append(textNumber.getText());
                setEdited(true);
            }
        });
        textNumberEdit.setShowSoftInputOnFocus(false);

        //hide when on call
        if (!showDialedNumberText) {
            view.findViewById(R.id.dial_number_text).setVisibility(View.GONE);
            view.findViewById(R.id.list_contacts).setVisibility(View.INVISIBLE);
        }

        view.findViewById(R.id.backspace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
                textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);

                if (isEdited()) {

                    //stringNumber = new StringBuilder(textNumberEdit.getText());

                    int start = textNumberEdit.getSelectionStart();
                    int end = textNumberEdit.getSelectionEnd();

                    if (start == end && start > 0 && textNumberEdit.length() >= 1) {
                        stringNumber.replace(start - 1, end, "");

                        textNumber.setText("");
                        textNumber.append(stringNumber.toString());
                        textNumberEdit.setText("");
                        textNumberEdit.append(stringNumber.toString());

                        textNumberEdit.setSelection(end - 1, end - 1);

                        if (start == stringNumber.length() - 1) {
                            goTextView();

                        }
                    }

                } else {
                    goTextView();
                    backSpace();
                    refreshList(stringNumber.toString());

                }
            }
        });

        view.findViewById(R.id.backspace).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                goTextView();
                clear();
                refreshList("");
                return true;
            }
        });

        LinearLayout button0 = (LinearLayout) view.findViewById(R.id.button_0);
        button0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                plusSign();
                return true;
            }
        });

        LinearLayout button1 = (LinearLayout) view.findViewById(R.id.button_1);
        button1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                voicemail();
                return true;
            }
        });

        LinearLayout button2 = (LinearLayout) view.findViewById(R.id.button_2);
        LinearLayout button3 = (LinearLayout) view.findViewById(R.id.button_3);
        LinearLayout button4 = (LinearLayout) view.findViewById(R.id.button_4);
        LinearLayout button5 = (LinearLayout) view.findViewById(R.id.button_5);
        LinearLayout button6 = (LinearLayout) view.findViewById(R.id.button_6);
        LinearLayout button7 = (LinearLayout) view.findViewById(R.id.button_7);
        LinearLayout button8 = (LinearLayout) view.findViewById(R.id.button_8);
        LinearLayout button9 = (LinearLayout) view.findViewById(R.id.button_9);
        LinearLayout buttonStar = (LinearLayout) view.findViewById(R.id.button_star);
        LinearLayout buttonDash = (LinearLayout) view.findViewById(R.id.button_dash);

        new DialButtonView(button0).setNumber("0").setText("+");
        new DialButtonView(button3).setNumber("3").setText("DEF");
        new DialButtonView(button4).setNumber("4").setText("GHI");
        new DialButtonView(button5).setNumber("5").setText("JKL");
        new DialButtonView(button6).setNumber("6").setText("MNO");
        new DialButtonView(button7).setNumber("7").setText("PQRS");
        new DialButtonView(button8).setNumber("8").setText("TUV");
        new DialButtonView(button9).setNumber("9").setText("WXYZ");

        button0.setOnClickListener(clickListener);
        button1.setOnClickListener(clickListener);
        button2.setOnClickListener(clickListener);
        button3.setOnClickListener(clickListener);
        button4.setOnClickListener(clickListener);
        button5.setOnClickListener(clickListener);
        button6.setOnClickListener(clickListener);
        button7.setOnClickListener(clickListener);
        button8.setOnClickListener(clickListener);
        button9.setOnClickListener(clickListener);
        buttonDash.setOnClickListener(clickListener);
        buttonStar.setOnClickListener(clickListener);
        if(textNumber.getText().length()>0){
            refreshList(textNumber.getText().toString());
        }


        String number = stringNumber.toString();
        if(!number.equals("")){
            textNumber.setText(number);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String number = stringNumber.toString();
        refreshList(number);

    }

    public void dial(View view) {

        textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
        textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);

        view.performHapticFeedback(HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

        String dialed = "";
        switch (view.getId()) {
            case R.id.button_0:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_0, 100);
                dialed = "0";
                break;
            case R.id.button_1:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_1, 100);
                dialed = "1";
                break;
            case R.id.button_2:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_2, 100);
                dialed = "2";
                break;
            case R.id.button_3:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_3, 100);
                dialed = "3";
                break;
            case R.id.button_4:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_4, 100);
                dialed = "4";
                break;
            case R.id.button_5:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_5, 100);
                dialed = "5";
                break;
            case R.id.button_6:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_6, 100);
                dialed = "6";
                break;
            case R.id.button_7:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_7, 100);
                dialed = "7";
                break;
            case R.id.button_8:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_8, 100);
                dialed = "8";
                break;
            case R.id.button_9:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_9, 100);
                dialed = "9";
                break;
            case R.id.button_dash:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_P, 100);
                dialed = "#";
                break;
            case R.id.button_star:
                //toneGenerator.startTone(ToneGenerator.TONE_DTMF_S, 100);
                dialed = "*";
                break;
        }

        if (dtmfCallback != null) {
            final String toDial = dialed;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dtmfCallback.playDTMF(toDial.charAt(0));
                }
            });
        }

        if(stringNumber.length()>24){
            return;
        }
        if (isEdited()) {

            //stringNumber = new StringBuilder(textNumberEdit.getText());

            int start = textNumberEdit.getSelectionStart();
            int end = textNumberEdit.getSelectionEnd();
            if (start == end) {
                stringNumber.insert(start, dialed);


                textNumber.setText("");
                textNumber.append(stringNumber.toString());
                textNumberEdit.setText("");
                textNumberEdit.append(stringNumber.toString());

                textNumberEdit.setSelection(start + 1, end + 1);
            }

            if (start == end && start == stringNumber.length() - 1) {
                goTextView();
            }

        } else {


            stringNumber.append(dialed);
            textNumberEdit.setText("");


            textNumber.setText("");
            textNumber.append(stringNumber.toString());
            textNumber.invalidate();

        }

        if(textNumber.length()>12 || textNumberEdit.length()>12){
            textNumber.setGravity(Gravity.END);
            textNumberEdit.setGravity(Gravity.END);
        }else{
            textNumber.setGravity(Gravity.CENTER_HORIZONTAL);
            textNumberEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        }


        //enable debug options
        if (stringNumber.toString().equals("*#*#94647662453#*#*")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            stringNumber = new StringBuilder();
            Settings.setPreference("config.allow_debug", "true", getContext());
            Toast.makeText(getContext(), "Debug enabled", Toast.LENGTH_LONG).show();
            return;
        }

        //enable debug options
        if (stringNumber.toString().equals("*#*#466453342537#*#*")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            stringNumber = new StringBuilder();
            Settings.setPreference("config.google_dialer", "true", getContext());
            Toast.makeText(getContext(), "Google Dialer enabled", Toast.LENGTH_LONG).show();
            return;
        }

        //special codes
        //IMEI
        if (stringNumber.toString().equals("*#06#")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            stringNumber = new StringBuilder();
            DialogUtils.showImeiDialog(getContext());
            return;
        }

        //factory test
        if (stringNumber.toString().equals("*#66#")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            String number = stringNumber.toString();
            stringNumber = new StringBuilder();
            handleSecretCode(getContext(), number);
            return;
        }

        //factory test W6
        if (stringNumber.toString().equals("*#66#")) {
            try {
                Intent showRegInfoIntent = new Intent("com.xw.intent.action.HARDWARE_TEST");
                startActivity(showRegInfoIntent);
            } catch (Exception e) {
                Timber.e(e, "com.xw.intent.action.HARDWARE_TEST not found");
            }
            return;
        }

        //MTK LOGGER
        if (stringNumber.toString().equals("*#3646#")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            stringNumber = new StringBuilder();
            try {
                Intent intent = new Intent();
                intent.setClassName("com.mediatek.mtklogger", "com.mediatek.mtklogger.MainActivity");
                getContext().startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        //teecheck
        if (stringNumber.toString().equals("*#08#")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            stringNumber = new StringBuilder();
            try {
                Intent intent = new Intent();
                intent.setClassName("com.magcomm.teecheck", "com.magcomm.teecheck.TeeCheckActivity");
                getContext().startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        //regulatory info
        if (stringNumber.toString().equals("*#07#")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            stringNumber = new StringBuilder();
            try {
                Intent intent = new Intent("android.settings.SHOW_REGULATORY_INFO");
                getContext().startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        //anything that start with *#*# and ends with #*#*
        if (stringNumber.toString().startsWith("*#*#") && stringNumber.toString().endsWith("#*#*")) {
            textNumber.setText("");
            textNumberEdit.setText("");
            String number = stringNumber.toString();
            stringNumber = new StringBuilder();
            handleSecretCode(getContext(), number);
            return;
        }

    }

    private void refreshList(String qry){
        contactsAdapter.getFilter().filter(qry);
    }

    private void backSpace() {
        textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
        textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);
        if (stringNumber.length() > 0) {
            stringNumber.setLength(stringNumber.length() - 1);
        }
        textNumber.setText((stringNumber.toString()));
        if(textNumber.length()>12 || textNumberEdit.length()>12){
            textNumber.setGravity(Gravity.END);
            textNumberEdit.setGravity(Gravity.END);
        }else{
            textNumber.setGravity(Gravity.CENTER_HORIZONTAL);
            textNumberEdit.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        listV.clearAnimation();
    }

    private void clear() {
        textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
        textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);
        stringNumber.setLength(0);
        textNumber.setText((stringNumber.toString()));
    }

    private void plusSign() {
        textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
        textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);

        if (isEdited()) {
            stringNumber = new StringBuilder(textNumberEdit.getText());
            int start = textNumberEdit.getSelectionStart();
            int end = textNumberEdit.getSelectionEnd();
            if (start == end) {
                stringNumber.insert(start, "+");
                textNumber.setText("");
                textNumber.append((stringNumber.toString()));
                textNumberEdit.setText("");
                textNumberEdit.append((stringNumber.toString()));

                textNumberEdit.setSelection(start + 1, end + 1);
            }

            if (start == end && start == stringNumber.length() - 1) {
                goTextView();
            }

        } else {
            stringNumber.append("+");
            textNumber.setText("");
            textNumber.append((stringNumber.toString()));
            textNumberEdit.setText("");
            textNumberEdit.append((stringNumber.toString()));
        }
    }

    private void voicemail() {
        try {
            TelephonyManager tel = (TelephonyManager) getActivity().getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            @SuppressLint("MissingPermission") String number = Uri.encode(tel.getVoiceMailNumber());
            Uri uri = Uri.parse("tel:" + number);
            Intent intent = new Intent(Intent.ACTION_CALL, uri);
            startActivity(intent);
        } catch (Exception e) {
        }
    }

    private String formatNumber(String number) {
        String formatted = PhoneNumberUtils.formatNumber(number, "ES");
        return formatted != null ? formatted : number;
    }

    public View getHideDialerButton() {
        return hideDialerButton;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    private void goTextView() {
        textNumber = (TextView) getActivity().findViewById(R.id.dial_number);
        textNumberEdit = (EditText) getActivity().findViewById(R.id.dial_number_edit);
        if (isEdited()) {
            setEdited(false);



            textNumber.setText(stringNumber.toString());
            textNumber.setVisibility(View.VISIBLE);
            textNumberEdit.setVisibility(View.GONE);
        }
    }

    public boolean isInitialized() {
        return initialized;
    }

    /**
     * Handles secret codes to launch arbitrary activities.
     *
     * @param context the context to use
     * @param number
     */
    public static void handleSecretCode(Context context, String number) {
        try {
            String secretCode = number.replaceAll("#", "");
            secretCode = secretCode.replaceAll("\\*", "");
            context.getSystemService(TelephonyManager.class).sendDialerSpecialCode(secretCode);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

}
