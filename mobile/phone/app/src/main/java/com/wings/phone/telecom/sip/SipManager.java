package com.wings.phone.telecom.sip;

import android.content.Context;
import android.content.ContextWrapper;

import com.wings.phone.BuildConfig;
import com.wings.phone.R;
import com.wings.phone.receivers.BluetoothManager;
import com.wings.phone.telecom.CallWrapper;
import com.wings.phone.util.EncryptUtils;
import com.wings.phone.util.FileUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.WingsConst;

import org.linphone.core.Address;
import org.linphone.core.AuthInfo;
import org.linphone.core.AuthMethod;
import org.linphone.core.Call;
import org.linphone.core.CallParams;
import org.linphone.core.CallStats;
import org.linphone.core.Conference;
import org.linphone.core.ConferenceParams;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.core.Factory;
import org.linphone.core.LogCollectionState;
import org.linphone.core.MediaEncryption;
import org.linphone.core.NatPolicy;
import org.linphone.core.PayloadType;
import org.linphone.core.ProxyConfig;
import org.linphone.core.RegistrationState;
import org.linphone.core.Transports;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SipManager {

    /**
     * Core Listener
     **/
    private class SipCoreListener extends CoreListenerStub {
        int counter = 0;

        //TODO: agregar mas eventos y loguear a ver cuando se corta de enserio
        //TODO: probar cuando el freeswitch esta apagado al register y al hacer llamadas

        @Override
        public void onRegistrationStateChanged(Core core, ProxyConfig proxyConfig, RegistrationState registrationState, String message) {

            if (registrationState.equals(RegistrationState.Ok) && !pendingCalls.isEmpty()) {

                CallWrapper pending = pendingCalls.remove(0);
                pending.setStatus(WingsConst.CALL_STATUS_REGISTERED);
                sipListener.onRegistrationStateChanged(pending);
                makeFinalCall(pending);

            } else if (registrationState.equals(RegistrationState.Failed)) {
                if (!pendingCalls.isEmpty()) {

                    CallWrapper pending = pendingCalls.remove(0);
                    pending.setStatus(WingsConst.CALL_STATUS_REGISTRATION_FAILED);
                    sipListener.onRegistrationStateChanged(pending);

                }
            }

        }

        @Override
        public void onAuthenticationRequested(Core core, AuthInfo authInfo, AuthMethod method) {
            if (counter++ < 3) {
                authInfo.setPassword(pass);
                core.addAuthInfo(authInfo);
            }
        }

        @Override
        public void onCallCreated(Core lc, Call call) {
            sipListener.onCallCreated(lc, call);
        }

        @Override
        public void onCallStateChanged(Core lc, Call call, Call.State cstate, String message) {
            sipListener.onCallStateChanged(call, cstate, message);
        }

        @Override
        public void onCallEncryptionChanged(Core lc, Call call, boolean on, String authenticationToken) {
            sipListener.onEncryptionChanged(call, on, authenticationToken);
        }

        @Override
        public void onCallStatsUpdated(Core lc, Call call, CallStats stats) {

            if (BuildConfig.DEBUG) {
                StringBuilder b = new StringBuilder();
                b.append("Jitter buffer size ").append(stats.getJitterBufferSizeMs()).append(System.lineSeparator());
                b.append("Late packets ").append(stats.getLatePacketsCumulativeNumber()).append(System.lineSeparator());
                b.append("Quality ").append(call.getCurrentQuality()).append(System.lineSeparator());
                b.append("Quality avg ").append(call.getAverageQuality()).append(System.lineSeparator());
                b.append("Download bandwidth ").append(stats.getDownloadBandwidth()).append(" kbit/s").append(System.lineSeparator());
                b.append("Upload bandwidth ").append(stats.getUploadBandwidth()).append(" kbit/s").append(System.lineSeparator());
                b.append("Audio Jitter comp ").append(lc.getAudioJittcomp()).append(System.lineSeparator());
                b.append("Audio Jitter comp enabled ").append(lc.audioAdaptiveJittcompEnabled()).append(System.lineSeparator());
                b.append(System.lineSeparator());
                //b.append("Call Log ").append(call.getCallLog().toStr()).append(System.lineSeparator());
                b.append("Media Encryption ").append(call.getCurrentParams().getMediaEncryption().name()).append(System.lineSeparator());
                b.append("Clock Rate ").append(call.getCurrentParams().getUsedAudioPayloadType().getClockRate()).append(System.lineSeparator());
                b.append("Desc ").append(call.getCurrentParams().getUsedAudioPayloadType().getDescription()).append(System.lineSeparator());
                b.append("Encoder ").append(call.getCurrentParams().getUsedAudioPayloadType().getEncoderDescription()).append(System.lineSeparator());
                b.append("Bit Rate ").append(call.getCurrentParams().getUsedAudioPayloadType().getNormalBitrate()).append(System.lineSeparator());
                b.append("Receive framerate ").append(call.getCurrentParams().getReceivedFramerate()).append(System.lineSeparator());
                b.append("Receive FMTP").append(call.getCurrentParams().getUsedAudioPayloadType().getRecvFmtp()).append(System.lineSeparator());
                b.append("Send FMTP").append(call.getCurrentParams().getUsedAudioPayloadType().getSendFmtp()).append(System.lineSeparator());

                sipListener.onStatsUpdate(b.toString());
            }
        }


        @Override
        public void onLogCollectionUploadProgressIndication(Core lc, int offset, int total) {
            if (offset == total) {
                lc.enableLogCollection(LogCollectionState.Disabled);
                destroy();
            }
        }

        @Override
        public void onLogCollectionUploadStateChanged(Core lc, Core.LogCollectionUploadState state, String info) {
            if (state == Core.LogCollectionUploadState.Delivered || state == Core.LogCollectionUploadState.NotDelivered) {
                lc.enableLogCollection(LogCollectionState.Disabled);
                destroy();
            }
        }
    }


    /**
     * instance variables
     **/
    private static SipManager instance;
    private SipCoreListener coreListener = new SipCoreListener();
    private Context context;
    private Core core;
    private AuthInfo authInfo;
    private Timer mTimer;
    private String server = Settings.getProvisioning("sip.sipServer", "sip.wingsmobile.net");
    private String stunServer = Settings.getProvisioning("sip.sipStunServer", "stun.wingsmobile.net");
    private List<CallWrapper> pendingCalls = new LinkedList<>();
    private String pass;
    private SipListener sipListener;

    /**
     * Singleton
     **/
    public static synchronized SipManager getInstance(Context context, SipListener listener) {
        if (instance == null) {
            instance = new SipManager(context);
        }
        instance.setSipListener(listener);
        return instance;
    }

    /**
     * private constructor
     **/
    private SipManager(Context context) {

        this.context = context;
        Factory.instance().setDebugMode(BuildConfig.DEBUG, "WingsPhone");
        core = Factory.instance().createCore(null, null, context);
        setCodecPrefs(Settings.getProvisioning("sip.codecString", "OPUS,PCMU,PCMA,G729"));

        core.getConfig().setInt("audio", "codec_bitrate_limit", 10);

        core.start();

        core.setUserAgent("WingsPhone", BuildConfig.VERSION_NAME);
        core.setDnsServers(Settings.getProvisioning("sip.dnsServers", "8.8.8.8,8.8.4.4").split(","));
        core.setDnsServersApp(Settings.getProvisioning("sip.dnsServers", "8.8.8.8,8.8.4.4").split(","));
        core.enableIpv6(false);

        final String basePath = context.getFilesDir().getAbsolutePath();
        String rootCA = basePath + "/rootca.pem";
        FileUtils.copyFileFromPackage(context, R.raw.rootca, new File(rootCA).getName());
        core.setRootCa(rootCA);

        // set audio port range
        core.setAudioPortRange(6000, 8000);

        // set sound files
        //core.setRingback(null);
        core.setPlayFile(null);
        core.setRing(null);

        // enable echo cancellation
        core.enableEchoCancellation(true);
        core.enableEchoLimiter(false);

        core.setPlaybackGainDb(0.0f);
        core.setMicGainDb(0.0f);
        core.enableMic(true);

        core.enableVideoDisplay(false);
        core.enableVideoCapture(false);

        final Transports transports = core.getTransports();
        transports.setUdpPort(Integer.valueOf(Settings.getProvisioning("sip.udpPort", "5060")));
        transports.setTcpPort(Integer.valueOf(Settings.getProvisioning("sip.tcpPort", "5060")));
        transports.setTlsPort(Integer.valueOf(Settings.getProvisioning("sip.tlsPort", "5061")));
        core.setTransports(transports); // liblinphone requires setting transports again.

        String audioJitter = Settings.getProvisioning("sip.audioJitterComp", "60");
        core.setAudioJittcomp(Integer.valueOf(audioJitter));

        core.enableAdaptiveRateControl(Settings.getProvisioning("sip.enableAdaptativeRateControl", "true").equals("true"));
        core.enableAudioAdaptiveJittcomp(Settings.getProvisioning("sip.enableAudioAdaptiveJittcomp", "true").equals("true"));
        core.enableEchoCancellation(Settings.getProvisioning("sip.enableEchoCancelation", "true").equals("true"));
        core.enableEchoLimiter(Settings.getProvisioning("sip.enableEchoLimiter", "true").equals("true"));

        LogCollectionState state = LogCollectionState.Disabled;
        if (Settings.getProvisioning("config.enableLogCollection", "true").equals("true")) {
            state = LogCollectionState.Enabled;
        }

        core.enableLogCollection(state);
        core.setLogCollectionUploadServerUrl(Settings.getProvisioning("config.logCollectionUploadServer", "http://switch.wingsmobile.net/api/logs/upload"));
        core.setLogCollectionPrefix(Settings.getRegisteredNumber());
        core.setLogCollectionPath(context.getFilesDir().getAbsolutePath());
        core.setNortpTimeout(Integer.valueOf(Settings.getProvisioning("sip.noRtpTimeout", "60")));

        //add core listener
        core.addListener(coreListener);

    }

    public void destroy() {

        if (core.logCollectionEnabled() != LogCollectionState.Disabled) {
            core.uploadLogCollection();
        } else {

            //TODO: revisar como hacer el unregister
            core.terminateAllCalls();

            if (core.getDefaultProxyConfig() != null) {
                core.getDefaultProxyConfig().edit();
                core.getDefaultProxyConfig().enableRegister(false);
                core.getDefaultProxyConfig().done();
            }

            //cancel timer
            mTimer.cancel();

            //iterate a couple of times to allow unregister
            for (int i = 0; i < 500; i++) {

               /* try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/

                core.iterate();
            }

            core.enableKeepAlive(false);
            core.clearAllAuthInfo();
            core.clearProxyConfig();

            instance = null;

        }

    }

    /**
     * Set listener
     *
     * @param sipListener
     */
    public void setSipListener(SipListener sipListener) {
        this.sipListener = sipListener;
    }

    /**
     * liblinphone iterate
     **/
    private void startIterate() {
        core.iterate();
        TimerTask lTask = new TimerTask() {
            @Override
            public void run() {
                core.iterate();
            }
        };
        mTimer = new Timer("WingsPhone scheduler");
        mTimer.schedule(lTask, 0, 20);
    }


    /**
     * sip registration
     */
    public void register(boolean useZrtp) {

        if (useZrtp) {
            // enable zrtp
            final String basePath = context.getFilesDir().getAbsolutePath();
            core.setZrtpSecretsFile(basePath + "/zrtp_secrets");

            core.setMediaEncryption(MediaEncryption.ZRTP);
            core.setMediaEncryptionMandatory(true);
        } else {
            core.setMediaEncryption(MediaEncryption.None);
            core.setMediaEncryptionMandatory(false);
        }

        String user = Settings.getRegisteredNumber();
        pass = EncryptUtils.decrypt(Settings.getToken());

        authInfo = Factory.instance().createAuthInfo(user, user, pass, null, "*", server);
        core.addAuthInfo(authInfo);

        core.clearProxyConfig();
        final ProxyConfig proxyCfg = core.createProxyConfig();
        proxyCfg.edit();
        proxyCfg.setIdentityAddress(Factory.instance().createAddress("sip:" + authInfo.getUsername() + "@" + server));
        proxyCfg.setServerAddr(server);
        proxyCfg.enableRegister(true);
        proxyCfg.setExpires(60); // connection times out after 1 minute. This overrides kamailio setting which is 3600 (1 hour).
        proxyCfg.enablePublish(false);
        proxyCfg.setPushNotificationAllowed(false);

        final NatPolicy natPolicy = core.createNatPolicy();
        natPolicy.setStunServer(stunServer);
        natPolicy.enableStun(true);
        natPolicy.enableIce(false);
        natPolicy.enableTurn(true);
        natPolicy.enableUpnp(false);
        core.setNatPolicy(natPolicy);
        proxyCfg.setNatPolicy(natPolicy);

        proxyCfg.done();
        core.addProxyConfig(proxyCfg);
        core.setDefaultProxyConfig(proxyCfg);

        startIterate();
    }


    /**
     * sip make call
     */
    public void makeCall(CallWrapper call) {
        call.setStatus(WingsConst.CALL_STATUS_REGISTERING);
        pendingCalls.add(call);
        register(call.getCallBuddy().getSipUri().contains("sip:sec"));
    }

    /**
     * Sip Make final Call, after registration
     **/
    private void makeFinalCall(CallWrapper wrapper) {

        BluetoothManager.getInstance().routeAudioToBluetooth();

        Call sipCAll = core.invite(wrapper.getCallBuddy().getSipUri());
        wrapper.setSipCall(sipCAll);
    }


    private void setCodecPrefs(String codecString) {
        //set codecs
        PayloadType[] codecs = core.getAudioPayloadTypes();
        for (PayloadType type : codecs) {
            type.enable(codecString.contains(type.getMimeType().toUpperCase()));
        }
    }

    public void createConference(Call call1, Call call2) {

        Address[] participants = new Address[]{
                call1.getRemoteAddress(),
                call2.getRemoteAddress()
        };

        ConferenceParams conferenceParams = core.createConferenceParams();
        conferenceParams.enableVideo(false);
        Conference conference = core.createConferenceWithParams(conferenceParams);
        CallParams params = core.createCallParams(call1);
        conference.inviteParticipants(participants, params);

    }

    public void removeConference(Call call1, Call call2) {
        core.getConference().removeParticipant(call1.getRemoteAddress());
        core.getConference().removeParticipant(call2.getRemoteAddress());
    }

}
