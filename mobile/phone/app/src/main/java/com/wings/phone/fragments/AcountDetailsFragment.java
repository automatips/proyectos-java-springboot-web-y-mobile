package com.wings.phone.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;
import com.wings.phone.R;
import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.NumberUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;
import com.wings.phone.util.UserUtil;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.DidNumberDTO;
import com.wings.voip.model.ServiceDTO;
import com.wings.voip.model.StoreServiceDTO;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AcountDetailsFragment extends Fragment {

    public final static String TAG = "AcountDetailsFragment";
    private View mine;

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_account_detail_fragment, null);
        mine = view;
        showStatus();
        showCart();
        return view;
    }

    /**
     *
     */
    @Override
    public void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateStatus();
            }
        }, 100);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateCart();
            }
        }, 200);

    }

    /**
     *
     */
    private void showStatus() {

        Double balance = Settings.getBalance(getContext());
        if (balance == null) {
            return;
        }

        String balanceFormatted = NumberUtils.formatMoney(balance);
        ((TextView) mine.findViewById(R.id.account_balance)).setText(balanceFormatted);

        //add payment link
        TextView paymentLink = mine.findViewById(R.id.account_payment_link);
        String paymentUrl = UserUtil.getBackofficeUrl();
        String paymentTitle = getString(R.string.account_add_payment);
        String paymentHtml = "<a href=\"" + paymentUrl + "\">" + paymentTitle + "</a>";

        Spanned spanned;
        spanned = Html.fromHtml(paymentHtml, Html.FROM_HTML_MODE_LEGACY);
        paymentLink.setMovementMethod(LinkMovementMethod.getInstance());
        paymentLink.setText(spanned);

        LinearLayout layout = mine.findViewById(R.id.account_services_mine);
        layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 20, 0, 8);

        List<ServiceDTO> myServices = Settings.getMyServices(getContext());
        for (final ServiceDTO service : myServices) {
            View container = inflater.inflate(R.layout.activity_account_detail_service, null);
            final TextView desc = container.findViewById(R.id.service_detail_description);
            final Button action = container.findViewById(R.id.service_detail_action);
            final ImageView icon = container.findViewById(R.id.service_detail_image);
            TextView serviceName = container.findViewById(R.id.service_detail_name);
            final ImageView expand = container.findViewById(R.id.service_detail_expand);
            TextView expire = container.findViewById(R.id.service_detail_expire);

            if (service.getIconUrl() != null && !service.getIconUrl().isEmpty()) {
                Picasso.get().load(service.getIconUrl()).transform(new CircleTransform()).into(icon, new com.squareup.picasso.Callback() {
                    //Picasso.with(getContext()).load(service.getIconUrl()).into(icon, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(Exception e) {
                        icon.setImageResource(R.drawable.service_default);
                    }
                });
            }

            serviceName.setText(service.getName());

            String expireDate;
            final boolean expires = !service.getExpireDate().equals("2101-01-31");
            if (expires) {
                expireDate = StringUtils.formatMysqlStringToLocaleString(service.getExpireDate());
            } else {
                expireDate = getString(R.string.never);
            }
            expire.setText(expireDate);

            String description = service.getDescription();
            desc.setText(Html.fromHtml(description));
            desc.setMovementMethod(LinkMovementMethod.getInstance());

            desc.setVisibility(View.GONE);
            action.setVisibility(View.GONE);

            //expand
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (desc.getVisibility() == View.VISIBLE) {
                        desc.setVisibility(View.GONE);
                        expand.setImageResource(R.drawable.baseline_arrow_downward_24);
                        action.setVisibility(View.GONE);
                    } else {
                        desc.setVisibility(View.VISIBLE);
                        expand.setImageResource(R.drawable.baseline_arrow_upward_24);
                        if (expires && !service.getType().equals("gift")) {
                            action.setVisibility(View.VISIBLE);
                        }
                    }
                }
            };
            icon.setOnClickListener(listener);
            serviceName.setOnClickListener(listener);
            expand.setOnClickListener(listener);

            final String url = paymentUrl + "?didnumber=true";
            //action button
            if (service.getCode() != null && service.getCode().equals("didnumber")) {

                if (service.getDids() != null) {

                    for (DidNumberDTO did : service.getDids()) {
                        if (did.getStatus() != null) {

                            if (did.getStatus().equals("created")) {
                                //action setup
                                action.setText(R.string.setup);

                            } else if (did.getStatus().equals("setup")) {
                                //action renew
                                action.setText(R.string.renew);
                            }
                            action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);
                                }
                            });
                        }
                    }
                }

            } else {

                if (expires) {
                    action.setText(R.string.renew);
                    action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            purchase(service.getServiceId(), true);
                        }
                    });
                }
            }

            container.setLayoutParams(params);

            layout.addView(container);
        }

    }

    /**
     *
     */
    private void updateStatus() {

        String apiUrl = getString(R.string.settings_api_url);
        String appId = getString(R.string.settings_api_app_id);
        String appToken = getString(R.string.settings_api_app_token);

        Call<AccountStatus> call = ApiClientFactory.getClient(apiUrl).getAccountStatus(appId, appToken, Settings.getRegisteredNumber(), Locale.getDefault().toString());
        try {
            call.enqueue(new Callback<AccountStatus>() {
                @Override
                public void onResponse(Call<AccountStatus> call, Response<AccountStatus> response) {

                    try {
                        if (response.body() != null) {
                            AccountStatus status = response.body();
                            Settings.updateStatus(status, getContext());
                        }
                    } catch (Exception e) {
                        Timber.e(e);
                    }
                    showStatus();
                }

                @Override
                public void onFailure(Call<AccountStatus> call, Throwable t) {
                    Timber.e(t, "Error on status update");
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error on status update");
        }
    }

    /**
     *
     */
    private void showCart() {

        if (getContext() == null) {
            return;
        }

        LinearLayout layout = mine.findViewById(R.id.account_services_buy);
        layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 0, 0, 8);

        List<StoreServiceDTO> storeServices = Settings.getStoreServices(getContext());
        if (storeServices == null) {
            return;
        }

        for (final StoreServiceDTO service : storeServices) {
            View container = inflater.inflate(R.layout.activity_account_store_service, null);

            final TextView desc = container.findViewById(R.id.store_service_description);
            final Button action = container.findViewById(R.id.store_service_action);
            final ImageView icon = container.findViewById(R.id.store_service_image);
            TextView serviceName = container.findViewById(R.id.store_service_name);
            TextView servicePrice = container.findViewById(R.id.store_service_price);
            final ImageView expand = container.findViewById(R.id.store_service_expand);

            if (service.getIconUrl() != null && !service.getIconUrl().isEmpty()) {
                Picasso.get().load(service.getIconUrl()).into(icon, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        icon.setImageResource(R.drawable.service_default);
                    }
                });
            }

            serviceName.setText(service.getName());
            desc.setText(Html.fromHtml(service.getDescription()));
            desc.setMovementMethod(LinkMovementMethod.getInstance());
            servicePrice.setText(NumberUtils.formatMoney(service.getCost()));

            desc.setVisibility(View.GONE);
            action.setVisibility(View.GONE);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (desc.getVisibility() == View.VISIBLE) {
                        desc.setVisibility(View.GONE);
                        action.setVisibility(View.GONE);
                        expand.setImageResource(R.drawable.baseline_arrow_downward_24);
                    } else {
                        desc.setVisibility(View.VISIBLE);
                        action.setVisibility(View.VISIBLE);
                        expand.setImageResource(R.drawable.baseline_arrow_upward_24);
                    }
                }
            };

            icon.setOnClickListener(listener);
            serviceName.setOnClickListener(listener);
            expand.setOnClickListener(listener);

            action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean extendSecure = false;
                    boolean extendDid = false;

                    List<ServiceDTO> myServices = Settings.getMyServices(getContext());
                    for (ServiceDTO myService : myServices) {

                        if (myService.getServiceId().equals(service.getServiceId()) && service.getCode().equals("secure")) {
                            extendSecure = true;
                            break;
                        }

                        if (myService.getServiceId().equals(service.getServiceId()) && service.getCode().equals("didnumber")) {
                            extendDid = true;
                            break;
                        }
                    }

                    if (extendSecure) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.confirm_buy)
                                .setMessage(R.string.confirm_extend_desc)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(R.string.extend, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        purchase(service.getServiceId(), true);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null)
                                .show();

                    } else if (extendDid) {

                        //TODO: si es didnumber y tiene mas de 1 nro, mostrar
                        //seleccion de numero a extender
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.confirm_buy)
                                .setMessage(R.string.confirm_extend_desc)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(R.string.extend, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        purchase(service.getServiceId(), true);
                                    }
                                })
                                .setNeutralButton(R.string.buy_new_one, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        purchase(service.getServiceId(), false);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null)
                                .show();

                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.confirm_buy)
                                .setMessage(R.string.confirm_buy_desc)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        purchase(service.getServiceId(), false);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null)
                                .show();
                    }
                }
            });

            container.setLayoutParams(params);
            layout.addView(container);
        }
    }

    /**
     *
     */
    private void updateCart() {

        String apiUrl = getString(R.string.settings_api_url);
        String appId = getString(R.string.settings_api_app_id);
        String appToken = getString(R.string.settings_api_app_token);

        Call<List<StoreServiceDTO>> call = ApiClientFactory.getClient(apiUrl).getStoreServices(appId, appToken, Settings.getRegisteredNumber(), Locale.getDefault().toString());
        try {
            call.enqueue(new Callback<List<StoreServiceDTO>>() {
                @Override
                public void onResponse(Call<List<StoreServiceDTO>> call, Response<List<StoreServiceDTO>> response) {

                    if (response.body() != null) {
                        List<StoreServiceDTO> storeServices = response.body();
                        Settings.setStoreServices(getContext(), storeServices);
                    }
                    showCart();
                }

                @Override
                public void onFailure(Call<List<StoreServiceDTO>> call, Throwable t) {
                    Timber.e(t, "Error on status update");
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error on status update");
        }
    }

    /**
     *
     * @param serviceId
     * @param extend
     */
    private void purchase(Long serviceId, boolean extend) {

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setMessage(getString(R.string.wait_please));
        pd.show();

        String apiUrl = getString(R.string.settings_api_url);
        String appId = getString(R.string.settings_api_app_id);
        String appToken = getString(R.string.settings_api_app_token);

        Call<AccountStatus> call = ApiClientFactory.getClient(apiUrl)
                .postStoreServiceBuy(appId, appToken, Settings.getRegisteredNumber(), Settings.getToken(), serviceId, extend);

        try {
            call.enqueue(new Callback<AccountStatus>() {
                @Override
                public void onResponse(Call<AccountStatus> call, Response<AccountStatus> response) {
                    pd.cancel();
                    if (response.body() != null) {
                        AccountStatus status = response.body();
                        Settings.updateStatus(status, getContext());
                        showStatus();
                    } else {

                        String message = getString(R.string.error_generic);
                        if (response.headers() != null && response.headers().get("error") != null) {
                            try {
                                int id = getResources().getIdentifier("com.wings.phone:string/" + response.headers().get("error"), null, null);
                                message = getString(id);
                            } catch (Exception e) {
                            }
                        }

                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AccountStatus> call, Throwable t) {
                    pd.cancel();
                    Timber.e(t, "Error on status update");
                    String message = getString(R.string.error_generic);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            pd.cancel();
            Timber.e(e, "Error on status update");
            String message = getString(R.string.error_generic);
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }

    }
}
