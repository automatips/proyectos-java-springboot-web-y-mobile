package com.wings.phone.data;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;

import com.wings.phone.telecom.CallBuddy;
import com.wings.phone.util.Settings;

import timber.log.Timber;

public class DataProvider {


    private static final String[] simpleContactProjection = new String[]{
            ContactsContract.PhoneLookup._ID,
            ContactsContract.PhoneLookup.DISPLAY_NAME,
            ContactsContract.PhoneLookup.TYPE,
            ContactsContract.PhoneLookup.NUMBER,
            ContactsContract.PhoneLookup.NORMALIZED_NUMBER,
            ContactsContract.PhoneLookup.PHOTO_URI,
            ContactsContract.CommonDataKinds.Phone.DATA3,
            ContactsContract.Data.RAW_CONTACT_ID

    };



    /**
     * Returns contact matching phone number, with only that number
     * If the contact has more than one will not be included, returns
     * unknown or number if does not exists
     *
     * @param context
     * @param number
     * @return
     */
    public static CallBuddy getCallBuddy(Context context, String number) {

        CallBuddy ret = new CallBuddy();
        try {

            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            Cursor cur;
            cur = context.getContentResolver().query(uri, simpleContactProjection, null, null, null);
                //cur = context.getContentResolver().query(uri, simpleContactProjection, null, null, null);


            if (cur.getCount() == 0) {

                String name = number;
                if (name == null || name.isEmpty()) {
                    name = context.getString(android.R.string.unknownName);
                    number = name;
                }

                ret.setName(name);
                ret.setNumber(number);
                ret.setFormattedNumber(PhoneNumberUtils.formatNumber(number, Settings.getCountry()));


            } else {
                cur.moveToPosition(-1);
                cur.moveToNext();

                ret.setId(cur.getString(0));
                ret.setName(cur.getString(1));
                ret.setPhotoUri(cur.getString(5));

                int type = cur.getInt(2);
                String label;
                label = cur.getString(6);
                ret.setNumberLabel(label);

                String fnro = cur.getString(3);
                String nro = cur.getString(4);
                if (nro == null) {
                    nro = PhoneNumberUtils.formatNumber(number, Settings.getCountry());
                }
                if (nro == null) {
                    nro = number;
                }

                ret.setNumber(nro);
                ret.setFormattedNumber(fnro);

            }

            cur.close();

        } catch (Exception e) {
            Timber.e(e);
        }

        return ret;
    }

    /**
     * Returns contact matching phone number, with only that number
     * If the contact has more than one will not be included, returns
     * unknown or number if does not exists
     *
     * @param context
     * @param data
     * @return
     */
    public static CallBuddy getCallBuddy(Context context, Uri data) {

        CallBuddy ret = new CallBuddy();
        try {

            //Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, data);
            Cursor cur;
            cur = context.getContentResolver().query(data, null, null, null, null);


            DatabaseUtils.dumpCursor(cur);

            cur.moveToPosition(-1);
            cur.moveToNext();

            ret.setId(cur.getString(cur.getColumnIndex("contact_id")));
            ret.setName(cur.getString(cur.getColumnIndex("display_name")));
            ret.setPhotoUri(cur.getString(cur.getColumnIndex("photo_uri")));

            int type = cur.getInt(2);
            String label;
            label = cur.getString(6);
            ret.setNumberLabel(label);

            String number = cur.getString(cur.getColumnIndex("data2"));
            String fnro = number;
            if (number == null) {
                fnro = PhoneNumberUtils.formatNumber(number, Settings.getCountry());
            }
            ret.setMimeFromContact(cur.getString(cur.getColumnIndex("data1")));
            ret.setNumber(number);
            ret.setFormattedNumber(fnro);

            cur.close();

        } catch (Exception e) {
            Timber.e(e);
        }

        return ret;

    }
}
