package com.wings.phone.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.wings.phone.MissedCallActivity;
import com.wings.phone.R;
import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.util.InternetCheck;
import com.wings.phone.util.NetworkUtils;
import com.wings.phone.util.NumberUtils;
import com.wings.phone.util.Settings;
import com.wings.voip.model.GenericResponse;
import java.util.HashMap;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AcountRegisterFragment extends Fragment {

    public final static String TAG = "AcountRegisterFragment";

    private TextView error;
    private TextView prefix;
    private TextView number;
    private ProgressBar progressBar;
    private String myNumber;
    private String myValidNumber;
    private String mcc;
    private String countryCode;

    private String apiUrl;
    private String appId;
    private String token;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_account_register_fragment, null);

        prefix = view.findViewById(R.id.account_register_prefix);
        number = view.findViewById(R.id.account_register_number);
        error = view.findViewById(R.id.account_register_error);
        progressBar = view.findViewById(R.id.account_register_progress);

        apiUrl = getString(R.string.settings_api_url);
        appId = getString(R.string.settings_api_app_id);
        token = getString(R.string.settings_api_app_token);

        //get real country
        String[] countries = getResources().getStringArray(R.array.DialingCountryCode);
        Map<String, String> cmap = new HashMap<String, String>();
        for (String country : countries) {
            cmap.put(country.split(",")[0], country.split(",")[1]);
        }

        prefix.setText(NumberUtils.getCountryDialCode(getContext()));
        number.requestFocus();

        view.findViewById(R.id.account_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error.setText("");

                countryCode = cmap.get(prefix.getText().toString());
                if (countryCode == null) {
                    error.setText(getText(R.string.account_register_error_number));
                    return;
                }
                Settings.setCountry(countryCode);

                myNumber = prefix.getText().toString() + number.getText().toString();
                myNumber = myNumber.replaceAll("\\+", "").replaceAll(" ", "");

                myValidNumber = PhoneNumberUtils.formatNumberToE164(myNumber, Settings.getCountry());

                checkInternet(myNumber, (myValidNumber == null));

            }
        });
        return view;
    }

    /**
     *
     * @param number
     * @param verify
     */
    public void checkInternet(final String number, final boolean verify) {
        NetworkUtils.hasInternet(new InternetCheck.Consumer() {
            @Override
            public void accept(Boolean internet) {
                if (internet) {
                    if (verify) {
                        validateNumberRemote(number);
                    } else {
                        showTermsDialog(number);
                    }
                } else {
                    error.setText(getString(R.string.no_internet_desc));
                }
            }
        });
    }

    /**
     *
     * @param number
     */
    private void validateNumberRemote(String number) {
        try {

            progressBar.setVisibility(View.VISIBLE);

            Call<GenericResponse> call = ApiClientFactory.getClient(apiUrl).verifyNumberValid(appId, token, number, Settings.getCountry());
            call.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {

                    progressBar.setVisibility(View.GONE);

                    GenericResponse body = response.body();
                    if (body != null && body.getCode().equals("200")) {
                        showTermsDialog(myNumber);
                    } else {
                        error.setText(getText(R.string.account_register_error_number));
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    Timber.e(t, "Error on validate number remote");
                    progressBar.setVisibility(View.GONE);
                    error.setText(getText(R.string.account_register_error_number));
                }
            });
        } catch (Exception ex) {
            Timber.e(ex, "Error on validate number remote");
            progressBar.setVisibility(View.GONE);
            error.setText(getText(R.string.account_register_error_number));
        }
    }

    /**
     *
     * @param myNumber
     */
    private void showTermsDialog(final String myNumber) {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.privacy_policy_title))
                .setView(R.layout.dialog_terms_layout)
                .setPositiveButton(getString(R.string.Continue), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        validate(myNumber);
                        //showSuccess();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null)
                .show();
    }

    /**
     *
     * @param number
     */
    public void validate(String number) {

        if (NetworkUtils.getConnectivityStatus(getContext()) == NetworkUtils.NETWORK_STATUS_NOT_CONNECTED) {
            error.setText(getString(R.string.no_internet_desc));
            return;
        }

        Intent i = new Intent(getContext(), MissedCallActivity.class);
        i.putExtra("myNumber", number);
        startActivityForResult(i, 154);

        error.setText("");
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 154) {
            showFinalStatus();
        } else {
            error.setText(getString(R.string.account_register_error));
        }
    }

    /**
     *
     */
    private void showFinalStatus() {
        Intent intent = getActivity().getIntent();
        getActivity().finish();
        startActivity(intent);
    }

}
