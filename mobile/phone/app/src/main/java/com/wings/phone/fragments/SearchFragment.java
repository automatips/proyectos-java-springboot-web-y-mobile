package com.wings.phone.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.squareup.picasso.Picasso;
import com.wings.phone.CallHistoryActivity;
import com.wings.phone.MainActivity;
import com.wings.phone.R;
import com.wings.phone.data.sync.ContactsManager;
import com.wings.phone.model.DataRepository;
import com.wings.phone.util.CircleTransform;
import com.wings.voip.model.contact.Contact;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by seba on 26/04/17.
 */

public class SearchFragment extends Fragment {

    public static final String TAG = "SearchFragment";

    private FloatingSearchView mSearchView;
    private DrawerLayout mDrawerLayout;
    private DataRepository dataRepository;
    private List<Contact> contacts;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(dataRepository==null){
            dataRepository = new DataRepository(getContext());
        }

        View view = inflater.inflate(R.layout.fragment_search, null);
        final Context context = getActivity().getApplicationContext();

        mSearchView = view.findViewById(R.id.floating_search_view);
        mSearchView.setOnBindSuggestionCallback(new SearchSuggestionsAdapter.OnBindSuggestionCallback() {
            @Override
            public void onBindSuggestion(final View suggestionView, final ImageView leftIcon, final TextView textView, final SearchSuggestion item, final int itemPosition) {


                String arr[] = item.getBody().split(",");
                textView.setText(arr[0]);
                final String photo = arr[1];
                final String id = arr[2];
                final String rawid = arr[3];

                if (photo != null && !photo.isEmpty() && !photo.equals("null")) {
                    Uri uri = Uri.parse(photo);
                    leftIcon.setColorFilter(null);
                    Picasso.get().load(uri).transform(new CircleTransform()).into(leftIcon);
                } else {
                    leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.baseline_person_black_24, null));
                    leftIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.dark_gray), android.graphics.PorterDuff.Mode.SRC_IN);
                }

                View.OnClickListener cl = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Contact contact = contacts.stream().filter(c -> (c.getId().equals(Integer.parseInt(id))) || c.getRawId().equals(Integer.parseInt(rawid))).collect(toList()).get(0);
                        ((MainActivity) getActivity()).showContactFiltered(contact);

                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                        mSearchView.clearQuery();
                        mSearchView.clearSearchFocus();
                        mSearchView.closeMenu(false);
                    }
                };

                suggestionView.setOnClickListener(cl);

            }
        });

        final List<SearchSuggestion> list = new ArrayList<>();

        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                contacts = dataRepository.getContacts();
                list.clear();
                for (Contact c: contacts) {
                    if(c.getName().toLowerCase().contains(newQuery.toLowerCase())){
                        SearchSuggestion ss = new SearchSuggestion() {
                            @Override
                            public String getBody() {
                                return c.getName() + "," + c.getPhotoUri() + "," + c.getId() + "," + c.getRawId();
                            }
                            @Override
                            public int describeContents() {
                                return 0;
                            }
                            @Override
                            public void writeToParcel(Parcel dest, int flags) {
                            }
                        };

                        list.add(ss);
                    }
                }
                mSearchView.swapSuggestions(list);
            }
        });


        //search bar action items
        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.action_sync) {
                    ContactsManager.requestSync(getContext());
                } else if (item.getItemId() == R.id.action_open) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, ContactsContract.Contacts.CONTENT_URI);
                    startActivity(intent);
                }else if(item.getItemId() == R.id.action_delete_call_log){
                    Intent intent = new Intent(getContext(), CallHistoryActivity.class);
                    startActivity(intent);
                }
            }
        });

        if (mDrawerLayout != null) {
            mSearchView.attachNavigationDrawerToMenuButton(mDrawerLayout);
        }
        return view;
    }
    public void setDrawerLayout(DrawerLayout mDrawerLayout) {
        this.mDrawerLayout = mDrawerLayout;
    }
}
