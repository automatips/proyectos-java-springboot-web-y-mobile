package com.wings.phone.fragments.tabs;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.wings.phone.R;
import com.wings.phone.adapters.ContactsAdapter;
import com.wings.phone.viewmodels.ContactsViewModel;
import com.wings.phone.viewmodels.FavoritesViewModel;
import com.wings.voip.model.contact.Contact;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


/**
 * Created by seba on 26/04/17.
 */

public class ContactsFragment extends Fragment {

    public static final String TAG = "ContactsFragment";
    private boolean isViewShown = false;
    private ContactsViewModel contactsViewModel;

    private View view;


    private ContactsAdapter contactsAdapter;
    private Context context;
    private final String[] GROUP_PROJECTION = new String[]{
            ContactsContract.PhoneLookup._ID,
            ContactsContract.PhoneLookup.DISPLAY_NAME,
            ContactsContract.PhoneLookup.PHOTO_URI,
            ContactsContract.Contacts.HAS_PHONE_NUMBER,
            ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID
    };
    ////private final String SELECTION = "(" + ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '1' AND (" + ContactsContract.Contacts.HAS_PHONE_NUMBER + " != 0 ))";
    private final String SELECTION ="(" + ContactsContract.Contacts.HAS_PHONE_NUMBER + " != ? )";

    private StickyListHeadersListView listV;
    private Integer transferFromId;
    private ArrayList<Contact> contactsArr;
    private String filterString;

    public static Fragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        return fragment;
    }

    public ContactsFragment(){

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(contactsArr==null){
            contactsArr = new ArrayList<>();
        }
        if(contactsAdapter==null){
            contactsAdapter = new ContactsAdapter(context, R.layout.view_contact_item, true, filterString);
        }
        if(contactsViewModel==null){
            contactsViewModel = new ViewModelProvider(this, new ViewModelFactory(getContext())).get(ContactsViewModel.class);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tab_contacts, null);
        listV = (StickyListHeadersListView) view.findViewById(R.id.list_contacts);
        listV.setFastScrollEnabled(true);
        filterString = "";
        //loadFragment();
        contactsViewModel.getContactList().observe(getViewLifecycleOwner(), new Observer<List<Contact>>() {
            @Override
            public void onChanged(List<Contact> contacts) {
                contactsAdapter.setContactsArr(contacts);
                contactsAdapter.notifyDataSetChanged();

            }
        });

        listV.setAdapter(contactsAdapter);

        return view;
    }


    public void setTransferFromId(Integer transferFromId) {
        this.transferFromId = transferFromId;
    }

    public void setFilterString(String f){
        this.filterString = f;
        contactsAdapter.getFilter().filter(f);
    }



    static class ViewModelFactory implements ViewModelProvider.Factory {
        private Application application;
        ViewModelFactory(Context context) {
            application = (Application) context.getApplicationContext();
        }
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull Class<T> modelClass) {
            return (T) new ContactsViewModel(application);
        }
    }


}
