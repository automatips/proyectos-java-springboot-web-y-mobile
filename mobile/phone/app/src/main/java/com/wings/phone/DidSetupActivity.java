package com.wings.phone;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.util.Settings;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.DidCityDTO;
import com.wings.voip.model.DidCountryDTO;
import com.wings.voip.model.DidNumberAvailableDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DidSetupActivity extends AppCompatActivity {


    private ProgressDialog dialog;
    private Spinner countriesSpinner;
    private Spinner citiesSpinner;
    private Spinner numbersSpinner;
    private Button buttonSearch;
    private Button buttonDone;

    private String apiUrl;
    private String appId;
    private String appToken;
    private String myDidId;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        apiUrl = getString(R.string.settings_api_url);
        appId = getString(R.string.settings_api_app_id);
        appToken = getString(R.string.settings_api_app_token);

        setContentView(R.layout.activity_second_numbers_setup);
        getSupportActionBar().setTitle(getString(R.string.second_numbers_setup));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dialog = new ProgressDialog(DidSetupActivity.this);
        dialog.setTitle(R.string.wait_please);

        countriesSpinner = findViewById(R.id.second_number_setup_country);
        citiesSpinner = findViewById(R.id.second_number_setup_city);
        numbersSpinner = findViewById(R.id.second_number_setup_number);

        buttonSearch = findViewById(R.id.second_number_setup_search);
        buttonDone = findViewById(R.id.second_number_setup_done);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmBuy();
            }
        });

        myDidId = getIntent().getAction();
    }


    @Override
    protected void onResume() {
        super.onResume();
        dialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getCountries();
            }
        }, 500);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void getCountries() {

        Call<List<DidCountryDTO>> call = ApiClientFactory.getClient(apiUrl).getDidCountries(appId, appToken, Settings.getRegisteredNumber(), Locale.getDefault().toString());
        try {
            call.enqueue(new Callback<List<DidCountryDTO>>() {
                @Override
                public void onResponse(Call<List<DidCountryDTO>> call, Response<List<DidCountryDTO>> response) {
                    if (response.body() != null) {
                        fillCountries(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<DidCountryDTO>> call, Throwable t) {
                    Timber.e(t, "Error on status update");
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error on status update");
        }

    }

    private void fillCountries(List<DidCountryDTO> countries) {

        final ArrayAdapter<DidCountryDTO> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countries);
        countriesSpinner.setAdapter(spinnerArrayAdapter);
        countriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DidCountryDTO selected = spinnerArrayAdapter.getItem(position);
                if (selected.getId() != -1) {
                    fillCities(selected.getId());
                    dialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        dialog.cancel();
    }

    private void fillCities(long countryId) {

        Call<List<DidCityDTO>> call = ApiClientFactory.getClient(apiUrl).getDidCities(appId, appToken, Settings.getRegisteredNumber(), Locale.getDefault().toString(), countryId);
        try {
            call.enqueue(new Callback<List<DidCityDTO>>() {
                @Override
                public void onResponse(Call<List<DidCityDTO>> call, Response<List<DidCityDTO>> response) {
                    if (response.body() != null) {
                        fillCities(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<DidCityDTO>> call, Throwable t) {
                    Timber.e(t, "Error on status update");
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error on status update");
        }
    }

    private void fillCities(List<DidCityDTO> cities) {

        final ArrayAdapter<DidCityDTO> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cities);
        citiesSpinner.setAdapter(spinnerArrayAdapter);
        dialog.cancel();

    }

    private void search() {

        dialog.show();
        DidCountryDTO selectedCountry = (DidCountryDTO) countriesSpinner.getSelectedItem();
        DidCityDTO selectedCity = (DidCityDTO) citiesSpinner.getSelectedItem();

        Long countryId = -1l;
        if (selectedCountry != null) {
            countryId = selectedCountry.getId();
        }

        Long cityId = -1l;
        if (selectedCity != null) {
            cityId = selectedCity.getId();
        }

        Call<List<DidNumberAvailableDTO>> call = ApiClientFactory.getClient(apiUrl).getDidNumbersAvailable(appId, appToken, Settings.getRegisteredNumber(), Locale.getDefault().toString(), countryId, cityId);
        try {
            call.enqueue(new Callback<List<DidNumberAvailableDTO>>() {
                @Override
                public void onResponse(Call<List<DidNumberAvailableDTO>> call, Response<List<DidNumberAvailableDTO>> response) {
                    if (response.body() != null) {
                        fillNumber(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<DidNumberAvailableDTO>> call, Throwable t) {
                    Timber.e(t, "Error on status update");
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error on status update");
        }
    }


    private void fillNumber(List<DidNumberAvailableDTO> availableNumbers) {

        final ArrayAdapter<DidNumberAvailableDTO> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, availableNumbers);
        numbersSpinner.setAdapter(spinnerArrayAdapter);

        dialog.cancel();
    }

    private void confirmBuy() {

        final DidNumberAvailableDTO selectedDid = (DidNumberAvailableDTO) numbersSpinner.getSelectedItem();
        if (selectedDid != null && !selectedDid.getRemoteId().equals("-1")) {

            String[] args = new String[]{""};

            String message = getString(R.string.confirm_did_buy, selectedDid.getNumber());
            new AlertDialog.Builder(DidSetupActivity.this)
                    .setTitle(R.string.confirm_buy)
                    .setMessage(message)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(R.string.extend, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            buyNumber(selectedDid);
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();
        } else {
            //debe elejir un numero
        }
    }


    private void buyNumber(DidNumberAvailableDTO selectedDid) {
        dialog.show();
        Call<AccountStatus> call = ApiClientFactory.getClient(apiUrl).postDidNumberBuy(appId, appToken, Settings.getRegisteredNumber(), Settings.getToken(),myDidId, selectedDid.getNumber(), selectedDid.getRemoteId());
        try {
            call.enqueue(new Callback<AccountStatus>() {
                @Override
                public void onResponse(Call<AccountStatus> call, Response<AccountStatus> response) {

                    dialog.cancel();

                    if (response.body() != null) {
                        Settings.updateStatus(response.body(), DidSetupActivity.this);
                        finish();
                    } else {
                        String message = getString(R.string.error_generic);
                        if (response.headers() != null && response.headers().get("error") != null) {
                            try {
                                int id = getResources().getIdentifier("com.wings.phone:string/" + response.headers().get("error"), null, null);
                                message = getString(id);
                            } catch (Exception e) {
                            }
                        }
                        Toast.makeText(DidSetupActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AccountStatus> call, Throwable t) {
                    dialog.cancel();
                    String message = getString(R.string.error_generic);
                    Toast.makeText(DidSetupActivity.this, message, Toast.LENGTH_LONG).show();
                    Timber.e(t, "Error on status update");
                }
            });
        } catch (Exception e) {
            dialog.cancel();
            String message = getString(R.string.error_generic);
            Toast.makeText(DidSetupActivity.this, message, Toast.LENGTH_LONG).show();
            Timber.e(e, "Error on status update");
        }
    }
}
