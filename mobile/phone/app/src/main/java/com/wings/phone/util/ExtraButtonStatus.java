package com.wings.phone.util;

public class ExtraButtonStatus {

    private int status;
    private boolean visible;
    private String number;

    public int getStatus() {
        return status;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getNumber() {
        return number;
    }

    public boolean isVisible() {
        return visible;
    }
}
