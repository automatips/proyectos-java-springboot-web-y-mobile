package com.wings.phone.fragments.tabs;


import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.wings.phone.MainActivity;
import com.wings.phone.R;
import com.wings.phone.adapters.FavoritesAdapter;
import com.wings.phone.model.DataRepository;
import com.wings.phone.viewmodels.FavoritesViewModel;
import com.wings.voip.model.contact.Contact;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by seba on 26/04/17.
 */

public class FavoritesFragment extends Fragment {

    public static final String TAG = "FavoritesFragment";
    private FavoritesViewModel favoritesViewModel;
    private boolean isViewShown = false;

    private GridView grid;
    private View view;
    private FavoritesAdapter favoritesAdapter;
    private List<Contact> contacts;


    public FavoritesFragment(){

    }


    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        //Bundle args = new Bundle();
        //args.putInt(int, counter);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(contacts==null){
            contacts=new LinkedList<>();
        }
        favoritesAdapter = new FavoritesAdapter(getContext(), contacts, (MainActivity) getActivity());
        favoritesViewModel = new ViewModelProvider(this, new FavoritesViewModelFactory(getContext())).get(FavoritesViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = (LinearLayout) inflater.inflate(R.layout.fragment_tab_favorites, null);
        grid = (GridView) view.findViewById(R.id.list_favorites);
        grid.setNumColumns(2);
        grid.setFastScrollEnabled(true);
        view.findViewById(R.id.add_favorites).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContacts(v);
            }
        });

        favoritesViewModel.getFavContactList().observe(getViewLifecycleOwner(), new Observer<List<Contact>>() {
            @Override
            public void onChanged(List<Contact> contacts) {
                favoritesAdapter.setContactList(contacts);
                favoritesAdapter.notifyDataSetChanged();

                if (contacts.size() == 0) {
                    view.findViewById(R.id.fav_no_favs).setVisibility(View.VISIBLE);
                    grid.setVisibility(View.GONE);
                }else{
                    view.findViewById(R.id.fav_no_favs).setVisibility(View.GONE);
                    grid.setVisibility(View.VISIBLE);
                }
            }

        });
        grid.setAdapter(favoritesAdapter);


        return view;

    }

    @Override
    public void onResume() {
        refreshUI();
        super.onResume();

    }

    public void openContacts(View view){
        Intent intent = new Intent(Intent.ACTION_DEFAULT, ContactsContract.Contacts.CONTENT_URI);
        getActivity().startActivity(intent);
    }

    public void refreshUI() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                favoritesViewModel.refresh();
            }
        }).run();
    }

    static class FavoritesViewModelFactory implements ViewModelProvider.Factory {
        private Application application;
        FavoritesViewModelFactory(Context context) {
            application = (Application) context.getApplicationContext();
        }
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull Class<T> modelClass) {
            return (T) new FavoritesViewModel(application);
        }
    }




}
