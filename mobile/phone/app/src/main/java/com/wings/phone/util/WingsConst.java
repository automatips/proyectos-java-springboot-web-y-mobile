package com.wings.phone.util;

import android.Manifest;
import android.content.Context;

/**
 * Created by seba on 10/10/17.
 */

public class WingsConst {

    public static final String[] permissions = new String[]
            {
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.MODIFY_AUDIO_SETTINGS,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.INTERNET,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.DISABLE_KEYGUARD,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.GET_ACCOUNTS,
                    Manifest.permission.PROCESS_OUTGOING_CALLS, //deprecated
                    Manifest.permission.MANAGE_OWN_CALLS, //nuevo
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.WRITE_CALL_LOG,
                    Manifest.permission.WAKE_LOCK,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.WRITE_CONTACTS,
                    Manifest.permission.WRITE_SYNC_SETTINGS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.ACCESS_NOTIFICATION_POLICY

            };


    public static final String[] permissionsPrivileged = new String[]
            {
                    Manifest.permission.CALL_PRIVILEGED,
                    Manifest.permission.BIND_INCALL_SERVICE,
                    Manifest.permission.MODIFY_PHONE_STATE,
            };


    public static final int CALL_TYPE_GSM = 1;
    public static final int CALL_TYPE_FREE = 2;
    public static final int CALL_TYPE_WINGS = 3;
    public static final int CALL_TYPE_SECURE = 4;
    public static final int CALL_TYPE_VIDEO = 5;

    public static final int CALL_DIRECTION_INCOMING = 1;
    public static final int CALL_DIRECTION_OUTGOING = 2;

    public static final String CALL_STATUS_INIT = "INIT";
    public static final String CALL_STATUS_ACTIVE = "ACTIVE";
    public static final String CALL_STATUS_REGISTERED = "REGISTERED";
    public static final String CALL_STATUS_REGISTERING = "REGISTERING";
    public static final String CALL_STATUS_REGISTRATION_FAILED = "REGISTRATION_ERROR";
    public static final String CALL_STATUS_CONNECTING = "CONNECTING";
    public static final String CALL_STATUS_DISCONNECTED = "DISCONNECTED";
    public static final String CALL_STATUS_DISCONNECTING = "DISCONNECTING";
    public static final String CALL_STATUS_HOLDING = "HOLDING";
    public static final String CALL_STATUS_DIALING = "DIALING";
    public static final String CALL_STATUS_NEW = "NEW";
    public static final String CALL_STATUS_PULLING_CALL = "PULLING_CALL";
    public static final String CALL_STATUS_RINGING = "RINGING";
    public static final String CALL_STATUS_SELECT_PHONE_ACCOUNT = "SELECT_PHONE_ACCOUNT";
    public static final String CALL_STATUS_TRANSFER_REQUEST = "TRANSFER_REQUEST";
    public static final String CALL_STATUS_TRANSFERRING = "TRANSFERRING";
    public static final String CALL_STATUS_TRANSFER_DONE = "TRANSFER_DONE";
    public static final String CALL_STATUS_TRANSFER_ERROR = "TRANSFER_ERROR";

    public static final String INTENT_WINGS_WINGS_CALL = "com.wings.phone.WINGS_CALL";
    public static final String INTENT_WINGS_FREE_CALL = "com.wings.phone.FREE_CALL";
    public static final String INTENT_WINGS_VIDEO_CALL = "com.wings.phone.VIDEO_CALL";
    public static final String INTENT_WINGS_SECURE_CALL = "com.wings.phone.SECURE_CALL";
    public static final String INTENT_WINGS_INCOMING = "com.wings.phone.INCOMING_CALL2";
    public static final String INTENT_WINGS_GSM_CALL_ADDED = "com.wings.phone.GSM_CALL_ADDED";
    public static final String INTENT_WINGS_TRANSFER_CALL = "com.wings.phone.TRANSFER_CALL";

    public static final String INTENT_WINGS_GSM_CALL_OUTGOING = "com.wings.phone.GSM_CALL_OUTGOING";
    public static final String INTENT_WINGS_GET_SIP_URI = "com.wings.phone.GET_SIP_URI";

    public static final String ACCOUNT_TYPE = "com.wings.profile.account";
    public static final String ACCOUNT_AUTHORITY = "com.wings.profile";
    public static final String ACCOUNT_NAME_SHORT = "Wings";

    public static final String MIMETYPE_WINGS = "vnd.android.cursor.item/vnd.com.wings.profile.wings";
    public static final String MIMETYPE_FREE = "vnd.android.cursor.item/vnd.com.wings.profile.free";
    public static final String MIMETYPE_SECURE = "vnd.android.cursor.item/vnd.com.wings.profile.secure";


    public static final String ACCOUNT_FREE = "ACCOUNT_FREE";
    public static final String ACCOUNT_WINGS = "ACCOUNT_WINGS";
    public static final String ACCOUNT_SECURE = "ACCOUNT_SECURE";
    public static final String ACCOUNT_VIDEO = "ACCOUNT_VIDEO";


    public static int getStringIdentifier(Context context, String name) {
        if (name == null || name.isEmpty()) {
            return 0;
        }
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }
}
