package com.wings.phone.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.wings.phone.model.DataRepository;
import com.wings.voip.model.contact.Contact;

import java.util.List;

public class FavoritesViewModel extends AndroidViewModel {


    private MutableLiveData<List<Contact>> contacts;
    private DataRepository repository;


    public FavoritesViewModel(Application aplication) {
        super(aplication);
        repository = new DataRepository(getApplication().getApplicationContext());
        contacts = new MutableLiveData<List<Contact>>();
        //contacts.setValue(getContacts());
    }



    public MutableLiveData<List<Contact>> getFavContactList() {
        return repository.getFavContactsLiveData();
    }



    public void refresh() {
        repository.asyncTaskFav();
    }

}
