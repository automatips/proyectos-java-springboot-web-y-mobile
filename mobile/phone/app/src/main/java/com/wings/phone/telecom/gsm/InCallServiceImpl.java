package com.wings.phone.telecom.gsm;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.telecom.Call;
import android.telecom.CallAudioState;
import android.telecom.InCallService;

import com.wings.phone.data.DataProvider;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.telecom.CallWrapper;
import com.wings.phone.util.ActivUtils;
import com.wings.phone.util.WingsConst;

import java.net.URLDecoder;

public class InCallServiceImpl extends InCallService {

    private CallManager callManager;

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onCallAdded(Call call) {


        callManager = CallManager.getInstance(InCallServiceImpl.this);




        //Add call to manager
        CallWrapper wrapper = new CallWrapper(call);
        String number = getCallNumber(call);
        wrapper.setCallBuddy(DataProvider.getCallBuddy(InCallServiceImpl.this, number));
        wrapper.setType(WingsConst.CALL_TYPE_GSM);
        callManager.addCall(wrapper);

        if (ActivUtils.isGsmActivityEnabled(InCallServiceImpl.this)) {
            Intent i = new Intent(WingsConst.INTENT_WINGS_GSM_CALL_ADDED);
            i.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            if(call.getDetails().getAccountHandle() == null){
                i.putExtra("account_selected", false);
            }

            //wake up call activity
            startActivity(i);
        }

        if(callManager.getCallList().size()>0){

        }
    }

    @Override
    public void onCallRemoved(Call call) {
        super.onCallRemoved(call);
        callManager.removeCall(String.valueOf(call.hashCode()));
    }

    @Override
    public void onCallAudioStateChanged(CallAudioState audioState) {
        super.onCallAudioStateChanged(audioState);
    }

    @Override
    public void onCanAddCallChanged(boolean canAddCall) {
        super.onCanAddCallChanged(canAddCall);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onConnectionEvent(Call call, String event, Bundle extras) {
        super.onConnectionEvent(call, event, extras);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onBringToForeground(boolean showDialpad) {
        super.onBringToForeground(showDialpad);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onSilenceRinger() {
        super.onSilenceRinger();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }
/*

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }
*/



    private String getCallNumber(Call call) {
        Uri handle = call.getDetails().getHandle();
        String number = getString(android.R.string.unknownName);
        if (handle != null) {
            number = handle.getEncodedSchemeSpecificPart();
            try {
                number = URLDecoder.decode(number, "UTF-8");
            } catch (Exception ex) {
            }
        }
        return number;
    }
}
