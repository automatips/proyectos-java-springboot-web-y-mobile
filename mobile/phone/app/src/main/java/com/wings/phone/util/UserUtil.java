package com.wings.phone.util;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by seba on 11/05/17.
 */
public class UserUtil {

    @SuppressWarnings("unchecked")
    public static Map<String, String> getUserInfo(Context context) {

        final String[] SELF_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Phone._ID,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.PHOTO_URI,};

        Cursor cursor = context.getContentResolver().query(ContactsContract.Profile.CONTENT_URI,
                SELF_PROJECTION, null, null, null);

        cursor.move(-1);
        cursor.moveToNext();

        Map ret = new HashMap();
        try {
            ret.put("name", cursor.getString(1));
            ret.put("pic", cursor.getString(2));
        } catch (Exception ex) {
            ret.put("name", "Me");
            ret.put("pic", "");
        }
        cursor.close();
        return ret;
    }

    /**
     * 
     * @return 
     */
    public static String getBackofficeUrl() {
        String url = Settings.getProvisioning("config.paymentLink", "https://servicios.wingsmobile.com/app/payments");
        try {
            url = url + "/" + URLEncoder.encode(EncryptUtils.encrypt(Settings.getRegisteredNumber(), true), "UTF-8");
        } catch (Exception e) {
        }
        return url;
    }
}
