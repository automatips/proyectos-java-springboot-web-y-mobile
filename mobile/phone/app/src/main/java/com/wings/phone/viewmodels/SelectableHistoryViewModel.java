package com.wings.phone.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.wings.phone.model.CallLog;
import com.wings.phone.model.DataRepository;

import java.util.List;

public class SelectableHistoryViewModel extends ViewModel {


    private MutableLiveData<List<CallLog>> callLogs;
    private DataRepository repository;

    public SelectableHistoryViewModel(DataRepository cr) {
        repository = cr;
        callLogs = new MutableLiveData<List<CallLog>>();
    }

    private List<CallLog> getCallLogs() {
        return null;
    }
    public LiveData<List<CallLog>> getCallLogtList() { return callLogs;}

    public void refresh() {
        callLogs.postValue(getCallLogs());
    }
}
