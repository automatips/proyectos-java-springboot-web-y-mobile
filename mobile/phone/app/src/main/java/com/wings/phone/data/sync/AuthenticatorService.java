package com.wings.phone.data.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.wings.phone.util.Settings;
import com.wings.phone.util.WingsConst;

public class AuthenticatorService extends Service {

    private Authenticator mAuthenticator;

    @Override
    public void onCreate() {
        mAuthenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {

        //When user deletes the account from android preferences
        AccountManager manager = AccountManager.get(AuthenticatorService.this);
        Account[] ass = manager.getAccountsByType(WingsConst.ACCOUNT_TYPE);
        if (ass.length == 0) {
            Settings.logout(AuthenticatorService.this);
        }

        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
