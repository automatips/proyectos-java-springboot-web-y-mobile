package com.wings.phone;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;
import com.wings.phone.util.UserUtil;
import com.wings.voip.model.DidNumberDTO;
import com.wings.voip.model.ServiceDTO;

import java.util.List;

import timber.log.Timber;

public class DidsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second_numbers);
        getSupportActionBar().setTitle(getString(R.string.second_numbers));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        showStatus();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showStatus() {

        LinearLayout layout = findViewById(R.id.second_number_my_container);
        layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(DidsActivity.this);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 0, 0, 8);

        //getDids service
        List<ServiceDTO> myServices = Settings.getMyServices(DidsActivity.this);
        ServiceDTO secondNumbers = null;
        for (final ServiceDTO service : myServices) {
            if (service.getCode().equals("didnumber")) {
                secondNumbers = service;
                break;
            }
        }

        if (secondNumbers == null) {
            return;
        }

        List<DidNumberDTO> dids = secondNumbers.getDids();
        for (final DidNumberDTO did : dids) {
            View container = inflater.inflate(R.layout.activity_second_numbers_detail, null);

            TextView txtNumber = container.findViewById(R.id.second_number_number);
            TextView txtStatus = container.findViewById(R.id.second_number_status);
            TextView txtExpiration = container.findViewById(R.id.second_number_expiry);
            Button action = container.findViewById(R.id.second_number_detail_action);

            if (did.getStatus().equals("created")) {
                txtNumber.setText(R.string.not_configured);
                txtStatus.setText(R.string.not_configured);
                txtExpiration.setText("");

                action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = UserUtil.getBackofficeUrl() + "?didnumber=true";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);

                        /*Intent intent = new Intent(DidsActivity.this, DidSetupActivity.class);
                        intent.setAction("" + did.getId());
                        startActivity(intent);*/
                    }
                });

            } else if (did.getStatus().equals("setup")) {

                try {
                    String number = PhoneNumberUtils.formatNumber(did.getNumber(), did.getCountry());
                    if (number == null) {
                        number = did.getNumber();
                    }
                    txtNumber.setText(number);
                    txtStatus.setText(R.string.configured);
                    txtExpiration.setText(StringUtils.formatMysqlStringToLocaleString(did.getExpiry()));
                    action.setVisibility(View.GONE);
                } catch (Exception e) {
                    Timber.e(e);
                }
            }

            container.setLayoutParams(params);
            layout.addView(container);

        }

    }

}
