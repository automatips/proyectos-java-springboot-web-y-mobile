package com.wings.phone.data;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;

import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CallLogManager {


    public static Map<String, String> getCallLogFromRegister(Context context, String number) {


        String unknown = context.getString(android.R.string.unknownName);
        Map<String, String> ret = new HashMap<>();
        int mcc = -1;
        int mnc = -1;

        Uri uri = CallLog.Calls.CONTENT_URI;
        Cursor cursor;
        if (number == null || number.equals(unknown)) {
            cursor = context.getContentResolver().query(uri, null, null, null, "_ID desc limit 1");
        } else {
            String queryString = "number = ?";
            cursor = context.getContentResolver().query(uri, null, queryString, new String[]{number}, null);
        }

        cursor.move(-1);
        cursor.moveToNext();

        try {
            if (cursor.getCount() != 0) {
                String subscriptionId = cursor.getString(cursor.getColumnIndex("subscription_id"));
                List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
                if (subscriptionInfos != null) {
                    for (int i = 0; i < subscriptionInfos.size(); i++) {
                        SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
                        if (lsuSubscriptionInfo.getIccId().equals(subscriptionId)) {
                            mcc = lsuSubscriptionInfo.getMcc();
                            mnc = lsuSubscriptionInfo.getMnc();
                        }
                    }
                }
            }
        } catch (SecurityException e) {
        }

        cursor.close();
        ret.put("mcc", String.valueOf(mcc));
        ret.put("mnc", String.valueOf(mnc));

        return ret;
    }

    public static void deleteCallLog(Context context, String number) {
        Uri uri = Uri.parse("content://call_log/calls");
        String queryString = "number = ?";
        context.getContentResolver().delete(uri, queryString, new String[]{number});
    }

    public static void deleteCallLog(Context context, Set<Integer> ids) {
        Uri uri = Uri.parse("content://call_log/calls");
        String queryString = "_ID = ?";
        for (Integer id : ids) {
            context.getContentResolver().delete(uri, queryString, new String[]{String.valueOf(id)});
        }
    }

    public static void deleteAllCallLog(Context context) {
        Uri uri = Uri.parse("content://call_log/calls");
        context.getContentResolver().delete(uri, null, null);
    }

    public static void markMissedCallsAsRead(Context context) {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {

            ContentValues values = new ContentValues();
            values.put(CallLog.Calls.NEW, 0);
            values.put(CallLog.Calls.IS_READ, 1);
            /*StringBuilder where = new StringBuilder();
            where.append(CallLog.Calls.NEW);
            where.append(" = 1 ");
            where.append(" AND ");
            where.append(CallLog.Calls.IS_READ);
            where.append(" = 0 ");*/

            context.getContentResolver().update(CallLog.Calls.CONTENT_URI, values, null, null);
        }

    }

}
