package com.wings.phone.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.wings.phone.util.ContactUtils;
import com.wings.voip.model.contact.Contact;

import java.util.LinkedList;
import java.util.List;

import static com.wings.phone.util.ContactUtils.getContactsArray;

@SuppressLint("MissingPermission")

public class DataRepository extends LiveData<List<CallLog>> {

    private static Context context;
    private static MutableLiveData<List<Contact>> contactsLiveData;
    private static MutableLiveData<List<Contact>> favContactsLiveData;
    private List<CallLog> CallLog;
    private static MutableLiveData<List<CallLog>> callLogLiveData;

    public DataRepository(Context context) {
        this.context = context.getApplicationContext();
        contactsLiveData = new MutableLiveData<>();

    }

    public MutableLiveData<List<Contact>> getContactsLiveData() {
        if(contactsLiveData ==null){
            contactsLiveData = new MutableLiveData<>();
        }
        List<Contact> cont = getContacts();
        contactsLiveData.postValue(cont);
        return contactsLiveData;
    }

    public MutableLiveData<List<Contact>> getFavContactsLiveData() {
        if(favContactsLiveData ==null){
            favContactsLiveData = new MutableLiveData<>();
        }
        List<Contact> favs = getFav();
        favContactsLiveData.postValue(favs);
        return favContactsLiveData;
    }


    public MutableLiveData<List<CallLog>> getCallLogLiveData() {
        if(callLogLiveData ==null){
            callLogLiveData = new MutableLiveData<>();
        }
        List<CallLog> callLogs = getCallLogs();
        callLogLiveData.postValue(callLogs);

        return callLogLiveData;
    }

    private static List<CallLog> getCallLogs(){
        List<CallLog> result = new LinkedList<>();
        String[] projection = new String[]{
                android.provider.CallLog.Calls.NUMBER,
                android.provider.CallLog.Calls.TYPE,
                android.provider.CallLog.Calls.DATE,
                android.provider.CallLog.Calls._ID,
                android.provider.CallLog.Calls.PHONE_ACCOUNT_ID,
                android.provider.CallLog.Calls.CACHED_NAME,
                android.provider.CallLog.Calls.CACHED_PHOTO_URI
        };
        Cursor cursor =  context.getContentResolver().query(android.provider.CallLog.Calls.CONTENT_URI, projection, null, null, android.provider.CallLog.Calls.DATE + " DESC ");
        while (cursor.moveToNext()) {
            CallLog current = new CallLog();
            current.setNumber(cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER)));
            current.setType(cursor.getInt(cursor.getColumnIndex(android.provider.CallLog.Calls.TYPE)));
            current.setDate(cursor.getLong(cursor.getColumnIndex(android.provider.CallLog.Calls.DATE)));
            current.setId(cursor.getInt(cursor.getColumnIndex(android.provider.CallLog.Calls._ID)));
            current.setAccountId(cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.PHONE_ACCOUNT_ID)));
            current.setCachedName(cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME)));
            current.setPhotoUri(cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.CACHED_PHOTO_URI)));
            current.process(context);
            result.add(current);


        }

        return result;
    }



    public List<Contact> getContacts(){
        List<Contact> result = new LinkedList<>();
        final String[] GROUP_PROJECTION = new String[]{
                ContactsContract.PhoneLookup._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup.PHOTO_URI,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID
        };
        final String SELECTION ="(" + ContactsContract.Contacts.HAS_PHONE_NUMBER + " != ? )";
        result = (List<Contact>) getContactsArray(context, GROUP_PROJECTION,SELECTION,new String[]{"0"},ContactsContract.Contacts.CONTENT_URI);
        return result;
    }
    public static List<Contact> getFav(){
        List<Contact> result = new LinkedList<>();
        result = ContactUtils.getFavContats(context);

        return result;
    }

    public void asyncTaskCallLog(){
        AsyncTask<Void, Void, List<CallLog>> task = new task();
        task.execute();
    }

    public static class task extends AsyncTask<Void, Void, List<CallLog>> {


        @Override
        protected List<CallLog> doInBackground(Void... voids) {
            return getCallLogs();
        }

        @Override
        protected void onPostExecute(List<CallLog> calls) {
            callLogLiveData.postValue(calls);

        }

    }



    public void asyncTaskFav() {
        AsyncTask<Void, Void, List<Contact>> task = new taskFav();
        task.execute();
    }

    public static class taskFav extends AsyncTask<Void, Void, List<Contact>> {


        @Override
        protected List<Contact> doInBackground(Void... voids) {
            return getFav();
        }

        @Override
        protected void onPostExecute(List<Contact> c) {
            favContactsLiveData.postValue(c);

        }

    }



}
