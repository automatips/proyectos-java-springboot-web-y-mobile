package com.wings.phone.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.R;
import com.wings.phone.gsm.MakeGsmCallHelper;
import com.wings.phone.model.CallLog;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.phone.util.Settings;
import com.wings.phone.util.WingsConst;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class SelectableHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<CallLog> calls;
    private Set<Integer> selectedIds = new HashSet<>();

    private int lastColor = 0;
    private String lastName = "";

    public SelectableHistoryAdapter(Context context, List<CallLog> calls){
        this.context = context;
        this.calls = calls;

    }

    @Override
    public int getCount() {
        return calls.size();
    }

    @Override
    public CallLog getItem(int position) { return calls.get(position); }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = LayoutInflater.from(context).inflate(R.layout.view_history_selectable_item, null);
        CallLog current = (CallLog)getItem(i);

        ((TextView) view.findViewById(R.id.when)).setText(when(current.getDate()));
        ((TextView) view.findViewById(R.id.via)).setText(Settings.getAccountMap().get(current.getAccountId()));

        view.findViewById(R.id.incoming_type).setVisibility(View.GONE);
        view.findViewById(R.id.outgoing_type).setVisibility(View.GONE);
        view.findViewById(R.id.rejected_type).setVisibility(View.GONE);

        CheckBox historyCheck = view.findViewById(R.id.history_check);
        Integer id = current.getId();
        historyCheck.setTag(id);
        historyCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Integer historyId = (Integer)buttonView.getTag();
                if(isChecked){
                    selectedIds.add(historyId);
                }else{
                    selectedIds.remove(historyId);
                }
            }
        });

        historyCheck.setChecked(selectedIds.contains(id));

        switch (current.getType()) {
            case android.provider.CallLog.Calls.INCOMING_TYPE:
                view.findViewById(R.id.incoming_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.OUTGOING_TYPE:
                view.findViewById(R.id.outgoing_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.VOICEMAIL_TYPE:
                break;
            case android.provider.CallLog.Calls.REJECTED_TYPE:
                view.findViewById(R.id.rejected_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.BLOCKED_TYPE:
                view.findViewById(R.id.rejected_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.ANSWERED_EXTERNALLY_TYPE:
                break;
            case android.provider.CallLog.Calls.MISSED_TYPE:
                view.findViewById(R.id.rejected_type).setVisibility(View.VISIBLE);
                break;
            default:
                Log.d("TYPE", "THis " + current.getType());
        }


        if (current.getNumber() != null) {

            //TODO: mejorar esto con busqueda por numero
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(current.getNumber()));

            String[] projection = new String[]{
                    ContactsContract.PhoneLookup._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME,
                    ContactsContract.PhoneLookup.PHOTO_URI
            };

            Cursor cursorContact = context.getContentResolver().query(uri, projection, null, null, null);
            cursorContact.moveToFirst();
            ((TextView) view.findViewById(R.id.phone_type)).setText("");
            try {
                String contactId = cursorContact.getString(0);

                String[] phoneProjection = new String[]{
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.LABEL,

                };

                Cursor phoneCur = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneProjection,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? ", new String[]{contactId}, null);
                phoneCur.move(-1);

                //TODO: mejorar esto con busqueda por numero
                while (phoneCur.moveToNext()) {

                    try {
                        String real = phoneCur.getString(0);
                        real = real.replaceAll("-", "").replaceAll(" ", "").replaceAll("\\+", "");
                        String toCompare = current.getNumber().replaceAll("-", "").replaceAll(" ", "").replaceAll("\\+", "");
                        if (real.equals(toCompare)) {
                            int phoneType = phoneCur.getInt(1);
                            String phoneLabel = phoneCur.getString(2);
                            CharSequence phoneTypeLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.getResources(), phoneType, phoneLabel);
                            ((TextView) view.findViewById(R.id.phone_type)).setText(phoneTypeLabel + ", ");
                            break;
                        }
                    } catch (Exception e) {
                    }
                }

            } catch (Exception e) {
            }


            cursorContact.close();
        }


        ((TextView) view.findViewById(R.id.history_name)).setText(current.getName());
        ImageView avatar = (ImageView) view.findViewById(R.id.history_avatar);
        int colorInt;
        if(!current.getName().equals(lastName)){
            colorInt = current.getId();
            lastName = current.getName();
            lastColor = colorInt;
        }else{
            colorInt = lastColor;
        }
        int color = ColorGenerator.MATERIAL.getColor(colorInt);

        if(current.getPhotoUri()!=null){
            Picasso.get().load(current.getPhotoUri()).transform(new CircleTransform()).into(avatar);
        }else {

            TextDrawable drawable = TextDrawable.builder().buildRound(current.getName().substring(0, 1), color);
            avatar.setImageDrawable(drawable);
        }
        ImageView callIcon = (ImageView) view.findViewById(R.id.call_icon);
        final String toDial = current.getNumber();

        callIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current.getAccountId() != null) {
                    MakeGsmCallHelper.makeIntentWithProvider(context, toDial, current.getAccountId());
                } else {
                    MakeGsmCallHelper.makeIntent(context, toDial);
                }
            }
        });

        if (current.getAccountId() != null && current.getAccountId().startsWith("ACCOUNT")) {
            switch (current.getAccountId()) {
                case WingsConst.ACCOUNT_WINGS:
                    callIcon.setImageResource(R.drawable.ic_call_wings2);
                    callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorBlue));
                    break;
                case WingsConst.ACCOUNT_SECURE:
                    callIcon.setImageResource(R.drawable.baseline_lock_24);
                    callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorBlue));
                    break;
                default:
                    callIcon.setImageResource(R.drawable.ic_call_free2);
                    callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPink));
                    break;
            }
        } else {
            callIcon.setImageResource(R.drawable.ic_call_gsm2);
            callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorGreen));
        }


        return view;
    }
    private String when(long date) {
        String ret;
        long diff = System.currentTimeMillis() - date;
        if (diff < 60000) {
            long seconds = TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS);
            if (seconds <= 1) {
                ret = context.getString(R.string.second_ago, 1);
            } else {
                ret = context.getString(R.string.seconds_ago, seconds);
            }
        } else if (diff < 3600000) {
            long minutes = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
            if (minutes <= 1) {
                ret = context.getString(R.string.minute_ago, 1);
            } else {
                ret = context.getString(R.string.minutes_ago, minutes);
            }
        } else if (diff < 86400000) {
            long hours = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
            if (hours <= 1) {
                ret = context.getString(R.string.hour_ago, 1);
            } else {
                ret = context.getString(R.string.hours_ago, hours);
            }
        } else if (diff < 2592000000L) {
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if (days <= 1) {
                ret = context.getString(R.string.day_ago, 1);
            } else {
                ret = context.getString(R.string.days_ago, days);
            }
        } else {
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            int months = (int) (days / 30);
            if (months <= 1) {
                ret = context.getString(R.string.month_ago);
            } else {
                ret = context.getString(R.string.months_ago, months);
            }
        }
        return ret;
    }
    public Set<Integer> getSelectedIds() {
        return selectedIds;
    }

    public void setCallLogList(List<CallLog> callLogs) {
        this.calls = callLogs;
    }
}
