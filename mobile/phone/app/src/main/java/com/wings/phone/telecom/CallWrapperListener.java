package com.wings.phone.telecom;


public interface CallWrapperListener {

    void onCallRemoved(CallWrapper call);

    void onCallStatusChange(CallWrapper call, String callStatus, String lastReason);

    void onStatsUpdate(String String);

}
