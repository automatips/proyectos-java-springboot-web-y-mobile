package com.wings.phone.firebase;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wings.phone.R;
import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.data.sync.ContactsManager;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.util.EncryptUtils;
import com.wings.phone.util.NotificationsHelper;
import com.wings.phone.util.Settings;
import com.wings.voip.model.GenericResponse;
import com.wings.voip.model.requests.RegisterRequest;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * @param remoteMessage
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if (notification != null) {
            handleNotification(notification);
        }

        Map<String, String> data = remoteMessage.getData();
        if (data != null && !data.isEmpty()) {
            handleData(data);
        }
    }

    /**
     * @param data
     */
    private void handleData(Map<String, String> data) {

        String title = data.get("title");
        String body = data.get("body");
        if (title == null || body == null) {
            return;
        }

        if (title.equals("INCOMING_CALL")) {
            String number = EncryptUtils.decrypt(body);
            boolean useZrtp = number.startsWith("sec");
            CallManager callManager = CallManager.getInstance(getApplicationContext());
            callManager.sipRegister(useZrtp);
        } else if (title.equals("UPDATE_CONTACTS")) {
            ContactsManager.requestSync(getApplicationContext());
        } else if (title.equals("UPDATE_PROVISIONING")) {
            Settings.updateProvisioning(getApplicationContext());
        }
    }

    /**
     * Handles notification in case app is on the foreground
     *
     * @param notification
     */
    private void handleNotification(RemoteMessage.Notification notification) {
        NotificationsHelper.showSecretNotification(getApplicationContext(), notification.getTitle(), notification.getBody());
    }


    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
        Timber.d("Listener -> Message sent");
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Settings.setPreference("wings.pushToken", s, MyFirebaseMessagingService.this);
        updateToken(s);
    }

    private void updateToken(String s) {

        String apiUrl = getString(R.string.settings_api_url);
        String appId = getString(R.string.settings_api_app_id);
        String appToken = getString(R.string.settings_api_app_token);

        Settings.init(MyFirebaseMessagingService.this);

        //data to send
        String login = Settings.getRegisteredNumber();
        String token = Settings.getToken();
        String pushToken = s;

        final RegisterRequest req = new RegisterRequest();
        req.setLogin(login);
        req.setToken(token);
        req.setPushToken(pushToken);
        req.setSerialNumber(Settings.getSerialNumber());

        Call<GenericResponse> call = ApiClientFactory.getClient(apiUrl).updateToken(appId, appToken, req);
        try {
            call.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }
}
