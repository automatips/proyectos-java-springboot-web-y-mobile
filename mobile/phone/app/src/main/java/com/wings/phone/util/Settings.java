package com.wings.phone.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wings.phone.BuildConfig;
import com.wings.phone.R;
import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.ServiceDTO;
import com.wings.voip.model.StoreServiceDTO;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class Settings {

    private static Gson gson = new Gson();

    private static boolean registered = false;
    private static String registeredNumber;
    private static String token;
    private static String imei1;
    private static String imei2;
    private static String operator1;
    private static int operator1Mcc;
    private static int operator1Mnc;
    private static String operator2;
    private static int operator2Mcc;
    private static int operator2Mnc;
    private static String operator1Id;
    private static String operator2Id;
    private static String operatorDefault;
    private static String pushToken;
    private static String instanceId;
    private static String udid;
    private static String country;
    private static String deviceId;
    private static String deviceName;
    private static String deviceModel;
    private static String serialNumber;
    private static int versionCode = BuildConfig.VERSION_CODE;
    private static String versionName = BuildConfig.VERSION_NAME;
    private static SharedPreferences preferences;
    private static Map<String, String> accountMap = new HashMap<>();
    private static final Map<String, String> provisioning = new HashMap<>();

    //format +54 9 351 803-9844
    //e164 +5493518039844
    //rfc tel:+54-9-351-803-9844

    public static void init(final Context context) {

        if (context.checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
        final PhoneAccountHandle handle = telecomManager.getDefaultOutgoingPhoneAccount("tel");
        if (handle == null) {
            operatorDefault = "GSM";
        } else {
            operatorDefault = telephonyManager.getNetworkOperatorName();
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Settings.setPreference("wings.pushToken", token, context);
                    }
                });


        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences == null || preferences.getAll().isEmpty()) {

            List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
            if (subscriptionInfos != null) {
                for (int i = 0; i < subscriptionInfos.size(); i++) {
                    SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
                    if (i == 0) {
                        operator1 = lsuSubscriptionInfo.getDisplayName().toString();
                        operator1Id = lsuSubscriptionInfo.getIccId();
                        operator1Mcc = lsuSubscriptionInfo.getMcc();
                        operator1Mnc = lsuSubscriptionInfo.getMnc();
                    } else {
                        operator2 = lsuSubscriptionInfo.getDisplayName().toString();
                        operator2Id = lsuSubscriptionInfo.getIccId();
                        operator2Mcc = lsuSubscriptionInfo.getMcc();
                        operator2Mnc = lsuSubscriptionInfo.getMnc();
                    }
                }
            }

            final String tmDevice, tmSerial, androidId;
            tmDevice = "" + telephonyManager.getImei();
            tmSerial = "" + telephonyManager.getSimSerialNumber();
            androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);


            BigInteger bint = new BigInteger(androidId, 16);
            //long mostbits = Long.parseLong(androidId, 16);
            long mostbits = bint.longValue();
            long minbits = ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode();

            //UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
            UUID deviceUuid = new UUID(mostbits, minbits);
            String udid = deviceUuid.toString();


            //init default settings
            preferences
                    .edit()
                    .putBoolean("wings.registered", false)
                    .putString("wings.registeredNumber", "")
                    .putString("wings.imei1", telephonyManager.getImei(0))
                    .putString("wings.imei2", telephonyManager.getImei(1))
                    .putString("wings.operator1", operator1)
                    .putInt("wings.operator1Mcc", operator1Mcc)
                    .putInt("wings.operator1Mnc", operator1Mnc)
                    .putString("wings.operator2", operator2)
                    .putInt("wings.operator2Mcc", operator2Mcc)
                    .putInt("wings.operator2Mnc", operator2Mnc)
                    .putString("wings.operator1_id", operator1Id)
                    .putString("wings.operator2_id", operator2Id)
                    .putString("wings.udid", udid)
                    .putString("wings.deviceId", androidId)
                    .putString("wings.deviceName", Build.MANUFACTURER)
                    .putString("wings.deviceModel", Build.MODEL)
                    .putString("wings.serialNumber", Build.getSerial())
                    //.putString("wings.pushToken", FirebaseInstanceId.getInstance().getToken())
                    .putString("wings.instanceId", FirebaseInstanceId.getInstance().getId())
                    .putString("wings.token", EncryptUtils.encrypt(StringUtils.generateRandomString()))
                    .putString("wings.country", getCountryIso(context))
                    .putBoolean("wings.useTLS", false)
                    .apply();
        }

        registered = preferences.getBoolean("wings.registered", false);
        registeredNumber = preferences.getString("wings.registeredNumber", "");
        imei1 = preferences.getString("wings.imei1", "");
        imei2 = preferences.getString("wings.imei2", "");
        operator1 = preferences.getString("wings.operator1", "");
        operator2 = preferences.getString("wings.operator2", "");
        operator1Id = preferences.getString("wings.operator1_id", "");
        operator2Id = preferences.getString("wings.operator2_id", "");
        udid = preferences.getString("wings.udid", "");
        deviceId = preferences.getString("wings.deviceId", "");
        deviceName = preferences.getString("wings.deviceName", "");
        deviceModel = preferences.getString("wings.deviceModel", "");
        serialNumber = preferences.getString("wings.serialNumber", "");
        pushToken = preferences.getString("wings.pushToken", "");
        instanceId = preferences.getString("wings.instanceId", "");
        token = preferences.getString("wings.token", "");
        country = preferences.getString("wings.country", "");
        operatorDefault = operatorDefault != null && !operatorDefault.isEmpty() ? operatorDefault : "GSM";

        operator1Mcc = preferences.getInt("wings.operator1Mcc", 0);
        operator1Mnc = preferences.getInt("wings.operator1Mnc", 0);
        operator2Mcc = preferences.getInt("wings.operator2Mcc", 0);
        operator2Mnc = preferences.getInt("wings.operator2Mnc", 0);

        accountMap.put(operator1Id, operator1);
        accountMap.put(operator2Id, operator2);
        accountMap.put(WingsConst.ACCOUNT_FREE, context.getString(R.string.call_detail_free));
        accountMap.put(WingsConst.ACCOUNT_WINGS, context.getString(R.string.call_detail_wings));
        accountMap.put(WingsConst.ACCOUNT_SECURE, context.getString(R.string.call_detail_secure));
        accountMap.put(WingsConst.ACCOUNT_VIDEO, "Video");

        setupProvisioning(context);

    }


    public static void loadImeis(Context context) {

        if (context.checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        imei1 = telephonyManager.getImei(0);
        imei2 = telephonyManager.getImei(1);

    }

    public static boolean isRegistered() {
        return registered;
    }

    public static void setRegistered(String number) {
        registeredNumber = number;
        registered = true;

        preferences
                .edit()
                .putBoolean("wings.registered", true)
                .putString("wings.registeredNumber", number)
                .apply();

    }

    public static String getToken() {
        return token;
    }

    public static String getImei1() {
        return imei1;
    }

    public static String getImei2() {
        return imei2;
    }

    public static String getOperator1() {
        return operator1;
    }

    public static String getOperator2() {
        return operator2;
    }

    public static String getRegisteredNumber() {
        return registeredNumber;
    }

    public static String getOperatorDefault() {
        if (operatorDefault == null || operatorDefault.isEmpty()) {
            return "GSM";
        }
        return operatorDefault;
    }

    public static String getPushToken(Context context) {
        if (pushToken == null || pushToken.isEmpty()) {
            pushToken = getPreference("wings.pushToken", "", context);
        }
        return pushToken;
    }

    public static String getDeviceModel() {
        return deviceModel;
    }

    public static String getDeviceName() {
        return deviceName;
    }

    public static String getSerialNumber() {
        return serialNumber;
    }

    public static String getDeviceId() {
        return deviceId;
    }

    public static String getUdid() {
        return udid;
    }

    public static String getInstanceId() {
        return instanceId;
    }

    public static String getVersionName() {
        return versionName;
    }

    public static int getVersionCode() {
        return versionCode;
    }

    public static String getCountry() {
        return country;
    }

    public static void setCountry(String country) {
        Settings.country = country;
    }
    
    public static void logout(Context context) {

        preferences.edit().clear().apply();

        SharedPreferences prefs = getPreferences(context);
        prefs.edit().clear().apply();

        registered = false;
        registeredNumber = null;
        imei1 = null;
        imei2 = null;
        operator1 = null;
        operator2 = null;
        operatorDefault = null;

        //delete the account on device
        AccountManager manager = AccountManager.get(context);
        Account[] accounts = manager.getAccountsByType(WingsConst.ACCOUNT_TYPE);
        for (Account account : accounts) {
            manager.removeAccountExplicitly(account);
        }
    }

    public static String getDetailsText() {
        StringBuilder b = new StringBuilder();

        b.append("wings.registered:  ").append(registered).append(System.lineSeparator());
        b.append("wings.registeredNumber:  ").append(registeredNumber).append(System.lineSeparator());
        b.append("wings.password:  ").append(EncryptUtils.decrypt(token)).append(System.lineSeparator());
        b.append("wings.imei1:  ").append(imei1).append(System.lineSeparator());
        b.append("wings.imei2:  ").append(imei2).append(System.lineSeparator());
        b.append("wings.operator1:  ").append(operator1).append(System.lineSeparator());
        b.append("wings.operator2:  ").append(operator2).append(System.lineSeparator());
        b.append("wings.operator1Id:  ").append(operator1Id).append(System.lineSeparator());
        b.append("wings.operator2Id:  ").append(operator2Id).append(System.lineSeparator());
        b.append("wings.operatorDefault:  ").append(operatorDefault).append(System.lineSeparator());
        b.append("wings.udid:  ").append(udid).append(System.lineSeparator());
        b.append("wings.deviceId:  ").append(deviceId).append(System.lineSeparator());
        b.append("wings.deviceName:  ").append(deviceName).append(System.lineSeparator());
        b.append("wings.deviceModel:  ").append(deviceModel).append(System.lineSeparator());
        b.append("wings.serialNumber:  ").append(serialNumber).append(System.lineSeparator());
        b.append("wings.instanceId:  ").append(instanceId).append(System.lineSeparator());
        b.append("wings.token:  ").append(token).append(System.lineSeparator());
        b.append("wings.pushToken:  ").append(pushToken).append(System.lineSeparator());
        b.append("wings.country:  ").append(country).append(System.lineSeparator());

        if (provisioning != null) {
            for (Map.Entry<String, String> entry : provisioning.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                b.append(key).append(" :").append(value).append(System.lineSeparator());
            }
        }

        return b.toString();
    }

    private static String getCountryIso(Context context) {
        TelephonyManager telephonyMngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyMngr.getSimCountryIso();
    }

    public static Map<String, String> getAccountMap() {
        return accountMap;
    }

    public static void updateOperatorDefault(String value, Context context) {
        operatorDefault = value;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences
                .edit()
                .putString("wings.operatorDefault", operatorDefault != null && !operatorDefault.isEmpty() ? operatorDefault : "GSM")
                .apply();
    }

    public static void setPreference(String key, String value, Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences
                .edit()
                .putString(key, value)
                .apply();
    }

    public static String getPreference(String key, String defValue, Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, defValue);
    }

    public static String getProvisioning(String key, String defaultValue) {
        if (provisioning.get(key) != null) {
            return provisioning.get(key);
        } else {
            Timber.e("Warning, provisioning key not found, using default");
            return defaultValue;
        }
    }

    public static void setProvisioning(Map<String, String> provisioning, Context context) {
        setObject(context, "provisioning", provisioning);
    }

    public static void setBalance(Double balance, Context context) {
        setObject(context, "client.balance", balance);
    }

    public static Double getBalance(Context context) {
        return getObject(context, "client.balance", Double.class);
    }

    public static List<ServiceDTO> getMyServices(Context context) {
        return getObject(context, "client.services", TypeToken.getParameterized(ArrayList.class, ServiceDTO.class).getType());
    }

    public static void setMyServices(Context context, List<ServiceDTO> myServices) {
        setObject(context, "client.services", myServices);
    }

    public static List<StoreServiceDTO> getStoreServices(Context context) {
        return getObject(context, "client.store", TypeToken.getParameterized(ArrayList.class, StoreServiceDTO.class).getType());
    }

    public static void setStoreServices(Context context, List<StoreServiceDTO> storeServices) {
        setObject(context, "client.store", storeServices);
    }

    public static Boolean getImSecure(Context context) {
        return getObject(context, "client.secure", Boolean.class);
    }

    public static void setImSecure(Context context, Boolean imSecure) {
        setObject(context, "client.secure", imSecure);
    }

    public static Boolean getHasDid(Context context) {
        return getObject(context, "client.hasdid", Boolean.class);
    }

    public static void setHasDid(Context context, Boolean hasDid) {
        setObject(context, "client.hasdid", hasDid);
    }

    public static void setRoamingFree(Context context, Boolean enabled) {
        setObject(context, "client.roamingFree", enabled);
    }

    public static Boolean getRoamingFree(Context context) {
        return getObject(context, "client.roamingFree", Boolean.class);
    }

    public static Boolean getWingsBook(Context context) {
        return getObject(context, "client.wingsBook", Boolean.class);
    }

    public static void setWingsBook(Context context, Boolean enabled) {
        setObject(context, "client.wingsBook", enabled);
    }

    public static void updateStatus(AccountStatus status, Context context) {
        Settings.setBalance(status.getClientBalance(), context);
        Settings.setMyServices(context, status.getServices());
        Settings.setImSecure(context, status.isSecure());
        Settings.setHasDid(context, status.getHasDid());
    }

    private static void setupProvisioning(Context context) {


        Map<String, String> pro = getObject(context, "provisioning", (Class<Map<String, String>>) (Class) Map.class);
        if (pro == null || pro.isEmpty()) {
            provisioning.put("sip.enableAdaptativeRateControl", "true");
            provisioning.put("sip.enableIce", "false");
            provisioning.put("sip.enableEchoLimiter", "true");
            provisioning.put("sip.enableEchoCancelation", "true");
            provisioning.put("sip.audioPortRange", "6000,8000");
            provisioning.put("sip.mainGain", "0.0");
            provisioning.put("sip.dnsServers", "8.8.8.8,8.8.4.4");
            provisioning.put("sip.sipStunServer", "stun.wingsmobile.net");
            provisioning.put("sip.adaptativeRateControlAlgo", "advanced");
            provisioning.put("config.logCollectionUploadServer", "http://switch.wingsmobile.net/api/logs/upload");
            provisioning.put("sip.registrationExpire", "60");
            provisioning.put("sip.tlsPort", "5061");
            provisioning.put("sip.videoJitterComp", "60");
            provisioning.put("sip.codecString", "OPUS,PCMU,PCMA,G729");
            provisioning.put("sip.enableStun", "true");
            provisioning.put("sip.playbackGainDb", "0.0");
            provisioning.put("sip.sipServer", "sip.wingsmobile.net");
            provisioning.put("config.enableLogCollection", "false");
            provisioning.put("sip.tcpPort", "0");
            provisioning.put("sip.enableTurn", "true");
            provisioning.put("sip.enableUpnp", "false");
            provisioning.put("sip.audioJitterComp", "60");
            provisioning.put("sip.udpPort", "0");
            provisioning.put("config.paymentLink", "https://wingsmobile.net/app/payments");
        } else {
            provisioning.clear();
            provisioning.putAll(pro);
        }
    }

    public static void updateProvisioning(final Context context) {

        final NotificationManager notifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        final Notification notif = NotificationsHelper.showWorkingNotification(context);
        notifyManager.notify(152, notif);

        String apiUrl = context.getString(R.string.settings_api_url);
        String appId = context.getString(R.string.settings_api_app_id);
        String token = context.getString(R.string.settings_api_app_token);

        //Request missed call
        Call<Map<String, String>> call = ApiClientFactory.getClient(apiUrl).getProvisioning(appId, token, Settings.getRegisteredNumber());
        try {
            call.enqueue(new Callback<Map<String, String>>() {
                @Override
                public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                    setObject(context, "provisioning", response.body());
                    notifyManager.cancel(152);
                    setupProvisioning(context);
                }

                @Override
                public void onFailure(Call<Map<String, String>> call, Throwable t) {
                    notifyManager.cancel(152);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static <T> T getObject(Context context, String key, Class<T> clazz) {

        SharedPreferences prefs = getPreferences(context);
        if (prefs == null) {
            return null;
        }

        String jsonContent = prefs.getString(key, "");
        if (jsonContent.isEmpty()) {
            return null;
        }

        return gson.fromJson(jsonContent, (Type) clazz);

    }

    private static <T> T getObject(Context context, String key, Type clazz) {

        SharedPreferences prefs = getPreferences(context);
        if (prefs == null) {
            return null;
        }

        String jsonContent = prefs.getString(key, "");
        if (jsonContent.isEmpty()) {
            return null;
        }

        return gson.fromJson(jsonContent, clazz);
    }

    private static void setObject(Context context, String key, Object toSave) {
        SharedPreferences prefs = getPreferences(context);
        if (prefs == null) {
            return;
        }
        SharedPreferences.Editor prefsEditor = prefs.edit();
        String json = gson.toJson(toSave);
        prefsEditor.putString(key, json);
        prefsEditor.commit();
    }

    private static SharedPreferences getPreferences(Context context) {
        try {
            SharedPreferences prefs = context.getSharedPreferences("com.wings.phone.provisioning", Context.MODE_PRIVATE);
            return prefs;
        } catch (Exception e) {
            Timber.e(e, "Error getting shared preferences");
        }
        return null;
    }


    public static int getOperator1Mcc() {
        return operator1Mcc;
    }

    public static int getOperator1Mnc() {
        return operator1Mnc;
    }

    public static int getOperator2Mcc() {
        return operator2Mcc;
    }

    public static int getOperator2Mnc() {
        return operator2Mnc;
    }
}
