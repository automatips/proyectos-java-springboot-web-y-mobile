package com.wings.phone.telecom;

import android.telecom.Call;

import com.wings.phone.util.NumberUtils;
import com.wings.phone.util.WingsConst;

import org.linphone.core.CoreListenerStub;
import org.linphone.core.Reason;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;



/**
 * Class used as envelope to wrap Sip Calls or Gsm Calls
 */
public final class CallWrapper {

    private android.telecom.Call gsmCall;
    private org.linphone.core.Call sipCall;
    private boolean sip = true;
    private CallBuddy callBuddy;
    private int type;
    private String status;
    private String lastReason;
    private boolean current = false;
    private int direction;
    private String secret;
    private int id = NumberUtils.getRandomCallId();
    private long activationTime = 0;
    private boolean onHold = false;
    private int holdCounter = 0;

    private CoreListenerStub mListener;


    public CallWrapper() {
    }

    public CallWrapper(android.telecom.Call gsmCall) {
        setGsmCall(gsmCall);
    }

    public CallWrapper(org.linphone.core.Call sipCall) {
        setSipCall(sipCall);
    }

    public void setGsmCall(Call gsmCall) {
        this.gsmCall = gsmCall;
        parseGsmState(gsmCall.getState());
        gsmCall.registerCallback(new Call.Callback() {
            @Override
            public void onStateChanged(Call call, int state) {
                super.onStateChanged(call, state);
                parseGsmState(state);
            }
        });
        sip = false;
    }

    public void setSipCall(org.linphone.core.Call sipCall) {
        this.sipCall = sipCall;
        sip = true;
    }

    public org.linphone.core.Call getSipCall() {
        return sipCall;
    }

    public boolean isSip() {
        return sip;
    }

    public void setSip(boolean sip) {
        this.sip = sip;
    }

    public void setCallBuddy(CallBuddy callBuddy) {
        this.callBuddy = callBuddy;
    }

    public CallBuddy getCallBuddy() {
        return callBuddy;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }

    public String getLastReason() {
        return lastReason;
    }

    public void setLastReason(String lastReason) {
        this.lastReason = lastReason;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getSecret() {
        return secret;
    }

    public Call getGsmCall() {
        return gsmCall;
    }

    public int getId() {
        return id;
    }

    public long getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(long activationTime) {
        this.activationTime = activationTime;
    }

    public void addGsmCallback(Call.Callback callback) {
        gsmCall.registerCallback(callback);
    }

    public boolean isOnHold() {
        return onHold;
    }


    public int decrementHoldCounter() {
        holdCounter--;
        if (holdCounter < 0) {
            holdCounter = 0;
        }
        return holdCounter;
    }

    public String getDurationString() {
        String ret = "00:00:00";
        if (activationTime != 0l) {
            long millis = System.currentTimeMillis() - activationTime;
            ret = String.format(Locale.ENGLISH, "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        }
        return ret;
    }

    public long getDurationInSeconds() {
        return (System.currentTimeMillis() - activationTime) / 1000;
    }

    public void hangUp() {
        if (sip) {
            sipCall.terminate();
        } else {
            gsmCall.disconnect();
        }
    }

    public void answer() {
        if (sip) {
            sipCall.accept();
        } else {
            gsmCall.answer(0);
        }
    }

    public void decline() {
        if (sip) {
            sipCall.decline(Reason.Declined);
        } else {
            gsmCall.reject(false, null);
        }
    }







    public void hold() {
        //Timber.e("onHold");

        holdCounter = 2;
        boolean result = false;
        if (sip) {
            sipCall.pause();

            try {
                int reintentos = 2;
                while(reintentos>0){
                    reintentos--;
                    Thread.sleep(1200);
                    org.linphone.core.Call.State state = sipCall.getState();
                    result = state == org.linphone.core.Call.State.Paused || state == org.linphone.core.Call.State.PausedByRemote;
                    //Timber.e("result: %s",result);
                    if(result){
                        break;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } else {
            gsmCall.hold();
            result = gsmCall.getState() == Call.STATE_HOLDING;
        }
        //Timber.e("sipCall.getState(): %s", sipCall.getState());
        //Timber.e("onHold: %s", result);
        onHold = result;
    }

    public void unHold() {
        //Timber.e("unHold");
        holdCounter = 0;
        onHold = false;
        if (sip) {
            //Timber.e("sipCall.getState(): %s", sipCall.getState());
            if (sipCall.getState() == org.linphone.core.Call.State.Paused) {
                sipCall.resume();
            }
        } else {
            gsmCall.unhold();
        }
    }


    public void playDtmfTone(char digit) {
        if (!sip) {
            gsmCall.playDtmfTone(digit);
            Timer t = new Timer();
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    gsmCall.stopDtmfTone();
                }
            }, 30);
        } else {
            sipCall.sendDtmf(digit);
        }
    }

    private void parseGsmState(int state) {
        switch (state) {
            case Call.STATE_ACTIVE:
                status = WingsConst.CALL_STATUS_ACTIVE;
                /*if (activationTime == 0l) {
                    activationTime = System.currentTimeMillis();
                }*/
                break;
            case Call.STATE_CONNECTING:
                status = WingsConst.CALL_STATUS_CONNECTING;
                direction = WingsConst.CALL_DIRECTION_OUTGOING;
                break;
            case Call.STATE_DIALING:
                status = WingsConst.CALL_STATUS_DIALING;
                direction = WingsConst.CALL_DIRECTION_OUTGOING;
                break;
            case Call.STATE_DISCONNECTED:
                status = WingsConst.CALL_STATUS_DISCONNECTED;
                break;
            case Call.STATE_DISCONNECTING:
                status = WingsConst.CALL_STATUS_DISCONNECTING;
                break;
            case Call.STATE_HOLDING:
                status = WingsConst.CALL_STATUS_HOLDING;
                break;
            case Call.STATE_NEW:
                status = WingsConst.CALL_STATUS_NEW;
                break;
            case Call.STATE_PULLING_CALL:
                status = WingsConst.CALL_STATUS_PULLING_CALL;
                break;
            case Call.STATE_RINGING:
                status = WingsConst.CALL_STATUS_RINGING;
                direction = WingsConst.CALL_DIRECTION_INCOMING;
                break;
            case Call.STATE_SELECT_PHONE_ACCOUNT:
                status = WingsConst.CALL_STATUS_SELECT_PHONE_ACCOUNT;
                break;
            default:
                status = "N/A";
        }




    }



}

