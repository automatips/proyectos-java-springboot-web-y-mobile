
package com.wings.play.domain.apkfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reason {

    @SerializedName("signature_validated")
    @Expose
    private SignatureValidated signatureValidated;
    @SerializedName("scanned")
    @Expose
    private Scanned scanned;

    public SignatureValidated getSignatureValidated() {
        return signatureValidated;
    }

    public void setSignatureValidated(SignatureValidated signatureValidated) {
        this.signatureValidated = signatureValidated;
    }

    public Scanned getScanned() {
        return scanned;
    }

    public void setScanned(Scanned scanned) {
        this.scanned = scanned;
    }

}
