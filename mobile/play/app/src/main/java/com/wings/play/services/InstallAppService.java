package com.wings.play.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
//import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.wings.play.BuildConfig;
import com.wings.play.R;
import com.wings.play.domain.ItemDetail;
import com.wings.play.providers.GenericFileProvider;
import com.wings.play.utils.ApiUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by seba on 22/10/17.
 */

public class InstallAppService extends Service {

    private final static String TAG = "InstallAppService";
    private final static String RECEIVER_TAG = "com.wings.play.InstallApp.DONE";

    private Context mContext;
    //private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotifyManager;

    private Queue<Integer> queue = new ConcurrentLinkedQueue<>();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.mContext = getApplicationContext();

        if (intent != null && intent.getExtras() != null) {
            Integer apkId = intent.getExtras().getInt("app_id", 0);
            if (apkId != 0) {
                if (queue.isEmpty()) {
                    start(apkId);
                }
                if (!queue.contains(apkId)) {
                    if (queue.isEmpty()) {
                        Toast.makeText(mContext, getString(R.string.item_started), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, getString(R.string.item_queued), Toast.LENGTH_SHORT).show();
                    }
                    queue.add(apkId);
                } else {
                    Toast.makeText(mContext, getString(R.string.item_already_queued), Toast.LENGTH_SHORT).show();
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void start(final Integer apkId) {

        String url = getString(R.string.api_url);
        Call<ItemDetail> call = ApiUtils.getApiClient(url).getAppDetail(apkId);
        try {
            call.enqueue(new Callback<ItemDetail>() {
                @Override
                public void onResponse(final Call<ItemDetail> call, Response<ItemDetail> response) {
                    downloadAnInstall(response.body());
                }

                @Override
                public void onFailure(Call<ItemDetail> call, Throwable t) {
                    queue.remove(apkId);
                    if (t instanceof IOException) {
                        Toast.makeText(InstallAppService.this, getString(R.string.net_error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (t instanceof SocketTimeoutException) {
                        Toast.makeText(InstallAppService.this, getString(R.string.net_error_timeout), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(InstallAppService.this, getString(R.string.net_error_generic), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            queue.remove(apkId);
            Toast.makeText(InstallAppService.this, getString(R.string.net_error_generic), Toast.LENGTH_SHORT).show();
        }
    }

    private void downloadAnInstall(ItemDetail detail) {

        final String appName = detail.getData().getName();
        final int id = detail.getData().getId();
        final String pkgName = detail.getData().getPackage();
        final String url = detail.getData().getFile().getPath();
        //final String storeTo = getFilesDir() + "/.play_download_" + detail.getData().getId() + ".apk";
        final String storeTo = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/play_download_" + detail.getData().getId() + ".apk";
        ;

        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //mBuilder = new NotificationCompat.Builder(this);
        /*mBuilder.setContentTitle(getString(R.string.install_downloading, appName))
                .setContentText(getString(R.string.notification_description))
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setSmallIcon(android.R.drawable.stat_sys_download);*/

        new Thread(new Runnable() {
            @Override
            public void run() {

                //registerReceiver(mInstallReceiver, new IntentFilter(RECEIVER_TAG));
                OkHttpClient client = ApiUtils.getClient();
                Request request = new Request.Builder().url(url).build();

                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {

                        int size = Integer.parseInt(response.header("Content-Length"));
                        int max = size / 1024;
                        InputStream is = response.body().byteStream();

                        BufferedInputStream input = new BufferedInputStream(is);
                        OutputStream output = new FileOutputStream(storeTo);

                        byte[] data = new byte[1024];
                        int total = 0;
                        int count;

                        int loops = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            output.write(data, 0, count);
                            if (loops % 5 == 0 || max == total / 1024) {

                                //mBuilder.setProgress(max, total / 1024, false).setContentText(total / 1024 + "/" + max);
                                //mNotifyManager.notify(id, mBuilder.build());
                            }
                            loops++;
                        }
                        output.flush();
                        output.close();
                        input.close();

                        //mBuilder.setContentTitle(getString(R.string.install_preparing)).setSmallIcon(R.drawable.ic_settings_black_24dp);
                        //String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(storeTo)).toString());
                        //String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                        PackageManager packageManger = getPackageManager();
                        boolean isNonPlayAppAllowed = packageManger.canRequestPackageInstalls();

                        File toInstall = new File(storeTo);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                            //Uri apkUri = FileProvider.getUriForFile(InstallAppService.this, "com.wings.play.fileprovider", toInstall);
                            //Uri apkUri = GenericFileProvider.getUriForFile(InstallAppService.this, BuildConfig.APPLICATION_ID + ".provider", toInstall);


                            Uri apkUri = GenericFileProvider.getUriForFile(InstallAppService.this, GenericFileProvider.PROVIDER_NAME, new File(storeTo));
                            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                            intent.setData(apkUri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        } else {
                            Uri apkUri = Uri.fromFile(toInstall);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }


                        /*if(!isNonPlayAppAllowed){
                            Intent i = new Intent(android.provider.Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.setData(Uri.parse("com.wings.play"));
                            startActivity(i);
                        }else{

                            Intent promptInstall = new Intent(Intent.ACTION_INSTALL_PACKAGE)
                                    .setData(Uri.fromFile(new File(storeTo)))
                                    .setType(mimetype);
                            promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(promptInstall);
                        }*/

                        /*PackageManager packageManger = getPackageManager();
                        PackageInstaller packageInstaller = packageManger.getPackageInstaller();
                        PackageInstaller.SessionParams sparams = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
                        sparams.setAppPackageName(pkgName);
                        try {
                            int sessionId = packageInstaller.createSession(sparams);
                            PackageInstaller.Session session = packageInstaller.openSession(sessionId);
                            OutputStream out = session.openWrite(pkgName + ".apk", 0, -1);

                            total = 0;
                            FileInputStream in = new FileInputStream(storeTo);
                            byte[] buffer = new byte[20480];
                            int c;
                            loops = 0;
                            while ((c = in.read(buffer)) != -1) {
                                out.write(buffer, 0, c);
                                total += c;
                                if (loops % 5 == 0 || max == total / 1024) {
                                    mBuilder.setProgress(max, total / 1024, false);
                                    mNotifyManager.notify(id, mBuilder.build());
                                }
                                loops++;
                            }
                            session.fsync(out);
                            in.close();
                            out.close();

                            mBuilder.setProgress(0, 0, true).
                                    setContentTitle(getString(R.string.install_installing, appName));
                            mNotifyManager.notify(id, mBuilder.build());

                            Intent intent = new Intent(RECEIVER_TAG);
                            intent.putExtra("app_pkg", pkgName);
                            intent.putExtra("app_name", appName);
                            intent.putExtra("app_id", id);

                            PendingIntent pIntent = PendingIntent.getBroadcast(mContext, sessionId, intent, 0);
                            session.commit(pIntent.getIntentSender());
                            session.close();

                            File f = new File(storeTo);
                            f.delete();

                        } catch (IOException e) {
                            queue.remove(id);
                            Log.d(TAG, "Error installing", e);
                            notifyError(appName);
                        }*/
                    } else {
                        queue.remove(id);
                        Log.e(TAG, "Error installing");
                        //notifyError(appName);
                    }

                } catch (Exception e) {
                    queue.remove(id);
                    Log.e(TAG, "Error installing", e);
                    //notifyError(appName);
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //unregisterReceiver(mInstallReceiver);
    }

    private BroadcastReceiver mInstallReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().equals(RECEIVER_TAG)) {

                int result = intent.getIntExtra(PackageInstaller.EXTRA_STATUS, PackageInstaller.STATUS_FAILURE);
                String pkgName = intent.getStringExtra("app_pkg");
                String appName = intent.getStringExtra("app_name");
                Integer appId = intent.getIntExtra("app_id", 0);

                if (result == PackageInstaller.STATUS_SUCCESS) {

                    mNotifyManager.cancelAll();
                    /*mBuilder = new NotificationCompat.Builder(mContext);

                    mBuilder.setContentTitle(getString(R.string.install_success, appName))
                            .setSmallIcon(R.drawable.ic_done_black_24dp)
                            .setAutoCancel(true);

                    Intent i = context.getPackageManager().getLaunchIntentForPackage(pkgName);
                    if (i != null) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        PendingIntent p = PendingIntent.getActivity(mContext, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);
                        mBuilder.setContentIntent(p);
                        mNotifyManager.notify(0, mBuilder.build());
                    }

                    */
                } else {
                    notifyError(appName);
                }

                if (appId != 0) {
                    queue.remove(appId);
                    Integer next = queue.peek();
                    if (next != null) {
                        start(next);
                    }
                }
            }
        }
    };


    private void notifyError(String appName) {

        mNotifyManager.cancelAll();
        while (mNotifyManager.getActiveNotifications().length > 0) {
            mNotifyManager.cancelAll();
        }

        /*mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setContentTitle(getString(R.string.install_error, appName))
                .setContentText(getString(R.string.notification_install_error_desc))
                .setSmallIcon(R.drawable.ic_error_black_24dp)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_error_black_24dp));

        mNotifyManager.notify(0, mBuilder.build());*/
    }

}
