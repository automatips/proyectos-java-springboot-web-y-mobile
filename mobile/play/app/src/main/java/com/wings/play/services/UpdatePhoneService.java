package com.wings.play.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
//import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.wings.play.R;
import com.wings.play.utils.ApiUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by seba on 20/10/17.
 */
public class UpdatePhoneService extends Service {

    private final static String TAG = "UpdatePhoneService";
    private final static String RECEIVER_TAG = "com.wings.play.UpdatePhone.DONE";

    private Context mContext;
    //private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotifyManager;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.mContext = getApplicationContext();

        download();

        return super.onStartCommand(intent, flags, startId);
    }

    private void download() {

        /*
        final int id = 0;
        final String url = getString(R.string.update_app_url);
        final String storeTo = getFilesDir() + "/.wings_update.zip";

        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_description))
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setSmallIcon(android.R.drawable.stat_sys_download);

        new Thread(new Runnable() {
            @Override
            public void run() {

                registerReceiver(mUpdatePhoneReceiver, new IntentFilter(RECEIVER_TAG));

                OkHttpClient client = ApiUtils.getClient();
                Request request = new Request.Builder().url(url).build();

                try {
                    Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {

                        int size = Integer.parseInt(response.header("Content-Length"));
                        int max = size / 1024;
                        InputStream is = response.body().byteStream();

                        BufferedInputStream input = new BufferedInputStream(is);
                        OutputStream output = new FileOutputStream(storeTo);

                        byte[] data = new byte[10240];
                        int total = 0;
                        int count;

                        int loops = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            output.write(data, 0, count);
                            if (loops % 5 == 0 || max == total / 1024) {
                                mBuilder.setProgress(max, total / 1024, false);
                                mNotifyManager.notify(id, mBuilder.build());
                            }
                            loops++;
                        }
                        output.flush();
                        output.close();
                        input.close();

                        mBuilder.setContentTitle(getString(R.string.notification_preparing))
                                .setSmallIcon(R.drawable.ic_settings_black_24dp);

                        PackageManager packageManger = getPackageManager();
                        PackageInstaller packageInstaller = packageManger.getPackageInstaller();
                        PackageInstaller.SessionParams sparams = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
                        sparams.setAppPackageName("com.wings.phone");
                        try {
                            int sessionId = packageInstaller.createSession(sparams);
                            PackageInstaller.Session session = packageInstaller.openSession(sessionId);
                            OutputStream out = session.openWrite("com.wings.phone.apk", 0, -1);

                            total = 0;
                            FileInputStream in = new FileInputStream(storeTo);
                            byte[] buffer = new byte[20480];
                            int c;
                            loops = 0;
                            while ((c = in.read(buffer)) != -1) {
                                out.write(buffer, 0, c);
                                total += c;
                                if (loops % 5 == 0 || max == total / 1024) {
                                    mBuilder.setProgress(max, total / 1024, false);
                                    mNotifyManager.notify(id, mBuilder.build());
                                }
                                loops++;
                            }
                            session.fsync(out);
                            in.close();
                            out.close();

                            mBuilder.setProgress(0, 0, true).
                                    setContentTitle(getString(R.string.notification_installing));
                            mNotifyManager.notify(id, mBuilder.build());

                            session.commit(PendingIntent.getBroadcast(mContext, sessionId, new Intent(RECEIVER_TAG), 0).getIntentSender());
                            session.close();

                            File f = new File(storeTo);
                            f.delete();

                        } catch (IOException e) {
                            Log.d(TAG, "Error upgrading", e);
                            notifyError();
                        }
                    } else {
                        notifyError();
                    }

                } catch (Exception e) {
                    Log.d("TESt", "ERROR", e);
                    notifyError();
                }
            }
        }).start();*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUpdatePhoneReceiver);
    }

    private BroadcastReceiver mUpdatePhoneReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            /*if (intent.getAction() != null && intent.getAction().equals(RECEIVER_TAG)) {

                int result = intent.getIntExtra(PackageInstaller.EXTRA_STATUS, PackageInstaller.STATUS_FAILURE);
                if (result == PackageInstaller.STATUS_SUCCESS) {

                    mNotifyManager.cancelAll();
                    mBuilder = new NotificationCompat.Builder(UpdatePhoneService.this);

                    mBuilder.setContentTitle(getString(R.string.notification_install_success))
                            .setSmallIcon(R.drawable.ic_done_black_24dp)
                            .setAutoCancel(true);

                    Intent i = context.getPackageManager().getLaunchIntentForPackage("com.wings.phone");
                    if (i != null) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        PendingIntent p = PendingIntent.getActivity(mContext, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);
                        mBuilder.setContentIntent(p);
                        mNotifyManager.notify(0, mBuilder.build());
                    }
                } else {
                    notifyError();
                }
            }

            UpdatePhoneService.this.stopSelf();*/
        }
    };

/*
    private void notifyError() {

        mNotifyManager.cancelAll();

        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setContentTitle(getString(R.string.notification_install_error))
                .setContentText(getString(R.string.notification_install_error_desc))
                .setSmallIcon(R.drawable.ic_error_black_24dp)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(UpdatePhoneService.this.getResources(), R.drawable.ic_error_black_24dp));

        mNotifyManager.notify(0, mBuilder.build());
    }
*/
}
