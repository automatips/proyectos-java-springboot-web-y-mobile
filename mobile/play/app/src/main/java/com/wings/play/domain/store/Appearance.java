
package com.wings.play.domain.store;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Appearance {

    @SerializedName("theme")
    @Expose
    private String theme;
    @SerializedName("description")
    @Expose
    private String description;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
