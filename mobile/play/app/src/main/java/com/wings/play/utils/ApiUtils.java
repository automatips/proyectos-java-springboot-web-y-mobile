package com.wings.play.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wings.play.api.ApiClientService;
import com.wings.play.domain.register.DeviceInfo;
import com.wings.play.views.Preferences;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by seba on 22/10/17.
 */

public class ApiUtils {

    public static ApiClientService getApiClient(String apiUrl) {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss Z").create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(ApiClientService.class);
    }

    public static OkHttpClient getClient() {

        String u = "android_app";
        String p = "wG6DEsS34rT59A9V";
        final String authToken = Credentials.basic(u, p);

        //HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)

                //.addInterceptor(loggingInterceptor)
                .build();
        return client;
    }

    public static void sendRegistrationToServer(final Context context, String url, DeviceInfo info) {

        final SharedPreferences shared = Preferences.getPreferences(context);
        shared.edit()
                .putString(Preferences.TOKEN, info.getToken())
                .putString(Preferences.ANDROID_ID, info.getAndroidId())
                .apply();

        Call<Void> call = ApiUtils.getApiClient(url).postRegisterDevice(info);
        try {
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(final Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        shared.edit().putBoolean(Preferences.TOKEN_SENT, true).apply();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("TEST", "error", t);
                }
            });

        } catch (Exception ex) {
            Log.d("TEST", "error", ex);
        }
    }
}
