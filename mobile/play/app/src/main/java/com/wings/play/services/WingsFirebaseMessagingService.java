package com.wings.play.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wings.play.MainActivity;
import com.wings.play.R;

import java.util.Map;

/**
 * Created by seba on 21/10/17.
 */

public class WingsFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "WingsFirebaseMessaging";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            parseNotification(remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle(),null);
        }
    }

    private void sendNotification(String body, String title,Intent intent) {

        if(intent == null){
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        PendingIntent pendingIntent = PendingIntent.getService(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_notification_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(WingsFirebaseMessagingService.this.getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void parseNotification(Map<String, String> data) {

        if (data.get("appId") != null) {
            Intent i = new Intent("com.wings.play.InstallApp");
            i.putExtra("app_id", Integer.valueOf(data.get("appId")));
            sendNotification(data.get("body"),data.get("title"),i);
        }
    }

}
