package com.wings.play.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wings.play.R;

/**
 * Created by seba on 21/10/17.
 */

public class ItemView extends LinearLayout {

    private Integer itemId;
    private TextView mName;
    private TextView mDownloads;
    private ImageView mImage;
    private RatingBar mRating;
    private Context mContext;

    public ItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.view_item_layout, this, true);

        mImage = (ImageView) layout.findViewById(R.id.item_image);
        mName = (TextView) layout.findViewById(R.id.item_title);
        mDownloads = (TextView) layout.findViewById(R.id.item_downloads);
        mRating = (RatingBar) layout.findViewById(R.id.item_rating);
        mContext = context;

    }

    public void setItemId(Integer id) {
        this.itemId = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setName(String name) {
        mName.setText(name);
    }

    public void setDownloads(int downloads) {
        mDownloads.setText(mContext.getString(R.string.downloads, parseDownloads(downloads)));
    }

    public void setRating(float rating) {
        mRating.setRating(rating);
    }

    public void setImage(String imageUrl) {
        Picasso.with(mContext)
                .load(imageUrl)
                .into(mImage);
    }

    private String parseDownloads(int downloads) {
        if (downloads < 1000) {
            return "" + downloads;
        } else if (downloads < 1000000) {
            return (downloads / 1000) + " k";
        } else {
            return (downloads / 1000000) + " M";
        }
    }
}
