
package com.wings.play.domain.apkfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Signature {

    @SerializedName("sha1")
    @Expose
    private String sha1;
    @SerializedName("owner")
    @Expose
    private String owner;

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

}
