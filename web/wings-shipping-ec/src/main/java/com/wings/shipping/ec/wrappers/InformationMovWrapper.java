/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.shipping.ec.wrappers;

/**
 *
 * @author seba
 */
public class InformationMovWrapper {

    private int idProc;
    private int idConc;
    private String nomConc;
    private String nomMov;
    private String oriMov;
    private String desMov;
    private String fecMov;
    private int idViewCliente;
    private String tipoMov;
    private String fechaProb;
    private String desTipoMov;
    private String observacion;

    public int getIdProc() {
        return idProc;
    }

    public void setIdProc(int idProc) {
        this.idProc = idProc;
    }

    public int getIdConc() {
        return idConc;
    }

    public void setIdConc(int idConc) {
        this.idConc = idConc;
    }

    public String getNomConc() {
        return nomConc;
    }

    public void setNomConc(String nomConc) {
        this.nomConc = nomConc;
    }

    public String getNomMov() {
        return nomMov;
    }

    public void setNomMov(String nomMov) {
        this.nomMov = nomMov;
    }

    public String getOriMov() {
        return oriMov;
    }

    public void setOriMov(String oriMov) {
        this.oriMov = oriMov;
    }

    public String getDesMov() {
        return desMov;
    }

    public void setDesMov(String desMov) {
        this.desMov = desMov;
    }

    public String getFecMov() {
        return fecMov;
    }

    public void setFecMov(String fecMov) {
        this.fecMov = fecMov;
    }

    public int getIdViewCliente() {
        return idViewCliente;
    }

    public void setIdViewCliente(int idViewCliente) {
        this.idViewCliente = idViewCliente;
    }

    public String getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(String tipoMov) {
        this.tipoMov = tipoMov;
    }

    public String getFechaProb() {
        return fechaProb;
    }

    public void setFechaProb(String fechaProb) {
        this.fechaProb = fechaProb;
    }

    public String getDesTipoMov() {
        return desTipoMov;
    }

    public void setDesTipoMov(String desTipoMov) {
        this.desTipoMov = desTipoMov;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

}
