/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.serial.domain.repo;

import com.wings.serial.domain.entities.KeyCode;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface KeyCodeRepository extends JpaRepository<KeyCode, Long> {

    KeyCode findOneByPinCode(String pinCode);

    List<KeyCode> findByPinCode(String pinCode);

}
