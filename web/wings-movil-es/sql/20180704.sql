alter table wings_registration.invoices add column period_number bigint default 0;

alter table wings_registration.orders add column anulada tinyint(1) default 0;
alter table wings_registration.order_activities add column order_id bigint(20) default null;
update wings_registration.order_activities oa set order_id = (select o.id from orders o where o.tracking_number = oa.tracking_code limit 1);

--alter table wings_registration.invoices drop column period_number;

-- CREATE TABLE `plan_price` (
--   `id` bigint(20) NOT NULL AUTO_INCREMENT,
--   `plan_id` bigint(20) NOT NULL ,
--   `price` decimal(10,2) NOT NULL,
--   `from_date` DATETIME DEFAULT NULL,
--   `to_date` DATETIME DEFAULT NULL,
--   `active` TINYINT(1) NOT NULL DEFAULT 0,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--  CREATE TABLE `email_request` (
--   `id` bigint(20) NOT NULL AUTO_INCREMENT,
--   `from` varchar(250),
--   `to` varchar(250),
--   `subject` varchar(250),
--   `body` text,
--   `attachments` text,
--   `schedule` DATETIME DEFAULT NULL,
--   `sended` TINYINT(1) NOT NULL DEFAULT 0,
--   `sended_date` DATETIME DEFAULT NULL,
--   `retries` INT NOT NULL DEFAULT 0,
-- 
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;