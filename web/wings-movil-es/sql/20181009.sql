CREATE TABLE `client_debts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,  
  `client_id` bigint(20) NOT NULL,
  `active_lines` bigint(20) NOT NULL,
  `canceled_lines` bigint(20) NOT NULL,  
  `balance` double(10,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--DROP TABLE `client_debts_reminder`;
CREATE TABLE `client_debts_reminder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,
  `creation_date` DATETIME DEFAULT NULL,
  `last_send_date` DATETIME DEFAULT NULL,
  `sms` int NOT NULL DEFAULT 0,
  `sms_text` varchar(160),
  `sms_status` varchar(160),
  `email` int NOT NULL DEFAULT 0,
  `email_text` text,
  `email_status` varchar(160),
  `token` varchar(25),
  `visited` int NOT NULL DEFAULT 0,
  `payed` tinyint(1) NOT NULL DEFAULT 0,
  `counter` int NOT NULL DEFAULT 0,
  `balance` double(10,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


