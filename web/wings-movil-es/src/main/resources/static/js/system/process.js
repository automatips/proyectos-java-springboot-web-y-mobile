$(document).ready(function ($) {

    /**acciones de los botones **/
    $('.action-item').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');

        $.post('api/system/process/' + action, null, function (data, status) {

            if (status === 'success') {
                $.notify({
                    title: '<strong>Informacion</strong>',
                    message: data
                }, {
                    newest_on_top: true,
                    type: 'success',
                    delay: 3000
                });
            } else {
                $.notify('Ocurrio un error al solicitar el pedido', {
                    type: 'warning',
                    allow_dismiss: true
                });
            }
        }).fail(function (jxrq) {
            notifyError(jxrq.responseText);
        });
    });

});
