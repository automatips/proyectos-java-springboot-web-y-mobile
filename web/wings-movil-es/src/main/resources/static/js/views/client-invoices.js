$(document).ready(function ($) {

    $('.action-button').unbind('click');
    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'copy-link') {
            var url = $(e.target).data('url');
            copyTextToClipboard(url);
        } else if (action === 'action-item-send-test-contract') {

        } else if (action === 'charge-invoice') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Confirmar cobro';
            var message = '¿Desea cobrar el monto de la factura  (#' + id + ') ' + name + ' ?';

            confirmDialogDanger(title, message, function (result) {
                if (result) {
                    showAlert('Contactando pasarela espere por favor...', 'alert-info');
                    $.post('/api/clients/invoice/charge/' + id, null, function (response) {
                        showAlert(response, 'alert-success');
                    }).fail(function (xhr, status, error) {
                        showAlert('Error en el cobro ' + xhr.responseText, 'alert-warning');
                    });
                }
            });

        } else if (action === 'refund-invoice') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Confirmar devolución';
            var message = '¿Desea devolver el monto de la factura  (#' + id + ') ' + name + ' ?';

            confirmDialogDanger(title, message, function (result) {
                if (result) {
                    showAlert('Contactando pasarela espere por favor...', 'alert-info');
                    $.post('/api/clients/invoice/refund/' + id, null, function (response) {
                        showAlert(response, 'alert-success');
                    }).fail(function (xhr, status, error) {
                        showAlert('Error en la devolucion ' + xhr.responseText, 'alert-warning');
                    });
                }
            });

        } else if (action === 'view-invoice-detail') {

            var id = $(e.target).data('id');
            $.post('/api/clients/invoice/payment-details/' + id, null, function (response) {
                showAlert(response, 'alert-success');
            }).fail(function (xhr, status, error) {
                showAlert('Error en la devolucion ' + xhr.responseText, 'alert-warning');
            });

        } else if (action === 'view-invoice-balance') {

            var id = $(e.target).data('id');
            $.post('/api/clients/invoice/balance-details/' + id, null, function (response) {
                showAlert(response, 'alert-success');
            }).fail(function (xhr, status, error) {
                showAlert('Error en la devolucion ' + xhr.responseText, 'alert-warning');
            });

        }
    });

    function fallbackCopyTextToClipboard(text) {
        var textarea = document.createElement('textarea');
        textarea.textContent = text;
        document.body.appendChild(textarea);
        var selection = document.getSelection();
        var range = document.createRange();
        range.selectNode(textarea);
        selection.removeAllRanges();
        selection.addRange(range);

        if (document.execCommand('copy')) {
            showalert("Enlace copiado al portapapeles", 'alert-success');
        } else {
            showalert("No se pudo copiar el enlace", 'alert-error');
        }
        selection.removeAllRanges();
        document.body.removeChild(textarea);
    }

    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
            return;
        }
        navigator.clipboard.writeText(text).then(function () {
            console.log('Async: Copying to clipboard was successful!');
        }, function (err) {
            console.error('Async: Could not copy text: ', err);
        });
    }



});
