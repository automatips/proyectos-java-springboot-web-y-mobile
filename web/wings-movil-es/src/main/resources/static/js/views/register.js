var bootbox;
$(document).ready(function ($) {

    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var ids = getCheckedItems();
        var action = $(e.target).attr('id');
        var postData = {};
        postData.ids = ids;

        if (action === 'action-create-line-holder') {

            if (ids.length > 1) {
                bootbox.alert("Solo se puede crear un titular a la vez");
                return;
            }

            var notify = $.notify('Solicitando creacion titular', {
                type: 'info',
                showProgressbar: true
            });

            postData.id = ids[0];
            $.post('/api/registration/create-holder', postData, function (data, status) {
                notify.close();
                if (status === 'success') {
                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 10000,
                        allow_dismiss: true
                    });
                } else {
                    $.notify('Ocurrio un error al crear los clientes', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            }).error(function (response) {
                notify(response.responseText);
            });
        }
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'action-mark-verified') {

            var regId = $(e.target).data('regid');
            var url = '/api/registration/mark-verified/' + regId;
                     
            $.confirm({
                title: 'Confirmación!',
                content: '¿Marcar como verificada?',
                buttons: {
                    confirmar: function () {
                        $.post(url,null,function(response){
                            $.alert(response);
                        });
                    },
                    cancelar: function () {
                        
                    }
                }
            });
        }
    });


    function getCheckedItems() {
        var ids = [];
        $('table input:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        return ids;
    }

});
