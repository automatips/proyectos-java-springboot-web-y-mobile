$(document).ready(function ($) {

    if($('#textarea-input')){
        $('#textarea-input').focus();
    }else{
        $('.search-input').focus();        
    }

    $('.action-item').click(function (e) {
        e.preventDefault();
        //var target = $(e.currentTarget).first();
        var url = window.location.href;
        setUpUrl(addParam(url, 'q', encodeURI(this.id)));
    });

});
