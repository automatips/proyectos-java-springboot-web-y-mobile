$(document).ready(function ($) {
    $('#dtp-desde').datepicker({
        dateFormat: "dd/mm/yy"
    });
    $('#dtp-hasta').datepicker({
        dateFormat: "dd/mm/yy"
    });

    $("#dtp-desde").on("dp.change", function (e) {
        $('#dtp-hasta').data("DatePicker").minDate(e.date);
    });
    $("#dtp-hasta").on("dp.change", function (e) {
        $('#dtp-desde').data("DatePicker").maxDate(e.date);
    });

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
                
        var client = $('#client').val();
        url = addParam(url, 'client', client);
        url = addParam(url, 'origin', $('#origin').val() );
        url = addParam(url, 'from',  $('#dtp-desde').val() );
        url = addParam(url, 'to', $('#dtp-hasta').val() );
        url = addParam(url, 'priced', $('#priced').val() );
        url = addParam(url, 'type', $('#type').val() );
        
        setUpUrl(url);
    });



});
