$(document).ready(function ($) {

    /***Acciones del dropdown**/
    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var ids = getCheckedItems();
        var action = $(e.target).attr('id');
        var postData = {};
        postData.ids = ids;

        if (action === 'action-gen-contracts') {

            var notify = $.notify('Creando contratos para las lineas existentes', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/clients/contract/gen', null, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        allow_dismiss: true
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar la creacion de contratos', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        } else if (action === 'action-send-contracts') {

            if (ids.length === 0) {

                var notify = $.notify('Debe seleccionar contratos', {
                    type: 'danger',
                    allow_dismiss: true
                });

            } else {

                var notify = $.notify('Enviando contratos al portal de firmas', {
                    type: 'info',
                    showProgressbar: true
                });

                $.post('api/clients/contract/send', postData, function (data, status) {

                    notify.close();

                    if (status === 'success') {

                        $.notify({
                            title: '<strong>Informacion</strong>',
                            message: data
                        }, {
                            newest_on_top: true,
                            type: 'success',
                            allow_dismiss: true
                        });

                    } else {

                        $.notify('Ocurrio un error enviar los contratos', {
                            type: 'warning',
                            allow_dismiss: true
                        });
                    }
                });
            }

        }
    });

    /**acciones de los botones **/
    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'action-item-check-contract-status') {

            var notify = $.notify('Verificando estado de contratos', {
                type: 'info',
                showProgressbar: true
            });

            $.post('/api/clients/contract/status', null, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar el pedido', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        } else if (action === 'action-item-send-test-contract') {
            postProcessRequest('/api/clients/contract/sendtest', null);
        } else if (action === 'edit-contract') {
            
            var url = $(e.target).data('url');
            //loadInDialog(url,"Editar",true,false);
            
             $('#viewModal').modal();
            $body = $('#viewModal').find('.modal-body');
            $body.load(url);
            
           
        }
    });


    function getCheckedItems() {
        var ids = [];
        $('table input:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        return ids;
    }


});
