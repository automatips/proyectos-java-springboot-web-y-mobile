$(document).ready(function ($) {

    /***Acciones del dropdown**/
    $('#actions-dropdown').click(function (e) {
        e.preventDefault();
        var ids = getCheckedItems();
        var action = $(e.target).attr('id');
        var postData = {};
        postData.ids = ids;
    });

    /**acciones de los botones **/
    $('.action-item').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');
        if (action === 'action-item-import') {
            
            $('#import-modal').modal().show();

            $('#action-form-import-submit').on('click', function () {

                var notify = $.notify('Importanto archivo', {
                    type: 'info',
                    showProgressbar: true
                });

                //grab all form data
                var formData = new FormData($('#form-upload-csv')[0]);
                $.ajax({
                    url: 'api/clients/orders/import',
                    type: 'POST',
                    data: formData,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (returndata) {
                        notify.close();
                        $.notify({
                            title: '<strong>Informacion</strong>',
                            message: returndata
                        }, {
                            newest_on_top: true,
                            type: 'success',
                            delay: 3000
                        });
                    },
                    error: function () {
                        notify.close();
                        $.notify('Ocurrio un error al importar el archivo.', {
                            type: 'warning',
                            allow_dismiss: true
                        });
                    },
                    complete: function (jqXHR, textStatus) {
                        $('#import-modal').modal('toggle');
                    }
                });
                return false;
            });

        } else if (action === 'action-item-send-ups') {

            var notify = $.notify({
                icon: 'fa fa-circle-o-notch fa-lg mt-4 fa-spin',
                title: 'Enviando pedidos a ups',
                message: 'Espere por favor...'
            }, {
                delay: 500000,
                type: "info",
                allow_dismiss: false,
                showProgressbar: false,
                icon_type: 'class'
            });


            $.post('api/clients/orders/send', null, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al enviar los pedidos', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        }
    });

    function getCheckedItems() {
        var ids = [];
        $('table input:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        return ids;
    }

});
