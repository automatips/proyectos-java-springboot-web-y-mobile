/*
 * Common funcionality for search and modals
 */
$(document).ready(function ($) {

    /*Modal*/
    $('#viewModal').on('shown.bs.modal', function (e) {
        var url = $(e.relatedTarget).data('url');
        $(this).find('.modal-body').load(url);
    });

    $('#viewModal').on('hidden.bs.modal', function (e) {
        $('#viewModal').find('.modal-body').empty();
        $(this).find('.modal-body').html('<div class="text-center"><i class="fa fa-circle-o-notch fa-lg mt-4 fa-spin"></i></div>');
    });

    $('#editModal').on('shown.bs.modal', function (e) {
        var url = $(e.relatedTarget).data('url');
        $(this).find('.modal-body').load(url);
    });

    $('#editModal').on('hidden.bs.modal', function (e) {
        $('#viewModal').find('.modal-body').empty();
        $(this).find('.modal-body').html('<div class="text-center"><i class="fa fa-circle-o-notch fa-lg mt-4 fa-spin"></i></div>');
    });

    /*Select checkboxes*/
    $(".chk-check-all").click(function (e) {
        if ($(this).prop("checked")) {
            $('input[type="checkbox"]').prop("checked", true);
        } else {
            $('input[type="checkbox"]').prop("checked", false);
        }
    });

    /*Search*/
    $(".search-clear").click(function () {
        $('.search-input').val('');
        var url = window.location.href;
        setUpUrl(addParam(url, 'q', ''));
    });

    $(".filter-clear").click(function () {
        $(this).next('input').val('');
    });

    $('.search-input').keypress(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            var url = window.location.href;
            setUpUrl(addParam(url, 'q', encodeURI(this.value)));
        }
    });

    $('.search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        setUpUrl(addParam(url, 'q', encodeURI($('.search-input').first().val())));
    });

});



/**Search functions***/
function addParam(url, param, value) {

    if (url.indexOf('?') === -1) {
        if (value !== '') {
            url += '?' + param + '=' + value;
        }
    } else {
        var hash = {};
        var init = url.substring(0, url.indexOf('?'));
        var params = url.substring(url.indexOf('?'), url.length);
        var parameters = params.split(/\?|&/);
        for (var i = 0; i < parameters.length; i++) {
            if (!parameters[i])
                continue;
            var ary = parameters[i].split('=');
            hash[ary[0]] = ary[1];
        }
        if (value !== '') {
            hash[param] = value;
        } else {
            delete hash[param];
        }
        var list = [];
        Object.keys(hash).forEach(function (key) {
            list.push(key + '=' + hash[key]);
        });
        url = init + '?' + list.join('&');
    }

    url = url.substring(url.indexOf("#") + 1);
    return url;
}


function postProcessRequest(url, data) {

    $.post(url, data, function (data, status) {
        var notifTitle = 'Ocurrió un error al enviar la peticion';
        var notifType = 'warning';
        if (status === 'success') {
            notifTitle = 'Solicitud enviada con exito';
            notifType = 'success';
        }

        $.notify({
            title: notifTitle,
            message: 'Ver el log de procesos para mas información'
        }, {
            newest_on_top: true,
            allow_dismiss: true,
            type: notifType,
            delay: 2000
        });
    });

}

function loadInDialog(url, title, big, scrollable) {

    var className = '';
    if (big) {
        className = 'modal-big';
    }

    var dialog = bootbox.dialog({
        title: title,
        onEscape: function () {},
        className: className,
        message: '<p><i class="fa fa-spin fa-spinner"></i> Cargando...</p>'
    });
    dialog.init(function () {
        if (scrollable) {
            dialog.find('.bootbox-body').addClass('pre-scrollable');
        }
        dialog.find('.bootbox-body').load(url);
    });
}

function notify(message) {
    $.notify({
        title: '<strong>Informacion</strong>',
        message: message
    }, {
        newest_on_top: true,
        type: 'info',
        allow_dismiss: true
    });
}

function notifySuccess(message) {
    $.notify({
        title: '<strong>Informacion</strong>',
        message: message
    }, {
        newest_on_top: true,
        type: 'success',
        allow_dismiss: true
    });
}

function notifyError(message) {
    $.notify({
        title: '<strong>Error</strong>',
        message: message
    }, {
        newest_on_top: true,
        type: 'warning',
        allow_dismiss: true
    });
}

function showAlert(message, alerttype) {
    $('#alert_placeholder').html('');
    $('#alert_placeholder').append('<div id="alertdiv" class="alert ' + alerttype + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
}

function showAlertAutoHide(message, alerttype) {
    $('#alert_placeholder').html('');
    $('#alert_placeholder').append('<div id="alertdiv" class="alert ' + alerttype + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    setTimeout(function () { // this will automatically close the alert and remove this if the users doesnt close it in 5 secs
        $("#alertdiv").remove();
    }, 5000);
}

function confirmDialogDanger(title, message, callback) {
    bootbox.confirm({
        title: title,
        message: message,
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Aceptar',
                className: 'btn-danger'
            }
        },
        callback: callback
    });
}