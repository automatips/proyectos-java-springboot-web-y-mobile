/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author seba
 */
public class DateUtils {

    /**
     * Returns days bettween from and today
     *
     * @param from
     * @return
     */
    public static long getDiffDays(Date from) {
        long timeFrom = from.getTime();
        long timeTo = Calendar.getInstance(new Locale("es", "ES")).getTimeInMillis();
        long ret = timeFrom - timeTo;
        return TimeUnit.DAYS.convert(ret, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns days between from and to
     *
     * @param from
     * @param to
     * @return
     */
    public static long getDiffDays(Date from, Date to) {
        long timeFrom = from.getTime();
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(to);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        long timeTo = cal.getTimeInMillis();
        long ret = timeTo - timeFrom;
        return TimeUnit.DAYS.convert(ret, TimeUnit.MILLISECONDS);
    }

    /**
     * Check if portability date is bigger than today, if it is returns same
     * date, else return tomorrows date
     *
     * @param plannedDate
     * @return
     */
    public static Date validatePortabilityDate(Date plannedDate) {
        long days = getDiffDays(plannedDate);
        if (days <= 0) {
            Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
            cal.add(Calendar.DAY_OF_MONTH, 1);
            plannedDate = cal.getTime();
        }
        return plannedDate;
    }

    /**
     * Return first day of the month with 0 hour, minute and seconds
     *
     * @param month
     * @param year
     * @return
     */
    public static Date getStartDateForPeriod(int month, int year) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * Return first day of the month with 0 hour, minute and seconds
     *
     * @return
     */
    public static Date getStartDateForNextPeriod() {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.add(Calendar.MONTH, 1);

        cal.set(Calendar.DATE, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        return cal.getTime();
    }

    /**
     * returns last day of the mont with 23 hours 59 minutes and seconds
     *
     * @param month
     * @param year
     * @return
     */
    public static Date getEndDateForPeriod(int month, int year) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, 15);
        cal.set(Calendar.MONTH, month - 1);
        int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * Return the month for date
     *
     * @param date
     * @return
     */
    public static int getMonthForDate(Date date) {

        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);
        return cal.get(Calendar.MONTH) + 1;

    }

    /**
     * Return the year for date
     *
     * @param date
     * @return
     */
    public static int getYearForDate(Date date) {

        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);
        return cal.get(Calendar.YEAR);

    }

    /**
     * returns tomorrow date with 00 hours 00 minutes and seconds
     *
     * @return
     */
    public static Date getTomorrowDate() {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
        cal.set(Calendar.HOUR_OF_DAY, 00);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        return cal.getTime();
    }

    /**
     * Return true if date it's between 9 and 17 in spain
     *
     * @param date
     * @return
     */
    public static boolean isInWorkHours(Date date) {

        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 9);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);

        Date from = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 17);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);

        Date to = cal.getTime();

        return date.after(from) && date.before(to);

    }

}
