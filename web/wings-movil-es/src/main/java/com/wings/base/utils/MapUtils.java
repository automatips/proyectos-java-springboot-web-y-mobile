/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationComplete;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author seba
 */
public class MapUtils {

    private final static Logger log = LoggerFactory.getLogger(MapUtils.class);

    public static List<FormElement> getFormElementsFiltered(Object o,List<String> fieldNames) {

        List<FormElement> list = new LinkedList<>();
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            try {
                
                if(fieldNames != null && !fieldNames.contains(field.getName())){
                    continue;
                }
                
                if (field.get(o) instanceof Collection) {
                    continue;
                }

                //log.error(field.getName());
                list.add(new FormElement(field.getName(), field.get(o), field.getType().toString()));

            } catch (IllegalAccessException | IllegalArgumentException e) {
            }
        }
        return list;
    }
    
    public static List<FormElement> getFormElements(Object o) {

        List<FormElement> list = new LinkedList<>();
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            try {
                if (field.get(o) instanceof Collection) {
                    continue;
                }

                //log.error(field.getName());
                list.add(new FormElement(field.getName(), field.get(o), field.getType().toString()));

            } catch (IllegalAccessException | IllegalArgumentException e) {
            }
        }
        return list;
    }

    public static class FormElement<T> {

        private String name;
        private T value;
        private String type;

        public FormElement(String name, T value, String type) {
            this.name = name;
            this.value = value;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }

    public static RegistrationComplete map(Registration o) {
        RegistrationComplete ret = new RegistrationComplete();
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                
                Field retField = ret.getClass().getDeclaredField(field.getName());
                retField.setAccessible(true);
                retField.set(ret, field.get(o));
                
            } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
    
    
    
    public static Map<Long,Double> stringToMap(String string){
        Map<Long,Double> ret = new HashMap<>();

        return ret;
    }

}
