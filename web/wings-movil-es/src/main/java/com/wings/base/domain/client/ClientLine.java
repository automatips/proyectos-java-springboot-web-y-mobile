/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.wings.base.domain.payment.PaymentAuthorization;
import com.wings.base.enums.LineStatus;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "client_lines")
public class ClientLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long number;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date activationDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date cancelDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date statusDate;

    private String internalState;
    private String externalState;
    private String iccid;
    private String iccidType;
    private String iccidPuk;
    private Long planId;
    private String planDescription;
    private Long billingAddress;
    private Long clientId;

    private boolean port;
    private String portOperator;
    private String portIccid;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date portPlannedDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date portDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date portLastStatusCheck;
    private String portStatus;
    private String contractNumber;
    private Long registrationId;
    
    private Float riskMargin;
    private Float riskExtension;
    private Float riskMonthPrice;
    private Boolean riskAutoBlock;
    private Boolean riskAutoUnblock;
    

    @ManyToOne(optional = false)
    @JoinColumn(name = "clientId", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonBackReference
    private Client client;

    @ManyToOne(optional = true)
    @JoinColumn(name = "planId", referencedColumnName = "id", insertable = false, updatable = false)
    private Plan plan;

    private boolean contractSigned;
    private String referer;

    @Transient
    private PaymentAuthorization paymentAuthorization;

    @Transient
    private CdrSums cdrSums;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public String getInternalState() {
        return internalState;
    }

    public void setInternalState(String internalState) {
        this.internalState = internalState;
    }

    public String getExternalState() {
        return externalState;
    }

    public void setExternalState(String externalState) {
        this.externalState = externalState;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getIccidType() {
        return iccidType;
    }

    public void setIccidType(String iccidType) {
        this.iccidType = iccidType;
    }

    public String getIccidPuk() {
        return iccidPuk;
    }

    public void setIccidPuk(String iccidPuk) {
        this.iccidPuk = iccidPuk;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public String getPlanDescription() {
        return planDescription;
    }

    public void setPlanDescription(String planDescription) {
        this.planDescription = planDescription;
    }

    public Long getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Long billingAddress) {
        this.billingAddress = billingAddress;
    }

    public boolean isPort() {
        return port;
    }

    public void setPort(boolean port) {
        this.port = port;
    }

    public String getPortIccid() {
        return portIccid;
    }

    public void setPortIccid(String portIccid) {
        this.portIccid = portIccid;
    }

    public Date getPortPlannedDate() {
        return portPlannedDate;
    }

    public void setPortPlannedDate(Date portPlannedDate) {
        this.portPlannedDate = portPlannedDate;
    }

    public Date getPortDate() {
        return portDate;
    }

    public void setPortDate(Date portDate) {
        this.portDate = portDate;
    }

    public Date getPortLastStatusCheck() {
        return portLastStatusCheck;
    }

    public void setPortLastStatusCheck(Date portLastStatusCheck) {
        this.portLastStatusCheck = portLastStatusCheck;
    }

    public String getPortStatus() {
        return portStatus;
    }

    public void setPortStatus(String portStatus) {
        this.portStatus = portStatus;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getClientId() {
        return clientId;
    }

    public String getPortOperator() {
        return portOperator;
    }

    public void setPortOperator(String portOperator) {
        this.portOperator = portOperator;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public boolean isActive() {
        return getInternalState().equals(LineStatus.active.name());
    }

    public boolean isContractSigned() {
        return contractSigned;
    }

    public void setContractSigned(boolean contractSigned) {
        this.contractSigned = contractSigned;
    }

    public void setPaymentAuthorization(PaymentAuthorization paymentAuthorization) {
        this.paymentAuthorization = paymentAuthorization;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    /**
     * Transient field, will return null unless is setted
     *
     * @return
     */
    public PaymentAuthorization getPaymentAuthorization() {
        return paymentAuthorization;
    }

    /**
     * Transient field, will return null unless is setted
     *
     * @return
     */
    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    /**
     * Transient field, will return null unless is setted
     *
     * @return
     */
    public CdrSums getCdrSums() {
        return cdrSums;
    }

    public void setCdrSums(CdrSums cdrSums) {
        this.cdrSums = cdrSums;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    @Override
    public String toString() {
        return getNumber() + "";
    }

    public Float getRiskMargin() {
        return riskMargin;
    }

    public void setRiskMargin(Float riskMargin) {
        this.riskMargin = riskMargin;
    }

    public Float getRiskExtension() {
        return riskExtension;
    }

    public void setRiskExtension(Float riskExtension) {
        this.riskExtension = riskExtension;
    }

    public Float getRiskMonthPrice() {
        return riskMonthPrice;
    }

    public void setRiskMonthPrice(Float riskMonthPrice) {
        this.riskMonthPrice = riskMonthPrice;
    }

    public Boolean getRiskAutoBlock() {
        return riskAutoBlock;
    }

    public void setRiskAutoBlock(Boolean riskAutoBlock) {
        this.riskAutoBlock = riskAutoBlock;
    }

    public Boolean getRiskAutoUnblock() {
        return riskAutoUnblock;
    }

    public void setRiskAutoUnblock(Boolean riskAutoUnblock) {
        this.riskAutoUnblock = riskAutoUnblock;
    }



}
