/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.Balance;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface BalanceRepository extends JpaRepository<Balance, Long>, JpaSpecificationExecutor<Balance> {

    List<Balance> findByLineIdAndType(Long lineId, String type);

    List<Balance> findByYearAndMonthAndType(Integer year, Integer month, String type);

    List<Balance> findByYearAndMonthAndTypeOrderByLineId(Integer year, Integer month, String type);

    List<Balance> findByYearAndMonthAndTypeAndLineId(Integer year, Integer month, String type, Long lineId);

}
