/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.enums;

/**
 *
 * @author seba
 */
public enum ClientActivityType {
    
    client,
    client_operator,
    line,
    line_contract,
    line_order,
    line_operator,
    client_invoice,

}
