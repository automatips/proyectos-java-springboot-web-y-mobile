/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.api.dto;




/**
 *
 * @author seba
 */
public class ServiceItem {

    private Long id;
    private String code;
    private String name;
    private String description;
    private Integer category;
    private Double price;
    private Double rankPoints;
    private Double payPoints;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getRankPoints() {
        return rankPoints;
    }

    public void setRankPoints(Double rankPoints) {
        this.rankPoints = rankPoints;
    }

    public Double getPayPoints() {
        return payPoints;
    }

    public void setPayPoints(Double payPoints) {
        this.payPoints = payPoints;
    }

}
