/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.repos.PlanRepository;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XRiskService;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.FormatUtils;
import com.wings.base.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ClientsService {

    @Autowired
    private XRiskService xRiskService;

    @Autowired
    private XLineService xLineService;

    @Autowired
    private PlanRepository planRepository;

    public String blockAdminClient(Client client, Long planId, String status, boolean riskUnblock) {

        StringBuilder b = new StringBuilder();

        Plan plan = planRepository.findOne(planId);
        List<ClientLine> lines = new ArrayList<>(client.getLines());
        for (ClientLine line : lines) {

            b.append("Linea ").append(line.getId()).append(":");

            if (riskUnblock) {
                b.append(" AutoDesbloqueo=NO");
                xRiskService.updateRisk(client.getRefCustomerId(), line.getNumber().toString(), "K", "30.0");
            } else {
                b.append(" AutoDesbloqueo=SI");
                xRiskService.updateRisk(client.getRefCustomerId(), line.getNumber().toString(), "H", "30.0");
            }

            //xLineService.changeLinePlan(line, plan, DateUtils.getTomorrowDate());
            //b.append(" Plan=").append(plan.getShortName()).append(", cuando=").append(FormatUtils.formatHumanDate(DateUtils.getTomorrowDate()));

            xLineService.setLineStatus(line, status);
            b.append(" Estado=").append(status.equals("S") ? "suspencion" : "call barring");

            b.append(System.lineSeparator());
        }

        return b.toString();
    }

    public String unblockAdminClient(Client client) {

        StringBuilder b = new StringBuilder();

        List<ClientLine> lines = new ArrayList<>(client.getLines());
        for (ClientLine line : lines) {

            b.append("Linea ").append(line.getId()).append(":");

            xRiskService.updateRisk(client.getRefCustomerId(), line.getNumber().toString(), "H", "30.0");

            //Plan plan = planRepository.findOne(line.getPlanId());
            //xLineService.changeLinePlan(line, plan, DateUtils.getTomorrowDate());
            //b.append(" Plan=").append(plan.getShortName()).append(", cuando=").append(FormatUtils.formatHumanDate(DateUtils.getTomorrowDate()));

            xLineService.setLineStatus(line, "R");
            b.append(" Estado=").append("Reactivacion");

            b.append(System.lineSeparator());
        }

        return b.toString();
    }

}
