/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.wings.base.domain.registration.Registration;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "orders")
public class Order {

    public static final String ORDER_TYPE_REGISTRATION = "Alta";
    public static final String ORDER_TYPE_RESEND = "Reenvio";

    public static final int TYPE_PORTA = 1;
    public static final int TYPE_NEW = 2;
    public static final int TYPE_SUS = 3;

    public static final String DESC_PORTA = "MODELO A";
    public static final String DESC_NEW = "MODELO B";
    public static final String DESC_SUS = "MODELO B";

    public static final String REF_PORTA = "SWP";
    public static final String REF_NEW = "SWN";
    public static final String REF_SUS = "SWS";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date sendDate;
    private String status;
    private Long registrationId;
    private Long clientId;
    private int type;
    private String descripcion;
    private String referencia;
    private String telefono;
    private String trackingNumber;
    private String orderStatus;

    private String shippingAddress;
    private String shippingCity;
    private String shippingState;
    private String shippingCp;
    private String upsStoreId;
    private boolean anulada;
        
    private Long lineId;
    private Long parent;
    
    @ManyToOne(optional = true)
    @JoinColumn(name = "clientId", referencedColumnName = "id", insertable = false, updatable = false)
    private Client client;
    
    @ManyToOne(optional = true)
    @JoinColumn(name = "lineId", referencedColumnName = "id", insertable = false, updatable = false)
    private ClientLine line;

    @ManyToOne(optional = true)
    @JoinColumn(name = "registrationId", referencedColumnName = "id", insertable = false, updatable = false)
    private Registration registration;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getClientId() {
        return clientId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingCp() {
        return shippingCp;
    }

    public void setShippingCp(String shippingCp) {
        this.shippingCp = shippingCp;
    }

    public String getUpsStoreId() {
        return upsStoreId;
    }

    public void setUpsStoreId(String upsStoreId) {
        this.upsStoreId = upsStoreId;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    public Long getLineId() {
        return lineId;
    }

    public ClientLine getLine() {
        return line;
    }

    public void setLine(ClientLine line) {
        this.line = line;
    }

    public boolean isAnulada() {
        return anulada;
    }

    public void setAnulada(boolean anulada) {
        this.anulada = anulada;
    }
}
