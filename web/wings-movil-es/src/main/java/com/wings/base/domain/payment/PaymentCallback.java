/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.payment;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * This class is used when tefpay sends callback for
 * payment request
 * @author seba
 */
@Entity
@Table(name = "payment_callback")
public class PaymentCallback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long requestId;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date responseDate;

    /**
     * Key:Ds_Amount
     */
    private String amount;
    /**
     * Key:Ds_Date
     */
    private String date;
    /**
     * Key:Ds_AuthorisationCode
     */
    private String authCode;
    /**
     * Key:Ds_Bank
     */
    private String bank;
    /**
     * Key:Ds_Message
     */
    private String message;
    /**
     * Key:Ds_Code
     */
    private String code;
    /**
     * Key:Ds_Merchant_MatchingData
     */
    private String matchingData;
    /**
     * Key:Ds_Merchant_TransactionType
     */
    private String transactionType;
    /**
     * Key:Ds_PanMask
     */
    private String panMask;
    /**
     * Key:Ds_Expiry
     */
    private String expiry;
    /**
     * Key:Ds_Merchant_Guarantees
     */
    private String guarantees;
    /**
     * Key:Ds_Signature
     */
    private String signature;
    /**
     * Key:Ds_Merchant_MerchantCode
     */
    private String merchantCode;
    /**
     * Key:Ds_Merchant_NumTransaction
     */
    private String numTransaction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMatchingData() {
        return matchingData;
    }

    public void setMatchingData(String matchingData) {
        this.matchingData = matchingData;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPanMask() {
        return panMask;
    }

    public void setPanMask(String panMask) {
        this.panMask = panMask;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getGuarantees() {
        return guarantees;
    }

    public void setGuarantees(String guarantees) {
        this.guarantees = guarantees;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getNumTransaction() {
        return numTransaction;
    }

    public void setNumTransaction(String numTransaction) {
        this.numTransaction = numTransaction;
    }

}
