/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.wings.base.utils.NumberUtils;
import java.math.BigDecimal;

/**
 *
 * @author seba
 */
public class CdrSums {

    private Long voice;
    private Long data;
    private Double price;
    private Long calls;
    private Long sms;
    private Long records;

    public CdrSums(Long voice, Long data, Double price, Long calls, Long sms,Long records) {

        this.voice = voice != null ? voice : 0;
        this.data = data != null ? data : 0;
        this.price = price != null ? price : 0;
        this.calls = calls != null ? calls : 0;
        this.sms = sms != null ? sms : 0;
        this.records = records != null ? records : 0;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getVoice() {
        return voice;
    }

    public void setVoice(Long voice) {
        this.voice = voice;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    public Long getCalls() {
        return calls;
    }

    public void setCalls(Long calls) {
        this.calls = calls;
    }

    public Long getSms() {
        return sms;
    }

    public void setSms(Long sms) {
        this.sms = sms;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public Long getRecords() {
        return records;
    }
    
    public String getDataText() {
        return NumberUtils.formatData(data);
    }

    public String getPriceText() {
        return NumberUtils.formatMoney(price);
    }

    public double getPriceRounded() {
        return NumberUtils.round(price, 4);
    }
    
    public String getVoiceText(){
        return String.valueOf(NumberUtils.round(voice/60d, 4));
    }

    
}
