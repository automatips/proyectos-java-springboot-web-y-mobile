/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.invoice.Invoice;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long>, JpaSpecificationExecutor<Invoice> {

    List<Invoice> findByMonthAndYear(int month, int year);

    List<Invoice> findByMonthAndYearAndClientId(int month, int year, Long clientId);

    Invoice findFirstByMonthAndYearAndClientId(int month, int year, Long clientId);

    List<Invoice> findByClientId(Long clientId);

    Invoice findByToken(String token);

    Invoice findByEcode(String ecode);

    List<Invoice> findByClientIdInAndStatus(List<Long> clientIds, String status);
    
    List<Invoice> findByClientIdInAndStatusIn(List<Long> clientIds, List<String> status);
    
    List<Invoice> findByClientIdAndStatus(Long clientId, String status);

    List<Invoice> findByStatus(String status);

    @Query(value = "select IFNULL(MAX(period_number), 0) from invoices where year = ?1", nativeQuery = true)
    long getMaxPeriodNumber(int year);
}
