/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.auth;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author seba
 */
public class WingsAutheticatorProvider implements AuthenticationProvider {
    
    private final WingsUsersDetailsService wingsUsersDetailsService;

    public WingsAutheticatorProvider(WingsUsersDetailsService wingsUsersDetailsService) {
        this.wingsUsersDetailsService = wingsUsersDetailsService;
    }
    
    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {

        String cred = DigestUtils.sha256Hex(a.getCredentials().toString());        
        UserDetails userDetails = wingsUsersDetailsService.loadUserByUsername(a.getName());
        if (userDetails != null && cred.equals(userDetails.getPassword())) {
            return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        }

        return null;
    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(UsernamePasswordAuthenticationToken.class);
    }

}
