/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.client.BillingAddress;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.registration.PortLine;
import com.wings.base.domain.registration.RegistrationComplete;
import com.wings.base.enums.LineStatus;
import com.wings.base.repos.BillingAddressRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.RegistrationCompleteRepository;
import com.wings.base.utils.DniUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * Service use for managing registrations
 *
 * @author seba
 */
@Component
public class RegistrationService {

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private BillingAddressRepository billingAddressRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PortLinesRepository portLinesRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private RegistrationCompleteRepository completeRepository;

    /**
     * Create clients from registrations completed
     * @param completes
     */
    public void createClientsFromRegistrations(List<RegistrationComplete> completes) {

        for (RegistrationComplete reg : completes) {

            Client client = clientsRepository.findOneByIdValue(reg.getNroDoc());
            //el cliente no existe
            if (client == null) {
                client = new Client();

                client.setName(WordUtils.capitalizeFully(reg.getNombre()));
                client.setLastname(WordUtils.capitalizeFully(reg.getApellido()));
                client.setMobile(reg.getTelefono());
                client.setPhone(reg.getTelefono());
                client.setIdType(DniUtil.getDniString(reg.getTipoDoc()));
                client.setIdValue(StringUtils.upperCase(reg.getNroDoc()));
                client.setBirthDate(reg.getFechaNac());

                client.setAddress(WordUtils.capitalizeFully(reg.getDireccion()));
                client.setCity(WordUtils.capitalizeFully(reg.getCiudad(), new char[]{'´', '\''}));
                client.setState(WordUtils.capitalizeFully(reg.getProvincia()));
                client.setContract(reg.getPathContrato());
                client.setCountry(reg.getPais());
                client.setNationality(WordUtils.capitalizeFully(reg.getNacionalidad()));
                client.setCreationDate(new Date());
                client.setEmail(reg.getEmail());
                client.setPostalCode(reg.getCp());

                client.setCompanyName(WordUtils.capitalizeFully(reg.getRazonSocial()));
                client.setCompanyIdType(DniUtil.getDniString(reg.getTipoDocEmpresa()));
                client.setCompanyIdValue(StringUtils.upperCase(reg.getNroDocEmpresa()));

                client.setReferer(reg.getReferer());
                client.setRegistrationId(reg.getId());

                clientsRepository.save(client);

                //ret.append("Cliente ").append(client).append(" creado").append("<br/>");
                //createdClients++;
            } else {
                //ret.append("El cliente ").append(client).append(" ya existe, continuando proceso...").append("<br/>");
            }

            //busco las direcciones de facturacion
            List<BillingAddress> addresses = billingAddressRepository.findByClientId(client.getId());

            BillingAddress toUse = new BillingAddress();
            toUse.setAddress(reg.getDireccion());
            toUse.setCity(reg.getCiudad());
            toUse.setClientId(client.getId());
            toUse.setCountry(reg.getPais());
            toUse.setCp(reg.getCp());
            toUse.setState(reg.getProvincia());

            //si esta vacia la creo
            if (addresses.isEmpty() || !addresses.contains(toUse)) {
                billingAddressRepository.save(toUse);
            } else {
                for (BillingAddress a : addresses) {
                    if (a.equals(toUse)) {
                        toUse = a;
                        break;
                    }
                }
            }

            Plan plan = planRepository.findByShortName(reg.getPlan());

            List<ClientLine> clientLines = new ArrayList<>();
            //creo las lineas a portar del cliente
            int porta = reg.getCantLineasPorta();
            if (porta > 0) {
                List<PortLine> portLines = portLinesRepository.findByRegistrationId(reg.getId());
                for (PortLine portLine : portLines) {

                    ClientLine cl = linesRepository.findByNumber(Long.valueOf(portLine.getPortNumber()));
                    if (cl != null) {
                        //ret.append("La linea ").append(portLine.getPortNumber()).append(" ya existe, continuando proceso...").append("<br/>");
                        continue;
                    }

                    ClientLine line = new ClientLine();
                    line.setClientId(client.getId());
                    line.setRegistrationId(reg.getId());
                    line.setCreationDate(new Date());
                    line.setInternalState(LineStatus.created_local.name());
                    line.setNumber(Long.valueOf(portLine.getPortNumber()));
                    line.setPlanDescription(plan.getDescription());
                    line.setPlanId(plan.getId());
                    line.setPort(true);
                    line.setPortPlannedDate(portLine.getPortDate());
                    line.setPortIccid(portLine.getIccid());
                    line.setPortOperator(portLine.getPortOperator());
                    line.setBillingAddress(toUse.getId());

                    clientLines.add(line);
                    //createdLines++;
                    //ret.append("Linea ").append(portLine.getPortNumber()).append(" creada ").append("<br/>");
                    portLine.setStatus("processed");
                }
                //update line status
                portLinesRepository.save(portLines);
            }

            //creo las lineas nuevas
            int nuevas = reg.getCantLineas();
            List<ClientLine> alreadyCreatedLines = linesRepository.findByRegistrationId(reg.getId());
            if (alreadyCreatedLines != null && !alreadyCreatedLines.isEmpty()) {
                if (alreadyCreatedLines.size() >= nuevas) {
                    //ret.append("Las lineas ya estan creadas para ese registro").append("<br/>");
                    nuevas = 0;
                } else {
                    nuevas = nuevas - alreadyCreatedLines.size();
                    //ret.append("Algunas lineas ya estan creadas para ese registro, lineas a crear ").append(nuevas).append("<br/>");
                }
            }

            for (int i = 0; i < nuevas; i++) {

                ClientLine line = new ClientLine();
                line.setClientId(client.getId());
                line.setCreationDate(new Date());
                line.setInternalState(LineStatus.created_local.name());
                line.setPlanDescription(plan.getDescription());
                line.setPlanId(plan.getId());
                line.setPort(false);
                line.setBillingAddress(toUse.getId());
                line.setRegistrationId(reg.getId());

                //createdLines++;
                //ret.append("Linea creada ").append("<br/>");
                clientLines.add(line);
            }

            linesRepository.save(clientLines);
            reg.setStatus("processed");
            completeRepository.save(reg);
        }
    }

}
