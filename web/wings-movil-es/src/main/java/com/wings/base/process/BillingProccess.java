/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.process;

import com.wings.base.WingsProperties;
import com.wings.base.dao.BalanceDAO;
import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.dao.specs.CdrSpecs;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.Cdr;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.invoice.InvoiceBalanceWrapper;
import com.wings.base.domain.invoice.InvoiceCdrWrapper;
import com.wings.base.domain.invoice.InvoiceItem;
import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationCallback;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.CdrRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.InvoiceItemsRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.PaymentAuthorizationRepository;
import com.wings.base.repos.PaymentRequestRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.RegistrationCallbackRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.service.BillingService;
import com.wings.base.service.InvoiceService;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.InvoiceUtils;
import com.wings.base.utils.NumberUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Component
public class BillingProccess {

    private final Logger log = LoggerFactory.getLogger(BillingProccess.class);

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationCallbackRepository registrationCallbackRepository;

    @Autowired
    private CdrRepository cdrRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private InvoiceItemsRepository invoiceItemsRepository;

    @Autowired
    private BillingService billingService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private BalanceDAO balanceDAO;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentAuthorizationRepository paymentAuthorizationRepository;

    private final WingsProperties properties;

    private final SystemStatus status = new SystemStatus();

    public BillingProccess(WingsProperties properties) {
        this.properties = properties;
        status.setPercent(0);
        status.setProcess("Ninguno");
        status.setStatus("Inactivo");
    }

    /**
     * For every line add item to balance acording to plan and activation date
     *
     * @param month - month number to proccess balance
     * @param year - year of the month
     * @return String - response text
     */
    public String processPlanMonthly(int month, int year) {

        status.setProcess("Generador balance " + month + "/" + year);
        status.setStatus("Ejecutando");

        StringBuilder bf = new StringBuilder();
        int activeLines = 0;
        int balanceItems = 0;
        int duplicatedItems = 0;

        SimpleDateFormat sdf = new SimpleDateFormat("MMMM", new Locale("es", "ES"));

        Date from = DateUtils.getStartDateForPeriod(month, year);
        Date to = DateUtils.getEndDateForPeriod(month, year);

        Map<Long, Plan> planCache = new HashMap<>();
        List<ClientLine> lines = linesRepository.findAll();
        int percent = lines.size() / 100;
        int processed = 0;

        for (ClientLine line : lines) {

            status.setPercent(processed++ / percent);

            //si no estan activas
            if (line.getActivationDate() == null) {
                log.info("Line with activation date null " + line.getId());
                continue;
            }

            //si estan activas despues de la fecha de fin de periodo
            if (line.getActivationDate().getTime() > to.getTime()) {
                log.info("Line with activation date bigger than to " + line.getId());
                log.info(line.getActivationDate() + " < " + to);
                continue;
            }

            if (line.getCancelDate() != null) {

                long cancelTime = line.getCancelDate().getTime();
                //si esta cancelada, pero se genera el balance del mes que se cancelo
                if (cancelTime > from.getTime() && cancelTime < to.getTime()) {
                    log.info("Line canceled, last invoice " + line.getCancelDate());
                } else {
                    log.info("Line canceled, not balance for " + month + "/" + year + " -> " + line.getCancelDate());
                    continue;
                }
            }

            activeLines++;

            Long clientId = line.getClientId();
            Long planId = line.getPlanId();

            Plan plan = planCache.get(planId);
            if (plan == null) {
                plan = planRepository.findOne(planId);
                planCache.put(planId, plan);
            }

            //Plan
            Balance b = new Balance();
            b.setAmount(plan.getPrice());
            b.setClientId(clientId);
            b.setDescription(StringUtils.capitalize(sdf.format(from)) + " - " + plan.getDescription());
            b.setObservation("Item automatico, proceso mensual linea " + line.getNumber());
            b.setLineId(line.getId());
            b.setControl(plan.getShortName() + "-" + month + "-" + year);
            b.setMonth(month);
            b.setYear(year);
            b.setType(BalanceType.plan.name());

            try {
                balanceRepository.save(b);
                balanceItems++;
            } catch (DataIntegrityViolationException e) {
                duplicatedItems++;
            }

            //nueva activacion
            if (line.getActivationDate().getTime() > from.getTime()
                    && line.getActivationDate().getTime() < to.getTime()) {

                //prorrateo
                long days = DateUtils.getDiffDays(from, line.getActivationDate());
                String dias = " días";
                if (days == 1) {
                    dias = " día";
                }

                if (days > 0) {
                    double discount = (days * plan.getPrice()) / 30;
                    discount = NumberUtils.round(discount * -1, 2);

                    b = new Balance();
                    b.setAmount(discount);
                    b.setClientId(clientId);
                    b.setDescription("Descuento período no utilizado " + days + dias);
                    b.setObservation("Item automatico prorrateo, proceso mensual linea " + line.getNumber());
                    b.setLineId(line.getId());
                    b.setControl("proporcional");
                    b.setPaymentMethod("system");
                    b.setMonth(month);
                    b.setYear(year);
                    b.setType(BalanceType.discount.name());
                    try {
                        balanceRepository.save(b);
                        balanceItems++;
                    } catch (DataIntegrityViolationException e) {
                        duplicatedItems++;
                    }
                }

                //envio
                b = new Balance();
                b.setAmount(3.9);
                b.setClientId(clientId);
                b.setDescription("Gastos admin. alta linea");
                b.setObservation("Gastos de envio monto por unica vez " + line.getNumber());
                b.setLineId(line.getId());
                b.setControl("envio-sim");
                b.setMonth(month);
                b.setYear(year);
                b.setType(BalanceType.extra.name());

                try {
                    balanceRepository.save(b);
                    balanceItems++;
                } catch (DataIntegrityViolationException e) {
                    duplicatedItems++;
                }

                //seguro                
                Long regId = line.getRegistrationId();
                Registration reg = registrationRepository.findOne(regId);

                if (reg.getScoring().equals("B") || reg.getScoring().equals("BB")) {

                    String matchingData = reg.getMatchingData();
                    RegistrationCallback callBack = registrationCallbackRepository.findByMatchingDataAndMessage(matchingData, "Aceptada");

                    b = new Balance();
                    /*double seguro = plan.getPrice() * 2;
                    if (seguro < 20) {
                        seguro = 20.0d;
                    }*/

                    double pagado = (Double.valueOf(callBack.getAmount()) / 100);
                    b.setAmount(pagado);
                    b.setClientId(clientId);
                    b.setDescription("Cuotas anticipadas");
                    b.setObservation("Seguro por scoring, unica vez " + line.getNumber());
                    b.setLineId(line.getId());
                    b.setControl("seguro");
                    b.setMonth(month);
                    b.setYear(year);
                    b.setType(BalanceType.extra.name());
                    try {
                        balanceRepository.save(b);
                        balanceItems++;
                    } catch (DataIntegrityViolationException e) {
                        duplicatedItems++;
                    }

                    if (!callBack.getTransactionType().equals("1")) {
                        log.error("WARNING: la transaccion no es de cobro, solo de autenticacion");
                        bf.append("<br>ATENCION: Pago de seguro con transaccion solo de autenticacion");
                    }

                    //pago del seguro 
                    if (callBack.getMessage().equals("Aceptada")) {
                        b = new Balance();
                        b.setAmount((Double.valueOf(callBack.getAmount()) / 100) * -1);
                        b.setClientId(clientId);
                        b.setDescription("Pago cuotas anticipadas");
                        b.setObservation("Pago Seguro por scoring, unica vez " + line.getNumber());
                        b.setLineId(line.getId());
                        b.setControl("pago-seguro");
                        b.setPaymentMethod("credit-card");
                        b.setPaymentRef(matchingData);
                        b.setMonth(month);
                        b.setYear(year);
                        b.setType(BalanceType.discount.name());
                        try {
                            balanceRepository.save(b);
                            balanceItems++;
                        } catch (DataIntegrityViolationException e) {
                            duplicatedItems++;
                        }
                    }
                }
            }

            //Consumos
            List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(line.getClient().getRefCustomerId(), from, to, null, null, line.getNumber().toString()));
            double totalDatos = 0;
            double totalVoz = 0;
            double totalSms = 0;

            for (Cdr cdr : cdrs) {
                switch (cdr.getCdrType()) {
                    case "DAT":
                        totalDatos += cdr.getPrice().doubleValue();
                        break;
                    case "VOZ":
                        totalVoz += cdr.getPrice().doubleValue();
                        break;
                    default:
                        totalSms += cdr.getPrice().doubleValue();
                        break;
                }
            }

            b = new Balance();
            b.setAmount(NumberUtils.round(totalDatos, 4));
            b.setClientId(clientId);
            b.setDescription("Datos");
            b.setObservation("Datos período " + line.getNumber());
            b.setLineId(line.getId());
            b.setControl("datos" + "-" + month + "-" + year);
            b.setMonth(month);
            b.setYear(year);
            b.setType(BalanceType.consume.name());
            try {
                balanceRepository.save(b);
                balanceItems++;
            } catch (DataIntegrityViolationException e) {
                duplicatedItems++;
            }

            b = new Balance();
            b.setAmount(NumberUtils.round(totalVoz, 4));
            b.setClientId(clientId);
            b.setDescription("Voz");
            b.setObservation("Voz período " + line.getNumber());
            b.setLineId(line.getId());
            b.setControl("voz" + "-" + month + "-" + year);
            b.setMonth(month);
            b.setYear(year);
            b.setType(BalanceType.consume.name());
            try {
                balanceRepository.save(b);
                balanceItems++;
            } catch (DataIntegrityViolationException e) {
                duplicatedItems++;
            }

            b = new Balance();
            b.setAmount(NumberUtils.round(totalSms, 4));
            b.setClientId(clientId);
            b.setDescription("SMS");
            b.setObservation("SMS período " + line.getNumber());
            b.setLineId(line.getId());
            b.setControl("sms" + "-" + month + "-" + year);
            b.setMonth(month);
            b.setYear(year);
            b.setType(BalanceType.consume.name());
            try {
                balanceRepository.save(b);
                balanceItems++;
            } catch (DataIntegrityViolationException e) {
                duplicatedItems++;
            }
        }

        status.setStatus("Terminado");
        status.setPercent(100);
        cleanUpStatus();

        bf.append("<br/><strong>Proceso finalizado</strong>");
        bf.append("<br/>Lineas totales:").append(lines.size());
        bf.append("<br/>Lineas activas:").append(activeLines);
        bf.append("<br/>Registros creados:").append(balanceItems);
        bf.append("<br/>Registros ya existentes:").append(duplicatedItems);

        return bf.toString();
    }

    public String createInvoices(int month, int year) {

        status.setProcess("Generador facturas " + month + "/" + year);
        status.setStatus("Ejecutando");

        List<Invoice> toSave = new ArrayList<>();
        List<Client> clients = clientsRepository.findAll();

        int percent = clients.size() / 100;
        int processed = 0;

        for (Client client : clients) {

            status.setPercent(processed++ / percent);

            List<Invoice> existing = invoiceRepository.findByMonthAndYearAndClientId(month, year, client.getId());
            if (!existing.isEmpty()) {
                continue;
            }

            List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(client.getId().toString(), null, String.valueOf(month), String.valueOf(year)));
            if (balances.isEmpty()) {
                continue;
            }

            Invoice invoice = new Invoice();
            invoiceRepository.save(invoice);

            invoice.setClient(client);
            invoice.setClientId(client.getId());

            String clientName = client.getName() + " " + client.getLastname();
            String clientAddress = client.getAddress();
            String clientCity = client.getCity();
            String clientState = client.getState();
            String clientDni = client.getIdType() + " " + client.getIdValue();
            if (client.getCompanyName() != null && !client.getCompanyName().isEmpty()) {
                clientName = client.getCompanyName();
                clientDni = client.getCompanyIdType() + " " + client.getCompanyIdValue();
            }
            invoice.setClientName(clientName);
            invoice.setClientAddress(clientAddress);
            invoice.setClientCity(clientCity);
            invoice.setClientState(clientState);
            invoice.setClientDni(clientDni);

            String ecode = InvoiceUtils.getEcode(client.getId(), month, year);

            //check ecode
            Invoice temp = invoiceRepository.findByEcode(ecode);
            while (temp != null) {
                ecode = InvoiceUtils.getEcode(client.getId(), month, year);
                temp = invoiceRepository.findByEcode(ecode);
            }

            invoice.setEcode(ecode);
            invoice.setToken(InvoiceUtils.getInvoiceToken(ecode));

            Date invoiceDate = new Date();
            Date expireDate = new Date(invoiceDate.getTime() + 864000000l);
            invoice.setExpireDate(expireDate);
            invoice.setInvoiceDate(invoiceDate);

            invoice.setMonth(month);
            invoice.setYear(year);

            long number = invoiceRepository.getMaxPeriodNumber(year);
            number++;
            invoice.setPeriodNumber(number);
            invoice.setInvoiceNumber(InvoiceUtils.getInvoiceNumber(number, year));
            invoiceRepository.save(invoice);

            Map<String, InvoiceItem> items = new HashMap<>();
            double grandTotal = 0.0d;
            for (Balance balance : balances) {

                grandTotal += balance.getAmount();

                if (items.containsKey(balance.getType())) {
                    InvoiceItem item = items.get(balance.getType());
                    item.setAmount(item.getAmount() + balance.getAmount());
                } else {
                    InvoiceItem item = new InvoiceItem();
                    String concept = "";
                    String type = balance.getType();
                    int order = 0;
                    if (type.equals(BalanceType.plan.name())) {
                        concept = "Cuotas";
                        order = 1;
                    } else if (type.equals(BalanceType.discount.name())) {
                        concept = "Descuentos y promociones";
                        order = 4;
                    } else if (type.equals(BalanceType.extra.name())) {
                        concept = "Otros cargos";
                        order = 3;
                    } else if (type.equals(BalanceType.consume.name())) {
                        concept = "Consumos";
                        order = 2;
                    }

                    item.setOrder(order);
                    item.setConcept(concept);
                    item.setAmount(balance.getAmount());
                    item.setInvoiceId(invoice.getId());
                    items.put(balance.getType(), item);
                }
            }

            List<InvoiceItem> toSort = new LinkedList<>(items.values());
            Collections.sort(toSort, (InvoiceItem o1, InvoiceItem o2) -> o1.getOrder().compareTo(o2.getOrder()));

            invoice.setItems(toSort);
            invoiceItemsRepository.save(invoice.getItems());
            invoice.setTotal(grandTotal);
            invoice.setStatus(InvoiceStatus.unpayed.name());

            toSave.add(invoice);
        }

        status.setPercent(100);
        status.setProcess("Generando pdf");
        invoiceRepository.save(toSave);

        percent = toSave.size() / 100;
        if (percent == 0) {
            percent = 1;
        }
        processed = 0;

        //generate invoice PDFs
        for (Invoice invoice : toSave) {

            status.setPercent(processed++ / percent);

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
            Date from = DateUtils.getStartDateForPeriod(month, year);
            Date to = DateUtils.getEndDateForPeriod(month, year);

            Client c = invoice.getClient();
            Collection<ClientLine> lines = c.getLines();
            List<InvoiceBalanceWrapper> balancesWrappers = new ArrayList<>();
            List<InvoiceCdrWrapper> cdrWrappers = new ArrayList<>();

            for (ClientLine line : lines) {
                List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(c.getId().toString(), line.getId().toString(), String.valueOf(month), String.valueOf(year)), new Sort(Sort.Direction.ASC, "id"));
                if (balances.isEmpty()) {
                    continue;
                }
                String title = line.getNumber().toString() + " (fecha activación: " + sdf.format(line.getActivationDate()) + ")";
                balancesWrappers.add(new InvoiceBalanceWrapper(title, balances));
                List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(null, from, to, null, null, line.getNumber().toString()), new Sort(Sort.Direction.ASC, "date", "time"));
                cdrWrappers.add(new InvoiceCdrWrapper(line.getNumber().toString(), cdrs));
            }

            String invoicePath = invoiceService.generateInvoicePDF(invoice, balancesWrappers, cdrWrappers);
            if (invoicePath != null && !invoicePath.isEmpty()) {
                invoice.setFilePath(invoicePath);
            }
        }

        //save again for file path
        status.setPercent(100);
        invoiceRepository.save(toSave);
        status.setStatus("Terminado");
        cleanUpStatus();

        return "Facturas generadas " + toSave.size();

    }

    public void proccessPayments(int month, int year) {

        status.setProcess("Procensando pagos " + month + "/" + year);
        status.setStatus("Ejecutando");

        List<Invoice> invoices = invoiceRepository.findByMonthAndYear(month, year);
        billingService.processPaymentForInvoices("system", invoices, status);
        invoiceRepository.save(invoices);

        status.setPercent(100);
        status.setStatus("Terminado");
        cleanUpStatus();

    }

    @Transactional
    public void regenerateInvoices() {

        List<Invoice> invoices = invoiceRepository.findAll();

        //generate invoice PDFs
        for (Invoice invoice : invoices) {

            /*if(invoice.getFilePath() != null){
                continue;
            }*/
            Date from = DateUtils.getStartDateForPeriod(invoice.getMonth(), invoice.getYear());
            Date to = DateUtils.getEndDateForPeriod(invoice.getMonth(), invoice.getYear());

            //invoice.setInvoiceNumber(InvoiceUtils.getInvoiceNumber(invoice.getId()));
            Client c = invoice.getClient();
            Collection<ClientLine> lines = c.getLines();
            List<InvoiceBalanceWrapper> balancesWrappers = new ArrayList<>();
            List<InvoiceCdrWrapper> cdrWrappers = new ArrayList<>();

            for (ClientLine line : lines) {
                List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(c.getId().toString(), line.getId().toString(), String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())), new Sort(Sort.Direction.ASC, "id"));
                if (balances.isEmpty()) {
                    continue;
                }

                Iterator<Balance> iterator = balances.iterator();
                while (iterator.hasNext()) {
                    Balance next = iterator.next();
                    if (next.getControl().contains("auto-payment-")) {
                        iterator.remove();
                    }
                }

                balancesWrappers.add(new InvoiceBalanceWrapper(line.getNumber().toString(), balances));

                List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(null, from, to, null, null, line.getNumber().toString()), new Sort(Sort.Direction.ASC, "date", "time"));
                cdrWrappers.add(new InvoiceCdrWrapper(line.getNumber().toString(), cdrs));
            }

            String invoicePath = invoiceService.generateInvoicePDF(invoice, balancesWrappers, cdrWrappers);
            if (invoicePath != null && !invoicePath.isEmpty()) {
                invoice.setFilePath(invoicePath);
            }
        }

        invoiceRepository.save(invoices);

    }

    public void arreglarError() {

        Map<String, Double> map = new HashMap<>();
        map.put("551311111111111111000", 23.82);
        map.put("238311111111111111000", 25.48);
        map.put("240311111111111111000", 13.03);
        map.put("264311111111111111000", 1.22);
        map.put("114041111111111111000", 8.31);
        map.put("332311111111111111000", 23.97);
        map.put("436311111111111111000", 23.74);
        map.put("437311111111111111000", 60.49);

    }

    public SystemStatus getStatus() {
        return status;
    }

    private void cleanUpStatus() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                status.setPercent(0);
                status.setProcess("Ninguno");
                status.setStatus("Inactivo");
            }
        }, 3000);
    }
}
