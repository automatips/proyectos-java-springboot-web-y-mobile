/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.process;

import com.wings.base.domain.ContractStatusResponse;
import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.BillingAddress;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.client.Order;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.payment.PaymentAuthorization;
import com.wings.base.domain.registration.PortLine;
import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationCallback;
import com.wings.base.domain.registration.RegistrationComplete;
import com.wings.base.domain.registration.RegistrationFile;
import com.wings.base.enums.ContractStatus;
import com.wings.base.enums.LineStatus;
import com.wings.base.enums.OrderStatus;
import com.wings.base.enums.RegistrationStatus;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.BillingAddressRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.ContractsRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.PaymentAuthorizationRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.RegistrationCallbackRepository;
import com.wings.base.repos.RegistrationCompleteRepository;
import com.wings.base.repos.RegistrationFilesRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.repos.TicketsRepository;
import com.wings.base.service.ClientActivityService;
import com.wings.base.service.ContractService;
import com.wings.base.service.ContractSignatureService;
import com.wings.base.service.EmailService;
import com.wings.base.service.OrdersService;
import com.wings.base.service.ProcessLogService;
import com.wings.base.service.XtraService;
import com.wings.base.service.xtra.PortOperators;
import com.wings.base.service.xtra.XClientService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.utils.DniUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class LineProcess {

    private final Logger log = LoggerFactory.getLogger(LineProcess.class);

    @Autowired
    private RegistrationCompleteRepository registrationCompleteRepository;

    @Autowired
    private RegistrationFilesRepository registrationFilesRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationCallbackRepository registrationCallbackRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private PortLinesRepository portLinesRepository;

    @Autowired
    private PlanRepository plansRepository;

    @Autowired
    private BillingAddressRepository billingAddressRepository;

    @Autowired
    private LinesRepository lineRepository;

    @Autowired
    private TicketsRepository ticketsRepository;

    @Autowired
    private PaymentAuthorizationRepository paymentAuthorizationRepository;

    @Autowired
    private ContractsRepository contractsRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private ContractSignatureService contractSignatureService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private XClientService xClientService;

    @Autowired
    private XLineService xLineService;

    @Autowired
    private XPortService xPortService;

    @Autowired
    private XtraService xtraService;

    @Autowired
    private ClientActivityService clientActivityService;

    @Autowired
    private PortOperators portOperators;

    private final String launcher = "system";

    //10 minutos, 1 hora
    //@Scheduled(initialDelay = 600000l, fixedDelay = 3600000l)
    public void autoProcess() {

        verifyRegistrations();

        createClients();

        createContracts();

        sendContracts();

        checkContractStatus();

        createOrders();

        sendOrders();

        //entre estos dos estados corren los procesos de ups
        createOperator();

        //despues de esto, corren los procesos de control de estado
        //en el operador y actualizan las lineas
    }

    public void verifyRegistrations() {
        verifyRegistrations(launcher);
    }

    /**
     * Dummy process to test long running threads
     *
     * @param launcher
     */
    public synchronized void dummyProcess(String launcher) {
        for (int i = 0; i < 10; i++) {
            try {
                System.out.println("running and waitng");
                wait(5000);
                System.out.println("done waiting");
            } catch (Exception e) {
                System.out.println("Error waiting");
                e.printStackTrace();
            }
        }
        System.out.println("done dummy");
    }

    public void verifyRegistrations(String launcher) {

        List<Ticket> tickets = new ArrayList<>();
        //Solo las que tienen estado payment done
        ProcessLog pl = processLogService.createProcess(launcher, "Verificar Registraciones");
        List<RegistrationComplete> complete = registrationCompleteRepository.findByStatus(RegistrationStatus.payment_done.name());

        pl.appendLine("Verificaciones a realizar " + complete.size());

        pl.setMax(complete.size());
        for (RegistrationComplete rgc : complete) {

            //comparo con otras registraciones
            for (RegistrationComplete rgc2 : complete) {

                //same registration, continue
                if (rgc.getId().equals(rgc2.getId())) {
                    continue;
                }

                //si tiene el mismo nombre y apellido
                if (compareEquals(rgc.getNombre(), rgc2.getNombre())
                        && compareEquals(rgc.getApellido(), rgc2.getApellido())) {

                    //tienen que tener el mismo dni
                    if (!compareEquals(rgc.getNroDoc(), rgc2.getNroDoc())) {
                        //si no marco las dos como invalidas
                        rgc.setStatus(RegistrationStatus.review.name());
                        rgc2.setStatus(RegistrationStatus.review.name());

                        tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Mismo Nombre y Apellido, pero distinto DNI", "system", "CSLEVEL1"));
                        tickets.add(new Ticket(TicketType.registration.name(), rgc2.getId(), "Mismo Nombre y Apellido, pero distinto DNI", "system", "CSLEVEL1"));
                        //razon, mismo nombre distinto dni                        
                    }
                }

                //al revez
                //Si tienen el mismo dni
                if (compareEquals(rgc.getNroDoc(), rgc2.getNroDoc())) {
                    //tienen que tener el mismo nombre y apellido
                    if (!(compareEquals(rgc.getNombre(), rgc2.getNombre())
                            && compareEquals(rgc.getApellido(), rgc2.getApellido()))) {
                        //si no marco las dos como invalidas
                        rgc.setStatus(RegistrationStatus.review.name());
                        rgc2.setStatus(RegistrationStatus.review.name());

                        tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Mismo DNI, pero distinto Nombre y Apellido", "system", "CSLEVEL1"));
                        tickets.add(new Ticket(TicketType.registration.name(), rgc2.getId(), "Mismo DNI, pero distinto Nombre y Apellido", "system", "CSLEVEL1"));
                        //razon, mismo dni distinto nombre
                    }
                }

                //si es portabilidad comparo con los otros 
                //para ver que no exista un pedido de portabilidad 
                //para el mismo nro
                if (rgc.isPorta()) {

                    if (rgc2.getNroPorta() != null && compareEquals(rgc.getNroPorta(), rgc2.getNroPorta())) {
                        //si no marco las dos como invalidas
                        rgc.setStatus(RegistrationStatus.review.name());
                        rgc2.setStatus(RegistrationStatus.review.name());

                        tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Mismo nro a portar", "system", "CSLEVEL1"));
                        tickets.add(new Ticket(TicketType.registration.name(), rgc2.getId(), "Mismo nro a portar", "system", "CSLEVEL1"));
                    }
                }
                registrationCompleteRepository.save(rgc2);
            }//loop interno

            //para la del loop de afuera
            //TODO: verificar el email
            //verifico si es portabilidad que no exista el nro en la base
            if (rgc.isPorta()) {
                ClientLine cl = lineRepository.findByNumber(Long.valueOf(rgc.getNroPorta()));
                if (cl != null) {
                    rgc.setStatus(RegistrationStatus.review.name());
                    //razon, ya existe el nro en la base de datos
                    tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Es Portabilidad y el nro ya existe para un cliente", "system", "CSLEVEL1"));
                }
            }

            //verifico el dni contra la base de datos
            //que el nombre y apellido sea el mismo
            String nroDoc = rgc.getNroDoc();
            Client client = clientsRepository.findOneByIdValue(nroDoc);
            if (client != null) {
                //tienen que tener el mismo nombre y apellido
                if (!(compareEquals(rgc.getNombre(), client.getName())
                        && compareEquals(rgc.getApellido(), client.getLastname()))) {
                    //si no marco las como invalida
                    rgc.setStatus(RegistrationStatus.review.name());
                    //razon, mismo dni distinto nombre
                    tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Existe un cliente con ese DNI pero distinto nombre", "system", "CSLEVEL1"));
                }
            }

            //verifico si es empresa que el dni sea diferente del titular
            if (rgc.isEmpresa() && rgc.getNroDoc().equalsIgnoreCase(rgc.getNroDocEmpresa())) {
                rgc.setStatus(RegistrationStatus.review.name());
                tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Mismo dni para titular y empresa", "system", "CSLEVEL1"));
            }

            //no deberia pasar nunca!
            //verifico si es prepago que tenga el iccid
            if (rgc.isPortaPrepago()) {
                if (rgc.getIccid() == null) {
                    rgc.setStatus(RegistrationStatus.review.name());
                    //razon, falta el iccid para la portabilidad
                    tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Iccid faltante para portabilidad prepago", "system", "CSLEVEL1"));
                }
            }

            if (rgc.getApellido().split(" ").length <= 1) {
                rgc.setStatus(RegistrationStatus.review.name());
                //tiene un solo apellido
                tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Un solo apellido", "system", "CSLEVEL1"));
            }

            //no deberia pasar nunca! pero paso 2 veces
            //verifico si el operador donante no es null
            if (rgc.isPorta() && (rgc.getOperadorPorta() == null || rgc.getOperadorPorta().isEmpty())) {

                //try to get Operator id
                boolean fixed = false;
                try {
                    String numero = rgc.getNroPorta();
                    String idOperator = portOperators.getOperator(numero);
                    if (idOperator != null && !idOperator.isEmpty()) {
                        rgc.setOperadorPorta(idOperator);
                        registrationCompleteRepository.save(rgc);
                        fixed = true;
                    }
                } catch (Exception e) {
                }

                if (!fixed) {
                    rgc.setStatus(RegistrationStatus.review.name());
                    //razon, falta el operator para la portabilidad
                    tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Operador donante no existe", "system", "CSLEVEL1"));
                }
            }

            //no deberia pasar nunca! pero paso una vez
            //verifico que el matching data no sea nulo
            if (rgc.getMatchingData() == null || rgc.getMatchingData().isEmpty()) {
                rgc.setStatus(RegistrationStatus.review.name());
                //razon, matching data esta vacio
                tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "Matching data es nulo", "system", "ADMIN"));
            }

            //verifico que tenga un callback
            if (rgc.getMatchingData() != null && !rgc.getMatchingData().isEmpty()) {
                RegistrationCallback callBack = registrationCallbackRepository.findByMatchingDataAndMessage(rgc.getMatchingData(), "Aceptada");
                if (callBack == null) {
                    rgc.setStatus(RegistrationStatus.review.name());
                    //razon, matching data esta vacio
                    tickets.add(new Ticket(TicketType.registration.name(), rgc.getId(), "No existe callback para el matching data", "system", "ADMIN"));
                }
            }

            //por ultimo lo marco como verificado
            if (rgc.getStatus().equals(RegistrationStatus.payment_done.name())) {
                rgc.setStatus(RegistrationStatus.verified.name());
            }
            registrationCompleteRepository.save(rgc);
            pl.increment();
        }

        if (!tickets.isEmpty()) {
            pl.appendLine("Tickets generados " + tickets.size());
            ticketsRepository.save(tickets);
        }

        pl.stop();
    }

    public void createClients() {
        createClients(launcher);
    }

    public void createClients(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Creacion de clientes");

        int createdClients = 0;
        int createdLines = 0;

        try {
            //registration completes
            List<RegistrationComplete> completes = registrationCompleteRepository.findByStatus(RegistrationStatus.verified.name());
            for (RegistrationComplete reg : completes) {

                Client client = clientsRepository.findOneByIdValue(reg.getNroDoc());
                //el cliente no existe
                if (client == null) {
                    client = new Client();

                    client.setName(WordUtils.capitalizeFully(reg.getNombre().trim()));
                    client.setLastname(WordUtils.capitalizeFully(reg.getApellido().trim()));
                    client.setMobile(reg.getTelefono());
                    client.setPhone(reg.getTelefono());
                    client.setIdType(DniUtil.getDniString(reg.getTipoDoc()));
                    client.setIdValue(StringUtils.upperCase(reg.getNroDoc().trim().replaceAll(" ", "")));
                    client.setBirthDate(reg.getFechaNac());

                    client.setAddress(WordUtils.capitalizeFully(reg.getDireccion()));
                    client.setCity(WordUtils.capitalizeFully(reg.getCiudad(), new char[]{'´', '\''}));
                    client.setState(WordUtils.capitalizeFully(reg.getProvincia()));
                    client.setContract(reg.getPathContrato());
                    client.setCountry(WordUtils.capitalizeFully(reg.getPais()));
                    client.setNationality(WordUtils.capitalizeFully(reg.getNacionalidad()));
                    client.setCreationDate(new Date());
                    client.setEmail(reg.getEmail());
                    client.setPostalCode(reg.getCp());

                    client.setCompanyName(WordUtils.capitalizeFully(reg.getRazonSocial()));
                    client.setCompanyIdType(DniUtil.getDniString(reg.getTipoDocEmpresa()));
                    client.setCompanyIdValue(StringUtils.upperCase(reg.getNroDocEmpresa().trim().replaceAll(" ", "")));

                    client.setReferer(reg.getReferer());
                    client.setRegistrationId(reg.getId());

                    clientsRepository.save(client);

                    pl.appendLine("Cliente " + client + " creado ");
                    createdClients++;

                } else {
                    pl.appendLine("Cliente " + client + " ya existe, continuando ");
                }

                //busco las direcciones de facturacion
                List<BillingAddress> addresses = billingAddressRepository.findByClientId(client.getId());

                BillingAddress toUse = new BillingAddress();
                toUse.setAddress(reg.getDireccion());
                toUse.setCity(reg.getCiudad());
                toUse.setClientId(client.getId());
                toUse.setCountry(reg.getPais());
                toUse.setCp(reg.getCp());
                toUse.setState(reg.getProvincia());

                //si esta vacia la creo
                if (addresses.isEmpty() || !addresses.contains(toUse)) {
                    billingAddressRepository.save(toUse);
                } else {
                    for (BillingAddress a : addresses) {
                        if (a.equals(toUse)) {
                            toUse = a;
                            break;
                        }
                    }
                }

                String planName = reg.getPlan();
                if (planName.equals("ilimitada10p")) {
                    planName = "ilimitada10";
                }
                Plan plan = plansRepository.findByShortName(planName);
                List<ClientLine> clientLines = new ArrayList<>();
                //creo las lineas a portar del cliente
                int porta = reg.getCantLineasPorta();
                if (porta > 0) {
                    List<PortLine> portLines = portLinesRepository.findByRegistrationId(reg.getId());
                    for (PortLine portLine : portLines) {

                        ClientLine cl = lineRepository.findByNumber(Long.valueOf(portLine.getPortNumber()));
                        if (cl != null) {
                            pl.appendLine("La linea " + portLine.getPortNumber() + " ya esiste, continuando");
                            continue;
                        }

                        ClientLine line = new ClientLine();
                        line.setClientId(client.getId());
                        line.setRegistrationId(reg.getId());
                        line.setCreationDate(new Date());
                        line.setInternalState(LineStatus.created_local.name());
                        line.setNumber(Long.valueOf(portLine.getPortNumber()));
                        line.setPlanDescription(plan.getDescription());
                        line.setPlanId(plan.getId());
                        line.setPort(true);
                        line.setPortPlannedDate(portLine.getPortDate());
                        line.setPortIccid(portLine.getIccid());
                        line.setPortOperator(portLine.getPortOperator());
                        line.setBillingAddress(toUse.getId());
                        line.setReferer(reg.getReferer());

                        clientLines.add(line);
                        createdLines++;
                        portLine.setStatus("processed");

                        pl.appendLine("Linea " + portLine.getPortNumber() + " creada ");
                    }
                    //update line status
                    portLinesRepository.save(portLines);
                }

                //creo las lineas nuevas
                int nuevas = reg.getCantLineas();
                List<ClientLine> alreadyCreatedLines = lineRepository.findByRegistrationId(reg.getId());
                if (alreadyCreatedLines != null && !alreadyCreatedLines.isEmpty()) {
                    if (alreadyCreatedLines.size() >= nuevas) {
                        pl.appendLine("Lineas ya creada para el registro");
                        nuevas = 0;
                    } else {
                        nuevas = nuevas - alreadyCreatedLines.size();
                        pl.appendLine("Algunas lineas ya estan creadas para ese registro, lineas a crear " + nuevas);
                    }
                }

                for (int i = 0; i < nuevas; i++) {

                    ClientLine line = new ClientLine();
                    line.setClientId(client.getId());
                    line.setCreationDate(new Date());
                    line.setInternalState(LineStatus.created_local.name());
                    line.setPlanDescription(plan.getDescription());
                    line.setPlanId(plan.getId());
                    line.setPort(false);
                    line.setBillingAddress(toUse.getId());
                    line.setRegistrationId(reg.getId());
                    line.setReferer(reg.getReferer());

                    createdLines++;
                    clientLines.add(line);
                }

                lineRepository.save(clientLines);

                processPaymentAuthorization(clientLines, launcher);

                reg.setStatus(RegistrationStatus.processed.name());
                registrationCompleteRepository.save(reg);

                //activities
                clientLines.forEach((clientLine) -> {
                    clientActivityService.createLineActivity(clientLine.getInternalState(), clientLine);
                });
            }

            pl.appendLine("Lineas creadas " + createdLines);
            pl.appendLine("Clientes creados " + createdClients + " de " + completes.size());
        } catch (Exception e) {
            log.error("Error creating clients ", e);
            pl.append("Error creando clientes " + e.getMessage());
        }

        pl.stop();

    }

    public void processPaymentAuthorization(List<ClientLine> lines, String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Proceso de autorizaciones");
        pl.setMax(lines.size());

        long currentLineId = 0;
        try {
            List<PaymentAuthorization> auths = new ArrayList<>();
            //create payment authorizations
            for (ClientLine line : lines) {

                currentLineId = line.getId();

                pl.increment();

                Registration reg = registrationRepository.findOne(line.getRegistrationId());
                RegistrationCallback callback = registrationCallbackRepository.findByMatchingDataAndMessage(reg.getMatchingData(), "Aceptada");

                if (callback == null) {
                    Ticket t = new Ticket(TicketType.authorization.name(), currentLineId, "El callback para la linea es nulo", "system", "ADMIN");
                    ticketsRepository.save(t);
                    pl.append("Callback para la linea es nulo, continuando");
                    continue;
                }

                //disable previos ones
                PaymentAuthorization existing = paymentAuthorizationRepository.findByLineIdAndClientIdAndActive(line.getId(), line.getClientId(), Boolean.TRUE);
                if (existing != null) {
                    existing.setActive(Boolean.FALSE);
                    paymentAuthorizationRepository.save(existing);
                }

                PaymentAuthorization auth = new PaymentAuthorization();
                auth.setActive(Boolean.TRUE);
                auth.setClientId(line.getClientId());
                auth.setCreationDate(new Date());
                auth.setLineId(line.getId());
                auth.setRejectedCount(0);
                auth.setCreatedBy(launcher);

                auth.setAmount(callback.getAmount());
                auth.setAuthCode(callback.getAuthCode());
                auth.setCardId(callback.getCardId());
                auth.setCode(callback.getCode());
                auth.setDate(callback.getDate());
                auth.setGuarantees(callback.getGuarantees());
                auth.setMatchingData(callback.getMatchingData());
                auth.setMessage(callback.getMessage());
                auth.setNumTransaction(callback.getNumTransaction());
                auth.setPanMask(callback.getPanMask());
                auth.setTransactionType(callback.getTransactionType());

                auths.add(auth);
            }

            if (!auths.isEmpty()) {
                paymentAuthorizationRepository.save(auths);
            }

            pl.appendLine("Autorizaciones procesadas " + auths.size());

        } catch (Exception e) {
            log.error("Error processing auths", e);
            pl.appendLine("Error procesando autorizaciones " + e.getMessage());
            Ticket t = new Ticket(TicketType.authorization.name(), currentLineId, "Error procesando authorizacion para la linea", "system", "CSLEVEL1");
            ticketsRepository.save(t);
        }
        pl.stop();
    }

    public void createContracts() {
        createContracts(launcher);
    }

    public void createContracts(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Creacion de contratos");
        int creados = 0;
        try {
            List<ClientLine> lines = lineRepository.findByInternalState(LineStatus.created_local.name());
            for (ClientLine line : lines) {

                List<Contract> contracts = contractsRepository.findByLineIdAndStatus(line.getId(), ContractStatus.signed.name());
                if (contracts != null && !contracts.isEmpty()) {
                    continue;
                }

                Long id = line.getRegistrationId();
                //Registration reg = registrationRepository.findOne(id);
                List<RegistrationFile> files = registrationFilesRepository.findByRegistrationId(id);
                File contractFile = contractService.getContract(line, files);

                Contract contract = new Contract();
                contract.setActive(false);
                contract.setClientId(line.getClientId());
                contract.setCreationDate(new Date());
                contract.setLineId(line.getId());
                contract.setPath(contractFile.getAbsolutePath());
                contract.setStatus(ContractStatus.created.name());
                contractsRepository.save(contract);
                creados++;

                line.setInternalState(LineStatus.contract_created.name());
            }

            lineRepository.save(lines);

            //activities
            lines.forEach((line) -> {
                clientActivityService.createContractActivity(line.getInternalState(), line);
            });

        } catch (Exception e) {
            log.error("Error generando contratos " + e.getMessage());
            pl.appendLine("Error generando contratos " + e.getMessage());
        }

        pl.appendLine("Contratos generados " + creados);
        pl.appendLine("Fin del proceso");
        pl.stop();

    }

    public void sendContracts() {
        sendContracts(launcher);
    }

    public void sendContracts(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Envio de contratos");

        int enviados = 0;
        int noEnviados = 0;

        try {
            List<Contract> contracts = contractsRepository.findByStatus(ContractStatus.created.name());
            for (Contract contract : contracts) {

                contractSignatureService.sendToSignature(contract);

                if (contract.getStatus().equals(ContractStatus.sended.name())) {
                    ClientLine line = contract.getLine();
                    line.setInternalState(LineStatus.contract_sended.name());
                    lineRepository.save(line);
                    clientActivityService.createContractActivity(line.getInternalState(), line);
                }

                if (contract.getRemotePath() != null && !contract.getRemotePath().startsWith("Error")) {

                    enviados++;

                    //send email
                    String body = emailService.getSignatureEmail(contract);
                    emailService.sendMail("Contrataciones<contratacion@wingsmobile.es>",
                            contract.getClient().getEmail(),
                            "Envio contrato al portal de Firmas.",
                            body,
                            null);

                } else {
                    noEnviados++;
                }
            }
            contractsRepository.save(contracts);

        } catch (Exception e) {
            log.error("Error enviando contratos " + e.getMessage());
            pl.appendLine("Error enviando contratos " + e.getMessage());
        }

        pl.appendLine("Contratos enviados " + enviados);
        pl.appendLine("Contratos No enviados " + noEnviados);
        pl.stop();

    }

    public void resendContracts(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Reenvio de contratos");

        int enviados = 0;
        int noEnviados = 0;

        try {
            List<Contract> contracts = contractsRepository.findByStatus(ContractStatus.recreated.name());
            contractSignatureService.resendToSignature(contracts, launcher);

            for (Contract contract : contracts) {

                clientActivityService.createContractActivity(contract.getStatus(), contract.getLine());

                if (contract.getRemotePath() != null && !contract.getRemotePath().startsWith("Error")) {
                    enviados++;
                    //send email
                    String body = emailService.getSignatureEmail(contract);
                    emailService.sendMail("Contrataciones<contratacion@wingsmobile.es>",
                            contract.getClient().getEmail(),
                            "Envio contrato al portal de Firmas.",
                            body,
                            null);
                } else {
                    noEnviados++;
                }
            }
            contractsRepository.save(contracts);

        } catch (Exception e) {
            log.error("Error reenviando contratos " + e.getMessage());
            pl.appendLine("Error reenviando contratos " + e.getMessage());
        }

        pl.appendLine("Contratos reenviados " + enviados);
        pl.appendLine("Contratos No reenviados " + noEnviados);
        pl.stop();
    }

    public void checkContractStatus() {
        checkContractStatus(launcher);
    }

    public void checkContractStatus(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Control estado contratos");

        int consultas = 0;
        int firmados = 0;
        int pendientes = 0;
        int expirados = 0;

        pl.appendLine("Solicitando estado de contratos");
        List<Contract> contracts = contractsRepository.findByStatus(ContractStatus.sended.name());
        pl.setMax(contracts.size());
        for (Contract contract : contracts) {

            ContractStatusResponse status = contractSignatureService.checkContractStatus(contract);
            consultas++;
            if (status != null && status.getAggregatedStatus() != null) {

                switch (status.getAggregatedStatus()) {
                    case "3":
                        firmados++;
                        contract.setStatus(ContractStatus.signed.name());
                        contract.setActive(true);
                        contract.setSigningDate(status.getDate());
                        contractsRepository.save(contract);
                        ClientLine line = contract.getLine();
                        line.setContractSigned(true);
                        line.setInternalState(LineStatus.contract_signed.name());

                        clientActivityService.createContractActivity(contract.getStatus(), contract.getLine());

                        lineRepository.save(line);
                        break;
                    case "7":
                        expirados++;
                        contract.setStatus(ContractStatus.expired.name());
                        contractsRepository.save(contract);
                        Ticket t = new Ticket(TicketType.contract.name(), contract.getId(), "Contrato expirado", "system", "ADMIN");
                        ticketsRepository.save(t);

                        clientActivityService.createContractActivity(contract.getStatus(), contract.getLine());

                        break;
                    case "2":
                        pendientes++;
                        break;
                    default:
                        //dejo esto para ver los status no conocidos
                        pendientes++;
                        log.error(status.getAggregatedStatus() + " --- " + status.getDescription() + " --- " + status.getId());
                        break;
                }
            }

            pl.increment();
        }

        pl.appendLine("Consultas realizadas: " + consultas);
        pl.appendLine("Contratos firmados: " + firmados);
        pl.appendLine("Contratos pendientes: " + pendientes);
        pl.appendLine("Contratos expirados: " + expirados);
        pl.stop();
    }

    //TODO: move to same method with flag to separate sended from resended
    //deberia verificar el estado de la linea y en base a eso
    //actualizar, si esta activa no hago nada si esta en contract sended
    //actualizo la linea
    /**
     * this will not interfere with the line status
     *
     * @param launcher
     */
    public void checkResendedContractStatus(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Control estado contratos reenviados");

        int consultas = 0;
        int firmados = 0;
        int pendientes = 0;
        int expirados = 0;

        pl.appendLine("Solicitando estado de contratos");
        /**
         * **IMPORTANT PART****
         */
        List<Contract> contracts = contractsRepository.findByStatus(ContractStatus.resended.name());
        pl.setMax(contracts.size());
        for (Contract contract : contracts) {

            ContractStatusResponse status = contractSignatureService.checkContractStatus(contract);
            consultas++;
            if (status != null && status.getAggregatedStatus() != null) {

                switch (status.getAggregatedStatus()) {
                    case "3":
                        firmados++;
                        contract.setStatus(ContractStatus.signed.name());
                        contract.setActive(true);
                        contract.setSigningDate(status.getDate());
                        contractsRepository.save(contract);
                        ClientLine line = contract.getLine();
                        line.setContractSigned(true);

                        if (line.getInternalState().equals(LineStatus.contract_sended.name())) {
                            line.setInternalState(LineStatus.contract_signed.name());
                        }

                        lineRepository.save(line);
                        break;
                    case "7":
                        expirados++;
                        contract.setStatus(ContractStatus.expired.name());
                        contractsRepository.save(contract);
                        Ticket t = new Ticket(TicketType.contract.name(), contract.getId(), "Contrato expirado", "system", "ADMIN");
                        ticketsRepository.save(t);
                        break;
                    default:
                        pendientes++;
                        //dejo esto para ver los status no conocidos
                        //el 2 es pendiente
                        log.error(status.getAggregatedStatus() + " --- " + status.getDescription() + " --- " + status.getId());
                        break;
                }
            }

            pl.increment();
        }

        pl.appendLine("Consultas realizadas: " + consultas);
        pl.appendLine("Contratos firmados: " + firmados);
        pl.appendLine("Contratos pendientes: " + pendientes);
        pl.appendLine("Contratos expirados: " + expirados);
        pl.stop();
    }

    public void createOrders() {
        createOrders(launcher);
    }

    public void createOrders(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Creacion de ordenes");

        List<ClientLine> lines = lineRepository.findByInternalState(LineStatus.contract_signed.name());

        //verifico que el cliente no tenga lineas con contratos sin firmar
        //para no hacer varios envios
        //TODO: preguntar antonio
        pl.appendLine("Ordenes a crear " + lines.size());
        pl.setMax(lines.size());

        List<Order> orders = new ArrayList<>();
        for (ClientLine clientLine : lines) {

            Long regId = clientLine.getRegistrationId();
            Registration reg = registrationRepository.findOne(regId);

            Order o = new Order();
            o.setCreationDate(new Date());
            o.setRegistrationId(reg.getId());
            o.setClientId(clientLine.getClientId());
            o.setLineId(clientLine.getId());
            o.setStatus(OrderStatus.created.name());

            String address = reg.getDireccionEnvio() != null ? reg.getDireccionEnvio() : reg.getDireccion();
            String city = reg.getCiudadEnvio() != null ? reg.getCiudadEnvio() : reg.getCiudad();
            String state = reg.getProvinciaEnvio() != null ? reg.getProvinciaEnvio() : reg.getProvincia();
            String cp = reg.getCpEnvio() != null ? reg.getCpEnvio() : reg.getCp();

            o.setShippingAddress(address);
            o.setShippingCity(city);
            o.setShippingState(state);
            o.setShippingCp(cp);
            o.setUpsStoreId(reg.getTiendaId());

            if (clientLine.isPort()) {
                o.setType(Order.TYPE_PORTA);
                o.setReferencia(Order.REF_PORTA);
                o.setDescripcion(Order.DESC_PORTA);
                o.setTelefono(clientLine.getNumber().toString());
            } else {
                o.setTelefono(reg.getTelefono());
                o.setType(Order.TYPE_NEW);
                o.setReferencia(Order.REF_NEW);
                o.setDescripcion(Order.DESC_NEW);
            }

            //para el caso de cambio de marca
            if (clientLine.isPort() && clientLine.getPortOperator().equals("044")) {
                o.setType(Order.TYPE_SUS);
                o.setReferencia(Order.REF_SUS);
                o.setDescripcion(Order.DESC_SUS);
                o.setTelefono(clientLine.getNumber().toString());
            }

            orders.add(o);
            clientLine.setInternalState(LineStatus.order_created.name());
            lineRepository.save(clientLine);

            clientActivityService.createOrderActivity(clientLine.getInternalState(), clientLine);

            pl.increment();
        }

        ordersRepository.save(orders);

        pl.appendLine("Ordenes creadas " + orders.size());
        pl.stop();

    }

    public void createOrdersReplacement(String launcher, List<ClientLine> lines) {

        ProcessLog pl = processLogService.createProcess(launcher, "Creacion de ordenes de reemplazo");
        pl.appendLine("Ordenes a crear " + lines.size());
        pl.setMax(lines.size());

        List<Order> orders = new ArrayList<>();
        for (ClientLine clientLine : lines) {

            //Long regId = clientLine.getRegistrationId();
            //Registration reg = registrationRepository.findOne(regId);
            Order o = new Order();
            o.setCreationDate(new Date());
            o.setRegistrationId(clientLine.getRegistrationId());
            o.setClientId(clientLine.getClientId());
            o.setLineId(clientLine.getId());
            o.setStatus(OrderStatus.replacement_created.name());

            Client c = clientLine.getClient();

            String address = c.getAddress();
            String city = c.getCity();
            String state = c.getState();
            String cp = c.getPostalCode();

            o.setShippingAddress(address);
            o.setShippingCity(city);
            o.setShippingState(state);
            o.setShippingCp(cp);
            //o.setUpsStoreId(reg.getTiendaId());

            o.setType(Order.TYPE_SUS);
            o.setReferencia(Order.REF_SUS);
            o.setDescripcion(Order.DESC_SUS);
            o.setTelefono(clientLine.getNumber().toString());
            orders.add(o);

            pl.increment();
        }

        ordersRepository.save(orders);

        pl.appendLine("Ordenes creadas " + orders.size());
        pl.stop();

    }

    public void sendOrders() {
        sendOrders(launcher);
    }

    public void sendOrders(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Envio de ordenes");

        List<Order> toSend = ordersRepository.findByStatus(OrderStatus.created.name());
        toSend.addAll(ordersRepository.findByStatus(OrderStatus.replacement_created.name()));

        pl.appendLine("Ordenes a enviar " + toSend.size());
        Map<String, List<Order>> groupedByAddress = new HashMap<>();
        for (Order order : toSend) {
            String key = order.getShippingAddress().toLowerCase().trim();

            if (order.getUpsStoreId() != null) {
                key += order.getUpsStoreId();
            }

            if (groupedByAddress.containsKey(key)) {
                long parentId = groupedByAddress.get(key).get(0).getId();
                order.setParent(parentId);
                groupedByAddress.get(key).add(order);
            } else {
                List<Order> list = new ArrayList<>();
                list.add(order);
                groupedByAddress.put(key, list);
            }
        }

        for (Map.Entry<String, List<Order>> entry : groupedByAddress.entrySet()) {
            List<Order> orders = entry.getValue();

            //set all orders, the same parent id
            if (orders.size() > 1) {
                orders.forEach((order) -> {
                    order.setParent(orders.get(0).getId());
                });
            }

            ordersService.sendOrders(orders);
        }

        ordersRepository.save(toSend);

        //update line status
        for (Order order : toSend) {
            ClientLine cl = lineRepository.findOne(order.getLineId());
            if (order.getStatus().equals(OrderStatus.sended.name())) {
                cl.setInternalState(LineStatus.order_sended.name());
            }
            lineRepository.save(cl);

            clientActivityService.createOrderActivity(cl.getInternalState(), cl);
        }

        pl.appendLine("ordenes enviadas:" + toSend.size());
        pl.appendLine("ordenes agrupadas:" + groupedByAddress.size());
        pl.stop();

    }

    public void createOperator() {
        createOperator(launcher);
    }

    public void createOperator(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Creacion lineas operador");

        List<ClientLine> lines = lineRepository.findByInternalState(LineStatus.order_delivered.name());

        int createdClients = 0;
        int newLines = 0;
        int portLines = 0;
        int brandChanges = 0;

        pl.appendLine("Lineas a crear " + lines.size());
        pl.setMax(lines.size());
        for (ClientLine line : lines) {

            pl.increment();

            Client client = line.getClient();
            if (client.getRefCustomerId() == null || client.getRefCustomerId().isEmpty()) {

                pl.appendLine("Cliente no existe creando " + client);

                xClientService.createClient(client);
                clientsRepository.save(client);

                //pregunto de nuevo para ver si se creo correctamente en xtra
                if (client.getRefCustomerId() == null || client.getRefCustomerId().isEmpty()) {
                    pl.appendLine("Error creando cliente, continuando " + client);
                    continue;
                }

                createdClients++;
            }

            Plan plan = plansRepository.findOne(line.getPlanId());
            line.setPlan(plan);
            if (line.getIccid() == null) {
                pl.appendLine("Iccid de la linea es nulo, continuando " + line.getId());
                continue;
            }

            if (line.isPort()) {

                //cambio de marca
                if (line.getPortOperator().equals("044")) {
                    xtraService.requestBrandChange(line);
                    line.setInternalState(LineStatus.change_brand_requested.name());
                    brandChanges++;
                } else {
                    pl.appendLine("Solicitando portabilidad " + line.getId());
                    xPortService.portClient(line, client, plan);
                    portLines++;
                }

            } else {
                pl.appendLine("Creando linea nueva " + line.getId());
                xLineService.createLine(client, line, plan);
                newLines++;
            }
        }

        lineRepository.save(lines);

        for (ClientLine line : lines) {
            clientActivityService.createOrderActivity(line.getInternalState(), line);
        }

        pl.appendLine("Clientes creados " + createdClients);
        pl.appendLine("Lineas nuevas creadas " + newLines);
        pl.appendLine("Portabilidades solicitadas " + portLines);
        pl.appendLine("Cambios de marca solicitados " + brandChanges);
        pl.stop();

    }

    public void sendTestContract(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Envio de contrato de prueba");
        try {
            contractSignatureService.postTest();

            //send email
            String body = emailService.getGenericEmail("Test", "Test Porta Sigma", "Esto es una prueba");
            emailService.sendMail("Contrataciones<contratacion@wingsmobile.es>",
                    "sebastian.lucero@wingsmobile.es",
                    "Envio contrato al portal de Firmas.",
                    body,
                    null);

        } catch (Exception e) {
            log.error("Error enviando contratos " + e.getMessage());
            pl.appendLine("Error enviando contratos " + e.getMessage());
        }

        pl.appendLine("Contratos enviados ");
        pl.stop();
    }

    private boolean compareEquals(String param1, String param2) {
        return param1.toLowerCase().trim().equals(param2.toLowerCase().trim());
    }

}
