/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.enums;

/**
 *
 * @author seba
 */
public enum ContractStatus {

    /**
     * Contract created local
     * we need to send to client and
     * to signature portal
     */
    created,    

    /**
     * Contract created local
     * we need to send to client and to signature portal
     * this status does not afect lines status
     */
    recreated,    
        
    /**
     * Sended to the client and to portal
     */
    sended,
    /**
     * Sended to the client and to portal
     */
    resended,
    /**
     * Error sending contract in 
     * portal or email
     */
    error,
    
    /**
     * Contract signed
     */
    signed,
    /**
     * Contract expired
     */
    expired 
    
}
