/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao;

import com.wings.base.domain.client.BalanceSums;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ClientDebtsDAO {

    @Autowired
    private EntityManager em;

    public BalanceSums find() {

        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.base.domain.client.BalanceSums(sum(d.balance) as total,");
        b.append("count(1) as records) ");
        b.append("FROM ClientDebt d ");
        b.append("WHERE 1=1 ");

        Query query = em.createQuery(b.toString(), BalanceSums.class);
        BalanceSums ret = (BalanceSums) query.getSingleResult();

        return ret;
    }

}
