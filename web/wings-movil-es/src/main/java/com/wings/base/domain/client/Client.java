/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.wings.base.utils.NumberUtils;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Formula;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "clients")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String lastname;
    private String idType;
    private String idValue;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    private String email;
    private String phone;
    private String mobile;
    private String fax;
    private String address;
    private String postalCode;
    private String city;
    private String state;
    private String country;
    private String nationality;
    private String companyName;
    private String companyIdType;
    private String companyIdValue;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date activationDate;
    private String status;
    private String refCustomerId;
    private String contract;
    private String signature;
    private Long registrationId;
    private String referer;
    @OneToMany(mappedBy = "clientId", targetEntity = ClientLine.class, fetch = FetchType.EAGER)
    private List<ClientLine> lines;
    @Formula(value = "(select sum(cb.amount) from clients_balance cb where cb.client_id = id)")
    private Double balance;
    private boolean adminBlocked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdValue() {
        return idValue;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefCustomerId() {
        return refCustomerId;
    }

    public void setRefCustomerId(String refCustomerId) {
        this.refCustomerId = refCustomerId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyIdType() {
        return companyIdType;
    }

    public void setCompanyIdType(String companyIdType) {
        this.companyIdType = companyIdType;
    }

    public String getCompanyIdValue() {
        return companyIdValue;
    }

    public void setCompanyIdValue(String companyIdValue) {
        this.companyIdValue = companyIdValue;
    }

    public List<ClientLine> getLines() {
        return lines;
    }

    public void setLines(List<ClientLine> lines) {
        this.lines = lines;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return name + " " + lastname + (companyName != null && !companyName.isEmpty() ? " - " + companyName : "");
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getBalanceText() {
        if (balance != null) {
            return NumberUtils.formatMoney(balance);
        }
        return "-";
    }

    public boolean isAdminBlocked() {
        return adminBlocked;
    }

    public void setAdminBlocked(boolean adminBlocked) {
        this.adminBlocked = adminBlocked;
    }

    public int getActiveLines() {
        int actives = 0;
        actives = lines.stream().filter((line) -> (line.getCancelDate() == null)).map((_item) -> 1).reduce(actives, Integer::sum);
        return actives;
    }

    public int getCanceledLines() {
        int canceled = 0;
        canceled = lines.stream().filter((line) -> (line.getCancelDate() != null)).map((_item) -> 1).reduce(canceled, Integer::sum);
        return canceled;
    }
}
