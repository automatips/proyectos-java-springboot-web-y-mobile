/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.ClientLine;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author seba
 */
public interface LinesRepository extends JpaRepository<ClientLine, Long>, JpaSpecificationExecutor<ClientLine> {

    ClientLine findByNumber(Long number);

    List<ClientLine> findByClientId(Long clientId);

    List<ClientLine> findByPlanId(Long planId);

    List<ClientLine> findByRegistrationId(Long registrationId);

    ClientLine findByIccid(String iccid);

    List<ClientLine> findByPort(Boolean porta);

    List<ClientLine> findByInternalState(String status);

    @Query(
            value = "select cl.* from client_lines cl INNER JOIN clients c on c.id = cl.client_id where cl.number like %:q% OR cl.iccid like %:q% OR c.`name` like %:q% OR c.lastname like %:q% or c.id_value like %:q% ORDER BY id DESC \n#pageable\n",
            countQuery = "select count(1) from from client_lines cl INNER JOIN clients c on c.id = cl.client_id where cl.number like %:q% OR cl.iccid like %:q% OR c.`name` like %:q% OR c.lastname like %:q% or c.id_value like %:q%",
            nativeQuery = true
    )
    Page<ClientLine> findByQuery(@Param("q") String q, Pageable pageable);

    @Query(
            value = "select * from client_lines where (number like %?1% OR iccid like %?1%) AND internal_state in ?2  ORDER BY id DESC \n#pageable\n",
            countQuery = "select count(1) from client_lines where (number like %?1% OR iccid like %?1%) AND internal_state in ?2",
            nativeQuery = true
    )
    Page<ClientLine> findByQueryWithStatus(String q, String[] internalState, Pageable pageable);

}
