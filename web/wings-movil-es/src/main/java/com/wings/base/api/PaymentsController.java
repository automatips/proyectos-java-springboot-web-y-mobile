/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.base.WingsProperties;
import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.domain.Token;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientActivity;
import com.wings.base.domain.client.ClientDebtsReminder;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.payment.PaymentAuthorization;
import com.wings.base.domain.payment.PaymentCallback;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.ClientActivityType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.enums.TokenType;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.ClientDebtsReminderRepository;
import com.wings.base.repos.ClientsActivitiesRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.PaymentAuthorizationRepository;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.wings.base.repos.PaymentCallbackRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.TokensRepository;
import com.wings.base.service.EnvironmentService;
import com.wings.base.utils.FormatUtils;
import com.wings.base.utils.NumberUtils;
import com.wings.payments.client.WingsPay;
import com.wings.payments.client.dto.CallbackDTO;
import com.wings.payments.client.dto.RequestDTO;
import com.wings.payments.client.dto.RequestDTOExtra;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author seba
 */
@Controller
public class PaymentsController {

    private final Logger log = LoggerFactory.getLogger(PaymentsController.class);

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ClientsActivitiesRepository clientsActivitiesRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private EnvironmentService environmentService;

    @Autowired
    private ClientDebtsReminderRepository clientDebtsReminderRepository;

    /**
     * Open url pay one invoice
     *
     * @param model
     * @param request
     * @param token
     * @return
     */
    @RequestMapping(value = {"/pagos/factura/{token}"}, method = {RequestMethod.GET})
    public String getPaymentInvoice(Model model, HttpServletRequest request, @PathVariable("token") String token) {

        String view = "views/clients/payments/invoice";
        Invoice invoice = invoiceRepository.findByToken(token);
        if (invoice == null) {
            model.addAttribute("error", "Token Inválido");
            return view;
        }

        double pagado = 0;
        double total = 0;
        double totalRemain = 0;
        List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())));
        for (Balance balance : balances) {
            if (balance.getType().equals(BalanceType.discount.name())
                    && !balance.getDescription().contains("Descuento")
                    && !balance.getDescription().contains("Pago cuotas anticipadas")) {
                pagado += balance.getAmount();
            }
            total += balance.getAmount();
        }
        invoice.setPayed(pagado * -1);
        invoice.setRemain(total);
        totalRemain += total;

        invoice.setPayed(pagado);
        invoice.setRemain(totalRemain);

        WingsPay.init(environmentService.isDevEnvironment());
        RequestDTO dto = new RequestDTO();

        dto.setAmount(totalRemain);
        dto.setBillingAddress(invoice.getClientAddress());
        dto.setBillingCity(invoice.getClientCity());
        dto.setBillingCountry("ES");
        dto.setBillingEmail(invoice.getClient().getEmail());
        dto.setBillingFirstName(invoice.getClient().getName());
        dto.setBillingLastName(invoice.getClient().getLastname());
        dto.setBillingState(invoice.getClientState());

        ClientDebtsReminder rem = clientDebtsReminderRepository.findFirstByClientId(invoice.getClientId());
        String remToken = rem.getToken();
        if (environmentService.isDevEnvironment()) {
            dto.setCallbackUrl("http://local.movil.wingsmobile.net/api/payments/callback/w");
            dto.setCancelUrl("http://local.movil.wingsmobile.net/p/" + remToken);
        } else {
            dto.setCallbackUrl("https://movil.wingsmobile.net/api/payments/callback/w");
            dto.setCancelUrl("https://movil.wingsmobile.net/p/" + remToken);
        }

        dto.setComsDiscountAmount(null);
        dto.setCountry("ES");
        dto.setCurrency("EUR");
        dto.setDescription("Período " + invoice.getMonth() + "-" + invoice.getYear());
        dto.setExcludeBacs(false);

        //EXTRAS:
        List<RequestDTOExtra> extras = new LinkedList<>();
        RequestDTOExtra idate = new RequestDTOExtra();
        idate.setExtraName("Fecha");
        idate.setExtraValue(FormatUtils.formatHumanOnlyDate(invoice.getInvoiceDate()));
        RequestDTOExtra venc = new RequestDTOExtra();
        venc.setExtraName("Vencimiento");
        venc.setExtraValue(FormatUtils.formatHumanOnlyDate(invoice.getExpireDate()));
        RequestDTOExtra tot = new RequestDTOExtra();
        tot.setExtraName("Total factura");
        tot.setExtraValue(invoice.getTotalText());

        extras.add(idate);
        extras.add(venc);
        extras.add(tot);
        dto.setExtras(extras);

        dto.setImage("https://movil.wingsmobile.net/img/logo_clean.png");
        dto.setOrderNumber(invoice.getId());
        dto.setPaymentType(null);
        dto.setPreferredGateway(null);
        dto.setRequesterName("movil");
        dto.setTitle("Factura " + invoice.getInvoiceNumber());

        String payUrl = WingsPay.createPaymentRequest(dto);

        ClientActivity activity = new ClientActivity();
        activity.setActivityDate(new Date());
        activity.setActivityType(ClientActivityType.client_invoice.name());
        activity.setClientId(invoice.getClientId());
        activity.setComment("Solicitud pago de factura nro " + invoice.getInvoiceNumber());
        activity.setCreationDate(new Date());
        activity.setTitle("invoice id " + invoice.getId());
        activity.setUser("VISITOR");
        activity.setTitle("Solicitud pago factura - Wings Pagos");
        clientsActivitiesRepository.save(activity);

        return "redirect:" + payUrl;

    }

    /**
     * Open pay all invoices
     *
     * @param model
     * @param request
     * @param token
     * @return
     */
    @RequestMapping(value = {"/pagos/facturas/{token}"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String getPaymentInvoices(Model model, HttpServletRequest request, @PathVariable("token") String token) {

        ClientDebtsReminder rem = clientDebtsReminderRepository.findFirstByToken(token);
        List<Invoice> invoices = invoiceRepository.findByClientId(rem.getClientId());
        double totalRemain = 0;
        for (Invoice invoice : invoices) {
            double pagado = 0;
            double total = 0;
            List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())));
            for (Balance balance : balances) {
                if (balance.getType().equals(BalanceType.discount.name())
                        && !balance.getDescription().contains("Descuento")
                        && !balance.getDescription().contains("Pago cuotas anticipadas")) {
                    pagado += balance.getAmount();
                }
                total += balance.getAmount();
            }
            invoice.setPayed(pagado * -1);
            invoice.setRemain(total);
            totalRemain += total;
        }

        WingsPay.init(environmentService.isDevEnvironment());
        RequestDTO dto = new RequestDTO();

        dto.setAmount(totalRemain);
        dto.setBillingAddress(rem.getClient().getAddress());
        dto.setBillingCity(rem.getClient().getCity());
        dto.setBillingCountry("ES");
        dto.setBillingEmail(rem.getClient().getEmail());
        dto.setBillingFirstName(rem.getClient().getName());
        dto.setBillingLastName(rem.getClient().getLastname());
        dto.setBillingState(rem.getClient().getState());

        if (environmentService.isDevEnvironment()) {
            dto.setCallbackUrl("http://local.movil.wingsmobile.net/api/payments/callback/w");
            dto.setCancelUrl("http://local.movil.wingsmobile.net/p/" + rem.getToken());
        } else {
            dto.setCallbackUrl("https://movil.wingsmobile.net/api/payments/callback/w");
            dto.setCancelUrl("https://movil.wingsmobile.net/p/" + rem.getToken());
        }

        //NUNCA AGREGAR EXTRAS - VER CALLBACK
        dto.setComsDiscountAmount(null);
        dto.setCountry("ES");
        dto.setCurrency("EUR");
        dto.setDescription("Pago total deuda cliente " + rem.getClient());
        dto.setExcludeBacs(false);

        dto.setImage("https://movil.wingsmobile.net/img/logo_clean.png");
        dto.setOrderNumber(rem.getId());
        dto.setPaymentType(null);
        dto.setPreferredGateway(null);
        dto.setRequesterName("movil");
        dto.setTitle("Pago total");

        String payUrl = WingsPay.createPaymentRequest(dto);
        return "redirect:" + payUrl;

    }

    @ResponseBody
    @RequestMapping(value = {"api/payments/callback/w"}, method = RequestMethod.POST)
    public ResponseEntity postCallbackFromWingsPagos(HttpServletRequest request) {

        try {

            ObjectMapper mapper = new ObjectMapper();
            CallbackDTO dto = mapper.readValue(request.getReader(), CallbackDTO.class);
            Long id = Long.parseLong(dto.getOrderNumber());

            ClientDebtsReminder rem = null;
            Invoice invoice = null;
            if (dto.hasExtras()) {
                invoice = invoiceRepository.findOne(id);
            } else {
                rem = clientDebtsReminderRepository.findOne(id);
            }

            if (invoice == null && rem == null) {
                return ResponseEntity.badRequest().body("No se encontró el item a pagar");
            }

            if (invoice != null) {
                //marco la factura como pagada
                if (dto.getStatus().equals("COMPLETED")) {

                    double pagado = 0;
                    double total = 0;
                    double totalRemain = 0;
                    List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())));
                    for (Balance balance : balances) {
                        if (balance.getType().equals(BalanceType.discount.name())
                                && !balance.getDescription().contains("Descuento")
                                && !balance.getDescription().contains("Pago cuotas anticipadas")) {
                            pagado += balance.getAmount();
                        }
                        total += balance.getAmount();
                    }
                    invoice.setPayed(pagado * -1);
                    invoice.setRemain(total);
                    totalRemain += total;
                    invoice.setPayed(pagado);
                    invoice.setRemain(totalRemain);

                    Long lineId = ((ClientLine) invoice.getClient().getLines().toArray()[0]).getId();
                    //marco la facura como pagada
                    invoice.setStatus(InvoiceStatus.payed.name());
                    invoiceRepository.save(invoice);

                    //inserto en el balance un saldo negativo restando lo de la factura
                    Balance b = new Balance();
                    b.setAmount(totalRemain * -1);
                    b.setClientId(invoice.getClientId());
                    b.setControl("wings-pagos-invoice-payment-" + invoice.getId());
                    b.setDate(new Date());
                    b.setDescription("Cobro de factura " + invoice.getInvoiceNumber() + " por Wings Pagos");
                    b.setLineId(lineId);
                    b.setMonth(invoice.getMonth());
                    b.setObservation("invoice charged");
                    b.setPaymentMethod(dto.getProcessor());
                    b.setPaymentRef("wings-pagos");
                    b.setType(BalanceType.discount.name());
                    b.setYear(invoice.getYear());

                    balanceRepository.save(b);

                    //si tiene reminder, y esta listado en morosos, actualizo
                    //si el balance del reminder es 0 elimino
                    ClientDebtsReminder rem1 = clientDebtsReminderRepository.findFirstByClientId(b.getClientId());
                    if (rem1 != null) {
                        rem1.setBalance(rem1.getBalance() - totalRemain);
                        if (rem1.getBalance() > 0) {
                            clientDebtsReminderRepository.save(rem1);
                        }
                    }

                    ClientActivity activity = new ClientActivity();
                    activity.setActivityDate(new Date());
                    activity.setActivityType(ClientActivityType.client_invoice.name());
                    activity.setClientId(invoice.getClientId());
                    activity.setComment("Solicitud pago de factura nro " + invoice.getInvoiceNumber() + " completa - monto pagado " + totalRemain);
                    activity.setCreationDate(new Date());
                    activity.setTitle("invoice id " + invoice.getId());
                    activity.setUser("VISITOR");
                    clientsActivitiesRepository.save(activity);

                }
            } else if (rem != null) {
                //pago todo como pagado
                if (dto.getStatus().equals("COMPLETED")) {

                    //double payed = Double.parseDouble(pr.getAmount()) / 100.0;
                    double payed = rem.getBalance();

                    List<Invoice> invoices = invoiceRepository.findByClientIdAndStatus(rem.getClientId(), InvoiceStatus.unpayed.name());
                    invoices.addAll(invoiceRepository.findByClientIdAndStatus(rem.getClientId(), InvoiceStatus.partial.name()));

                    for (Invoice invc : invoices) {

                        double pagado = 0;
                        double total = 0;
                        List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invc.getClientId().toString(), null, String.valueOf(invc.getMonth()), String.valueOf(invc.getYear())));
                        for (Balance balance : balances) {
                            if (balance.getType().equals(BalanceType.discount.name())
                                    && !balance.getDescription().contains("Descuento período")
                                    && !balance.getDescription().contains("Pago cuotas anticipadas")) {
                                pagado += balance.getAmount();
                            }
                            total += balance.getAmount();
                        }
                        invc.setPayed(pagado * -1);
                        invc.setRemain(total);

                        Long lineId = ((ClientLine) invc.getClient().getLines().toArray()[0]).getId();
                        //marco la facura como pagada
                        invc.setStatus(InvoiceStatus.payed.name());
                        invoiceRepository.save(invc);

                        //inserto en el balance un saldo negativo restando lo de la factura
                        Balance b = new Balance();
                        b.setAmount(invc.getRemain() * -1);
                        b.setClientId(invc.getClientId());
                        b.setControl("wings-pagos-invoice-payment-" + invc.getId());
                        b.setDate(new Date());
                        b.setDescription("Cobro de factura " + invc.getInvoiceNumber() + " pago total");
                        b.setLineId(lineId);
                        b.setMonth(invc.getMonth());
                        b.setObservation("invoice charged");
                        b.setPaymentMethod(dto.getProcessor());
                        b.setPaymentRef("wings-pagos");
                        b.setType(BalanceType.discount.name());
                        b.setYear(invc.getYear());

                        balanceRepository.save(b);
                    }

                    ClientActivity activity = new ClientActivity();
                    activity.setActivityDate(new Date());
                    activity.setActivityType(ClientActivityType.client_invoice.name());
                    activity.setClientId(rem.getClientId());
                    activity.setComment("Solicitud pago total completa - monto pagado " + payed);
                    activity.setCreationDate(new Date());
                    activity.setTitle("rem id " + rem.getId());
                    activity.setUser("VISITOR");
                    clientsActivitiesRepository.save(activity);

                    rem.setBalance(rem.getBalance() - payed);
                    clientDebtsReminderRepository.save(rem);

                }
            } else {
                return ResponseEntity.badRequest().body("No se encontró el item a pagar");
            }
            return ResponseEntity.ok("success");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error en callback " + e.getMessage());
        }
    }

}
