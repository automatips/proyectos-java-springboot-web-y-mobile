/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.domain.client.Plan;
import com.wings.base.domain.xtra.Product;
import com.wings.base.repos.PlanRepository;
import com.wings.base.service.xtra.XOperatorsService;
import com.wings.base.service.xtra.XProductsService;
import com.wings.base.utils.MapUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.base.utils.PageWrapper;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class PlansController {

    private final Logger log = LoggerFactory.getLogger(PlansController.class);

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private XProductsService xProductsService;

    @Autowired
    private XOperatorsService xOperatorsService;

    @RequestMapping(value = {"views/system/plans"}, method = RequestMethod.GET)
    public String getPlans(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        Page<Plan> items = planRepository.findAll(new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id")));

        PageWrapper<Plan> pageWrapper = new PageWrapper<>(items, "views/system/plans");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("page", pageWrapper);

        return "views/system/plans";
    }

    @RequestMapping(value = {"views/system/plans/{id}/form"}, method = RequestMethod.GET)
    public String getPalnForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Plan plan = planRepository.findOne(id);
        model.addAttribute("title", "Cliente");
        model.addAttribute("subTitle", plan.toString());
        model.addAttribute("elements", MapUtils.getFormElements(plan));

        return "views/common/view-form";
    }

    @ResponseBody
    @RequestMapping(value = {"api/plans/operator"}, method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getClient(Model model, HttpServletRequest request) {
        List<Product> ret = xProductsService.getProducts();
        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/operators/operator"}, method = RequestMethod.GET)
    public ResponseEntity<String> getOperators(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(xOperatorsService.getOperators());
    }   
}
