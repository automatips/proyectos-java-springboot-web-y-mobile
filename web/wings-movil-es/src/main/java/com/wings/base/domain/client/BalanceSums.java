/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.wings.base.utils.NumberUtils;

/**
 *
 * @author seba
 */
public class BalanceSums {

    private Double total;
    private Long records;

    public BalanceSums(Double sum, Long records) {        
        this.total = sum != null ? sum : 0;
        this.records = records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public Long getRecords() {
        return records;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTotal() {
        return total;
    }

    public String getTotalText() {
        return NumberUtils.formatMoney(total);
    }

}
