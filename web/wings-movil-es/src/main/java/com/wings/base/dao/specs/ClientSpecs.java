/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Client;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class ClientSpecs {

    public static Specification<Client> clientSearch(String q, String status) {

        return (Root<Client> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("name"), search),
                                builder.like(root.get("lastname"), search),
                                builder.like(root.get("email"), search),
                                builder.like(root.get("idValue"), search),
                                builder.like(root.get("companyName"), search),
                                builder.like(root.get("companyIdValue"), search),
                                builder.like(builder.concat(builder.concat(root.get("name"), " "), root.get("lastname")), search)
                        )
                );

                try {
                    if (q.contains("id:")) {

                        predicates.clear();

                        String qq = q.replaceAll("id:", "");
                        Long clientId = Long.parseLong(qq);
                        predicates.add(builder.equal(root.get("id"), clientId));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (status != null) {
                String[] statuses = status.split(",");
                Expression<String> exp = root.get("status");
                predicates.add(exp.in(Arrays.asList(statuses)));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
