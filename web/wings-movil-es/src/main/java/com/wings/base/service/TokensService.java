/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.Token;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.enums.TokenType;
import com.wings.base.repos.TokensRepository;
import java.util.Date;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class TokensService {

    private final long clientTokenDuration3Days = 259200000l;
    private final long clientTokenDuration2Days = 172800000l;
    private final long invoiceTokenDuration5Days = 432000000;

    @Autowired
    private TokensRepository tokensRepository;

    public Token generateTokenForInvoice(Invoice invoice) {
        Token t = new Token();
        Date creation = new Date();
        t.setCreationDate(creation);
        t.setExpireDate(new Date(creation.getTime() + invoiceTokenDuration5Days));
        t.setReferenceId(invoice.getId());
        t.setTokenType(TokenType.invoice.name());
        t.setActive(Boolean.TRUE);
        t.setToken(getToken(invoice.getId() + creation.getTime() + "INVOICE"));
        tokensRepository.save(t);
        return t;
    }

    public Token generateTokenForClient(Client client) {

        List<Token> tokens = tokensRepository.findByReferenceIdAndTokenType(client.getId(), TokenType.client.name());
        tokensRepository.delete(tokens);

        Token t = new Token();
        Date creation = new Date();
        t.setCreationDate(creation);
        t.setExpireDate(new Date(creation.getTime() + clientTokenDuration3Days));
        t.setReferenceId(client.getId());
        t.setTokenType(TokenType.client.name());
        t.setActive(Boolean.TRUE);
        t.setToken(getToken(client.getId() + creation.getTime() + "CLIENT"));

        tokensRepository.save(t);
        return t;
    }

    public Token generateTokenForClient(Long clientId) {

        List<Token> tokens = tokensRepository.findByReferenceIdAndTokenType(clientId, TokenType.client.name());
        tokensRepository.delete(tokens);

        Token t = new Token();
        Date creation = new Date();
        t.setCreationDate(creation);
        t.setExpireDate(new Date(creation.getTime() + clientTokenDuration2Days));
        t.setReferenceId(clientId);
        t.setTokenType(TokenType.client.name());
        t.setActive(Boolean.TRUE);
        t.setToken(getToken(clientId + creation.getTime() + "CLIENT"));

        tokensRepository.save(t);
        return t;
    }

    public Token getTokenForClient(Long clientId) {

        List<Token> tokens = tokensRepository.findByReferenceIdAndTokenType(clientId, TokenType.client.name());

        Token active = null;
        if (tokens.isEmpty()) {
            active = generateTokenForClient(clientId);
        } else {

            Date now = new Date();
            for (Token token : tokens) {

                //si no esta activo
                if (!token.getActive()) {
                    tokensRepository.delete(token);
                    continue;
                }

                //si esta expirado
                if (token.getExpireDate().before(now)) {
                    tokensRepository.delete(token);
                    continue;
                }

                active = token;
            }

            if (active == null) {
                active = generateTokenForClient(clientId);
            }
        }

        return active;
    }

    private String getToken(String data) {
        return DigestUtils.sha512Hex(data);
    }

}
