/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.client.Plan;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class LineSpecs {

    public static Specification<ClientLine> lineSearch(String q, String status) {

        return (Root<ClientLine> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            
            Join<ClientLine,Client> client = root.join("client");
            Join<ClientLine,Plan> plan = root.join("plan");
                        
            List<Predicate> predicates = new ArrayList<>();
            List<Predicate> orPredicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(client.get("name"), search),
                                builder.like(client.get("lastname"), search),
                                builder.like(client.get("email"), search),
                                builder.like(client.get("idValue"), search),
                                builder.like(
                                        builder.concat(
                                                builder.concat(client.get("name"), " "),
                                                client.get("lastname")),
                                         search),                                
                                builder.like(root.get("iccid"), search),
                                builder.like(plan.get("description"), search)
                        )
                );
                
                try {
                    Long.parseLong(q);
                    predicates.add(builder.equal(root.get("number"), q));
                } catch (Exception e) {
                }                
            }

            if (status != null) {
                String[] statuses = status.split(",");
                Expression<String> exp = root.get("internalState");
                predicates.add(exp.in(Arrays.asList(statuses)));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
