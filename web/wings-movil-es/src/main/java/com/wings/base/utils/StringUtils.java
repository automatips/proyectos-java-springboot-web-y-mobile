/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seba
 */
public class StringUtils {

    public static boolean compareEquals(String param1, String param2) {
        return param1.toLowerCase().trim().equals(param2.toLowerCase().trim());
    }

    /**
     * Get the String difference bewteen 2 string formated as resutl of
     * ReflectionToStringBuilder(object).toString()
     *
     * @param one
     * @param two
     * @return
     */
    public static String getDiff(String one, String two) {

        //com.wings.base.domain.client.Client@4bd9a19a[id=100074,name=Jordia,lastname=Arriaza Sebaia,idType=NIEa,idValue=123123123,birthDate=1982-07-22,email=test@gmail.com,phone=12312323123,mobile=638093752,fax=<null>,address=C/sio 3,postalCode=08233,city=Vacarisses,state=Barcelona,country=Es,nationality=España,companyName=,companyIdType=,companyIdValue=,creationDate=2017-09-29 08:47:01.0,activationDate=<null>,status=Cliente dado de alta con numero F33563012323,refCustomerId=F33563012323,contract=<null>,signature=<null>,registrationId=6319,referer=5252212,lines=[638093752],balance=49.14599999999999]
        one = one.substring(one.indexOf("[id"), one.length() - 1);
        two = two.substring(two.indexOf("[id"), two.length() - 1);

        String[] first = one.split(",");
        String[] second = two.split(",");

        List<String> before = new ArrayList<>();
        List<String> after = new ArrayList<>();

        for (int i = 0; i < second.length; i++) {
            String f = first[i];
            String s = second[i];
            if (!s.equals(f)) {
                before.add(f);
                after.add(s);
            }
        }

        if (before.isEmpty() && after.isEmpty()) {
            return "";
        }

        return "Antes: " + before.toString() + System.lineSeparator() + "Despues:" + after.toString();
    }

}
