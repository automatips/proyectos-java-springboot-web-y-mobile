/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.WingsProperties;
import com.wings.base.domain.ContractStatusResponse;
import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.Contract;
import com.wings.base.enums.ContractStatus;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.TicketsRepository;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class ContractSignatureService {

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private TicketsRepository ticketsRepository;

    @Autowired
    private EmailService emailService;

    private final Logger log = LoggerFactory.getLogger(ContractSignatureService.class);

    private final String postUrl;
    private final String authString;

    @Autowired
    public ContractSignatureService(WingsProperties wingsProperties) {
        this.postUrl = wingsProperties.getSignature().getUrl();
        String toEncode = wingsProperties.getSignature().getUsername() + ":" + wingsProperties.getSignature().getPassword();
        byte[] encodedBytes = Base64.encodeBase64(toEncode.getBytes());
        authString = new String(encodedBytes);
    }

    public void sendToSignature(Contract contract) {
        postDocument(contract);
    }

    /**
     * Resend the contract to signature service, without affecting the line
     * status, either was expired or plan change
     *
     * @param contracts
     * @param launcher
     */
    public void resendToSignature(List<Contract> contracts, String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Reenvio de contratos");
        pl.setMax(contracts.size());

        int enviados = 0;
        for (Contract contract : contracts) {
            postDocument(contract);
            if (contract.getStatus().equals(ContractStatus.sended.name())) {
                enviados++;
                contract.setStatus(ContractStatus.resended.name());
            }
            pl.increment();

            if (contract.getRemotePath() != null && !contract.getRemotePath().startsWith("Error")) {
                //send email
                String body = emailService.getSignatureEmail(contract);
                emailService.sendMail("Contrataciones<contratacion@wingsmobile.es>",
                        contract.getClient().getEmail(),
                        "Envio contrato al portal de Firmas.",
                        body,
                        null);
            }
        }

        pl.append("Contratos enviados " + enviados);
        pl.append("Contratos NO enviados " + (contracts.size() - enviados));
        pl.stop();
    }

    private void postDocument(Contract contract) {

        RestTemplate template = new RestTemplate();

        File file = new File(contract.getPath());

        String desc = " - ";
        if (contract.getLine().isPort()) {
            desc += "Portabilidad " + contract.getLine().getNumber() + " - ";
        }
        desc += "Plan " + contract.getLine().getPlanDescription();

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new FileSystemResource(file));

        String name = contract.getClient().toString();
        if (name.length() > 35) {
            name = name.substring(0, 35);
        }
        map.add("description", "Firmado digital - " + name + desc);
        map.add("reference", contract.getId() + "-" + contract.getClientId() + "-" + contract.getLineId() + "_signature");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", "Basic " + authString);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        try {
            ResponseEntity<String> result = template.postForEntity(postUrl, requestEntity, String.class);
            String url = result.getHeaders().getFirst("Location");

            //add signer
            String signersUrl = url + "signers/";
            template = new RestTemplate();

            map = new LinkedMultiValueMap<>();
            map.add("name", contract.getClient().toString());
            map.add("id-country", "ES");
            map.add("id-number", contract.getClientId() + "-" + contract.getLineId());
            map.add("type", "2");
            map.add("email", contract.getClient().getEmail());
            map.add("phone-number", contract.getClient().getMobile());

            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.add("Authorization", "Basic " + authString);

            requestEntity = new HttpEntity<>(map, headers);
            result = template.postForEntity(signersUrl, requestEntity, String.class);

            String ret = result.getHeaders().getFirst("Location");

            contract.setRemotePath(ret);
            contract.setStatus(ContractStatus.sended.name());

        } catch (RestClientException e) {
            log.error("Error on requesting signature client " + contract.getClient() + " " + contract.getLineId(), e);
            Ticket t = new Ticket(TicketType.contract.name(), contract.getId(), "Error enviando el contrato al portal de firmas", "system", "ADMIN");
            ticketsRepository.save(t);
            contract.setRemotePath("Error:" + e.getMessage());
            contract.setStatus(ContractStatus.error.name());
        }

    }

    public File downloadDocument(Contract contract) {

        File downloaded = null;
        String url = contract.getRemotePath();
        url = url.substring(0, url.indexOf("/signers"));
        url += ".pdf";

        String path = contract.getPath();
        path = path.substring(0, path.indexOf(".pdf"));
        path += "_firmado.pdf";

        File cc = new File(path);
        if (cc.exists()) {
            downloaded = cc;
        } else {
            try {

                URL website = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) website.openConnection();
                connection.setRequestProperty("Authorization", "Basic " + authString);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/pdf");
                ReadableByteChannel rbc = Channels.newChannel(connection.getInputStream());
                FileOutputStream fos = new FileOutputStream(path);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                fos.close();
                downloaded = new File(path);

            } catch (IOException e) {
                log.error("Error downloading signed contract", e);
            }
        }
        return downloaded;
    }

    public ContractStatusResponse checkContractStatus(Contract contract) {

        ContractStatusResponse ret = null;
        if (contract.getRemotePath() != null) {
            String url = contract.getRemotePath();
            url = url.substring(0, url.indexOf("/signers"));

            RestTemplate template = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + authString);

            try {
                HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(headers);
                ResponseEntity<ContractStatusResponse> response = template.exchange(url, HttpMethod.GET, requestEntity, ContractStatusResponse.class);
                ret = response.getBody();

            } catch (RestClientException e) {
                log.error("Error getting contract status " + e.getMessage());
            }
        }

        return ret;

    }

    public String postTest() {

        RestTemplate template = new RestTemplate();

        File file = new File("/var/wings/contract/condiciones.pdf");

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new FileSystemResource(file));
        map.add("description", "Firmado digital - Test Prueba");
        map.add("reference", "TEST_SIGNATURE_signature");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", "Basic " + authString);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        String ret = "";
        try {
            ResponseEntity<String> result = template.postForEntity(postUrl, requestEntity, String.class);
            String url = result.getHeaders().getFirst("Location");

            //add signer
            String signersUrl = url + "signers/";
            template = new RestTemplate();

            map = new LinkedMultiValueMap<>();
            map.add("name", "Sebastian Lucero");
            map.add("email", "contratacion@wingsmobile.es");
            map.add("id-country", "PE");
            //map.add("id-number", "1");
            map.add("type", "2");
            //map.add("phone-number", "693909139");

            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.add("Authorization", "Basic " + authString);

            requestEntity = new HttpEntity<>(map, headers);
            result = template.postForEntity(signersUrl, requestEntity, String.class);

            ret = result.getHeaders().getFirst("Location");
        } catch (RestClientException e) {
            log.error("Error on requesting signature client Test", e);
        }

        return ret;
    }
}
