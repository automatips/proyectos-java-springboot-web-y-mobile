/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.LineSpecs;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.ClientLineHolder;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.cs.Message;
import com.wings.base.domain.registration.RegistrationFile;
import com.wings.base.domain.xtra.XtraLineStatus;
import com.wings.base.enums.ContractStatus;
import com.wings.base.enums.LineStatus;
import com.wings.base.process.LineProcess;
import com.wings.base.process.XtraProccess;
import com.wings.base.repos.ClientLineHoldersRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.ContractsRepository;
import com.wings.base.repos.CsMessageRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.RegistrationFilesRepository;
import com.wings.base.service.ContractService;
import com.wings.base.service.XtraService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.wings.base.service.xtra.XClientService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.MapUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author seba
 */
@Controller
public class LinesController {

    private final Logger log = LoggerFactory.getLogger(LinesController.class);

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private PlanRepository plansRepository;

    @Autowired
    private XtraService xtraService;

    @Autowired
    private LinesRepository lineRepository;

    @Autowired
    private XClientService xClientService;

    @Autowired
    private XPortService xPortService;

    @Autowired
    private XLineService xLineService;

    @Autowired
    private XtraProccess xtraProccess;

    @Autowired
    private LineProcess lineProcess;

    @Autowired
    private ClientLineHoldersRepository clientLineHoldersRepository;

    @Autowired
    private RegistrationFilesRepository registrationFilesRepository;

    @Autowired
    private ContractService contractService;

    @Autowired
    private ContractsRepository contractsRepository;

    @Autowired
    private CsMessageRepository csMessageRepository;

    @RequestMapping(value = {"views/clients/lines"}, method = RequestMethod.GET)
    public String getRegistrarions(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));
        Page<ClientLine> items = lineRepository.findAll(LineSpecs.lineSearch(q, status), pr);
        PageWrapper<ClientLine> pageWrapper = new PageWrapper<>(items, "views/clients/lines");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("q", q);

        return "views/clients/lines";
    }

    @RequestMapping(value = {"views/clients/lines/{id}/form"}, method = RequestMethod.GET)
    public String getLineForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        ClientLine clientLine = lineRepository.findOne(id);
        model.addAttribute("title", "Linea");
        model.addAttribute("subTitle", clientLine.toString());
        model.addAttribute("elements", MapUtils.getFormElements(clientLine));

        return "views/common/view-form";
    }

    @RequestMapping(value = {"views/clients/lines/{id}/edit"}, method = RequestMethod.GET)
    public String getLineEditForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        ClientLine clientLine = lineRepository.findOne(id);
        model.addAttribute("title", "Linea");
        model.addAttribute("subTitle", clientLine.toString());
        model.addAttribute("clientLine", clientLine);

        model.addAttribute("elements", MapUtils.getFormElementsFiltered(clientLine, Arrays.asList("iccid", "portIccid", "referer")));
        model.addAttribute("postUrl", "views/clients/lines/" + clientLine.getId() + "/edit");

        return "views/clients/lines/line-edit-form";
    }

    @RequestMapping(value = {"views/clients/lines/{id}/edit"}, method = RequestMethod.POST)
    public String postLineEdit(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("id") Long id) {

        String referer = request.getParameter("referer");
        String iccid = request.getParameter("iccid");
        String portIccid = request.getParameter("portIccid");

        if (referer != null && referer.trim().isEmpty()) {
            referer = null;
        }

        /*String msg = "";
        boolean error = false;
        if (iccid == null) {
            msg += "El iccid no puede ser nulo";
            error = true;
        }

        if (iccid != null && iccid.length() < 19 || !iccid.startsWith("8934")) {
            msg += "El iccid tiene que tener 19 caracteres y debe comenzar por 8934";
            error = true;
        }

        if (portIccid != null && !portIccid.trim().isEmpty()) {
            if (portIccid.length() < 19 || !portIccid.startsWith("8934")) {
                msg += "El iccid de portabilidad tiene que tener 19 caracteres y debe comenzar por 8934";
                error = true;
            }
        }*/
        ClientLine clientLine = lineRepository.findOne(id);

        StringBuilder b = new StringBuilder();
        b.append("Modificacion de Linea:").append(System.lineSeparator());
        String antes = new ReflectionToStringBuilder(clientLine).toString();

        clientLine.setIccid(iccid);
        clientLine.setPortIccid(portIccid);
        clientLine.setReferer(referer);
        lineRepository.save(clientLine);

        String despues = new ReflectionToStringBuilder(clientLine).toString();
        String diff = com.wings.base.utils.StringUtils.getDiff(antes, despues);
        b.append(System.lineSeparator()).append(diff);
        if (!diff.isEmpty()) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(((User) auth.getPrincipal()).getUsername());
            message.setClientId(clientLine.getClientId());
            message.setLineId(clientLine.getId());
            message.setLine(clientLine.getNumber().toString());
            message.setText(b.toString());
            csMessageRepository.save(message);
        }

        redirectAttributes.addAttribute("id", clientLine.getClientId());
        redirectAttributes.addAttribute("msg", "Linea " + clientLine.getNumber() + " modificada");
        
        return "redirect:/#views/support/clients?msg={msg}&q=id:{id}";
    }
    
    @RequestMapping(value = {"views/clients/lines/{id}/timeline"}, method = RequestMethod.GET)
    public String getLineTimeline(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        /*ClientLine clientLine = lineRepository.findOne(id);
        model.addAttribute("title", "Linea");
        model.addAttribute("subTitle", clientLine.toString());
        model.addAttribute("elements", MapUtils.getFormElements(clientLine));*/
        return "views/fragments/timeline";
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines"}, method = RequestMethod.GET)
    public ResponseEntity<List<ClientLine>> getLines(Model model, HttpServletRequest request) {
        List<ClientLine> lines = lineRepository.findAll();
        return ResponseEntity.ok(lines);
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/requestportability"}, method = RequestMethod.POST)
    public ResponseEntity<String> requestPortability(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        String ret = "";
        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        for (Long id : ids) {
            ClientLine line = lineRepository.findOne(id);

            if (!line.getInternalState().equals(LineStatus.order_delivered.name()) && !line.getInternalState().equals(LineStatus.port_rejected.name())) {
                ret += "\n La linea no tiene el estado correspondiente para hacer la portabilidad";
                continue;
            }

            Plan plan = plansRepository.findOne(line.getPlanId());

            if (line.getIccid() == null) {
                ret += "\nNo se encontró iccid para hacer la portabilidad";
                continue;
            }

            if (line.isPort()) {

                //seteo la fecha
                log.error("Planned Date" + line.getPortPlannedDate());
                line.setPortPlannedDate(DateUtils.validatePortabilityDate(line.getPortPlannedDate()));
                log.error("Planned Date After" + line.getPortPlannedDate());

                xPortService.portClient(line, line.getClient(), plan);
                ret += "\n" + line.getExternalState();
                lineRepository.save(line);
                ret += "\nSolicitud enviada";
            }
        }

        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/requestportability-holder"}, method = RequestMethod.POST)
    public ResponseEntity<String> requestPortabilityWithHolder(Model model, HttpServletRequest request, @RequestParam("id") Long id) {

        ClientLine line = lineRepository.findOne(id);
        if (line == null) {
            return ResponseEntity.badRequest().body("No se encontro la linea");
        }

        List<ClientLineHolder> holders = clientLineHoldersRepository.findByLineId(id);
        if (holders.isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontro titular");
        }

        if (holders.size() > 1) {
            return ResponseEntity.badRequest().body("Existe mas de un titlar para esa linea, no se puede continuar");
        }

        ClientLineHolder holder = holders.get(0);
        if (line.getIccid() == null) {
            return ResponseEntity.badRequest().body("No se encontro el iccid para hacer la portabilidad");
        }

        if (line.getInternalState().equals(LineStatus.active.name())) {
            return ResponseEntity.badRequest().body("La linea ya esta activa no se puede portar");
        }

        if (line.isPort()) {
            try {
                line.setPortPlannedDate(DateUtils.validatePortabilityDate(line.getPortPlannedDate()));
                xPortService.portClient(line, holder);
                lineRepository.save(line);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body("Error solicitando la portabilidad:" + e.getMessage());
            }
        }

        return ResponseEntity.ok("La portabilidad se solicito");
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/brand-change"}, method = RequestMethod.POST)
    public ResponseEntity<String> requestBrandChange(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        String ret = "";
        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        for (Long id : ids) {
            ClientLine line = lineRepository.findOne(id);

            Plan plan = plansRepository.findOne(line.getPlanId());
            if (line.getIccid() == null) {
                ret += "<br/>No se encontró iccid para hacer el cambio de marca";
                continue;
            }

            if (line.isPort()) {
                line.setPlan(plan);
                xtraService.requestBrandChange(line);
                ret += "<br/>Solicitud enviada";
            } else {
                ret += "<br/>La linea no es de portabilidad";
            }
        }

        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/status/xtra/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> getLineStatus(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        String ret = "";
        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        for (Long id : ids) {
            ClientLine line = lineRepository.findOne(id);
            List<XtraLineStatus> stats = xLineService.findLines(line.getClient().getRefCustomerId(), "A");

        }

        return ResponseEntity.ok().build();
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/create/xtra/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<List<ClientLine>> createClientsXtra(Model model, HttpServletRequest request, @PathVariable("id") Long clientId) {

        Client client = clientsRepository.findOne(clientId);
        if (client.getRefCustomerId() == null || client.getRefCustomerId().isEmpty()) {
            xClientService.createClient(client);
            clientsRepository.save(client);
        }

        //Portabilidad        
        List<ClientLine> lines = lineRepository.findByClientId(client.getId());
        for (ClientLine line : lines) {
            Plan plan = plansRepository.findOne(line.getPlanId());
            if (line.getIccid() == null) {
                continue;
            }
            if (!line.getInternalState().equals(LineStatus.order_delivered.name())) {
                continue;
            }
            if (line.isPort()) {
                xPortService.portClient(line, client, plan);
                lineRepository.save(line);
            } else {
                xLineService.createLine(client, line, plan);
            }
        }

        //new Lines

        /*List<ClientLine> newLines = lineRepository.findByPort(Boolean.FALSE);
        for (ClientLine newLine : newLines) {
            
            if(!newLine.getInternalState().equals("local_create")){
                continue;
            }         
            
            Client c = clientsRepository.findOne(newLine.getClientId());
            Plan plan = plansRepository.findOne(newLine.getPlanId());
            xLineService.createLine(c, newLine, plan);
            
            lineRepository.save(newLine);
            
        }*/
        //riesgo
        /*List<ClientLine> lines = lineRepository.findAll();
        for (ClientLine line : lines) {
            
            if(line.getNumber() == null){
                continue;
            }
            
            Client c = clientsRepository.findOne(line.getClientId());
            Plan plan = plansRepository.findOne(line.getPlanId());

            int amount = 20;
            if (plan.getPrice() > 20) {
                amount = 25;
            }
            
            xRiskService.updateRisk(c.getRefCustomerId(), line.getNumber().toString(), "M", String.valueOf(amount));            

        }*/
        return ResponseEntity.ok().build();
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/check-status"}, method = RequestMethod.POST)
    public ResponseEntity<String> checkLineStatus(Model model, HttpServletRequest request) {
        new Thread(() -> {
            xtraProccess.checkCreatedLineStatus();
        }).start();
        return ResponseEntity.ok("Solicitud Enviada");
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/check-port-status"}, method = RequestMethod.POST)
    public ResponseEntity<String> checkLinePortStatus(Model model, HttpServletRequest request) {
        new Thread(() -> {
            xtraProccess.checkPortStatus();
        }).start();
        return ResponseEntity.ok("Solicitud Enviada");
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/process-payment"}, method = RequestMethod.POST)
    public ResponseEntity<String> processPaymnetAuths(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        List<ClientLine> lines = lineRepository.findAll(ids);
        new Thread(() -> {
            lineProcess.processPaymentAuthorization(lines, launcher);
        }).start();
        return ResponseEntity.ok("Solicitud Enviada");

    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/regenerate-contract"}, method = RequestMethod.POST)
    public ResponseEntity<String> regenerateContract(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        List<ClientLine> lines = lineRepository.findAll(ids);
        for (ClientLine line : lines) {
            Long id = line.getRegistrationId();
            //Registration reg = registrationRepository.findOne(id);
            List<RegistrationFile> files = registrationFilesRepository.findByRegistrationId(id);
            File contractFile = contractService.getContract(line, files);

            Contract contract = new Contract();
            contract.setActive(false);
            contract.setClientId(line.getClientId());
            contract.setCreationDate(new Date());
            contract.setLineId(line.getId());
            contract.setPath(contractFile.getAbsolutePath());
            contract.setStatus(ContractStatus.recreated.name());
            contractsRepository.save(contract);
        }
        return ResponseEntity.ok("Solicitud Enviada");
    }

    @ResponseBody
    @RequestMapping(value = {"api/lines/orders/create/replacement"}, method = RequestMethod.POST)
    public ResponseEntity<String> createOrderReplacement(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        String ret = "";
        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        if (ids.isEmpty()) {
            return ResponseEntity.badRequest().body("Debe seleccionar al menos una línea");
        }

        List<ClientLine> lines = lineRepository.findAll(ids);
        if (lines.isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontraron líneas");
        }

        new Thread(() -> {
            lineProcess.createOrdersReplacement(launcher, lines);
        }).start();

        return ResponseEntity.ok(ret);
    }

}
