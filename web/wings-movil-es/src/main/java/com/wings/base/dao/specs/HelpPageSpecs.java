/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.cs.HelpPage;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class HelpPageSpecs {

    public static Specification<HelpPage> search(String q) {

        return (Root<HelpPage> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("title"), search),
                                builder.like(root.get("content"), search),
                                builder.like(root.get("canonicalTitle"), search)
                        )
                );
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
