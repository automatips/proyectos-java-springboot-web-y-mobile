/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.process;

/**
 *
 * @author seba
 */
public class SystemStatus {

    private String process;
    private String status;
    private int percent;

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "<pre>Proceso:" + process + System.lineSeparator()
                + "Estado: " + status + " " + percent + "%";
    }

}
