/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.cs.Message;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class MessageSpecs {

    public static Specification<Message> incidenceSearch(Boolean incidence, String q, String status) {

        return (Root<Message> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Contract, Client> client = root.join("client");
            List<Predicate> predicates = new ArrayList<>();

            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(client.get("name"), search),
                                builder.like(client.get("lastname"), search),
                                builder.like(client.get("email"), search),
                                builder.like(client.get("idValue"), search),
                                builder.like(client.get("companyName"), search),
                                builder.like(client.get("companyIdValue"), search),
                                builder.like(builder.concat(builder.concat(client.get("name"), " "), client.get("lastname")), search)
                        )
                );
            }

            if (status != null) {
                String[] statuses = status.split(",");
                Expression<String> exp = root.get("incidenceStatus");
                predicates.add(exp.in(Arrays.asList(statuses)));
            }

            if (incidence != null) {
                predicates.add(builder.equal(root.get("incidence"), incidence));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
