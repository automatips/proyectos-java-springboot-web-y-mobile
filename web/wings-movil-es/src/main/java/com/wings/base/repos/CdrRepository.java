/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.Cdr;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface CdrRepository extends JpaRepository<Cdr, Long>, JpaSpecificationExecutor<Cdr> {

    List<Cdr> findAllByClient(String refCustomerId);

    Page<Cdr> findByClient(String refCustomerId, Pageable pageable);

    Page<Cdr> findByDateBetween(Date from, Date to, Pageable pageable);

    Page<Cdr> findByClientAndDateBetween(String refCustomerId, Date from, Date to, Pageable pageable);

}
