/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.admin;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "tickets")
public class Ticket {

    public Ticket() {
    }

    public Ticket(String type, Long idValue, String comment, String user, String role) {
        this.type = type;
        this.idValue = idValue;
        this.comment = comment;
        this.user = user;
        this.role = role;
        this.creationDate  = new Date();
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;    
    private String type;
    private Long idValue;
    private String comment;
    private String user;
    private boolean solved;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date solvedDate;
    private String solvedUser;
    private String solvedComment;
    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setIdValue(Long idValue) {
        this.idValue = idValue;
    }

    public Long getIdValue() {
        return idValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    public Date getSolvedDate() {
        return solvedDate;
    }

    public void setSolvedDate(Date solvedDate) {
        this.solvedDate = solvedDate;
    }

    public String getSolvedUser() {
        return solvedUser;
    }

    public void setSolvedUser(String solvedUser) {
        this.solvedUser = solvedUser;
    }

    public String getSolvedComment() {
        return solvedComment;
    }

    public void setSolvedComment(String solvedComment) {
        this.solvedComment = solvedComment;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
}
