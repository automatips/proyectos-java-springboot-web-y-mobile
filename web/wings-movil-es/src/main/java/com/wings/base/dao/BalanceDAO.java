/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao;

import com.wings.base.domain.client.BalanceSums;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class BalanceDAO {

    @Autowired
    private EntityManager em;
    
    public BalanceSums find(String client, String line, Integer month, Integer year) {
        
        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.base.domain.client.BalanceSums(sum(c.amount) as total,");
        b.append("count(1) as records) ");
        b.append("FROM Balance c "); 
        b.append("WHERE 1=1 ");
        
        if(client != null){
            b.append(" AND client_id = '").append(client).append("'");
        }
        
        if(line != null){
            b.append(" AND line_id = '").append(line).append("'");
        }
        
        if(month != null){
            b.append(" AND month = '").append(month).append("'");
        }
        
        if(year != null){
            b.append(" AND year = '").append(year).append("'");
        }
        
        
        Query query = em.createQuery(b.toString(),BalanceSums.class);        
        BalanceSums ret = (BalanceSums)query.getSingleResult();
        
        return ret;
    }

}
