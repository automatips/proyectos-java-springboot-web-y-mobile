/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.ibm.icu.text.SimpleDateFormat;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.wings.base.WingsProperties;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ContractService {

    private final Logger log = LoggerFactory.getLogger(ContractService.class);

    private final String direccionWings;
    private final String telefonoWings;
    private final String condiciones;
    private final String destDir;
    private final String logo;
    private final String contrato;

    private final Map<String, String> dnis = new HashMap<>();

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private final SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    public ContractService(WingsProperties wingsProperties) {
        direccionWings = wingsProperties.getContract().getDireccionWings();
        telefonoWings = wingsProperties.getContract().getTelefonoWings();
        condiciones = wingsProperties.getContract().getCondiciones();
        destDir = wingsProperties.getContract().getDest();
        logo = wingsProperties.getLogo();
        contrato = wingsProperties.getContrato();

        dnis.put("1", "NIF");
        dnis.put("2", "NIE");
        dnis.put("3", "Pasaporte");
        dnis.put("4", "CIF");
    }

    /*public File getContract(Registration registration, List<RegistrationFile> images) {
        return generate(registration, images);
    }*/
    public File getContract(ClientLine line, List<RegistrationFile> images) {
        File ret = null;
        String destName = destDir + "/" + line.getRegistrationId() + "_contrato_cliente.pdf";
        Map<String, Object> map = new HashMap<>();

        try {
            String foto1 = null;
            String foto2 = null;

            if (images.size() > 0) {
                foto1 = images.get(0).getPath();
            }

            if (images.size() > 1) {
                foto2 = images.get(1).getPath();
            }

            Client client = line.getClient();

            map.put("nombre", client.getName());
            map.put("apellido", client.getLastname());
            map.put("nroContrato", line.getRegistrationId() + "" + client.getId() + "" + line.getId());
            map.put("fecha", sdf.format(client.getCreationDate()));
            map.put("hora", sdfHora.format(client.getCreationDate()));
            map.put("nroCliente", client.getId());
            map.put("nroDoc", client.getIdType() + " " + client.getIdValue());
            //map.put("nacionalidad", "");
            map.put("telefono", line.getNumber() != null ? line.getNumber().toString() : client.getPhone());
            map.put("fechaNac", sdf.format(client.getBirthDate()));
            map.put("direccion", client.getAddress());
            map.put("ciudad", client.getCity());
            map.put("provincia", client.getState());
            map.put("cp", client.getPostalCode());
            map.put("email", client.getEmail());
            map.put("plan", line.getPlan().getShortName());
            map.put("planDetalle", line.getPlan());

            String empresa = "";
            String empresa2 = "";
            map.put("tipoCliente", "Particular");
            if (client.getCompanyName() != null && !client.getCompanyName().trim().isEmpty()) {
                empresa = client.getCompanyName() + " - " + client.getCompanyIdType() + ": " + client.getCompanyIdValue();
                empresa2 = " / " + client.getCompanyName();
                map.put("nroDoc", client.getIdType() + " " + client.getIdValue() + " / " + client.getCompanyIdType() + ": " + client.getCompanyIdValue());
                map.put("tipoCliente", "Empresa");
            }
            
            map.put("empresa", empresa);
            map.put("empresa2", empresa2);

            map.put("direccionWings", direccionWings);
            map.put("telefonoWings", telefonoWings);

            map.put("logo", logo);
            //map.put("bk", bk.getFile().getAbsolutePath());
            map.put("foto1", foto1);
            map.put("foto2", foto2);

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(contrato));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, map, new JREmptyDataSource());

            File f = File.createTempFile("temp_contract_", ".pdf");
            JasperExportManager.exportReportToPdfFile(jprint, f.getAbsolutePath());

            mergeCondiciones(f.getAbsolutePath(), destName);
            f.delete();

            ret = new File(destName);

        } catch (JRException | IOException e) {
            log.error("Error generating pdf ", e);
        }

        return ret;
    }

    /*private File generate(Registration registration, List<RegistrationFile> images) {
        File ret = null;
        String destName = destDir + "/" + registration.getId() + "_contrato_cliente.pdf";
        Map<String, Object> map = new HashMap<>();

        try {
            String foto1 = null;
            String foto2 = null;

            if (images.size() > 0) {
                foto1 = images.get(0).getPath();
            }

            if (images.size() > 1) {
                foto2 = images.get(1).getPath();
            }

            map.put("nombre", WordUtils.capitalizeFully(registration.getNombre()));
            map.put("apellido", WordUtils.capitalizeFully(registration.getApellido()));
            map.put("nroContrato", registration.getMatchingData());
            map.put("fecha", sdf.format(registration.getFechaCreacion()));
            map.put("hora", sdfHora.format(registration.getFechaCreacion()));
            map.put("nroCliente", registration.getId());
            map.put("nroDoc", dnis.get(registration.getTipoDoc()) + " " + registration.getNroDoc().toUpperCase());
            //map.put("nacionalidad", "");
            map.put("telefono", registration.getTelefono());
            map.put("fechaNac", sdf.format(registration.getFechaNac()));
            map.put("direccion", WordUtils.capitalizeFully(registration.getDireccion()));
            map.put("ciudad", WordUtils.capitalizeFully(registration.getCiudad(), new char[]{'´', '\''}));
            map.put("provincia", WordUtils.capitalizeFully(registration.getProvincia()));
            map.put("cp", registration.getCp());
            map.put("email", registration.getEmail().toLowerCase());
            map.put("plan", registration.getPlan());
            map.put("planDetalle", registration.getPlanDesc());

            String empresa = "";
            String empresa2 = "";
            if (registration.isEmpresa()) {
                empresa = registration.getRazonSocial() + " - " + dnis.get(registration.getTipoDocEmpresa()) + ": " + registration.getNroDocEmpresa().toUpperCase();
                empresa2 = " / " + registration.getRazonSocial();
            }
            map.put("empresa", empresa);
            map.put("empresa2", empresa2);

            map.put("direccionWings", direccionWings);
            map.put("telefonoWings", telefonoWings);

            map.put("logo", logo);
            //map.put("bk", bk.getFile().getAbsolutePath());
            map.put("foto1", foto1);
            map.put("foto2", foto2);

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(contrato));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, map, new JREmptyDataSource());

            File f = File.createTempFile("temp_contract_", ".pdf");
            JasperExportManager.exportReportToPdfFile(jprint, f.getAbsolutePath());

            mergeCondiciones(f.getAbsolutePath(), destName);
            f.delete();

            ret = new File(destName);

        } catch (JRException | IOException e) {
            log.error("Error generating pdf ", e);
        }

        return ret;
    }*/

    private void mergeCondiciones(String temp, String dest) {

        try {

            Document document = new Document();
            PdfCopy copy = new PdfSmartCopy(document, new FileOutputStream(dest));
            document.open();

            PdfReader reader = new PdfReader(temp);
            copy.addDocument(reader);
            reader.close();

            PdfReader reader2 = new PdfReader(condiciones);
            copy.addDocument(reader2);
            reader2.close();

            document.close();

        } catch (DocumentException | IOException e) {
            log.error("Error generating pdf ", e);
        }

    }

}
