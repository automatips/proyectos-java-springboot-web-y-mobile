/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;


import com.wings.base.domain.client.Plan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface PlanRepository extends JpaRepository<Plan, Long>{

    Plan findByShortName(String shortName);
    
}
