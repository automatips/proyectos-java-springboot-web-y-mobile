/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.WingsProperties;
import com.wings.base.domain.payment.PaymentCallback;
import com.wings.base.domain.payment.PaymentRequest;
import com.wings.base.domain.payment.PaymentResponse;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Component
public class TefPayService {

    private final Logger log = LoggerFactory.getLogger(TefPayService.class);

    private final HttpHeaders headers;
    private final WingsProperties properties;
    private final String paymentUrl;
    private final String merchantKey;

    public TefPayService(WingsProperties properties) {
        this.headers = new HttpHeaders();
        headers.add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.220 Safari/535.1");
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        headers.add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,/;q=0.8");
        headers.add("Accept-Encoding", "gzip,deflate");
        headers.add("Accept-Language", "es-ES,es;q=0.8 Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3");

        this.properties = properties;
        this.paymentUrl = properties.getPay().getUrlAuto();
        this.merchantKey = properties.getPay().getMerchantKey();

    }

    public void postChargeAmmount(PaymentRequest request) {

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate rest = new RestTemplate(clientHttpRequestFactory);
        Jaxb2RootElementHttpMessageConverter xmlMessageConverter = new Jaxb2RootElementHttpMessageConverter();
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.TEXT_HTML);
        xmlMessageConverter.setSupportedMediaTypes(mediaTypes);
        rest.getMessageConverters().add(xmlMessageConverter);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("Ds_Merchant_TransactionType", request.getTransactionType());
        map.add("Ds_Merchant_MatchingData", request.getMatchingData());
        map.add("Ds_Merchant_MerchantCode", request.getMerchantCode());
        map.add("Ds_Merchant_Amount", request.getAmount());
        map.add("Ds_Date", request.getDate());
        map.add("Ds_Merchant_PanMask", request.getPanMask());
        map.add("Ds_Merchant_MerchantSignature", request.getSignature());
        map.add("Ds_Merchant_Recurrency", request.getRecurrency());
        map.add("Ds_Merchant_Url", request.getUrl());
        map.add("Ds_CostumerCreditCardId", request.getCardId());
        map.add("Ds_AuthorisationCode", request.getAuthCode());
        map.add("Ds_Merchant_UserName", request.getName());

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, headers);

        try {
            URI uri = new URI(paymentUrl);
            ResponseEntity<PaymentResponse> response = rest.exchange(uri, HttpMethod.POST, requestEntity, PaymentResponse.class);
            PaymentResponse payResponse = response.getBody();

            log.error("Payment status code " + response.getStatusCodeValue());
            log.error("Payment status response code" + response.getBody().getDs_Code());
            log.error("Payment status response message" + response.getBody().getDs_Message());

            if (response.getStatusCode().is2xxSuccessful()) {
                request.setStatus(payResponse.getDs_Message());
                request.setStatusCode(payResponse.getDs_Code());
                request.setBank(payResponse.getDs_Bank());
                request.setExpiry(payResponse.getDs_Expiry());
                request.setGuarantees(payResponse.getDs_Merchant_Guarantees());
            } else {
                request.setStatus(response.getStatusCode().getReasonPhrase());
                request.setStatusCode(response.getStatusCodeValue() + "");
            }

        } catch (Exception e) {
            request.setStatus(e.getMessage());
            request.setStatusCode("ERROR");
            log.error("Error on payment auto", e);
        }
    }

    public PaymentRequest postRefundAmmount(PaymentRequest refundRequest, PaymentCallback callback) {

        //set signature
        String signature = DigestUtils.sha1Hex("4" + refundRequest.getAmount() + refundRequest.getMerchantCode() + refundRequest.getMatchingData() + merchantKey);
        refundRequest.setSignature(signature);

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate rest = new RestTemplate(clientHttpRequestFactory);
        Jaxb2RootElementHttpMessageConverter xmlMessageConverter = new Jaxb2RootElementHttpMessageConverter();
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.TEXT_HTML);
        xmlMessageConverter.setSupportedMediaTypes(mediaTypes);
        rest.getMessageConverters().add(xmlMessageConverter);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

        map.add("Ds_Merchant_TransactionType", refundRequest.getTransactionType());
        map.add("Ds_Merchant_MerchantCode", refundRequest.getMerchantCode());
        map.add("Ds_Merchant_MatchingData", refundRequest.getMatchingData());
        map.add("Ds_Merchant_PanMask", refundRequest.getPanMask());
        map.add("Ds_Date", callback.getDate());
        map.add("Ds_Merchant_Amount", refundRequest.getAmount());
        map.add("Ds_Merchant_UserName", refundRequest.getName());
        map.add("Ds_Merchant_MerchantSignature", signature);

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, headers);
        try {
            URI uri = new URI(paymentUrl);
            ResponseEntity<PaymentResponse> response = rest.exchange(uri, HttpMethod.POST, requestEntity, PaymentResponse.class);

            PaymentResponse payResponse = response.getBody();
            if (response.getStatusCode().is2xxSuccessful()) {
                refundRequest.setStatus(payResponse.getDs_Message());
                refundRequest.setStatusCode(payResponse.getDs_Code());
                refundRequest.setBank(payResponse.getDs_Bank());
                refundRequest.setExpiry(payResponse.getDs_Expiry());
                refundRequest.setGuarantees(payResponse.getDs_Merchant_Guarantees());
            } else {
                refundRequest.setStatus(response.getStatusCode().getReasonPhrase());
                refundRequest.setStatusCode(response.getStatusCodeValue() + "!");
            }
        } catch (Exception e) {
            refundRequest.setStatus(e.getMessage());
            refundRequest.setStatusCode("ERROR");
            log.error("Error on refund auto", e);
        }

        return refundRequest;
    }
}
