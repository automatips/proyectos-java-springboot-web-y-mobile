/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.PlanChange;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface PlanChangesRepository extends JpaRepository<PlanChange, Long>, JpaSpecificationExecutor<PlanChange> {

    List<PlanChange> findByLineId(Long lineId);

    List<PlanChange> findByLineIdAndOldPlanIdAndNewPlanId(Long lineId, Long newPlanId, Long oldPlanId);

    List<PlanChange> findByOldPlanIdAndNewPlanId(Long newPlanId, Long oldPlanId);

}
