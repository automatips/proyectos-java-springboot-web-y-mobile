/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.api;

import com.wings.base.api.dto.ExternalInvoice;
import com.wings.base.api.dto.ExternalInvoiceItem;
import com.wings.base.dao.BalanceDAO;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.BalanceSums;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class ExternalInvoiceController {

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private BalanceDAO balanceDAO;

    @ResponseBody
    @RequestMapping(value = {"/api/services/invoices"}, method = RequestMethod.POST)
    public ResponseEntity<List<ExternalInvoice>> getServiceOrders(HttpServletRequest request, @RequestBody List<Long> ids) {

        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("a0e3fdf8e14cf3b7842a3d715e100faccac0efdb49e9fe510fd33b6ea603bd1a")) {
            return ResponseEntity.badRequest().build();
        }

        Set<Long> clientsIds = new HashSet<>();
        List<ClientLine> lines = linesRepository.findAll(ids);
        lines.forEach((line) -> {
            clientsIds.add(line.getClientId());
        });

        List<Long> idsToQuery = new ArrayList<>(clientsIds);
        List<String> status = new ArrayList<>();
        status.add(InvoiceStatus.created.name());
        status.add(InvoiceStatus.partial.name());
        status.add(InvoiceStatus.payed.name());
        status.add(InvoiceStatus.unpayed.name());

        List<Invoice> invoices = invoiceRepository.findByClientIdInAndStatusIn(idsToQuery, status);

        List<ExternalInvoice> ret = new ArrayList<>();
        for (Invoice invoice : invoices) {

            ExternalInvoice ext = new ExternalInvoice();
            ext.setClientAddress(invoice.getClientAddress());
            ext.setClientCity(invoice.getClientCity());
            ext.setClientDni(invoice.getClientDni());
            ext.setClientName(invoice.getClientName());
            ext.setClientState(invoice.getClientState());
            ext.setCompanyName(invoice.getClient().getCompanyName());
            ext.setCreationDate(invoice.getInvoiceDate());
            ext.setFilePath(null);
            ext.setImponible(invoice.getTotalTextWithoutIva());
            ext.setInvoiceDate(invoice.getInvoiceDate());
            ext.setInvoiceNumber(invoice.getInvoiceNumber());
            ext.setIva(invoice.getTotalIva());
            ext.setLastSended(null);
            ext.setRetenciones("0.0");
            ext.setStatus(invoice.getStatus());
            ext.setTotal(invoice.getTotal());
            ext.setTotalText(invoice.getTotalText());
            ext.setType("service");
            ext.setTypeId(invoice.getId());
            ext.setToken(invoice.getToken());

            if (invoice.getClientName() == null || invoice.getClientName().isEmpty()) {
                Client client = invoice.getClient();

                ext.setClientAddress(client.getAddress());
                ext.setClientCity(client.getCity());
                ext.setClientDni(client.getIdValue());
                ext.setClientName(client.getName() + " " + client.getLastname());
                ext.setClientState(client.getState());
                ext.setCompanyName(client.getCompanyName());

            }

            List<ExternalInvoiceItem> extItems = new ArrayList<>();
            invoice.getItems().forEach((item) -> {
                ExternalInvoiceItem extItem = new ExternalInvoiceItem();
                extItem.setAmount(item.getAmount());
                extItem.setConcept(item.getConcept());
                extItems.add(extItem);
            });
            ext.setItems(extItems);

            ret.add(ext);

        }

        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"/invoices/external/payed"}, method = RequestMethod.POST)
    public ResponseEntity<String> markAsPayed(HttpServletRequest request, @RequestBody List<Long> invoiceIds) {

        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("a0e3fdf8e14cf3b7842a3d715e100faccac0efdb49e9fe510fd33b6ea603bd1a")) {
            return ResponseEntity.badRequest().body("Unauthorized");
        }

        List<Invoice> invoices = invoiceRepository.findAll(invoiceIds);
        for (Invoice invoice : invoices) {

            Client client = invoice.getClient();
            List<ClientLine> lines = linesRepository.findByClientId(client.getId());
            for (ClientLine line : lines) {

                if (invoice.getStatus().equals(InvoiceStatus.payed.name())) {
                    continue;
                }

                BalanceSums sum = balanceDAO.find(client.getId().toString(), line.getId().toString(), invoice.getMonth(), invoice.getYear());
                if (sum.getTotal() == null || sum.getTotal() <= 0) {
                    continue;
                }

                Balance b = new Balance();
                b.setAmount(sum.getTotal() * -1);
                b.setClientId(client.getId());
                b.setControl("auto-payment-referal-" + invoice.getMonth() + "-" + invoice.getYear() + "-#" + line.getId());
                b.setDate(new Date());
                b.setLineId(line.getId());
                b.setDescription("Cobro automático fondos referido");
                b.setObservation("Cobro automático fondos referido " + new Date());
                b.setMonth(invoice.getMonth());
                b.setYear(invoice.getYear());
                b.setPaymentMethod("referal funds");
                b.setType(BalanceType.discount.name());
                balanceRepository.save(b);

            }

            invoice.setStatus(InvoiceStatus.payed.name());
            invoiceRepository.save(invoice);

        }

        return ResponseEntity.ok("");
    }

}
