/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.registration.Registration;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author seba
 */
public interface RegistrationRepository extends PagingAndSortingRepository<Registration, Long> {
    
    
    Long countByStatus(String status);

    Registration getOneBySessionId(String sessionId);

    List<Registration> findByStatus(String status);
    
    @Query(
            value = "select * from registration where nombre like %?1 OR apellido like %?1 OR email like %?1 OR empresa like %?1  ORDER BY id DESC \n#pageable\n",
            countQuery = "select count(1) from registration where nombre like %?1 OR apellido like %?1 OR email like %?1 OR empresa like %?1",
            nativeQuery = true
    )            
    Page<Registration> findByQuery(String q, Pageable pageable);
    
    @Query(
            value = "select * from registration where (nombre like %?1 OR apellido like %?1 OR email like %?1 OR empresa like %?1) AND status in ?2  ORDER BY id DESC \n#pageable\n",
            countQuery = "select count(1) from registration where (nombre like %?1 OR apellido like %?1 OR email like %?1 OR empresa like %?1) AND status in ?2",
            nativeQuery = true
    )            
    Page<Registration> findByQueryWithStatus(String q,String[] status, Pageable pageable);

}
