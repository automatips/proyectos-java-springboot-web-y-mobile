/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientDebt;
import com.wings.base.domain.client.ClientDebtsReminder;
import com.wings.base.domain.invoice.Invoice;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class ClientSDebtsReminderSpecs {

    public static Specification<ClientDebtsReminder> search(String q) {

        return (Root<ClientDebtsReminder> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Invoice, Client> client = root.join("client");
            List<Predicate> predicates = new ArrayList<>();

            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(client.get("name"), search),
                                builder.like(client.get("lastname"), search),
                                builder.like(client.get("email"), search),
                                builder.like(client.get("idValue"), search),
                                builder.like(client.get("companyName"), search),
                                builder.like(client.get("companyIdValue"), search),
                                builder.like(builder.concat(builder.concat(client.get("name"), " "), client.get("lastname")), search)
                        )
                );

                try {
                    if (q.contains("id:")) {

                        predicates.clear();

                        String qq = q.replaceAll("id:", "");
                        Long clientId = Long.parseLong(qq);
                        predicates.add(builder.equal(client.get("id"), clientId));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
