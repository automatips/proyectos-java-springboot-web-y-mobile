/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.auth.WingsGrantedAuthority;
import com.wings.base.domain.admin.User;
import com.wings.base.repos.RegistrationRepository;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class DashboardController {

    @Autowired
    private RegistrationRepository registrationRepository;

    @RequestMapping(value = {"views/main"}, method = RequestMethod.GET)
    public String getMain(Model model, HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String view = "views/main";
        if(!user.getAuthorities().contains(new WingsGrantedAuthority("ADMIN"))){
            view = "views/maincs";        
        }

        
        
//        if (true) {
//            view = "views/maincs";
//            
//            model.addAttribute("portingRejected",15);
//            model.addAttribute("pendingOrders",22);
//            model.addAttribute("unsignedContracts",14);
//            model.addAttribute("pendingInvoices",5);
//            
//
//        }else{
            long complete = registrationRepository.countByStatus("complete");
            long processing = registrationRepository.countByStatus("processing");
            long total = registrationRepository.count();
            long paymentDone = registrationRepository.countByStatus("payment_done");

            model.addAttribute("completed", complete);
            model.addAttribute("processing", processing);
            model.addAttribute("total", total);
            model.addAttribute("paymentDone", paymentDone);
        
//        }



        return view;
    }

}
