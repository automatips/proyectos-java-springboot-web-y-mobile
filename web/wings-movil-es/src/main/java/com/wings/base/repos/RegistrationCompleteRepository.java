/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;


import com.wings.base.domain.registration.RegistrationComplete;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author seba
 */
public interface RegistrationCompleteRepository extends PagingAndSortingRepository<RegistrationComplete, Long> {
    
    RegistrationComplete getOneBySessionId(String sessionId);
    
    @Query(
            value = "select * from registration_complete where nombre like %?1% OR apellido like %?1% OR email like %?1% OR empresa like %?1%  ORDER BY id DESC \n#pageable\n",
            countQuery = "select count(1) from registration_complete where nombre like %?1% OR apellido like %?1% OR email like %?1% OR empresa like %?1%",
            nativeQuery = true
    )            
    Page<RegistrationComplete> findByQuery(String q, Pageable pageable);
    
    List<RegistrationComplete> findByStatus(String status);
}
