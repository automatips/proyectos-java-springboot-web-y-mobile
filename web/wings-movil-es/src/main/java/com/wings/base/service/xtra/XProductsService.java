/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import com.wings.base.domain.xtra.Product;
import com.wings.xtra.UrlLocator;
import com.wings.xtra.lines.products.CableMsisdnUtils;
import com.wings.xtra.lines.products.CableMsisdnUtilsPort;
import com.wings.xtra.lines.products.TestFindObjectFactory;
import com.wings.xtra.lines.products.request.Activate;
import com.wings.xtra.lines.products.request.AuxData;
import com.wings.xtra.lines.products.request.MsisdnUtilsMaintenanceRequest;
import com.wings.xtra.lines.products.request.Operation;
import com.wings.xtra.lines.products.request.SoapRequest;
import com.wings.xtra.lines.products.response.MsisdnUtilsMaintenanceRequestResponse;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seba
 */
public class XProductsService {

    private static final String TYPE_PRODUCT_PORTA = "S";
    private static final String TYPE_PRODUCT_OTHERS = "N";

    private final String env;

    public XProductsService(String env) {
        this.env = env;
    }

    public List<Product> getProducts() {
        List<Product> ret = getProducts(TYPE_PRODUCT_PORTA);
        ret.addAll(getProducts(TYPE_PRODUCT_OTHERS));
        return ret;
    }

    private List<Product> getProducts(String type) {

        List<Product> ret = new ArrayList<>();
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getProductListURL();

        try {
            CableMsisdnUtils service = new CableMsisdnUtils(url);
            CableMsisdnUtilsPort port = service.getCableMsisdnUtilsSoapService();
            MsisdnUtilsMaintenanceRequest part1 = new MsisdnUtilsMaintenanceRequest();
            SoapRequest request = new SoapRequest();
            Operation operation = new Operation();

            operation.setInstruction(TestFindObjectFactory.getInstruction());

            Activate a = new Activate();
            AuxData aux = new AuxData();
            //S productos disponibles para portabilidad
            //N todo lo demas
            aux.setGetProductPort(type);
            a.setAuxData(aux);

            operation.setActivate(a);

            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            MsisdnUtilsMaintenanceRequestResponse result = port.msisdnUtilsMaintenance(part1);

            List<MsisdnUtilsMaintenanceRequestResponse.Return.Productos.Producto> list = result.getReturn().getProductos().getProducto();
            for (MsisdnUtilsMaintenanceRequestResponse.Return.Productos.Producto producto : list) {

                Product p = new Product();
                p.setProductProfile(producto.getIdPerfil());
                p.setDescription(producto.getDescripcion());
                p.setBonusDat(producto.getBonosConfDat());
                p.setBonusDefault(producto.getBonosDefecto());
                p.setBonusOptional(producto.getBonosOpcionales());
                p.setBonusSMS(producto.getBonosConfSms());
                p.setBonusVoice(producto.getBonosConfVoz());

                ret.add(p);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return ret;
    }

}
