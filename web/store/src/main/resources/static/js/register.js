$(document).ready(function () {

    $('#link-afiliate').click(function (e) {
        $('#form-new').hide();
        $('#form-afiliate').show();
        $('#link-new').show();
        $('#link-afiliate').hide();
    });

    $('#link-new').click(function (e) {
        $('#form-afiliate').hide();
        $('#form-new').show();
        $('#link-new').hide();
        $('#link-afiliate').show();
    });

    $("#terms").click(function (e) {
        e.preventDefault();
        var url = "/terms";
        var title = "Términos y condiciones";
        loadInDialog(url, title, true, true);
    });

    $('#shipping-inputs-check').click(function (e) {
        if ($('#shipping-inputs-check').prop('checked')) {
            $('#shipping-inputs').show();
        } else {
            $('#shipping-inputs').hide();
        }
    });

    $('#terms-checked').click(function (e) {
        if ($('#terms-checked').prop('checked')) {
            $('#btn-submit').removeAttr("disabled");
        } else {
            $('#btn-submit').attr("disabled", "disabled");
        }
    });

    $('#member-login').click(function (e) {
        e.preventDefault();
        $('#preloder').show();
        $.post('register/login', $('#form-afiliate-login').serialize(), function (response) {

            $('#btn-submit').removeAttr("disabled");
            $('#btn-submit').attr("formnovalidate", "formnovalidate");
            $('#btn-submit').click();

        }).fail(function (response) {
            $('#preloder').hide();
            $('#login-message').html(response.responseText);
            $('#login-message').show();
        });
    });

    $("#form-new").submit(function (event) {
        $('#preloder').show();
    });

});
