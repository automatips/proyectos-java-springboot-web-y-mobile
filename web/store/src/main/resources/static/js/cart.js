$(document).ready(function () {

    $('#addItem').click(function (e) {
        e.preventDefault();
        var itemId = $("#itemId").val();
        var quantity = $("#quantity").val();
        var url = "/cart/" + itemId;

        $.post(url, {quantity: quantity, id: itemId}, function (response) {
            //window.location = response;
            window.location.replace(window.location.origin + "/cart");
        });
    });

    $('.delete-item').click(function (e) {
        e.preventDefault();
        var itemId = $(this).data("id");
        var url = "/cart/" + itemId;

        $.ajax({
            url: url,
            type: 'DELETE',
            success: function (result) {
                
                window.location.reload();
                
                /*if (result != "") {
                    var row = "#item-" + itemId;
                    $(row).remove();
                    var items = result.items;
                    $("#cart-size").text(items.length);
                    $("#cart-total").text(result.totalString);
                    $("#cart-discount").text(result.shippingString);
                    $("#cart-subtotal").text(result.subTotalString);
                    if (items.length == 0) {
                        $(".cart-details-not-empty").attr("hidden", true);
                        $("#cart-details-empty").attr("hidden", false);
                    }
                }*/
            }
        });
    });

});


