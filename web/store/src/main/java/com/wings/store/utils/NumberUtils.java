/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author seba
 */
public class NumberUtils {

    /**
     * Format money with conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static String formatMoney(double money, Locale locale) {
        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        return f.format(money);
    }

}
