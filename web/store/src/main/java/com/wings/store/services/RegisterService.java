/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.services;

import com.wings.api.models.PersonalInfoDto;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class RegisterService {

    @Autowired
    private MessageSource messageSource;

    private static final Pattern EMAIL_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     *
     * @param label
     * @param locale
     * @return
     */
    public String getMessage(String label, Locale locale) {
        return messageSource.getMessage(label, null, locale);
    }

    /**
     * if list is empty no errors where found
     *
     * @param info
     * @param locale
     * @return
     */
    public List<String> validatePersonalInfo(PersonalInfoDto info, Locale locale) {

        List<String> list = new ArrayList<>();
        if (info.getEmail() != null && info.getEmail().trim().isEmpty() && !validateEmailRegex(info.getEmail())) {
            list.add(messageSource.getMessage("validation.email", null, locale));
        }
        if (info.getFirstName() != null && info.getFirstName().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.firstName", null, locale));
        }
        if (info.getLastName() != null && info.getLastName().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.lastName", null, locale));
        }
        if (info.getPhoneNumber() != null && info.getPhoneNumber().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.phoneNumber", null, locale));
        }
        if (info.getDniType() != null && info.getDniType().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.dniType", null, locale));
        }
        if (info.getDni() != null && info.getDni().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.dni", null, locale));
        }
        if (info.getBillingAddress() != null && info.getBillingAddress().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.billingAddress", null, locale));
        }
        if (info.getBillingCity() != null && info.getBillingCity().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.billingCity", null, locale));
        }
        if (info.getBillingState() != null && info.getBillingState().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.billingState", null, locale));
        }
        if (info.isDifferentShippingAddress()) {
            if (info.getShippingName() != null && info.getShippingName().trim().isEmpty()) {
                list.add(messageSource.getMessage("validation.shippingName", null, locale));
            }
            if (info.getShippingAddress() != null && info.getShippingAddress().trim().isEmpty()) {
                list.add(messageSource.getMessage("validation.shippingAddress", null, locale));
            }
            if (info.getShippingCity() != null && info.getShippingCity().trim().isEmpty()) {
                list.add(messageSource.getMessage("validation.shippingCity", null, locale));
            }
            if (info.getShippingState() != null && info.getShippingState().trim().isEmpty()) {
                list.add(messageSource.getMessage("validation.shippingState", null, locale));
            }
            if (info.getShippingPostalCode() != null && info.getShippingPostalCode().trim().isEmpty()) {
                list.add(messageSource.getMessage("validation.shippingPostalCode", null, locale));
            }
        }

        if (!info.isTermsChecked()) {
            list.add(messageSource.getMessage("validation.termsChecked", null, locale));
        }

        return list;
    }

    /**
     * if list is empty no errors where found
     *
     * @param info
     * @param locale
     * @return
     */
    public List<String> validateTopUpInfo(PersonalInfoDto info, Locale locale) {

        List<String> list = new ArrayList<>();
        if (info.getEmail() != null && info.getEmail().trim().isEmpty() && !validateEmailRegex(info.getEmail())) {
            list.add(messageSource.getMessage("validation.email", null, locale));
        }
        if (info.getFirstName() != null && info.getFirstName().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.firstName", null, locale));
        }
        if (info.getLastName() != null && info.getLastName().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.lastName", null, locale));
        }
        if (info.getPhoneNumber() != null && info.getPhoneNumber().trim().isEmpty()) {
            list.add(messageSource.getMessage("validation.phoneNumber", null, locale));
        }
        if (locale.getCountry().toLowerCase().equals("co") && info.getPhoneNumber().length() != 10) {
            list.add(messageSource.getMessage("validation.phoneNumber", null, locale));
        }
        return list;
    }

    /**
     * validate email by regex
     *
     * @param email
     * @return
     */
    public boolean validateEmailRegex(String email) {
        return EMAIL_REGEX.matcher(email).find();
    }

    /**
     * validate email hostanme exists not in use for now
     *
     * @param hostName
     * @return
     */
    public boolean validateEmailHostName(String hostName) {
        boolean ret = false;
        try {
            Hashtable env = new Hashtable();
            env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
            DirContext ictx = new InitialDirContext(env);
            Attributes attrs = ictx.getAttributes(hostName, new String[]{"MX"});
            Attribute attr = attrs.get("MX");

            if (attr != null && attr.size() > 0) {

                attrs = ictx.getAttributes(hostName, new String[]{"A"});
                attr = attrs.get("A");
                ret = attr != null;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

}
