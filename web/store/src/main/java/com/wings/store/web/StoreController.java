/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.web;

import com.wings.api.models.StoreItemDto;
import com.wings.api.response.StoreItemResponse;
import com.wings.store.services.ApiClientService;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lucas
 */
@Controller
public class StoreController {

    @Autowired
    private ApiClientService storeService;

    /**
     *
     * @param model
     * @param request
     * @param category
     * @return
     */
    @RequestMapping(value = {"/q", "/q/{category}"}, method = RequestMethod.GET)
    public String search(Model model, HttpServletRequest request,
            @PathVariable(value = "category", required = false) String category) {

        String sessionId = request.getSession().getId();
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String query = request.getParameter("q");
        String categoryStr = request.getParameter("category");
        String country = request.getSession().getAttribute("country").toString();
        String subCategory = request.getParameter("subCategory");
        String view = "grid";

        if (request.getSession().getAttribute("view") == null) {
            request.getSession().setAttribute("view", "grid");
        } else {
            view = request.getSession().getAttribute("view").toString();
        }

        if (category != null && category.equals("all")) {
            category = null;
        }

        if (category == null && !categoryStr.isEmpty()) {
            category = categoryStr;
        }

        if (subCategory != null && subCategory.equals("all")) {
            subCategory = null;
        }

        if (ssize == null) {
            ssize = "100";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 100;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (query != null && !query.isEmpty()) {
            try {
                query = URLDecoder.decode(query.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }
        StoreItemResponse search = storeService.search(page, size, category, subCategory, query, country, sessionId, null);
        model.addAttribute("items", search.getItems());
        model.addAttribute("page", search);
        model.addAttribute("category", category);

        if (view.equals("grid")) {
            return "listing_grid";
        } else {
            return "listing_list";
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/details/{id}"}, method = RequestMethod.GET)
    public String viewDetails(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        String country = (String) request.getSession().getAttribute("country");
        StoreItemDto item = storeService.getItem(id, country);

        model.addAttribute("item", item);
        model.addAttribute("category", item != null ? item.getCategoryName() : null);
        model.addAttribute("itemsRelated", storeService.searchRelated(id, country));

        return "product";
    }

    /**
     *
     * @param model
     * @param request
     * @param slug
     * @return
     */
    @RequestMapping(value = {"/kit/{slug}", "/p/{slug}"}, method = RequestMethod.GET)
    public String viewDetails(Model model, HttpServletRequest request, @PathVariable("slug") String slug) {

        String country = (String) request.getSession().getAttribute("country");
        StoreItemDto item = storeService.getItem(slug, country);

        model.addAttribute("item", item);
        //model.addAttribute("category", item != null ? item.getCategoryName() : null);
        //model.addAttribute("itemsRelated", storeService.searchRelated(item != null ? item.getId() : 0l, country));

        return "product";
    }

}
