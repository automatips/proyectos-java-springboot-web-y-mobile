/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.services;

import com.wings.api.models.CartDto;
import com.wings.api.models.CartDtoResponse;
import com.wings.api.models.CountryDto;
import com.wings.api.response.CategoryResponse;
import com.wings.api.models.StoreItemDto;
import com.wings.api.response.CountryResponse;
import com.wings.api.response.StoreItemResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class ApiClientService {

    @Autowired
    private RestTemplate restTemplate;

    private final List<CountryDto> countriesCache = new ArrayList<>();

    @Value("${wings.apiUrl}")
    private String urlApiBase;

    @Value("${wings.auth.token}")
    private String authToken;

    private final Logger log = LoggerFactory.getLogger(ApiClientService.class);

    /**
     *
     * @param page
     * @param size
     * @param category
     * @param subcategory
     * @param query
     * @param country
     * @param session
     * @param sort
     * @return
     */
    public StoreItemResponse search(int page, int size, String category, String subcategory, String query, String country, String session, String sort) {

        String url = urlApiBase + "items?page={page}&size={size}&q={q}&category={category}&country={country}&sort={sort}";

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        headers.set("auth", authToken);

        Map<String, Object> params = new HashMap<>();
        params.put("page", page);
        params.put("size", size);
        params.put("q", query);
        params.put("country", country);
        params.put("category", category);
        params.put("sort", sort);

        HttpEntity entity = new HttpEntity(headers);
        HttpEntity<StoreItemResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, StoreItemResponse.class, params);

        StoreItemResponse data = response.getBody();
        return data;
    }

    /**
     *
     * @param country
     * @return
     */
    public List<StoreItemDto> mostPopular(String country) {
        StoreItemResponse res = search(0, 20, null, null, "popular", country, null, null);
        return res.getItems();

    }

    /**
     *
     * @param country
     * @return
     */
    public List<StoreItemDto> searchKitsNoInstallment(String country) {
        StoreItemResponse res = search(0, 20, "kit", null, null, country, null, null);
        List<StoreItemDto> ret = new LinkedList<>();
        res.getItems().stream().filter((item) -> (!item.isInstallment())).forEachOrdered((item) -> {
            ret.add(item);
        });
        return ret;
    }

    /**
     *
     * @param id
     * @param country
     * @return
     */
    public List<StoreItemDto> searchRelated(Long id, String country) {
        StoreItemResponse res = search(0, 4, null, null, null, country, null, null);
        return res.getItems();
    }

    /**
     *
     * @param id
     * @param session
     * @param country
     * @return
     */
    public StoreItemResponse getItem(Long id, String session, String country) {
        return search(0, 10, null, null, String.valueOf(id), country, session, null);
    }

    /**
     *
     * @param id
     * @param country
     * @return
     */
    public StoreItemDto getItem(Long id, String country) {
        StoreItemDto ret = null;
        try {
            String url = urlApiBase + "items/{id}/{country}";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);

            Map<String, Object> params = new HashMap<>();
            params.put("id", id);
            params.put("country", country);

            HttpEntity entity = new HttpEntity(headers);
            HttpEntity<StoreItemDto> response = restTemplate.exchange(url, HttpMethod.GET, entity, StoreItemDto.class, params);

            if (response.getBody() != null) {
                ret = response.getBody();
            }
        } catch (Exception e) {
            log.error("Exception on getItem " + id + " " + country + " " + e.getMessage());
        }
        return ret;
    }

    /**
     *
     * @param slug
     * @param country
     * @return
     */
    public StoreItemDto getItem(String slug, String country) {
        StoreItemDto ret = null;
        try {

            String url = urlApiBase + "slugs/{slug}/{country}";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);

            Map<String, Object> params = new HashMap<>();
            params.put("slug", slug);
            params.put("country", country);

            HttpEntity entity = new HttpEntity(headers);
            HttpEntity<StoreItemDto> response = restTemplate.exchange(url, HttpMethod.GET, entity, StoreItemDto.class, params);

            if (response.getBody() != null) {
                ret = response.getBody();
            }
        } catch (Exception e) {
            log.error("Exception on getItem " + slug + " " + country + " " + e.getMessage());
        }
        return ret;
    }

    /**
     *
     * @param cart
     * @return
     */
    public CartDtoResponse postCartToBackoffice(CartDto cart) {
        CartDtoResponse ret = new CartDtoResponse();
        try {

            //HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
            //RestTemplate template = new RestTemplate(clientHttpRequestFactory);
            String url = urlApiBase + "cart";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);
            HttpEntity<CartDto> requestEntity = new HttpEntity<>(cart, headers);
            ResponseEntity<CartDtoResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, CartDtoResponse.class);
            if (response.getBody() != null) {
                ret = response.getBody();
            }
        } catch (Exception e) {
            //TODO: UI para la gente
            ret.setStatus("error");
            ret.setMessage("Error: " + e.getMessage());
        }
        return ret;
    }

    /**
     *
     * @return
     */
    public List<CountryDto> getCountries() {
        if (countriesCache.isEmpty()) {
            try {
                String url = urlApiBase + "countries";
                HttpHeaders headers = new HttpHeaders();
                headers.set("Accept", "application/json");
                headers.set("auth", authToken);
                HttpEntity<CountryResponse> requestEntity = new HttpEntity<>(headers);
                ResponseEntity<CountryResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, CountryResponse.class);
                CountryResponse countries = response.getBody();

                countriesCache.clear();
                countriesCache.addAll(countries.getCountries());

            } catch (Exception e) {
                log.error("Error gettting countries " + e.getMessage());
            }
        }
        return countriesCache;
    }

    /**
     *
     * @return
     */
    public List<CountryDto> getOtherCountries() {
        List<CountryDto> ret = new ArrayList<>();
        try {
            String url = urlApiBase + "countries/other";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);
            HttpEntity<CountryResponse> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<CountryResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, CountryResponse.class);
            CountryResponse countries = response.getBody();
            ret = countries.getCountries();
        } catch (Exception e) {
            log.error("Error gettting other countries " + e.getMessage());
        }
        return ret;
    }

    /**
     *
     * @return
     */
    public List<String> getCategories() {
        String url = urlApiBase + "categories";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        headers.set("auth", authToken);
        HttpEntity<CountryResponse> requestEntity = new HttpEntity<>(headers);
        ResponseEntity<CategoryResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, CategoryResponse.class);
        CategoryResponse categories = response.getBody();
        return categories.getCategories();
    }

    /**
     *
     * @param username
     * @param password
     * @return
     */
    public Map<String, String> loginMember(String username, String password) {

        Map<String, String> params = new HashMap<>();
        Map<String, String> ret = new HashMap<>();
        try {

            String shaPass = DigestUtils.sha256Hex(password);
            params.put("username", username);
            params.put("password", shaPass);

            String url = urlApiBase + "login";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);
            HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(params, headers);
            ResponseEntity<Map> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Map.class);

            if (response.getBody() != null) {
                ret = response.getBody();
            }
        } catch (Exception e) {
            //TODO: UI para la gente
            ret.put("error", e.getMessage());
        }
        return ret;

    }

    /**
     *
     * @param username
     * @return
     */
    public Map<String, String> getMember(String username) {

        Map<String, String> ret = new HashMap<>();
        try {
            String url = urlApiBase + "members/" + username;

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);
            HttpEntity requestEntity = new HttpEntity<>(headers);
            ResponseEntity<Map> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Map.class);

            if (response.getBody() != null) {
                ret = response.getBody();
            }
        } catch (Exception e) {
            //TODO: UI para la gente
            ret.put("error", e.getMessage());
        }
        return ret;

    }
}
