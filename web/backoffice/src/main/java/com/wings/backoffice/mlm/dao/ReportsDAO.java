/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ReportsDAO {

    @Autowired
    @Qualifier(value = "entityManagerFactory")
    private EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public ReportSums find(Date from, Date to) {

        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.backoffice.mlm.dao.ReportSums(o.paymentCurrency, o.billingCountry, o.paymentType, sum(o.paymentAmount) as amount, count(1) as recordCount) ");
        b.append("FROM Order as o ");
        b.append("WHERE 1=1 ");

        if (from != null) {
            b.append(" AND (o.activationDate >= '").append(sdf.format(from)).append("')");
        }

        if (to != null) {
            b.append(" AND (o.activationDate <= '").append(sdf.format(to)).append("')");
        }

        b.append(" group by o.paymentCurrency, o.paymentType, o.billingCountry ");

        Query query = em.createQuery(b.toString(), ReportSums.class);
        ReportSums ret = (ReportSums) query.getSingleResult();

        return ret;
    }

}
