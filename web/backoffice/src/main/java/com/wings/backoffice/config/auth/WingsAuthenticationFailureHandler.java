/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config.auth;

import com.wings.backoffice.mlm.domain.Log;
import com.wings.backoffice.services.LogsService;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 *
 * @author seba
 */
public class WingsAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Autowired
    private LogsService logsService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException ae) throws IOException, ServletException {

        String modIp = request.getHeader("X-Forwarded-For");
        if (modIp == null) {
            modIp = request.getRemoteAddr();
        }

        try {
            LoginFailureException e = (LoginFailureException) ae;
            Log l = new Log();

            l.setComment(e.getComment());
            l.setCreationDate(new Date());
            l.setMessage(e.getMessage());
            l.setModIp(modIp);

            String modUser = e.getModUser();
            if (modUser.length() > 50) {
                modUser = modUser.substring(0, 50);
            }
            l.setModUser(modUser);
            l.setReference(e.getReference());
            l.setReferenceId(e.getReferenceId());
            l.setType(e.getType());

            logsService.addLog(l);
        } catch (Exception e) {
        }

        //localeResolver.setLocale(hsr, hsr1, new Locale("en"));
        response.sendRedirect("/login");

    }

}
