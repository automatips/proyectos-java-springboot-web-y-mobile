/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.enums;

/**
 *
 * @author seba
 */
public enum InvoiceType {
    
    product,
    kit,
    service,
    withdrawal,
    store
    
}
