/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis.orders;

import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_store_item_i18n")
public class StoreItemI18n implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "store_item_id")
    private Long storeItemId;
    @Size(max = 10)
    @Column(name = "country")
    private String country;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Column(name = "price")
    private Double price;
    @Column(name = "price_for_coms")
    private Double priceForComs;
    @Column(name = "stock")
    private Long stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreItemId() {
        return storeItemId;
    }

    public void setStoreItemId(Long storeItemId) {
        this.storeItemId = storeItemId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceForComs() {
        return priceForComs;
    }

    public void setPriceForComs(Double priceForComs) {
        this.priceForComs = priceForComs;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public String getLocalPrice() {
        if (price == null) {
            return "";
        }
        return NumberUtils.convertAndformatMoneyForCountryIso(price, country);
    }

    public Double getLocalPriceNumber() {
        if (price == null) {
            return 0.0;
        }
        return NumberUtils.round(NumberUtils.convertMoneyFromEuros(price, CountryUtils.getLocaleForCountry(country)), 2);
    }

    public String getLocalPriceNumberString() {
        if (price == null) {
            return "0.0";
        }
        return String.format("%.0f", NumberUtils.convertMoneyFromEuros(price, CountryUtils.getLocaleForCountry(country)));
    }
}
