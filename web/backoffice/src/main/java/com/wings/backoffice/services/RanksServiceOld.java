/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.RankHistory;
import com.wings.backoffice.mlm.domain.utils.MemberSnapshotOld;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.RankHistoryRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class RanksServiceOld {

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private CalificationPointsRepository calificationPointsRepository;

    @Autowired
    private RankHistoryRepository rankHistoryRepository;

    @Autowired
    private RanksBisRepository ranksBisRepository;

    @Autowired
    private MemberSnapshotServiceOld memberSnapshotService;

    @Autowired
    private BalanceRepository balanceRepository;

    private final Logger log = LoggerFactory.getLogger(RanksServiceOld.class);

    /**
     * Returns the rank of the member at that particular Date returns current
     * ranks if there's not rank history
     *
     * @param member
     * @param date
     * @return RankBis at that date
     */
//    public RankBis getRankAtDate(Member member, Date date) {
//
//        //RankHistory history = rankHistoryRepository.findFirstByMemberIdAndDateGreaterThanOrderByDateAsc(member.getId(), date);
//        RankBis ret;
////        //if there's no history return current rank
////        if (history == null) {
////            ret = member.getRank();
////        } else {
////            ret = ranksBisRepository.findOne(history.getRankId());
////        }
//        return ret;
//    }

    private boolean running = false;
    private int process = 0;
    private int cant = 0;

    /**
     * Analize ranks for each member adding record on rank history if needed
     *
     * @return
     */
    public String analizeRanks() {
        if (!running) {
            running = true;
            process = 0;
            List<Member> members = membersRepository.findAll();
            cant = members.size();

            List<RankHistory> toSave = new LinkedList<>();
            members.forEach((member) -> {
                toSave.addAll(analizeRankHistory(member));
                process++;
                log.error("Analizing member " + member.getId());
            });

            rankHistoryRepository.save(toSave);
            running = false;
        }

        return "Procesados " + process + " de " + cant;
    }

    public void analizeRanksTest() {
        Member member = membersRepository.findOne(17L);
        analizeRankHistory(member);
    }

    public List<RankHistory> analizeRankHistory(Member member) {

        RankBis initialRank = ranksBisRepository.findOne(1l);
        RankBis curentRank = member.getRank();
        List<RankHistory> historys = new LinkedList<>();
        List<CalificationPoint> points = calificationPointsRepository.findByMemberIdOrderByDateAsc(member.getId());

        if (getNextRank(member.getRank()) == null) {
            //maximo alcanzado
            return historys;
        }

        for (CalificationPoint point : points) {

            RankBis next = getNextRank(initialRank);
            if (next == null) {
                //maximo alcanzado
                break;
            }

            MemberSnapshotOld snap = memberSnapshotService.getMemberSnapshot(member, point.getDate(), point.getEventId());
            boolean speed = snap.isInSpeedBonus();
            snap.setNextRank(next);
            Double sum = snap.getSum6040(speed);

            double required = (speed && next.getSbQuantity() > 0) ? next.getSbQuantity() : next.getQuantity();
            if (sum > required) {

                int referers = 5;
                if (initialRank.getId().equals(1l)) {
                    referers = 3;
                }
                if (snap.getSumsPerBranch().size() - 1 < referers) {
                    continue;
                }

                //sube
                RankHistory history = new RankHistory();
                history.setMemberId(member.getId());
                history.setDate(point.getDate());
                history.setRankId(initialRank.getId());
                history.setAuto(true);

                member.setRank(next);
                member.setRankId(next.getId());

                String desc = "Auto Rango puntos ";
                desc += (speed) ? " (speed bonus) " : "";
                desc += sum + " sube a " + member.getRank().getName();
                desc += " aplicadas 60/40 - " + referers + " referidos";
                history.setDescription(desc);

                log.error(member.getId() + " " + desc);

                //si sube inserto - speed bonus
                Balance bal = balanceRepository.findByMemberIdAndType(member.getId(), BalanceType.speed.name());
                double amount = 0.0;
                switch (member.getRankId().intValue()) {
                    case 2:
                        amount = member.getSpeedBonus().getPriceRank1();
                        break;
                    case 3:
                        amount = member.getSpeedBonus().getPriceRank2();
                        break;
                    case 4:
                        amount = member.getSpeedBonus().getPriceRank3();
                        break;
                }

                if (bal == null) {
                    bal = new Balance();
                    bal.setBonusType(BalanceType.speed.name());
                    bal.setType(BalanceType.speed.name());
                    bal.setComment("New record");
                    bal.setControl("speed-" + member.getId());
                    bal.setDescription(desc);
                    bal.setEffective(true);
                    bal.setMemberId(member.getId());
                    bal.setReferenceId(0l);
                    bal.setStatus(BalanceStatus.active.name());
                } else {
                    bal.setComment("Update");
                }

                bal.setCreationDate(point.getDate());
                bal.setAmount(amount);
                if (speed) {
                    balanceRepository.save(bal);
                }

                historys.add(history);
                if (member.getRankId() > curentRank.getId()) {
                    membersRepository.save(member);
                }

                initialRank = next;
            }
        }
        return historys;
    }

    /**
     * Given a rank, return the inmediate superior
     *
     * @param rank
     * @return
     */
    public RankBis getNextRank(RankBis rank) {
        return ranksBisRepository.findByDirectRequired(rank.getId());
    }

    public String updateRank(Long memberId, Long newRankId, Date date) {

        try {
            RankBis newRank = ranksBisRepository.findOne(newRankId);
            Member m = membersRepository.findOne(memberId);

            RankHistory history = new RankHistory();
            history.setAuto(false);
            history.setDate(date);
            history.setDescription("Manual update de " + m.getRank().getName() + " a " + newRank.getName());
            history.setMemberId(memberId);
            history.setRankId(m.getRankId());

            rankHistoryRepository.save(history);

            m.setRankId(newRankId);
            membersRepository.save(m);

        } catch (Exception e) {
            return "Ocurrio un error actualizando el rango " + e.getMessage();
        }

        return "Rango actualizado";
    }

    //    public List<RankHistory> analizeRankHistoryold(Member member) {
//
//        log.error("Analizing member " + member.getId());
//
//        RankBis initialRank = member.getInitialRank();
//
//        List<RankHistory> historys = new LinkedList<>();
//
//        RankBis next = getNextRank(initialRank);
//        if (next != null) {
//
//            List<CalificationPoint> points = calificationPointsRepository.findByMemberIdOrderByDateAsc(member.getId());
//
//            Long required = next.getQuantity();
//            if (next.getSbQuantity() != 0) {
//                required = next.getSbQuantity();
//            }
//            double sum = 0.0;
//
//            for (CalificationPoint point : points) {
//
//                String aplicadas = "";
//                //verificar fecha, si despues 30/09
//                boolean extra = false;
//                try {
//                    extra = point.getDate().after(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-09-30 23:59:59"));
//                } catch (Exception e) {
//                }
//
//                MemberSnapshot snap = null;
//                if (extra) {
//                    snap = memberSnapshotService.getMemberSnapshot(member, point.getDate(), point.getEventId());
//                    aplicadas = "aplicadas: 60/40";
//                    sum = snap.getSum6040(true);
//                } else {
//                    sum += point.getPoints();
//                }
//
//                if (sum >= required) {
//
//                    if (extra) {
//                        int referers = 5;
//                        if (initialRank.getId().equals(1l)) {
//                            referers = 3;
//                        }
//
//                        if (snap.getSumsPerBranch().size() < referers) {
//                            //no cumple la regla
//                            continue;
//                        }
//                        aplicadas += "," + referers + " referidos";
//                    }
//
//                    RankHistory history = new RankHistory();
//                    history.setMemberId(member.getId());
//                    history.setDate(point.getDate());
//                    history.setRankId(initialRank.getId());
//                    history.setAuto(true);
//                    historys.add(history);
//
//                    initialRank = getNextRank(initialRank);
//                    if (initialRank == null) {
//                        //maximun level reached
//                        break;
//                    } else {
//                        history.setDescription("Auto Rango puntos (speed bonus) " + sum + " sube a  " + initialRank.getName() + " " + aplicadas);
//
//                        if (member.getRankId() < initialRank.getId()) {
//                            member.setRankId(initialRank.getId());
//                            membersRepository.save(member);
//                        }
//                        //TODO: aca agregar el speed bonus
//                    }
//
//                    RankBis n = getNextRank(initialRank);
//                    if (n != null) {
//                        required = n.getQuantity();
//                        if (n.getSbQuantity() != 0) {
//                            required = n.getSbQuantity();
//                        }
//                    }
//                }
//            }
//
//            if (historys.isEmpty()
//                    && member.getSponsor() != null
//                    && //si no es la primera linea
//                    member.getSponsor().getId() > 3
//                    && // si no es hijo de la primera linea
//                    member.getRank().getId() > 1
//                    && //si el rango actual es mayor al inicial
//                    !member.getRankId().equals(member.getInitialRankId()) //si el rango inicial es distinto del inicial                    
//                    ) {
//
//                RankHistory h = new RankHistory();
//                h.setDate(new Date());
//                h.setDescription("Rango mayor al inicial");
//                h.setMemberId(member.getId());
//                h.setAuto(false);
//                h.setRankId(1l);
//
//                historys.add(h);
//            }
//
//            rankHistoryRepository.save(historys);
//            log.error("Saving for member " + member.getId());
//        }
//
//        return historys;
//    }
}
