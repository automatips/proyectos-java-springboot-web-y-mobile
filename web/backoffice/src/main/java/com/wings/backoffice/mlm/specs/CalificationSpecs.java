/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class CalificationSpecs {

    /**
     *
     * @param q
     * @param type
     * @param from
     * @param to
     * @return
     */
    public static Specification<CalificationPoint> search(String q, String type, Date from, Date to) {

        return (Root<CalificationPoint> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<CalificationPoint, Member> member = root.join("member");

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                //busco por event id primero
                try {
                    Long id = Long.parseLong(q);
                    predicates.add(builder.equal(root.get("eventId"), id));
                } catch (Exception e) {
                }

                if (predicates.isEmpty()) {
                    String search = "%" + q + "%";
                    predicates.add(
                            builder.or(
                                    builder.like(member.get("firstName"), search),
                                    builder.like(member.get("lastName"), search),
                                    builder.like(member.get("email"), search),
                                    builder.like(member.get("username"), search),
                                    builder.like(builder.concat(builder.concat(member.get("firstName"), " "), member.get("lastName")), search)
                            )
                    );
                }
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("type"), type));
            }

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("date"), from));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("date"), to));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    /**
     *
     * @param memberId
     * @param from
     * @param to
     * @return
     */
    public static Specification<CalificationPoint> searchPointsByMember(Long memberId, Date from, Date to) {

        return (Root<CalificationPoint> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("memberId"), memberId));

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("date"), from));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("date"), to));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
