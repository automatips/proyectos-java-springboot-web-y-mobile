/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ErrorController {
    
    @RequestMapping(value = {"/error/{error}"}, method = RequestMethod.GET)
    public String getError(Model model, HttpServletRequest request,@PathVariable("error") String error) {        
        return "error/" + error;
    }
    
}
