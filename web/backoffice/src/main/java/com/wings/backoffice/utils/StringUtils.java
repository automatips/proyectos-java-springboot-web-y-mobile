/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author seba
 */
public class StringUtils {

    private static final String[] SALTS = {"ZFjzTFr6", "EDg8NCgT", "qT6awE3k", "CdvtFGep", "y3xqbbUa", "YYZzhYw5", "s2KkcCtk", "7yA6vppu", "bUfgb6ax", "Zs8HtWFp"};

    public static String capitalize(String str) {
        if (str != null) {
            str = WordUtils.capitalizeFully(str.trim());
        }
        return str;
    }

    public static String capitalizeFirst(String str) {
        str = Character.toUpperCase(str.charAt(0)) + str.substring(1);
        return str;
    }

    public static String toLowerCase(String str) {
        if (str != null) {
            str = str.trim().toLowerCase();
        }
        return str;
    }

    public static String trimAll(String str) {
        if (str != null) {
            return str.trim().replaceAll(" ", "");
        }
        return str;
    }

    /**
     *
     * @param ticketNumber
     * @return
     */
    public static String generateRandomHash(String ticketNumber) {
        String randString = String.valueOf(NumberUtils.randInt1to9999());
        String toInt = randString.substring(randString.length() - 1);
        int index = Integer.valueOf(toInt);
        String salt = SALTS[index];
        String toHash = ticketNumber + "~" + salt + "~" + randString + "~" + System.currentTimeMillis();
        return DigestUtils.sha256Hex(toHash);
    }
}