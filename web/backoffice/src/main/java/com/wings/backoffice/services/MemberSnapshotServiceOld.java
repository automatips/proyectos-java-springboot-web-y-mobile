/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.utils.MemberSnapshotOld;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.utils.MemberUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class MemberSnapshotServiceOld {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private CalificationPointsRepository calificationPointsRepository;
    @Autowired
    private RanksService ranksService;
    @Autowired
    private CacheService cacheService;

    private final Logger log = LoggerFactory.getLogger(MemberSnapshotServiceOld.class);
    private final Map<Long, MemberSnapshotOld> snaps = new HashMap<>();
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    public MemberSnapshotOld getMemberSnapshot(Member member, Date date, Long eventId) {

        String key = member.getId() + "" + sdf.format(date) + "" + eventId;
        Object o = cacheService.get(key);

        MemberSnapshotOld ret;
        if (o != null && o instanceof MemberSnapshotOld) {
            try {
                ret = (MemberSnapshotOld) o;
            } catch (ClassCastException e) {
                log.error("Error casting ", e);
                return null;

                /*cacheService.delete(key);

                ret = new MemberSnapshot();
                ret.setMember(member);
                ret.setSnapshotDate(date);
                ret.setSumsPerBranch(getSumsPerBranch(member, date, ret));
                ret.setMostValuableBranch(getIdBranchMostValuable(ret.getSumsPerBranch()));
                ret.setNextRank(ranksService.getNextRank(member.getRank()));

                cacheService.set(key, 0, ret);*/
            }
        } else {
            ret = new MemberSnapshotOld();
            ret.setMember(member);
            ret.setSnapshotDate(date);
            ret.setSumsPerBranch(getSumsPerBranch(member, date, ret));
            ret.setMostValuableBranch(getIdBranchMostValuable(ret.getSumsPerBranch()));
            ret.setNextRank(ranksService.getNextRank(member.getRank()));

        }
        return ret;
    }

    private Map<Long, Double> getSumsPerBranch(Member member, Date date, MemberSnapshotOld s) {

        //get map with child id - list of descendants
        Map<Long, List<Long>> ids = new HashMap<>();
        List<Long> myId = new ArrayList<>();
        myId.add(member.getId());
        ids.put(member.getId(), myId);

        List<Member> children = member.getChildren();
        children.forEach((child) -> {
            ids.put(child.getId(), MemberUtils.getChildIds(child));
        });

        double productPv = 0.0;
        double kitPv = 0.0;
        double servicePv = 0.0;
        double personalServicePv = 0.0;

        //get map with memberId, points offered - filter by date
        Map<Long, Double> pointsPerChild = new HashMap<>();
        List<CalificationPoint> points = calificationPointsRepository.findByMemberIdAndDateLessThanEqual(member.getId(), date);
        for (CalificationPoint point : points) {

            Long eventId = point.getEventId();
            Long memberId = ordersService.findMemberIdByOrderId(eventId);

            if (memberId == null) {
                //mi rama
                if (pointsPerChild.containsKey(member.getId())) {
                    pointsPerChild.put(member.getId(), pointsPerChild.get(member.getId()) + point.getPoints());
                } else {
                    pointsPerChild.put(member.getId(), point.getPoints());
                }
            } else {
                //de afiliados    
                if (pointsPerChild.containsKey(memberId)) {
                    pointsPerChild.put(memberId, pointsPerChild.get(memberId) + point.getPoints());
                } else {
                    pointsPerChild.put(memberId, point.getPoints());
                }
            }

            if (point.getType().equals(OrderType.product.name())) {
                productPv += point.getPoints();
            } else if (point.getType().equals(OrderType.kit.name())) {
                kitPv += point.getPoints();
            } else if (point.getType().equals(OrderType.service.name())) {
                //TODO: si coincide el dni o el email setear el member id al servicio                
                if (memberId != null && memberId.equals(member.getId())) {
                    personalServicePv += point.getPoints();
                } else {
                    servicePv += point.getPoints();
                }
            }
        }

        s.setKitsPv(kitPv);
        s.setProductsPv(productPv);
        s.setServicesPv(servicePv);
        s.setPersonalServicePv(personalServicePv);

        //parse above maps and get a map with branch id and points offered 
        Map<Long, Double> sumsPerBranch = new HashMap<>();
        for (Map.Entry<Long, List<Long>> entry : ids.entrySet()) {
            Long key = entry.getKey();
            List<Long> idList = entry.getValue();

            double sum = 0.0;
            for (Map.Entry<Long, Double> entry1 : pointsPerChild.entrySet()) {
                Long key1 = entry1.getKey();
                Double value1 = entry1.getValue();
                if (idList.contains(key1)) {
                    sum += value1;
                }
            }
            sumsPerBranch.put(key, sum);
        }

        return sumsPerBranch;
    }

    private Long getIdBranchMostValuable(Map<Long, Double> pointsPerBranch) {
        Long ret = null;
        double maxPoints = 0.0;
        for (Map.Entry<Long, Double> entry : pointsPerBranch.entrySet()) {
            Long key = entry.getKey();
            Double value = entry.getValue();
            if (maxPoints < value) {
                maxPoints = value;
                ret = key;
            }
        }
        return ret;
    }

    //    public MemberSnapshot getMemberSnapshot(Member member, CalificationPoint point) {
//
//        MemberSnapshot ret;
//        if (snaps.containsKey(member.getId())) {
//            ret = snaps.get(member.getId());
//            ret.setSnapshotDate(point.getDate());
//            parsePoint(point, ret);
//        } else {
//            ret = new MemberSnapshot();
//            snaps.put(member.getId(), getMemberSnapshot(member, point.getDate(), point.getEventId()));
//        }
//        return ret;
//    }
//
//    public void parsePoint(CalificationPoint point, MemberSnapshot snap) {
//
//        Map<Long, Double> pointsPerChild = snap.getPointsPerChild();
//        Member member = snap.getMember();
//        double kitPv = snap.getKitsPv();
//        double productPv = snap.getProductsPv();
//        double servicePv = snap.getServicesPv();
//        double personalServicePv = snap.getPersonalServicePv();
//
//        Map<Long, List<Long>> ids = new HashMap<>();
//        List<Long> myId = new ArrayList<>();
//        myId.add(member.getId());
//        ids.put(member.getId(), myId);
//
//        List<Member> children = snap.getMember().getChildren();
//        children.forEach((child) -> {
//            ids.put(child.getId(), membersService.getChildIds(child.getId()));
//        });
//
//        Long eventId = point.getEventId();
//        Long memberId = ordersRepository.findMemberIdByOrderId(eventId);
//
//        if (memberId == null) {
//            //mi rama
//            if (pointsPerChild.containsKey(member.getId())) {
//                pointsPerChild.put(member.getId(), pointsPerChild.get(member.getId()) + point.getPoints());
//            } else {
//                pointsPerChild.put(member.getId(), point.getPoints());
//            }
//        } else {
//            //de afiliados    
//            if (pointsPerChild.containsKey(memberId)) {
//                pointsPerChild.put(memberId, pointsPerChild.get(memberId) + point.getPoints());
//            } else {
//                pointsPerChild.put(memberId, point.getPoints());
//            }
//        }
//
//        if (point.getType().equals(OrderType.product.name())) {
//            productPv += point.getPoints();
//        } else if (point.getType().equals(OrderType.kit.name())) {
//            kitPv += point.getPoints();
//        } else if (point.getType().equals(OrderType.service.name())) {
//            //TODO: si coincide el dni o el email setear el member id al servicio                
//            if (memberId != null && memberId.equals(member.getId())) {
//                personalServicePv += point.getPoints();
//            } else {
//                servicePv += point.getPoints();
//            }
//        }
//
//        snap.setProductsPv(productPv);
//        snap.setServicesPv(servicePv);
//        snap.setPersonalServicePv(personalServicePv);
//        snap.setKitsPv(kitPv);
//
//        Map<Long, Double> sumsPerBranch = snap.getSumsPerBranch();
//        for (Map.Entry<Long, List<Long>> entry : ids.entrySet()) {
//            Long key = entry.getKey();
//            List<Long> idList = entry.getValue();
//
//            double sum = 0.0;
//            for (Map.Entry<Long, Double> entry1 : pointsPerChild.entrySet()) {
//                Long key1 = entry1.getKey();
//                Double value1 = entry1.getValue();
//                if (idList.contains(key1)) {
//                    sum += value1;
//                }
//            }
//            sumsPerBranch.put(key, sum);
//        }
//
//        snap.setMostValuableBranch(getIdBranchMostValuable(snap.getSumsPerBranch()));
//        snap.setNextRank(ranksService.getNextRank(snap.getMember().getRank()));
//
//    }
}
