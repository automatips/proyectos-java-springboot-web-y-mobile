/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import com.wings.backoffice.config.auth.WingsGrantedAuthority;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "members")
public class MemberOld implements UserDetails, Serializable {

    private static final long serialVersionUID = 562500937725816920L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;
    private String username;
    private String password;
    private String pssw;
    private String email;
    private String firstName;
    private String lastName;
    private Integer gender;
    private String skype;

    private String street;
    private String city;
    private String state;
    private String country;
    private String postal;
    private String phone;
    private Long sponsorId;
    private Long regDate;
    private Long lastAccess;
    private Long ipAddress;
    private Boolean isActive;
    private String membership;
    private Long membershipExpiration;
    private Integer matrixLevel;
    private Integer processor;
    private String accountId;
    private Long emailFromCompany;
    private Long emailFromUpline;
    private Long notifyChanges;
    private Long displayName;
    private Long displayEmail;
    private Long logIp;
    private Long mLevel;
    private Double adCredits;
    private Long type;
    private String forgotToken;
    private String dni;
    private String dniType;
    private String dniValidated;
    private boolean founder;
    private boolean colorSelected;
    private String colors;
    private Long orderId;
    private boolean invitationConfirmed;

    @Transient
    private Collection<WingsGrantedAuthority> grantedAuthorities;

    @OneToMany
    @JoinColumn(name = "sponsorId")
    private List<MemberOld> children = new LinkedList<MemberOld>();

    @ManyToOne
    @JoinColumn(name = "sponsorId", updatable = false, insertable = false, nullable = true)
    private MemberOld sponsor;

    @OneToOne
    @JoinColumn(name = "mLevel", updatable = false, insertable = false)
    private RankBis rank;

    @OneToOne
    @JoinColumn(name = "orderId", updatable = false, insertable = false)
    private Order productOrder;

    @Transient
    private Map<Long, Long> differential;

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPssw() {
        return pssw;
    }

    public void setPssw(String pssw) {
        this.pssw = pssw;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(Long sponsorId) {
        this.sponsorId = sponsorId;
    }

    public Long getRegDate() {
        return regDate;
    }

    public void setRegDate(Long regDate) {
        this.regDate = regDate;
    }

    public Long getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Long lastAccess) {
        this.lastAccess = lastAccess;
    }

    public Long getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(Long ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }

    public Long getMembershipExpiration() {
        return membershipExpiration;
    }

    public void setMembershipExpiration(Long membershipExpiration) {
        this.membershipExpiration = membershipExpiration;
    }

    public Integer getMatrixLevel() {
        return matrixLevel;
    }

    public void setMatrixLevel(Integer matrixLevel) {
        this.matrixLevel = matrixLevel;
    }

    public Integer getProcessor() {
        return processor;
    }

    public void setProcessor(Integer processor) {
        this.processor = processor;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Long getEmailFromCompany() {
        return emailFromCompany;
    }

    public void setEmailFromCompany(Long emailFromCompany) {
        this.emailFromCompany = emailFromCompany;
    }

    public Long getEmailFromUpline() {
        return emailFromUpline;
    }

    public void setEmailFromUpline(Long emailFromUpline) {
        this.emailFromUpline = emailFromUpline;
    }

    public Long getNotifyChanges() {
        return notifyChanges;
    }

    public void setNotifyChanges(Long notifyChanges) {
        this.notifyChanges = notifyChanges;
    }

    public Long getDisplayName() {
        return displayName;
    }

    public void setDisplayName(Long displayName) {
        this.displayName = displayName;
    }

    public Long getDisplayEmail() {
        return displayEmail;
    }

    public void setDisplayEmail(Long displayEmail) {
        this.displayEmail = displayEmail;
    }

    public Long getLogIp() {
        return logIp;
    }

    public void setLogIp(Long logIp) {
        this.logIp = logIp;
    }

    public Long getmLevel() {
        return mLevel;
    }

    public void setmLevel(Long mLevel) {
        this.mLevel = mLevel;
    }

    public Double getAdCredits() {
        return adCredits;
    }

    public void setAdCredits(Double adCredits) {
        this.adCredits = adCredits;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getForgotToken() {
        return forgotToken;
    }

    public void setForgotToken(String forgotToken) {
        this.forgotToken = forgotToken;
    }

    public List<MemberOld> getChildren() {
        return children;
    }

    public void setChildren(List<MemberOld> children) {
        this.children = children;
    }

    public RankBis getRank() {
        return rank;
    }

    public void setRank(RankBis rank) {
        this.rank = rank;
    }

    public Map<Long, Long> getDifferential() {
        return differential;
    }

    public void setDifferential(Map<Long, Long> differential) {
        this.differential = differential;
    }

    public void setFounder(boolean founder) {
        this.founder = founder;
    }

    public boolean isFounder() {
        return founder;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDniType() {
        return dniType;
    }

    public void setDniType(String dniType) {
        this.dniType = dniType;
    }

    public String getDniValidated() {
        return dniValidated;
    }

    public void setDniValidated(String dniValidated) {
        this.dniValidated = dniValidated;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setGrantedAuthorities(Collection<WingsGrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public String getNameText() {
        return firstName + " " + lastName + " (" + username + ") (#" + memberId + ")";
    }

    public MemberOld getSponsor() {
        return sponsor;
    }

    public void setSponsor(MemberOld sponsor) {
        this.sponsor = sponsor;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public Order getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(Order productOrder) {
        this.productOrder = productOrder;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public boolean getColorSelected() {
        return colorSelected;
    }

    public void setColorSelected(boolean colorSelected) {
        this.colorSelected = colorSelected;
    }

    public boolean isInvitationConfirmed() {
        return invitationConfirmed;
    }

    public void setInvitationConfirmed(boolean invitationConfirmed) {
        this.invitationConfirmed = invitationConfirmed;
    }

}
