package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.Country;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author lucas
 */
public interface CountriesRepository extends JpaRepository<Country, String>, JpaSpecificationExecutor<Country> {
    List<Country> findByEnabled(Boolean enabled);
}