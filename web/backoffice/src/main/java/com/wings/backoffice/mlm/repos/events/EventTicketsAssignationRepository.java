/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventTicketAssignation;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface EventTicketsAssignationRepository extends JpaRepository<EventTicketAssignation, Long> {

    List<EventTicketAssignation> findByPurchaseId(Long purchaseId);
    
    List<EventTicketAssignation> findByPurchaseIdIn(List<Long> purchaseIds);
    
    EventTicketAssignation findByTicketId(Long ticketId);

}
