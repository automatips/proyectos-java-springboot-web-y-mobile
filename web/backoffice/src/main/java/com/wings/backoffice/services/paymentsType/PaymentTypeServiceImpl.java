/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.paymentsType;

import com.wings.backoffice.mlm.repos.PaymentTypeDao;
import com.wings.backoffice.mlm.domain.PaymentType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author maurib
 */
@Service
public class PaymentTypeServiceImpl implements PaymentTypeService{

    @Autowired
    private PaymentTypeDao paymentTypeDao;
    
    @Override
    public List<PaymentType> findAll() {
       return (List<PaymentType>)this.paymentTypeDao.findAll();
    }
    
}
