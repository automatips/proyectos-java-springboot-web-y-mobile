/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.domain.MemberSim;
import com.wings.backoffice.mlm.repos.MemberSimsRepository;
import com.wings.backoffice.mlm.domain.Sim;
import com.wings.backoffice.mlm.repos.SimsRepository;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.repos.OrdersRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas
 */
@Service
public class MemberSimsService {

    @Autowired
    private MemberSimsRepository membersimsRepository;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private SimsRepository simsRepository;
    @Autowired
    private OrdersService ordersService;

    public MemberSim getOneMemberSim(Long memberSimId) {
        return membersimsRepository.findOne(memberSimId);
    }

    public Member getOneMember(Long memberId) {
        return membersRepository.findOne(memberId);
    }

    public Sim getOneSim(Long simId) {
        return simsRepository.findOne(simId);
    }

    public MemberSim save(MemberSim memberSim) {
        return membersimsRepository.save(memberSim);
    }

    public List<MemberSim> save(List<MemberSim> memberSims) {
        return membersimsRepository.save(memberSims);
    }

    public List<MemberSim> getMemberSims(List<Long> membersimIds) {
        return membersimsRepository.findAll(membersimIds);
    }

    public List<MemberSim> getMemberSims(Long memberId) {
        return membersimsRepository.findByMemberId(memberId);
    }

    public List<Member> getMembers(List<Long> membersId) {
        return membersRepository.findAll(membersId);
    }

    public List<Sim> getSims(List<Long> simsId) {
        return simsRepository.findAll(simsId);
    }

    public List<Sim> getSims() {
        return simsRepository.findAll();
    }

    public List<Sim> getSimsByIccid(String iccid) {
        String iccidNumber = addLuhnVerificationNumber(iccid);
        return simsRepository.findByIccid(iccidNumber);
    }

    public String deleteMemberSim(Long memberSimId, HttpServletRequest request) {

        String ret;
        try {
            MemberSim m = membersimsRepository.findOne(memberSimId);
            membersimsRepository.delete(m);

            ret = "Sim desasignada exitosamente";
        } catch (Exception e) {
            ret = "Error al desasignar la Sim" + e.getMessage();
        }
        return ret;
    }

    public long getMemberSimCount(MemberSim m) {
        long ret;
        try {
            ret = membersimsRepository.count(Example.of(m));
        } catch (Exception e) {
            ret = 0;
        }
        return ret;
    }

    public long getMemberSimAssigned(Long memberId) {
        long ret = 0;
        try {
            long vItemId = 258;
            ret = ret + (ordersService.countByMemberIdAndItemIdAndDeletedAndStatus(memberId, vItemId, Boolean.FALSE, OrderStatus.paid.name()) * 5);

            vItemId = 259;
            ret = ret + (ordersService.countByMemberIdAndItemIdAndDeletedAndStatus(memberId, vItemId, Boolean.FALSE, OrderStatus.paid.name()) * 10);

            vItemId = 260;
            ret = ret + (ordersService.countByMemberIdAndItemIdAndDeletedAndStatus(memberId, vItemId, Boolean.FALSE, OrderStatus.paid.name()) * 20);

            vItemId = 261;
            ret = ret + (ordersService.countByMemberIdAndItemIdAndDeletedAndStatus(memberId, vItemId, Boolean.FALSE, OrderStatus.paid.name()) * 30);

            vItemId = 262;
            ret = ret + (ordersService.countByMemberIdAndItemIdAndDeletedAndStatus(memberId, vItemId, Boolean.FALSE, OrderStatus.paid.name()) * 50);

            return ret;
        } catch (Exception ex) {
            return 0;
        }
    }

    private String addLuhnVerificationNumber(String iccid) {
        if (iccid != null) {
            int sum = 0;
            for (int i = 0; i < iccid.length(); i++) {
                int digito = Character.digit(iccid.charAt(i), 10);
                if (i % 2 == 0) {
                    sum += digito;
                } else {
                    sum += 2 * digito;
                    if (digito >= 5) {
                        sum -= 9;
                    }
                }
            }
            if (sum % 10 == 0 && iccid.length() > 18) {
                return iccid;
            } else {
                sum *= 9;
                int luhnNumber = sum % 10;
                iccid += luhnNumber;
            }
        }
        return iccid;
    }
}
