/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface CalificationPointsRepository extends JpaRepository<CalificationPoint, Long>, JpaSpecificationExecutor<CalificationPoint> {

    List<CalificationPoint> findByMemberId(Long memberId);

    List<CalificationPoint> findByMemberIdOrderByDateAsc(Long memberId);

    List<CalificationPoint> findByMemberIdAndDateLessThanEqual(Long memberId, Date date);

    @Query(value = "select COALESCE(sum(points),0) from bis_calification where member_id = ?1", nativeQuery = true)
    long getTotalCalificationPoints(Long memberId);

}
