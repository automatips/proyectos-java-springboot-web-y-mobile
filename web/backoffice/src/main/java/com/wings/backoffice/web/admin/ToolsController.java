/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.marketing.services.SyncroService;
import com.wings.backoffice.services.BalanceService;
import com.wings.backoffice.services.BonusService;
import com.wings.backoffice.services.CalificationService;
import com.wings.backoffice.services.CountryOpenService;
import com.wings.backoffice.services.EnergyService;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.RanksService;
import com.wings.backoffice.services.ResidualServicesService;
import com.wings.backoffice.services.ToolsService;
import com.wings.backoffice.services.WingsAdsService;
import com.wings.backoffice.services.energy.response.Response;
import com.wings.backoffice.store.services.StoreService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class ToolsController {

    @Autowired
    private CalificationService calificationService;
    @Autowired
    private RanksService ranksService;
    @Autowired
    private MembersService membersService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private BonusService bonusService;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private MediaService mediaService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ResidualServicesService residualServicesService;
    @Autowired
    private EnergyService energyService;
    @Autowired
    private ToolsService toolsService;
    @Autowired
    private SyncroService syncroService;
    @Autowired
    private CountryOpenService countryOpenService;
    @Autowired
    private WingsAdsService wingsAdsService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/import"}, method = RequestMethod.GET)
    public String getImport(Model model, HttpServletRequest request) {
        model.addAttribute("running", toolsService.isRunning());
        return "admin/tools/import";
    }

    /**
     *
     * @param model
     * @param request
     * @param year
     * @param month
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/import/services/{year}/{month}"}, method = RequestMethod.POST)
    public ResponseEntity<String> importServices(Model model, HttpServletRequest request, @PathVariable("year") Integer year, @PathVariable("month") Integer month) {
        return ResponseEntity.ok(ordersService.importServiceOrders(year, month));
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/invoices/import"}, method = RequestMethod.POST)
    public ResponseEntity<String> importInvoices(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(residualServicesService.importLineInvoices());
    }

    /**
     * (0)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/calculation"}, method = RequestMethod.POST)
    public ResponseEntity<String> calculateAll(Model model, HttpServletRequest request) {
        toolsService.recalculate();
        return ResponseEntity.ok("success");
    }

    /**
     * (1)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/calification"}, method = RequestMethod.POST)
    public ResponseEntity<String> parseCalification(Model model, HttpServletRequest request) {
        calificationService.parse();
        return ResponseEntity.ok("success");
    }

    /**
     * (2)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/import/services/lines"}, method = RequestMethod.POST)
    public ResponseEntity<String> importServiceLines(Model model, HttpServletRequest request) {
        residualServicesService.importGsmLineServices();
        return ResponseEntity.ok("ok");
    }

    /**
     * (3)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/ranks"}, method = RequestMethod.POST)
    public ResponseEntity<String> parseRanks(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(ranksService.analizeRanks());
    }

    /**
     * (4)
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/differential/calc"}, method = RequestMethod.POST)
    public ResponseEntity<String> differentialCalc(Model model, HttpServletRequest request) {
        bonusService.calculateDifferential();
        return ResponseEntity.ok("success");
    }

    /**
     * (5)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/balances/generate"}, method = RequestMethod.POST)
    public ResponseEntity<String> generateBalances(Model model, HttpServletRequest request) {
        String ret;
        try {
            ret = balanceService.processBalance();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    /**
     * (6)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/balances/activate"}, method = RequestMethod.POST)
    public ResponseEntity<String> activateBalances(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(balanceService.activateProductsAndKitsBalances());
    }

    /**
     * (7)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/members/process"}, method = RequestMethod.POST)
    public ResponseEntity<String> processMembers(Model model, HttpServletRequest request) {
        try {
            membersService.postImportMembers();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("ok");
    }

    /**
     * (8)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/members/recalculate-enddate"}, method = RequestMethod.POST)
    public ResponseEntity<String> recalculateSpeedBonusEndDate(Model model, HttpServletRequest request) {
        try {
            ranksService.recalculateMemberEndDate();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("ok");
    }

    /**
     * (A)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/import/avatars"}, method = RequestMethod.POST)
    public ResponseEntity<String> importAvatars(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(mediaService.importAvatars());
    }

    /**
     * (B)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/fix/referers"}, method = RequestMethod.POST)
    public ResponseEntity<String> fixReferers(Model model, HttpServletRequest request) {
        return ResponseEntity.ok("Eliminado");
    }

    /**
     * (C)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/fix-stores"}, method = RequestMethod.POST)
    public ResponseEntity<String> fixStore(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(storeService.fixStores());
    }

    /**
     * (D)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/reset-ranks"}, method = RequestMethod.POST)
    public ResponseEntity<String> resetRanks(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(ranksService.resetRanks());
    }

    /**
     * (E)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/energy/test"}, method = RequestMethod.POST)
    public ResponseEntity<List<Response>> testEnergy(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(energyService.getContracts());
    }

    /**
     * (F)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/order/fixdni"}, method = RequestMethod.POST)
    public ResponseEntity<String> fixdni(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(storeService.fixDnis());
    }

    /**
     * (G)
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/sync-marketing"}, method = RequestMethod.POST)
    public ResponseEntity<String> syncMarketing(Model model, HttpServletRequest request) {
        String ret = wingsAdsService.validateEmail("lino@wingsmobile.com");
        return ResponseEntity.ok(ret);
    }

    /**
     *
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/admin/tools/order-mark-installment"}, method = RequestMethod.POST)
    public ResponseEntity<String> markInstallment(Model model, HttpServletRequest request) {
        //ordersService.markAsInstallmentComplete();
        countryOpenService.getCountryLevelInfo();
        return ResponseEntity.ok("ok");
    }
}
