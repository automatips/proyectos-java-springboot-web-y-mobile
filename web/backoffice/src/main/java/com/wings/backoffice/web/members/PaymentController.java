/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.NativeServicesService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.PaymentsService;
import com.wings.backoffice.services.StoreItemsService;
import com.wings.backoffice.services.TopUpsService;
import com.wings.backoffice.utils.DateUtils;
import com.wings.payments.client.dto.CallbackDTO;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class PaymentController {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private TopUpsService topUpsService;
    @Autowired
    private EventsService eventsService;
    @Autowired
    private PaymentsService paymentsService;
    @Autowired
    private StoreItemsService storeItemsService;
    @Autowired
    private NativeServicesService nativeServicesService;

    private final Logger log = LoggerFactory.getLogger(PaymentController.class);

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/members/pay"}, method = {RequestMethod.GET})
    public String payOrder(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        Order order = (Order) model.asMap().get("order");
        String url = paymentsService.createPayment(order);
        if (url != null) {
            return "redirect:" + url;
        }

        //si hay error muestro mensaje
        redirectAttributes.addFlashAttribute("message", "Ocurrió un error con la pasarela, intente nuevamente mas tarde");
        redirectAttributes.addFlashAttribute("messageType", "error");

        return "redirect:/";
    }

    /**
     *
     * @param redirectAttributes
     * @param request
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/members/pay/{orderId}"}, method = {RequestMethod.GET})
    public String payOrderRedirect(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("orderId") Long orderId) {
        Order order = ordersService.getOrder(orderId);
        redirectAttributes.addFlashAttribute("order", order);
        return "redirect:/members/pay";
    }

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/payments/callback/"}, method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<String> callbackWingsPays(HttpServletRequest request) {
        try {
            //TODO: verificar firma
            ObjectMapper mapper = new ObjectMapper();
            CallbackDTO dto = mapper.readValue(request.getReader(), CallbackDTO.class);

            String orderNumber = dto.getOrderNumber();
            Order order = ordersService.getOrder(Long.parseLong(orderNumber));
            //order already marked as pay, do nothing
            if (order.getStatus().equals(OrderStatus.paid.name())) {
                return ResponseEntity.ok("");
            }

            String status = OrderStatus.pending.name();
            if (dto.getStatus().equals("COMPLETED")) {
                status = OrderStatus.paid.name();
                order.setActivationDate(dto.getStatusDate());
                order.setActive(true);
                order.setDeleted(false);//por si estaba eliminada de antes
                order.setComment("Payment mark by system, using  " + dto.getProcessor() + " " + dto.getStatusMessage() + " " + new Date() + (order.getComment() != null ? System.lineSeparator() + order.getComment() : ""));
            }

            order.setPaymentType(dto.getProcessor());
            order.setTypeStatus(status);
            order.setPaymentStatus(status);
            order.setStatus(status);
            order.setModificationDate(new Date());

            ordersService.save(order);

            //TODO: estas dos cosas deberia ir en el order service            
            //if it's from tickets, fullfill product purchase
            if (order.getType().equals(OrderType.ticket.name()) && order.getStatus().equals(OrderStatus.paid.name())) {
                eventsService.fulfillEventProductPurchase(order);
            }

            //if it's from topup, fullfill topup
            if (order.getType().equals(OrderType.topup.name())
                    && order.getStatus().equals(OrderStatus.paid.name())
                    && order.getPhone() != null && !order.getPhone().trim().isEmpty()
                    && !order.getTypeStatus().equals(OrderStatus.complete.name())) {

                //Servicios nativos
                StoreItem storeItem = storeItemsService.getOne(order.getItemId());
                if (storeItem.getSubCategoryName() != null && storeItem.getSubCategoryName().equals("voip")) {
                    Double doble = storeItem.getPrice();
                    if (nativeServicesService.topUpNumber(order.getPhone(), doble, order.getId())) {
                        order.setTypeStatus(OrderStatus.complete.name());
                        order.setComment("Topup applied " + DateUtils.formatNormalDateTime(new Date()) + " to " + order.getPhone() + " " + order.getOrderItem().getName() + " " + order.getComment());
                        ordersService.save(order);
                    }
                } else {
                    //Servicios de SUMA                       
                    if (topUpsService.redeemPackage(order.getPhone(), order)) {
                        order.setTypeStatus(OrderStatus.complete.name());
                        order.setComment("Pkg applied " + DateUtils.formatNormalDateTime(new Date()) + " to " + order.getPhone() + " " + order.getOrderItem().getName() + " " + order.getComment());
                        ordersService.save(order);
                    } else {
                        log.error("Package reedem error for " + order.getId());
                    }
                }
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("");
    }

}
