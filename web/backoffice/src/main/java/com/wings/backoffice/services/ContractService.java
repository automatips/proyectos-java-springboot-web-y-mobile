/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.config.WingsProperties;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.utils.DateUtils;
import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class ContractService {

    private final String postUrl;
    private final String authString;

    private final Logger log = LoggerFactory.getLogger(ContractService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    public ContractService(WingsProperties wingsProperties) {
        this.postUrl = wingsProperties.getSignature().getUrl();
        String toEncode = wingsProperties.getSignature().getUsername() + ":" + wingsProperties.getSignature().getPassword();
        byte[] encodedBytes = Base64.encodeBase64(toEncode.getBytes());
        authString = new String(encodedBytes);
    }

    public File getMemberContract(Member member) {

        String path = "/var/wings/contract/afiliation/members/" + member.getUsername() + ".pdf";
        File ret = new File(path);
        if (!ret.exists()) {
            ret = generateContract(member);
        }
        return ret;

    }

    public String postDocument(Member member, File contract) {

        String ret = "";
        RestTemplate template = new RestTemplate();

        File file = new File(contract.getPath());

        String desc = "Contrato Afiliación";

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new FileSystemResource(file));

        String name = member.getNameText();
        if (name.length() > 35) {
            name = name.substring(0, 35);
        }
        map.add("description", "Firmado digital - " + name + desc);
        map.add("reference", member.getUsername() + "_afiliate_" + DateUtils.formatNormalDateTime(new Date()));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", "Basic " + authString);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        try {

            ResponseEntity<String> result = template.postForEntity(postUrl, requestEntity, String.class);
            String url = result.getHeaders().getFirst("Location");
            member.setContractUrl(url);

            //add signer
            String signersUrl = url + "signers/";
            template = new RestTemplate();

            map = new LinkedMultiValueMap<>();
            map.add("name", member.toString());
            map.add("id-country", "ES");
            map.add("id-number", member.getDni());
            map.add("type", "2");            
            map.add("phone-number", member.getPhone());
            map.add("access-url", "true");

            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.add("Authorization", "Basic " + authString);

            requestEntity = new HttpEntity<>(map, headers);
            result = template.postForEntity(signersUrl, requestEntity, String.class);
            ret = result.getHeaders().getFirst("Location");

        } catch (RestClientException e) {
            log.error("Error posting contract ", e);
        }
        return ret;
    }

    public void deleteDocument(Member m) {

        if (m.getContractUrl() != null && !m.getContractUrl().isEmpty()) {
            try {
                String url = m.getContractUrl();
                RestTemplate template = new RestTemplate();
                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", "Basic " + authString);
                RequestEntity request = new RequestEntity(headers, HttpMethod.DELETE, new URI(url));
                template.exchange(request, String.class);

            } catch (Exception e) {
                log.error("Error deleting contract ", e);
            }
        }

    }

    public File generateContract(Member member) {

        try
        {
            boolean company = member.getIsCompany();

            String basePath = "/var/wings/contract/afiliation/";
            String storePath = basePath + "members/" + member.getUsername() + ".pdf";
            String form = basePath + "afiliacion_persona.pdf";
            if (company) {
                form = basePath + "afiliacion_empresa.pdf";
            }

            String pais = messageSource.getMessage(member.getCountry(), null, new Locale("es", "ES"));
            Map<String, String> map = new HashMap<>();

            map.put("Nombre/razon social", member.getFirstName() + " " + member.getLastName());
            map.put("fecha nacimiento", DateUtils.formatNormalDate(member.getBirthDate()));
            map.put("direcion", member.getStreet());
            map.put("poblacion", member.getCity());
            map.put("codigo postal", member.getPostal());
            map.put("provincia", member.getState());
            map.put("pais", pais);
            map.put("E-mail", member.getEmail());
            map.put("dni", member.getDniType() + " " + member.getDni());
            map.put("Nacionalidad", pais);
            map.put("FECHA", DateUtils.formatNormalDate(member.getRegistrationDate()));

            if (company) {
                map.put("fecha", DateUtils.formatNormalDate(member.getRegistrationDate()));
                map.put("dni", member.getCompanyName());
                map.put("Texto1", member.getDniType() + " " + member.getDni());
                map.put("Nombre rep.legal", member.getFirstName() + " " + member.getLastName());
                map.put("Texto 1", member.getDniType() + " " + member.getDni());
                map.put("Nacionalidad", member.getCompanyDniType() + " " + member.getCompanyDni());
            }

            PDDocument pdf;

            pdf = PDDocument.load(new File(form));

            PDDocumentCatalog docCatalog = pdf.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();
            List<PDField> fields = acroForm.getFields();
            for (PDField field : fields) {
                field.setValue(map.get(field.getPartialName()));
            }
            acroForm.flatten();
            pdf.save(storePath);
            pdf.close();

            return new File(storePath);   
        }
        catch(Exception ex)
        {
            //log.error("Error on pdf", ex);
            return null;
        }
    }

}
