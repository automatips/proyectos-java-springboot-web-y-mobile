/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author seba
 */
public class QrUtils {

    public static final String QR_DIRS = "/var/wings/bo_media/events_content/qr";

    public static File generateQrCode(String data) {
        File ret = null;
        try {

            String fileName = QR_DIRS + "/" + DigestUtils.sha256Hex(data) + ".png";
            ret = new File(fileName);
            if (ret.exists()) {
                return ret;
            }

            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(data, BarcodeFormat.QR_CODE, 250, 250);
            Path path = FileSystems.getDefault().getPath(ret.getAbsolutePath());
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);

        } catch (WriterException | IOException e) {

        }
        return ret;
    }

}
