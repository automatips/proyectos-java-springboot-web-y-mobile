/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankHistory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class RankHistorySpecs {

    public static Specification<RankHistory> search(String q, Boolean auto) {

        return (Root<RankHistory> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<RankHistory, Member> member = root.join("member");

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(member.get("firstName"), search),
                                builder.like(member.get("lastName"), search),
                                builder.like(member.get("email"), search),
                                builder.like(member.get("username"), search),
                                builder.like(builder.concat(builder.concat(member.get("firstName"), " "), member.get("lastName")), search)
                        )
                );
            }

            if (auto != null) {
                predicates.add(builder.equal(root.get("auto"), auto));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    public static Specification<RankHistory> searchByMember(Long id, Boolean auto) {

        return (Root<RankHistory> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<RankHistory, Member> member = root.join("member");

            List<Predicate> predicates = new ArrayList<>();
            if (id != null) {
                predicates.add(builder.equal(root.get("memberId"), id));
            }

            if (auto != null) {
                predicates.add(builder.equal(root.get("auto"), auto));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
