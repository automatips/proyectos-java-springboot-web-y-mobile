/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventsProductPurchase;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface EventProductPurchaseRepository extends JpaRepository<EventsProductPurchase, Long> {

    int countByEventId(Long eventId);

    @Transactional
    int deleteByEventId(Long eventId);

    List<EventsProductPurchase> findByMemberIdAndEventProductIdAndStatus(Long memberId, Long eventProductId, String status);

    List<EventsProductPurchase> findByMemberIdAndStatusInOrderByIdDesc(Long memberId, List<String> status);

    List<EventsProductPurchase> findByEventId(Long eventId);

    Page<EventsProductPurchase> findByEventId(Long eventId, Pageable page);

    EventsProductPurchase findByOrderId(Long orderId);
    
    long countByEventIdAndStatus(Long eventId, String status);
    

}
