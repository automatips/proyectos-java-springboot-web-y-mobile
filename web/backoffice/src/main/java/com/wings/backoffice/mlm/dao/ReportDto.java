/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucas
 */
public class ReportDto {

    private List<CountryDto> countryDtos;
    private String currency;

    public ReportDto(List<CountryDto> countryDtos, Map<String, Integer> productsByCountry) {
        this.countryDtos = countryDtos != null ? countryDtos : new ArrayList<>();
    }

    public ReportDto() {
        this(null, null);
    }

    public List<CountryDto> getCountryDtos() {
        return countryDtos;
    }

    public void setCountryDtos(List<CountryDto> countryDtos) {
        this.countryDtos = countryDtos;
    }

    public void addCountryDto(CountryDto countryDto) {
        this.countryDtos.add(countryDto);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /*
     Métodos para el reporte de afiliados por país.
     */
    public Integer membersByYearAndMonth(Integer year, Integer month, Boolean active) {
        Integer total = 0;
        total = countryDtos.stream().map((countryDto) -> countryDto.getMembersByActiveAndYearAndMonth(year, month, active)).reduce(total, Integer::sum);
        return total;
    }

    public Integer membersByYear(Integer year, Boolean active) {
        Integer total = 0;
        total = countryDtos.stream().map((countryDto) -> countryDto.getMembersByActiveAndYear(year, active)).reduce(total, Integer::sum);
        return total;
    }

    public String pendings(Long itemId, Integer year) {
        int count = 0;
        Double total = 0.0, temp, amount;

        for (CountryDto country : countryDtos) {
            List<OrderDto> ordersPendingBy = itemId != null? country.getOrdersPendingBy(itemId, year) : country.getOrdersPendingBy(null, year);
            count += ordersPendingBy.size();
            Double countryTotal = country.getTotalPending(ordersPendingBy);

            if (currency == null) {
                Double euros = NumberUtils.convertMoneyToEuros(countryTotal, CountryUtils.getLocaleForCountry(country.getCountry()));
                countryTotal = NumberUtils.convertMoneyFromEuros(euros, "EC");
            }

            total += countryTotal;
        }

        if(currency == null){
            return NumberUtils.formatMoney(total, CountryUtils.getLocaleForCountry("EC")) + " (" + count + ")";
        }else{
            return NumberUtils.formatMoney(total, currency) + " (" + count + ")";
        }
    }
}
