/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.OrderItem;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class TestService {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private CountriesService countriesService;
    @Autowired
    private BacsService bacsService;

    private final Logger log = LoggerFactory.getLogger(TestService.class);
    private List<Order> all;
    private List<Order> orderPaid;
    private final Map<String, List<Order>> ordersPerCountry = new HashMap<>();
    private final Map<String, List<Order>> ordersPaidPerCountry = new HashMap<>();
    private final Map<Long, List<OrderItem>> orderItems = new HashMap<>();

    boolean test = false;

    /**
     *
     * @param countries
     * @param months
     * @param years
     * @param from
     * @param to
     * @param paymentStatus
     * @return
     */
    public List<TestServiceDto> getOrdersByProducts(List<String> countries, List<Integer> months, List<Integer> years, Date from, Date to, String paymentStatus) {

        //List<String> names = ordersService.getItemsNamesByCategory("product");
        List<String> names = Arrays.asList("W2", "W5", "W3", "W6", "W4", "W7", "Wings OneFour", "Wings BOOK", "Wings ONEFOUR PRO", "WINGS STORE BASE", "WINGS STORE FULL", "WINGS STORE CORNER LIGHT", "WINGS STORE CORNER FULL", "Smartwatch Fashion", "Wings Executive", "Wings Watch Light", "Smartwatch Executive", "SIM Colombia", "7 Dias para Volar - Daniele Viganó", "Computador All In One", "Wings Scooter", "Wings Doors", "Wings Water", "Lámpara luz ultravioleta", "Termómetro \"no táctil\"");

        //TODO: por fechas y summary        
        List<TestServiceDto> list = new LinkedList<>();
        for (String country : countries) {

            years.forEach((Integer year) -> {

                TestServiceDto dto = new TestServiceDto();
                List<String> cols = new LinkedList<>();
                cols.addAll(Arrays.asList("month"));
                cols.addAll(names);
                dto.setColumns(cols);

                List<List<String>> rows = new LinkedList<>();
                dto.setRows(rows);
                dto.setTitle(country + " " + year);

                //                
                months.stream().map((month) -> {

                    Date f = DateUtils.getStartDateForPeriod(month, year);
                    Date t = DateUtils.getEndDateForPeriod(month, year);

                    List<Long> ids = new ArrayList<>();
                    if (paymentStatus == null || paymentStatus.equals("all")) {

                        if (ordersPerCountry.get(country) != null) {

                            Stream<Order> stream = ordersPerCountry.get(country).stream()
                                    .filter(o -> !o.isDeleted())
                                    .filter(o -> o.getCreationDate().after(f) && o.getCreationDate().before(t));

                            stream.forEach(order -> {
                                ids.add(order.getId());
                            });

                        }

                    } else if (paymentStatus.equals("complete")) {

                        if (ordersPaidPerCountry.get(country) != null) {

                            Stream<Order> stream = ordersPaidPerCountry.get(country).stream()
                                    .filter(o -> o.getActivationDate().after(f) && o.getActivationDate().before(t))
                                    .filter((o) -> (o.getInstallmentNumber() == null || o.getInstallmentNumber() == 0) || (o.getInstallmentNumber() != null && o.getInstallmentNumber() == 1 && o.getInstallmentComplete()));

                            stream.forEach(order -> {
                                ids.add(order.getId());
                            });
                        }

                    } else if (paymentStatus.equals("partial")) {

                        if (ordersPaidPerCountry.get(country) != null) {

                            Stream<Order> stream = ordersPaidPerCountry.get(country).stream()
                                    .filter(o -> o.getActivationDate().after(f) && o.getActivationDate().before(t));
                            stream.forEach(order -> {
                                ids.add(order.getId());
                            });

                        }
                    }

                    List<String> row = new LinkedList<>();
                    row.add(DateUtils.getMonthName(month));

                    //List<OrderItem> details = ordersService.getOrderDetailsByOrderIds(ids);
                    List<OrderItem> details = new ArrayList<>();
                    ids.forEach(id -> {
                        if (orderItems.get(id) != null) {
                            details.addAll(orderItems.get(id));
                        }
                    });

                    names.forEach(name -> {
                        Integer total = 0;
                        total = details.stream()
                                .filter((o) -> o.getName().equals(name))
                                .map((order) -> order.getQuantity())
                                .reduce(total, (accumulator, _item) -> accumulator + _item);
                        row.add(String.valueOf(total));
                    });
                    return row;

                }).forEachOrdered((row) -> {
                    rows.add(row);
                });

                list.add(dto);
            });
        }

        return list;
    }

    /**
     *
     * @param countries
     * @param months
     * @param years
     * @param from
     * @param to
     * @param currency
     * @return
     */
    public List<TestServiceDto> getOrdersByBacs(List<String> countries, List<Integer> months, List<Integer> years, Date from, Date to, String currency) {

        //TODO: por fechas y summary        
        List<TestServiceDto> list = new LinkedList<>();
        for (String country : countries) {

            String tempCurrency;
            if (currency == null) {
                tempCurrency = countriesService.getPropertiesByCode(country).getCurrency();
            } else {
                tempCurrency = currency;
            }
            List<String> bacs = bacsService.getBacsForCountry(country);
            if (bacs == null || bacs.isEmpty()) {
                continue;
            }

            years.forEach((Integer year) -> {

                TestServiceDto dto = new TestServiceDto();
                List<String> cols = new LinkedList<>();
                cols.addAll(Arrays.asList("month"));
                cols.addAll(bacs);
                cols.add("others");
                cols.add("total");
                dto.setColumns(cols);

                List<List<String>> rows = new LinkedList<>();
                dto.setRows(rows);
                dto.setTitle(country + " " + year);

                months.stream().map((month) -> {

                    double sumTotal = 0d;
                    Date f = DateUtils.getStartDateForPeriod(month, year);
                    Date t = DateUtils.getEndDateForPeriod(month, year);
                    List<String> row = new LinkedList<>();
                    row.add(DateUtils.getMonthName(month));
                    for (String bac : bacs) {
                        Stream<Order> stream = getFilterBacs(getFilter(country, f, t, "bacs"), bac);
                        double temp = 0d;
                        if (stream != null) {
                            temp = getSum(stream, country, tempCurrency);
                        }
                        sumTotal += temp;
                        row.add(NumberUtils.formatMoney(temp, tempCurrency));
                    }
                    Stream<Order> stream = getFilterBacs(getFilter(country, f, t, "bacs"), null);
                    double temp = 0d;
                    if (stream != null) {
                        temp = getSum(stream, country, tempCurrency);
                    }
                    sumTotal += temp;
                    row.add(NumberUtils.formatMoney(temp, tempCurrency));
                    row.add(NumberUtils.formatMoney(sumTotal, tempCurrency));
                    return row;

                }).forEachOrdered((row) -> {
                    rows.add(row);
                });

                list.add(dto);
            });
        }

        return list;
    }

    /**
     *
     * @param countries
     * @param months
     * @param years
     * @param from
     * @param to
     * @param currency
     * @return
     */
    public List<TestServiceDto> getOrdersByCountryDto(List<String> countries, List<Integer> months, List<Integer> years, Date from, Date to, String currency) {

        List<TestServiceDto> list = new LinkedList<>();
        //from  date to date
        if (from != null || to != null) {

            if (from == null) {
                from = DateUtils.getStartDateForPeriod(1, 2016);
            } else {
                from = DateUtils.getFirstHourOfDay(from);
            }

            if (to == null) {
                to = DateUtils.getEndDateCurrentMonth();
            } else {
                to = DateUtils.getLastHourOfDay(to);
            }

            for (String country : countries) {

                String tempCurrency;
                if (currency == null) {
                    tempCurrency = countriesService.getPropertiesByCode(country).getCurrency();
                } else {
                    tempCurrency = currency;
                }

                TestServiceDto dto = new TestServiceDto();
                dto.setColumns(Arrays.asList("month", "total", "bacs", "others", "coms"));
                List<List<String>> rows = new LinkedList<>();
                dto.setRows(rows);
                String match = DateUtils.formatNormalDate(from) + " - " + DateUtils.formatNormalDate(to);
                dto.setTitle(country + " " + match);

                double sumBacs = 0d;
                double sumComs = 0d;
                double sumOther = 0d;
                double sumTotal = 0d;

                List<String> row = new LinkedList<>();

                //sums
                double bacs = getSum(getFilter(country, from, to, "bacs"), country, tempCurrency);
                double coms = getSum(getFilter(country, from, to, "coms"), country, tempCurrency);
                double other = getSum(getFilter(country, from, to, "other"), country, tempCurrency);
                double total = bacs + coms + other;

                sumTotal += total;
                sumBacs += bacs;
                sumOther += other;
                sumComs += coms;

                //cells
                row.add(match);
                row.add(NumberUtils.formatMoney(total, tempCurrency));
                row.add(NumberUtils.formatMoney(bacs, tempCurrency));
                row.add(NumberUtils.formatMoney(other, tempCurrency));
                row.add(NumberUtils.formatMoney(coms, tempCurrency));

                rows.add(row);

                //summary
                dto.setSummary(Arrays.asList("Total",
                        NumberUtils.formatMoney(sumTotal, tempCurrency),
                        NumberUtils.formatMoney(sumBacs, tempCurrency),
                        NumberUtils.formatMoney(sumOther, tempCurrency),
                        NumberUtils.formatMoney(sumComs, tempCurrency)
                ));
                list.add(dto);

            }

        } else {

            for (String country : countries) {

                String tempCurrency;
                if (currency == null) {
                    tempCurrency = countriesService.getPropertiesByCode(country).getCurrency();
                } else {
                    tempCurrency = currency;
                }

                for (Integer year : years) {

                    TestServiceDto dto = new TestServiceDto();
                    dto.setColumns(Arrays.asList("month", "total", "bacs", "others", "coms"));
                    List<List<String>> rows = new LinkedList<>();
                    dto.setRows(rows);
                    dto.setTitle(country + " " + year);

                    double sumBacs = 0d;
                    double sumComs = 0d;
                    double sumOther = 0d;
                    double sumTotal = 0d;

                    for (Integer month : months) {

                        List<String> row = new LinkedList<>();
                        Date f = DateUtils.getStartDateForPeriod(month, year);
                        Date t = DateUtils.getEndDateForPeriod(month, year);

                        //sums
                        double bacs = getSum(getFilter(country, f, t, "bacs"), country, tempCurrency);
                        double coms = getSum(getFilter(country, f, t, "coms"), country, tempCurrency);
                        double other = getSum(getFilter(country, f, t, "other"), country, tempCurrency);
                        double total = bacs + coms + other;

                        sumTotal += total;
                        sumBacs += bacs;
                        sumOther += other;
                        sumComs += coms;

                        //cells
                        row.add(DateUtils.getMonthName(month));
                        row.add(NumberUtils.formatMoney(total, tempCurrency));
                        row.add(NumberUtils.formatMoney(bacs, tempCurrency));
                        row.add(NumberUtils.formatMoney(other, tempCurrency));
                        row.add(NumberUtils.formatMoney(coms, tempCurrency));

                        rows.add(row);
                    }

                    //summary
                    dto.setSummary(Arrays.asList("Total",
                            NumberUtils.formatMoney(sumTotal, tempCurrency),
                            NumberUtils.formatMoney(sumBacs, tempCurrency),
                            NumberUtils.formatMoney(sumOther, tempCurrency),
                            NumberUtils.formatMoney(sumComs, tempCurrency)
                    ));
                    list.add(dto);
                }
            }
        }

        return list;
    }

    /**
     *
     * @param country
     * @param from
     * @param to
     * @param type
     * @return
     */
    public List<Order> getOrders(String country, Date from, Date to, String type) {
        Stream<Order> stream = getFilter(country, from, to, type);
        List<Order> ret = stream.collect(Collectors.toList());
        return ret;
    }

    /**
     *
     * @param country
     * @param from
     * @param to
     * @param type
     * @param activation
     * @param currency
     * @return
     */
    private Stream<Order> getFilter(String country, Date from, Date to, String type) {
        List<Order> orders = ordersPaidPerCountry.get(country);
        if (orders == null) {
            return null;
        }
        Stream<Order> stream = orders.stream();
        stream = stream.filter(o -> (o.getPaymentDate() != null) ? o.getPaymentDate().after(from) && o.getPaymentDate().before(to) : o.getActivationDate().after(from) && o.getActivationDate().before(to));

        if (type.equals("other")) {
            stream = stream.filter(order -> !(order.getPaymentType().equals("bacs") || order.getPaymentType().equals("coms")));
        } else {
            stream = stream.filter(order -> order.getPaymentType().equals(type));
        }
        return stream;
    }

    /**
     *
     * @param stream
     * @param paymentBacs
     * @return
     */
    public Stream<Order> getFilterBacs(Stream<Order> stream, String paymentBacs) {
        try {
            if (paymentBacs == null) {
                return stream.filter(order -> order.getPaymentBacs() == null);
            } else {
                return stream.filter(order -> order.getPaymentBacs() != null && order.getPaymentBacs().equals(paymentBacs));
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param stream
     * @param country
     * @param currency
     * @return
     */
    private Double getSum(Stream<Order> stream, String country, String currency) {

        Double total = 0D;
        if (stream == null) {
            return total;
        }

        total = stream
                .map((order) -> order.getPaymentAmount())
                .reduce(total, (accumulator, _item) -> accumulator + _item);

        if (currency != null) {
            double temp = NumberUtils.convertMoneyToEuros(total, countriesService.getLocalesMap().get(country));
            temp = NumberUtils.convertMoneyFromEuros(temp, currency);
            total = temp;
        }

        return total;
    }

    /**
     *
     */
    @Scheduled(initialDelay = 1000, fixedRate = 10800000)
    public void loadOrders() {

        log.error("Start TEST service");
        if (all != null) {
            all.clear();
        }
        if (orderPaid != null) {
            orderPaid.clear();
            ordersPerCountry.clear();
            ordersPaidPerCountry.clear();
            orderItems.clear();
        }
        all = ordersService.findAll();
        orderPaid = new ArrayList<>();

        all.forEach((order) -> {

            String country = order.getBillingCountry();
            if (ordersPerCountry.containsKey(country)) {
                ordersPerCountry.get(country).add(order);
            } else {
                List<Order> temp = new ArrayList<>();
                temp.add(order);
                ordersPerCountry.put(country, temp);
            }

            //paid and non deleted and non free
            if (order.getStatus().equals(OrderStatus.paid.name()) && !order.isDeleted() && order.getItemId() != 15) {
                orderPaid.add(order);

                if (ordersPaidPerCountry.containsKey(country)) {
                    ordersPaidPerCountry.get(country).add(order);
                } else {
                    List<Order> temp = new ArrayList<>();
                    temp.add(order);
                    ordersPaidPerCountry.put(country, temp);
                }
            }
        });

        List<Long> ids = new ArrayList<>();
        all.forEach(order -> {
            ids.add(order.getId());
        });

        List<OrderItem> items = ordersService.getOrderDetailsByOrderIds(ids);
        items.forEach(item -> {
            if (orderItems.containsKey(item.getOrderId())) {
                orderItems.get(item.getOrderId()).add(item);
            } else {
                List<OrderItem> itemsTemp = new ArrayList<>();
                itemsTemp.add(item);
                orderItems.put(item.getOrderId(), itemsTemp);
            }
        });

        log.error("Done TEST service");
    }

    /**
     * TEST TEST TEST TEST
     */
    public void testCleanUpOrders() {
        log.error("Start clean up orders details");
        all.forEach(order -> {
            ordersService.processOrderDetails(order);
        });
        log.error("Done clean up orders details");
    }

    public void testDetailsSums(List<String> countries, List<Integer> months, List<Integer> years, Date from, Date to) {

        /*ist<Long> ids = new ArrayList<>();

        //pagos completos
        Stream<Order> stream = orderPaid.stream()
                .filter((o) -> (o.getInstallmentNumber() == null || o.getInstallmentNumber() == 0) || (o.getInstallmentNumber() != null && o.getInstallmentNumber() == 1 && o.getInstallmentComplete()));
        stream.forEach(order -> {
            ids.add(order.getId());
        });

        List<OrderItem> details = ordersService.getOrderDetails(ids);
        Set<String> names = new HashSet<>();
        details.forEach(detail -> {
            names.add(detail.getName());
        });

        names.forEach(name -> {
            Integer total = 0;
            total = details.stream()
                    .filter((o) -> o.getName().equals(name))
                    .map((order) -> order.getQuantity())
                    .reduce(total, (accumulator, _item) -> accumulator + _item);
            System.out.println(name + " -> " + total);
        });*/
    }
}
