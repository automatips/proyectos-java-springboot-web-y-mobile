/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config;

import com.wings.backoffice.config.auth.WingsAuthenticationFailureHandler;
import com.wings.backoffice.config.auth.WingsAuthenticationSuccessHandler;
import com.wings.backoffice.config.auth.WingsAutheticatorProvider;
import com.wings.backoffice.config.auth.WingsUsersDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private WingsUsersDetailsService wingsUsersDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.authorizeRequests().anyRequest().permitAll();                
        http
                .csrf().disable()
                //.addFilterAfter(switchUserFilter(), FilterSecurityInterceptor.class)
                .cors().disable()
                .authorizeRequests()
                .antMatchers("/css/**/*", "/js/**/*", "/vendors/**/*", "/fonts/**/*", "/img/**/*", "/images/**/*").permitAll()
                .antMatchers("/api/v1/**/*").permitAll()
                .antMatchers(HttpMethod.POST,
                        "/api/payments/callback/**/*"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/api/payments/callback/"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/api/payments/coms/charge"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/api/payments/callback/**/*"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/eventos/**/*"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/eventos/**/*"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/eventos/register"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/eventos/register"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/eventos/registerec"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/eventos/registerec"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/eventos/afiliateec"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/eventos/afiliateec"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/api/members/login"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/open/invoices/**/*"
                ).permitAll()
                .antMatchers("/member/status/**/*", "/member/service/sales/**/*", "/layout/**/", "/checkingbonus/**/*", "/checkingbonus", "/api/events/**/**/*"
                ).permitAll()
                .antMatchers("/members/**/*").access("hasAnyRole('MEMBER','ADMIN')")
                .antMatchers("/admin/**/*").access("hasRole('ADMIN')")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .failureHandler(failure())
                .successHandler(successHandler())
                .permitAll()
                .and()
                .logout()
                .permitAll().and()
                .exceptionHandling().authenticationEntryPoint(new AjaxAwareAuthenticationEntryPoint("/login"));

        http.sessionManagement()
                .invalidSessionUrl("/login?invalid")
                .enableSessionUrlRewriting(false)
                .maximumSessions(1)
                .expiredUrl("/login?expire")
                .maxSessionsPreventsLogin(false)
                .sessionRegistry(sessionRegistry())
                .and()
                .sessionFixation().none();

        http.headers()
                .frameOptions()
                .sameOrigin();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(new WingsAutheticatorProvider(wingsUsersDetailsService));
        /*.inMemoryAuthentication()
                .withUser("user").password("test").roles("USER")
                .and()
                .withUser("admin").password("admin").roles("ADMIN","ACTUATOR");*/
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("css/**/*", "js/**/*", "/images/**", "/img/**", "/vendors/**", "/fonts/**/*", "/open/**/*");
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(
                        "http://wingsmobile.net",
                        "http://www.wingsmobile.net",
                        "https://wingsmobile.net",
                        "https://www.wingsmobile.net",
                        "http://beta.wingsmobile.net",
                        "https://beta.wingsmobile.net",
                        "https://dev.backoffice.wingsmobile.com",
                        "https://backoffice.wingsmobile.com",
                        "http://localhost:8099"
                ).allowedHeaders("Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Authorization", "X-Requested-With", "Origin", "X-Frame-Options");
            }
        };
    }

    @Bean
    @DependsOn("localeResolver")
    public AuthenticationSuccessHandler successHandler() {
        return new WingsAuthenticationSuccessHandler();
    }

    @Bean
    public AuthenticationFailureHandler failure() {
        return new WingsAuthenticationFailureHandler();
    }

    @Bean("sessionRegistry")
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowSemicolon(true);
        return firewall;
    }

//    @Bean
//    public SwitchUserFilter switchUserFilter() {
//        SwitchUserFilter filter = new SwitchUserFilter();
//        filter.setUserDetailsService(wingsUsersDetailsService);
//        filter.setUsernameParameter("username");
//        filter.setSwitchUserUrl("/admin/member/login");        
//        filter.setExitUserUrl("/admin/member/logout");
//        filter.setTargetUrl("/");
//        return filter;
//    }
}
