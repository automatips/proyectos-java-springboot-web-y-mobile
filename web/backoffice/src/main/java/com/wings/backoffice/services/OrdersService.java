/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.api.models.CartDto;
import com.wings.api.models.CartDtoResponse;
import com.wings.api.models.PersonalInfoDto;
import com.wings.api.models.StoreItemDto;
import com.wings.backoffice.mlm.dao.OrderDao;
import com.wings.backoffice.mlm.dao.OrderDto;
import com.wings.backoffice.mlm.domain.bis.orders.ProductsView;
import com.wings.backoffice.mlm.domain.Customer;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.OrderDetailOld;
import com.wings.backoffice.mlm.domain.OrderHeader;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.OrderItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemChild;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemI18n;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemOption;
import com.wings.backoffice.mlm.enums.OrderPaymentStatus;
import com.wings.backoffice.mlm.enums.OrderShipmentStatus;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.enums.ShipmentStatus;
import com.wings.backoffice.mlm.repos.CustomersRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.OrderDetailsRepository;
import com.wings.backoffice.mlm.repos.OrderHeaderRepository;
import com.wings.backoffice.mlm.repos.OrderItemsRepository;
import com.wings.backoffice.mlm.repos.OrdersRepository;
import com.wings.backoffice.mlm.repos.ProductsViewRepository;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.mlm.specs.OrdersSpecs;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import com.wings.backoffice.utils.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 *
 * @author seba
 */
@Service
public class OrdersService {

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;
    @Autowired
    private OrderHeaderRepository orderHeaderRepository;
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private GsmLinesService gmGsmLinesService;
    @Autowired
    private OrderItemsRepository orderItemsRepository;
    @Autowired
    private StoreItemsService storeItemsService;
    @Autowired
    private MembersService membersService;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private ProductsViewRepository productsViewRepository;
    @Autowired
    private PaymentsService paymentsService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private NativeServicesService nativeServicesService;

    private final Logger log = LoggerFactory.getLogger(OrdersService.class);

    @Deprecated
    public String importKitsOrders() {

        List<Order> toSave = new LinkedList<>();
        List<OrderDetailOld> details = orderDetailsRepository.findAll();
        for (OrderDetailOld detail : details) {

            List<Order> exists = ordersRepository.findByCreationDateAndPayerEmail(detail.getDateorder(), detail.getPayerEmail());
            if (!exists.isEmpty()) {
                continue;
            }

            StoreItem item = storeItemsRepository.findByName(detail.getItemName());
            if (item == null || item.getCategory() != 1) {
                continue;
            }

            OrderHeader header = orderHeaderRepository.findOne(detail.getId());

            Order order = new Order();
            order.setType(OrderType.kit.name());
            order.setCreationDate(detail.getDateorder());
            order.setPayerEmail(detail.getPayerEmail());
            order.setFirstName(detail.getFirstName());
            order.setLastName(detail.getLastName());
            order.setPaymentAmount(Double.valueOf(detail.getPaymentAmount()));
            order.setPaymentCurrency(detail.getPaymentCurrency());
            order.setPaymentStatus(detail.getPaymentStatus());
            order.setPaymentType(detail.getPaymentType());
            order.setItemId(item.getId());

            if (header == null) {

                order.setStatus(OrderStatus.review.name());
                order.setPaymentStatus(OrderPaymentStatus.unpaid.name());

                order.setComment("Orden sin header");
            } else {

                order.setStatus(OrderStatus.unpaid.name());
                order.setPaymentStatus(OrderPaymentStatus.unpaid.name());
                order.setTypeStatus(OrderPaymentStatus.unpaid.name());

                if (header.getPaymentStatus().equals("Completed") && detail.getPaymentStatus().equals("Completed")) {
                    order.setActive(true);
                    order.setActivationDate(detail.getDateorder());

                    order.setStatus(OrderStatus.paid.name());
                    order.setTypeStatus(OrderPaymentStatus.paid.name());
                    order.setPaymentStatus(OrderPaymentStatus.paid.name());

                    order.setComment("Registro de pago " + detail.getDateorder());
                } else {

                    order.setActive(false);
                    order.setStatus(OrderStatus.review.name());
                    order.setTypeStatus(OrderPaymentStatus.unpaid.name());
                    order.setPaymentStatus(OrderPaymentStatus.unpaid.name());

                    order.setComment("Uno de los dos incompleto " + detail.getPaymentStatus() + " " + header.getPaymentStatus());
                    if (!header.getPaymentStatus().equals("Completed") && !detail.getPaymentStatus().equals("Completed")) {

                        order.setStatus(OrderStatus.unpaid.name());
                        order.setTypeStatus(OrderPaymentStatus.unpaid.name());
                        order.setPaymentStatus(OrderPaymentStatus.unpaid.name());

                        order.setComment("Sin pago registrado");
                    }
                }
            }

            //set customer
            List<Customer> customers = customersRepository.findByPayerEmail(detail.getPayerEmail());
            if (!customers.isEmpty()) {
                String storeId = customers.get(0).getStoreOwner();
                order.setStoreId(storeId);

                if (customers.size() > 1) {
                    log.error("Mas de un cliente para " + detail.getPayerEmail());
                }
            } else {
                order.setStoreId("SIN STORE ID");
            }

            toSave.add(order);
        }

        save(toSave);

        return "Ordernes importadas " + toSave.size();
    }

    @Deprecated
    public String importProductOrders() {

        int saved = 0;
        List<OrderDetailOld> details = orderDetailsRepository.findByItemName("w5");
        details.addAll(orderDetailsRepository.findByItemName("w2"));

        for (OrderDetailOld detail : details) {

            OrderHeader header = orderHeaderRepository.findOne(detail.getId());

            List<Order> exists = ordersRepository.findByCreationDateAndPayerEmail(detail.getDateorder(), detail.getPayerEmail());
            if (!exists.isEmpty()) {
                continue;
            }

            StoreItem item = storeItemsRepository.findByName(detail.getItemName());
            if (item == null || item.getCategory() != 2) {
                continue;
            }

            Order o = new Order();
            o.setActivationDate(null);
            o.setCreationDate(detail.getDateorder());
            o.setActivationDate(null);
            o.setFirstName(detail.getFirstName());
            o.setItemId(item.getId());

            o.setLastName(detail.getLastName());
            o.setPayerEmail(detail.getPayerEmail());
            o.setPaymentAmount(Double.valueOf(detail.getPaymentAmount()));
            o.setPaymentCurrency(detail.getPaymentCurrency());

            o.setStatus(OrderStatus.unpaid.name());
            o.setTypeStatus(OrderStatus.unpaid.name());
            o.setPaymentStatus(OrderPaymentStatus.unpaid.name());

            if (detail.getPaymentStatus().equals("Completed") && header.getPaymentStatus().equals("Completed")) {
                o.setActive(true);
                o.setActivationDate(detail.getDateorder());

                o.setStatus(OrderStatus.paid.name());
                o.setTypeStatus(OrderStatus.paid.name());
                o.setPaymentStatus(OrderPaymentStatus.paid.name());

            } else if (detail.getPaymentStatus().equals("Completed") && !header.getPaymentStatus().equals("Completed")) {
                o.setActive(false);

                o.setStatus(OrderStatus.review.name());
                o.setTypeStatus(OrderStatus.review.name());
                o.setPaymentStatus(OrderPaymentStatus.unpaid.name());

            }

            o.setPaymentType(detail.getPaymentType());
            o.setType(OrderType.product.name());

            List<Customer> customers = customersRepository.findByPayerEmail(detail.getPayerEmail());
            if (customers.isEmpty()) {
                o.setComment("No se encontro cliente para el pedido");
            } else if (customers.size() > 1) {
                o.setComment("Mas de un cliente encontrado para ese email");
            } else {
                o.setStoreId(customers.get(0).getStoreOwner());
            }

            save(o);
            saved++;

        }

        return "Pedidos importados " + saved;
    }

    /**
     * //TODO: revisar este metodo, y mejorar la forma en que se importan los
     * servicios de lineas para españa
     *
     * @param year
     * @param month
     * @return
     */
    public String importServiceOrders(Integer year, Integer month) {

        Map<String, Member> emailCache = new HashMap<>();
        Map<String, Member> dniCache = new HashMap<>();
        Map<String, StoreItem> storeItemCache = new HashMap<>();

        List<Member> members = membersRepository.findAll();
        members.forEach((member) -> {
            emailCache.put(member.getEmail(), member);
            if (member.getDni() != null && !member.getDni().isEmpty()) {
                dniCache.put(member.getDni(), member);
            }
        });

        List<StoreItem> items = storeItemsRepository.findAll();
        items.stream().filter((item) -> (item.getCategory() == 3)).forEachOrdered((item) -> {
            storeItemCache.put(item.getCode(), item);
        });

        log.error("Importing service orders for " + year + "/" + month);
        int inserted = 0;
        int updated = 0;
        List<Order> orders = gmGsmLinesService.getServiceOrders(year, month);

        log.error("Got " + orders.size() + " service orders");
        int cant = 0;

        List<String> types = new ArrayList<>();
        types.add(OrderType.service.name());

        List<Order> toSave = new ArrayList<>();
        for (Order order : orders) {

            log.error("parsing " + cant++);
            Order alreadyInserted = ordersRepository.findFirstByExternalIdAndTypeIn(order.getExternalId(), types);
            if (alreadyInserted != null) {

                alreadyInserted.setStoreId(order.getStoreId());
                alreadyInserted.setCreationDate(order.getCreationDate());
                alreadyInserted.setTypeStatus(order.getTypeStatus());

                if (order.getStatus().equals("active")) {
                    alreadyInserted.setActive(true);
                    alreadyInserted.setActivationDate(order.getActivationDate());
                }

                toSave.add(alreadyInserted);
                updated++;

            } else {

                String email = order.getPayerEmail();
                Member m = emailCache.get(email);
                if (m != null) {
                    order.setMemberId(m.getId());
                }

                if (order.getMemberId() == null) {
                    Member mem = dniCache.get(order.getComment());
                    if (mem != null) {
                        order.setMemberId(mem.getId());
                    }
                }

                //TODO: si el dni coincide setear el member id a la orden 
                //si no, es de cliente setear el storeId - username
                order.setStoreTitle(order.getOrderItem().getName());
                order.setActive(order.getStatus().equals("active"));
                StoreItem item = storeItemCache.get(order.getOrderItem().getCode());

                if (item == null) {
                    log.error("Store item not found for name " + order.getOrderItem().getCode());
                    continue;
                }

                order.setItemId(item.getId());
                order.setOrderItem(null);
                order.setShippingStatus("created");
                order.setSource("external");
                order.setPaymentCurrency("EUR");
                order.setPaymentType("tefpay");

                toSave.add(order);
                inserted++;
            }
        }

        save(toSave);
        log.error("Done!");
        return "Insertadas: " + inserted + " - Actualizadas:" + updated;
    }

    /**
     * Returns cuotas per order, create new ones if they don't exists or are
     * deleted
     *
     * @param member
     * @return
     */
    public List<Order> getCuotasOrder(Member member) {
        List<Order> ret = getOrders(member);
        return ret;
    }

    public List<Order> getCuotasOrder(Long memberId) {
        Member member = membersRepository.findOne(memberId);
        List<Order> orders = getOrders(member);
        return orders;
    }

    private List<Order> getOrders(Member member) {
        List<Order> ret = new LinkedList<>();
        List<Order> orders = ordersRepository.findByPayerEmailAndDeleted(member.getEmail(), Boolean.FALSE);
        for (Order order : orders) {

            //es un item con cuotas
            if (order.getOrderItem().isInstallments()) {

                //nunca deberia pasar, pero pasa
                if (order.getInstallmentNumber() == null) {
                    order.setInstallmentNumber(1);
                    ordersRepository.save(order);
                }

                if (order.getInstallmentNumber() == 1) {

                    List<Order> cuotas = ordersRepository.findByPayerEmailAndExternalIdAndDeleted(order.getPayerEmail(), order.getId(), false);

                    if (cuotas.isEmpty()) {

                        for (int i = 1; i < order.getOrderItem().getInstallmentsQty(); i++) {
                            Order newOne = new Order();
                            newOne.setActivationDate(null);
                            newOne.setActive(false);
                            newOne.setBillingAddress(order.getBillingAddress());
                            newOne.setBillingCity(order.getBillingCity());
                            newOne.setBillingCompany(order.getBillingCompany());
                            newOne.setBillingCountry(order.getBillingCountry());
                            newOne.setBillingName(order.getBillingName());
                            newOne.setBillingPostalCode(order.getBillingPostalCode());
                            newOne.setBillingState(order.getBillingState());
                            newOne.setComment("Auto created order - cuotas");
                            newOne.setCreationDate(new Date());
                            newOne.setDeleted(false);
                            newOne.setDni(order.getDni());
                            newOne.setExternalId(order.getId());
                            newOne.setFirstName(order.getFirstName());
                            newOne.setItemId(order.getItemId());
                            newOne.setLastName(order.getLastName());
                            newOne.setMemberId(order.getMemberId());
                            newOne.setMessage("Cuota " + (i + 1));
                            newOne.setModificationDate(null);
                            newOne.setMultiple(false);
                            newOne.setOrderItem(order.getOrderItem());
                            newOne.setPayerEmail(order.getPayerEmail());
                            newOne.setPaymentCurrency(order.getPaymentCurrency());
                            newOne.setPaymentStatus(OrderStatus.pending.name());
                            newOne.setPaymentType("bacs");
                            newOne.setInstallmentNumber(i + 1);

                            //en el pago descontamos el envio
                            //Locale locale = CountryUtils.getLocaleForCountry(order.getBillingCountry());
                            //Double price = NumberUtils.convertMoneyFromEuros(order.getOrderItem().getPrice(), locale);
                            //en el pago descontamos el envio
                            Locale locale = CountryUtils.getLocaleForCountry(order.getBillingCountry());
                            Double localePrice = order.getOrderItem().getPrice();
                            StoreItemI18n i18n = order.getOrderItem().getI18nForCountry(order.getBillingCountry());
                            if (i18n != null && i18n.getPrice() != null) {
                                localePrice = i18n.getPrice();
                            }
                            Double price = NumberUtils.convertMoneyFromEuros(localePrice, locale);

                            newOne.setPaymentAmount(price);

                            newOne.setPrizeText(null);
                            newOne.setShippingStatus("created");
                            newOne.setSource("system");
                            newOne.setStatus(OrderStatus.pending.name());
                            newOne.setStoreId(order.getStoreId());
                            newOne.setStoreTitle(order.getStoreTitle());
                            newOne.setType(order.getOrderItem().getCategoryName());
                            newOne.setTypeStatus(OrderStatus.pending.name());
                            newOne.setWithPrize(false);

                            save(newOne);

                            ret.add(newOne);
                        }
                    } else {
                        ret.add(order);
                        ret.addAll(cuotas);
                    }
                }
            }
        }
        return ret;
    }

    /**
     * return available upgrades for member
     *
     * @param member
     * @return
     */
    public List<StoreItem> getAvailaveUpgrades(Member member) {

        //primero verifico si ya tengo upgrade
        List<Order> orders = getMemberOrders(member);
        for (Order order : orders) {
            if (order.getType().equals(OrderType.upgrade.name())) {
                if (order.getStatus().equals(OrderStatus.paid.name())) {
                    member.setOrder(order);
                }
            }
        }

        Long id = member.getOrder().getOrderItem().getId();
        String sid = String.valueOf(id);
        if (id < 10) {
            sid = "0" + sid;
        }
        String storeItemId = "%" + sid + "%";
        List<StoreItem> items = storeItemsRepository.findByAvailableToLikeAndEnabled(storeItemId, true);

        //check for updates of updates
        List<Order> upgrades = ordersRepository.findByTypeAndMemberId(OrderType.upgrade.name(), member.getId());
        List<Long> ids = new ArrayList<>();
        for (Order upgrade : upgrades) {
            if (upgrade.getActive()) {
                ids.add(upgrade.getOrderItem().getFinalItemId());
            }
        }

        for (Long idUpgrade : ids) {
            if (idUpgrade == null) {
                continue;
            }

            sid = String.valueOf(idUpgrade);
            if (idUpgrade < 10) {
                sid = "0" + sid;
            }
            storeItemId = "%" + sid + "%";
            items.addAll(storeItemsRepository.findByAvailableToLikeAndEnabled(storeItemId, true));
        }

        return items;
    }

    /**
     * Add Order to member with particular Upgrade
     *
     * @param member
     * @param upgradeId
     * @param modUser
     */
    public void addUpgrade(Member member, String upgradeId, String modUser) {

        StoreItem storeItem = storeItemsRepository.findOne(Long.valueOf(upgradeId));
        Order order = member.getOrder();
        Order newOrder = new Order();

        newOrder.setActivationDate(null);
        newOrder.setActive(false);
        newOrder.setBillingAddress(order.getBillingAddress());
        newOrder.setBillingCity(order.getBillingCity());
        newOrder.setBillingCompany(order.getBillingCompany());
        newOrder.setBillingCountry(order.getBillingCountry());
        newOrder.setBillingName(order.getBillingName());
        newOrder.setBillingPostalCode(order.getBillingPostalCode());
        newOrder.setBillingState(order.getBillingState());
        newOrder.setComment(null);
        newOrder.setCreationDate(new Date());
        newOrder.setDeleted(false);
        newOrder.setDni(order.getDni());
        newOrder.setFirstName(order.getFirstName());
        newOrder.setItemId(Long.valueOf(upgradeId));
        newOrder.setStoreTitle(storeItem.getName());
        newOrder.setLastName(order.getLastName());
        newOrder.setMemberId(member.getId());
        newOrder.setMessage("Manually added upgrade  - " + modUser);
        newOrder.setModificationDate(null);

        if (storeItem.isInstallments()) {
            newOrder.setInstallmentNumber(1);
        }
        newOrder.setMultiple(false);
        newOrder.setPayerEmail(order.getPayerEmail());
        newOrder.setPaymentCurrency(CountryUtils.getCurrencyForCountry(member.getCountry()));

        newOrder.setPaymentType("bacs");

        String storeTitle = storeItem.getName();
        newOrder.setStoreTitle(storeTitle);

        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        Double price = storeItem.getPrice();
        Double realPrice = NumberUtils.convertMoneyFromEuros(price, locale);
        newOrder.setPaymentAmount(realPrice);

        newOrder.setStoreId(order.getStoreId());
        newOrder.setPrizeText(null);
        newOrder.setShippingStatus(ShipmentStatus.created.name());
        newOrder.setSource("local");
        newOrder.setType(OrderType.upgrade.name());

        newOrder.setPaymentStatus(OrderStatus.pending.name());
        newOrder.setStatus(OrderStatus.pending.name());
        newOrder.setTypeStatus(OrderStatus.pending.name());

        newOrder.setWithPrize(false);

        save(newOrder);

    }

    /**
     * get all members orders not deleted and kit, upgrade or store
     *
     * @param m
     * @return
     */
    public List<Order> getMemberOrders(Member m) {
        List<String> types = Arrays.asList(OrderType.kit.name(), OrderType.upgrade.name(), OrderType.store.name());
        return ordersRepository.findByPayerEmailAndDeletedAndTypeIn(m.getEmail(), false, types);
    }

    /**
     * get all members orders not deleted and product
     *
     * @param m
     * @return
     */
    public List<Order> getMemberProductOrders(Member m) {
        List<String> types = Arrays.asList(OrderType.product.name(), OrderType.topup.name(), OrderType.ticket.name());
        return ordersRepository.findByPayerEmailAndDeletedAndTypeInOrderByIdDesc(m.getEmail(), false, types);
    }

    /**
     * get all members orders not deleted and kit, upgrade or store
     *
     * @param m
     * @return
     */
    public List<Order> getMemberPayedOrders(Member m) {
        List<String> types = Arrays.asList(OrderType.kit.name(), OrderType.upgrade.name(), OrderType.store.name());
        List<Order> orders = ordersRepository.findByPayerEmailAndDeletedAndTypeIn(m.getEmail(), false, types);

        /*for (Order order : orders) {
            
            if(order.)
            
        }*/
        return orders;
    }

    /**
     *
     * returns final afiliation order
     *
     * @param memberId
     * @return
     */
    public Order getMemberFinalOrder(Long memberId) {

        return null;
    }

    /**
     * get member upgrades, non deleted
     *
     * @param member
     * @return
     */
    public List<Order> getMemberUpgrades(Member member) {
        List<String> types = Arrays.asList(OrderType.upgrade.name());
        List<Order> upgrades = ordersRepository.findByPayerEmailAndDeletedAndTypeIn(member.getEmail(), Boolean.FALSE, types);
        return upgrades;
    }

    public List<Order> getMemberUpgrades(Long memberId) {
        List<String> types = Arrays.asList(OrderType.upgrade.name());
        Member member = membersRepository.findOne(memberId);
        List<Order> upgrades = ordersRepository.findByPayerEmailAndDeletedAndTypeIn(member.getEmail(), Boolean.FALSE, types);
        return upgrades;
    }

    /**
     * getOne order
     *
     * @param orderId
     * @return
     */
    public Order getOrder(Long orderId) {
        return ordersRepository.findOne(orderId);
    }

    /**
     *
     * @param ids
     * @return
     */
    public List<Order> getOrders(List<Long> ids) {
        return ordersRepository.findAll(ids);
    }

    /**
     *
     * @return
     */
    public List<Order> fixOrders() {

        List<Order> ret = new LinkedList<>();
        //memberid,orderId
        Map<Long, Long> memberOrders = new HashMap<>();

        List<Member> members = membersRepository.findByCountry("CO");
        members.addAll(membersRepository.findByCountry("PE"));
        members.addAll(membersRepository.findByCountry("EC"));

        for (Member member : members) {

            Order last = getLastOrder(member);
            if (last != null) {
                memberOrders.put(member.getId(), last.getId());
            }
        }

        //deliverable orders
        //TODO: this should be once store is set up
        memberOrders.entrySet().stream().map((entry) -> entry.getValue()).forEachOrdered((orderId) -> {
            ret.add(prepareOrderForShipment(orderId));
        });

        return ret;
    }

    /**
     *
     * method to create order items, fix wrong ones
     *
     * @param orderId
     * @return order
     *
     */
    public Order prepareOrderForShipment(Long orderId) {

        //TODO: esto se deberia hacer al momento de creacion de la orden
        //para rellenar los atributos seleccionados por el cliente
        Order order = ordersRepository.findOne(orderId);

        //already fixed
        if (order.getShippingStatus().equals(OrderShipmentStatus.prepared.name())) {
            return order;
        }

        //borro las antiguas
        boolean delete = true;
        List<OrderItem> orderItems = orderItemsRepository.findByOrderId(orderId);
        /*for (OrderItem orderItem : orderItems) {
            if (orderItem.isShipped()) {
                delete = false;
                break;
            }
        }*/

        if (delete) {
            orderItemsRepository.delete(orderItems);
        }
        orderItems.clear();

        //if it's upgrade, put the final item 
        StoreItem storeItem = order.getOrderItem();
        if (storeItem.getCategoryName().equals("upgrade")) {
            storeItem = storeItem.getFinalItem();
        }

        //get children
        List<StoreItemChild> children = storeItem.getChildren();

        //for each child add qty as order item
        for (StoreItemChild child : children) {

            //get store item options for child
            List<StoreItemOption> options = storeItemsService.getStoreItemOptions(child.getChildId());
            Long storeItemOptionId = null;
            StoreItemOption selectedOption = null;

            //TODO: set this from post form on store
            //each in qty has own different item options
            for (StoreItemOption option : options) {
                if (option.isDefaultValue()) {
                    storeItemOptionId = option.getId();
                    selectedOption = option;
                }
            }

            //create order items            
            int qty = child.getQuantity();
            for (int i = 0; i < qty; i++) {
                OrderItem newItem = new OrderItem();
                newItem.setName(child.getChild().getName());
                newItem.setOrderId(orderId);
                newItem.setShipped(false);
                newItem.setStoreItemOptionId(storeItemOptionId);
                newItem.setStoreItemEan(selectedOption != null ? selectedOption.getEanCode() : null);
                orderItems.add(newItem);
            }
            orderItemsRepository.save(orderItems);
        }
        order.setShippingStatus(OrderShipmentStatus.prepared.name());
        save(order);

        return order;
    }

    /**
     * Return final order for member
     *
     * @param member
     * @return order
     */
    public Order getLastOrder(Member member) {

        //si es null continuo
        if (member.getOrder() == null) {
            return null;
        }
        //si esta eliminado, continuo
        if (member.getDeleted()) {
            return null;
        }

        //si es free continuo
        if (member.getOrder().getItemId() == 15) {
            return null;
        }

        //si tiene orden eliminada
        if (member.getOrder().isDeleted()) {
            return null;
        }

        //si tiene el kit de afiliacion pagado
        if (member.getOrder().getStatus().equals(OrderStatus.paid.name())) {

            //si debe alguna cuota, lo salto
            boolean todoPago = true;
            List<Order> cuotas = getCuotasOrder(member);
            for (Order cuota : cuotas) {
                if (!cuota.getStatus().equals(OrderStatus.paid.name())) {
                    todoPago = false;
                    break;
                }
            }

            if (!todoPago) {
                return null;
            }

            //get member upgrades, 
            List<Order> upgrades = getMemberUpgrades(member);
            for (Order upgrade : upgrades) {
                if (!upgrade.getStatus().equals(OrderStatus.paid.name())) {
                    todoPago = false;
                    break;
                }
            }

            //si tiene algun upgrade, y no esta pago
            if (!todoPago) {
                return null;
            }

            //si tiene mas de un upgrade pago, busco el upgrade ultimo
            if (upgrades.size() > 1) {
                Collections.sort(upgrades, (Order o1, Order o2) -> o1.getActivationDate().compareTo(o2.getActivationDate()));
            }

            //pongo el kit de afiliacion si tiene todo pago
            if (!upgrades.isEmpty()) {
                return upgrades.get(0);
            } else {
                return member.getOrder();
            }
        } else {
            return null;
        }
    }

    /**
     * if order has installments, return installment 2,3... etc with status PAID
     *
     * @param order
     * @return
     */
    public List<Order> getPayedInstallments(Order order) {
        return ordersRepository.findByPayerEmailAndExternalIdAndStatusAndDeleted(order.getPayerEmail(), order.getId(), OrderStatus.paid.name(), Boolean.FALSE);
    }

    /**
     * Add Order to member with particular product
     *
     * @param member
     * @param storeItemId
     * @param modUser
     * @return
     */
    public Order addProduct(Member member, Long storeItemId, String modUser) {

        StoreItem storeItem = storeItemsRepository.findOne(storeItemId);
        Order order = member.getOrder();
        Order newOrder = new Order();

        //from member profile, if not, create from las member order
        String firstName = member.getFirstName() != null ? member.getFirstName() : order.getFirstName();
        String lastName = member.getLastName() != null ? member.getLastName() : order.getLastName();
        String address = member.getStreet() != null ? member.getStreet() : order.getBillingAddress();
        String city = member.getCity() != null ? member.getCity() : order.getBillingCity();
        String state = member.getState() != null ? member.getState() : order.getBillingState();
        String country = member.getCountry() != null ? member.getCountry() : order.getBillingCountry();
        String zip = member.getPostal() != null ? member.getPostal() : order.getBillingPostalCode();

        newOrder.setBillingAddress(address);
        newOrder.setBillingCity(city);
        newOrder.setBillingCountry(country);
        newOrder.setBillingName(firstName + " " + lastName);
        newOrder.setBillingPostalCode(zip);
        newOrder.setBillingState(state);
        newOrder.setFirstName(firstName);
        newOrder.setLastName(lastName);

        String storeTitle = storeItem.getName();
        newOrder.setStoreTitle(storeTitle);
        newOrder.setActivationDate(null);
        newOrder.setActive(false);
        newOrder.setBillingCompany(order.getBillingCompany());
        newOrder.setComment(null);
        newOrder.setCreationDate(new Date());
        newOrder.setDeleted(false);
        newOrder.setDni(order.getDni());
        newOrder.setItemId(storeItemId);
        newOrder.setMemberId(member.getId());
        newOrder.setMessage("Backoffice purchase  - " + modUser);
        newOrder.setModificationDate(null);
        newOrder.setMultiple(false);
        newOrder.setPayerEmail(order.getPayerEmail());
        newOrder.setPaymentCurrency(CountryUtils.getCurrencyForCountry(member.getCountry()));

        newOrder.setPaymentType("bacs");

        try {
            Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
            StoreItemI18n i18n = storeItem.getI18nForCountry(member.getCountry());
            Double price = i18n.getPrice();
            Double realPrice = NumberUtils.convertMoneyFromEuros(price, locale);
            newOrder.setPaymentAmount(realPrice);
        } catch (Exception e) {
            newOrder.setPaymentAmount(storeItem.getPrice());
        }

        if (storeItem.isInstallments()) {
            newOrder.setInstallmentNumber(1);
        }
        newOrder.setStoreId(member.getUsername());
        newOrder.setPrizeText(null);
        newOrder.setShippingStatus(ShipmentStatus.created.name());
        newOrder.setSource("local");
        newOrder.setType(storeItem.getCategoryName());

        newOrder.setPaymentStatus(OrderStatus.pending.name());
        newOrder.setStatus(OrderStatus.pending.name());
        newOrder.setTypeStatus(OrderStatus.pending.name());

        newOrder.setWithPrize(false);

        save(newOrder);

        return newOrder;
    }

    /**
     * Creates an order using TOPUP, if activation date is not null, the order
     * is paid and typeStatus is
     *
     * @param member
     * @param storeItemId
     * @param modUser
     * @param amount
     * @param number
     * @param activation
     * @return
     */
    public Order addTopUp(Member member, Long storeItemId, String modUser, Double amount, Long number, Date activation) {

        StoreItem storeItem = storeItemsRepository.findOne(storeItemId);
        String storeTitle = storeItem.getName();

        Double realPrice;
        String currency = CountryUtils.getCurrencyForCountry(member.getCountry());
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        if (storeItem.getPrice() == 0) {
            //TOP UP
            storeTitle += " " + number + " " + currency + ":" + amount;
            realPrice = amount;
        } else {
            //Pack
            StoreItemI18n i18n = storeItem.getI18nForCountry(member.getCountry());
            Double price = i18n.getPrice();
            realPrice = NumberUtils.convertMoneyFromEuros(price, locale);
        }

        Order order = member.getOrder();
        Order newOrder = new Order();

        //from member profile, if not, create from las member order
        String firstName = member.getFirstName() != null ? member.getFirstName() : order.getFirstName();
        String lastName = member.getLastName() != null ? member.getLastName() : order.getLastName();
        String address = member.getStreet() != null ? member.getStreet() : order.getBillingAddress();
        String city = member.getCity() != null ? member.getCity() : order.getBillingCity();
        String state = member.getState() != null ? member.getState() : order.getBillingState();
        String country = member.getCountry() != null ? member.getCountry() : order.getBillingCountry();
        String zip = member.getPostal() != null ? member.getPostal() : order.getBillingPostalCode();

        newOrder.setBillingAddress(address);
        newOrder.setBillingCity(city);
        newOrder.setBillingCountry(country);
        newOrder.setBillingName(firstName + " " + lastName);
        newOrder.setBillingPostalCode(zip);
        newOrder.setBillingState(state);
        newOrder.setFirstName(firstName);
        newOrder.setLastName(lastName);

        newOrder.setActive(false);
        newOrder.setActivationDate(activation);
        newOrder.setPaymentStatus(OrderStatus.pending.name());
        newOrder.setStatus(OrderStatus.pending.name());
        newOrder.setTypeStatus(OrderStatus.pending.name());

        //TODO: verificr si se usa en otro lado, si no dejar siempre como pagado
        if (activation != null) {
            newOrder.setActive(true);
            newOrder.setPaymentStatus(OrderStatus.paid.name());
            newOrder.setStatus(OrderStatus.paid.name());
            //COMPLETE PARA EVITAR EL REDEEM
            newOrder.setTypeStatus(OrderStatus.complete.name());
        }

        newOrder.setStoreTitle(storeTitle);
        newOrder.setBillingCompany(order.getBillingCompany());
        newOrder.setComment(null);
        newOrder.setCreationDate(new Date());
        newOrder.setDeleted(false);
        newOrder.setDni(order.getDni());
        newOrder.setItemId(storeItemId);
        newOrder.setMemberId(member.getId());
        newOrder.setMessage("Backoffice purchase username:" + modUser);
        newOrder.setModificationDate(null);
        newOrder.setMultiple(false);
        newOrder.setPayerEmail(order.getPayerEmail());
        newOrder.setPaymentCurrency(CountryUtils.getCurrencyForCountry(member.getCountry()));

        newOrder.setPaymentType("coms");
        newOrder.setPaymentAmount(realPrice);
        newOrder.setPaymentCurrency(currency);

        newOrder.setStoreId(member.getUsername());
        newOrder.setPrizeText(null);
        newOrder.setWithPrize(false);
        newOrder.setShippingStatus(ShipmentStatus.disabled.name());
        newOrder.setSource("local");
        newOrder.setType(OrderType.topup.name());

        save(newOrder);

        return newOrder;
    }

    public List<Order> getOrdersPaidByCountryAndMonthAndYear(String country, Integer month, Integer year) {
        Date from = DateUtils.getStartDateForPeriod(month, year);
        Date to = DateUtils.getEndDateForPeriod(month, year);
        List<Order> ret = ordersRepository.findAll(OrdersSpecs.searchByCountryAndActivationDate(country, from, to));
        return ret;
    }

    public List<OrderDto> getOrdersDtoPaidByCountryFromAndTo(String country, Date from, Date to) {
        final List<OrderDto> orders = orderDao.find(country, from, to);
        return orders;
    }

    public List<OrderDto> getOrdersDtoPaidByCountryMonthAndYear(String country, Integer month, Integer year) {
        final Date from = DateUtils.getStartDateForPeriod(month, year);
        final Date to = DateUtils.getEndDateForPeriod(month, year);
        return getOrdersDtoPaidByCountryFromAndTo(country, from, to);
    }

    public List<ProductsView> getProductsViewPaidByCountryFromAndTo(String country, Date from, Date to) {
        final List<ProductsView> ordersFinal = productsViewRepository.findByBillingCountryAndPeriod(country, from, to);
        return ordersFinal;
    }

    public List<ProductsView> getProductsViewPaidByCountryMonthAndYear(String country, List<String> months, List<String> years) {
//        final Date from = DateUtils.getStartDateForPeriod(month, year);
//        final Date to = DateUtils.getEndDateForPeriod(month, year);
        final List<ProductsView> orders = productsViewRepository.findByBillingCountryAndMonth(country, months, years);
        return orders;
    }

    /**
     * Returns order for member , kit and store, payed and id not in
     *
     * @param memberId
     * @param ids
     * @return
     */
    public List<Order> getMemberOrdersPayedNotIn(Long memberId, List<Long> ids) {
        List<Order> ret = ordersRepository.findByMemberIdAndStatusAndTypeInAndIdNotIn(memberId, "paid", Arrays.asList("kit", "store"), ids);
        return ret;
    }

    /**
     * Returns orders for date and status
     *
     * @param status
     * @param date
     * @return
     */
    public List<OrderDto> getOrdersStatusAtDate(List<String> status, Date date) {
        List<Order> ret = ordersRepository.findByActivationDateAndStatusIn(date, status);
        List<OrderDto> retDto = new ArrayList<>();
        ret.forEach((order) -> {
            retDto.add(new OrderDto(order));
        });
        return retDto;
    }

    /**
     * Mark order as deleted
     *
     * @param o
     */
    public void deleteOrder(Order o) {
        o.setActive(false);
        o.setDeleted(true);
        o.setStatus(OrderStatus.pending.name());
        //TODO: unificar este método para borrar ordenes
        o.setComment("Order delete by system " + new Date() + " - " + o.getComment());
        save(o);
    }

    /**
     *
     * @param orders
     */
    public void save(List<Order> orders) {
        ordersRepository.save(orders);
    }

    /**
     *
     * @param order
     */
    public void save(Order order) {
        ordersRepository.save(order);
    }

    public void markAsInstallmentComplete() {
        log.error("Start mark installment complete");
        List<Order> orders = ordersRepository.findByDeletedAndInstallmentNumberAndInstallmentComplete(Boolean.FALSE, 1, Boolean.FALSE);
        for (Order order : orders) {

            if (order.getItemId() == 15) {
                continue;
            }

            List<Order> installments = ordersRepository.findByPayerEmailAndExternalIdAndDeleted(order.getPayerEmail(), order.getId(), false);
            boolean completed = false;
            for (Order installment : installments) {

                if (!installment.getItemId().equals(order.getItemId())) {
                    continue;
                }

                completed = installment.getStatus().equals(OrderStatus.paid.name());
                if (!completed) {
                    break;
                }
            }
            order.setInstallmentComplete(completed);
        }
        save(orders);
        log.error("Done mark installment complete");
    }

    /**
     *
     * @param cart
     * @return
     */
    public CartDtoResponse processCart(CartDto cart) {

        CartDtoResponse ret = new CartDtoResponse();

        try {
            PersonalInfoDto personalInfoDto = cart.getPersonalInfo();
            //Personal info must not be null
            if (personalInfoDto == null) {
                ret.setStatus("error");
                ret.setMessage("empty_info");
                return ret;
            }

            //cart is empty, should not be here
            if (cart.getItems() == null || cart.getItems().isEmpty()) {
                ret.setStatus("error");
                ret.setMessage("empty_cart");
                return ret;
            }

            //cart 
            if (cart.getCountry() == null) {
                ret.setStatus("error");
                ret.setMessage("empty_cart");
                return ret;
            }

            //country code is mandatory
            if (cart.getCountry().getCode() == null || cart.getCountry().getCode().isEmpty()) {
                ret.setStatus("error");
                ret.setMessage("error_country_code");
                return ret;
            }

            //currency is mandatory
            if (cart.getCountry().getCurrency() == null || cart.getCountry().getCurrency().isEmpty()) {
                ret.setStatus("error");
                ret.setMessage("error_currency");
                return ret;
            }

            //payment amount is mandatory
            if (cart.getTotal() == null) {
                ret.setStatus("error");
                ret.setMessage("error_amount");
                return ret;
            }

            //si ya es afiliado, lleno al personal info con sus datos
            if (personalInfoDto.isAlreadyAfiliate()) {
                Member member = membersRepository.findByUsername(personalInfoDto.getUsername());
                if (member == null) {
                    ret.setStatus("error");
                    ret.setMessage("empty_info");
                    return ret;
                }
                parsePersonalInfoDtoFromMember(member, personalInfoDto);
            }

            //get Category
            String categoryName = cart.getItems().get(0).getCategoryName();

            //Sponsor is valid ?
            List<String> needSponsor = Arrays.asList("kit", "store", "product");
            //only for some categories
            if (needSponsor.contains(categoryName)) {
                Member sponsor = membersRepository.findByUsername(cart.getPersonalInfo().getSponsor());
                if (sponsor == null) {
                    ret.setStatus("error");
                    ret.setMessage("member_wrong");
                    return ret;
                }
            }

            //si es topup el telefono tiene que venir obligatorio
            if (categoryName.equals("topup")
                    && (cart.getPersonalInfo().getPhoneNumber() == null || cart.getPersonalInfo().getPhoneNumber().trim().isEmpty())) {
                ret.setStatus("error");
                ret.setMessage("empty_info");
                return ret;
            }

            //TODO: esto se hace adentro del for tambiem, revisar
            StoreItemDto itemDto = cart.getItems().get(0);
            StoreItem storeItem = storeItemsRepository.findOne(itemDto.getId());
            String subCategoryName = storeItem.getSubCategoryName();

            //verificar si es servicios nativos , que el nro este registrado
            if (categoryName.equals("topup") && subCategoryName != null && subCategoryName.equals("voip")) {
                if (!nativeServicesService.validateNumber(cart.getPersonalInfo().getPhoneNumber())) {
                    ret.setStatus("error");
                    ret.setMessage("native_invalid");
                    return ret;
                }
            }

            Order o = new Order();
            o.setCreationDate(cart.getCreationDate());
            o.setFirstName(StringUtils.capitalize(personalInfoDto.getFirstName()));
            o.setLastName(StringUtils.capitalize(personalInfoDto.getLastName()));
            o.setPayerEmail(StringUtils.toLowerCase(personalInfoDto.getEmail()));

            o.setPaymentAmount(cart.getTotal());
            o.setPaymentCurrency(cart.getCountry().getCurrency());
            o.setPaymentType("bacs");
            o.setShippingStatus(ShipmentStatus.initial.name());
            o.setMultiple(false);
            o.setSource("new-store");
            o.setStoreId(cart.getPersonalInfo().getSponsor());

            o.setStatus(OrderStatus.pending.name());
            o.setPaymentStatus(OrderStatus.pending.name());
            o.setMessage("created by new Store");
            o.setPriceForComs(null);
            o.setPhone(cart.getPersonalInfo().getPhoneNumber());

            //TODO: ver si se puede agregar lo de los premios
            o.setPrizeText(null);
            o.setWithPrize(false);

            //billing info
            o.setBillingCountry(cart.getCountry().getCode());

            o.setBillingName(o.getFirstName() + " " + o.getLastName());
            o.setBillingCompany(StringUtils.capitalize(personalInfoDto.getBillingName()));
            o.setBillingAddress(StringUtils.capitalize(personalInfoDto.getBillingAddress()));
            o.setBillingCity(StringUtils.capitalize(personalInfoDto.getBillingCity()));
            o.setBillingState(StringUtils.capitalize(personalInfoDto.getBillingState()));
            o.setBillingPostalCode(personalInfoDto.getBillingPostalCode());

            //Shipping info
            o.setShippingExtra(personalInfoDto.getExtraInformacion());

            if (personalInfoDto.isDifferentShippingAddress()) {

                o.setShippingAddress(personalInfoDto.getShippingAddress());
                o.setShippingCity(personalInfoDto.getShippingCity());
                o.setShippingCountry(personalInfoDto.getShippingCountry());
                o.setShippingName(personalInfoDto.getShippingName());
                o.setShippingPostalCode(personalInfoDto.getShippingPostalCode());
                o.setShippingState(personalInfoDto.getShippingState());

            } else {

                o.setShippingAddress(o.getBillingAddress());
                o.setShippingCity(o.getBillingCity());
                o.setShippingCountry(o.getBillingCountry());
                o.setShippingName(o.getBillingName());
                o.setShippingPostalCode(o.getBillingPostalCode());
                o.setShippingState(o.getBillingState());

            }

            //guardar la order
            save(o);

            //TODO: esto en realidad ahora lo hace para un solo item del carrito, 
            //ver cuando el carrito sea para mas items
            List<OrderItem> orderItems = new ArrayList<>();
            List<StoreItemDto> items = cart.getItems();
            for (StoreItemDto item : items) {

                //si tiene varios , por el momento es uno solo
                //storeItem = storeItemsRepository.findOne(item.getId());
                //TODO: esto lo hace por que es un solo carrito
                o.setItemId(storeItem.getId());
                o.setStoreTitle(storeItem.getName());
                o.setType(storeItem.getCategoryName());
                o.setTypeStatus(OrderStatus.pending.name());
                o.setInstallmentNumber(item.isInstallment() ? 1 : null);

                if (storeItem.getChildren().isEmpty()) {

                    OrderItem detail = new OrderItem();
                    detail.setComplexId(null);
                    detail.setComplexName(null);
                    detail.setSimpleId(storeItem.getId());
                    detail.setSimpleName(storeItem.getName());
                    //TODO: deberia ir el id de la opcion seleccionada, que pasa si es mas de una
                    detail.setItemOptionId(null);
                    detail.setItemOptionSelected(null);
                    //TODO: si tenia opcion, aca iva el id de lo viejo, se puede borrar creo
                    detail.setStoreItemOptionId(null);
                    //TODO: duplicado?
                    detail.setName(storeItem.getName());
                    detail.setOrderId(o.getId());

                    //TODO:
                    //detail.setQuantity(item.getQuantity());
                    detail.setShipped(false);
                    //todo: servía para el codigo de barras del shipping no se si sirve ahora
                    detail.setStoreItemEan(null);
                    detail.setQuantity(1);

                    orderItems.add(detail);

                } else {

                    //lots of details
                    List<StoreItemChild> children = storeItem.getChildren();
                    for (StoreItemChild storeItemChild : children) {

                        OrderItem detail = new OrderItem();
                        detail.setComplexId(storeItem.getId());
                        detail.setComplexName(storeItem.getName());
                        detail.setSimpleId(storeItemChild.getChildId());
                        detail.setSimpleName(storeItemChild.getChild().getName());

                        //TODO: deberia ir el id de la opcion seleccionada, que pasa si es mas de una
                        detail.setItemOptionId(null);
                        detail.setItemOptionSelected(null);
                        //TODO: si tenia opcion, aca iva el id de lo viejo, se puede borrar creo
                        detail.setStoreItemOptionId(null);

                        //TODO: duplicado?
                        detail.setName(storeItemChild.getChild().getName());
                        detail.setOrderId(o.getId());
                        detail.setQuantity(storeItemChild.getQuantity());
                        detail.setShipped(false);
                        //todo: servía para el codigo de barras del shipping no se si sirve ahora
                        detail.setStoreItemEan(null);

                        orderItems.add(detail);

                    }
                }
            }

            //guardar los items, antes hay que guardar la ordern
            orderItemsRepository.save(orderItems);
            //guardar la order again
            save(o);

            //crear member si no existe y si es kit
            //si es kit o store, crea member si es propducto y es cliente queda info en la orden
            membersService.createMember(o);

            //generar código de pago
            String url = paymentsService.createPayment(o.getId());
            if (url == null) {
                ret.setStatus("error");
                ret.setMessage("Payment gateway error");
                return ret;
            }

            ret.setOrderId(o.getId());
            ret.setPaymentToken(url);
            ret.setStatus("success");

            sendOrderEmail(o, url);

        } catch (Exception e) {
            ret.setStatus("error");
            ret.setMessage(e.getMessage() + " " + e.getStackTrace()[0].toString());
        }

        return ret;
    }

    public void sendOrderEmail(Order order, String paymentLink) {
        try {
            Locale locale = CountryUtils.getLocaleForCountry(order.getBillingCountry());
            StoreItem item = storeItemsRepository.findOne(order.getItemId());
            String subject = messageSource.getMessage("email.thanksForYourOrder", null, locale) + " #" + order.getId();
            String body = item.getName() + "<br/>" + item.getShortDescription();
            body = emailService.getOrderEmail(order.getBillingName(), body, locale, paymentLink, order);
            String from = messageSource.getMessage("members.store", null, locale) + " - Wings Mobile<noreply@wingsmobile.com>";
            emailService.sendMail(from, order.getPayerEmail(), subject, body, null);
        } catch (Exception e) {
            log.error("error sending mail ", e);
        }
    }

    public List<OrderDto> getOrdersDtoPending(String country, Integer year) {
        final Date from = DateUtils.getStartDateForPeriod(1, year);
        final Date to = DateUtils.getEndDateForPeriod(12, year);
        return getListDto(country, from, to);
    }

    public List<OrderDto> getOrdersDtoPending(String country, List<Integer> years) {
        if (years.size() == 1) {
            return getOrdersDtoPending(country, years.get(0));
        }

        final Date from = DateUtils.getStartDateForPeriod(1, years.get(0));
        final Date to = DateUtils.getEndDateForPeriod(12, years.get(years.size() - 1));

        return getListDto(country, from, to);
    }

    /**
     *
     * @param country
     * @param from
     * @param to
     * @return
     */
    private List<OrderDto> getListDto(String country, Date from, Date to) {
        final List<Order> findAll = ordersRepository.findAll(OrdersSpecs.searchByCountryAndCreationYearAndPending(country, from, to));
        final List<OrderDto> result = new ArrayList<>(findAll.size());
        findAll.forEach(order -> {
            List<Order> installments = ordersRepository.findByPayerEmailAndExternalIdAndDeleted(order.getPayerEmail(), order.getId(), false);
            installments.stream().filter((installment) -> (installment.getStatus().equals(OrderStatus.pending.name()))).map((installment) -> new OrderDto(installment)).forEachOrdered((orderDto) -> {
                result.add(orderDto);
            });
        });

        return result;
    }

    /**
     *
     * @param items
     * @return
     */
    public List<OrderItem> buildOrderItems(List<StoreItem> items) {
        return null;
    }

    /**
     * para el proceso de pago, si es kit, producto factura y envio si es
     * ticket, asignar tickets, si es upgrade, aplicar upgrade, si es tienda ver
     * el 30/70 si son recargas de gsm, y no es afiliado, por el momento solo
     * afiliado servicios, compra de servicios nativos, o sacarlos de la lista
     *
     */
    public void test1() {

    }

    /**
     * Creates personalInfo from Member object
     *
     * @param m
     * @param info must not be null
     */
    private void parsePersonalInfoDtoFromMember(Member m, PersonalInfoDto info) {

        info.setBillingAddress(m.getStreet());
        info.setBillingCity(m.getCity());
        info.setBillingCountry(m.getCountry());
        info.setBillingName(m.getFirstName() + " " + m.getLastName());
        info.setBillingPostalCode(m.getPostal());
        info.setBillingState(m.getState());
        info.setDifferentShippingAddress(false);
        info.setDni(m.getDni());
        info.setDniType(m.getDniType());
        info.setEmail(m.getEmail());
        info.setFirstName(m.getFirstName());
        info.setLastName(m.getLastName());
        info.setSponsor(m.getUsername());
        info.setTermsChecked(true);

    }

    /**
     *
     * @param active
     * @param email
     * @return
     */
    public List<Order> findByActiveAndPayerEmail(boolean active, String email) {
        return ordersRepository.findByActiveAndPayerEmail(active, email);
    }

    /**
     *
     * @param active
     * @param deleted
     * @return
     */
    public List<Order> findByActiveAndDeleted(boolean active, boolean deleted) {
        return ordersRepository.findByActiveAndDeleted(active, deleted);
    }

    /**
     *
     * @param status
     * @param storeItemIds
     * @param typeStatus
     * @param deleted
     * @return
     */
    public List<Order> findByStatusAndItemIdInAndTypeStatusAndDeleted(String status, List<Long> storeItemIds, String typeStatus, Boolean deleted) {
        return ordersRepository.findByStatusAndItemIdInAndTypeStatusAndDeleted(status, storeItemIds, typeStatus, deleted);
    }

    /**
     *
     * @param active
     * @return
     */
    public List<Order> findByActive(Boolean active) {
        return ordersRepository.findByActive(active);
    }

    /**
     *
     * @param memberId
     * @param itemId
     * @param deleted
     * @param status
     * @return
     */
    public long countByMemberIdAndItemIdAndDeletedAndStatus(Long memberId, Long itemId, Boolean deleted, String status) {
        return ordersRepository.countByMemberIdAndItemIdAndDeletedAndStatus(memberId, itemId, deleted, status);
    }

    /**
     *
     * @param orderId
     * @return
     */
    public Long findMemberIdByOrderId(Long orderId) {
        return ordersRepository.findMemberIdByOrderId(orderId);
    }

    /**
     *
     * @param type
     * @param memberId
     * @return
     */
    public List<Order> findByTypeAndMemberId(String type, Long memberId) {
        return ordersRepository.findByTypeAndMemberId(type, memberId);
    }

    /**
     *
     * @param email
     * @return
     */
    public List<Order> findByPayerEmail(String email) {
        return ordersRepository.findByPayerEmail(email);
    }

    /**
     *
     * @param storeId
     * @param type
     * @param active
     * @param activationDate
     * @return
     */
    public Long countByStoreIdAndTypeAndActiveAndActivationDateLessThanEqual(String storeId, String type, Boolean active, Date activationDate) {
        return ordersRepository.countByStoreIdAndTypeAndActiveAndActivationDateLessThanEqual(storeId, type, active, activationDate);
    }

    /**
     *
     * @param type
     * @return
     */
    public List<Order> findByType(String type) {
        return ordersRepository.findByType(type);
    }

    /**
     *
     * @param source
     * @return
     */
    public List<Order> findBySource(String source) {
        return ordersRepository.findBySource(source);
    }

    /**
     *
     * @param externalId
     * @param type
     * @param source
     * @return
     */
    public Order findFirstByExternalIdAndTypeInAndSource(Long externalId, List<String> type, String source) {
        return ordersRepository.findFirstByExternalIdAndTypeInAndSource(externalId, type, source);
    }

    /**
     *
     * @return
     */
    public List<Order> findAll() {
        return ordersRepository.findAll();
    }

    /**
     *
     * @param deleted
     * @param memberId
     * @return
     */
    public List<Order> findByDeletedAndMemberId(Boolean deleted, Long memberId) {
        return ordersRepository.findByDeletedAndMemberId(deleted, memberId);
    }

    /**
     *
     * @param type
     * @param deleted
     * @param status
     * @return
     */
    public List<Order> findByTypeAndDeletedAndStatus(String type, Boolean deleted, String status) {
        return ordersRepository.findByTypeAndDeletedAndStatus(type, deleted, status);
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param type
     * @param showDeleted
     * @param source
     * @param payment
     * @param country
     * @param f
     * @param t
     * @param status
     * @param activation
     * @param ticketNumber
     * @param bac
     * @return
     */
    public Page<Order> search(int page, int size, String q, String type, Boolean showDeleted, String source, String payment, String country, Date f, Date t, String status, Boolean activation, String ticketNumber, String bac) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate", "id"));
        if (activation != null && activation) {
            pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "activationDate", "id"));
        }
        return ordersRepository.findAll(OrdersSpecs.search(q, type, showDeleted, source, payment, country, f, t, status, activation, ticketNumber, bac), pr);
    }

    public void processOrderDetails(Order order) {

        //clear old ones
        List<OrderItem> toDelete = orderItemsRepository.findByOrderId(order.getId());
        orderItemsRepository.delete(toDelete);

        //only first installment of every order
        if (order.getInstallmentNumber() != null && order.getInstallmentNumber() > 1) {
            return;
        }

        //deleted, we dont' need details
        if (order.isDeleted()) {
            return;
        }

        //tops ups and services skip
        if (order.getOrderItem().getCategoryName().equals("topup") || order.getOrderItem().getCategoryName().equals("service")) {
            return;
        }

        //FREE
        if (order.getItemId() == 15) {
            return;
        }

        //recreate new ones
        List<OrderItem> orderItems = new ArrayList<>();
        StoreItem storeItem = storeItemsRepository.findOne(order.getItemId());
        if (storeItem.getChildren().isEmpty()) {

            OrderItem detail = new OrderItem();
            detail.setComplexId(null);
            detail.setComplexName(null);
            detail.setSimpleId(storeItem.getId());
            detail.setSimpleName(storeItem.getName());
            //TODO: deberia ir el id de la opcion seleccionada, que pasa si es mas de una
            detail.setItemOptionId(null);
            detail.setItemOptionSelected(null);
            //TODO: si tenia opcion, aca iva el id de lo viejo, se puede borrar creo
            detail.setStoreItemOptionId(null);
            //TODO: duplicado?
            detail.setName(storeItem.getName());
            detail.setOrderId(order.getId());

            //TODO:
            //detail.setQuantity(item.getQuantity());
            detail.setShipped(false);
            //todo: servía para el codigo de barras del shipping no se si sirve ahora
            detail.setStoreItemEan(null);
            detail.setQuantity(1);

            orderItems.add(detail);

        } else {

            //lots of details
            List<StoreItemChild> children = storeItem.getChildren();
            for (StoreItemChild storeItemChild : children) {

                OrderItem detail = new OrderItem();
                detail.setComplexId(storeItem.getId());
                detail.setComplexName(storeItem.getName());
                detail.setSimpleId(storeItemChild.getChildId());
                detail.setSimpleName(storeItemChild.getChild().getName());

                //TODO: deberia ir el id de la opcion seleccionada, que pasa si es mas de una
                detail.setItemOptionId(null);
                detail.setItemOptionSelected(null);
                //TODO: si tenia opcion, aca iva el id de lo viejo, se puede borrar creo
                detail.setStoreItemOptionId(null);

                //TODO: duplicado?
                detail.setName(storeItemChild.getChild().getName());
                detail.setOrderId(order.getId());
                detail.setQuantity(storeItemChild.getQuantity());
                detail.setShipped(false);
                //todo: servía para el codigo de barras del shipping no se si sirve ahora
                detail.setStoreItemEan(null);

                orderItems.add(detail);

            }
        }

        //guardar los items, antes hay que guardar la ordern
        orderItemsRepository.save(orderItems);

    }

    public List<OrderItem> getOrderDetailsByOrderIds(List<Long> ids) {
        return orderItemsRepository.findByOrderIdIn(ids);
    }

    public List<String> getItemsNamesByCategory(String category) {
        return storeItemsRepository.getItemsNamesByCategory(category);
    }

}
