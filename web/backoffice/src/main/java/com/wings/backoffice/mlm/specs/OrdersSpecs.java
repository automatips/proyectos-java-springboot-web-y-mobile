/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class OrdersSpecs {

    public static Specification<Order> search(String q, String type, Boolean showDeleted, String source,
            String payment, String country, Date from, Date to, String status, Boolean activation, String ticketNumber, String bac) {

        return (Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (q != null) {

                //if it's only numbers, search by id
                try {
                    Long id = Long.parseLong(q);
                    predicates.add(builder.or(builder.equal(root.get("externalId"), id)));
                    predicates.add(builder.or(builder.equal(root.get("id"), id)));
                    return builder.or(predicates.toArray(new Predicate[]{}));
                } catch (NumberFormatException e) {
                }

                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("firstName"), search),
                                builder.like(root.get("lastName"), search),
                                builder.like(root.get("payerEmail"), search),
                                builder.like(root.get("storeId"), search),
                                builder.like(root.get("billingName"), search),
                                builder.like(root.get("billingCompany"), search),
                                builder.like(builder.concat(builder.concat(root.get("firstName"), " "), root.get("lastName")), search)
                        )
                );
            }

            if (ticketNumber != null) {
                String search = "%" + ticketNumber + "%";
                predicates.add(builder.like(root.get("ticketNumber"), search.replaceAll("\\.", "")));
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("type"), type));
            }

            if (showDeleted == null || !showDeleted) {
                predicates.add(builder.equal(root.get("deleted"), false));
            }

            if (source != null) {
                predicates.add(builder.equal(root.get("source"), source));
            }

            if (payment != null) {
                predicates.add(builder.equal(root.get("paymentType"), payment));
            }

            if (bac != null) {
                predicates.add(builder.equal(root.get("paymentBacs"), bac));
            }

            if (activation != null && activation) {
                if (from != null) {
                    predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("activationDate"), from));
                }

                if (to != null) {
                    predicates.add(builder.lessThanOrEqualTo(root.<Date>get("activationDate"), to));
                }
            } else {
                if (from != null) {
                    predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), from));
                }

                if (to != null) {
                    predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), to));
                }
            }

            if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (country != null) {
                if (country.equals("other")) {
                    //TODO: cambia a listado de paises por config
                    List<String> countries = Arrays.asList("CO", "EC", "PE", "ES");
                    predicates.add(root.get("billingCountry").in(countries).not());
                } else {
                    predicates.add(builder.equal(root.get("billingCountry"), country));
                }
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    /**
     * Filters orders by status = paid and deleted = false, also use country and
     * dates
     *
     * @param country
     * @param from
     * @param to
     * @return
     */
    public static Specification<Order> searchByCountryAndActivationDate(String country, Date from, Date to) {

        return (Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            predicates.add(builder.equal(root.get("deleted"), false));
            predicates.add(builder.equal(root.get("status"), "paid"));

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("activationDate"), from));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("activationDate"), to));
            }

            if (country != null) {
                predicates.add(builder.equal(root.get("billingCountry"), country));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    /**
     * Filters orders by status = paid and deleted = false, also use country and
     * dates
     *
     * @param country
     * @param from
     * @param to
     * @return
     */
    public static Specification<Order> searchByCountryAndCreationYearAndPending(String country, Date from, Date to) {

        return (Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            predicates.add(builder.equal(root.get("deleted"), false));
            predicates.add(builder.equal(root.get("status"), "paid"));
            predicates.add(builder.equal(root.get("installmentComplete"), false));
            predicates.add(builder.equal(root.get("installmentNumber"), 1));
            predicates.add(builder.notEqual(root.get("itemId"), 15));

            if (country != null) {
                predicates.add(builder.equal(root.get("billingCountry"), country.toUpperCase()));
            }

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), from));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), to));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
