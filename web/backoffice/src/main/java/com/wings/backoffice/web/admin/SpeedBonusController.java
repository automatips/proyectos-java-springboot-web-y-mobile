/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.bis.SpeedBonus;
import com.wings.backoffice.mlm.repos.SpeedBonusRepository;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.SpeedBonusService;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class SpeedBonusController {

    @Autowired
    private SpeedBonusRepository speedBonusRepository;

    @Autowired
    private SpeedBonusService speedBonusService;
    @Autowired
    private CountriesService countriesService;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/speed"}, method = RequestMethod.GET)
    public String getToolsSpeed(Model model, HttpServletRequest request) {
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String offer = request.getParameter("offer");
        String receive = request.getParameter("receive");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "name"));
        Page<SpeedBonus> items = speedBonusRepository.findAll(pr);

        PageWrapper<SpeedBonus> pageWrapper = new PageWrapper<>(items, "admin/tools/speed");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("offer", offer);
        model.addAttribute("receive", receive);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        return "admin/tools/speed";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/speed/asign"}, method = RequestMethod.POST)
    public ResponseEntity<String> asignSpeed(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(speedBonusService.asignSpeedBonusToAll());
    }

    /**
     *
     * @param model
     * @param redirectAttributes
     * @param id
     * @return
     */
    @RequestMapping(value = {"/admin/tools/speed/edit/{id}"}, method = RequestMethod.GET)
    public String updateSpeed(Model model, RedirectAttributes redirectAttributes, @PathVariable("id") Long id) {
        SpeedBonus speed = speedBonusService.findOne(id);
        List<String> countries = countriesService.getEnabledCountries();
        model.addAttribute("speed", speed);
        model.addAttribute("countries", countries);
        return "admin/tools/speed-abmc";
    }

    /**
     * 
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping(value = {"/admin/tools/speed/edit"}, method = RequestMethod.POST)
    public ResponseEntity<String> postUpdateSpeed(Model model, HttpServletRequest request) {

        String id = request.getParameter("id");
        SpeedBonus speed = speedBonusService.findOne(Long.valueOf(id));
        if (speed == null) {
            return ResponseEntity.badRequest().body("Speed not found!");
        }

        String name = request.getParameter("name");
        String country = request.getParameter("country");
        if (country != null && country.equals("null")) {
            country = null;
        }
        String excludeStr = request.getParameter("exclude");
        Boolean exclude = Boolean.valueOf(excludeStr);
        String validFromStr = request.getParameter("validFrom");
        String validToStr = request.getParameter("validTo");
        String durationToStr = request.getParameter("durationTo");
        String durationToFounderStr = request.getParameter("durationToFounder");
        String durationDaysStr = request.getParameter("durationDays");
        String durationDaysFounderStr = request.getParameter("durationDaysFounder");
        String priceRankStr1 = request.getParameter("priceRank1");
        String priceRankStr2 = request.getParameter("priceRank2");
        String priceRankStr3 = request.getParameter("priceRank3");
        String priceRankStr4 = request.getParameter("priceRank4");
        String priceRankStr5 = request.getParameter("priceRank5");
        String priceRankStr6 = request.getParameter("priceRank6");
        String priceRankStr7 = request.getParameter("priceRank7");
        String priceRankStr8 = request.getParameter("priceRank8");
        String priceRankStr9 = request.getParameter("priceRank9");

        Date validFrom = null;
        Date validTo = null;
        if (validFromStr != null) {
            try {
                validFrom = sdf.parse(validFromStr);
                validFrom = DateUtils.getFirstHourOfDay(validFrom);
            } catch (ParseException e) {
            }
        }
        if (validToStr != null) {
            try {
                validTo = sdf.parse(validToStr);
                validTo = DateUtils.getLastHourOfDay(validTo);
            } catch (ParseException e) {
            }
        }

        Date durationTo = null;
        Date durationToFounder = null;
        if (durationToStr != null) {
            try {
                durationTo = sdf.parse(durationToStr);
                durationTo = DateUtils.getLastHourOfDay(durationTo);
            } catch (ParseException e) {
            }
        }
        if (durationToFounderStr != null) {
            try {
                durationToFounder = sdf.parse(durationToFounderStr);
                durationToFounder = DateUtils.getLastHourOfDay(durationToFounder);
            } catch (ParseException e) {
            }
        }

        Integer durationDays = null;
        Integer durationDaysFounder = null;
        if (durationDaysStr != null) {
            try {
                durationDays = Integer.parseInt(durationDaysStr);
            } catch (NumberFormatException e) {
            }
        }
        if (durationDaysFounderStr != null) {
            try {
                durationDaysFounder = Integer.parseInt(durationDaysFounderStr);
            } catch (NumberFormatException e) {
            }
        }

        Double priceRank1, priceRank2, priceRank3, priceRank4, priceRank5, priceRank6, priceRank7, priceRank8, priceRank9;
        priceRank1 = priceRank2 = priceRank3 = priceRank4 = priceRank5 = priceRank6 = priceRank7 = priceRank8 = priceRank9 = null;
        if (priceRankStr1 != null) {
            try {
                priceRank1 = Double.parseDouble(priceRankStr1);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr2 != null) {
            try {
                priceRank2 = Double.parseDouble(priceRankStr2);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr3 != null) {
            try {
                priceRank3 = Double.parseDouble(priceRankStr3);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr4 != null) {
            try {
                priceRank4 = Double.parseDouble(priceRankStr4);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr5 != null) {
            try {
                priceRank5 = Double.parseDouble(priceRankStr5);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr6 != null) {
            try {
                priceRank6 = Double.parseDouble(priceRankStr6);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr7 != null) {
            try {
                priceRank7 = Double.parseDouble(priceRankStr7);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr8 != null) {
            try {
                priceRank8 = Double.parseDouble(priceRankStr8);
            } catch (NumberFormatException e) {
            }
        }
        if (priceRankStr9 != null) {
            try {
                priceRank9 = Double.parseDouble(priceRankStr9);
            } catch (NumberFormatException e) {
            }
        }

        speed.setName(name);
        speed.setCountry(country);
        speed.setExcludeClientCount(exclude);
        speed.setValidFrom(validFrom);
        speed.setValidTo(validTo);
        speed.setDurationTo(durationTo);
        speed.setDurationToFounder(durationToFounder);
        speed.setDurationDays(durationDays);
        speed.setDurationDaysFounder(durationDaysFounder);
        speed.setPriceRank1(priceRank1);
        speed.setPriceRank2(priceRank2);
        speed.setPriceRank3(priceRank3);
        speed.setPriceRank4(priceRank4);
        speed.setPriceRank5(priceRank5);
        speed.setPriceRank6(priceRank6);
        speed.setPriceRank7(priceRank7);
        speed.setPriceRank8(priceRank8);
        speed.setPriceRank9(priceRank9);

        try {
            speedBonusService.save(speed);
            return ResponseEntity.ok("Guardado con éxito");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error al guardar: " + e.getMessage());
        }
    }
}
