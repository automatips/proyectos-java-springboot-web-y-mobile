package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.FoundersView;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author lucas
 */
public interface OrdersViewRepository extends ViewRepository<FoundersView, Long> {
    @Query(value = "SELECT *, FLOOR(RAND()*(654564654-1)+1) as id FROM vw_orders_paid_diamond;", nativeQuery = true)
    List<FoundersView> findCompleteDiamond();
    @Query(value = "SELECT *, FLOOR(RAND()*(654564654-1)+1) as id FROM vw_orders_paid_golden", nativeQuery = true)
    List<FoundersView> findCompleteGolden();
    @Query(value = "SELECT *, FLOOR(RAND()*(654564654-1)+1) as id FROM vw_orders_first_diamond", nativeQuery = true)
    List<FoundersView> findFirstDiamond();
    @Query(value = "SELECT *, FLOOR(RAND()*(654564654-1)+1) as id FROM vw_orders_first_golden", nativeQuery = true)
    List<FoundersView> findFirstGolden();
    @Query(value = "SELECT *, FLOOR(RAND()*(654564654-1)+1) as id FROM vw_orders_second_diamond", nativeQuery = true)
    List<FoundersView> findSecondDiamond();
    @Query(value = "SELECT *, FLOOR(RAND()*(654564654-1)+1) as id FROM vw_orders_second_golden", nativeQuery = true)
    List<FoundersView> findSecondGolden();
}