/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import com.wings.backoffice.utils.NumberUtils;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bsc_matching")
public class Matching {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long receiveUserId;
    private Long offerUserId;
    private Double quantity;

    @Temporal(TemporalType.DATE)
    private Date date;

    @OneToOne
    @JoinColumn(name = "offerUserId", referencedColumnName = "id", updatable = false, insertable = false)
    private Member offerUser;

    @OneToOne
    @JoinColumn(name = "receiveUserId", referencedColumnName = "id", updatable = false, insertable = false)
    private Member receiveUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(Long receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public Long getOfferUserId() {
        return offerUserId;
    }

    public void setOfferUserId(Long offerUserId) {
        this.offerUserId = offerUserId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Member getOfferUser() {
        return offerUser;
    }

    public void setOfferUser(Member offerUser) {
        this.offerUser = offerUser;
    }

    public Member getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(Member receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getQuantityPayText(){
        return NumberUtils.formatMoney(quantity);
    }
}
