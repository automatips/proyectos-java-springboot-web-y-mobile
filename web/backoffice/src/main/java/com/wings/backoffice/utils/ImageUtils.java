/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author seba
 */
public class ImageUtils {

    private static final int width = 250;
    private static final int height = 250;

    /**
     *
     * @param sourceImg	The source of the image to resize.
     * @param destImg	The source of the image to resize.
     * @return	File new file scaled
     */
    public static boolean resizeImage(File sourceImg, File destImg) {
        BufferedImage origImage;
        try {

            origImage = ImageIO.read(sourceImg);
            int type = origImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : origImage.getType();

            //*Special* if the width or height is 0 use image src dimensions
            int fHeight = height;
            int fWidth = width;

            //Work out the resized width/height
            if (origImage.getHeight() > height || origImage.getWidth() > width) {
                fHeight = height;
                int wid = width;
                float sum = (float) origImage.getWidth() / (float) origImage.getHeight();
                fWidth = Math.round(fHeight * sum);

                if (fWidth > wid) {
                    //rezise again for the width this time
                    fHeight = Math.round(wid / sum);
                    fWidth = wid;
                }
            }

            BufferedImage resizedImage = new BufferedImage(fWidth, fHeight, type);
            Graphics2D g = resizedImage.createGraphics();
            g.setComposite(AlphaComposite.Src);

            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g.drawImage(origImage, 0, 0, fWidth, fHeight, null);
            g.dispose();

            ImageIO.write(resizedImage, "png", destImg);

        } catch (Exception ex) {
            return false;
        }

        return true;
    }

}
