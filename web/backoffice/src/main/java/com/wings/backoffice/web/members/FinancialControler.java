/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberBankAccount;
import com.wings.backoffice.mlm.domain.MemberMedia;
import com.wings.backoffice.mlm.domain.Withdrawal;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.enums.BonusType;
import com.wings.backoffice.mlm.enums.WithdrawalStatus;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.repos.InvoiceRepository;
import com.wings.backoffice.mlm.repos.MembersBankAccountRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.WithdrawalRepository;
import com.wings.backoffice.mlm.specs.BalanceSpecs;
import com.wings.backoffice.mlm.specs.InvoiceSpecs;
import com.wings.backoffice.mlm.specs.WithdrawalSpecs;
import com.wings.backoffice.services.BalanceService;
import com.wings.backoffice.services.InvoiceService;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.WithdrawalService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller("membersFinancialController")
public class FinancialControler {

    @Autowired
    private WithdrawalRepository withdrawalRepository;
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private BalanceDAO balanceDAO;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private InvoiceService invoiceService;
    //@Autowired
    //private MembersRepository membersRepository;
    @Autowired
    private MembersBankAccountRepository membersBankAccountRepository;
    @Autowired
    private MediaService mediaService;
    @Autowired
    private WithdrawalService withdrawalService;
    @Autowired
    private LogsService logsService;
    @Autowired
    private MembersService membersService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private final Logger log = LoggerFactory.getLogger(FinancialControler.class);

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/financial/balances"}, method = RequestMethod.GET)
    public String getBalances(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        Member m = AuthUtils.getMemberUser();
        m = membersService.getOne(m.getId());

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(null, f, t, type, null, m.getId(), null), pr);
        BalanceSums sums = balanceDAO.find(null, f, t, type, null, m.getId(), null);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        sums.setLocale(locale);
        for (Balance item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "members/financial/balances");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        //model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);
        model.addAttribute("type", type);

        return "members/financial/balances";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/withdrawal"}, method = RequestMethod.GET)
    public String getWithdrawalRequests(Model model, HttpServletRequest request) {

        String q = request.getParameter("q");
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        Member m = AuthUtils.getMemberUser();
        m = membersService.getOne(m.getId());

        Sort.Order dateSort = new Sort.Order(Sort.Direction.ASC, "requestDate");
        PageRequest pr = new PageRequest(page, size, new Sort(dateSort));
        Page<Withdrawal> items = withdrawalRepository.findAll(WithdrawalSpecs.search(q, status, f, t, m.getId(), null), pr);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        for (Withdrawal item : items) {
            item.setLocale(locale);
        }
        PageWrapper<Withdrawal> pageWrapper = new PageWrapper<>(items, "members/financial/withdrawal");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        List<MemberBankAccount> accounts = membersBankAccountRepository.findByMemberId(m.getId());
        for (MemberBankAccount account : accounts) {
            String code = mediaService.getBacsCode(account.getId());
            MemberMedia bac = mediaService.getMediaByCode(code);
            account.setBac(bac);
        }

        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", size);
        model.addAttribute("accounts", accounts);

        return "members/financial/withdrawal";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/withdrawal/create"}, method = RequestMethod.GET)
    public String createWithdrawalRequest(Model model, HttpServletRequest request) {
        model.addAttribute("postUrl", "/members/financial/withdrawal/create");
        Member m = AuthUtils.getMemberUser();
        m = membersService.getOne(m.getId());

        BalanceSums sums = balanceService.getMemberBalance(m.getId());
        sums.setLocale(CountryUtils.getLocaleForCountry(m.getCountry()));
        List<MemberBankAccount> bacs = membersBankAccountRepository.findByMemberIdAndVerified(m.getId(), Boolean.TRUE);

        model.addAttribute("bacs", bacs);
        model.addAttribute("effective", sums.getEffectiveText());
        model.addAttribute("signed", m.getContractSigned());

        return "members/financial/withdrawal-create";
    }

    /**
     * Post for create withdrawal
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/withdrawal/create"}, method = RequestMethod.POST)
    public ResponseEntity<String> postWithdrawalRequests(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());
        String user = member.getUsername();
        String amount = request.getParameter("amount");
        String description = request.getParameter("description");
        String bacsId = request.getParameter("bacs");

        if (amount == null) {
            return ResponseEntity.badRequest().body("El monto debe ser válido");
        }
        Double monto;
        try {
            monto = Double.parseDouble(amount);
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().body("El monto debe ser válido");
        }

        if (monto <= 0) {
            return ResponseEntity.badRequest().body("El monto debe ser válido");
        }

        //money effective convertion
        BalanceSums sums = balanceService.getMemberBalance(member.getId());
        double effective = sums.getEffective();
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        effective = NumberUtils.convertMoneyFromEuros(effective, locale);

        if (monto > effective) {
            return ResponseEntity.badRequest().body("El monto no puede ser mayor al saldo efectivo");
        }

        if (member.getDniType() == null || member.getDniType().equals("0")) {
            return ResponseEntity.badRequest().body("El tipo de dni no es válido, por favor actualize su perfil");
        }

        if (member.getDni() == null || member.getDni().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El número de dni no es válido, por favor actualize su perfil");
        }

        if (bacsId == null || bacsId.isEmpty()) {
            return ResponseEntity.badRequest().body("Cuenta de retiro no valida!");
        }

        long bacsIdL;
        try {
            bacsIdL = Long.parseLong(bacsId);
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().body("Cuenta de retiro no valida!");
        }

        MemberBankAccount bacs = membersBankAccountRepository.findOne(bacsIdL);
        if (bacs == null || !bacs.getVerified()) {
            return ResponseEntity.badRequest().body("Cuenta de retiro no valida!");
        }

        Date creatioDate = new Date();
        Withdrawal w = new Withdrawal();
        w.setAccountName(bacs.getAccountName());
        w.setAccountNumber(bacs.getAccountNumber());
        w.setAccountBank(bacs.getBankName());
        w.setAccountDescription(bacs.getDescription());
        w.setAccountIban(bacs.getAccountIban());
        w.setAccountSwift(bacs.getAccountSwift());
        w.setMemberId(member.getId());
        w.setProcessor("bacs");
        w.setRequestDate(creatioDate);
        w.setStatus(WithdrawalStatus.pending.name());
        w.setRequestUser(user);
        w.setRequestDescription(description);
        w.setIprf(false);
        w.setCurrency(CountryUtils.getCurrencyForCountry(member.getCountry()));

        //TODO: esto es para mantener el detalle en la moneda del afiliado
        //revisar una vez que se pase todo a cada moneda
        String details = withdrawalService.getDetails(member, monto);

        //go back to euros
        monto = NumberUtils.convertMoneyToEuros(monto, locale);

        //calculos
        w.setAmount(monto);
        withdrawalService.fillForMember(member, w);

        //TODO: cambiar, ver mas arriba, seteo el detalle en moneda local
        w.setDetails(details);

        withdrawalRepository.save(w);

        Balance b = new Balance();
        b.setAmount(monto * -1);
        b.setControl("withdrawal-" + w.getId());
        b.setCreationDate(creatioDate);
        b.setDescription("Solicitud retiro " + DateUtils.formatNormalDateTime(creatioDate) + " (#" + w.getId() + ")");
        b.setMemberId(member.getId());
        b.setReferenceId(w.getId());
        b.setEffective(true);
        b.setStatus(BalanceStatus.effective.name());
        b.setType(BalanceType.withdrawal.name());
        b.setComment("Auto balance created from withdrawal request - effective by default");

        balanceRepository.save(b);

        return ResponseEntity.ok("Solicitud creada");
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/withdrawal/calc"}, method = RequestMethod.POST)
    public ResponseEntity<String> getDetails(Model model, HttpServletRequest request) {

        String samount = request.getParameter("amount");

        if (samount == null) {
            return ResponseEntity.badRequest().body("El monto no es valido");
        }

        double amount;
        try {
            amount = Double.parseDouble(samount);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("El monto no es valido");
        }

        Member m = AuthUtils.getMemberUser();
        String details = withdrawalService.getDetails(m, amount);

        return ResponseEntity.ok(details);
    }

    /**
     *
     * @param model
     * @param request
     * @param requestId
     * @return
     */
    @RequestMapping(value = {"/members/financial/withdrawal/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deleteWithdrawalRequest(Model model, HttpServletRequest request, @PathVariable("id") Long requestId) {

        Withdrawal r = withdrawalRepository.findOne(requestId);
        if (r.getStatus().equals(WithdrawalStatus.completed.name())) {
            return ResponseEntity.badRequest().body("No se puede eliminar una solicitud con estado completo");
        }
        withdrawalRepository.delete(requestId);

        List<Balance> toDelete = balanceRepository.findByTypeAndReferenceId(BalanceType.withdrawal.name(), requestId);
        balanceRepository.delete(toDelete);

        return ResponseEntity.ok("Solicitud eliminada, balance asociado eliminado");
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/invoices"}, method = RequestMethod.GET)
    public String getInvoices(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        Member member = AuthUtils.getMemberUser();
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "invoiceDate"));
        Page<Invoice> items = invoiceRepository.findAll(InvoiceSpecs.search(q, f, t, type, "-1111111", null), pr);
        //BalanceSums sums = balanceDAO.find(q, f, t, type, null, null);

        PageWrapper<Invoice> pageWrapper = new PageWrapper<>(items, "members/financial/invoices");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        //model.addAttribute("total", sums);
        model.addAttribute("type", type);

        return "members/financial/invoices";
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"members/financial/invoices/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getInvoicePdf(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        Invoice i = invoiceRepository.findOne(id);
        if (i == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!member.getDni().equals(i.getClientDni())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (i.getFilePath() == null || i.getFilePath().trim().isEmpty()) {
            String path = invoiceService.generateInvoicePDF(i);
            i.setFilePath(path);
            invoiceRepository.save(i);
        }

        File pdfFile = new File(i.getFilePath().trim());
        if (!pdfFile.exists()) {
            String path = invoiceService.generateInvoicePDF(i);
            i.setFilePath(path);
            invoiceRepository.save(i);
        }

        FileInputStream fis;
        try {
            fis = new FileInputStream(pdfFile);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Disposition", "filename=" + i.getInvoiceNumber() + ".pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/account/create"}, method = RequestMethod.GET)
    public String getAccountCreate(Model model, HttpServletRequest request) {
        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());
        model.addAttribute("member", member);
        return "members/financial/account-create";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/account/create"}, method = RequestMethod.POST)
    public ResponseEntity<String> postAccountCreate(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        String accountName = request.getParameter("accountName");
        String bankName = request.getParameter("bankName");
        String accountNumber = request.getParameter("accountNumber");
        String accountIban = request.getParameter("accountIban");
        String accountSwift = request.getParameter("accountSwift");
        String description = request.getParameter("description");
        String accountType = request.getParameter("accountType");

        MemberBankAccount bac = new MemberBankAccount();
        bac.setMemberId(member.getId());
        bac.setAccountIban(accountIban);
        bac.setAccountName(accountName);
        bac.setAccountNumber(accountNumber);
        bac.setAccountSwift(accountSwift);
        bac.setAccountType(accountType);
        bac.setBankName(bankName);
        bac.setCreationDate(new Date());
        bac.setDescription(description);
        bac.setMediaId(null);
        bac.setMemberId(member.getId());
        bac.setVerified(Boolean.FALSE);

        membersBankAccountRepository.save(bac);

        return ResponseEntity.ok("Cuenta creada exitosamente!");
    }

    /**
     *
     * @param model
     * @param request
     * @param accountId
     * @return
     */
    @RequestMapping(value = {"/members/financial/account/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postAccountDelete(Model model, HttpServletRequest request, @PathVariable("id") Long accountId) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());
        MemberBankAccount acc = membersBankAccountRepository.findOne(accountId);

        if (acc.getMemberId().equals(member.getId())) {

            String code = mediaService.getBacsCode(accountId);
            mediaService.deleteMedia(code);

            membersBankAccountRepository.delete(acc);

            return ResponseEntity.ok("Cuenta eliminada!");
        } else {
            return ResponseEntity.badRequest().body("Error al eliminar la cuenta!");
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param accountId
     * @return
     */
    @RequestMapping(value = {"/members/financial/account/edit/{id}"}, method = RequestMethod.GET)
    public String getAccountEdit(Model model, HttpServletRequest request, @PathVariable("id") Long accountId) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());
        MemberBankAccount acc = membersBankAccountRepository.findOne(accountId);

        model.addAttribute("member", member);
        model.addAttribute("acc", acc);

        return "members/financial/account-edit";
    }

    /**
     *
     * @param model
     * @param request
     * @param accountId
     * @return
     */
    @RequestMapping(value = {"/members/financial/account/edit/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postAccountEdit(Model model, HttpServletRequest request, @PathVariable("id") Long accountId) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        String accountName = request.getParameter("accountName");
        String bankName = request.getParameter("bankName");
        String accountNumber = request.getParameter("accountNumber");
        String accountIban = request.getParameter("accountIban");
        String accountSwift = request.getParameter("accountSwift");
        String description = request.getParameter("description");
        String accountType = request.getParameter("accountType");

        MemberBankAccount bac = membersBankAccountRepository.findOne(accountId);

        if (!bac.getMemberId().equals(member.getId())) {
            return ResponseEntity.badRequest().body("Error modificando cuenta!");
        }

        bac.setAccountIban(accountIban);
        bac.setAccountName(accountName);
        bac.setAccountNumber(accountNumber);
        bac.setAccountSwift(accountSwift);
        bac.setAccountType(accountType);
        bac.setBankName(bankName);
        bac.setDescription(description);
        bac.setVerified(Boolean.FALSE);

        membersBankAccountRepository.save(bac);

        return ResponseEntity.ok("Cuenta modificada exitosamente!");
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/transfers"}, method = RequestMethod.GET)
    public String getTransfers(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        //get user balance
        BalanceSums total = balanceDAO.find(null, null, null, null, null, member.getId(), null);
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        total.setLocale(locale);
        model.addAttribute("total", total);
        model.addAttribute("member", member);

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        Page<Balance> items = balanceRepository.findByMemberIdAndBonusType(member.getId(), BonusType.transfer.name(), pr);

        //set locale for currency conversion
        for (Balance item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/members/financial/transfers");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);
        model.addAttribute("page", pageWrapper);

        return "members/financial/transfers";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/transfers"}, method = RequestMethod.POST)
    public ResponseEntity<String> postTransfers(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        String recipient = request.getParameter("recipient");
        String samount = request.getParameter("amount");

        if (recipient == null || recipient.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }
        if (samount == null || samount.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Monto inválido");
        }

        Double amount;
        try {
            amount = Double.parseDouble(samount);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Monto inválido");
        }

        if (amount <= 0) {
            return ResponseEntity.badRequest().body("Monto inválido");
        }

        Member dest = membersService.findByUsername(recipient);
        if (dest == null) {
            dest = membersService.findByEmail(recipient);
        }

        if (dest == null) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        //get locale
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        //get balance
        BalanceSums sums = balanceService.getMemberBalance(member.getId());
        double effective = sums.getEffective();
        //amount un euros
        double realAmount = NumberUtils.convertMoneyToEuros(amount, locale);

        if (realAmount > effective) {
            return ResponseEntity.badRequest().body("El monto no puede ser mayor al saldo efectivo");
        }

        if (realAmount > 10000) {
            log.error("WARNING!!! transfer amount bigger than 10k euros from " + member + " to " + dest);
        }

        //creo los balances
        Date creationDate = new Date();

        //retiro
        Balance discount = new Balance();
        discount.setAmount(realAmount * -1);
        discount.setControl("transfer-to-" + dest.getId());
        discount.setCreationDate(creationDate);
        discount.setDescription("Transferencia de fondos " + DateUtils.formatNormalDateTime(creationDate) + " a " + dest);
        discount.setMemberId(member.getId());
        discount.setReferenceId(dest.getId());
        discount.setEffective(true);
        discount.setStatus(BalanceStatus.effective.name());
        discount.setType(BalanceType.discount.name());
        discount.setBonusType(BonusType.transfer.name());
        discount.setComment("Transfer from user backoffice effective by default");
        balanceRepository.save(discount);

        //suma
        Balance positive = new Balance();
        positive.setAmount(realAmount);
        positive.setControl("transfer-from-" + member.getId());
        positive.setCreationDate(creationDate);
        positive.setDescription("Transferencia de fondos " + DateUtils.formatNormalDateTime(creationDate) + " de " + member);
        positive.setMemberId(dest.getId());
        positive.setReferenceId(member.getId());
        positive.setEffective(true);
        positive.setStatus(BalanceStatus.effective.name());
        positive.setType(BalanceType.append.name());
        positive.setBonusType(BonusType.transfer.name());
        positive.setComment("Transfer from user backoffice effective by default");
        balanceRepository.save(positive);

        //LOG ACTION
        String modUser = member.toString();
        if (AuthUtils.isAdmin()) {
            modUser = AuthUtils.getAdminUser().getUsername();
        }
        String ip = AuthUtils.getIp(request);
        logsService.addGenericLog(ip, modUser, member, "Transferencias de fondos", "Transferencia de fondos de " + member.getId() + " a " + dest.getId() + " amount " + amount + "," + realAmount + " effective balance " + sums.getEffective() + "," + effective);

        return ResponseEntity.ok("Transferencia realizada con exito!");

    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/financial/transfers/search"}, method = RequestMethod.POST)
    public ResponseEntity<String> searchMember(Model model, HttpServletRequest request) {

        String recipient = request.getParameter("recipient");
        if (recipient == null || recipient.isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        Member member = membersService.findByUsername(recipient);
        if (member == null) {
            member = membersService.findByEmail(recipient);
        }

        if (member == null) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        return ResponseEntity.ok(member.toString() + " (" + member.getUsername() + ")");
    }

}
