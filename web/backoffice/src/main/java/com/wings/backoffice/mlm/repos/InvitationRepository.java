/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.Invitation;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface InvitationRepository extends JpaRepository<Invitation, Long>, JpaSpecificationExecutor<Invitation> {

    List<Invitation> findByNombreAndApellidoAndMemberId(String nombre, String apellido, Long memberId);
    
    List<Invitation> findByNombreAndApellidoAndEmail(String nombre, String apellido, String email);

    List<Invitation> findByStatus(String status);
    
}
