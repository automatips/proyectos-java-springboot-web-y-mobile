/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.repos;

import com.wings.backoffice.store.domain.Referrals;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface ReferralsRepository extends JpaRepository<Referrals, Long> {

    Referrals findByReference(String reference);

}
