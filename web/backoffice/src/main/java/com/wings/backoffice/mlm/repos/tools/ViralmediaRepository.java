/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.tools;

import com.wings.backoffice.mlm.domain.tools.Viralmedia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface ViralmediaRepository extends JpaRepository<Viralmedia, Long> {

    Viralmedia findFirstByTaken(Boolean taken);

    Viralmedia findByMemberId(Long memberId);

}
