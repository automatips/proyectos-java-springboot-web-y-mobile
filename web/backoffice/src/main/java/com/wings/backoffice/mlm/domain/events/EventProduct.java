/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.events;

import com.wings.backoffice.utils.NumberUtils;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_events_products")
public class EventProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private long eventId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "short_name")
    private String shortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "country")
    private String country;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "member_discount")
    private boolean memberDiscount;
    @Column(name = "discount_price")
    private Double discountPrice;
    @Size(min = 1, max = 50)
    @Column(name = "status")
    private String status;
    @Column(name = "seats_available")
    private boolean seatsAvailable;
    @Column(name = "seats_pool")
    private Long seatsPool;
    @Column(name = "store_item_id")
    private Long storeItemId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", updatable = false, insertable = false, nullable = true)
    private Event event;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventProduct)) {
            return false;
        }
        EventProduct other = (EventProduct) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.web.admin.events.EventProduct[ id=" + id + " ]";
    }

    public boolean isMemberDiscount() {
        return memberDiscount;
    }

    public void setMemberDiscount(boolean memberDiscount) {
        this.memberDiscount = memberDiscount;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getPriceText() {
        return NumberUtils.formatMoneyForCountryIso(price, country);
    }

    public String getDiscountPriceText() {
        return NumberUtils.formatMoneyForCountryIso(discountPrice, country);
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSeatsAvailable() {
        return seatsAvailable;
    }

    public void setSeatsAvailable(boolean seatsAvailable) {
        this.seatsAvailable = seatsAvailable;
    }

    public Long getStoreItemId() {
        return storeItemId;
    }

    public void setStoreItemId(Long storeItemId) {
        this.storeItemId = storeItemId;
    }

    public Long getSeatsPool() {
        return seatsPool;
    }

    public void setSeatsPool(Long seatsPool) {
        this.seatsPool = seatsPool;
    }

    public String getDiscountPercent() {
        if (memberDiscount && price != null && discountPrice != null) {
            return String.valueOf(((int) ((((discountPrice * 100) / price) - 100) * -1)));
        } else {
            return "";
        }
    }

}
