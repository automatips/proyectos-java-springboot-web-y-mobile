/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.messages.SupportMessage;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class SupportMessagesSpecs {

    public static Specification<SupportMessage> search(String q, String status, String country, String assignation, Long memberId) {

        return (Root<SupportMessage> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("firstName"), search),
                                builder.like(root.get("lastName"), search),
                                builder.like(root.get("email"), search),
                                builder.like(root.get("username"), search),
                                builder.like(root.get("description"), search),
                                builder.like(builder.concat(builder.concat(root.get("firstName"), " "), root.get("lastName")), search)
                        )
                );
            }

            if (memberId != null) {
                predicates.add(builder.equal(root.get("memberId"), memberId));
            }

            if (status == null) {
                predicates.add(builder.equal(root.get("status"), "open"));
            }

            if (status != null && !status.equals("all")) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (country != null && !country.equals("all")) {
                predicates.add(builder.equal(root.get("country"), country));
            }

            if (assignation == null || assignation.equals("support")) {
                predicates.add(builder.equal(root.get("assignedTo"), "support"));
            }

            if (assignation != null && assignation.equals("admin")) {
                predicates.add(builder.equal(root.get("assignedTo"), "admin"));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
