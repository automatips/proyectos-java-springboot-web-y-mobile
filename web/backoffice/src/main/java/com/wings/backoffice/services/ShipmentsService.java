/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.config.WingsProperties;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.OrderItem;
import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import com.wings.backoffice.mlm.domain.bis.orders.ShipmentActivity;
import com.wings.backoffice.mlm.domain.bis.orders.ShipmentItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemOption;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.enums.ShipmentStatus;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.OrderItemsRepository;
import com.wings.backoffice.mlm.repos.ShipmentActivitiesRepository;
import com.wings.backoffice.mlm.repos.ShipmentItemsRepository;
import com.wings.backoffice.mlm.repos.ShipmentsRepository;
import com.wings.backoffice.mlm.repos.StoreItemOptionsRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.services.shippings.ecuador.EcuadorGuiaResponse;
import com.wings.backoffice.services.shippings.ecuador.EcuadorShippingService;
import com.wings.backoffice.services.shippings.peru.PeruResponseWrapper;
import com.wings.backoffice.services.shippings.peru.PeruShippingService;
import com.wings.backoffice.store.domain.Post;
import com.wings.backoffice.store.enums.PostMetaKeys;
import com.wings.backoffice.store.repos.PostsRepository;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.StringUtils;
import com.wings.shipping.co.ColombiaShippingService;
import com.wings.shipping.co.wrappers.InformationMovWrapper;
import com.wings.shipping.co.wrappers.ShipmentInformationWrapper;
import com.wings.shipping.ec.EcuadorWSShippingService;
import com.wings.shipping.co.wrappers.ShipmentItemWrapperCo;
import com.wings.shipping.co.wrappers.ShipmentWraperCo;
import com.wings.shipping.pe.PeruWSShippingService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author seba
 */
@Service
public class ShipmentsService {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrderItemsRepository orderItemsRepository;
    @Autowired
    private ShipmentsRepository shipmentsRepository;
    @Autowired
    private ShipmentItemsRepository shipmentItemsRepository;
    @Autowired
    private ShipmentActivitiesRepository shipmentActivitiesRepository;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UpsService upsService;
    @Autowired
    private PostsRepository postsRepository;
    @Autowired
    private StoreItemOptionsRepository storeItemOptionsRepository;
    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private EcuadorShippingService ecuadorShippingService;
    @Autowired
    private PeruShippingService peruShippingService;
    @Autowired
    private InvoiceService invoiceService;

    private final ColombiaShippingService colombiaShippingService = ColombiaShippingService.getInstance(true);
    private final EcuadorWSShippingService ecuadorWSShippingService = EcuadorWSShippingService.getInstance(true);
    private final PeruWSShippingService peruWSShippingService = PeruWSShippingService.getInstance(true);

    private final Logger log = LoggerFactory.getLogger(ShipmentsService.class);
    private final WingsProperties properties;
    private final String logo;

    private final String companyName;
    private final String companyAddress;

    private final String from;
    private final String to;

    private final String reportPath;
    private boolean running = false;

    private final Map<String, String> esStates = new HashMap<>();

    @Autowired
    public ShipmentsService(WingsProperties properties) {
        this.properties = properties;
        this.logo = properties.getLogo();
        this.companyName = properties.getPay().getMerchantName();
        this.companyAddress = properties.getContract().getDireccionWings();
        this.from = properties.getUps().getFrom();
        this.to = properties.getUps().getTo();
        this.reportPath = properties.getUps().getReportPath();

        esStates.put("C", "A Coruña");
        esStates.put("VI", "Araba/álava");
        esStates.put("AB", "Albacete");
        esStates.put("A", "Alicante");
        esStates.put("AL", "Almería");
        esStates.put("O", "Asturias");
        esStates.put("AV", "Ávila");
        esStates.put("BA", "Badajoz");
        esStates.put("PM", "Baleares");
        esStates.put("B", "Barcelona");
        esStates.put("BU", "Burgos");
        esStates.put("CC", "Cáceres");
        esStates.put("CA", "Cádiz");
        esStates.put("S", "Cantabria");
        esStates.put("CS", "Castellón");
        esStates.put("CE", "Ceuta");
        esStates.put("CR", "Ciudad Real");
        esStates.put("CO", "Córdoba");
        esStates.put("CU", "Cuenca");
        esStates.put("GI", "Girona");
        esStates.put("GR", "Granada");
        esStates.put("GU", "Guadalajara");
        esStates.put("SS", "Gipuzkoa");
        esStates.put("H", "Huelva");
        esStates.put("HU", "Huesca");
        esStates.put("J", "Jaén");
        esStates.put("LO", "La Rioja");
        esStates.put("GC", "Las Palmas");
        esStates.put("LE", "León");
        esStates.put("L", "Lleida");
        esStates.put("LU", "Lugo");
        esStates.put("M", "Madrid");
        esStates.put("MA", "Málaga");
        esStates.put("ML", "Melilla");
        esStates.put("MU", "Murcia");
        esStates.put("NA", "Navarra");
        esStates.put("OR", "Ourense");
        esStates.put("P", "Palencia");
        esStates.put("PO", "Pontevedra");
        esStates.put("SA", "Salamanca");
        esStates.put("TF", "Santa Cruz de Tenerife");
        esStates.put("SG", "Segovia");
        esStates.put("SE", "Sevilla");
        esStates.put("SO", "Soria");
        esStates.put("T", "Tarragona");
        esStates.put("TE", "Teruel");
        esStates.put("TO", "Toledo");
        esStates.put("V", "Valencia");
        esStates.put("VA", "Valladolid");
        esStates.put("BI", "Bizkaia");
        esStates.put("ZA", "Zamora");
        esStates.put("Z", "Zaragoza");

    }

    public boolean isRunning() {
        return running;
    }

    public String generateShipments() {

        running = true;
        int generated = 0;

        try {

            List<Order> orders = ordersService.findByType(OrderType.kit.name());
            for (Order order : orders) {

                if (!order.getActive()) {
                    log.error("Orden no activa, continuando " + order.getId());
                    continue;
                }

                if (order.getSource().equals("store")) {
                    log.error("Orden no de store, continuando " + order.getId());
                    continue;
                }

                if (order.getItemId().equals(15l)) {
                    log.error("Orden free, continuando " + order.getId());
                    continue;
                }

                if (order.getMemberId() == null) {
                    log.error("Atencion orden activa sin memberId " + order.getId());
                    continue;
                }

                Member member = membersRepository.findOne(order.getMemberId());
                if (!member.isColorSelected()) {
                    log.error("Orden sin color, continuando " + order.getId());
                    continue;
                }

                if (member.isSpecialOrder()) {
                    log.error("Orden especial, continuando " + order.getId());
                    continue;
                }

                List<String> enviados = new ArrayList<>();
                List<Shipment> already = shipmentsRepository.findByOrderId(order.getId());
                if (!already.isEmpty()) {
                    for (Shipment shipment : already) {
                        List<ShipmentItem> items = shipment.getItems();
                        for (ShipmentItem item : items) {
                            enviados.add(item.getItemEan());
                        }
                    }
                }

                String colorsSelected = member.getColors();
                if (colorsSelected == null || colorsSelected.trim().isEmpty()) {
                    log.error("Orden sin color, continuando " + order.getId());
                    continue;
                }

                List<String> shouldSend = new ArrayList(Arrays.asList(colorsSelected.split(";")));
                for (Iterator<String> iterator = shouldSend.iterator(); iterator.hasNext();) {
                    String next = iterator.next();
                    if (enviados.contains(next)) {
                        enviados.remove(next);
                        iterator.remove();
                    }
                }

                if (shouldSend.isEmpty()) {
                    log.error("Orden no deberia enviarse, continuando " + order.getId());
                    continue;
                }

                Shipment shipment = new Shipment();
                shipment.setCreationDate(new Date());
                shipment.setOrderId(order.getId());
                shipment.setDescription(order.getOrderItem().getName());
                shipment.setShippingAddress(member.getStreet());
                shipment.setShippingCity(member.getCity());
                shipment.setShippingCountry(member.getCountry());
                shipment.setShippingName(member.getFirstName() + " " + member.getLastName());
                shipment.setShippingPostalCode(member.getPostal());
                shipment.setShippingState(member.getState());
                shipment.setStatus(ShipmentStatus.created.name());
                shipment.setEmail(member.getEmail());
                shipment.setPhoneNumber(member.getPhone());
                shipment.setUsername(member.getUsername());
                shipment.setDeliverable(true);

                List<ShipmentItem> items = new LinkedList<>();
                for (String color : shouldSend) {

                    //sin stock
                    if (color.equals("8436574450040")) {
                        continue;
                    }

                    if (color.trim().isEmpty()) {
                        continue;
                    }

                    String name = "W5";
                    if (color.equals("8436574450019") || color.equals("8436574450026")) {
                        name = "W2";
                    }

                    String desc = "";
                    switch (color) {
                        case "8436574450019":
                            desc = name + " Gris " + order.getOrderItem().getName();
                            break;
                        case "8436574450026":
                            desc = name + " Dorado " + order.getOrderItem().getName();
                            break;
                        case "8436574450033":
                            desc = name + " Plata " + order.getOrderItem().getName();
                            break;
                        case "8436574450040":
                            desc = name + " Dorado " + order.getOrderItem().getName();
                            break;
                    }

                    ShipmentItem item = new ShipmentItem();
                    item.setItemName(name);
                    item.setItemEan(color);
                    item.setItemDescription(desc);
                    items.add(item);
                    shipmentItemsRepository.save(item);
                }

                if (items.isEmpty()) {
                    continue;
                }

                generated++;
                shipment.setItems(items);
                shipmentsRepository.save(shipment);
                shipment.setShipmentNumber(InvoiceUtils.getShipmentId(shipment.getId()));
                shipmentsRepository.save(shipment);

                File xml = createXml(shipment);
                File pdf = createPdf(shipment);
                shipment.setXmlPath(xml.getAbsolutePath());
                shipment.setPdfPath(pdf.getAbsolutePath());
                shipmentsRepository.save(shipment);
            }
        } catch (Exception e) {
            log.error("Error generando envio ", e);
        }

        running = false;
        return "Envios generados " + generated;
    }

    @Transactional
    public String generateShipments(Long orderId) {

        List<Shipment> deletables = shipmentsRepository.findByOrderId(orderId);
        for (Shipment shipment : deletables) {
            if (shipment.getStatus().equals("created") && shipment.getSendedDate() == null) {
                shipmentsRepository.delete(shipment.getId());
            }
        }

        Order order = ordersService.getOrder(orderId);
        int generated = 0;

        if (!order.getActive()) {
            return "\nOrden no active";
        }

        if (order.getSource().equals("store")) {
            return "\nOrden no generada por ser del nuevo store";
        }

        if (order.getItemId().equals(15l)) {
            return "\nOrden no generada por ser FREE";
        }

        if (order.getMemberId() == null) {
            return "\nAtencion orden activa sin meberId " + order.getId();
        }

        Member member = membersRepository.findOne(order.getMemberId());
        if (!member.isColorSelected()) {
            return "\nSin colores definidos";
        }

        if (member.isSpecialOrder()) {
            return "\nOrden marcada como Especial";
        }

        List<String> enviados = new ArrayList<>();
        List<Shipment> already = shipmentsRepository.findByOrderId(order.getId());
        if (!already.isEmpty()) {
            for (Shipment shipment : already) {
                List<ShipmentItem> items = shipment.getItems();
                for (ShipmentItem item : items) {
                    enviados.add(item.getItemEan());
                }
            }
        }

        String colorsSelected = member.getColors();
        if (colorsSelected == null || colorsSelected.trim().isEmpty()) {
            return "\nSin colores definidos";
        }

        List<String> shouldSend = new ArrayList(Arrays.asList(colorsSelected.split(";")));
        for (Iterator<String> iterator = shouldSend.iterator(); iterator.hasNext();) {
            String next = iterator.next();
            if (enviados.contains(next)) {
                enviados.remove(next);
                iterator.remove();
            }
        }

        if (shouldSend.isEmpty()) {
            return "\nSin items para enviar!";
        }

        Shipment shipment = new Shipment();
        shipment.setCreationDate(new Date());
        shipment.setOrderId(order.getId());
        shipment.setDescription(order.getOrderItem().getName());
        shipment.setShippingAddress(member.getStreet());
        shipment.setShippingCity(member.getCity());
        shipment.setShippingCountry(member.getCountry());
        shipment.setShippingName(member.getFirstName() + " " + member.getLastName());
        shipment.setShippingPostalCode(member.getPostal());
        shipment.setShippingState(member.getState());
        shipment.setStatus(ShipmentStatus.created.name());
        shipment.setEmail(member.getEmail());
        shipment.setPhoneNumber(member.getPhone());
        shipment.setUsername(member.getUsername());
        shipment.setDeliverable(true);

        List<ShipmentItem> items = new LinkedList<>();
        for (String color : shouldSend) {

            //sin stock
            if (color.equals("8436574450040")) {
                continue;
            }

            if (color.trim().isEmpty()) {
                continue;
            }

            String name = "W5";
            if (color.equals("8436574450019") || color.equals("8436574450026")) {
                name = "W2";
            }

            String desc = "";
            switch (color) {
                case "8436574450019":
                    desc = name + " Gris " + order.getOrderItem().getName();
                    break;
                case "8436574450026":
                    desc = name + " Dorado " + order.getOrderItem().getName();
                    break;
                case "8436574450033":
                    desc = name + " Plata " + order.getOrderItem().getName();
                    break;
                case "8436574450040":
                    desc = name + " Dorado " + order.getOrderItem().getName();
                    break;
            }

            ShipmentItem item = new ShipmentItem();
            item.setItemName(name);
            item.setItemEan(color);
            item.setItemDescription(desc);
            items.add(item);
            shipmentItemsRepository.save(item);
        }

        if (items.isEmpty()) {
            return "\nSin items para enviar!";
        }

        generated++;
        shipment.setItems(items);
        shipmentsRepository.save(shipment);
        shipment.setShipmentNumber(InvoiceUtils.getShipmentId(shipment.getId()));
        shipmentsRepository.save(shipment);

        File xml = createXml(shipment);
        File pdf = createPdf(shipment);
        shipment.setXmlPath(xml.getAbsolutePath());
        shipment.setPdfPath(pdf.getAbsolutePath());
        shipmentsRepository.save(shipment);

        return "\nEnvios generados " + generated;
    }

    public String processMembersColors() {

        running = true;
        List<Member> members = membersRepository.findAll();

        int procesados = 0;
        try {

            for (Member member : members) {

                Order o = member.getOrder();

                if (o == null) {
                    log.error("Order null para " + member);
                    continue;
                }

                if (o.getOrderItem() == null) {
                    log.error("Order Item null para " + member);
                    continue;
                }

                if (o.getActive() && o.getType().equals(OrderType.kit.name()) && o.getItemId() != 15
                        && (!member.isColorSelected() || (member.isColorSelected() && member.getColors() == null) || (member.isColorSelected() && member.getColors() != null && member.getColors().trim().isEmpty()))) {

                    String kitName = o.getOrderItem().getName().trim();
                    String colors = "";
                    switch (kitName) {
                        case "STARTER":
                            colors = (o.getId() % 2 == 0) ? "8436574450019" : "8436574450026";
                            break;
                        case "STARTER 2":
                            colors = "8436574450033";
                            break;
                        case "COMBO":
                            colors = "8436574450026;8436574450033";
                            break;
                        case "COMBO PRO":
                            colors = "8436574450026;8436574450033;8436574450033";
                            break;
                        case "COMBO BUSINESS":
                            colors = "8436574450019;8436574450026;8436574450033;8436574450033";
                            break;
                        case "COMBO FOUNDER":
                            colors = "8436574450019;8436574450026;8436574450019;8436574450026;8436574450033;8436574450033";
                            break;
                        default:
                            continue;
                    }

                    member.setColorSelected(true);
                    member.setColors(colors);

                    membersRepository.save(member);
                    procesados++;
                }

            }
        } catch (Exception e) {
            log.error("Error setting colors " + e.getMessage(), e);
            return "Error " + e.getMessage();
        }

        running = false;
        return "Registros procesados " + procesados;
    }

    public String processMembersColors(Long memberId) {

        try {

            Member member = membersRepository.findOne(memberId);
            Order o = member.getOrder();

            if (o == null) {
                log.error("Order null para " + member);
                return "Order null ";
            }

            if (o.getOrderItem() == null) {
                log.error("Order Item null para " + member);
                return "Order item null";
            }

            member.setColorSelected(false);
            member.setColors(null);

            if (o.getActive() && o.getType().equals(OrderType.kit.name()) && o.getItemId() != 15
                    && (!member.isColorSelected() || (member.isColorSelected() && member.getColors() == null) || (member.isColorSelected() && member.getColors() != null && member.getColors().trim().isEmpty()))) {

                String kitName = o.getOrderItem().getName().trim();
                String colors = "";
                switch (kitName) {
                    case "STARTER":
                        colors = (o.getId() % 2 == 0) ? "8436574450019" : "8436574450026";
                        break;
                    case "STARTER 2":
                        colors = "8436574450033";
                        break;
                    case "COMBO":
                        colors = "8436574450026;8436574450033";
                        break;
                    case "COMBO PRO":
                        colors = "8436574450026;8436574450033;8436574450033";
                        break;
                    case "COMBO BUSINESS":
                        colors = "8436574450019;8436574450026;8436574450033;8436574450033";
                        break;
                    case "COMBO FOUNDER":
                        colors = "8436574450019;8436574450026;8436574450019;8436574450026;8436574450033;8436574450033";
                        break;
                }

                member.setColorSelected(true);
                member.setColors(colors);

                membersRepository.save(member);
            }

        } catch (Exception e) {
            log.error("Error setting colors " + e.getMessage(), e);
            return "Error " + e.getMessage();
        }
        return "\n Colores seteados";
    }

    public String sendOne(Long id) {
        Shipment shipment = shipmentsRepository.findOne(id);
        List<Shipment> shipments = new ArrayList<>();
        shipments.add(shipment);
        return sendSelected(shipments);
    }

    public String sendAll() {
        PageRequest pr = new PageRequest(0, 10);
        Page<Shipment> page = shipmentsRepository.findAll(pr);
        List<Shipment> shipments = page.getContent();
        return sendSelected(shipments);
    }

    public String sendSelected(List<Shipment> shipments) {
        int sended = 0;
        for (Shipment shipment : shipments) {

            if (shipment.isConfirmed() && shipment.isDeliverable() && shipment.getStatus().equals(ShipmentStatus.created.name())) {

                List<File> attachments = new ArrayList<>();

                File pdf = createPdf(shipment);
                attachments.add(pdf);

                createXml(shipment);
                String path = shipment.getXmlPath();
                path = path.substring(0, path.length() - 5);
                for (int i = 0; i < shipment.getItems().size(); i++) {
                    File xml = new File(path + i + ".xml");
                    attachments.add(xml);
                }

                sendShipment(shipment, attachments);
                sended++;
            }
        }
        return "Pedidos enviados " + sended;
    }

    /**
     * Send Orders To UPS ftp server, marks order as sended or
     * replacement_sended acording to order state is error, mark order as error
     * or error_replacement
     *
     * @param shipment
     * @param attachments
     */
    public void sendShipment(Shipment shipment, List<File> attachments) {

        try {
            boolean emailSend = emailService.sendMail(from, to, "Pedido " + shipment.getShipmentNumber() + "0", "", attachments);

            if (!emailSend) {
                log.error("Error sending orders to UPS, failed to send email for  " + shipment.getId());
            }

            boolean ftpUpload = false;
            //upload ftp
            try {
                ftpUpload = upsService.upload(attachments);
            } catch (Exception e) {
                log.error("Error uploadings orders to UPS ftp", e);
            }

            Date sended = new Date();
            shipment.setSendedDate(ftpUpload ? sended : null);
            shipment.setStatus(ShipmentStatus.sended.name());

            shipmentsRepository.save(shipment);

        } catch (Exception e) {
            log.error("Error sending orders to UPS ", e);
        }
    }

    public File createXml(Shipment shipment) {

        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("XML");
        resolver.setSuffix(".xml");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);

        Context context = new Context();
        context.setVariable("empresa", shipment.getShippingName());
        context.setVariable("nombreCompleto", shipment.getShippingName());

        String address = shipment.getShippingAddress();
        String cp = shipment.getShippingPostalCode();
        String city = shipment.getShippingCity();
        String province = shipment.getShippingState();
        String country = shipment.getShippingCountry();

        context.setVariable("direccion", address);
        context.setVariable("ciudad", city);
        context.setVariable("cp", cp);
        context.setVariable("country", country);

        context.setVariable("description", shipment.getDescription());
        context.setVariable("shipperNumber", properties.getUps().getShipperNumber());
        context.setVariable("email", shipment.getEmail());
        context.setVariable("telefono", shipment.getPhoneNumber());
        context.setVariable("numberOfPackages", "1");

        //create multiple xmls
        File first = null;

        int i = 0;
        List<ShipmentItem> items = shipment.getItems();
        for (ShipmentItem item : items) {
            List<String> packages = new ArrayList<>();
            packages.add(item.getItemEan());
            context.setVariable("packages", packages);
            context.setVariable("numPedido", shipment.getShipmentNumber() + i);

            File ret = new File(properties.getUps().getOrdersDir() + "/" + shipment.getShipmentNumber() + i + ".xml");
            String response = engine.process("templates/xmls/pedido_template", context);

            i++;
            try {
                FileWriter fw = new FileWriter(ret);
                fw.write(response);
                fw.close();
                if (first == null) {
                    first = ret;
                }
            } catch (IOException e) {
                log.error("Error creating order XML", e);
            }
        }

        return first;
    }

    public File createPdf(Shipment shipment) {

        String numPedido = shipment.getShipmentNumber();

        String destName = properties.getUps().getOrdersDir() + "/" + numPedido + "0.pdf";
        Map<String, Object> map = new HashMap<>();

        String destinatario = shipment.getShippingName() + ", " + shipment.getAddressText();

        map.put("logo", logo);
        map.put("numPedido", numPedido + "0");
        map.put("remitente", companyName + ", " + companyAddress);
        map.put("destinatario", destinatario);

        try {

            List<Map<String, String>> fields = new ArrayList<>();
            int i = 0;
            for (ShipmentItem item : shipment.getItems()) {
                Map<String, String> field = new HashMap<>();
                field.put("pedido", numPedido + "" + (i));
                field.put("referencia", item.getItemEan());
                field.put("descripcion", item.getItemName());
                fields.add(field);
                i++;
            }

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(reportPath));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, map, new JRMapArrayDataSource(fields.toArray()));

            JasperExportManager.exportReportToPdfFile(jprint, destName);

        } catch (JRException e) {
            log.error("Error creating orders pdf ", e);
        }

        return new File(destName);
    }

    public void checkUpsStatus(String launcher) {

        List<File> downloaded = upsService.downloadOrders();
        if (downloaded.isEmpty()) {
            log.error("Lista vacia");
            return;
        }

        for (File file : downloaded) {
            try {

                log.error("Procesando:" + file.getAbsolutePath());
                BufferedReader bf = new BufferedReader(new FileReader(file));

                String strLine;
                while ((strLine = bf.readLine()) != null) {

                    log.error("parseando :" + strLine);

                    if (strLine.startsWith("Anulado")) {
                        continue;
                    }

                    if (strLine.trim().isEmpty()) {
                        continue;
                    }

                    //Anulado,Tracking,pedido,ICC,Tipo,PackageReference4,PackageReference5,Destinatario,Telefono
                    String[] array = strLine.split(",");
                    Shipment i = null;
                    //if this is true, order has a parent
                    if (array[0].replaceAll("\"", "").equals("Y")) {
                        continue;
                    } else {
                        String shipmentNumber = array[2].replaceAll("\"", "");
                        if (shipmentNumber == null || shipmentNumber.isEmpty()) {
                            continue;
                        }

                        shipmentNumber = shipmentNumber.substring(0, shipmentNumber.length() - 1);
                        i = shipmentsRepository.findByShipmentNumber(shipmentNumber);

                    }

                    if (i == null) {
                        log.error("Error Importing orders, order not found for " + array[2]);
                        continue;
                    }

                    if (i.getTrackingNumber() != null && !i.getTrackingNumber().trim().isEmpty()) {
                        log.error("Tracking number ya seteado, " + i.getId() + " viejo: " + i.getTrackingNumber() + ", nuevo: " + array[1].replaceAll("\"", ""));
                        continue;
                    }

                    i.setTrackingNumber(array[1].replaceAll("\"", ""));
                    i.setStatus(ShipmentStatus.transit.name());

                    shipmentsRepository.save(i);

                    log.error("fin line :" + strLine);
                }

            } catch (IOException | NumberFormatException e) {
                log.error("Error downloading csv", e);
            }
        }

    }

    /**
     * temp used to recheck from downloaded files
     */
    public void checkUpsStatus2() {

        File dir = new File("/var/wings/orders201/complete201");
        List<File> downloaded = Arrays.asList(dir.listFiles());

        if (downloaded.isEmpty()) {
            log.error("Lista vacia");
            return;
        }

        for (File file : downloaded) {
            try {

                log.error("Procesando:" + file.getAbsolutePath());
                BufferedReader bf = new BufferedReader(new FileReader(file));

                String strLine;
                while ((strLine = bf.readLine()) != null) {

                    log.error("parseando :" + strLine);

                    if (strLine.startsWith("Anulado")) {
                        continue;
                    }

                    if (strLine.trim().isEmpty()) {
                        continue;
                    }

                    //Anulado,Tracking,pedido,ICC,Tipo,PackageReference4,PackageReference5,Destinatario,Telefono
                    String[] array = strLine.split(",");
                    Shipment i = null;
                    //if this is true, order has a parent
                    if (array[0].replaceAll("\"", "").equals("Y")) {
                        continue;
                    } else {
                        String shipmentNumber = array[2].replaceAll("\"", "");
                        if (shipmentNumber == null || shipmentNumber.isEmpty()) {
                            continue;
                        }

                        shipmentNumber = shipmentNumber.substring(0, shipmentNumber.length() - 1);
                        i = shipmentsRepository.findByShipmentNumber(shipmentNumber);

                    }

                    if (i == null) {
                        log.error("Error Importing orders, order not found for " + array[2]);
                        continue;
                    }

                    if (i.getTrackingNumber() != null && !i.getTrackingNumber().trim().isEmpty()) {
                        log.error("Tracking number ya seteado, " + i.getId() + " viejo: " + i.getTrackingNumber() + ", nuevo: " + array[1].replaceAll("\"", ""));
                        continue;
                    }

                    i.setTrackingNumber(array[1].replaceAll("\"", ""));
                    i.setStatus(ShipmentStatus.transit.name());

                    shipmentsRepository.save(i);

                    log.error("fin line :" + strLine);
                }

            } catch (IOException | NumberFormatException e) {
                log.error("Error downloading csv", e);
            }
        }

    }

    public String generateStoreShipment() {

        running = true;
        int procesados = 0;

        try {

            List<Order> orders = ordersService.findBySource("store");

            for (Order order : orders) {

                if (order.isDeleted()) {
                    continue;
                }

                if (!order.getStatus().equals(OrderStatus.paid.name())) {
                    continue;
                }

                //TODO: fix when mix
                if (!order.getType().equals(OrderType.kit.name()) && !order.getType().equals(OrderType.product.name())) {
                    continue;
                }

                List<Shipment> already = shipmentsRepository.findByOrderId(order.getId());
                if (!already.isEmpty()) {
                    continue;
                }

                Post post = postsRepository.findOne(order.getExternalId());

                Shipment shipment = new Shipment();
                shipment.setCreationDate(new Date());
                shipment.setOrderId(order.getId());
                shipment.setDescription(order.getStoreTitle());

                shipment.setShippingAddress(post.getMetaMap().get(PostMetaKeys.SHIPPING_ADDRESS_1) + " " + post.getMetaMap().get(PostMetaKeys.SHIPPING_ADDRESS_2));
                shipment.setShippingCity(post.getMetaMap().get(PostMetaKeys.SHIPPING_CITY));
                shipment.setShippingCountry(post.getMetaMap().get(PostMetaKeys.SHIPPING_COUNTRY));
                shipment.setShippingName(post.getMetaMap().get(PostMetaKeys.SHIPPING_FIRST_NAME) + " " + post.getMetaMap().get(PostMetaKeys.SHIPPING_LAST_NAME));
                shipment.setShippingPostalCode(post.getMetaMap().get(PostMetaKeys.SHIPPING_POST_CODE));

                String state = esStates.get(post.getMetaMap().get(PostMetaKeys.SHIPPING_STATE));
                if (state == null) {
                    state = post.getMetaMap().get(PostMetaKeys.SHIPPING_STATE);
                }
                shipment.setShippingState(state);

                shipment.setStatus(ShipmentStatus.created.name());
                shipment.setEmail(order.getPayerEmail());
                shipment.setPhoneNumber(post.getMetaMap().get(PostMetaKeys.BILLING_PHONE));

                shipment.setUsername(order.getMember() != null ? order.getMember().getUsername() : "cliente");

                shipment.setDeliverable(true);

                List<ShipmentItem> items = new LinkedList<>();
                for (OrderItem item : order.getItems()) {

                    Long optionId = item.getStoreItemOptionId();
                    if (optionId == null) {
                        continue;
                    }

                    StoreItemOption option = storeItemOptionsRepository.findOne(item.getStoreItemOptionId());
                    if (option == null) {
                        continue;
                    }

                    ShipmentItem si = new ShipmentItem();
                    si.setItemDescription(item.getName() + " " + option.getOptionName() + " " + option.getValue() + " ");
                    si.setItemEan(option.getEanCode());
                    si.setItemName(item.getName());
                    items.add(si);

                    item.setShipped(true);
                    orderItemsRepository.save(item);
                }

                if (items.isEmpty()) {
                    continue;
                }

                shipmentsRepository.save(shipment);
                shipment.setShipmentNumber(InvoiceUtils.getShipmentId(shipment.getId()));
                shipmentsRepository.save(shipment);

                items.forEach((item) -> {
                    item.setShipmentId(shipment.getId());
                });
                shipmentItemsRepository.save(items);
                shipment.setItems(items);

                File xml = createXml(shipment);
                File pdf = createPdf(shipment);
                shipment.setXmlPath(xml.getAbsolutePath());
                shipment.setPdfPath(pdf.getAbsolutePath());

                shipmentsRepository.save(shipment);

                procesados++;
            }

        } catch (Exception e) {
            log.error("Error generando envios", e);
            return "Error generando envios " + e.getMessage();
        }

        running = false;
        return "Procesados " + procesados;
    }

    //@Scheduled(initialDelay = 15000l, fixedRate = 43200000l)
    public void checkShipmentStatus() {
        List<Shipment> shipments = shipmentsRepository.findByStatus(ShipmentStatus.transit.name());
        shipments.forEach((shipment) -> {
            checkShipmentStatus(shipment);
        });
    }

    public void checkShipmentStatus(Shipment shipment) {

        if (shipment.getTrackingNumber() == null || shipment.getTrackingNumber().isEmpty()) {
            log.error("Envio en transito sin tracking number");
            return;
        }

        List<ShipmentActivity> activities = upsService.getTrackingActivity(shipment.getTrackingNumber());
        activities.forEach((activity) -> {
            try {
                shipmentActivitiesRepository.save(activity);
            } catch (DataIntegrityViolationException e) {
            }
        });

        //set last activity to order
        boolean error = false;

        ShipmentActivity last = null;
        if (!activities.isEmpty()) {
            Collections.sort(activities, (ShipmentActivity o1, ShipmentActivity o2) -> o1.getActivityDate().compareTo(o2.getActivityDate()));
            last = activities.get(activities.size() - 1);
            shipment.setLastTrackingActivity(last.getDescription());
            for (ShipmentActivity activity : activities) {
                if (activity.getPackageAddressDescription() != null
                        && !activity.getPackageAddressDescription().isEmpty()
                        && activity.getPackageAddressDescription().equalsIgnoreCase("ReturnTo Address")) {
                    error = true;
                    break;
                }
            }
        }

        //set delivered status
        if (last != null && shipment.getLastTrackingActivity() != null
                && (shipment.getLastTrackingActivity().contains("Entregado") || shipment.getLastTrackingActivity().contains("El cliente recogió el paquete"))
                && (!shipment.getLastTrackingActivity().contains("Entregado a UPS") && !shipment.getLastTrackingActivity().contains("a que el cliente lo recoja"))) {

            if (error) {
                shipment.setStatus(ShipmentStatus.error.name());
                shipment.setStatusText("Devuelto al remitente");
            } else {
                shipment.setStatus(ShipmentStatus.delivered.name());
                shipment.setStatusText(last.getDescription());
                shipment.setDeliveryDate(last.getActivityDate());
            }
        }

        shipmentsRepository.save(shipment);
    }

    //TODO: all above code is old, should be deprecated, review usage
    ////////////////////////////////////////////////////////////////////////////
    /**
     *
     */
    public void createShipmemts() {

        //1 repasar todas las ordenes y sus envios
        //verificar que se envió del total
        //generar nuevos shipments si tiene W3 en el paquete
        //usar el estado de la orden para vovler a procesar
//      estados de orden:
//            created
//            partial
//            completed
//        estado de los envios
//              created
//              sended
//              recevived
//              traking
//              completed                      
//        una vez creaado el shipment y los shipments items
//        enviar a cada cliente de acuerdo el pais
        List<Order> orders = ordersService.fixOrders();
        for (Order order : orders) {
            createShipment(order);
        }

    }

    /**
     * Given order create shipment
     *
     * @param order
     */
    public void createShipment(Order order) {

        try {
            invoiceService.generateInvoice(order);
        } catch (Exception e) {
        }

        List<Shipment> already = shipmentsRepository.findByOrderIdAndDeleted(order.getId(), Boolean.FALSE);
        if (!already.isEmpty()) {
            //if has error, recreate with member data
            /*for (Shipment shipment : already) {

                if (shipment.getStatus().equals(ShipmentStatus.error.name()) && shipment.getTrackingNumber() == null) {

                    Member member = order.getMember();
                    shipment.setShippingAddress(member.getStreet());
                    shipment.setShippingCity(member.getCity());
                    shipment.setShippingCountry(member.getCountry());
                    shipment.setShippingName(member.getFirstName() + " " + member.getLastName());
                    shipment.setShippingPostalCode(member.getPostal());
                    shipment.setShippingState(member.getState());
                    shipment.setPhoneNumber(member.getPhone());
                    shipment.setUsername(member.getUsername());

                    shipment.setStatus(ShipmentStatus.created.name());
                    shipment.setTrackingNumber(null);
                    shipment.setStatusText("Recreated, prepared for send");
                    shipmentsRepository.save(shipment);

                }
            }*/
            return;
        }

        String prefix = "S0";
//        List<Shipment> already = shipmentsRepository.findByOrderId(order.getId());
//        if (!already.isEmpty()) {
//            prefix = "S" + already.size();
//        }

        //objetos a crear
        Shipment shipment = new Shipment();
        shipment.setConfirmed(false);
        shipment.setCreationDate(new Date());
        shipment.setDeleted(false);
        shipment.setDeliverable(true);
        shipment.setDeliveryDate(null);
        shipment.setDescription(order.getOrderItem().getName());
        shipment.setEmail(order.getPayerEmail());
        shipment.setLastTrackingActivity(null);
        shipment.setModificationDate(null);
        shipment.setOrderId(order.getId());
        shipment.setPdfPath(null);
        shipment.setSendedDate(null);
        shipment.setShipmentNumber(prefix + order.getId());

        Member member = order.getMember();
        shipment.setShippingAddress(member.getStreet());
        shipment.setShippingCity(member.getCity());
        shipment.setShippingCountry(member.getCountry());
        shipment.setShippingName(member.getFirstName() + " " + member.getLastName());
        shipment.setShippingPostalCode(member.getPostal());
        shipment.setShippingState(member.getState());
        shipment.setPhoneNumber(member.getPhone());
        shipment.setUsername(member.getUsername());

        shipment.setStatus(ShipmentStatus.created.name());
        shipment.setTrackingNumber(null);
        shipment.setXmlPath(null);

        //tantos items como tenga la orden
        //con los orders items, creo los shipment items
        List<OrderItem> items = order.getItems();
        List<ShipmentItem> shitems = new ArrayList<>();
        for (OrderItem item : items) {

            Long storeItemOptionId = item.getStoreItemOptionId();
            if (storeItemOptionId == null) {
                log.error("Store item option id is null for " + order.getId() + " " + shipment.getId());
                continue;
            }

            StoreItemOption option = storeItemOptionsRepository.findOne(storeItemOptionId);
            if (option.getStock() > 0) {
                ShipmentItem sitem = new ShipmentItem();
                sitem.setItemDescription(item.getName());
                sitem.setItemEan(item.getStoreItemEan());
                sitem.setItemName(item.getName());
                sitem.setStoreItemid(option.getStoreItemId());
                //sitem.setShipmentId(shipment.getId());
                shitems.add(sitem);
            }
        }

        //is theres nothing to deliver, do not create shipment
        if (!shitems.isEmpty()) {
            shipmentsRepository.save(shipment);
            shitems.forEach((shitem) -> {
                shitem.setShipmentId(shipment.getId());
            });
            shipmentItemsRepository.save(shitems);
        }

    }

    /**
     *
     * @param ids
     */
    public void sendToCurier(List<Long> ids) {

        List<Shipment> shipments = shipmentsRepository.findAll(ids);
        for (Shipment shipment : shipments) {

            //continue if it's not created
            if (!shipment.getStatus().equals(ShipmentStatus.created.name())) {
                continue;
            }

            switch (shipment.getShippingCountry()) {
                case "CO":
                    List<String> ret = colombiaShippingService.sendToCurier(createWrapperForCo(shipment));
                    if (ret != null && !ret.isEmpty()) {
                        String guia = ret.get(0);
                        try {
                            Long.parseLong(guia);
                            shipment.setStatus(ShipmentStatus.sended.name());
                            shipment.setSendedDate(new Date());
                            shipment.setTrackingNumber(guia);
                            shipment.setStatusText("Envio generado");
                        } catch (Exception e) {
                            shipment.setStatus(ShipmentStatus.error.name());
                            shipment.setStatusText(guia);
                        }
                    } else {
                        shipment.setStatus(ShipmentStatus.error.name());
                        shipment.setStatusText("Error enviando pedido");
                    }
                    break;
                case "EC":
                    EcuadorGuiaResponse response = ecuadorShippingService.sendToCurier(shipment);
                    if (response.getId() != 0l) {
                        shipment.setTrackingNumber(response.getId().toString());
                        shipment.setStatus(ShipmentStatus.sended.name());
                        shipment.setSendedDate(new Date());
                        shipment.setStatusText(response.getMsj());
                    } else {
                        shipment.setStatus(ShipmentStatus.error.name());
                        shipment.setStatusText(response.getMsj());
                    }
                    break;
                case "PE":
                    PeruResponseWrapper wrapper = peruShippingService.sendToCurier(shipment);

                    if (wrapper.getStatus().equals("ok")) {
                        shipment.setStatus(ShipmentStatus.sended.name());
                        shipment.setSendedDate(new Date());
                        shipment.setStatusText(wrapper.getMessage());

                    } else {

                        shipment.setStatus(ShipmentStatus.error.name());
                        shipment.setStatusText(wrapper.getMessage());

                    }

                    break;
            }
        }
        shipmentsRepository.save(shipments);

    }

    /**
     *
     * @param shipment
     * @return
     */
    private ShipmentWraperCo createWrapperForCo(Shipment shipment) {

        ShipmentWraperCo ret = new ShipmentWraperCo();

        ret.setDescription(shipment.getDescription());
        ret.setEmail(shipment.getEmail());
        ret.setId(shipment.getId());
        ret.setOrderId(shipment.getOrderId());
        ret.setPhoneNumber(shipment.getPhoneNumber());
        ret.setShipmentNumber(shipment.getShipmentNumber());
        ret.setShippingAddress(shipment.getShippingAddress());
        ret.setShippingCity(shipment.getShippingCity());
        ret.setShippingCountry(shipment.getShippingCountry());
        ret.setShippingName(shipment.getShippingName());
        ret.setShippingPostalCode(shipment.getShippingPostalCode());
        ret.setShippingState(shipment.getShippingState());

        List<ShipmentItemWrapperCo> witems = new LinkedList<>();
        List<ShipmentItem> items = shipment.getItems();
        for (ShipmentItem item : items) {

            StoreItem storeItem = storeItemsRepository.findOne(item.getStoreItemid());

            ShipmentItemWrapperCo witem = new ShipmentItemWrapperCo();
            witem.setHeight(storeItem.getSizeHeight());
            witem.setLength(storeItem.getSizeLength());
            witem.setWidth(storeItem.getSizeWidth());
            witem.setWeigth(storeItem.getWeight());

            if (witem.getHeight() == null || witem.getHeight() < 1) {
                witem.setHeight(2);
            }
            if (witem.getWidth() == null || witem.getWidth() < 1) {
                witem.setWidth(2);
            }
            if (witem.getLength() == null || witem.getLength() < 1) {
                witem.setLength(2);
            }
            if (witem.getWeigth() == null || witem.getWeigth() < 3) {
                witem.setWeigth(4);
            }

            witem.setId(item.getId());
            witem.setItemDescription(item.getItemDescription());
            witem.setItemEan(item.getItemEan());
            witem.setItemName(item.getItemName());
            witem.setShipmentId(item.getShipmentId());

            witems.add(witem);
        }

        ret.setItems(witems);

        return ret;
    }

    public Shipment getOne(Long shipmentId) {
        return shipmentsRepository.getOne(shipmentId);
    }

    public void checkTracking() {

        SimpleDateFormat CO_DATETIME = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        //List<Shipment> shipments = new LinkedList<>();
        //shipments.add(shipmentsRepository.findOne(4969l));
        //shipments.add(shipmentsRepository.findOne(5022l));
        List<Shipment> shipments = shipmentsRepository.findByStatusAndDeleted(ShipmentStatus.sended.name(), Boolean.FALSE);
        shipments.addAll(shipmentsRepository.findByStatusAndDeleted(ShipmentStatus.transit.name(), Boolean.FALSE));

        log.error("Total to check " + shipments.size());

        shipments.forEach((shipment) -> {
            if (shipment.getTrackingNumber() != null && !shipment.getTrackingNumber().isEmpty()) {

                switch (shipment.getShippingCountry()) {
                    case "CO":
                        ShipmentInformationWrapper info = colombiaShippingService.checkTracking(shipment.getTrackingNumber());
                        String error = null;
                        String lastStatusText = "";
                        //parseo los estados
                        List<InformationMovWrapper> movs = info.getMov();
                        List<ShipmentActivity> actsToSave = new LinkedList<>();
                        for (InformationMovWrapper mov : movs) {
                            ShipmentActivity actv = new ShipmentActivity();
                            try {
                                Date fecMov = CO_DATETIME.parse(mov.getFecMov());
                                actv.setActivityDate(fecMov);

                                String desc = (mov.getNomConc() != null ? mov.getNomConc() : "") + " " + (mov.getNomMov() != null ? mov.getNomMov() : "");
                                desc = StringUtils.capitalize(desc);
                                actv.setDescription(desc);

                                actv.setCode(String.valueOf(mov.getIdProc()));
                                actv.setTrackingCode(shipment.getTrackingNumber());

                                String locDesc = mov.getOriMov() + " " + mov.getDesMov();
                                locDesc = StringUtils.capitalize(locDesc);
                                actv.setLocationDescription(locDesc);
                                actv.setShipmentId(shipment.getId());

                                lastStatusText = mov.getNomMov();

                                actsToSave.add(actv);
                                if (mov.getNomMov() != null && mov.getNomMov().equals("INGRESO AL CENTRO LOGISTICO POR DEVOLUCION")) {
                                    error = mov.getNomMov();
                                }
                            } catch (Exception e) {
                            }
                        }

                        if (!actsToSave.isEmpty()) {
                            for (ShipmentActivity shipmentActivity : actsToSave) {
                                try {
                                    shipmentActivitiesRepository.save(shipmentActivity);
                                } catch (Exception e) {
                                }
                            }
                        }

                        //si entregado
                        if (info.getEstAct().equals("ENTREGADO")) {

                            shipment.setStatus(ShipmentStatus.delivered.name());
                            try {
                                Date fechaEstado = CO_DATETIME.parse(info.getFecEst());
                                shipment.setDeliveryDate(fechaEstado);
                                shipment.setStatusText("Entregado");
                            } catch (Exception e) {
                            }
                        }

                        if (info.getEstAct().equals("EN PROCESAMIENTO")) {
                            if (error != null && lastStatusText.equals(error)) {
                                shipment.setStatus(ShipmentStatus.error.name());
                                shipment.setStatusText(error);
                            } else {
                                shipment.setStatus(ShipmentStatus.transit.name());
                                shipment.setStatusText(lastStatusText);
                            }
                        }

                        if (info.getEstAct().equals("ENTREGADO A REMITENTE")) {
                            shipment.setStatus(ShipmentStatus.error.name());
                            shipment.setStatusText("Devuelto a Logistica, fin de ciclo.");
                        }

                        break;

                    case "EC":
                        com.wings.shipping.ec.wrappers.ShipmentInformationWrapper infoEc = ecuadorWSShippingService.checkTracking(shipment.getTrackingNumber());
                        String errorEC = null;
                        String lastStatusTextEc = "";
                        //parseo los estados
                        List<com.wings.shipping.ec.wrappers.InformationMovWrapper> movsEc = infoEc.getMov();
                        List<ShipmentActivity> actsToSaveEc = new LinkedList<>();
                        for (com.wings.shipping.ec.wrappers.InformationMovWrapper mov : movsEc) {
                            ShipmentActivity actv = new ShipmentActivity();
                            try {
                                Date fecMov = CO_DATETIME.parse(mov.getFecMov());
                                actv.setActivityDate(fecMov);

                                String desc = (mov.getNomConc() != null ? mov.getNomConc() : "") + " " + (mov.getNomMov() != null ? mov.getNomMov() : "");
                                desc = StringUtils.capitalize(desc);
                                actv.setDescription(desc);

                                actv.setCode(String.valueOf(mov.getIdProc()));
                                actv.setTrackingCode(shipment.getTrackingNumber());

                                String locDesc = mov.getOriMov() + " " + mov.getDesMov();
                                locDesc = StringUtils.capitalize(locDesc);
                                actv.setLocationDescription(locDesc);
                                actv.setShipmentId(shipment.getId());

                                lastStatusTextEc = mov.getNomMov();

                                actsToSaveEc.add(actv);
                                if (mov.getNomMov() != null && mov.getNomMov().equals("INGRESO AL CENTRO LOGISTICO POR DEVOLUCION")) {
                                    errorEC = mov.getNomMov();
                                }
                            } catch (Exception e) {
                            }
                        }

                        if (!actsToSaveEc.isEmpty()) {
                            for (ShipmentActivity shipmentActivity : actsToSaveEc) {
                                try {
                                    shipmentActivitiesRepository.save(shipmentActivity);
                                } catch (Exception e) {
                                }
                            }
                        }

                        if (infoEc.getEstAct() != null) {
                            //si entregado
                            if (infoEc.getEstAct().equals("ENTREGADO")) {

                                shipment.setStatus(ShipmentStatus.delivered.name());
                                try {
                                    Date fechaEstado = CO_DATETIME.parse(infoEc.getFecEst());
                                    shipment.setDeliveryDate(fechaEstado);
                                    shipment.setStatusText("Entregado");
                                } catch (Exception e) {
                                }
                            }

                            if (infoEc.getEstAct().equals("EN PROCESAMIENTO")) {
                                if (errorEC != null && lastStatusTextEc.equals(errorEC)) {
                                    shipment.setStatus(ShipmentStatus.error.name());
                                    shipment.setStatusText(errorEC);
                                } else {
                                    shipment.setStatus(ShipmentStatus.transit.name());
                                    shipment.setStatusText(lastStatusTextEc);
                                }
                            }

                            if (infoEc.getEstAct().equals("ENTREGADO A REMITENTE")) {
                                shipment.setStatus(ShipmentStatus.error.name());
                                shipment.setStatusText("Devuelto a Logistica, fin de ciclo.");
                            }
                        }

                        break;
                    case "PE":

                        com.wings.shipping.pe.wrappers.ShipmentInformationWrapper infoPe = peruWSShippingService.checkTracking(shipment.getTrackingNumber());
                        String errorPE = null;
                        String lastStatusTextPE = "";
                        //parseo los estados
                        List<com.wings.shipping.pe.wrappers.InformationMovWrapper> movsPe = infoPe.getMov();
                        List<ShipmentActivity> actsToSavePe = new LinkedList<>();
                        for (com.wings.shipping.pe.wrappers.InformationMovWrapper mov : movsPe) {
                            ShipmentActivity actv = new ShipmentActivity();
                            try {
                                Date fecMov = CO_DATETIME.parse(mov.getFecMov());
                                actv.setActivityDate(fecMov);

                                String desc = (mov.getNomConc() != null ? mov.getNomConc() : "") + " " + (mov.getNomMov() != null ? mov.getNomMov() : "");
                                desc = StringUtils.capitalize(desc);
                                actv.setDescription(desc);

                                actv.setCode(String.valueOf(mov.getIdProc()));
                                actv.setTrackingCode(shipment.getTrackingNumber());

                                String locDesc = mov.getOriMov() + " " + mov.getDesMov();
                                locDesc = StringUtils.capitalize(locDesc);
                                actv.setLocationDescription(locDesc);
                                actv.setShipmentId(shipment.getId());

                                lastStatusTextPE = mov.getNomMov();

                                actsToSavePe.add(actv);
                                if (mov.getNomMov() != null && mov.getNomMov().equals("INGRESO AL CENTRO LOGISTICO POR DEVOLUCION")) {
                                    errorPE = mov.getNomMov();
                                }
                            } catch (Exception e) {
                            }
                        }

                        if (!actsToSavePe.isEmpty()) {
                            for (ShipmentActivity shipmentActivity : actsToSavePe) {
                                try {
                                    shipmentActivitiesRepository.save(shipmentActivity);
                                } catch (Exception e) {
                                }
                            }
                        }

                        if (infoPe.getEstAct() != null) {
                            //si entregado
                            if (infoPe.getEstAct().equals("ENTREGADO")) {

                                shipment.setStatus(ShipmentStatus.delivered.name());
                                try {
                                    Date fechaEstado = CO_DATETIME.parse(infoPe.getFecEst());
                                    shipment.setDeliveryDate(fechaEstado);
                                    shipment.setStatusText("Entregado");
                                } catch (Exception e) {
                                }
                            }

                            if (infoPe.getEstAct().equals("EN PROCESAMIENTO")) {
                                if (errorPE != null && lastStatusTextPE.equals(errorPE)) {
                                    shipment.setStatus(ShipmentStatus.error.name());
                                    shipment.setStatusText(errorPE);
                                } else {
                                    shipment.setStatus(ShipmentStatus.transit.name());
                                    shipment.setStatusText(lastStatusTextPE);
                                }
                            }

                            if (infoPe.getEstAct().equals("ENTREGADO A REMITENTE")) {
                                shipment.setStatus(ShipmentStatus.error.name());
                                shipment.setStatusText("Devuelto a Logistica, fin de ciclo.");
                            }
                        }

                        break;
                }
            }
        });

        shipments.forEach((shipment) -> {
            shipmentsRepository.save(shipment);
        });
    }

    public void sendEmailEcuador() {

        List<Shipment> shipments = shipmentsRepository.findByStatusAndDeletedAndEmailSentAndShippingCountry(
                ShipmentStatus.sended.name(),
                Boolean.FALSE,
                Boolean.FALSE,
                "EC");

        if (shipments.isEmpty()) {
            return;
        }

        StringBuilder csv = new StringBuilder();
        boolean header = true;
        for (Shipment shipment : shipments) {

            if (shipment.getTrackingNumber() == null || shipment.getTrackingNumber().isEmpty()) {
                continue;
            }

            if (header) {
                csv.append("NUMERO_GUIA,DETALLE_ENVIO_2,RAZON_SOCIAL_DESTINATARIO,SHIPMENT NUMBER,DIRECCION1_DESTINATARIO,TELEFONO1_DESTINATARIO,NOMBRE_PROVINCIA_DESTINO,NOMBRE_CIUDAD_DESTINO,NUMERO_PIEZAS");
                csv.append(System.lineSeparator());
                header = false;
            }

            csv.append("\"");
            csv.append(shipment.getTrackingNumber()).append("\",\"");
            csv.append(shipment.getItemsDetails()).append("\",\"");
            csv.append(shipment.getShippingName()).append("\",\"");
            csv.append(shipment.getShipmentNumber()).append("\",\"");
            csv.append(shipment.getShippingAddress()).append("\",\"");
            csv.append(shipment.getPhoneNumber()).append("\",\"");
            csv.append(shipment.getShippingState()).append("\",\"");
            csv.append(shipment.getShippingCity()).append("\",");
            csv.append(shipment.getItems().size());
            csv.append(System.lineSeparator());

            shipment.setEmailSent(true);

        }

        if (!csv.toString().isEmpty()) {
            Date when = new Date();
            List<File> attachements = new ArrayList<>();
            try {
                String prefix = DateUtils.formatComputerDate(when) + "_envios_wings";
                File f = File.createTempFile(prefix, ".csv");
                FileWriter fw = new FileWriter(f);
                fw.write(csv.toString());
                fw.close();

                attachements.add(f);
                String dfrom = "Wings Mobile<contratacion@wingsmobile.com>";
                String subject = "Envios Wings Mobile EC " + DateUtils.formatNormalDate(new Date());
                String dto = "jonathan.munoz@servientrega.com.ec,sebastian.lucero@wingsmobile.es,Lorena Alonso<lorena.alonso@wingsmobile.co>";
                boolean sent = emailService.sendMail(dfrom, dto, subject, subject, attachements);

                if (sent) {
                    shipmentsRepository.save(shipments);
                }
            } catch (Exception e) {
                log.error("Error sending mail to Ecuador Curier", e);
            }
        }
    }

    public void sendEmailColombia() {

        List<Shipment> shipments = shipmentsRepository.findByStatusAndDeletedAndEmailSentAndShippingCountry(
                ShipmentStatus.sended.name(),
                Boolean.FALSE,
                Boolean.FALSE,
                "CO");

        if (shipments.isEmpty()) {
            return;
        }

        StringBuilder csv = new StringBuilder();
        boolean header = true;
        for (Shipment shipment : shipments) {

            if (shipment.getTrackingNumber() == null || shipment.getTrackingNumber().isEmpty()) {
                continue;
            }

            if (header) {
                csv.append("NUMERO_GUIA,DETALLE_ENVIO_2,RAZON_SOCIAL_DESTINATARIO,SHIPMENT NUMBER,DIRECCION1_DESTINATARIO,TELEFONO1_DESTINATARIO,NOMBRE_PROVINCIA_DESTINO,NOMBRE_CIUDAD_DESTINO,NUMERO_PIEZAS");
                csv.append(System.lineSeparator());
                header = false;
            }

            csv.append("\"");
            csv.append(shipment.getTrackingNumber()).append("\",\"");
            csv.append(shipment.getItemsDetails()).append("\",\"");
            csv.append(shipment.getShippingName()).append("\",\"");
            csv.append(shipment.getShipmentNumber()).append("\",\"");
            csv.append(shipment.getShippingAddress()).append("\",\"");
            csv.append(shipment.getPhoneNumber()).append("\",\"");
            csv.append(shipment.getShippingState()).append("\",\"");
            csv.append(shipment.getShippingCity()).append("\",");
            csv.append(shipment.getItems().size());
            csv.append(System.lineSeparator());

            shipment.setEmailSent(true);

        }

        if (!csv.toString().isEmpty()) {
            Date when = new Date();
            List<File> attachements = new ArrayList<>();
            try {
                String prefix = DateUtils.formatComputerDate(when) + "_envios_wings";
                File f = File.createTempFile(prefix, ".csv");
                FileWriter fw = new FileWriter(f);
                fw.write(csv.toString());
                fw.close();

                attachements.add(f);
                String dfrom = "Wings Mobile<contratacion@wingsmobile.com>";
                String subject = "Envios Wings Mobile CO " + DateUtils.formatNormalDate(new Date());
                String dto = "Bryan Steeven Martinez Rojas <bryan.martinez@servientrega.com>, Sebastian Lucero <sebastian.lucero@wingsmobile.es>, Irene Viviana Enciso Triana <irene.enciso@servientrega.com>,Carlos Andres Ruiz <carlosa.ruiz@servientrega.com>,Lorena Alonso<lorena.alonso@wingsmobile.co>, David Mejia<david.mejia@servientrega.com>";
                boolean sent = emailService.sendMail(dfrom, dto, subject, subject, attachements);

                if (sent) {
                    shipmentsRepository.save(shipments);
                }
            } catch (Exception e) {
                log.error("Error sending mail to Colombia Curier", e);
            }
        }
    }

}
