/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config.auth;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.utils.AuthUtils;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.servlet.LocaleResolver;

/**
 *
 * @author seba
 */
public class WingsAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private LogsService logsService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication a) throws IOException, ServletException {

        String modIp = AuthUtils.getIp(request);
        if (a.getPrincipal() instanceof Member) {
            Member m = (Member) a.getPrincipal();
            logsService.addMemberLoginLog(a.getName(), modIp, m, true);
        } else {
            AdminUser admin = (AdminUser) a.getPrincipal();
            logsService.addAdminLoginLog(a.getName(), modIp, admin, true);
        }

        //localeResolver.setLocale(hsr, hsr1, new Locale("en"));
        response.sendRedirect("/");
    }

}
