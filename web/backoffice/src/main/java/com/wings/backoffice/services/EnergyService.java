/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.backoffice.services.energy.ContractRequest;
import com.wings.backoffice.services.energy.response.Response;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class EnergyService {

    public List<Response> getContracts() {

        String url = "http://www.signmethod.com:8080/PORTAL_COMUNICACION/rest/contract/getContratosComerciales";

        /*Map req = new HashMap();
        req.put("apiKey", "c99648f8-480d-4e27-ad0e-a9a6edd6da3f");
        req.put("comerciales", new String[]{"okseba1"});
        req.put("fechaInicio", "01/04/2018");
        req.put("fechaFin", "20/04/2018");
        req.put("empresa", "particular");*/
        ContractRequest request = new ContractRequest();
        request.setApiKey("c99648f8-480d-4e27-ad0e-a9a6edd6da3f");
        List<String> comerciales = new ArrayList<>();
        comerciales.add("okseba1");
        request.setComerciales(comerciales);
        request.setFechaInicio(DateUtils.getStartDateForPeriod(4, 2018));
        request.setFechaFin(DateUtils.getEndDateForPeriod(4, 2018));
        request.setEmpresa("particular");

        StringBuilder b = new StringBuilder();
        b
                .append("{")
                .append("apiKey:'c99648f8-480d-4e27-ad0e-a9a6edd6da3f',")
                .append("comerciales:[okseba1],")
                .append("fechaInicio:'01/04/2018',")
                .append("fechaFin:'25/04/2018',")
                .append("empresa:'particular'")
                .append("}");
        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        HttpEntity<String> requestEntity = new HttpEntity<>(b.toString(), headers);

        ResponseEntity<String> result = template.postForEntity(url, requestEntity, String.class);

        System.out.println(result.getBody());
        
        
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = result.getBody();
        try {
            //JSON from file to Object
            Response[] obj = mapper.readValue(jsonInString, Response[].class);
            return Arrays.asList(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

}
