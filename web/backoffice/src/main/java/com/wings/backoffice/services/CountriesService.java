package com.wings.backoffice.services;

import com.wings.api.models.CountryDto;
import com.wings.backoffice.mlm.domain.Country;
import com.wings.backoffice.mlm.domain.CountryProperties;
import com.wings.backoffice.mlm.repos.CountriesRepository;
import com.wings.backoffice.mlm.repos.CountryPropertiesRepository;
import com.wings.backoffice.mlm.specs.CountrySpecs;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.LocaleUtils;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas
 */
@Service
public class CountriesService {

    @Autowired
    private CountriesRepository countriesRepository;
    @Autowired
    private CountryPropertiesRepository countryPropertiesRepository;

    private final Logger log = LoggerFactory.getLogger(CountriesService.class);
    private final Map<String, Locale> mapLocales = new HashMap<>();
    private final Map<String, Double> mapExchanges = new HashMap<>();
    private final Map<String, String> mapIsoAndCurrency = new HashMap<>();
    private final Map<String, String> mapCurrencyAndIso = new HashMap<>();
    private final List<String> listIsos = new LinkedList<>();

    /**
     *
     * @param q
     * @param page
     * @param size
     * @param status
     * @return
     */
    public Page<Country> search(String q, int page, int size, String status) {
        page = page == 0 ? page : page - 1;
        Sort sort = new Sort(Sort.Direction.ASC, "code");
        PageRequest pr = new PageRequest(page, size, sort);
        return countriesRepository.findAll(CountrySpecs.search(q, status), pr);
    }

    /**
     *
     * @param code
     * @return
     */
    public Country getByCode(String code) {
        return countriesRepository.findOne(code);
    }

    /**
     *
     * @param code
     * @return
     */
    public CountryProperties getPropertiesByCode(String code) {
        return countryPropertiesRepository.findOne(code);
    }

    /**
     *
     * @param code
     */
    public void enabled(String code) {
        Country findOne = countriesRepository.findOne(code);
        findOne.setEnabled(!findOne.isEnabled());
        countriesRepository.save(findOne);
    }

    /**
     *
     * @param action
     * @param code
     * @param oldCode
     * @param name
     * @param enabled
     * @return
     */
    public Result<Country> insertOrUpdate(String action, String code, String oldCode, String name, String enabled) {
        Result<Country> result = new Result<>();
        Country country = new Country(code, false, name, Boolean.parseBoolean(enabled));
        validateInsertOrUpdate(action, result, country);

        if (result.isOk()) {
            result.setResult(countriesRepository.save(country));
        }
        return result;
    }

    /**
     *
     * @param action
     * @param result
     * @param country
     */
    private void validateInsertOrUpdate(String action, Result<Country> result, Country country) {
        country.setCode(country.getCode().toUpperCase());
        country.setNameEs(country.getNameEs().toUpperCase());

        if (country.getCode() == null || country.getCode().trim().length() == 0 || country.getCode().length() > 2) {
            result.addErrorMessage("El código de país no puede estar vacío o superar los 2 carácteres.");
        }

        if (country.getNameEs() == null || country.getNameEs().trim().length() == 0) {
            result.addErrorMessage("El nombre de país no puede estar vacío.");
        }

        if (action.equals("create") && countriesRepository.findOne(country.getCode()) != null) {
            result.addErrorMessage("El código de país ya se encuentra registrado.");
        } else if (action.equals("edit") && countriesRepository.findOne(country.getCode()) == null) {
            result.addErrorMessage("El código de país no se encuentra registrado.");
        }
    }

    /**
     *
     * @return
     */
    public List<String> getEnabledCountries() {
        List<Country> findByEnabled = countriesRepository.findByEnabled(Boolean.TRUE);
        final List<String> countries = new ArrayList<>(findByEnabled.size());
        findByEnabled.forEach(country -> countries.add(country.getCode()));
        return countries;
    }

    /**
     *
     * @param properties
     */
    public void saveProperties(CountryProperties properties) {
        clearCaches();
        countryPropertiesRepository.save(properties);
    }

    /**
     * get all countries available for store
     *
     * @return
     */
    public List<CountryProperties> getAllCountries() {
        return countryPropertiesRepository.findAll();
    }

    /**
     * get all countries available for store
     *
     * @return
     */
    public List<CountryProperties> getCountriesAvailableForStore() {
        Sort sort = new Sort(Sort.Direction.ASC, "sortOrder");
        return countryPropertiesRepository.getByAvailableStore(true, sort);
    }

    /**
     * get all available countries DTO
     *
     * @return
     */
    public List<CountryDto> getCountriesDtoAvailableForStore() {
        List<CountryDto> dtos = new LinkedList<>();
        List<CountryProperties> props = getCountriesAvailableForStore();
        props.stream().map((prop) -> {
            return transformDto(prop);
        }).forEachOrdered((dto) -> {
            dtos.add(dto);
        });
        return dtos;
    }

    /**
     * get all countries not enabled yet
     *
     * @return
     */
    public List<CountryDto> getDisabledCountries() {
        List<CountryDto> dtos = new LinkedList<>();
        List<Country> disabled = countriesRepository.findByEnabled(Boolean.FALSE);
        disabled.forEach((country) -> {
            CountryDto dto = new CountryDto();
            dto.setCode(country.getCode());
            dtos.add(dto);
        });
        return dtos;
    }

    /**
     * get one DTO from code
     *
     * @param code
     * @return
     */
    public CountryDto getCountryDto(String code) {
        return transformDto(countryPropertiesRepository.getOneByCode(code));
    }

    /**
     *
     * @param prop
     * @return
     */
    private CountryDto transformDto(CountryProperties prop) {
        CountryDto dto = new CountryDto();
        dto.setAvailableBackoffice(prop.isAvailableBackoffice());
        dto.setAvailableStore(prop.isAvailableStore());
        dto.setCode(prop.getCode());
        dto.setCurrency(prop.getCurrency());
        dto.setCustom1(prop.getCustom1());
        dto.setCustom2(prop.getCustom2());
        dto.setCustom3(prop.getCustom3());
        dto.setCustom4(prop.getCustom4());
        dto.setCustom5(prop.getCustom5());
        dto.setDefaultLang(prop.getDefaultLang());
        dto.setDefaultLocale(prop.getDefaultLocale());
        dto.setDnisList(prop.getDnisList());
        dto.setExchangeRate(prop.getExchangeRate());
        dto.setFlag(prop.getFlag());
        dto.setIdtsList(prop.getIdtsList());
        dto.setIva1(prop.getIva1());
        dto.setIva2(prop.getIva2());
        dto.setIva3(prop.getIva3());
        dto.setShipping1(prop.getShipping1());
        dto.setShipping2(prop.getShipping2());
        dto.setShipping3(prop.getShipping3());
        dto.setShipping4(prop.getShipping4());
        dto.setSortOrder(prop.getSortOrder());
        dto.setCountryLevel(prop.getCountryLevel());
        dto.setPopulationLevel(prop.getPopulationLevel());
        return dto;
    }

    /**
     * Convert money from euro to locale conversion rate
     *
     * @param money
     * @param country
     * @return
     */
    public Double convertMoneyFromEuros(double money, String country) {
        if (country == null) {
            country = "ES";
        }
        CountryProperties props = countryPropertiesRepository.getOneByCode(country);
        double conversionRate = 1.0;
        if (props != null && props.getExchangeRate() != null) {
            conversionRate = props.getExchangeRate();
        }
        return money * conversionRate;
    }

    /**
     * Convert money from locale to euros using conversion rate
     *
     * @param money
     * @param country
     * @return
     */
    public Double convertMoneyToEuros(double money, String country) {
        if (country == null) {
            country = "ES";
        }
        CountryProperties props = countryPropertiesRepository.getOneByCode(country);
        double conversionRate = 1.0;
        if (props != null && props.getExchangeRate() != null) {
            conversionRate = props.getExchangeRate();
        }
        return money / conversionRate;
    }

    /**
     * Format money
     *
     * @param money
     * @param country
     * @return
     */
    public String formatMoney(double money, String country) {
        if (country == null) {
            country = "ES";
        }
        CountryProperties props = countryPropertiesRepository.getOneByCode(country);
        Locale locale = null;
        try {
            String[] iso = props.getDefaultLocale().split("_");
            locale = new Locale(iso[0], iso[1]);
        } catch (Exception e) {
        }

        if (!LocaleUtils.isAvailableLocale(locale)) {
            locale = new Locale(props.getDefaultLang().toLowerCase(), props.getCode());
        }

        if (!LocaleUtils.isAvailableLocale(locale)) {
            locale = new Locale("es", "US");
        }

        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        return f.format(money);
    }

    /////TODOS ESTOS METODOS SON PARA LOS Utils Estaticos NumberUtils CountryUtils
    //TODO: reemplazar los utils estaticos de alguna manera o mover esos metodos al service
    /**
     * Create a map with iso and exchange rates
     *
     * @return
     */
    public Map<String, Double> getExchangeRates() {
        if (mapExchanges.isEmpty()) {
            List<CountryProperties> props = countryPropertiesRepository.findAll();
            props.forEach((prop) -> {
                mapExchanges.put(prop.getCode(), prop.getExchangeRate());
            });
        }
        return mapExchanges;
    }

    /**
     * Creates a Map with iso and Locale
     *
     * @return
     */
    public Map<String, Locale> getLocalesMap() {
        if (mapLocales.isEmpty()) {
            List<CountryProperties> props = countryPropertiesRepository.findAll();
            props.forEach((prop) -> {
                mapLocales.put(prop.getCode(), new Locale(prop.getDefaultLocale().split("_")[0], prop.getDefaultLocale().split("_")[1]));
            });
        }
        return mapLocales;
    }

    /**
     * Creates a Map with iso as key and currency as value
     *
     * @return
     */
    public Map<String, String> getCurrenciesMap() {
        if (mapIsoAndCurrency.isEmpty()) {
            List<CountryProperties> props = countryPropertiesRepository.findAll();
            props.forEach((prop) -> {
                mapIsoAndCurrency.put(prop.getCode(), prop.getCurrency());
            });
        }
        return mapIsoAndCurrency;
    }

    /**
     * Creates a Map with currency as key and iso2 as value
     *
     * @return
     */
    public Map<String, String> getCurrenciesMapInverted() {
        if (mapCurrencyAndIso.isEmpty()) {
            List<CountryProperties> props = countryPropertiesRepository.findAll();
            props.forEach((prop) -> {
                mapCurrencyAndIso.put(prop.getCurrency(), prop.getCode());
            });
        }
        return mapCurrencyAndIso;
    }

    /**
     * get a list of all the isos
     *
     * @return
     */
    public List<String> getCountriesIsos() {
        if (listIsos.isEmpty()) {
            List<CountryProperties> list = getAllCountries();
            list.forEach((countryProperties) -> {
                listIsos.add(countryProperties.getCode());
            });
        }
        return listIsos;
    }

    private void clearCaches() {
        mapLocales.clear();
        mapExchanges.clear();
        mapIsoAndCurrency.clear();
        mapCurrencyAndIso.clear();
        listIsos.clear();
    }

}
