/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api.store;

import com.wings.api.models.CartDto;
import com.wings.api.models.CartDtoResponse;
import com.wings.backoffice.services.ApiAuthService;
import com.wings.backoffice.services.OrdersService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
@RequestMapping(value = "api/v1/")
public class CartController {

    @Autowired
    private ApiAuthService apiAuthService;
    @Autowired
    private OrdersService ordersService;

    @RequestMapping(value = {"cart"}, method = {RequestMethod.POST})
    public ResponseEntity<CartDtoResponse> saveCart(Model model, HttpServletRequest request, @RequestBody CartDto cart) {
        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }
        CartDtoResponse cartResponse = ordersService.processCart(cart);
        return ResponseEntity.ok(cartResponse);
    }

}
