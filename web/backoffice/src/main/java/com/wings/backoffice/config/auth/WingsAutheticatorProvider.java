/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config.auth;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.enums.LogReference;
import com.wings.backoffice.mlm.enums.LogType;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author seba
 */
public class WingsAutheticatorProvider implements AuthenticationProvider {

    private final WingsUsersDetailsService wingsUsersDetailsService;

    public WingsAutheticatorProvider(WingsUsersDetailsService wingsUsersDetailsService) {
        this.wingsUsersDetailsService = wingsUsersDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {

        UserDetails userDetails = wingsUsersDetailsService.loadUserByUsername(a.getName());
        if (userDetails != null) {

            String cred = DigestUtils.sha256Hex(a.getCredentials().toString());
            if (cred.equals(userDetails.getPassword())) {
                return new UsernamePasswordAuthenticationToken(userDetails, null,userDetails.getAuthorities());
            } else {
                throw buildException("Wrong username or password", userDetails, a.getName());
            }

        } else {
            throw buildException("Username not found", a.getName());
        }
    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(UsernamePasswordAuthenticationToken.class);
    }

    private LoginFailureException buildException(String message, UserDetails details, String username) {

        LoginFailureException ex = new LoginFailureException(message);

        ex.setMessageText(message);
        ex.setModUser(username);

        if (details instanceof Member) {
            ex.setType(LogType.member_login.name());
            ex.setReference(LogReference.member.name());
            ex.setReferenceId(((Member) details).getId());
            ex.setComment("member login failure");
        } else {
            ex.setType(LogType.admin_login.name());
            ex.setReference(LogReference.admin.name());
            ex.setReferenceId(((AdminUser) details).getId());
            ex.setComment("admin login failure");

        }

        return ex;

    }

    private LoginFailureException buildException(String message, String username) {
        LoginFailureException ex = new LoginFailureException(message);
        ex.setType(LogType.login_error.name());
        ex.setComment("invalid username on login");
        ex.setMessageText(message);
        ex.setModUser(username);
        return ex;

    }

}
