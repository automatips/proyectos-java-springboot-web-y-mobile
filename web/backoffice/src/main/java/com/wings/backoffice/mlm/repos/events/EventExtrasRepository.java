/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventExtra;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface EventExtrasRepository extends JpaRepository<EventExtra, Long> {

    List<EventExtra> findByEventId(Long eventId);

    @Transactional
    int deleteByEventId(Long eventId);

}
