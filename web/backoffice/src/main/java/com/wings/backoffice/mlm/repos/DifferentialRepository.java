/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.Differential;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface DifferentialRepository extends JpaRepository<Differential, Long>, JpaSpecificationExecutor<Differential> {

    List<Differential> findByReceiveUserIdAndOfferUserIdIn(Long receiveUserId, List<Long> offerUserId);

    List<Differential> findByReceiveUserIdAndOfferUserIdInAndDateGreaterThanEqualAndDateLessThanEqual(Long receiveUserId, List<Long> offerUserId, Date from, Date to);

    List<Differential> findByReceiveUserId(Long receiveUserId);

    List<Differential> findByEventIdAndMatchingOrderByReceiveUserIdDesc(Long eventId, boolean matching);

    List<Differential> findByEventIdAndReceiveUserIdInAndMatchingOrderByReceiveUserIdDesc(Long eventId, List<Long> receiveUserIds, boolean matching);

}
