/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Withdrawal;
import com.wings.backoffice.mlm.repos.WithdrawalRepository;
import com.wings.backoffice.utils.NumberUtils;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class WithdrawalService {

    @Autowired
    private WithdrawalRepository withdrawalRepository;

    public String getDetails(Member m, double amount) {

        Map<String, Double> temp;
        switch (m.getCountry()) {
            case "PE":
                temp = calcForPeru(amount);
                break;
            case "ES":
                temp = calcForSpain(amount, m);
                break;
            case "CO":
                temp = calcForColombia(amount);
                break;
            case "EC":
                temp = calcForEcuador(amount);
                break;
            default:
                temp = calcForSpain(amount, m);
        }

        StringBuilder b = new StringBuilder();
        temp.entrySet().forEach((entry) -> {
            String key = entry.getKey();
            Double value = entry.getValue();
            b.append(key).append(": ").append(NumberUtils.formatMoneyForCountryIso(value, m.getCountry())).append(System.lineSeparator());
        });

        return b.toString();
    }

    public Withdrawal fillForMember(Member m, Withdrawal w) {

        double amount = w.getAmount();
        double payable;
        double base;
        double tax;
        double retention;
        String details;

        Map<String, Double> temp;
        switch (m.getCountry()) {
            case "PE":
                temp = calcForPeru(amount);

                base = temp.get("Base imponible");
                payable = temp.get("Total a retirar");
                tax = temp.get("IVA 18%");
                retention = 0;
                details = getDetails(m, amount);
                break;

            case "CO":
                temp = calcForColombia(amount);

                base = temp.get("Base imponible");
                tax = temp.get("IVA 19%");
                retention = temp.get("Retenciones 11%");
                payable = temp.get("Total a retirar");
                details = getDetails(m, amount);

                break;
            case "EC":
                temp = calcForEcuador(amount);

                double ret1 = temp.get("Retenciones sobre base 2%");
                double ret2 = temp.get("Retenciones sobre IVA 70%");
                base = temp.get("Base imponible");
                tax = temp.get("IVA 12%");
                retention = ret1 + ret2;
                payable = temp.get("Total a retirar");
                details = getDetails(m, amount);
                break;

            default://ES
                temp = calcForSpain(amount, m);

                base = temp.get("Base imponible");
                tax = temp.get("IVA 21%");
                retention = temp.get("Retenciones 15%");
                payable = temp.get("Total a retirar");
                details = getDetails(m, amount);

                break;
        }

        w.setMoneyPayable(payable);
        w.setMoneyBase(base);
        w.setMoneyTax(tax);
        w.setMoneyRetention(retention);
        w.setDetails(details);

        return w;

    }

    public Map<String, Double> calcForColombia(Double amount) {

        Map<String, Double> ret = new LinkedHashMap<>();
        double iva = 19;
        double retention = 11;
        double ivaCalc = (iva / 100d);
        double retCalc = (retention / 100d);
        double base_imponible = amount / (ivaCalc + 1d);
        double montoIva = amount - base_imponible;
        double toPay = base_imponible / (retCalc + 1d);
        double retenciones = base_imponible - toPay;

        ret.put("Monto", amount);
        ret.put("IVA 19%", montoIva);
        ret.put("Base imponible", base_imponible);
        ret.put("Retenciones 11%", retenciones);
        ret.put("Total a retirar", toPay);

        return ret;

    }

    public Map<String, Double> calcForEcuador(Double amount) {

        Map<String, Double> ret = new LinkedHashMap<>();

        double iva = 12;
        double retention = 2;
        double retention2 = 70;

        double ivaCalc = (iva / 100d);
        double retCalc = (retention / 100d);
        double retCalc2 = (retention2 / 100d);

        double base_imponible = amount / (ivaCalc + 1d);
        double montoIva = amount - base_imponible;
        double retencion1 = base_imponible * retCalc;
        double retencion2 = montoIva / (retCalc2 + 1d);
        double toPay = amount - retencion1 - retencion2;

        ret.put("Monto", amount);
        ret.put("IVA 12%", montoIva);
        ret.put("Base imponible", base_imponible);
        ret.put("Retenciones sobre base 2%", retencion1);
        ret.put("Retenciones sobre IVA 70%", retencion2);
        ret.put("Total a retirar", toPay);

        return ret;
    }

    public Map<String, Double> calcForPeru(Double amount) {

        Map<String, Double> ret = new LinkedHashMap<>();
        double iva = 18;
        double ivaCalc = (iva / 100d);
        double base_imponible = amount / (ivaCalc + 1d);
        double montoIva = amount - base_imponible;

        ret.put("Monto", amount);
        ret.put("IVA 18%", montoIva);
        ret.put("Base imponible", base_imponible);
        ret.put("Total a retirar", amount);

        return ret;
    }

    public Map<String, Double> calcForSpain(Double amount, Member m) {

        Map<String, Double> ret = new LinkedHashMap<>();
        double iva = 21;
        double ivaCalc = (iva / 100d);
        double base_imponible = amount / (ivaCalc + 1d);
        double montoIva = amount - base_imponible;
        double retention = 0d;
        double payable;

        if (m.getDniType() != null && (m.getDniType().equals("NIF") || m.getDniType().equals("NIE"))) {
            retention = amount * 0.15;
            payable = amount - retention;
        } else {
            payable = amount;
        }

        ret.put("Monto", amount);
        ret.put("IVA 21%", montoIva);
        ret.put("Base imponible", base_imponible);
        ret.put("Retenciones 15%", retention);
        ret.put("Total a retirar", payable);

        return ret;
    }

}
