/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.ConfigItem;
import com.wings.backoffice.mlm.repos.ConfigRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class ConfigService {

    @Autowired
    private ConfigRepository configRepository;

    private final Map<String, String> items = new HashMap<>();

    public String getProperty(String key) {
        if (items.isEmpty()) {
            loadProperties();
        }
        return items.get(key);
    }

    public void setProperty(String key, String value) {
        items.put(key, value);
    }

    public void setPropertyAndSave(String key, String value) {
        items.put(key, value);
        save();
    }

    public void reload() {
        loadProperties();
    }

    private void save() {
        items.entrySet().forEach((entry) -> {
            String key = entry.getKey();
            String value = entry.getValue();
            ConfigItem item = configRepository.findByK(key);
            if (item == null) {
                item = new ConfigItem(key, value);
            } else {
                item.setV(value);
            }
            configRepository.save(item);
        });
    }

    private void loadProperties() {
        List<ConfigItem> citems = configRepository.findAll();
        citems.forEach((citem) -> {
            items.put(citem.getK(), citem.getV());
        });
    }

}
