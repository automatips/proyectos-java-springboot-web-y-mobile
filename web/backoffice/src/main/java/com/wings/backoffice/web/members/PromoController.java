/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import com.wings.backoffice.services.promos.PromoLatamService;
import com.wings.backoffice.utils.AuthUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class PromoController {

    @Autowired
    private PromoLatamService promoLatamService;

    @RequestMapping(value = {"/members/promos/promoLatam"}, method = {RequestMethod.GET})
    public String getPromoLatam(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();

        PromoLatam latam = promoLatamService.getPromoLatamForMember(m.getId());
        model.addAttribute("latam", latam);

        List<PromoLatam> items = promoLatamService.getTop20PromoLatam();
        model.addAttribute("items", items);

        return "members/promo/promoLatam";
    }

}
