package com.wings.backoffice.web.api.store;

import com.wings.api.models.StoreItemDto;
import com.wings.api.request.StoreItemRequest;
import com.wings.api.response.StoreItemResponse;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.services.ApiAuthService;
import com.wings.backoffice.services.StoreItemsService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "api/v1/")
public class StoreItemsController {

    @Autowired
    private StoreItemsService storeItemsService;
    @Autowired
    private ApiAuthService apiAuthService;

    /**
     *
     * @param id
     * @param request
     * @param q
     * @param page
     * @param size
     * @param country
     * @param category
     * @param subCategory
     * @return
     */
    @RequestMapping(value = {"items", "items/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<StoreItemResponse> getStoreItems(HttpServletRequest request,
            @PathVariable(required = false) Long id,
            @RequestParam(required = false, value = "q") String q,
            @RequestParam(required = false, value = "page") Integer page,
            @RequestParam(required = false, value = "size") Integer size,
            @RequestParam(required = false, value = "country") String country,
            @RequestParam(required = false, value = "category") String category,
            @RequestParam(required = false, value = "subCategory") String subCategory
    ) {
        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        if (page == null) {
            page = 0;
        }
        if (size == null) {
            size = 20;
        }

        if (id != null) {
            q = id.toString();
        }

        if (category != null && category.isEmpty()) {
            category = null;
        }

        Page<StoreItem> items;
        if (q != null && q.equals("popular")) {
            items = storeItemsService.searchItemsPopular(page, size, Boolean.TRUE, country);
        } else {
            items = storeItemsService.searchItems(q, page, size, Boolean.TRUE, category, null, country, Sort.Direction.ASC);
        }

        final StoreItemResponse response = new StoreItemResponse();
        final List<StoreItemDto> dtos = new ArrayList<>();
        items.getContent().stream().forEach(item -> {
            final StoreItemDto storeItemDto = storeItemsService.getSimpleDto(item, country);
            dtos.add(storeItemDto);
        });
        response.setItems(dtos);
        response.setCategory(category);
        response.setPage(page);
        response.setQ(q);
        response.setSize(size);
        response.setSubCategory(subCategory);
        response.setTotalElements(items.getTotalElements());
        response.setTotalPages(items.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "items/{id}/{country}", method = RequestMethod.GET)
    public ResponseEntity<StoreItemDto> getItem(HttpServletRequest httprequest,
            @PathVariable("id") Long itemId,
            @PathVariable("country") String country) {

        if (!apiAuthService.authRequest(httprequest)) {
            return ResponseEntity.badRequest().build();
        }

        StoreItem item = storeItemsService.getOne(itemId);
        if (item == null) {
            return ResponseEntity.badRequest().build();
        }

        if (!item.getCountries().contains(country.toUpperCase())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(storeItemsService.getSimpleDto(item, country));
    }

    @RequestMapping(value = "slugs/{slug}/{country}", method = RequestMethod.GET)
    public ResponseEntity<StoreItemDto> getItemSlug(HttpServletRequest httprequest,
            @PathVariable("slug") String slug,
            @PathVariable("country") String country) {

        if (!apiAuthService.authRequest(httprequest)) {
            return ResponseEntity.badRequest().build();
        }

        StoreItem item = storeItemsService.getOneByCode(slug);
        if (item == null) {
            return ResponseEntity.badRequest().build();
        }

        if (!item.getCountries().contains(country.toUpperCase())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(storeItemsService.getSimpleDto(item, country));
    }

    /**
     *
     * @param httprequest
     * @param request
     * @return
     */
    @RequestMapping(value = "items/popular", method = RequestMethod.POST)
    public ResponseEntity<StoreItemResponse> getMostPopular(HttpServletRequest httprequest, @RequestBody StoreItemRequest request) {

        if (!apiAuthService.authRequest(httprequest)) {
            return ResponseEntity.badRequest().build();
        }

        final StoreItemResponse response = new StoreItemResponse();
        final List<StoreItemDto> dtos = new ArrayList<>(20);
        List<StoreItem> searchItems = storeItemsService.searchItemsAsList(null, true, "product", null, null);

        searchItems.stream().forEach(item -> {
            final StoreItemDto storeItemDto = storeItemsService.getSimpleDto(item, request.getCountry());
            dtos.add(storeItemDto);
        });

        response.setItems(dtos);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "items/kits", method = RequestMethod.POST)
    public ResponseEntity<StoreItemResponse> getKits(HttpServletRequest httprequest, @RequestBody StoreItemRequest request) {

        if (!apiAuthService.authRequest(httprequest)) {
            return ResponseEntity.badRequest().build();
        }

        final StoreItemResponse response = new StoreItemResponse();
        final List<StoreItemDto> dtos = new ArrayList<>(20);
        List<StoreItem> searchItems = storeItemsService.searchItemsAsList(null, true, "kit", null, null);

        searchItems.stream().forEach(item -> {
            final StoreItemDto storeItemDto = storeItemsService.getSimpleDto(item, request.getCountry());
            dtos.add(storeItemDto);
        });

        response.setItems(dtos);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "items/{id}", method = RequestMethod.POST)
    public ResponseEntity<StoreItemResponse> postItem(HttpServletRequest httprequest, @PathVariable Long id, @RequestBody StoreItemRequest request) {

        if (!apiAuthService.authRequest(httprequest)) {
            return ResponseEntity.badRequest().build();
        }

        final StoreItemResponse response = new StoreItemResponse();
        final StoreItem item = storeItemsService.getOne(id);

        final StoreItemDto storeItemDto = storeItemsService.getFullDto(item, request.getCountry());
        //response.setItem(storeItemDto);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
