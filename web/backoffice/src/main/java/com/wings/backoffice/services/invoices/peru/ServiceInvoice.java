/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.invoices.peru;

import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.domain.bis.invoices.InvoiceItem;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.utils.NumberUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * This class uses REST API to create a sunat invoice and get PDF report
 *
 * @author seba
 */
@Service
public class ServiceInvoice {

    private String urlApi = "https://api.contadorsos.com/api_facturacion/";

    public void getInvoice(Order order, Invoice invoice) {

        InvoiceDTO dto = new InvoiceDTO();

        boolean fatura = true;

        String url = urlApi + "boleta.php";
        dto.setSerieComprobante("B0001");
        dto.setClienteTipoDocumento("1");
        dto.setCodTipoDocumento("03");
        if (fatura) {
            url = urlApi + "factura.php";
            dto.setSerieComprobante("F0001");
            //1 DNI, 6 RUC para factura
            dto.setClienteTipoDocumento("6");
            dto.setCodTipoDocumento("01");
        }

        dto.setNumeroComprobante(invoice.getInvoiceNumber());
        dto.setClienteCiudad(invoice.getClientCity());
        dto.setClienteCodigoUbigeo("");
        dto.setClienteDepartamento(invoice.getClientState());
        dto.setClienteDireccion(invoice.getClientAddress());
        dto.setClienteDistrito("");
        dto.setClienteNombre(invoice.getClientName());
        dto.setClienteNumeroDocumento(invoice.getClientDni());
        dto.setClientePais("Peru");
        dto.setClienteProvincia(invoice.getClientState());

        dto.setCodGuiaRemision("");
        dto.setCodMoneda("PEN");
        dto.setFechaComprobante(invoice.getInvoiceDate());
        dto.setFechaVtoComprobante(invoice.getInvoiceDate());

        dto.setNroGuiaRemision("");
        dto.setNroOtrComprobante("");
        dto.setNumeroComprobante(invoice.getId().toString());
        dto.setPorcentajeIgv("18.00");        
        dto.setSubTotal(String.valueOf(NumberUtils.unFormatMoney(invoice.getImponible(), "PEN")));        
        dto.setTipoOperacion("0101");
        dto.setTotal(String.valueOf(invoice.getTotal()));
        dto.setTotalDescuento("0");
        dto.setTotalExoneradas("0");
        dto.setTotalExportacion("0");
        dto.setTotalGratuitas("0");
        dto.setTotalGravadas("0");
        dto.setTotalIcbPer(0);
        dto.setTotalIgv(String.valueOf(NumberUtils.unFormatMoney(invoice.getIva(), "PEN")));
        dto.setTotalInafecta("0");
        dto.setTotalIsc("0");
        
        /*System.out.println(invoice.getTotal());
        System.out.println(NumberUtils.convertNumberToLetter(invoice.getTotal()));        
        dto.setTotalLetras(NumberUtils.convertNumberToLetter(invoice.getTotal()));
        dto.setTotalOtrImp("");*/

        Emisor emisor = new Emisor();
        emisor.setCodigoUbigeo("");
        emisor.setRazonSocial("WINGS MOBILE PERU SAC");
        emisor.setNomComercial("Wings Mobile");
        emisor.setDireccion("MZA. D-1 LOTE. 2 DPTO. 101 URB. SANTA PATRICIA 1ERA ETAPA");
        emisor.setDireccionCodigoPais("PE");
        emisor.setDireccionDepartamento("Lima");
        emisor.setDireccionDistrito("LA MOLINA");
        emisor.setDireccionProvincia("Lima");
        emisor.setRuc("20603544898");
        emisor.setTipoDoc("6");
        emisor.setUsuarioSol("MODDATOS");
        emisor.setClaveSol("moddatos");

        dto.setEmisor(emisor);

        List<Item> detalle = new ArrayList<>();
        dto.setDetalle(detalle);

        List<InvoiceItem> items = invoice.getItems();
        for (InvoiceItem i : items) {
            Item item = new Item();
            item.setTextCodigoDet(i.getCode());
            item.setTextDescripcionDet(i.getConcept());
            item.setIcbperDet(0);
            item.setTextCantidadDet("1");
            item.setTextCodigoProdSUNAT("23251602");
            item.setTextCodigoTipoOperacion("");
            item.setTextIGV(String.valueOf(NumberUtils.unFormatMoney(invoice.getIva(), "PEN")));
            item.setTextISC("0");
            item.setTextItem(i.getId().intValue());
            item.setTextImporteDet(String.valueOf(i.getAmount()));
            item.setTextPrecioDet(String.valueOf(i.getAmount()));
            item.setTextPrecioSinIGVDet(String.valueOf(NumberUtils.unFormatMoney(invoice.getImponible(), "PEN")));
            item.setTextSubTotalDet(String.valueOf(i.getAmount()));
            item.setTextPrecioTipoCodigo("01");
            item.setTextUnidadMedidaDet("NIU");
            detalle.add(item);
        }

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //RestTemplate template = new RestTemplate();        
        //ResponseEntity<String> result = template.postForEntity(url, dto, String.class);
        //System.out.println(result.getBody());

    }

}
