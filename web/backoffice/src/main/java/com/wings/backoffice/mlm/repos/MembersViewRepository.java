package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.MembersView;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author lucas
 */
public interface MembersViewRepository extends ViewRepository<MembersView, Long> {
    @Query(value = "SELECT * FROM vw_members WHERE country_final = :country AND (registration_date >= :from AND registration_date <= :to OR activation_date >= :from AND activation_date <= :to)", nativeQuery = true)
    List<MembersView> findByBillingCountryAndPeriod(@Param("country") String country, @Param("from") Date from, @Param("to") Date to);

    @Query(value = "SELECT * FROM vw_members WHERE country_final = :country AND (MONTH(registration_date) IN (:months) AND YEAR(registration_date) IN (:years) OR MONTH(activation_date) IN (:months) AND YEAR(activation_date) IN (:years))", nativeQuery = true)
    List<MembersView> findByBillingCountryAndMonth(@Param("country") String country, @Param("months") List<String> months, @Param("years") List<String> years);
}