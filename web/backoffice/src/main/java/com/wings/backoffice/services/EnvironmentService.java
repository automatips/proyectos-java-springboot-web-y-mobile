/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class EnvironmentService {

    @Autowired
    private Environment environment;

    private boolean traceOn = false;

    public boolean isDevEnvironment() {
        boolean ret = false;
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (!profile.contains("prod")) {
                ret = true;
                break;
            }
        }
        return ret;
    }

    public void setTraceOn(boolean traceOn) {
        this.traceOn = traceOn;
    }

    public boolean isTraceOn() {
        return traceOn;
    }

}
