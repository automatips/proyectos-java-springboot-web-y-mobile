/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.services.CalificationService;
import com.wings.backoffice.services.MembersService;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class MembersLoginController {

    @Autowired
    private MembersService membersService;
    @Autowired
    private CalificationService calificationService;

    /**
     *
     * @param model
     * @param request
     * @param values
     * @return
     */
    @RequestMapping(value = {"/api/members/login", "/api/v1/login"}, method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> apiLogin(Model model, HttpServletRequest request, @RequestBody Map<String, String> values) {

        Map<String, String> ret = new HashMap<>();
        ret.put("login", "false");

        String username = values.get("username");
        String password = values.get("password");

        Member m = membersService.findByUsername(username);
        if (m == null) {
            m = membersService.findByEmail(username);
        }

        if (m == null) {
            ret.put("error", "Wrong username or password");
            return ResponseEntity.ok(ret);
        }

        if (!m.getPassword().equals(password)) {
            ret.put("error", "Wrong username or password");
            return ResponseEntity.ok(ret);
        }

        ret.put("login", "true");
        ret.put("username", m.getUsername());
        ret.put("firstName", m.getFirstName());
        ret.put("lastName", m.getLastName());
        ret.put("email", m.getEmail());
        ret.put("rank", m.getRank().getName());
        ret.put("id", m.getId().toString());
        ret.put("sponsorId", m.getSponsorId().toString());
        ret.put("points", String.valueOf(calificationService.getCalificationPoints(m.getId())));

        return ResponseEntity.ok(ret);
    }

}
