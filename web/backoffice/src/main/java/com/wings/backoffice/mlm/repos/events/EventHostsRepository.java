/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventHost;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface EventHostsRepository extends JpaRepository<EventHost, Long>, JpaSpecificationExecutor<EventHost> {

    EventHost findByEmail(String email);

    List<EventHost> findByIdNotIn(List<Long> excludeIds);

}
