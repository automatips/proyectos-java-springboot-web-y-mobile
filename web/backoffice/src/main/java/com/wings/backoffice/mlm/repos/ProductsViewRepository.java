package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.ProductsView;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author lucas
 */
public interface ProductsViewRepository extends ViewRepository<ProductsView, Long> {    
    @Query(value = "SELECT * FROM vw_ordersActives WHERE billing_country = :country AND activation_date >= :from and activation_date <= :to", nativeQuery = true)
    List<ProductsView> findByBillingCountryAndPeriod(@Param("country") String country, @Param("from") Date from, @Param("to") Date to );
    
    @Query(value = "SELECT * FROM vw_ordersActives WHERE billing_country = :country", nativeQuery = true)
    List<ProductsView> findByBillingCountry(@Param("country") String country);
    
    @Query(value = "SELECT * FROM vw_ordersActives WHERE billing_country = :country AND MONTH(activation_date) IN (:months) AND YEAR(activation_date) IN (:years)", nativeQuery = true)
    List<ProductsView> findByBillingCountryAndMonth(@Param("country") String country, @Param("months") List<String> months, @Param("years") List<String> years);
}
