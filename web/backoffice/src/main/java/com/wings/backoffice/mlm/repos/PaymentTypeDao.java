/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.PaymentType;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author maurib
 */
public interface PaymentTypeDao extends CrudRepository<PaymentType, Long> {
    
}
