/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.maxmind.db.InvalidDatabaseException;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class GeoIpService {

    private final Logger log = LoggerFactory.getLogger(GeoIpService.class);
    private DatabaseReader reader;

    public GeoIpService() {
        File database = new File("/var/wings/bo_media/GeoLite2-Country.mmdb");
        try {
            reader = new DatabaseReader.Builder(database).build();
        } catch (IOException e) {
            log.error("Error initializing geo ip service", e);
        }
    }

    public String getIsoForIp(String ip) {

        String ret = "none";
        //skip localhost
        if (ip.startsWith("127")) {
            return ret;
        }

        if (ip.contains(",")) {
            String[] ips = ip.split(",");
            ip = ips[ips.length - 1];
            ip = ip.trim();
        }

        if (reader != null) {
            try {
                InetAddress ipAddress = InetAddress.getByName(ip);
                CountryResponse response = reader.country(ipAddress);
                ret = response.getCountry().getIsoCode().toLowerCase();
            } catch (InvalidDatabaseException e) {
                try {
                    File database = new File("/var/wings/bo_media/GeoLite2-Country.mmdb");
                    reader = new DatabaseReader.Builder(database).build();
                } catch (IOException ex) {
                    log.error("Error reloading database");
                }
            } catch (GeoIp2Exception | IOException e) {
                log.error("Error finding country " + ip);
            } catch (Exception e) {
                log.error("Error finding country " + ip);
            }
        }
        return ret;
    }

    public String getIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}
