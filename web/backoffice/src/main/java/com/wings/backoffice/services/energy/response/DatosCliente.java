
package com.wings.backoffice.services.energy.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "direccion",
    "direccionNumero",
    "codigoPostal",
    "poblacion",
    "provincia"
})
public class DatosCliente {

    @JsonProperty("direccion")
    private String direccion;
    @JsonProperty("direccionNumero")
    private String direccionNumero;
    @JsonProperty("codigoPostal")
    private String codigoPostal;
    @JsonProperty("poblacion")
    private String poblacion;
    @JsonProperty("provincia")
    private String provincia;

    @JsonProperty("direccion")
    public String getDireccion() {
        return direccion;
    }

    @JsonProperty("direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @JsonProperty("direccionNumero")
    public String getDireccionNumero() {
        return direccionNumero;
    }

    @JsonProperty("direccionNumero")
    public void setDireccionNumero(String direccionNumero) {
        this.direccionNumero = direccionNumero;
    }

    @JsonProperty("codigoPostal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    @JsonProperty("codigoPostal")
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @JsonProperty("poblacion")
    public String getPoblacion() {
        return poblacion;
    }

    @JsonProperty("poblacion")
    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    @JsonProperty("provincia")
    public String getProvincia() {
        return provincia;
    }

    @JsonProperty("provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

}
