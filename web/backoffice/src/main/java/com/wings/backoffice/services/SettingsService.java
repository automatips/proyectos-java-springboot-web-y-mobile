/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SettingsService {

    @Autowired
    private CountriesService countriesService;

    public List<String> getCountryCodes() {
        return countriesService.getEnabledCountries();
    }

    public List<String> getCategories() {
        return Arrays.asList("kit", "product", "store", "upgrade", "ticket", "service", "topup");
    }

    public List<String> getSubCategories() {
        return Arrays.asList("phone", "computer", "smartwatch", "tablet", "accessory", "gsm", "voip");
    }

    public Map<String, Double> getCountryInvoiceParams(String country) {

        Double tax = 0.0;
        switch (country) {
            case "CO":
                break;
            case "EC":
                break;
            case "PE":
                tax = 1.18;
                break;
            case "MX":
                tax = 1.16;
                break;
            case "ES":
                tax = 1.21;
                break;
        }

        Map<String, Double> ret = new HashMap<>();
        ret.put("tax", tax);
        return ret;
    }

    public Double getCountryTax(String country) {

        Double tax = 0.0;
        switch (country) {
            case "CO":
                tax = 1.19;
                break;
            case "EC":
                tax = 1.12;
                break;
            case "PE":
                tax = 1.18;
                break;
            case "MX":
                tax = 1.16;
                break;
            case "ES":
                tax = 1.21;
                break;
        }
        return tax;
    }

}
