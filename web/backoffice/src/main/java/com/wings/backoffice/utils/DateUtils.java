/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import com.ibm.icu.text.SimpleDateFormat;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author seba
 */
public class DateUtils {

    private static final SimpleDateFormat NORMAL_DATE = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat NORMAL_DATETIME = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static final SimpleDateFormat COMPUTER_DATETIME = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat MYSQL_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat BROWSER_DATETIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    private static final SimpleDateFormat MONTH_NAME = new SimpleDateFormat("MMMM");

    /**
     * Returns days bettween from and today
     *
     * @param from
     * @return
     */
    public static long getDiffDays(Date from) {
        long timeFrom = from.getTime();
        long timeTo = new Date().getTime();
        long ret = timeTo - timeFrom;
        return TimeUnit.DAYS.convert(ret, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns days bettween from and to
     *
     * @param from
     * @param to
     * @return
     */
    public static long getDiffDays(Date from, Date to) {
        long timeFrom = from.getTime();
        long timeTo = to.getTime();
        long ret = timeTo - timeFrom;
        return TimeUnit.DAYS.convert(ret, TimeUnit.MILLISECONDS);
    }

    public static String formatNormalDate(Date date) {
        return date != null ? NORMAL_DATE.format(date) : "";
    }

    public static String formatComputerDate(Date date) {
        return date != null ? COMPUTER_DATETIME.format(date) : "";
    }

    public static String formatNormalDateTime(Date date) {
        return date != null ? NORMAL_DATETIME.format(date) : "";
    }

    public static Date getStartDateForPeriod(int month, int year) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public static Date getEndDateForPeriod(int month, int year) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, 15);
        cal.set(Calendar.MONTH, month - 1);
        int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public static Date getStartDateCurrentMonth() {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public static Date getEndDateCurrentMonth() {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.DAY_OF_MONTH, 15);
        int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public static String getMonthName(int month) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.MONTH, month - 1);
        return MONTH_NAME.format(cal.getTime());
    }

    public static Boolean dateInRange(Date toCheck, Date rangeFrom, Date rangeTo) {
        return toCheck.after(rangeFrom) && toCheck.before(rangeTo);
    }

    public static String elapsedTime(Date from) {
        final long l = System.currentTimeMillis() - from.getTime();
        final long hr = TimeUnit.MILLISECONDS.toHours(l);
        final long min = TimeUnit.MILLISECONDS.toMinutes(l - TimeUnit.HOURS.toMillis(hr));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        return String.format("%02d:%02d:%02d", hr, min, sec);
    }

    public static long elapsedTimeInSeconds(Date from) {
        final long l = System.currentTimeMillis() - from.getTime();
        final long sec = TimeUnit.MILLISECONDS.toSeconds(l);
        return sec;
    }

    /**
     * Given a date, returns same date with time 00:00:00
     *
     * @param date
     * @return
     */
    public static Date getFirstHourOfDay(Date date) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * * Given a date, returns same date with time 23:59:59
     *
     * @param date
     * @return
     */
    public static Date getLastHourOfDay(Date date) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public static int getDateYear(Date date) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    /**
     * parse String to Date in normal format dd/MM/yyyy HH:mm:ss
     *
     * @param normalString
     * @return
     */
    public static Date fromNormalStringToDate(String normalString) {
        try {
            return NORMAL_DATETIME.parse(normalString);
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * parse String to Date in mysql format yyyy-MM-dd HH:mm:ss
     *
     * @param mysqlString
     * @return
     */
    public static Date fromMysqlStringToDate(String mysqlString) {
        try {
            return MYSQL_DATETIME.parse(mysqlString);
        } catch (Exception e) {
        }
        return new Date();
    }

    public static String fromDateToMysqlString(Date date) {
        return MYSQL_DATETIME.format(date);
    }

    /**
     * parse String to Date in mysql format yyyy-MM-ddTHH:mm
     *
     * @param browserDateTime
     * @return
     */
    public static Date fromBrowserStringToDate(String browserDateTime) {
        try {
            return BROWSER_DATETIME.parse(browserDateTime);
        } catch (Exception e) {
        }
        return new Date();
    }
    
    public static String getMonthName(Integer month){
        Month name = Month.of(month);
        return StringUtils.capitalizeFirst(name.getDisplayName(TextStyle.FULL, new Locale("es", "ES")));
    }
}
