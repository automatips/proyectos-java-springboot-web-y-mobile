/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api;

import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.services.BalanceService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.Date;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ComsController {

    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private BalanceRepository balanceRepository;

    private final String salt = "Eeop8pxYqtaa9mTH";

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/payments/coms/charge"}, method = RequestMethod.POST)
    public ResponseEntity<String> chargeMember(HttpServletRequest request) {

        String username = request.getParameter("username");
        String authToken = request.getParameter("auth_token");

        if (username == null) {
            return ResponseEntity.ok("No autorizado");
        }

        if (authToken == null) {
            return ResponseEntity.ok("No autorizado");
        }

        Member m = membersRepository.findByUsername(username);
        if (m == null) {
            m = membersRepository.findFirstByEmail(username);
        }

        if (m == null) {
            return ResponseEntity.ok("No autorizado (326)");
        }

        String mpass = m.getPassword();
        String myAuth = DigestUtils.sha256Hex(username + salt + mpass);

        if (!myAuth.equals(authToken)) {
            return ResponseEntity.ok("No autorizado, token incorrecto!.");
        }

        if (!m.isPaymentsEnabled()) {
            return ResponseEntity.ok("Pagos con comisiones no habilitado.");
        }

        String samount = request.getParameter("amount");
        String orderId = request.getParameter("order_id");
        String requester = request.getParameter("requester");

        //if requester is null, return error
        if (requester == null) {
            return ResponseEntity.ok("Solicitante no autorizado.");
        }

        //verificar que la orden exista
        long oid;
        try {
            oid = Long.parseLong(orderId);
        } catch (Exception e) {
            return ResponseEntity.ok("Orden no encontrada.");
        }

        //verificar que la orden exista
        Order o = ordersService.getOrder(oid);
        if (o == null) {
            return ResponseEntity.ok("Orden no encontrada.");
        }

        //verifico que no este paga
        if (o.getStatus().equals(OrderStatus.paid.name())) {
            return ResponseEntity.ok("La orden ya esta paga!.");
        }

        //este monto viene en moneda local
        Double amount;
        try {
            amount = Double.parseDouble(samount);
        } catch (NumberFormatException e) {
            return ResponseEntity.ok("Monto incorrecto");
        }

        //verifico si tiene saldo
        BalanceSums sums = balanceService.getMemberBalance(m.getId());
        Double effective = sums.getEffective(); //si tiene saldo, creo el balance y respondo ok

        //transformo a euros
        Locale locale = CountryUtils.getLocaleForCountry(o.getBillingCountry());
        double realAmount = NumberUtils.convertMoneyToEuros(amount, locale);
        if (realAmount > effective) {
            return ResponseEntity.ok("Fondos insuficientes");
        }

        switch (requester) {
            case "backoffice":

                //verifico que el monto no sea menor al de la orden
                if (Math.abs(o.getPaymentAmount() - amount) > 1) {
                    if (o.getPriceForComs() == null) {
                        return ResponseEntity.ok("El monto de la orden difiere del pago");
                    } else {
                        if (Math.abs(o.getPriceForComs() - amount) > 1) {
                            return ResponseEntity.ok("El monto de la orden difiere del pago");
                        }
                    }
                }

                try {
                    Date creatioDate = new Date();
                    Balance b = new Balance();
                    b.setAmount(realAmount * -1);
                    b.setControl("payments-" + orderId);
                    b.setCreationDate(creatioDate);
                    b.setDescription("Pago con comisiones " + DateUtils.formatNormalDateTime(creatioDate) + " (#" + orderId + ") " + o.getFullName() + " - " + o.getStoreTitle());
                    b.setMemberId(m.getId());
                    b.setReferenceId(Long.parseLong(orderId));
                    b.setOrderId(Long.parseLong(orderId));
                    b.setEffective(true);
                    b.setStatus(BalanceStatus.effective.name());
                    b.setType(BalanceType.discount.name());
                    b.setComment("Auto balance created from payment request - effective by default");
                    balanceRepository.save(b);

                    o.setComment("Payment using  coms, balance id " + b.getId() + " " + new Date() + " " + m.toString() + " " + (o.getComment() != null ? o.getComment() : ""));
                    ordersService.save(o);

                    return ResponseEntity.ok("Transaccion aceptada");

                } catch (Exception e) {
                    return ResponseEntity.ok("Error " + e.getMessage());
                }
            case "movil":
                Date creatioDate = new Date();
                Balance b = new Balance();
                b.setAmount(realAmount * -1);
                b.setControl("payments-" + orderId);
                b.setCreationDate(creatioDate);
                b.setDescription("Pago con comisiones factura móvil " + DateUtils.formatNormalDateTime(creatioDate) + " (#" + orderId + ") ");
                b.setMemberId(m.getId());
                b.setReferenceId(Long.parseLong(orderId));
                b.setOrderId(Long.parseLong(orderId));
                b.setEffective(true);
                b.setStatus(BalanceStatus.effective.name());
                b.setType(BalanceType.discount.name());
                b.setComment("Auto balance created from payment request - effective by default");
                balanceRepository.save(b);
                return ResponseEntity.ok("Transaccion aceptada");
            case "voip":
                break;
            default:
                return ResponseEntity.ok("Solicitante no autorizado.");
        }

        return ResponseEntity.ok("Error generando cargos.");
    }

}
