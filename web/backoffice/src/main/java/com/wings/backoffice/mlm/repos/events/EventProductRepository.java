/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventProduct;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface EventProductRepository extends JpaRepository<EventProduct, Long> {

    Page<EventProduct> findAllByEventId(Long eventId, Pageable page);

    List<EventProduct> findByEventId(Long eventId);

    List<EventProduct> findByEventIdAndStatus(Long eventId, String status);

    int countByEventId(Long eventId);

    List<EventProduct> findByCountryAndEventId(String country, Long eventId);

    EventProduct findFirstByEventIdOrderByIdDesc(Long eventId);

    EventProduct findByStoreItemId(Long storeItemId);

    @Transactional
    int deleteByEventId(Long eventId);

}
