/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.open;

import com.wings.backoffice.services.MediaService;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class MediaController {

    @Autowired
    private MediaService mediaService;

    /**
     *
     * @param response
     * @param code
     *
     */
    @ResponseBody
    @RequestMapping(value = "/open/media/{code}", method = RequestMethod.GET)
    //public ResponseEntity<InputStreamResource> downloadUserAvatarImage(HttpServletResponse response, @PathVariable("code") String code) {
    public void downloadUserAvatarImage(HttpServletResponse response, @PathVariable("code") String code) {

        try {
            String path = mediaService.getUserAvatar(code);
            if (path == null || path.isEmpty()) {
                //return ResponseEntity.status(404).build();
                return;
            }

            File f = new File(path);
            FileInputStream fis = new FileInputStream(f);

            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            response.setContentLengthLong(f.length());

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

            //return ResponseEntity.ok().contentLength(f.length()).contentType(MediaType.IMAGE_JPEG).body(new InputStreamResource(fis));
        } catch (Exception e) {
        }
    }

}
