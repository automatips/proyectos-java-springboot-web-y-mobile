package com.wings.backoffice.web.api.store;

import com.wings.api.models.CountryDto;
import com.wings.api.response.CountryResponse;
import com.wings.backoffice.services.ApiAuthService;
import com.wings.backoffice.services.CountriesService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Lucas
 */
@Controller
@RequestMapping(value = "api/v1/")
public class CountryApiController {

    @Autowired
    private CountriesService countriesService;
    @Autowired
    private ApiAuthService apiAuthService;

    /**
     * @param request
     * @return
     */
    @RequestMapping(value = "countries", method = RequestMethod.GET)
    public ResponseEntity<CountryResponse> getAll(HttpServletRequest request) {

        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        CountryResponse resp = new CountryResponse();
        resp.setCountries(countriesService.getCountriesDtoAvailableForStore());
        return ResponseEntity.ok(resp);
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping(value = "countries/other", method = RequestMethod.GET)
    public ResponseEntity<CountryResponse> getAllOthers(HttpServletRequest request) {

        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        CountryResponse resp = new CountryResponse();
        resp.setCountries(countriesService.getDisabledCountries());
        return ResponseEntity.ok(resp);
    }

    /**
     *
     * @param request
     * @param code
     * @return
     */
    @RequestMapping(value = "countries/{code}", method = RequestMethod.GET)
    public ResponseEntity<CountryDto> getOne(HttpServletRequest request, @PathVariable("code") String code) {

        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(countriesService.getCountryDto(code));
    }

}
