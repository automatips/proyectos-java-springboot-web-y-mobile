/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "bis_sims")
public class Sim implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    
    @Column(name = "id")
    private Long id;
    @Size(max = 20)
    
    @Column(name = "iccid")
    private String iccid;
    @Size(max = 100)
    
    @Column(name = "imsi")
    private String imsi;
    @Size(max = 100)
    
    @Column(name = "country")
    private String country;
    @Size(max = 100)
    
    @Column(name = "operator")
    private String operator;
    @Size(max = 100)
    
    @Column(name = "active")
    private boolean active;
    
    @Column(name = "enabled")
    private boolean enabled;   

    public Sim() {
    }

    public Sim(Long id) {
        this.id = id;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String i) {
        this.iccid = i;
    }
    
    public String getImsi()
    {
        return imsi;
    }
    
    public void setImsi(String i)
    {
        this.imsi = i;
    }
    
    public String getCountry()
    {
        return country;
    }

    public void setCountry(String c)
    {
        this.country = c;
    }
    
    public String getOperator()
    {
        return operator;
    }
    
    public void setOperator(String o)
    {
        this.operator = o;
    }
    
    public boolean getActive()
    {
        return active;
    }
    
    public void setActive(boolean a)
    {
        this.active = a;
    }
    
    public boolean getEnabled()
    {
        return enabled;
    }
    
    public void setEnabled(boolean e)
    {
        this.enabled = e;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sim)) {
            return false;
        }
        Sim other = (Sim) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
