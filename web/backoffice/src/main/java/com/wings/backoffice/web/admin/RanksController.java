/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.dao.CalificationDAO;
import com.wings.backoffice.mlm.dao.CalificationSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.RankHistory;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.RankHistoryRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.mlm.specs.CalificationSpecs;
import com.wings.backoffice.mlm.specs.RankHistorySpecs;
import com.wings.backoffice.services.CalificationService;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class RanksController {

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private RanksBisRepository ranksBisRepository;

    @Autowired
    private RankHistoryRepository rankHistoryRepository;

    @Autowired
    private CalificationPointsRepository calificationPointsRepository;

    @Autowired
    private CalificationService calificationService;

    @Autowired
    private CalificationDAO calificationDAO;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = {"/admin/ranks/list"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {

        List<RankBis> ranks = ranksBisRepository.findAll();
        model.addAttribute("items", ranks);

        return "admin/ranks/list";
    }

    @RequestMapping(value = {"/admin/ranks/calification"}, method = RequestMethod.GET)
    public String getCalificationList(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "date"));
        Page<CalificationPoint> items = calificationPointsRepository.findAll(CalificationSpecs.search(q, type, f, t), pr);
        CalificationSums sums = calificationDAO.find(q, type, f, t);

        PageWrapper<CalificationPoint> pageWrapper = new PageWrapper<>(items, "admin/ranks/calification");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("size", ssize);
        model.addAttribute("sums", sums);
        model.addAttribute("type", type);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/ranks/calification";
    }

    @RequestMapping(value = {"/admin/ranks/history"}, method = RequestMethod.GET)
    public String ranksHistory(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String auto = request.getParameter("auto");

        Boolean bauto = null;
        if (auto != null && !auto.equals("all")) {
            bauto = auto.equals("true");
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "date"));
        Page<RankHistory> items = rankHistoryRepository.findAll(RankHistorySpecs.search(q, bauto), pr);

        PageWrapper<RankHistory> pageWrapper = new PageWrapper<>(items, "admin/ranks/history");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("auto", auto);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        return "admin/ranks/history";
    }

    @RequestMapping(value = {"/admin/ranks/report"}, method = RequestMethod.GET)
    public String getReport(Model model, HttpServletRequest request) {

        List<Member> members = membersRepository.findAll();

        StringBuilder line = new StringBuilder();

        line.append("Afiliados de tercera linea para abajo con rango mayor a dealer").append(System.lineSeparator());
        line.append(System.lineSeparator());
        for (Member member : members) {
            if (member.getSponsorId() != null && member.getSponsorId() > 3 && member.getRank().getId() > 1) {

                line.append(member.getNameText()).append(System.lineSeparator());
                line.append("\tRango:").append(member.getRank().getName()).append(System.lineSeparator());
                line.append("\tSponsor:").append(member.getSponsor().getNameText()).append(System.lineSeparator());
                line.append("-----------------------------------------------------------------").append(System.lineSeparator());
            }
        }

        model.addAttribute("line", line.toString());

        return "admin/ranks/report";
    }

    @RequestMapping(value = {"/admin/ranks/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deleteRankHistory(Model model, HttpServletRequest request, @PathVariable("id") Long rankHistoryId) {

        try {
            rankHistoryRepository.delete(rankHistoryId);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error eliminado el historial de rango");
        }

        return ResponseEntity.ok("Rango eliminado correctamente");
    }

}
