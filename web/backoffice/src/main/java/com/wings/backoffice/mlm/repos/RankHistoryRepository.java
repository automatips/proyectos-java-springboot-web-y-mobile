/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.RankHistory;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface RankHistoryRepository extends JpaRepository<RankHistory, Long>, JpaSpecificationExecutor<RankHistory> {

    RankHistory findByMemberId(Long memberId);

    //RankHistory findFirstByMemberIdAndDateGreaterThanOrderByDateAsc(Long memberId, Date date);    
    RankHistory findFirstByMemberIdAndDateLessThanEqualOrderByDateDesc(Long memberId, Date date);

    List<RankHistory> findByAuto(Boolean auto);

    List<RankHistory> findByAutoAndMemberId(Boolean auto, Long memberId);

    RankHistory findFirstByMemberIdAndDateLessThanEqualAndAutoOrderByDateDesc(Long memberId, Date date, Boolean auto);

}
