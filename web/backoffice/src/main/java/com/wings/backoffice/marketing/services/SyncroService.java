/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.marketing.services;

import com.wings.backoffice.marketing.domain.MktUser;
import com.wings.backoffice.marketing.repos.MktUserRespository;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.repos.MembersRepository;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SyncroService {

    @Autowired
    private MktUserRespository mktUserRespository;

    @Autowired
    private MembersRepository membersRepository;

    public void syncAllUsers() {
        List<Member> members = membersRepository.findAll();
        members.forEach((member) -> {
            syncMember(member.getId());
        });
    }

    public void syncMember(Long memberId) {

        Member m = membersRepository.findOne(memberId);
        MktUser mkt = transform(m);
        mktUserRespository.save(mkt);
        String url = "https://marketing.wingsmobile.net/id/login_auto/" + mkt.getId() + "/" + mkt.getAutopass();
        m.setMarketingUrl(url);
        membersRepository.save(m);

    }

    private MktUser transform(Member member) {
        MktUser ret = new MktUser();
        
        ret.setActivationCode("");
        ret.setActive(Boolean.TRUE);
        ret.setAdmin(0);
        ret.setUsername(member.getUsername());
        ret.setFirstName(member.getFirstName());
        ret.setLastName(member.getLastName());
        ret.setAutopass(DigestUtils.sha256Hex(member.getPassword() + System.currentTimeMillis()));
        ret.setEmail(member.getEmail());
        ret.setPais(member.getCountry());

        ret.setAffiliateName("");
        ret.setAnalytics("");
        ret.setAutoresponderOk(Boolean.FALSE);
        ret.setC2s("");
        ret.setCampaignCode("");
        ret.setCampaignName("");
        ret.setCircle("");
        ret.setClickbank("");
        ret.setCoinbase("");
        ret.setConfirm(Boolean.TRUE);
        ret.setCountry("");
        ret.setCreatedOn(0);
        ret.setCuponGenesismining("");
        ret.setDatefull(Integer.BYTES);
        ret.setDatetrial(Integer.BYTES);
        ret.setEmprendedora("");
        ret.setEncuestaAct(Boolean.FALSE);
        ret.setEncuestaActDate("");
        ret.setEncuestaOk(Boolean.TRUE);
        ret.setFacebook("");
        ret.setForgottenPasswordCode("");
        ret.setForgottenPasswordTime(Integer.SIZE);
        ret.setGetresponse("");
        ret.setGustaria("");
        ret.setGvo("");
        ret.setHashflare("");
        ret.setIdioma("es");
        ret.setIfttCita("");
        ret.setIfttContact("");
        ret.setIfttVisto("");
        ret.setIfttVistowhatsapp("");
        ret.setIpAddress(" ");
        ret.setLastLogin(Integer.MAX_VALUE);
        ret.setLider(0);
        ret.setLiderOk(0);
        ret.setLp("");
        ret.setLpPordefecto("");
        ret.setLr("");
        ret.setMktonline("");
        ret.setMktonlinedesc("");
        ret.setMonedero("");
        ret.setNew1(Boolean.FALSE);
        ret.setPagado(Boolean.FALSE);
        ret.setPagadoOk(Boolean.FALSE);
        ret.setPagoPrimero(Boolean.TRUE);
        ret.setPagos(Boolean.FALSE);
        ret.setPassword(" ");
        ret.setPasswordFirst(" ");
        ret.setPhone("");
        ret.setPixelConversion("");
        ret.setPixelFb("");
        ret.setPixelVisita("");
        ret.setPriceNum(Boolean.TRUE);
        ret.setRecyclix("");
        ret.setRef(0);
        ret.setRememberCode("");
        ret.setRetos("");
        ret.setRuncpa("");
        ret.setRuncpaGenesismining("");
        ret.setRuncpaHashflare("");
        ret.setSalt("");
        ret.setSendResp(Boolean.TRUE);
        ret.setSkype("");
        ret.setTipo("");
        ret.setTrack(" ");
        ret.setTracker("");
        ret.setTrial(Boolean.FALSE);
        ret.setTrialUsado(Boolean.FALSE);
        ret.setVideoInicio(0);
        ret.setVip(Boolean.FALSE);
        ret.setWeb("");
        ret.setWhatsapp("");
        ret.setWhatsappKey("");
        ret.setWingsmobile("");
        ret.setYoucanbook("");
        ret.setZaxaa("");

        return ret;
    }

}
