/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberOld;
import com.wings.backoffice.mlm.domain.OrderDetailOld;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.bis.SpeedBonus;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.MembersOldRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.OrderDetailsRepository;
import com.wings.backoffice.mlm.repos.SpeedBonusRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.services.membersTree.MembersTreeService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.StringUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class MembersService {

    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private SpeedBonusService speedBonusService;
    @Autowired
    private MembersOldRepository membersOldRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrderDetailsRepository orderDetailsRepository;
    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private SpeedBonusRepository speedBonusRepository;
    @Autowired
    private MembersTreeService membersTreeService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private LogsService logsService;

    private final Logger log = LoggerFactory.getLogger(MembersService.class);
    private final Map<Long, List<Long>> childIds = new HashMap<>();
    private final Map<Long, List<Long>> uplineIds = new HashMap<>();

    /*@PostConstruct
    public void init() {
        parseIds();
    }*/
    /**
     *
     */
    public void postImportMembers() {

        log.error("Start fixing members");
        List<Member> members = membersRepository.findByDeleted(Boolean.FALSE);

        members.forEach((member) -> {

            //indirect count
            member.setIndirectCount(countIndirect(member));

            try {
                member.setFirstName(WordUtils.capitalizeFully(member.getFirstName().trim()));
                member.setLastName(WordUtils.capitalizeFully(member.getLastName().trim()));
                member.setStreet(WordUtils.capitalizeFully(member.getStreet() != null ? member.getStreet().trim() : ""));
                member.setCity(WordUtils.capitalizeFully(member.getCity().trim()));
                member.setState(WordUtils.capitalizeFully(member.getState().trim()));
                member.setEmail(member.getEmail().toLowerCase());

                if (member.getCompanyName() != null) {
                    member.setCompanyName(WordUtils.capitalizeFully(member.getCompanyName().trim()));
                }

                if (member.getOrder() != null) {

                    if (!member.isFounder()) {
                        boolean founder = member.getOrder().getItemId() == 8l;
                        founder = founder || (member.getOrder().getStoreTitle() != null
                                && (member.getOrder().getStoreTitle().toLowerCase().contains("upgrade")
                                || member.getOrder().getStoreTitle().toLowerCase().contains("founder")));

                        if (founder) {
                            member.setFounderDate(member.getOrder().getActivationDate());
                        }
                        member.setFounder(founder);

                        if (!founder) {
                            List<Order> orders = ordersService.findByTypeAndMemberId(OrderType.upgrade.name(), member.getId());
                            for (Order order : orders) {
                                founder = (order.getActive() && order.getStoreTitle() != null
                                        && (order.getStoreTitle().toLowerCase().contains("upgrade")
                                        || order.getStoreTitle().toLowerCase().contains("founder")));
                                member.setFounder(founder);
                                member.setFounderDate(order.getActivationDate());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("Error fixing member " + member.getId() + " " + e.getMessage());
            }
        });

        members.forEach((member) -> {
            membersRepository.save(member);
        });
        log.error("Done Fixing members");

        log.error("Start Fixing orders");
        //fix orders texts
        List<Order> orders = ordersService.findByActiveAndDeleted(Boolean.TRUE, Boolean.FALSE);
        int i = 0;
        int max = orders.size();
        for (Order order : orders) {

            order.setFirstName(StringUtils.capitalize(order.getFirstName()));
            order.setLastName(StringUtils.capitalize(order.getLastName()));
            order.setPayerEmail(StringUtils.toLowerCase(order.getPayerEmail()));
            order.setBillingCompany(StringUtils.capitalize(order.getBillingCompany()));

            order.setBillingAddress(StringUtils.capitalize(order.getBillingAddress()));
            order.setBillingCity(StringUtils.capitalize(order.getBillingCity()));
            order.setBillingName(StringUtils.capitalize(order.getBillingName()));
            order.setBillingState(StringUtils.capitalize(order.getBillingState()));

            //log.error("Fixing order " + i++ + " of " + max);
        }
        orders.forEach((order) -> {
            ordersService.save(order);
        });
        log.error("Done Fixing orders");
    }

    /**
     *
     * @param memberId
     * @param kitId
     * @param request
     * @return
     */
    public String updateKit(Long memberId, Long kitId, HttpServletRequest request) {

        Member m = membersRepository.findOne(memberId);
        if (m == null) {
            return "No se econtro el afiliado";
        }

        Long orderId = m.getOrderId();
        Order o = ordersService.getOrder(orderId);
        if (o == null) {
            return "No se encontro la orden";
        }

        StoreItem olditem = storeItemsRepository.findOne(o.getItemId());
        String oldItemDesc = olditem.getName() + "(" + olditem.getId() + ")";

        StoreItem item = storeItemsRepository.findOne(kitId);
        if (item == null) {
            return "No se encontro kit con el nuevo id";
        }

        /* if (item.getCategory() != 1) {
            return "El id seleccionado no corresponde a un kit de afiliado";
        }*/
        String newItemDesc = item.getName() + "(" + item.getId() + ")";
        o.setItemId(kitId);
        o.setType(OrderType.kit.name());
        if (item.getCategory() == 5) {
            o.setType(OrderType.store.name());
        }

        ordersService.save(o);

        String ip = AuthUtils.getIp(request);
        String modUSer = AuthUtils.getAdminUser().getUsername();
        String message = "Cambio de KIT " + oldItemDesc + " a " + newItemDesc + " por " + modUSer + " desde " + ip;
        logsService.addGenericLog(ip, modUSer, m, "member-kit-change", message);

        return "Kit se cambio existosamente";
    }

    /**
     *
     * @param member
     * @return
     */
    public Long countIndirect(Member member) {
        List<Long> all = getChildIds(member.getId());
        if (all == null) {
            return 0l;
        }
        long allcount = all.size();
        long direct = member.getChildren().size();
        long indirect = allcount - direct - 1;
        /*long indirect = 0;
        if (member.getChildren().size() > 0) {
            for (Member child : member.getChildren()) {
                indirect += child.getChildren().size();
                indirect += countIndirect(child);
            }
        }*/
        return indirect;
    }

    /**
     *
     * @return
     */
    public String checkMemberUpdates() {
        List<MemberOld> olds = membersOldRepository.findAll();
        StringBuilder b = new StringBuilder();

        b.append("<pre>");

        for (MemberOld old : olds) {
            Member m = membersRepository.findByUsername(old.getUsername());
            if (m != null) {

                boolean first = true;

                String itemName = m.getOrder().getOrderItem().getName();
                List<OrderDetailOld> details = orderDetailsRepository.findByPayerEmail(old.getEmail());
                for (OrderDetailOld detail : details) {
                    if (detail.getPaymentStatus().equals("Completed") && !detail.getItemName().equals(itemName)) {

                        if (first) {
                            b.append(m).append(System.lineSeparator());
                            first = false;
                        }

                        b.append("\tPago completo y distinto").append(detail.getItemName()).append(System.lineSeparator());
                    }
                }
                membersRepository.save(m);
            } else {
                b.append("Falta bismember para ").append(old.getUsername()).append(System.lineSeparator());
            }

        }
        b.append("</pre>");
        return b.toString();
    }

    /**
     *
     * @param memberId
     * @return
     */
    public String setPayed(Long memberId) {

        AdminUser admin = AuthUtils.getAdminUser();

        Member m = membersRepository.findOne(memberId);
        if (m == null) {
            return "No se enctro el afiliado";
        }

        Order o = ordersService.getOrder(m.getOrderId());
        if (o == null) {
            return "No se encontro la order";
        }

        o.setActivationDate(o.getCreationDate());
        o.setModificationDate(new Date());

        o.setStatus("paid");
        o.setPaymentStatus("paid");
        o.setTypeStatus("paid");
        o.setActive(true);

        o.setComment("Payment mark " + admin.getUsername() + " " + new Date());

        ordersService.save(o);

        return "Orden marcada como pagada";
    }

    /**
     *
     * @param memberId
     * @return
     */
    public List<Long> getChildIds(Long memberId) {
        if (childIds.isEmpty()) {
            parseIds();
        }
        return childIds.get(memberId);
    }

    /**
     *
     * @param memberId
     * @return
     */
    public List<Long> getUplineIds(Long memberId) {
        if (uplineIds.isEmpty()) {
            parseIds();
        }
        return uplineIds.get(memberId);
    }

    /**
     *
     */
    public final void clearCache() {
        childIds.clear();
        uplineIds.clear();
    }

    /**
     *
     */
    public final void parseIds() {
        List<Member> members = membersRepository.findByDeleted(Boolean.FALSE);
        log.error("Start parsing members");
        members.forEach((member) -> {
            childIds.put(member.getId(), membersTreeService.getChildIds(member.getId()));
            uplineIds.put(member.getId(), membersTreeService.getUplineIds(member.getId()));
        });
        log.error("Done parsing members");
    }

    /**
     *
     * @param member
     * @return
     */
    /*private List<Long> getChildIds(Member member) {
        Set<Long> set = new HashSet<>();

        //current memberId
        set.add(member.getId());
        //then my children id
        if (member.getChildren() != null && !member.getChildren().isEmpty()) {
            member.getChildren().forEach((child) -> {
                set.addAll(getChildIds(child));
            });
        }
        List<Long> ids = new ArrayList<>(set);
        return ids;
    }*/
    /**
     *
     * @param member
     * @return
     */
    /*private List<Long> getUplineIds(Member member) {
        List<Long> ids = new LinkedList<>();
        //first my id
        if (member != null && member.getId() != null) {
            ids.add(member.getId());
            while ((member = member.getSponsor()) != null) {
                ids.add(member.getId());
            }
        }
        return ids;
    }*/
    /**
     *
     * @param memberId
     * @param request
     * @return
     */
    public String deleteMember(Long memberId, HttpServletRequest request) {

        String ret;
        try {
            Member m = membersRepository.findOne(memberId);
            m.setDeleted(Boolean.TRUE);
            m.setLoginEnabled(Boolean.FALSE);
            membersRepository.save(m);
            ret = "Usuario marcado como eliminado!\n";
            try {
                Order o = ordersService.getOrder(m.getOrderId());
                o.setDeleted(true);
                ordersService.save(o);
                ret += "Orden marcada como eliminada!\n";

                //subimos a los hijos al sponsor del eliminado
                List<Member> children = m.getChildren();
                List<Long> idModified = new ArrayList<>();
                children.forEach((child) -> {
                    child.setSponsorId(m.getSponsorId());
                    idModified.add(child.getId());
                });
                membersRepository.save(children);
                ret += "Arbol de afiliados ajustado\n";

                String ip = AuthUtils.getIp(request);
                String modUSer = AuthUtils.getAdminUser().getUsername();
                String message = "Afiliado " + m.toString() + " eliminado por " + modUSer + " desde " + ip;
                message += System.lineSeparator() + (idModified.isEmpty() ? " No se modificaron hijos " : " Hijos modificados " + idModified.toString());
                message += System.lineSeparator() + " Sponsor #" + m.getSponsorId() + " " + m.getSponsor().toString();

                logsService.addGenericLog(ip, modUSer, m, "member-deleted", message);

            } catch (Exception e) {
                ret += "Error eliminando orden:" + e.getMessage() + "\n";
            }
        } catch (Exception e) {
            ret = "Error " + e.getMessage();
        }
        return ret;
    }

    /**
     *
     * @param memberId
     * @param sponsorId
     * @param ip
     * @return
     */
    public String updateSponsor(Long memberId, Long sponsorId, String ip) {

        Member m = membersRepository.findOne(memberId);
        Member sponsor = membersRepository.findOne(sponsorId);

        if (m == null) {
            return "No se encontro afiliado";
        }

        if (sponsor == null) {
            return "No se encontro sponsor para ese id";
        }

        String message = "Sponsor Update for afilliate id " + m.getId() + " old: " + m.getSponsorId() + " new:" + sponsorId;
        m.setSponsorId(sponsorId);
        membersRepository.save(m);

        String modUser = AuthUtils.getAdminUser().getUsername();
        logsService.addGenericLog(ip, modUser, m, "sponsor modification", message);

        String ret = "Sponsor actualizado para " + m + "\n nuevo sponsor " + sponsor;
        return ret;

    }

    /**
     *
     * @param memberId
     * @param speedId
     * @return
     */
    public String updateSpeedBonus(Long memberId, Long speedId) {

        String ret;
        try {
            Member m = membersRepository.findOne(memberId);
            SpeedBonus bonus = speedBonusRepository.findOne(speedId);

            if (m == null) {
                return "No se encontro afiliado";
            }

            if (bonus == null) {
                return "No se encontro speed bonus para el id " + speedId;
            }

            m.setSpeedBonusId(speedId);
            membersRepository.save(m);

            ret = "Speed bonus actualizado para " + m + "\n nuevo speed bonus " + bonus.getName();

        } catch (Exception e) {
            ret = "Error actualizando el speed bonus: " + e.getMessage();
            log.error("Error on speed bonus update", e);
        }

        return ret;

    }

    /**
     *
     * @return
     */
    public String getCsv() {

        List<Member> members = membersRepository.findAll();
        StringBuilder b = new StringBuilder();
        b.append("Nombre;Email").append(System.lineSeparator());
        members.forEach((member) -> {
            if (member.getLoginEnabled() && !member.getDeleted()) {
                b.append(member.getFirstName())
                        .append(" ")
                        .append(member.getLastName())
                        .append(";")
                        .append(member.getEmail())
                        .append(System.lineSeparator());
            }
        });

        return b.toString();

    }

    /**
     *
     * @param memberId
     * @param newEmail
     * @param request
     * @return
     * @throws Exception
     */
    public String changeEmail(Long memberId, String newEmail, HttpServletRequest request) throws Exception {

        StringBuilder b = new StringBuilder();
        Member member = membersRepository.findOne(memberId);
        String oldEmail = member.getEmail();

        List<Order> withNewEmail = ordersService.findByPayerEmail(newEmail);
        if (!withNewEmail.isEmpty()) {
            throw new Exception("Existen " + withNewEmail.size() + " ordenes con el nuevo email, no se realizaran cambios");
        }

        List<Order> orders = ordersService.findByPayerEmail(member.getEmail());
        for (Order order : orders) {
            order.setPayerEmail(newEmail);
            b.append("Modificado email para la orden ").append(order.getId()).append("<br/>");
        }
        ordersService.save(orders);

        member.setEmail(newEmail);
        membersRepository.save(member);
        b.append("Modificado email para el afiliado<br/>");

        String ip = AuthUtils.getIp(request);
        String modUser = AuthUtils.getAdminUser().getUsername();
        logsService.addEmailModificationLog(ip, modUser, member, oldEmail);

        return b.toString();
    }

    public Member getOne(Long memberId) {
        return membersRepository.findOne(memberId);
    }

    public Member save(Member member) {
        return membersRepository.save(member);
    }

    public List<Member> save(List<Member> members) {
        return membersRepository.save(members);
    }

    public List<Member> getMembers(List<Long> memberIds) {
        return membersRepository.findAll(memberIds);
    }

    public Member findByUsername(String username) {
        return membersRepository.findByUsernameAndLoginEnabled(username, Boolean.TRUE);
    }

    public Member findByEmail(String email) {
        return membersRepository.findByEmailAndLoginEnabled(email, Boolean.TRUE);
    }

    public List<Member> getBwnToSend() {
        return membersRepository.findByBwnClaimGreaterThanAndBwnSent(0, false);
    }

    public List<Member> getMembers() {
        return membersRepository.findAll();
    }

    public List<Member> getNonDeletedMembers() {
        return membersRepository.findByDeleted(Boolean.FALSE);
    }

    public boolean sendActivationEmail(Member m, String newPassword) {
        StringBuilder body = new StringBuilder("Bienvenido a Wings Mobile, para acceder a tu oficina virtual ");
        body.append("puedes hacer click <a href=\"https://beta.wingsmobile.net/login\" target=\"_blank\">AQUI</a> <br>");
        body.append("Tu Nombre de usuario es: ").append(m.getUsername()).append("<br/>");
        body.append("Tu contraseña es: ").append(newPassword);
        body.append("<br/><br/>");
        String text = emailService.getGenericEmail(m.getFirstName(), "Cuenta de Afiliado", body.toString());
        return emailService.sendMail("Wings Mobile <contratacion@wingsmobile.com>", m.getEmail(), "Cuenta de Afiliado", text, null);
    }

    /**
     *
     * @param order
     */
    public void createMember(Order order) {

        StoreItem storeItem = storeItemsRepository.findOne(order.getItemId());
        try {
            if (storeItem.getCategory() != null && (storeItem.getCategory() == 1 || storeItem.getCategory() == 5) && storeItem.getId() != 15) {

                List<Member> exist = membersRepository.findByEmail(order.getPayerEmail());
                if (exist.isEmpty()) {

                    Member m = new Member();

                    m.setActivationDate(order.getActivationDate());
                    m.setCity(order.getBillingCity());
                    m.setCountry(order.getBillingCountry());
                    m.setEmail(order.getPayerEmail());
                    m.setFirstName(order.getFirstName());
                    m.setLastName(order.getLastName());
                    m.setCompanyName(order.getBillingCompany());
                    m.setDni(order.getDni());
                    m.setDniType(order.getDniType());
                    m.setCalculatedRankId(1l);
                    m.setRankId(1l);
                    m.setInitialRankId(1l);
                    m.setOrderId(order.getId());
                    m.setSource("new-store");
                    m.setDeleted(Boolean.FALSE);
                    m.setLoginEnabled(Boolean.TRUE);
                    m.setIndirectCount(0l);

                    String generatedPassword = InvoiceUtils.getRandomString();
                    m.setPassword(DigestUtils.sha256Hex(generatedPassword));

                    m.setPhone(null);
                    m.setPostal(order.getBillingPostalCode());
                    m.setRegistrationDate(order.getCreationDate());

                    if (order.getStoreId() != null) {
                        Member sponsor = membersRepository.findByUsername(order.getStoreId());
                        if (sponsor == null) {
                            log.error("No se econtro sponsor y el store id existe " + order.getId() + " " + order.getStoreId());
                        } else {
                            m.setSponsorId(sponsor.getId());
                        }
                    }

                    m.setState(order.getBillingState());
                    m.setStatus("created_from_store");
                    m.setStreet(order.getBillingAddress());

                    String newUsername = InvoiceUtils.getNumberRandomString(11);
                    while (membersRepository.findByUsername(newUsername) != null || newUsername.startsWith("0")) {
                        newUsername = InvoiceUtils.getNumberRandomString(11);
                    }
                    m.setUsername(newUsername);
                    m.setDniValidated("0");
                    m.setPaymentsEnabled(true);

                    membersRepository.save(m);

                    speedBonusService.asignSpeedBonus(m);

                    //save member id on the order
                    order.setMemberId(m.getId());
                    ordersService.save(order);

                    sendActivationEmail(m, generatedPassword);

                } else {
                    order.setMemberId(exist.get(0).getId());
                    ordersService.save(order);
                }
            } else {

                //para el topup
                Member m = findByEmail(order.getPayerEmail());
                if (m != null) {
                    order.setMemberId(m.getId());
                    ordersService.save(order);
                }
            }

        } catch (Exception e) {
            log.error("Error creando afiliado " + order.getId() + " - " + order.getId(), e);
        }
        //create member if has a kit
    }

    public long countPaidMembersByCountry(String country) {
        return membersRepository.countPaidMembersByCountry(country);
    }

}
