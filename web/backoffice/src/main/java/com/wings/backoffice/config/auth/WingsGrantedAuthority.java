/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config.auth;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author seba
 */
public class WingsGrantedAuthority implements GrantedAuthority{

    private static final long serialVersionUID = 2263975995034651157L;

    private String authority;

    public WingsGrantedAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
