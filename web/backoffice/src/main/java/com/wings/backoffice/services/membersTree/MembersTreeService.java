/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.membersTree;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.repos.MembersRepository;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class MembersTreeService {

    @Autowired
    private MembersRepository membersRepository;

    private final GenericTree<Long> tree = new GenericTree<>();

    private final Logger log = LoggerFactory.getLogger(MembersTreeService.class);

    private void buildTree() {
        GenericTreeNode<Long> root = new GenericTreeNode<>();
        root.setData(1l);
        tree.setRoot(root);
        log.error("Building tree");
        root.setChildren(getChildNodes(root));
        log.error("Done Building tree");
    }

    /**
     *
     * @param memberId
     * @return
     */
    public List<Long> getChildIds(Long memberId) {
        if (tree.getNumberOfNodes() == 0) {
            buildTree();
        }
        GenericTreeNode<Long> target = tree.find(memberId);

        if (target == null) {
            log.debug("Node children not found for member Id " + memberId);
            return new ArrayList<>();
        }

        return target.getChildrenIds();
    }

    /**
     *
     * @param memberId
     * @return
     */
    public List<Long> getUplineIds(Long memberId) {
        if (tree.getNumberOfNodes() == 0) {
            buildTree();
        }
        GenericTreeNode<Long> target = tree.find(memberId);
        if (target == null) {
            log.debug("Node uplines not found for member Id " + memberId);
            return new ArrayList<>();
        }

        List<Long> ret = new LinkedList<>();
        ret.add(memberId);
        ret.addAll(target.getParentIds());

        return ret;
    }

    /**
     *
     * @param parent
     * @return
     */
    public List<GenericTreeNode<Long>> getChildNodes(GenericTreeNode<Long> parent) {
        List<GenericTreeNode<Long>> ret = new LinkedList<>();
        List<Long> childIds = membersRepository.getChildIds(parent.getData());
        for (Long childId : childIds) {
            GenericTreeNode<Long> child = new GenericTreeNode<>(childId);
            child.setChildren(getChildNodes(child));
            parent.addChild(child);
            ret.add(child);
        }
        return ret;
    }

    public GenericTreeNode find(Long memberId) {
        if (tree.getNumberOfNodes() == 0) {
            buildTree();
        }
        return tree.find(memberId);
    }

    /**
     *
     * @param memberId
     * @return
     */
    public String buildTreeAsTextFrom(Long memberId) {

        if (tree.getNumberOfNodes() == 0) {
            buildTree();
        }
        GenericTreeNode<Long> root = tree.find(memberId);
        Member m = membersRepository.findOne(memberId);

        //build text tree
        StringBuilder b = new StringBuilder();
        b.append("Arbol para ").append(m)
                .append(" - ")
                .append(m.getId())
                .append(System.lineSeparator());

        b.append(getTreeAsText(root, 1));
        return b.toString();
    }

    private String getTreeAsText(GenericTreeNode<Long> root, int level) {
        StringBuilder b = new StringBuilder();
        List<GenericTreeNode<Long>> children = root.getChildren();

        String lvlString = "";
        for (int i = 0; i < level; i++) {
            lvlString += "-";
        }

        for (GenericTreeNode<Long> genericTreeNode : children) {
            b.append(lvlString).append(genericTreeNode.getData());
            b.append(System.lineSeparator());
            b.append(getTreeAsText(genericTreeNode, level + 1));
            //b.append(System.lineSeparator());
        }
        return b.toString();
    }

}
