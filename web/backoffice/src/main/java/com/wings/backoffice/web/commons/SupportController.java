/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.commons;

import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.messages.SupportMessage;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.Result;
import com.wings.backoffice.services.messages.SupportService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author lucas
 */
@Controller
public class SupportController {

    @Autowired
    private SupportService supportService;
    @Autowired
    private CountriesService countriesService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"support/incidences"}, method = {RequestMethod.GET})
    public String getIncidencesList(Model model, HttpServletRequest request) {
        String action = request.getParameter("action");
        boolean isAdmin = AuthUtils.isAdmin();
        UserDetails m = isAdmin ? AuthUtils.getAdminUser() : AuthUtils.getMemberUser();
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String country = request.getParameter("country");
        String asignation = request.getParameter("asignation");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        if (action != null && "members".equals(action) && isAdmin) {
            m = AuthUtils.getMemberUser();
        }

        PageWrapper<SupportMessage> pageWrapper = supportService.search(m, q, spage, ssize, status, country, asignation);
        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("page", pageWrapper);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());
        model.addAttribute("asignation", asignation);
        model.addAttribute("status", status);
        model.addAttribute("size", ssize);
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("action", action);

        return "common/incidences";
    }

    @RequestMapping(value = {"support/incidences"}, method = {RequestMethod.POST})
    public String postNewIncidences(Model model, HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "attachment", required = false) MultipartFile attachment
    ) {
        String action = request.getParameter("action");
        if (action == null || action.isEmpty()) {
            action = "admin";
        }
        UserDetails user = AuthUtils.isAdmin() && "admin".equals(action) ? AuthUtils.getAdminUser() : AuthUtils.getMemberUser();
        String title = request.getParameter("title");
        String description = request.getParameter("description");

        Result<String> result = supportService.save(user, action, title, description, attachment);

        redirectAttributes.addFlashAttribute("message", result.toString());
        redirectAttributes.addFlashAttribute("messageType", result.isOk() ? "success" : "error");

        if (result.isOk()) {
            if ("admin".equals(action)) {
                return "redirect:/admin#support/incidences?action=admin";
            } else {
                return "redirect:/members#support/incidences?action=members";
            }
        } else {
            if ("admin".equals(action)) {
                return "redirect:/admin#support/incidences/create?action=admin";
            } else {
                return "redirect:/members#support/incidences/create?action=members";
            }
        }
    }

    @RequestMapping(value = {"support/incidences/create"}, method = {RequestMethod.GET})
    public String getNewIncidence(Model model, HttpServletRequest request) {
        model.addAttribute("action", request.getParameter("action"));
        model.addAttribute("isAdmin", AuthUtils.isAdmin());
        return "common/incidences-create";
    }

    @RequestMapping(value = {"support/incidences/{id}"}, method = {RequestMethod.GET})
    public String viewIncidence(Model model, HttpServletRequest request, @PathVariable("id") Long incidenceId) {
        SupportMessage message = supportService.getOne(incidenceId);
        String action = request.getParameter("action");
        model.addAttribute("msg", message);
        model.addAttribute("action", action);
        model.addAttribute("isAdmin", AuthUtils.isAdmin());
        return "common/incidences-view";
    }

    @RequestMapping(value = {"support/incidences/{id}"}, method = {RequestMethod.POST})
    public String updateIncidence(Model model, HttpServletRequest request, @PathVariable("id") Long incidenceId, RedirectAttributes redirectAttributes) {
        AdminUser admin = AuthUtils.getAdminUser();
        String action = request.getParameter("action");
        String closeMessage = request.getParameter("closeMessage");
        String status = request.getParameter("status");
        String asignation = request.getParameter("asignation");

        Result<String> result = supportService.update(admin, incidenceId, action, closeMessage, status, asignation);
        redirectAttributes.addFlashAttribute("message", result.toString());
        redirectAttributes.addFlashAttribute("messageType", result.isOk() ? "success" : "error");
        if (result.isOk()) {
            return "redirect:/admin#support/incidences?action=admin";
        } else {
            return "redirect:/admin#support/incidences/" + incidenceId + "?action=admin";
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param response
     */
    @RequestMapping(value = {"support/attachment"}, method = {RequestMethod.GET})
    public void getAttachment(Model model, HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("file");
        try {
            File outputFile = new File("/var/wings/bo_media/attachments/" + name);
            FileInputStream fis = new FileInputStream(outputFile);

            response.setContentType(URLConnection.guessContentTypeFromName(outputFile.getName()));
            if (name.contains(".mp4")) {
                response.setContentType("video/mp4");
            }

            response.setContentLengthLong(outputFile.length());
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Content-Disposition", "filename=" + name);

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
