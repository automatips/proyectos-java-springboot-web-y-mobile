/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin.reports;

import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.membersTree.GenericTreeNode;
import com.wings.backoffice.services.membersTree.MembersTreeService;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ModificationsController {

    @Autowired
    private MembersService membersService;
    @Autowired
    private MembersTreeService membersTreeService;

    private Logger log = LoggerFactory.getLogger(ModificationsController.class);

    @RequestMapping(value = {"admin/reports/modifications"}, method = {RequestMethod.GET})
    public String getModifications(Model model, HttpServletRequest request) {

        //Busco el afiliado
        //List<Long> childIds = membersTreeService.getChildIds(1898l);
        //log.error("child ids " + childIds);
        /*select * from bis_logs 
        where reference_id 
        in (5889, 5891, 5892, 5895, 5897, 5898, 5900, 2829, 5902, 6927, 3087, 5903, 5904, 5649, 5906, 5908, 5910, 5912, 7449, 5913, 5914, 7451, 5915, 3356, 5917, 5406, 8992, 5920, 3108, 5924, 5927, 7719, 5929, 2859, 5932, 3887, 5935, 5936, 5938, 5942, 3405, 5965, 7246, 2384, 5201, 5969, 7765, 2901, 9309, 3168, 2913, 5474, 8296, 5993, 8304, 1908, 373, 2165, 3190, 2679, 2681, 8318, 4224, 1921, 3458, 6020, 2949, 3463, 8586, 2703, 5794, 5809, 5812, 5818, 5563, 5565, 5821, 5567, 5823, 5824, 5569, 5825, 5570, 5827, 5828, 5830, 5832, 5833, 5834, 5835, 5837, 2770, 7381, 5849, 7899, 2783, 5599, 3555, 3043, 9188, 5863, 5353, 5865, 5098, 5867, 5868, 5869, 5870, 5361, 5873, 2547, 5875, 2548, 5876, 7925, 3061, 3829, 3062, 3063, 3831, 3832, 5880, 3833, 5882, 7419, 5884, 7421, 2813, 5887, 1898)
        and `type` not in ('member_login');*/
        
        
        /*List<Integer> childIds = Arrays.asList(5889, 5891, 5892, 5895, 5897, 5898, 5900, 2829, 5902, 6927, 3087, 5903, 5904, 5649, 5906, 5908, 5910, 5912, 7449, 5913, 5914, 7451, 5915, 3356, 5917, 5406, 8992, 5920, 3108, 5924, 5927, 7719, 5929, 2859, 5932, 3887, 5935, 5936, 5938, 5942, 3405, 5965, 7246, 2384, 5201, 5969, 7765, 2901, 9309, 3168, 2913, 5474, 8296, 5993, 8304, 1908, 373, 2165, 3190, 2679, 2681, 8318, 4224, 1921, 3458, 6020, 2949, 3463, 8586, 2703, 5794, 5809, 5812, 5818, 5563, 5565, 5821, 5567, 5823, 5824, 5569, 5825, 5570, 5827, 5828, 5830, 5832, 5833, 5834, 5835, 5837, 2770, 7381, 5849, 7899, 2783, 5599, 3555, 3043, 9188, 5863, 5353, 5865, 5098, 5867, 5868, 5869, 5870, 5361, 5873, 2547, 5875, 2548, 5876, 7925, 3061, 3829, 3062, 3063, 3831, 3832, 5880, 3833, 5882, 7419, 5884, 7421, 2813, 5887, 1898);
        for (Integer childId : childIds) {
            String searchString = "'%old: " + childId + "%'";
            log.error("select * from bis_logs where message like " + searchString);
        }*/

        //log.error("Tree " + membersTreeService.buildTreeAsTextFrom(1898l));
        
        
        //busco el arbol de ids de afiliados
        //busco logs con ids dentro del arbol de afiliados
        return "admin/members/modifications";
    }

}
