package com.wings.backoffice.mlm.domain.bis.orders;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "vw_members")
public class MembersView {
    @Id
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date registrationDate;
    @Temporal(TemporalType.DATE)
    private Date activationDate; 
    private String country;
    private String countryFinal;
    private String fullName;
    private String kit;
    private String rankName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public Integer getMonth(boolean activated){
        Calendar c = new GregorianCalendar();
        if(activated) {
            c.setTime(activationDate);
        }else{
            c.setTime(registrationDate);
        }
        return c.get(Calendar.MONTH) + 1;
    }
    
    public Integer getYear(boolean activated){
        Calendar c = new GregorianCalendar();
        if(activated) {
            c.setTime(activationDate);
        }else{
            c.setTime(registrationDate);
        }
        return c.get(Calendar.YEAR);
    }

    public String getCountryFinal() {
        return countryFinal;
    }

    public void setCountryFinal(String countryFinal) {
        this.countryFinal = countryFinal;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getKit() {
        return kit;
    }

    public void setKit(String kit) {
        this.kit = kit;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rango) {
        this.rankName = rango;
    }
    
    
}
