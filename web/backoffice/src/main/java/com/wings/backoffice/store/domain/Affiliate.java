/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "wingstore_affiliate_wp_affiliates")
public class Affiliate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Long affiliateId;
    private long userId;
    private String rate;
    private String rateType;
    private String paymentEmail;
    private String status;
    private String earnings;
    private String unpaidEarnings;
    private long referrals;
    private long visits;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRegistered;

    public Affiliate() {
    }

    public Affiliate(Long affiliateId) {
        this.affiliateId = affiliateId;
    }

    public Affiliate(Long affiliateId, long userId, String rate, String rateType, String paymentEmail, String status, String earnings, String unpaidEarnings, long referrals, long visits, Date dateRegistered) {
        this.affiliateId = affiliateId;
        this.userId = userId;
        this.rate = rate;
        this.rateType = rateType;
        this.paymentEmail = paymentEmail;
        this.status = status;
        this.earnings = earnings;
        this.unpaidEarnings = unpaidEarnings;
        this.referrals = referrals;
        this.visits = visits;
        this.dateRegistered = dateRegistered;
    }

    public Long getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(Long affiliateId) {
        this.affiliateId = affiliateId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getPaymentEmail() {
        return paymentEmail;
    }

    public void setPaymentEmail(String paymentEmail) {
        this.paymentEmail = paymentEmail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEarnings() {
        return earnings;
    }

    public void setEarnings(String earnings) {
        this.earnings = earnings;
    }

    public String getUnpaidEarnings() {
        return unpaidEarnings;
    }

    public void setUnpaidEarnings(String unpaidEarnings) {
        this.unpaidEarnings = unpaidEarnings;
    }

    public long getReferrals() {
        return referrals;
    }

    public void setReferrals(long referrals) {
        this.referrals = referrals;
    }

    public long getVisits() {
        return visits;
    }

    public void setVisits(long visits) {
        this.visits = visits;
    }

    public Date getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(Date dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (affiliateId != null ? affiliateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Affiliate)) {
            return false;
        }
        Affiliate other = (Affiliate) object;
        if ((this.affiliateId == null && other.affiliateId != null) || (this.affiliateId != null && !this.affiliateId.equals(other.affiliateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.store.domain.WingstoreAffiliateWpAffiliates[ affiliateId=" + affiliateId + " ]";
    }
    
}
