/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Log;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.enums.LogReference;
import com.wings.backoffice.mlm.enums.LogType;
import com.wings.backoffice.mlm.repos.LogsRepository;
import com.wings.backoffice.mlm.specs.LogsSpecs;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class LogsService {

    @Autowired
    private LogsRepository logsRepository;

    @Autowired
    private GeoIpService geoIpService;

    public void addLog(Log log) {
        log.setCountry(geoIpService.getIsoForIp(log.getModIp()));
        logsRepository.save(log);
    }

    public void addContractSignedLog(String message, String modIp, String modUser, Member m) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_profile_edit.name());
        l.setReference(LogReference.member.name());
        l.setComment("member contract signature");

        l.setMessage(message);
        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    public void addProfileModificationLog(String message, String modIp, String modUser, Member m) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_profile_edit.name());
        l.setReference(LogReference.member.name());
        l.setComment("member profile modification");

        l.setMessage(message);
        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    public void addPasswordModificationLog(String modIp, String modUser, Member m) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_profile_edit.name());
        l.setReference(LogReference.member.name());
        l.setComment("member password modification");

        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    public void addMemberLoginLog(String login, String modIp, Member m, boolean success) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_login.name());
        l.setReference(LogReference.member.name());
        if (success) {
            l.setComment("member login success");
        } else {
            l.setComment("member login failure");
        }
        l.setModIp(modIp);
        l.setModUser(login);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    public void addEmailModificationLog(String modIp, String modUser, Member m, String oldEmail) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_profile_edit.name());
        l.setReference(LogReference.member.name());
        l.setComment("member email modification");
        l.setMessage("Cambio email de " + oldEmail + " a " + m.getEmail());

        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    public void addKitModificationLog(String modIp, String modUser, Member m, Long oldKit) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_profile_edit.name());
        l.setReference(LogReference.member.name());
        l.setComment("member kit modification");
        l.setMessage("Cambio kit de " + oldKit + " a " + m.getOrderId());

        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    /**
     *
     * @param modIp
     * @param modUser
     * @param referenceId
     * @param logReferenceName
     * @param comment
     * @param message
     */
    public void addGenericLog(String modIp, String modUser, Long referenceId, String logReferenceName, String comment, String message) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.generic_log.name());
        l.setReference(logReferenceName);
        l.setComment(comment);
        l.setMessage(message);

        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(referenceId);

        logsRepository.save(l);
    }

    public void addGenericLog(String modIp, String modUser, Member m, String comment, String message) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.generic_log.name());
        l.setReference(LogReference.member.name());
        l.setComment(comment);
        l.setMessage(message);

        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);
    }

    public void addKitUpgradeLog(String modIp, String modUser, Member m, Long upgradeKit) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.member_profile_edit.name());
        l.setReference(LogReference.member.name());
        l.setComment("member kit upgrade");
        l.setMessage("Nuevo upgrade " + upgradeKit);

        l.setModIp(modIp);
        l.setModUser(modUser);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(m.getId());

        logsRepository.save(l);

    }

    public void addAdminLoginLog(String login, String modIp, AdminUser a, boolean success) {

        Log l = new Log();
        l.setCreationDate(new Date());
        l.setType(LogType.admin_login.name());
        l.setReference(LogReference.admin.name());
        if (success) {
            l.setComment("admin login success");
        } else {
            l.setComment("admin login failure");
        }
        l.setModIp(modIp);
        l.setModUser(login);
        l.setCountry(geoIpService.getIsoForIp(modIp));

        l.setReferenceId(a.getId());

        logsRepository.save(l);
    }

    /**
     * return last 5/7 access
     *
     * @param member
     * @return
     */
    public List<Log> getMemberAccessLog(Member member) {
        fillCountries();
        List<Log> ret;
        if (member == null) {
            ret = logsRepository.findTop6ByTypeOrderByCreationDateDesc(LogType.member_login.name());
        } else {
            ret = logsRepository.findTop5ByTypeAndReferenceIdOrderByCreationDateDesc(LogType.member_login.name(), member.getId());
        }
        return ret;
    }

    /**
     * return 7 admin access
     *
     * @return
     */
    public List<Log> getAdminAccessLog() {
        fillCountries();
        List<Log> ret;
        ret = logsRepository.findTop6ByTypeOrderByCreationDateDesc(LogType.admin_login.name());
        return ret;
    }

    /**
     * return 7 admin access
     *
     * @return
     */
    public List<Log> getEditLog() {
        fillCountries();
        List<Log> ret;
        ret = logsRepository.findTop6ByTypeOrderByCreationDateDesc(LogType.member_profile_edit.name());
        return ret;
    }

    /**
     *
     */
    public void fillCountries() {
        List<Log> logs = logsRepository.findByCountryIsNull();
        logs.forEach((log) -> {
            log.setCountry(geoIpService.getIsoForIp(log.getModIp()));
        });
        logsRepository.save(logs);
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param from
     * @param to
     * @param type
     * @param comment
     * @param secret
     * @return
     */
    public Page<Log> search(int page, int size, String q, Date from, Date to, String type, String comment) {
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        return logsRepository.findAll(LogsSpecs.search(q, from, to, type, comment), pr);
    }

}
