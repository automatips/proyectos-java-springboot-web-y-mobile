/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.RankHistory;
import com.wings.backoffice.mlm.domain.bis.SpeedBonus;
import com.wings.backoffice.mlm.domain.utils.MemberSnapshot;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.mlm.repos.MemberSnapshotRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.RankHistoryRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.mlm.repos.ResidualServicesRepository;
import com.wings.backoffice.mlm.specs.MemberSpecs;
import com.wings.backoffice.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class RanksService {

    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private CalificationPointsRepository calificationPointsRepository;
    @Autowired
    private RankHistoryRepository rankHistoryRepository;
    @Autowired
    private RanksBisRepository ranksBisRepository;
    @Autowired
    private MemberSnapshotRepository memberSnapshotRepository;
    @Autowired
    private MembersService membersService;
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private ResidualServicesRepository residualServicesRepository;

    private final Logger log = LoggerFactory.getLogger(RanksService.class);

    private boolean running = false;
    private int process = 0;
    private int cant = 0;

    /**
     * Returns the rank of the member at that particular Date returns current
     * ranks if there's not rank history
     *
     * @param member
     * @param date
     * @return RankBis at that date
     */
    public RankBis getRankAtDate(Member member, Date date) {

        RankBis ret;
        RankHistory history = rankHistoryRepository.findFirstByMemberIdAndDateLessThanEqualOrderByDateDesc(member.getId(), date);
        if (history == null) {
            ret = member.getInitialRank();
        } else {
            Long idReal = history.getRankId() + 1;
            if (member.getInitialRankId() > idReal) {
                ret = member.getInitialRank();
            } else {
                ret = ranksBisRepository.findOne(idReal);
            }
        }

        return ret;
    }

    public void updateMemberLastRank(Long memberId) {
        Member member = membersRepository.findOne(memberId);
        MemberSnapshot lastSnap = memberSnapshotRepository.findFirstByMemberIdOrderBySnapshotDateDescEventIdDesc(member.getId());
        if (lastSnap == null) {
            return;
        }
        long rankid = lastSnap.getNextRankId() - 1;
        if (member.getRankId() < rankid) {
            member.setRankId(rankid);
            membersRepository.save(member);
        }
    }

    /**
     * Analize ranks for each member adding record on rank history if needed
     *
     * @return
     */
    @Transactional
    public String analizeRanks() {
        if (!running) {
            running = true;
            process = 0;
            try {

                log.error("Start Analizing ranks");
                //delete all auto rank history
                rankHistoryRepository.delete(rankHistoryRepository.findByAuto(true));

                //delete all snapshots
                //memberSnapshotRepository.deleteAll();
                memberSnapshotRepository.deleteAllInBatch();

                //delete all balance from speed bonus
                balanceRepository.deleteByType(BalanceType.speed.name());

                //clear member service cache
                //membersService.clearCache();
                //List<Member> members = membersRepository.findByDeleted(false);
                List<Member> members = membersRepository.findAll(MemberSpecs.getPaidMembers());
                cant = members.size();

                //reset calculated rank
                members.forEach((member) -> {
                    member.setCalculatedRankId(member.getInitialRankId());
                    member.setSpeedBonusEndDate(setSpeedBonusEndDate(member));
                });
                membersRepository.save(members);

                List<Long> excluded = Arrays.asList(1l, 2l, 3l, 1477l);

                for (Member member : members) {

                    if (excluded.contains(member.getId())) {
                        continue;
                    }

                    analizeSpeedBonus(member);
                    analizeRankHistory(member);
                    updateMemberLastRank(member.getId());
                    process++;
                    //log.error("Analizing member " + member.getId());
                }
//                members.forEach((member) -> {
//                });

                //rankHistoryRepository.save(toSave);
            } catch (Exception e) {
                log.error("Error parseando rangos ", e);
                return "Error:" + e.getMessage();
            }
            running = false;
        }

        log.error("Done Analizing ranks");
        return "Procesados " + process + " de " + cant;
    }

    /**
     * Analize rank and speed bonus for this particular member
     *
     * @param member
     * @return
     */
    @Transactional
    public String analizeRanks(Member member) {
        try {

            //delete all auto rank history
            rankHistoryRepository.delete(rankHistoryRepository.findByAutoAndMemberId(true, member.getId()));

            //delete all snapshots
            memberSnapshotRepository.delete(memberSnapshotRepository.findByMemberId(member.getId()));

            //delete all balance from speed bonus
            balanceRepository.deleteByTypeAndMemberId(BalanceType.speed.name(), member.getId());

            //clear member service cache
            membersService.clearCache();

            //reset calculated rank
            member.setCalculatedRankId(member.getInitialRankId());
            setSpeedBonusEndDate(member);
            membersRepository.save(member);

            analizeSpeedBonus(member);
            analizeRankHistory(member);

        } catch (Exception e) {
            log.error("Error parseando rangos ", e);
            return "Error:" + e.getMessage();
        }
        return "Rango analizado para afiliado " + member;
    }

    /**
     * This is only used to generate rank history and snapshots for speed bonus
     * and calculated rank
     *
     * @param member
     * @return
     */
    public List<RankHistory> analizeRankHistory(Member member) {

        List<RankHistory> historysToSave = new LinkedList<>();
        List<MemberSnapshot> snapsToSave = new LinkedList<>();

        //get Dealer always
        //RankBis initialRank = ranksBisRepository.findOne(1l);
        RankBis initialRank = member.getInitialRank();
        RankBis currentRank = member.getRank();

        List<CalificationPoint> points = calificationPointsRepository.findByMemberIdOrderByDateAsc(member.getId());
        if (getNextRank(member.getRank()) == null) {
            //maximo alcanzado
            return historysToSave;
        }

        MemberSnapshot snap = null;
        for (CalificationPoint point : points) {

            RankBis next = getNextRank(initialRank);
            /*RankHistory manual = rankHistoryRepository.findFirstByMemberIdAndDateLessThanEqualAndAutoOrderByDateDesc(member.getId(), point.getDate(), Boolean.FALSE);
            if (manual != null) {
                next = getNextRank(manual.getRank());
            }*/

            if (next == null) {
                //max level reached
                break;
            }

            //--si esto es false, no se guarda el historial de rango
            //Si el speed bonus a finalizado se toma en cuenta mes a mes y 
            //trimestral
            //pero si se guarda el snapshot
            boolean speed = isInSpeedBonus(member, point.getDate());

            double productPv = 0.0;
            double kitPv = 0.0;
            double personalServicePv = 0.0;
            double servicePv = 0.0;

            Map<Long, Double> pointsPerChild;
            Map<Long, Double> sumsPerBranch;

            Long eventId = point.getEventId();
            Long offerMemberId = ordersService.findMemberIdByOrderId(eventId);

            //init snap
            if (snap == null) {

                snap = new MemberSnapshot();
                snap.setMemberId(member.getId());
                snap.setMember(member);

                pointsPerChild = new HashMap<>();
                sumsPerBranch = new HashMap<>();

                Map<Long, List<Long>> ids = new HashMap<>();
                List<Long> myId = new ArrayList<>();
                myId.add(member.getId());
                ids.put(member.getId(), myId);

                List<Member> children = snap.getMember().getChildren();
                children.forEach((child) -> {
                    ids.put(child.getId(), membersService.getChildIds(child.getId()));
                });

                snap.setChildIds(ids);
                snap.setSumsPerBranchMap(sumsPerBranch);

            } else {
                productPv = snap.getProductsPv();
                servicePv = snap.getServicesPv();
                kitPv = snap.getKitsPv();
                personalServicePv = snap.getPersonalServicePv();

                pointsPerChild = snap.getPointsPerChildMap();
                sumsPerBranch = snap.getSumsPerBranchMap();

            }

            snap.setNextRankRequired(next.getQuantity());
            snap.setNextRankRequiredSB(next.getSbQuantity());
            snap.setSnapshotDate(point.getDate());
            snap.setSpeedBonus(speed);
            snap.setNextRankId(next.getId());
            snap.setNextRankName(next.getName());
            snap.setNextRank(next);
            snap.setEventId(point.getEventId());
            snap.setEventPoints(point.getPoints());
            snap.setIsFounder(member.isFounder());

            //sum values
            if (point.getType().equals(OrderType.product.name())) {
                productPv += point.getPoints();
            } else if (point.getType().equals(OrderType.kit.name()) || point.getType().equals(OrderType.upgrade.name()) || point.getType().equals(OrderType.store.name())) {
                kitPv += point.getPoints();
            } else if (point.getType().equals(OrderType.service.name())) {
                //TODO: si coincide el dni o el email setear el member id al servicio                
                if (personalServicePv == 0 && offerMemberId != null && offerMemberId.equals(member.getId())) {
                    personalServicePv += point.getPoints();
                } else {
                    servicePv += point.getPoints();
                }
            }

            snap.setKitsPv(kitPv);
            snap.setPersonalServicePv(personalServicePv);
            snap.setProductsPv(productPv);
            snap.setServicesPv(servicePv);

            //fill points per child
            if (offerMemberId == null) {
                //mi rama
                if (pointsPerChild.containsKey(member.getId())) {
                    pointsPerChild.put(member.getId(), pointsPerChild.get(member.getId()) + point.getPoints());
                } else {
                    pointsPerChild.put(member.getId(), point.getPoints());
                }
            } else {
                //de afiliados    
                if (pointsPerChild.containsKey(offerMemberId)) {
                    pointsPerChild.put(offerMemberId, pointsPerChild.get(offerMemberId) + point.getPoints());
                } else {
                    pointsPerChild.put(offerMemberId, point.getPoints());
                }
            }

            //sum points per branch
            for (Map.Entry<Long, List<Long>> entry : snap.getChildIds().entrySet()) {
                Long key = entry.getKey();
                List<Long> idList = entry.getValue();

                if (idList == null) {
                    continue;
                }

                double sum = 0.0;
                for (Map.Entry<Long, Double> entry1 : pointsPerChild.entrySet()) {
                    Long key1 = entry1.getKey();
                    Double value1 = entry1.getValue();
                    if (idList.contains(key1)) {
                        sum += value1;
                    }
                }
                sumsPerBranch.put(key, sum);
            }

            //clear
            snap.setPointsPerChild(null);
            snap.setSumsPerBranch(null);
            snap.setPointsPerChildMap(pointsPerChild);
            snap.setSumsPerBranchMap(sumsPerBranch);

            snap.setMostValuableBranch(getIdBranchMostValuable(snap.getSumsPerBranchMap()));
            snap.setSpeedBonus(speed);

            //required points
            long required = next.getQuantity();
            if (speed && next.getSbQuantity() > 0) {
                required = next.getSbQuantity();
            }

            double sum;
            Double most = sumsPerBranch.get(snap.getMostValuableBranch());
            Double mostVal = most != null ? most : 0.0;
            Double sixty = required * 0.60;
            if (mostVal > sixty) {
                sum = sixty;
            } else {
                sum = mostVal;
            }

            for (Map.Entry<Long, Double> entry : sumsPerBranch.entrySet()) {
                Long key = entry.getKey();
                Double value = entry.getValue();
                if (!key.equals(snap.getMostValuableBranch())) {
                    sum += value;
                }
            }
            snap.setSum6040(sum);

            //counters
            Long clients = ordersService.countByStoreIdAndTypeAndActiveAndActivationDateLessThanEqual(member.getUsername(), OrderType.product.name(), Boolean.TRUE, point.getDate());
            clients += residualServicesRepository.countByStoreIdAndStatusAndCreationDateLessThanEqual(member.getUsername(), "active", point.getDate());
            snap.setClientCount(clients);

            Long refered = membersRepository.countBySponsorIdAndRegistrationDateLessThanEqual(member.getId(), point.getDate());
            snap.setReferers(refered);

            if (sum >= required) {

                //si fecha del evento posterior a final de octubre se tiene en cuenta los afiliados y clientes
                boolean despuesDeOctubre = false;
                try {
                    despuesDeOctubre = point.getDate().after(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-10-31 23:59:59"));
                } catch (Exception e) {
                }

                //filtros
                int referersRequired = 5;
                int clientsRequired = 5;
                if (initialRank.getId().equals(1l)) {
                    referersRequired = 3;
                    clientsRequired = 3;
                }

                String referersRule = "";
                if (despuesDeOctubre) {
                    referersRule = " - " + referersRequired + " referidos" + "/" + clientsRequired + " clients";
                }

                if (member.getSpeedBonus().getExcludeClientCount()) {
                    clientsRequired = 0;
                }

                if (despuesDeOctubre && refered < referersRequired) {
                    snap.setComment("No cumple condicion " + referersRequired + " afiliados");
                } else if (despuesDeOctubre && clients < clientsRequired) {
                    snap.setComment("No cumple condicion " + referersRequired + " clientes");
                } else {
                    //sube
                    RankHistory history = new RankHistory();
                    history.setMemberId(member.getId());
                    history.setDate(point.getDate());
                    history.setRankId(initialRank.getId());
                    history.setAuto(true);

                    member.setCalculatedRank(next);
                    member.setCalculatedRankId(next.getId());

                    if (member.getRankId() < next.getId()) {
                        member.setRankId(next.getId());
                    }

                    String desc = "Auto Rango puntos ";
                    desc += (speed) ? " (speed bonus) " : "";
                    desc += sum + " sube a " + next.getName();
                    desc += " aplicadas 60/40 " + referersRule;
                    history.setDescription(desc);
                    snap.setComment("Califica a " + next.getName());

                    historysToSave.add(history);
                    membersRepository.save(member);
                    initialRank = next;

                }
            } else {
                snap.setComment("No cumple puntos requeridos " + sum + "/" + required);
            }

            snapsToSave.add(snap.getClone());
        }

        snapsToSave.forEach((memberSnapshot) -> {
            memberSnapshot.setPointsPerChild(null);
            ///memberSnapshot.setSumsPerBranch(null);
            memberSnapshotRepository.save(memberSnapshot);
        });

        historysToSave.forEach((rankHistory) -> {
            rankHistoryRepository.save(rankHistory);
        });
        return historysToSave;
    }

    /**
     * This is only used to generate rank history and snapshots for speed bonus
     * and calculated rank
     *
     * @param member
     * @return
     */
    public List<RankHistory> analizeSpeedBonus(Member member) {

        List<RankHistory> historysToSave = new LinkedList<>();
        if (getNextRank(member.getRank()) == null) {
            //maximo alcanzado
            return historysToSave;
        }

        //get Dealer always
        RankBis initialRank = ranksBisRepository.findOne(1l);

        List<CalificationPoint> points = calificationPointsRepository.findByMemberIdOrderByDateAsc(member.getId());
        MemberSnapshot snap = null;

        for (CalificationPoint point : points) {
            RankBis next = getNextRank(initialRank);
            if (next == null) {
                //max level reached
                break;
            }

            //--si esto es false, no se guarda el historial de rango
            //Si el speed bonus a finalizado se toma en cuenta mes a mes y 
            //trimestral
            //pero si se guarda el snapshot
            boolean speed = isInSpeedBonus(member, point.getDate());

            double productPv = 0.0;
            double kitPv = 0.0;
            double personalServicePv = 0.0;
            double servicePv = 0.0;

            Map<Long, Double> pointsPerChild;
            Map<Long, Double> sumsPerBranch;

            Long eventId = point.getEventId();
            Long offerMemberId = ordersService.findMemberIdByOrderId(eventId);

            //init snap
            if (snap == null) {

                snap = new MemberSnapshot();
                snap.setMemberId(member.getId());
                snap.setMember(member);

                pointsPerChild = new HashMap<>();
                sumsPerBranch = new HashMap<>();

                Map<Long, List<Long>> ids = new HashMap<>();
                List<Long> myId = new ArrayList<>();
                myId.add(member.getId());
                ids.put(member.getId(), myId);

                List<Member> children = snap.getMember().getChildren();
                children.forEach((child) -> {
                    ids.put(child.getId(), membersService.getChildIds(child.getId()));
                });

                snap.setChildIds(ids);
                snap.setSumsPerBranchMap(sumsPerBranch);

            } else {
                productPv = snap.getProductsPv();
                servicePv = snap.getServicesPv();
                kitPv = snap.getKitsPv();
                personalServicePv = snap.getPersonalServicePv();

                pointsPerChild = snap.getPointsPerChildMap();
                sumsPerBranch = snap.getSumsPerBranchMap();

            }

            snap.setNextRankRequired(next.getQuantity());
            snap.setNextRankRequiredSB(next.getSbQuantity());
            snap.setSnapshotDate(point.getDate());
            snap.setSpeedBonus(speed);
            snap.setNextRankId(next.getId());
            snap.setNextRankName(next.getName());
            snap.setNextRank(next);
            snap.setEventId(point.getEventId());
            snap.setEventPoints(point.getPoints());
            snap.setIsFounder(member.isFounder());

            //sum values
            if (point.getType().equals(OrderType.product.name())) {
                productPv += point.getPoints();
            } else if (point.getType().equals(OrderType.kit.name()) || point.getType().equals(OrderType.upgrade.name()) || point.getType().equals(OrderType.store.name())) {
                kitPv += point.getPoints();
            } else if (point.getType().equals(OrderType.service.name())) {
                //TODO: si coincide el dni o el email setear el member id al servicio                
                if (personalServicePv == 0 && offerMemberId != null && offerMemberId.equals(member.getId())) {
                    personalServicePv += point.getPoints();
                } else {
                    servicePv += point.getPoints();
                }
            }

            snap.setKitsPv(kitPv);
            snap.setPersonalServicePv(personalServicePv);
            snap.setProductsPv(productPv);
            snap.setServicesPv(servicePv);

            //fill points per child
            if (offerMemberId == null) {
                //mi rama
                if (pointsPerChild.containsKey(member.getId())) {
                    pointsPerChild.put(member.getId(), pointsPerChild.get(member.getId()) + point.getPoints());
                } else {
                    pointsPerChild.put(member.getId(), point.getPoints());
                }
            } else {
                //de afiliados    
                if (pointsPerChild.containsKey(offerMemberId)) {
                    pointsPerChild.put(offerMemberId, pointsPerChild.get(offerMemberId) + point.getPoints());
                } else {
                    pointsPerChild.put(offerMemberId, point.getPoints());
                }
            }

            //sum points per branch
            for (Map.Entry<Long, List<Long>> entry : snap.getChildIds().entrySet()) {
                Long key = entry.getKey();
                List<Long> idList = entry.getValue();

                if (idList == null) {
                    continue;
                }

                double sum = 0.0;
                for (Map.Entry<Long, Double> entry1 : pointsPerChild.entrySet()) {
                    Long key1 = entry1.getKey();
                    Double value1 = entry1.getValue();
                    if (idList.contains(key1)) {
                        sum += value1;
                    }
                }
                sumsPerBranch.put(key, sum);
            }

            //clear
            snap.setPointsPerChild(null);
            snap.setSumsPerBranch(null);
            snap.setPointsPerChildMap(pointsPerChild);
            snap.setSumsPerBranchMap(sumsPerBranch);

            snap.setMostValuableBranch(getIdBranchMostValuable(snap.getSumsPerBranchMap()));
            snap.setSpeedBonus(speed);

            //required points
            long required = next.getQuantity();
            if (speed && next.getSbQuantity() > 0) {
                required = next.getSbQuantity();
            }

            double sum;
            Double most = sumsPerBranch.get(snap.getMostValuableBranch());
            Double mostVal = most != null ? most : 0.0;
            Double sixty = required * 0.60;
            if (mostVal > sixty) {
                sum = sixty;
            } else {
                sum = mostVal;
            }

            for (Map.Entry<Long, Double> entry : sumsPerBranch.entrySet()) {
                Long key = entry.getKey();
                Double value = entry.getValue();
                if (!key.equals(snap.getMostValuableBranch())) {
                    sum += value;
                }
            }
            snap.setSum6040(sum);

            //counters
            Long clients = ordersService.countByStoreIdAndTypeAndActiveAndActivationDateLessThanEqual(member.getUsername(), OrderType.product.name(), Boolean.TRUE, point.getDate());
            clients += residualServicesRepository.countByStoreIdAndStatusAndCreationDateLessThanEqual(member.getUsername(), "active", point.getDate());
            snap.setClientCount(clients);

            //TODO:FILTRAR POR FECHA DE ACTIVACION
            Long refered = membersRepository.countBySponsorIdAndRegistrationDateLessThanEqual(member.getId(), point.getDate());
            snap.setReferers(refered);

            if (sum >= required) {

                //si fecha del evento posterior a final de octubre se tiene en cuenta los afiliados y clientes
                boolean despuesDeOctubre = false;
                try {
                    despuesDeOctubre = point.getDate().after(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-10-31 23:59:59"));
                } catch (Exception e) {
                }

                //filtros
                int referersRequired = 5;
                int clientsRequired = 5;
                if (initialRank.getId().equals(1l)) {
                    referersRequired = 3;
                    clientsRequired = 3;
                }

                String referersRule = "";
                if (despuesDeOctubre) {
                    referersRule = " - " + referersRequired + " referidos" + "/" + clientsRequired + " clients";
                }

                if (member.getSpeedBonus().getExcludeClientCount()) {
                    clientsRequired = 0;
                }

                if (despuesDeOctubre && refered < referersRequired) {
                    snap.setComment("No cumple condicion " + referersRequired + " afiliados");
                } else if (despuesDeOctubre && clients < clientsRequired) {
                    snap.setComment("No cumple condicion " + referersRequired + " clientes");
                } else {
                    //sube
                    RankHistory history = new RankHistory();
                    history.setMemberId(member.getId());
                    history.setDate(point.getDate());
                    history.setRankId(initialRank.getId());
                    history.setAuto(true);

                    member.setCalculatedRank(next);
                    member.setCalculatedRankId(next.getId());

                    String desc = "Auto Rango puntos ";
                    desc += (speed) ? " (speed bonus) " : "";
                    desc += sum + " sube a " + next.getName();
                    desc += " aplicadas 60/40 " + referersRule;
                    history.setDescription(desc);

                    historysToSave.add(history);

                    //si sube inserto - speed bonus
                    if (speed) {
                        Balance bal = balanceRepository.findByMemberIdAndType(member.getId(), BalanceType.speed.name());
                        double amount = 0.0;
                        switch (next.getId().intValue()) {
                            case 2:
                                amount = member.getSpeedBonus().getPriceRank1();
                                break;
                            case 3:
                                amount = member.getSpeedBonus().getPriceRank2();
                                break;
                            case 4:
                                amount = member.getSpeedBonus().getPriceRank3();
                                break;
                        }

                        if (bal == null) {
                            bal = new Balance();
                            bal.setBonusType(BalanceType.speed.name());
                            bal.setType(BalanceType.speed.name());
                            bal.setComment("New record");
                            bal.setControl("speed-" + member.getId() + "-" + next.getId());
                            bal.setDescription(desc);
                            bal.setEffective(true);
                            bal.setMemberId(member.getId());
                            bal.setReferenceId(0l);
                            bal.setStatus(BalanceStatus.active.name());
                        } else {
                            bal.setComment("Update");
                            bal.setDescription(desc);
                        }

                        bal.setCreationDate(point.getDate());
                        bal.setAmount(amount);
                        if (amount > 0) {
                            balanceRepository.save(bal);
                        }
                    }

                    //membersRepository.save(member);
                    initialRank = next;

                }
            } else {
                snap.setComment("No cumple puntos requeridos " + sum + "/" + required);
            }

            //snapsToSave.add(snap.getClone());
        }

        //memberSnapshotRepository.save(snapsToSave);
        //rankHistoryRepository.save(historysToSave);
        return historysToSave;
    }

    /**
     * Given a rank, return the inmediate superior
     *
     * @param rank
     * @return
     */
    public RankBis getNextRank(RankBis rank) {
        return ranksBisRepository.findByDirectRequired(rank.getId());
    }

    /**
     * recalculate all members endDate for new speed bonus
     */
    public void recalculateMemberEndDate() {
        List<Member> members = membersRepository.findAll();
        for (Member member : members) {
            setSpeedBonusEndDate(member);
        }
        membersRepository.save(members);
    }

    private boolean isInSpeedBonus(Member member, Date snapshotDate) {
        SpeedBonus speed = member.getSpeedBonus();

        long days;
        long daysFounder;

        //Fecha exacta para dates, esta nunca es null
        Date registrationDate = member.getRegistrationDate();
        if (member.getOrder() != null && member.getOrder().getActivationDate() != null) {
            registrationDate = member.getOrder().getActivationDate();
        }

        if (speed.getDurationDays() == 0) {
            //por fecha
            days = DateUtils.getDiffDays(registrationDate, speed.getDurationTo());
            daysFounder = DateUtils.getDiffDays(registrationDate, speed.getDurationToFounder());
        } else {
            //por dias
            days = speed.getDurationDays();
            daysFounder = speed.getDurationDaysFounder();
        }

        long sinceRegistered = DateUtils.getDiffDays(registrationDate, snapshotDate);
        if (member.isFounder()) {
            return sinceRegistered < daysFounder;
        } else {
            return sinceRegistered < days;
        }
    }

    private Date setSpeedBonusEndDate(Member member) {
        SpeedBonus speed = member.getSpeedBonus();

        long days;
        long daysFounder;

        //Fecha exacta para dates, esta nunca es null
        Date registrationDate = member.getRegistrationDate();
        if (member.getOrder() != null && member.getOrder().getActivationDate() != null) {
            registrationDate = member.getOrder().getActivationDate();
        }

        if (speed.getDurationDays() == 0) {
            //por fecha
            days = DateUtils.getDiffDays(registrationDate, speed.getDurationTo());
            daysFounder = DateUtils.getDiffDays(registrationDate, speed.getDurationToFounder());
        } else {
            //por dias
            days = speed.getDurationDays();
            daysFounder = speed.getDurationDaysFounder();
        }

        //set speed bonus end date
        Calendar cal = Calendar.getInstance();
        cal.setTime(registrationDate);
        if (member.isFounder()) {
            cal.add(Calendar.DAY_OF_YEAR, (int) daysFounder);
        } else {
            cal.add(Calendar.DAY_OF_YEAR, (int) days);
        }
        return cal.getTime();
    }

    private Long getIdBranchMostValuable(Map<Long, Double> pointsPerBranch) {
        Long ret = 0l;
        double maxPoints = 0.0;
        for (Map.Entry<Long, Double> entry : pointsPerBranch.entrySet()) {
            Long key = entry.getKey();
            Double value = entry.getValue();
            if (maxPoints < value) {
                maxPoints = value;
                ret = key;
            }
        }
        return ret;
    }

    public String updateRank(Long memberId, Long newRankId, Date date) {

        try {
            RankBis newRank = ranksBisRepository.findOne(newRankId);
            RankBis oldRank = ranksBisRepository.findOne(newRankId - 1);
            Member m = membersRepository.findOne(memberId);

            RankHistory history = new RankHistory();
            history.setAuto(false);
            history.setDate(date);
            history.setDescription("Manual update de " + oldRank.getName() + " a " + newRank.getName());
            history.setMemberId(memberId);
            history.setRankId(oldRank.getId());

            rankHistoryRepository.save(history);

            m.setRankId(newRankId);
            membersRepository.save(m);

        } catch (Exception e) {
            return "Ocurrio un error actualizando el rango " + e.getMessage();
        }

        return "Rango actualizado";
    }

    public String resetRanks() {
        try {
            List<Member> members = membersRepository.findAll();
            members.stream().filter((member) -> !(member.getSponsorId() == null || member.getSponsorId() == 1)).forEachOrdered((member) -> {
                if (member.getSponsorId() == 2 || member.getSponsorId() == 3) {
                    member.setRankId(3l);
                    member.setInitialRankId(3l);
                } else if (member.getEmail().equals("joaquin.modrego@hotmail.com")) {
                    member.setRankId(2l);
                    member.setInitialRankId(2l);
                } else {
                    member.setRankId(1l);
                    member.setInitialRankId(1l);
                }
            });
            membersRepository.save(members);

        } catch (Exception e) {
            return "Ocurrio un error reseteando los rangos " + e.getMessage();
        }
        return "Rango reinicializados";
    }
}
