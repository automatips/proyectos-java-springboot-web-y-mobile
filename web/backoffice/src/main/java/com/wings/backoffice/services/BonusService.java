/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberSim;
import com.wings.backoffice.mlm.domain.TopupProvider;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.Differential;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.utils.MemberSnapshot;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.enums.BonusType;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.MembersRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.wings.backoffice.mlm.repos.DifferentialRepository;
import com.wings.backoffice.mlm.specs.MemberSpecs;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class BonusService {

    @Autowired
    private RanksService ranksService;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private DifferentialRepository differentialBisRepository;
    @Autowired
    private MemberSnapshotService memberSnapshotService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private TopUpsService topUpsService;
    @Autowired
    private MemberSimsService memberSimsService;
    @Autowired
    private MembersService membersService;

    private final Logger log = LoggerFactory.getLogger(BonusService.class);

    //caches
    private final Map<Long, Member> memberCacheId = new HashMap<>();
    private final Map<String, Member> memberCacheUsername = new HashMap<>();

    @Transactional
    public void calculateDifferential() {

        log.error("Start calculating differential");

        List<Differential> toDelete = differentialBisRepository.findAll();
        differentialBisRepository.delete(toDelete);
        //TODO: sacar esto luego
        //differentialDAO.truncate();

        List<Member> members = membersRepository.findAll(MemberSpecs.getPaidMembers(), new Sort(Sort.Direction.DESC, "id"));
        int c = 0;
        for (Member member : members) {

            try {
                if (member == null) {
                    log.error("Afiliado NULLL ");
                    continue;
                }
                if (member.getOrderId() == null) {
                    log.debug("Afiliado sin order id " + member.getId());
                    continue;
                }
                if (member.getDeleted()) {
                    log.debug("Afiliado eliminado " + member.getId());
                    continue;
                }

                //si es pago en cuotas, proceso todas las ordenes
                //si no , solo la de afiliacion
                Order meOrder = member.getOrder();
                List<Order> orders = new LinkedList<>();
                if (meOrder.getInstallmentNumber() == null || meOrder.getInstallmentNumber() == 0) {
                    orders.add(meOrder);
                } else {
                    orders = ordersService.getCuotasOrder(member);
                }

                List<Long> ids = new ArrayList<>();
                for (Order order : orders) {
                    ids.add(order.getId());
                }

                //agrego otras ordenes si no estan en las de arriba pero estan pagas, que no sean upgrade
                List<Order> extras = ordersService.getMemberOrdersPayedNotIn(member.getId(), ids);
                if (!extras.isEmpty()) {
                    orders.addAll(extras);
                }

                for (Order order : orders) {

                    if (order == null || order.getOrderItem().getName().equals("FREE") || !order.getActive()) {
                        log.debug("Order nula o free o no activa " + member.getOrderId());
                        continue;
                    }

                    if (!(order.getOrderItem().getCategoryName().equals("kit") || order.getOrderItem().getCategoryName().equals("store"))) {
                        continue;
                    }

                    member = order.getMember();
                    if (order.isDeleted()) {
                        log.debug("Order eliminada " + member.getOrderId());
                        continue;
                    }

                    List<Differential> toSave = new ArrayList<>();

                    Date orderDate = order.getActivationDate();
                    if (orderDate == null) {
                        log.debug("Order con fecha activacion null " + member.getOrderId());
                        continue;
                    }

                    StoreItem item = order.getOrderItem();
                    RankBis rank = ranksService.getRankAtDate(member, orderDate);
                    Long eventId = order.getId();

                    Differential df = new Differential();

                    df.setOfferUserId(member.getId());
                    df.setOfferRankId(rank.getId());
                    df.setReceiveUserId(member.getId());
                    df.setReceiveRankId(rank.getId());
                    df.setDate(order.getActivationDate());
                    df.setComment("Propio - solo calificación - " + item.getName() + " (" + item.getPrice() + ")");
                    df.setQuantityRank(item.getRankPoints());
                    df.setQuantityPay(0.0);
                    df.setPercentage(rank.getPercentage());
                    df.setEventId(eventId);
                    toSave.add(df);

                    Member parent;
                    List<Long> payed = new ArrayList<>();
                    double payedPercent = 0.0;
                    long originalOfferId = member.getId();
                    long originalRankId = rank.getId();
                    String originalName = member.toString();

                    List<Long> matchingRankPayed = new ArrayList<>();
                    Map<Long, Differential> matchingOffers = new HashMap<>();

                    while ((parent = member.getSponsor()) != null) {

                        RankBis parentRank = ranksService.getRankAtDate(parent, orderDate);

                        df = new Differential();
                        df.setOfferUserId(originalOfferId);
                        df.setReceiveUserId(parent.getId());
                        df.setOfferRankId(originalRankId);
                        df.setReceiveRankId(parentRank.getId());
                        df.setDate(order.getActivationDate());
                        df.setQuantityRank(item.getRankPoints());
                        df.setEventId(eventId);

                        //si ya pague el rango, solo pago 0
                        if (payed.contains(parentRank.getId())) {

                            df.setComment("Rango igualado, solo calificación - " + item.getName() + " (" + item.getPrice() + ") " + originalName);
                            df.setQuantityPay(0.0);
                            df.setPercentage(rank.getPercentage());

                        } else {

                            if (parentRank.getPercentage() - payedPercent <= 0) {

                                df.setQuantityPay(0.0);
                                df.setPercentage(parentRank.getPercentage());
                                df.setComment("Maxima diferencia alcanzada " + item.getName() + " (" + item.getPrice() + ") " + originalName);

                            } else {

                                double toPay = ((parentRank.getPercentage() - payedPercent) * item.getPayPoints()) / 100;
                                df.setQuantityPay(toPay);
                                df.setPercentage(parentRank.getPercentage());
                                df.setComment((parentRank.getPercentage() - payedPercent) + "% diferencia " + item.getName() + " (" + item.getPrice() + ")  " + originalName);
                                payedPercent += (parentRank.getPercentage() - payedPercent);

                                if (parentRank.getId() >= 3 && !matchingOffers.containsKey(parentRank.getId())) {
                                    Differential dfm = new Differential();

                                    dfm.setOfferUserId(parent.getId());
                                    dfm.setOfferRankId(originalRankId);

                                    dfm.setDate(order.getActivationDate());
                                    dfm.setQuantityRank(0.0);
                                    dfm.setPercentage(0l);
                                    dfm.setQuantityPay(toPay * 0.20);
                                    dfm.setComment("Matching bonus " + item.getName() + " (" + item.getPrice() + ") " + originalName);
                                    dfm.setMatching(true);
                                    dfm.setEventId(eventId);

                                    matchingOffers.put(parentRank.getId(), dfm);
                                }

                            }

                            payed.add(parentRank.getId());

                        }

                        if (matchingOffers.containsKey(parentRank.getId())
                                && !matchingOffers.get(parentRank.getId()).getOfferUserId().equals(parent.getId())
                                && !matchingRankPayed.contains(parentRank.getId())) {

                            matchingRankPayed.add(parentRank.getId());
                            matchingOffers.get(parentRank.getId()).setReceiveUserId(parent.getId());
                            matchingOffers.get(parentRank.getId()).setReceiveRankId(parentRank.getId());
                            toSave.add(matchingOffers.get(parentRank.getId()));
                        }

                        toSave.add(df);

                        member = parent;
                        rank = parentRank;
                    }

                    toSave.forEach((differential) -> {
                        differentialBisRepository.save(differential);
                    });

                }

            } catch (Exception e) {
                log.error("Error parseando differential afiliado " + (member != null ? member.getId() : "member is null"), e);
            }
        }

        calculateDifferentialForUpgrades();

        log.error("Done calculating differential");
    }

    public void calculateDifferentialForUpgrades() {

        List<Order> orders = ordersService.findByType(OrderType.upgrade.name());

        int c = 0;
        for (Order order : orders) {

            try {
                Member member = order.getMember();
                if (order.getOrderItem().getName().equals("FREE") || !order.getActive()) {
                    log.debug("Order free o no activa " + order.getId());
                    continue;
                }

                if (order.isDeleted()) {
                    continue;
                }

                List<Differential> toSave = new ArrayList<>();

                Date orderDate = order.getActivationDate();
                if (orderDate == null) {
                    log.debug("Order con fecha activacion null " + order.getId());
                    continue;
                }

                StoreItem item = order.getOrderItem();
                RankBis rank = ranksService.getRankAtDate(member, orderDate);
                Long eventId = order.getId();

                Differential df = new Differential();

                df.setOfferUserId(member.getId());
                df.setOfferRankId(rank.getId());
                df.setReceiveUserId(member.getId());
                df.setReceiveRankId(rank.getId());
                df.setDate(order.getActivationDate());
                df.setComment("Propio - solo calificación - " + item.getName() + " (" + item.getPrice() + ")");
                df.setQuantityRank(item.getRankPoints());
                df.setQuantityPay(0.0);
                df.setPercentage(rank.getPercentage());
                df.setEventId(eventId);
                toSave.add(df);

                Member parent;
                List<Long> payed = new ArrayList<>();
                double payedPercent = 0.0;
                long originalOfferId = member.getId();
                long originalRankId = rank.getId();
                String originalName = member.toString();

                List<Long> matchingRankPayed = new ArrayList<>();
                Map<Long, Differential> matchingOffers = new HashMap<>();

                while ((parent = member.getSponsor()) != null) {

                    RankBis parentRank = ranksService.getRankAtDate(parent, orderDate);

                    df = new Differential();
                    df.setOfferUserId(originalOfferId);
                    df.setReceiveUserId(parent.getId());
                    df.setOfferRankId(originalRankId);
                    df.setReceiveRankId(parentRank.getId());
                    df.setDate(order.getActivationDate());
                    df.setQuantityRank(item.getRankPoints());
                    df.setEventId(eventId);

                    //si ya pague el rango, solo pago 0
                    if (payed.contains(parentRank.getId())) {

                        df.setComment("Rango igualado, solo calificación - " + item.getName() + " (" + item.getPrice() + ") " + originalName);
                        df.setQuantityPay(0.0);
                        df.setPercentage(rank.getPercentage());

                    } else {

                        if (parentRank.getPercentage() - payedPercent <= 0) {

                            df.setQuantityPay(0.0);
                            df.setPercentage(parentRank.getPercentage());
                            df.setComment("Maxima diferencia alcanzada " + item.getName() + " (" + item.getPrice() + ") " + originalName);

                        } else {

                            double toPay = ((parentRank.getPercentage() - payedPercent) * item.getPayPoints()) / 100;
                            df.setQuantityPay(toPay);
                            df.setPercentage(parentRank.getPercentage());
                            df.setComment((parentRank.getPercentage() - payedPercent) + "% diferencia " + item.getName() + " (" + item.getPrice() + ")  " + originalName);
                            payedPercent += (parentRank.getPercentage() - payedPercent);

                            if (parentRank.getId() >= 3 && !matchingOffers.containsKey(parentRank.getId())) {
                                Differential dfm = new Differential();

                                dfm.setOfferUserId(parent.getId());
                                dfm.setOfferRankId(originalRankId);

                                dfm.setDate(order.getActivationDate());
                                dfm.setQuantityRank(0.0);
                                dfm.setPercentage(0l);
                                dfm.setQuantityPay(toPay * 0.20);
                                dfm.setComment("Matching bonus " + item.getName() + " (" + item.getPrice() + ") " + originalName);
                                dfm.setMatching(true);
                                dfm.setEventId(eventId);

                                matchingOffers.put(parentRank.getId(), dfm);
                            }

                        }

                        payed.add(parentRank.getId());

                    }

                    if (matchingOffers.containsKey(parentRank.getId())
                            && !matchingOffers.get(parentRank.getId()).getOfferUserId().equals(parent.getId())
                            && !matchingRankPayed.contains(parentRank.getId())) {

                        matchingRankPayed.add(parentRank.getId());
                        matchingOffers.get(parentRank.getId()).setReceiveUserId(parent.getId());
                        matchingOffers.get(parentRank.getId()).setReceiveRankId(parentRank.getId());
                        toSave.add(matchingOffers.get(parentRank.getId()));
                    }

                    toSave.add(df);

                    member = parent;
                    rank = parentRank;
                }
                differentialBisRepository.save(toSave);

            } catch (Exception e) {
                log.error("Error parseando differential upgrade " + order.getId(), e);
            }

        }

    }

    //TODO, hacer esto si es cliente y no afiliado
    public List<Balance> getProductBalances(Order order) {

        if (memberCacheId.isEmpty()) {
            List<Member> membersTemp = membersRepository.findAll();
            membersTemp.forEach((member) -> {
                memberCacheId.put(member.getId(), member);
                memberCacheUsername.put(member.getUsername(), member);
            });
        }

        List<Balance> ret = new LinkedList<>();

        if (order.isDeleted()) {
            return ret;
        }

        if (order.getOrderItem() == null) {
            log.debug("No item found for order " + order.getId());
            return ret;
        }

        if (order.getStoreId() == null) {
            log.debug("No store found for order " + order.getId());
            return ret;
        }

        if (!order.getActive()) {
            return ret;
        }

        Member m = memberCacheUsername.get(order.getStoreId());
        StoreItem i = order.getOrderItem();
        //List<Long> ids = MemberUtils.getUplineIds(m);
        List<Long> ids = membersService.getUplineIds(m.getId());

        if (i.getRankPoints() == 0) {
            return ret;
        }

        long skipMine = m.getId();

        String control = "product-" + order.getId() + "-" + m.getId();
        Balance balance = new Balance();
        //25% del precio de venta
        balance.setAmount(i.getPrice() * 0.25);
        balance.setDescription("Bono directo venta producto " + order.getOrderItem().getName() + " - " + order.getFirstName() + " " + order.getLastName());
        balance.setMemberId(m.getId());
        balance.setCreationDate(order.getActivationDate());
        balance.setStatus(BalanceStatus.created.name());
        balance.setType(BalanceType.product.name());
        balance.setControl(control);
        balance.setReferenceId(order.getId());
        balance.setBonusType(BonusType.direct.name());
        balance.setOrderId(order.getId());

        ret.add(balance);

        int level = 0;
        for (Long id : ids) {

            //skip first branchs
            if (id <= 3) {
                continue;
            }

            //skip mine
            if (id.equals(skipMine)) {
                continue;
            }

            Member member = memberCacheId.get(id);
            RankBis rank = ranksService.getRankAtDate(member, order.getActivationDate() != null ? order.getActivationDate() : order.getCreationDate());
            MemberSnapshot snap = memberSnapshotService.getMemberSnapshot(member.getId(), order.getId());

            balance = new Balance();
            control = "product-" + order.getId() + "-" + id;
            balance.setDescription("Bono indirecto venta producto " + order.getOrderItem().getName() + " - " + order.getFirstName() + " " + order.getLastName());
            balance.setMemberId(id);
            balance.setCreationDate(order.getActivationDate());
            balance.setStatus(BalanceStatus.created.name());
            balance.setType(BalanceType.product.name());
            balance.setControl(control);
            balance.setReferenceId(order.getId());
            balance.setBonusType(BonusType.indirect.name());

            switch (level) {
                case 0:
                    //4% del precio de venta
                    balance.setAmount(i.getPayPoints() * 0.04d);
                    balance.setStatusText("Nivel 1 - 4%");
                    break;
                case 1:
                    //1%
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    balance.setStatusText("Nivel 2 - 1%");
                    break;
                case 2:
                    //1%
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    balance.setStatusText("Nivel 3 - 1%");
                    break;
                case 3:
                    balance.setAmount(i.getPayPoints() * 0.01d);

                    //SI ES DIRECTOR O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    if (member.isFounder() || rank.getId() >= 3) {
                        balance.setStatusText("Nivel 4 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 4");
                    }
                    break;
                case 4:
                    //SI ES REGIONAL PRESIDENT O MAYOR O FUNDER (5% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.05d);
                    if (member.isFounder() || rank.getId() >= 5) {
                        balance.setStatusText("Nivel 5 - 5%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 5");
                    }
                    break;
                case 5:
                    //SI ES NATIONAL PRESIDENT O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    if (member.isFounder() || rank.getId() >= 6) {
                        balance.setStatusText("Nivel 6 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 6");
                    }
                    break;
                case 6:
                    //SI ES INTERNATIONAL PRESIDENT O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    if (member.isFounder() || rank.getId() >= 7) {
                        balance.setStatusText("Nivel 7 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 7");
                    }
                    break;
            }

            //condicion para el bono
            if (snap != null && snap.getProductsPv() < 10) {
                balance.setStatus(BalanceStatus.not_aplicable.name());
                balance.setStatusText("Bonus condicion no cumplida - 10 PV productos (" + snap.getProductsPv() + ")");
            }

            if (balance.getAmount() != null) {
                ret.add(balance);
            }

            level++;
            if (level == 7) {
                break;
            }

        }

        return ret;
    }

    public List<Balance> getServiceBalances(Order order) {

        if (memberCacheId.isEmpty()) {
            List<Member> membersTemp = membersRepository.findAll();
            membersTemp.forEach((member) -> {
                memberCacheId.put(member.getId(), member);
                memberCacheUsername.put(member.getUsername(), member);
            });
        }

        Member m = memberCacheUsername.get(order.getStoreId());
        StoreItem i = order.getOrderItem();
        List<Long> ids = membersService.getUplineIds(m.getId());
        List<Balance> ret = new LinkedList<>();

        if (i.getRankPoints() == 0) {
            log.debug("Order without rank points " + order.getId());
            return ret;
        }

        if (order.isDeleted()) {
            log.debug("Order deleted " + order.getId());
            return ret;
        }

        if (order.getOrderItem() == null) {
            log.debug("No item found for order " + order.getId());
            return ret;
        }

        if (order.getStoreId() == null) {
            log.debug("No store found for order " + order.getId());
            return ret;
        }

        if (!order.getActive()) {
            log.debug("Order not active " + order.getId());
            return ret;
        }

        int level = 0;
        for (Long id : ids) {

            //skip first branch
            if (id <= 3) {
                continue;
            }

            String control = "service-" + order.getId() + "-" + id;
            Balance balance = new Balance();

            balance.setDescription("Bono residual servicio " + order.getOrderItem().getName() + " - " + order.getFullName() + " (#" + order.getExternalId() + ")");
            balance.setMemberId(id);
            balance.setCreationDate(order.getActivationDate());
            balance.setStatus(BalanceStatus.created.name());
            balance.setType(BalanceType.service.name());
            balance.setControl(control);
            balance.setReferenceId(order.getId());
            balance.setBonusType(BonusType.residual.name());

            //Member member = membersRepository.findOne(id);
            Member member = memberCacheId.get(id);
            RankBis rank = ranksService.getRankAtDate(member, order.getActivationDate() != null ? order.getActivationDate() : order.getCreationDate());
            MemberSnapshot snap = memberSnapshotService.getMemberSnapshot(member.getId(), order.getId());

            if (snap == null) {
                log.debug("Snap is null");
                return ret;
            }

            switch (level) {
                case 0:
                    //4% del precio de venta
                    balance.setAmount(i.getPayPoints() * 0.04d);
                    balance.setStatusText("Nivel 1 - 4%");
                    break;
                case 1:
                    //1%
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    balance.setStatusText("Nivel 2 - 1%");
                    break;
                case 2:
                    //1%
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    balance.setStatusText("Nivel 3 - 1%");
                    break;
                case 3:
                    //SI ES DIRECTOR O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    if (member.isFounder() || rank.getId() >= 3) {
                        balance.setStatusText("Nivel 4 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 4");
                    }
                    break;
                case 4:
                    //SI ES REGIONAL PRESIDENT O MAYOR O FUNDER (5% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.05d);
                    if (member.isFounder() || rank.getId() >= 5) {
                        balance.setStatusText("Nivel 5 - 5%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 5");
                    }
                    break;
                case 5:
                    //SI ES NATIONAL PRESIDENT O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    if (member.isFounder() || rank.getId() >= 6) {
                        balance.setStatusText("Nivel 6 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 6");
                    }
                    break;
                case 6:
                    //SI ES INTERNATIONAL PRESIDENT O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(i.getPayPoints() * 0.01d);
                    if (member.isFounder() || rank.getId() >= 7) {
                        balance.setStatusText("Nivel 7 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 7");
                    }

                    break;
            }

            //condicion para el bono // este 30 es solo de clientes directos de servicios no de afiliados
            if (snap.getPersonalServicePv() < 6 || snap.getServicesPv() < 30) {
                balance.setStatus(BalanceStatus.not_aplicable.name());
                balance.setStatusText("Bonus condicion no cumplida - (6/30) - (" + snap.getPersonalServicePv() + "/" + snap.getServicesPv() + ")");
            }

            if (balance.getAmount() != null) {
                if (balance.getStatus().equals(BalanceStatus.created.name()) && order.getActive()) {
                    balance.setEffective(true);
                    balance.setStatus(BalanceStatus.effective.name());
                }
                ret.add(balance);
            }
            level++;
            if (level == 7) {
                break;
            }
        }

        return ret;
    }

    public void deleteEvent(Long eventId, String cause) {
        List<Differential> diffs = differentialBisRepository.findByEventIdAndMatchingOrderByReceiveUserIdDesc(eventId, false);
        diffs.addAll(differentialBisRepository.findByEventIdAndMatchingOrderByReceiveUserIdDesc(eventId, true));

        for (Differential diff : diffs) {
            if (!diff.getLocked()) {
                diff.setComment(diff.getComment() + " " + cause);
                diff.setQuantityPay(0.0);
                diff.setQuantityRank(0.0);
            }
        }
        differentialBisRepository.save(diffs);
    }

    public void clearCache() {
        memberCacheId.clear();
        memberCacheUsername.clear();
    }

    /**
     *
     * @return
     */
    public List<Balance> proccessTopups() {
        List<Balance> ret = new LinkedList<>();
        List<TopupProvider> tops = topUpsService.getAllTopupProvider();
        tops.forEach(top -> {
            ret.addAll(getServiceBalances(top));
        });
        return ret;
    }

    /**
     *
     * @param topup
     * @return
     */
    public List<Balance> getServiceBalances(TopupProvider topup) {

        List<Balance> ret = new LinkedList<>();
        //get member form iccid
        MemberSim ms = memberSimsService.getMemberSimIdByIccid(topup.getIccid().substring(0, 18));
        if (ms == null || ms.getMemberId() == null) {
            return ret;
        }

        Long memberId = ms.getTransferMemberId() == null ? ms.getMemberId() : ms.getTransferMemberId();
        Member m = memberCacheId.get(memberId);
        if (m == null) {
            return ret;
        }

        //verificar que el afiliado este pago, la orden no este eliminada o sea nula
        Long afiliationOrderId = m.getOrderId();
        Order o = ordersService.getOrder(afiliationOrderId);
        if (o == null || o.isDeleted() || !o.getStatus().equals("paid")) {
            return ret;
        }

        //set as active
        if (ms.getActivationDate() == null || ms.getActivationDate().after(topup.getCreationDate())) {
            ms.setActivationDate(topup.getCreationDate());
            memberSimsService.save(ms);
        }

        List<Long> ids = membersService.getUplineIds(memberId);
        int level = 0;
        for (Long id : ids) {

            //skip first branch
            if (id <= 3) {
                continue;
            }

            String control = "topup-" + topup.getId() + "-" + id;
            Balance balance = new Balance();

            balance.setDescription("Bono residual recarga " + topup.getPaymentAmountText() + " - " + topup.getIccid() + " - " + topup.getMsisdnHide() + " (#" + topup.getId() + ")");
            if (topup.getAmount() < 0) {
                balance.setDescription("Bono residual devolución " + topup.getPaymentAmountText() + " - " + topup.getIccid() + " - " + topup.getMsisdnHide() + " (#" + topup.getId() + ")");
            }
            balance.setMemberId(id);
            balance.setCreationDate(topup.getCreationDate());
            balance.setStatus(BalanceStatus.created.name());
            balance.setType(BalanceType.service.name());
            balance.setControl(control);
            balance.setReferenceId(topup.getId());
            balance.setBonusType(BonusType.residual.name());

            Member member = membersRepository.findOne(id);
            RankBis rank = ranksService.getRankAtDate(member, topup.getCreationDate());

            //cada 5000 pesos puntos de recarga
            int pr = (int) (topup.getAmount() / 5000);
            //puntos de recarga por 0.25 pv
            Double pv = pr * 0.25;
            //cada pv vale 5 euros
            Double payPoints = pv * 5;

            switch (level) {
                case 0:
                    //4% del precio de venta
                    balance.setAmount(payPoints * 0.04d);
                    balance.setStatusText("Nivel 1 - 4%");
                    break;
                case 1:
                    //1%
                    balance.setAmount(payPoints * 0.01d);
                    balance.setStatusText("Nivel 2 - 1%");
                    break;
                case 2:
                    //1%
                    balance.setAmount(payPoints * 0.01d);
                    balance.setStatusText("Nivel 3 - 1%");
                    break;
                case 3:
                    //SI ES DIRECTOR O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(payPoints * 0.01d);
                    if (member.isFounder() || rank.getId() >= 3) {
                        balance.setStatusText("Nivel 4 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 4");
                    }
                    break;
                case 4:
                    //SI ES REGIONAL PRESIDENT O MAYOR O FUNDER (5% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(payPoints * 0.05d);
                    if (member.isFounder() || rank.getId() >= 5) {
                        balance.setStatusText("Nivel 5 - 5%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 5");
                    }
                    break;
                case 5:
                    //SI ES NATIONAL PRESIDENT O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(payPoints * 0.01d);
                    if (member.isFounder() || rank.getId() >= 6) {
                        balance.setStatusText("Nivel 6 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 6");
                    }
                    break;
                case 6:
                    //SI ES INTERNATIONAL PRESIDENT O MAYOR O FUNDER (1% X (PV DEL EVENTO X 5 EUROS))
                    balance.setAmount(payPoints * 0.01d);
                    if (member.isFounder() || rank.getId() >= 7) {
                        balance.setStatusText("Nivel 7 - 1%");
                    } else {
                        balance.setStatus(BalanceStatus.not_aplicable.name());
                        balance.setStatusText("Bonus condicion no cumplida - nivel 7");
                    }

                    break;
            }

            //Puntos de comision por recarga
            //Cada 5000 pesos de recarga se asignan 0.25 puntos de recarga (PR), 
            //si recargo menos de 5000 no tiene puntos, si recarga 10000 tiene 0.5 PR, etc
            //esos puntos de recarga se suman y se acumulan por mes y luego se pasan a PV
            //
            //El primer mes se pagan todas las comisiones
            //a partir del segundo se tiene que cumplir la siguiente condición:
            //
            //* 20 puntos o mas
            //* 5 sims activas o mas
            /**
             * ********************
             */
            //condicion para el bono // este 30 es solo de clientes directos de servicios no de afiliados
            /*if (snap.getPersonalServicePv() < 6 || snap.getServicesPv() < 30) {
                balance.setStatus(BalanceStatus.not_aplicable.name());
                balance.setStatusText("Bonus condicion no cumplida - (6/30) - (" + snap.getPersonalServicePv() + "/" + snap.getServicesPv() + ")");
            }*/
            if (!balance.getStatus().equals(BalanceStatus.not_aplicable.name())) {
                balance.setEffective(true);
                balance.setStatus(BalanceStatus.effective.name());
                ret.add(balance);
            }

            level++;
            if (level == 7) {
                break;
            }
        }

        return ret;
    }

}
