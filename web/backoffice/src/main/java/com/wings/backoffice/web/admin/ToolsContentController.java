/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.bis.CorpoContent;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.repos.CorpoContentRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.mlm.specs.CorpoContentSpecs;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Controller
public class ToolsContentController {

    @Autowired
    private RanksBisRepository ranksBisRepository;
    @Autowired
    private CorpoContentRepository corpoContentRepository;
    @Autowired
    private CountriesService countriesService;

    private final Logger log = LoggerFactory.getLogger(ToolsContentController.class);

    private final Map<String, String> uploadedMap = new HashMap<>();

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content"}, method = RequestMethod.GET)
    public String getContent(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String rank = request.getParameter("rank");
        String country = request.getParameter("country");
        String type = request.getParameter("type");

        Boolean published = null;
        if (status != null && !status.equals("all")) {
            published = status.equals("active");
        }

        if (rank != null && rank.equals("all")) {
            rank = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));
        Page<CorpoContent> items = corpoContentRepository.findAll(CorpoContentSpecs.search(q, rank, country, published, type), pr);

        PageWrapper<CorpoContent> pageWrapper = new PageWrapper<>(items, "admin/members/list");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        List<RankBis> ranks = ranksBisRepository.findAll();

        model.addAttribute("ranks", ranks);
        model.addAttribute("is_admin", true);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("rank", rank);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("type", type);

        return "admin/tools/content";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-add"}, method = RequestMethod.GET)
    public String getContentAdd(Model model, HttpServletRequest request) {
        List<RankBis> ranks = ranksBisRepository.findAll();
        model.addAttribute("ranks", ranks);
        return "admin/tools/content-add";
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postContentDelete(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        corpoContentRepository.delete(id);
        return ResponseEntity.ok("ok");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-enable/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postContentEnable(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        CorpoContent corpo = corpoContentRepository.findOne(id);
        corpo.setPublished(Boolean.TRUE);
        corpoContentRepository.save(corpo);
        return ResponseEntity.ok("ok");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-disable/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postContentDisable(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        CorpoContent corpo = corpoContentRepository.findOne(id);
        corpo.setPublished(Boolean.FALSE);
        corpoContentRepository.save(corpo);
        return ResponseEntity.ok("ok");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-edit/{id}"}, method = RequestMethod.GET)
    public String getContentEdit(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        CorpoContent content = corpoContentRepository.findOne(id);
        List<RankBis> ranks = ranksBisRepository.findAll();
        model.addAttribute("ranks", ranks);

        model.addAttribute("id", content.getId());
        model.addAttribute("published", content.getPublished());
        model.addAttribute("title", content.getTitle());
        model.addAttribute("description", content.getDescription());
        model.addAttribute("countryAvailable", content.getCountryAvailable());
        model.addAttribute("rankAvailable", content.getRankAvailable());

        return "admin/tools/content-add";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-add"}, method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> postContentAdd(Model model, HttpServletRequest request) {

        Map<String, String> ret = new HashMap<>();

        String id = request.getParameter("content-id");
        String published = request.getParameter("published");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String[] countries = request.getParameterValues("content-country");
        String[] ranks = request.getParameterValues("content-rank");
        String contentFileHash = request.getParameter("content-file-hidden");
        String thumbFileHash = request.getParameter("thumb-file-hidden");

        try {

            CorpoContent content = null;
            if (id != null && !id.isEmpty()) {
                content = corpoContentRepository.findOne(Long.valueOf(id));
            }

            if (content == null) {
                content = new CorpoContent();
            }

            content.setCountryAvailable(Arrays.toString(countries));
            content.setRankAvailable(Arrays.toString(ranks));
            content.setCreationDate(content.getCreationDate() == null ? new Date() : content.getCreationDate());
            content.setDescription(description);
            content.setDownloaded(0);
            content.setTitle(title);

            if (contentFileHash != null && !contentFileHash.isEmpty()) {
                String pathAndMime = uploadedMap.get(contentFileHash);
                content.setPathFile(pathAndMime.split(":")[0]);
                content.setMimeType(pathAndMime.split(":")[1]);
                content.setHashFile(contentFileHash);
            }

            if (thumbFileHash != null && !thumbFileHash.isEmpty()) {
                String pathAndMimeThumb = uploadedMap.get(thumbFileHash);
                content.setPathThumb(pathAndMimeThumb.split(":")[0]);
                content.setHashThumb(thumbFileHash);
            }

            content.setPublished(published != null && published.equals("on"));

            corpoContentRepository.save(content);

            ret.put("contentId", content.getId().toString());
            ret.put("message", "Contenido almacenado correctamente");

        } catch (Exception e) {
            ret.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(ret);
        }

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param request
     * @param file
     * @param thumb
     * @return
     */
    @RequestMapping(value = {"/admin/tools/content-add/file"}, method = RequestMethod.POST)
    public ResponseEntity<String> handleFileUpload(HttpServletRequest request,
            @RequestParam(value = "file", required = false) MultipartFile file,
            @RequestParam(value = "thumb", required = false) MultipartFile thumb) {

        if (file == null && thumb == null) {
            return ResponseEntity.badRequest().body("error file is null");
        }

        String ret = "";
        try {

            if (thumb != null && !thumb.getContentType().contains("image")) {
                return ResponseEntity.badRequest().body("error - la miniatura debe ser una imagen");
            }

            MultipartFile uploaded = file != null ? file : thumb;
            String mime = uploaded.getContentType();
            String fileName = "/var/wings/bo_media/corpo_content/" + System.currentTimeMillis() + "_" + uploaded.getOriginalFilename();
            try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName))) {
                stream.write(uploaded.getBytes());
            }

            ret = DigestUtils.sha1Hex(fileName);
            uploadedMap.put(ret, fileName + ":" + mime);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("error " + e.getMessage());
        }

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param response
     * @param token
     * @return
     */
    @RequestMapping(value = {"/content-get/{token}"}, method = RequestMethod.GET)
    //public ResponseEntity<InputStreamResource> getContent(Model model, HttpServletRequest request, HttpServletResponse response, @PathVariable("token") String token) {
    public void getContent(Model model, HttpServletRequest request, HttpServletResponse response, @PathVariable("token") String token) {

        try {

            CorpoContent content = corpoContentRepository.findByHashFileOrHashThumb(token, token);
            File ret;
            if (content.getHashFile().equals(token)) {
                ret = new File(content.getPathFile());
            } else {
                ret = new File(content.getPathThumb());
            }

            if (!ret.exists()) {
                //return ResponseEntity.badRequest().build();
                return;
            }

            FileInputStream fis;
            try {
                fis = new FileInputStream(ret);
            } catch (FileNotFoundException e) {
                //return ResponseEntity.notFound().build();
                return;
            }

            HttpHeaders headers = new HttpHeaders();
            if (content.getHashFile().equals(token)) {
                //headers.setContentType(MediaType.parseMediaType(content.getMimeType()));
                response.setContentType(MediaType.parseMediaType(content.getMimeType()).getType());
            } else {
                //headers.setContentType(MediaType.IMAGE_JPEG);
                response.setContentType(MediaType.IMAGE_JPEG.getType());
            }
            try {
                response.setContentLengthLong(ret.length());
            } catch (Exception e) {
            }

            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + ret.getName() + "\"");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Content-Transfer-Encoding", "binary");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();
            //return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
        } catch (Exception e) {
        }
    }

}
