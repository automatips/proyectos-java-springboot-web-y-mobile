/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.MemberSim;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author lucas
 */
public interface MemberSimsRepository extends JpaRepository<MemberSim, Long> {

    List<MemberSim> findByMemberId(Long MemberId);
    Page<MemberSim> findByMemberId(Long MemberId, Pageable page);
    
    List<MemberSim> findByIccidId(Long IccidId);
    List<MemberSim> findByIccid(String Iccid);
    List<MemberSim> findByOrderId(Long OrderId);
    List<MemberSim> findByStatus(String Status);
    List<MemberSim> findByDeleted(Boolean Deleted);
}
