/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Matching;
import com.wings.backoffice.mlm.domain.Member;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class MatchingSpecs {

    public static Specification<Matching> memberSearch(String offerUser, String receiveUser, Date from, Date to) {

        return (Root<Matching> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Matching, Member> offer = root.join("offerUser");
            Join<Matching, Member> receive = root.join("receiveUser");

            List<Predicate> predicates = new ArrayList<>();
            if (offerUser != null) {
                String search = "%" + offerUser + "%";
                predicates.add(
                        builder.or(
                                builder.like(offer.get("firstName"), search),
                                builder.like(offer.get("lastName"), search),
                                builder.like(offer.get("email"), search),
                                builder.like(offer.get("username"), search),
                                builder.like(builder.concat(builder.concat(offer.get("firstName"), " "), offer.get("lastName")), search)
                        )
                );
            }

            if (receiveUser != null) {
                String search = "%" + receiveUser + "%";
                predicates.add(
                        builder.or(
                                builder.like(receive.get("firstName"), search),
                                builder.like(receive.get("lastName"), search),
                                builder.like(receive.get("email"), search),
                                builder.like(receive.get("username"), search),
                                builder.like(builder.concat(builder.concat(receive.get("firstName"), " "), receive.get("lastName")), search)
                        )
                );
            }

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("date"), from));
            }
            
            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("date"), to));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
