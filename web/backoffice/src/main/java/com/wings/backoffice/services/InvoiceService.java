/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Country;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Withdrawal;
import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.domain.bis.invoices.InvoiceItem;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.enums.InvoiceType;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.enums.WithdrawalStatus;
import com.wings.backoffice.mlm.repos.CountriesRepository;
import com.wings.backoffice.mlm.repos.InvoiceItemRepository;
import com.wings.backoffice.mlm.repos.InvoiceRepository;
import com.wings.backoffice.mlm.repos.WithdrawalRepository;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class InvoiceService {

    @Autowired
    private SettingsService settingsService;
    @Autowired
    private InvoiceItemRepository invoiceItemRepository;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private WithdrawalRepository withdrawalRepository;
    @Autowired
    private CountriesRepository countriesRepository;

    private final Map<String, Country> countries = new HashMap<>();
    private final Logger log = LoggerFactory.getLogger(InvoiceService.class);

    /**
     *
     * @param order
     * @return
     */
    public Invoice generateInvoice(Order order) {

        //get invoice settings
        Double tax = settingsService.getCountryTax(order.getBillingCountry());
        Invoice i = invoiceRepository.findByOrderId(order.getId());
        if (i == null) {
            i = new Invoice();
        }

        i.setClientAddress(order.getBillingAddress());
        i.setClientCity(order.getBillingCity());
        i.setClientName(order.getBillingName());
        i.setCompanyName(order.getBillingCompany());
        i.setClientState(order.getBillingState());
        i.setInvoiceDate(order.getActivationDate());
        i.setYear(DateUtils.getDateYear(order.getActivationDate()));
        i.setClientCountry(order.getBillingCountry());
        i.setTypeId(order.getId());
        i.setType(order.getType());
        i.setTotal(order.getPaymentAmount());
        i.setOrderId(order.getId());
        i.setToken(InvoiceUtils.getRandomString(15));

        Member member = order.getMember();
        if (member != null) {
            String dni = "";
            //normal dni
            if (member.getDniType() != null && member.getDni() != null) {
                dni = member.getDniType() + " " + member.getDni();
            }
            //idt
            if (member.getIdtType() != null && member.getIdt() != null) {
                dni = member.getIdtType() + " " + member.getIdt();
            }
            i.setClientDni(dni);
        }

        double tot = i.getTotal();
        double imp = tot / tax;
        double iv = tot - imp;

        String imponible = NumberUtils.formatMoney(imp, order.getPaymentCurrency());
        String iva = NumberUtils.formatMoney(iv, order.getPaymentCurrency());
        String total = NumberUtils.formatMoney(tot, order.getPaymentCurrency());

        i.setImponible(imponible);
        i.setIva(iva);

        i.setRetenciones(NumberUtils.formatMoney(0, order.getPaymentCurrency()));
        i.setTotalText(total);

        i.setSended(false);
        i.setStatus("created");

        List<String> types = Arrays.asList(OrderType.kit.name(), OrderType.upgrade.name(), OrderType.store.name(), OrderType.product.name());
        long typeNumber = getMaxTypeNumber(types, order.getBillingCountry());
        i.setTypeNumber(typeNumber);
        i.setInvoiceNumber(InvoiceUtils.getInvoiceNumber(typeNumber, order.getBillingCountry()));

        //TODO: ver para otros paises
        /*i.setTypeNumber(getMaxTypeNumber(order.getType(), i.getYear()));
        if (order.getType().equals(OrderType.kit.name()) || order.getType().equals(OrderType.upgrade.name())) {
            i.setInvoiceNumber(InvoiceUtils.getKitInvoiceNumber(i.getTypeNumber(), i.getYear()));
        } else if (order.getType().equals(OrderType.product.name())) {
            i.setInvoiceNumber(InvoiceUtils.getProductInvoiceNumber(i.getTypeNumber(), i.getYear()));
        } else if (order.getType().equals(OrderType.store.name())) {
            i.setInvoiceNumber(InvoiceUtils.getProductInvoiceNumber(i.getTypeNumber(), i.getYear()));
        }
         */
        invoiceRepository.save(i);

        List<InvoiceItem> items = invoiceItemRepository.findByInvoiceId(i.getId());
        if (!items.isEmpty()) {
            invoiceItemRepository.delete(items);
            items.clear();
        }
        //List<InvoiceItem> items = new ArrayList<>();
        if (order.getItems() != null && !order.getItems().isEmpty()) {

            String concept = (order.getStoreTitle() != null ? order.getStoreTitle() : order.getOrderItem().getName());

            //TODO: mejorar la facturacion con un detalle del detalle            
            InvoiceItem iitem = new InvoiceItem();
            iitem.setAmount(order.getPaymentAmount());
            iitem.setCode(order.getId().toString());
            iitem.setConcept(concept);
            iitem.setInvoiceId(i.getId());
            items.add(iitem);

        } else {

            InvoiceItem iitem = new InvoiceItem();
            iitem.setAmount(order.getPaymentAmount());
            iitem.setCode(order.getId().toString());
            iitem.setConcept(order.getOrderItem().getName());
            iitem.setInvoiceId(i.getId());
            items.add(iitem);

        }

        invoiceItemRepository.save(items);
        i.setItems(items);
        i.setFilePath(generateInvoicePDF(i));
        invoiceRepository.save(i);

        return i;
    }

    /**
     *
     * @param order
     * @return
     */
    public Invoice getInvoiceForOrder(Order order) {
        Invoice i = invoiceRepository.findByOrderId(order.getId());
        if (i == null) {
            i = generateInvoice(order);
        }
        return i;
    }

    public String generateInvoices() {

        /*List<Long> ids = Arrays.asList(40142l, 39929l, 39911l, 39855l, 39851l);
        //List<Order> orders = ordersRepository.findAll(ids);
        List<Order> orders = ordersRepository.findByStatusAndBillingCountryAndDeleted("paid", "PE", Boolean.FALSE);        
        for (Order order : orders) {
            Invoice i = generateInvoice(order);
            serviceInvoice.getInvoice(order, i);
        }*/
        return "OK";
    }

    //OLD METHODS, DEPRECATED
    /*----------------------------------------------------------------------*/
    public String generateInvoices2() {

        if (countries.isEmpty()) {
            List<Country> cs = countriesRepository.findAll();
            cs.forEach((c) -> {
                countries.put(c.getCode(), c);
            });
        }

        List<Invoice> toDelete = invoiceRepository.findByType(OrderType.kit.name());
        toDelete.addAll(invoiceRepository.findByType(OrderType.product.name()));
        toDelete.addAll(invoiceRepository.findByType(OrderType.store.name()));
        toDelete.addAll(invoiceRepository.findByType(OrderType.upgrade.name()));

        invoiceRepository.delete(toDelete);

        List<Order> orders = ordersService.findByActive(true);

        log.error("Orders active = " + orders.size());

        int skiped = 0;
        int created = 0;

        Collections.sort(orders, (Order o1, Order o2) -> o1.getActivationDate().compareTo(o2.getActivationDate()));

        for (Order order : orders) {

            if (order.isDeleted()) {
                skiped++;
                continue;
            }

            //Skip colombian orders
            /*if (order.getSource().equals("store-co")) {
                skiped++;
                continue;
            }*/
            if (order.getType().equals(OrderType.service.name())) {
                skiped++;
                continue;
            }

            created++;

            //skip freeitems
            if (order.getItemId() == 15) {
                continue;
            }
//
//            if (order.getType().equals(OrderType.kit.name()) && (order.getMemberId() == null || order.getMember() == null)) {
//                continue;
//            }
//
//            if (order.getType().equals(OrderType.kit.name()) || order.getType().equals(OrderType.upgrade.name()) || order.getType().equals(OrderType.store.name())) {
//
//                if (order.getMemberId() == null) {
//                    continue;
//                }
//
//                if (order.getMember() == null) {
//                    order.setMember(membersRepository.findOne(order.getMemberId()));
//                }
//
//                if (order.getMember().getDniType() == null || order.getMember().getDniType().trim().isEmpty() || order.getMember().getDniType().equals("0")) {
//                    order.getMember().setDniType("DNI");
//                }
//
//                /*if (!MemberUtils.isProfileComplete(order.getMember())) {
//                    continue;
//                }*/
//            } else if (order.getType().equals(OrderType.product.name())) {
//
//                if (order.getStoreId() == null) {
//                    continue;
//                }
//
//            }

//            Invoice already = invoiceRepository.findByTypeAndTypeId(order.getType(), order.getId());
//            if (already != null) {
//                continue;
//            }
            Invoice i = new Invoice();

            i.setClientAddress(order.getBillingAddress());
            i.setClientCity(order.getBillingCity());
            i.setClientName(order.getBillingName());
            i.setCompanyName(order.getBillingCompany());
            i.setClientState(order.getBillingState());
            i.setInvoiceDate(order.getActivationDate());
            i.setYear(DateUtils.getDateYear(order.getActivationDate()));
            i.setClientCountry(order.getBillingCountry());
            i.setTypeId(order.getId());
            i.setType(order.getType());
            i.setTotal(order.getPaymentAmount());

            Member member = order.getMember();
            if (member != null && member.getDni() != null) {
                i.setClientDni(member.getDni());
            }

            double tot = i.getTotal();
            double imp = tot / 1.21;
            double iv = tot - imp;

            String imponible = NumberUtils.formatMoney(imp);
            String iva = NumberUtils.formatMoney(iv);
            String total = NumberUtils.formatMoney(tot);

            i.setImponible(imponible);
            i.setIva(iva);

            try {
                if (i.getClientCountry() == null || i.getClientCountry().isEmpty()) {
                    i.setClientCountry(order.getMember().getCountry());
                }
            } catch (Exception e) {
            }

            String itemPrefix = "";
            if (i.getClientCountry() != null && !i.getClientCountry().isEmpty()) {
                Country c = countries.get(i.getClientCountry());
                if (c != null && !c.getCode().toLowerCase().equals("es")) {
                    i.setIva("0.0");
                    i.setImponible(total);
                }

                if (c != null && !c.getIsEu()) {
                    itemPrefix = "Cuota de afiliación - ";

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    cal.set(Calendar.MONTH, 5);
                    cal.set(Calendar.YEAR, 2018);

                    cal.set(Calendar.HOUR, 0);
                    cal.set(Calendar.MINUTE, 0);
                    cal.set(Calendar.SECOND, 0);
                    Date firstOfJuly = cal.getTime();

                    //si no es union europea y pago con tefpay o es transferencia y pago antes del 1ro de julio
                    if (order.getPaymentType().equals("tefpay") || (order.getPaymentType().equals("bacs") && order.getActivationDate().before(firstOfJuly))) {
                        //se factura
                    } else {
                        continue;
                    }
                }

            }

            i.setRetenciones(NumberUtils.formatMoney(0));
            i.setTotalText(total);

            i.setSended(false);
            i.setStatus("created");

            i.setTypeNumber(getMaxTypeNumber(order.getType(), i.getYear()));

            if (order.getType().equals(OrderType.kit.name()) || order.getType().equals(OrderType.upgrade.name())) {
                i.setInvoiceNumber(InvoiceUtils.getKitInvoiceNumber(i.getTypeNumber(), i.getYear()));
            } else if (order.getType().equals(OrderType.product.name())) {
                i.setInvoiceNumber(InvoiceUtils.getProductInvoiceNumber(i.getTypeNumber(), i.getYear()));
            } else if (order.getType().equals(OrderType.store.name())) {
                i.setInvoiceNumber(InvoiceUtils.getProductInvoiceNumber(i.getTypeNumber(), i.getYear()));
            }

            if (i.getClientDni() == null || i.getClientDni().isEmpty()) {
                i.setClientDni(order.getDni());
            }

            if (i.getClientDni() == null) {
                i.setClientDni("");
            }

            invoiceRepository.save(i);

            List<InvoiceItem> items = new ArrayList<>();
            if (order.getItems() != null && !order.getItems().isEmpty()) {

                InvoiceItem iitem = new InvoiceItem();
                iitem.setAmount(order.getPaymentAmount());
                iitem.setCode(order.getId().toString());
                iitem.setConcept(itemPrefix + (order.getStoreTitle() != null ? order.getStoreTitle() : order.getOrderItem().getName()));
                iitem.setInvoiceId(i.getId());
                items.add(iitem);

                //TODO: mejorar la facturacion con un detalle del detalle
                /*for (OrderItem item : order.getItems()) {
                    iitem = new InvoiceItem();
                    iitem.setAmount(0d);
                    iitem.setCode(item.getId().toString());
                    iitem.setConcept(item.getName());
                    iitem.setInvoiceId(i.getId());
                    items.add(iitem);
                }*/
            } else {

                InvoiceItem iitem = new InvoiceItem();
                iitem.setAmount(order.getPaymentAmount());
                iitem.setCode(order.getId().toString());
                iitem.setConcept(itemPrefix + order.getOrderItem().getName());
                iitem.setInvoiceId(i.getId());
                items.add(iitem);

            }

            invoiceItemRepository.save(items);
            i.setItems(items);
            i.setFilePath(generateInvoicePDF(i));
            invoiceRepository.save(i);
            //order.setInvoiceId(i.getId());
        }

        ordersService.save(orders);

        log.error("Orders skiped " + skiped + " created " + created);

        generateWithdrawalInvoices();

        return "Facturas generadas";
    }

    public String generateWithdrawalInvoices() {

        invoiceRepository.delete(invoiceRepository.findByType(InvoiceType.withdrawal.name()));

        List<Withdrawal> ws = withdrawalRepository.findByStatus(WithdrawalStatus.completed.name());
        for (Withdrawal w : ws) {

            Invoice already = invoiceRepository.findByTypeAndTypeId(InvoiceType.withdrawal.name(), w.getId());
            if (already != null) {
                continue;
            }

            Invoice i = new Invoice();

            Member m = w.getMember();
            i.setClientAddress(m.getStreet());
            i.setClientCity(m.getCity());
            i.setClientDni(m.getDni());
            i.setClientName(m.toString());
            i.setCompanyName(m.getCompanyName());
            i.setClientState(m.getState());
            i.setInvoiceDate(w.getRequestDate());
            i.setYear(DateUtils.getDateYear(w.getRequestDate()));
            i.setTypeId(w.getId());
            i.setType(InvoiceType.withdrawal.name());
            i.setTotal(w.getMoneyPayable());
            i.setClientCountry(m.getCountry());

            String imponible = NumberUtils.formatMoney(w.getMoneyBase());
            String iva = NumberUtils.formatMoney(w.getMoneyTax());
            String total = NumberUtils.formatMoney(w.getMoneyPayable());
            String ret = NumberUtils.formatMoney(w.getMoneyRetention());

            i.setImponible(imponible);
            i.setIva(iva);
            i.setRetenciones(ret);
            i.setTotalText(total);

            if (i.getClientCountry() != null && !i.getClientCountry().isEmpty()) {
                Country c = countries.get(i.getClientCountry());
                if (c != null && !c.getIsEu()) {
                    i.setIva("0.0");
                    i.setImponible(total);
                }
            }

            i.setSended(false);
            i.setStatus("created");

            i.setTypeNumber(getMaxTypeNumber(InvoiceType.withdrawal.name(), i.getYear()));
            i.setInvoiceNumber(InvoiceUtils.getWithdrawalInvoiceNumber(i.getTypeNumber(), i.getYear()));

            invoiceRepository.save(i);

            List<InvoiceItem> items = new ArrayList<>();

            InvoiceItem iitem = new InvoiceItem();
            iitem.setAmount(w.getMoneyPayable());
            iitem.setCode(w.getId().toString());
            iitem.setConcept("Comisiones por publicidad, programa de afiliados " + m.getUsername());
            iitem.setInvoiceId(i.getId());
            items.add(iitem);

            invoiceItemRepository.save(items);
            i.setItems(items);
            i.setFilePath(generateInvoicePDF(i));
            invoiceRepository.save(i);

        }

        return "";
    }

    public String generateInvoicePDF(Invoice invoice) {

        String logo = "/var/wings/invoices_bo/report/logo.png";
        String jasper = "/var/wings/invoices_bo/report/bo_invoice.jasper";

        String ret = "";
        //check if directory exists
        String dirName = "/var/wings/invoices_bo/invoices/" + invoice.getYear() + "/";
        File dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String destName = dir.getAbsolutePath() + "/" + invoice.getInvoiceNumber() + ".pdf";
        Map<String, Object> map = new HashMap<>();

        JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(invoice.getItems());

        JREmptyDataSource empty = new JREmptyDataSource();
        map.put(JRParameter.REPORT_LOCALE, new Locale("es", "ES"));
        map.put("invoice", invoice);
        map.put("detailds", ds);
        map.put("logo", logo);
        try {

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(jasper));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, map, empty);
            JasperExportManager.exportReportToPdfFile(jprint, destName);
            ret = destName;
        } catch (Exception e) {
            log.error("Error creating invoice", e);
        }
        return ret;
    }

    public long getMaxTypeNumber(String type, int year) {
        return invoiceRepository.getMaxTypeNumber(type, year) + 1;
    }

    public long getMaxTypeNumber(List<String> types, String country) {
        return invoiceRepository.getMaxTypeNumber(types, country) + 1;
    }
}
