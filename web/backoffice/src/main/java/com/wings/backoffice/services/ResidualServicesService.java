/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.ResidualService;
import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.domain.bis.invoices.InvoiceItem;
import com.wings.backoffice.mlm.enums.InvoiceType;
import com.wings.backoffice.mlm.enums.ResidualReference;
import com.wings.backoffice.mlm.enums.ResidualType;
import com.wings.backoffice.mlm.repos.InvoiceItemRepository;
import com.wings.backoffice.mlm.repos.InvoiceRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wings.backoffice.mlm.repos.ResidualServicesRepository;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class ResidualServicesService {

    @Autowired
    private ResidualServicesRepository residualServicesRepository;

    @Autowired
    private GsmLinesService gsmLinesService;

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private InvoiceItemRepository invoiceItemRepository;

    private final Logger log = LoggerFactory.getLogger(ResidualService.class);

    /**
     *
     */
    @Transactional
    public void importGsmLineServices() {

        //temp delte after        
        List<ResidualService> lines = gsmLinesService.getLineServices();
        List<ResidualService> linesToSave = new ArrayList<>();

        for (ResidualService line : lines) {
            /*ResidualService already = residualServicesRepository.findByReferenceAndReferenceId(line.getReference(), line.getReferenceId());
            if (already != null) {
                //already inserted, must update existing ones
                continue;
            }*/

            line.setPersonal(false);

            //find afiliates
            List<Member> members = membersRepository.findByDni(line.getIdValue());
            if (members.size() == 1) {

                line.setMemberId(members.get(0).getId());
                line.setPersonal(Boolean.TRUE);

                if (line.getStoreId() == null || line.getStoreId().isEmpty()) {
                    line.setStoreId(members.get(0).getUsername());
                    log.error("Actualizacion requerida linea: " + line.getReferenceId() + " sponsor: " + members.get(0).getUsername());
                }

            } else {
                members = membersRepository.findByEmail(line.getEmail());
                if (members.size() == 1) {
                    //line.setMemberId(members.get(0).getId());
                    //line.setPersonal(Boolean.TRUE);

                    if (line.getStoreId() == null || line.getStoreId().isEmpty()) {
                        line.setStoreId(members.get(0).getUsername());
                        log.error("Actualizacion requerida linea: " + line.getReferenceId() + " sponsor: " + members.get(0).getUsername());
                    }
                }

            }

            line.setReference(ResidualReference.gsm.name());
            line.setType(ResidualType.line.name());
            linesToSave.add(line);

        }

        residualServicesRepository.deleteAll();
        residualServicesRepository.save(linesToSave);

    }

    /**
     *
     * @return
     */
    public String importLineInvoices() {

        List<Invoice> toDelete = invoiceRepository.findByType(InvoiceType.service.name());
        invoiceRepository.delete(toDelete);

        List<ResidualService> lines = residualServicesRepository.findByTypeAndPersonal(ResidualType.line.name(), Boolean.TRUE);
        List<Invoice> ret = gsmLinesService.getLinesInvoices(lines);

        ret.stream().map((invoice) -> {
            int year = DateUtils.getDateYear(invoice.getInvoiceDate());
            long typeNumber = invoiceRepository.getMaxTypeNumber(InvoiceType.service.name(), year);
            typeNumber++;
            invoice.setTypeNumber(typeNumber);
            invoice.setClientCountry("ES");
            return invoice;
        }).forEachOrdered((invoice) -> {
            invoiceRepository.save(invoice);

            List<InvoiceItem> items = invoice.getItems();
            items.forEach((item) -> {
                item.setInvoiceId(invoice.getId());
            });
            invoiceItemRepository.save(items);

        });

        return "Facturas importadas";
    }
}
