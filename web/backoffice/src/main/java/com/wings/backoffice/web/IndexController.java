/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.dao.DifferentialDAO;
import com.wings.backoffice.mlm.dao.DifferentialSums;
import com.wings.backoffice.mlm.domain.CountryOpen;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import com.wings.backoffice.mlm.domain.utils.MemberSnapshot;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.CountriesRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.services.BitwingsService;
import com.wings.backoffice.services.CountryOpenService;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.services.MemberSnapshotService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.promos.PromoLatamService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.MemberUtils;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class IndexController {

    @Autowired
    private MemberSnapshotService memberSnapshotService;
    @Autowired
    private MediaService mediaService;
    @Autowired
    private DifferentialDAO differentialDAO;
    @Autowired
    private BalanceDAO balanceDAO;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private SessionRegistry sessionRegistry;
    @Autowired
    private LogsService logsService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private BitwingsService bitwingsService;
    @Autowired
    private PromoLatamService promoLatamService;
    @Autowired
    private CountriesRepository countriesRepository;
    @Autowired
    private RanksBisRepository ranksBisRepository;
    @Autowired
    private CountryOpenService countryOpenService;

    private int loggedIn;
    private String loggedInDetails;

    @Value("${server.session.timeout}")
    private String serverSessionTimeout;

    private final List<String> dnis = Arrays.asList("DNI", "NIF", "PAS", "TAR", "NIE", "C.C.", "C.I.");

    private final List<String> idts = Arrays.asList("CIF", "CPF", "CUIT", "NIE", "NIF", "NIT", "RUT", "RUC", "RFC", "RIF");

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String getIndex(Model model, HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails detail = (UserDetails) auth.getPrincipal();

        if (detail instanceof AdminUser) {
            return "redirect:/admin";
        } else {
            //if menu != complete hide menu 
            Member member = AuthUtils.getMemberUser();

            model.addAttribute("rank", ranksBisRepository.findOne(member.getRankId()));

            Locale locale = LocaleContextHolder.getLocale();
            String acode = mediaService.getAvatarCode(member);
            model.addAttribute("avatar", "/open/media/" + acode);
            model.addAttribute("member", member);
            request.getSession().setAttribute("member", member);

            model.addAttribute("lang", locale.getLanguage());
            model.addAttribute("countries", countriesRepository.findAll());
            model.addAttribute("dnis", dnis);
            model.addAttribute("idts", idts);
            model.addAttribute("ranks", ranksBisRepository.findAll());
            model.addAttribute("contractSigned", member.getContractSigned());

            Boolean ipc = MemberUtils.isProfileComplete(member);
            model.addAttribute("profileComplete", ipc);
            if (!ipc) {
                return "redirect:/required";
            } else {
                return "redirect:/members";
            }
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String getIndexAdmin(Model model, HttpServletRequest request) {

        AdminUser admin = AuthUtils.getAdminUser();
        if (admin == null) {
            return "redirect:/login";
        }

        model.addAttribute("firstName", admin.getUsername());
        model.addAttribute("lastName", "");
        model.addAttribute("is_admin", true);
        model.addAttribute("impersonating", false);
        model.addAttribute("user", AuthUtils.getAdminUser());

        return "index";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members"}, method = RequestMethod.GET)
    public String getIndexMember(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        if (m == null) {
            return "redirect:/";
        }
        m = membersRepository.findOne(m.getId());
        model.addAttribute("firstName", m.getFirstName());
        model.addAttribute("lastName", m.getLastName());
        model.addAttribute("username", m.getUsername());
        model.addAttribute("is_admin", false);

        model.addAttribute("impersonating", AuthUtils.getAdminUser() != null);

        //get user balance
        BalanceSums total = balanceDAO.find(null, null, null, null, null, m.getId(), null);
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        total.setLocale(locale);
        model.addAttribute("total", total);

        String acode = mediaService.getAvatarCode(m);
        model.addAttribute("userAvatar", "/open/media/" + acode);

        model.addAttribute("rank", ranksBisRepository.findOne(m.getRankId()));

        Boolean ipc = MemberUtils.isProfileComplete(m);
        model.addAttribute("profileComplete", ipc);
        if (!ipc) {
            //if menu != complete hide menu
            model.addAttribute("avatar", "/open/media/" + acode);
            model.addAttribute("member", m);
            model.addAttribute("lang", locale.getLanguage());
            model.addAttribute("countries", countriesRepository.findAll());
            model.addAttribute("dnis", dnis);
            model.addAttribute("idts", idts);
            model.addAttribute("ranks", ranksBisRepository.findAll());
            model.addAttribute("contractSigned", m.getContractSigned());
            return "redirect:/required";
        } else {
            return "index";
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"views/main"}, method = RequestMethod.GET)
    public String getMain(Model model, HttpServletRequest request) {

        String urlReferer = request.getHeader("referer");
        if (urlReferer == null) {
            urlReferer = request.getHeader("Referer");
        }

        String view = "members/main";
        if ((urlReferer != null && urlReferer.contains("admin"))) {

            //model.addAttribute("sessions", sessionListener.getCounter());
            listLoggedInUsers();
            model.addAttribute("sessions", loggedIn);
            model.addAttribute("sessionsDetail", loggedInDetails);

            long total = membersRepository.count();
            model.addAttribute("members", total);

            Long nonDeleted = membersRepository.countByDeleted(Boolean.FALSE);
            Long deleted = membersRepository.countByDeleted(Boolean.TRUE);
            Long pending = membersRepository.countByOrderStatus(OrderStatus.pending.name());

            model.addAttribute("nonDeleted", nonDeleted);
            model.addAttribute("deleted", deleted);
            model.addAttribute("pending", pending);

            model.addAttribute("logs", logsService.getMemberAccessLog(null));
            model.addAttribute("logs_admin", logsService.getAdminAccessLog());
            model.addAttribute("logs_edit", logsService.getEditLog());

            view = "admin/main";
        } else {

            Member m = AuthUtils.getMemberUser();
            if (m == null) {
                return "redirect:/";
            }

            m = membersRepository.findOne(m.getId());
            AuthUtils.setMember(m);
            MemberSnapshot snap = memberSnapshotService.getLastMemberSnapshot(m.getId());
            model.addAttribute("snap", snap);

            DifferentialSums diff = differentialDAO.find(null, m.getUsername(), null, null, false);
            DifferentialSums match = differentialDAO.find(null, m.getUsername(), null, null, true);
            BalanceSums direct = balanceDAO.find(m.getUsername(), null, null, OrderType.product.name(), null, m.getId(), null);
            BalanceSums residual = balanceDAO.find(m.getUsername(), null, null, OrderType.service.name(), null, m.getId(), null);

            Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
            diff.setLocale(locale);
            match.setLocale(locale);
            direct.setLocale(locale);
            residual.setLocale(locale);

            model.addAttribute("diff", diff);
            model.addAttribute("match", match);
            model.addAttribute("direct", direct);
            model.addAttribute("residual", residual);

            boolean canUpgrade = true;
            //pago en cuotas, primero por que genera las cuotas si no existen
            List<Order> cuotas = ordersService.getCuotasOrder(m);
            //vemos si le falta pagar alguna cuota
            if (!cuotas.isEmpty()) {
                for (Order cuota : cuotas) {
                    if (!cuota.getStatus().equals(OrderStatus.paid.name())) {
                        canUpgrade = false;
                        break;
                    }
                }
            }

            //todas las ordenes del afiliado            
            List<Order> orders = ordersService.getMemberOrders(m);
            model.addAttribute("orders", orders);

            //verificamos si puede hacer upgrade
            Long id = m.getOrder().getOrderItem().getId();
            for (Order order : orders) {

                if (order.getType().equals(OrderType.upgrade.name())) {
                    id = order.getItemId();
                }

                if (!order.getStatus().equals(OrderStatus.paid.name())) {
                    canUpgrade = false;
                }
            }

            //upgrade
            //String storeItemId = "%" + String.valueOf(id) + "%";
            //List<StoreItem> items = storeItemsRepository.findByAvailableToLike(storeItemId);
            List<StoreItem> items = ordersService.getAvailaveUpgrades(m);

            if (!canUpgrade) {
                items.clear();
            }

            model.addAttribute("reclaimBwn", false);
            //Bitwings reclaim
            if (m.getBwnWallet() == null) {
                int bwn = bitwingsService.canReclaim(m.getId());
                model.addAttribute("bwnAmount", bwn);
                model.addAttribute("reclaimBwn", bwn != -1);
            }

            PromoLatam latam = promoLatamService.getPromoLatamForMember(m.getId());
            model.addAttribute("latam", latam);

            CountryOpen copen = countryOpenService.getCountryOpen(m.getCountry());
            model.addAttribute("copen", copen);

            //membersService.countIndirect(m);
            model.addAttribute("storeItems", items);

            model.addAttribute("member", m);
            model.addAttribute("rank", ranksBisRepository.findOne(m.getRankId()));
            model.addAttribute("lang", locale.getLanguage());
            model.addAttribute("countries", countriesRepository.findAll());
            model.addAttribute("dnis", dnis);
            model.addAttribute("idts", idts);
            model.addAttribute("profileComplete", true);
            model.addAttribute("ranks", ranksBisRepository.findAll());
            model.addAttribute("contractSigned", m.getContractSigned());
            model.addAttribute("lang", locale.getLanguage());
        }
        return view;
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/bitwings/claim"}, method = {RequestMethod.POST})
    public ResponseEntity<String> claimBwn(Model model, HttpServletRequest request) {

        String wallet = request.getParameter("wallet");
        int validation = bitwingsService.checkValid(wallet);

        if (validation == 0) {
            return ResponseEntity.badRequest().body("La direccion de la billetera no es válida");
        }

        Member m = AuthUtils.getMemberUser();
        m = membersRepository.findOne(m.getId());
        if (m.getBwnWallet() != null) {
            return ResponseEntity.badRequest().body("La billetera ya se asigno");
        }

        int bwn = bitwingsService.canReclaim(m.getId());
        m.setBwnClaim(bwn);
        m.setBwnWallet(wallet);
        m.setBwnSent(false);
        membersRepository.save(m);

        return ResponseEntity.ok("Billetera registrada correctamente.");
    }

    /**
     *
     */
    private void listLoggedInUsers() {
        final List<Object> allPrincipals = sessionRegistry.getAllPrincipals();

        StringBuilder b = new StringBuilder();
        loggedIn = 0;
        for (final Object principal : allPrincipals) {
            List<SessionInformation> activeUserSessions = sessionRegistry.getAllSessions(principal, true);
            if (!activeUserSessions.isEmpty()) {
                for (SessionInformation a : activeUserSessions) {
                    long server = Long.parseLong(serverSessionTimeout);
                    long sess = DateUtils.elapsedTimeInSeconds(a.getLastRequest());
                    if (server < sess) {
                        sessionRegistry.removeSessionInformation(a.getSessionId());
                    }
                    String expired = a.isExpired() ? " (e)" : " (a)";
                    b.append(a.getPrincipal()).append(" - ").append(DateUtils.elapsedTime(a.getLastRequest())).append(expired).append(System.lineSeparator());
                }
                loggedIn++;
            }
        }
        loggedInDetails = b.toString();
    }

}
