package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItemAttribute;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author lucas
 */
public interface StoreItemAttributesRepository extends JpaRepository<StoreItemAttribute, Long> {

    List<StoreItemAttribute> findByStoreItemId(Long storeItemId);

}
