/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.dao.DifferentialDAO;
import com.wings.backoffice.mlm.dao.DifferentialSums;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.Differential;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.specs.DifferentialSpecs;
import com.wings.backoffice.services.BonusService;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.backoffice.mlm.repos.DifferentialRepository;
import com.wings.backoffice.mlm.specs.BalanceSpecs;

/**
 *
 * @author seba
 */
@Controller
public class BonusController {

    @Autowired
    private BonusService bonusService;

    @Autowired
    private DifferentialRepository differentialBisRepository;

    @Autowired
    private DifferentialDAO differentialDAO;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private BalanceDAO balanceDAO;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = {"/admin/bonus/differential"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String offer = request.getParameter("offer");
        String receive = request.getParameter("receive");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        Sort.Order dateSort = new Sort.Order(Sort.Direction.ASC, "date");
        Sort.Order offerUserIdsort = new Sort.Order(Sort.Direction.DESC, "offerUserId");
        PageRequest pr = new PageRequest(page, size, new Sort(dateSort, offerUserIdsort));

        //Page<Member> items = membersRepository.findAll(MemberSpecs.memberSearch(q), pr);
        Page<Differential> items = differentialBisRepository.findAll(DifferentialSpecs.differentialSearch(offer, receive, f, t, false), pr);
        DifferentialSums sums = differentialDAO.find(offer, receive, f, t, false);

        PageWrapper<Differential> pageWrapper = new PageWrapper<>(items, "admin/bonus/differential");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("offer", offer);
        model.addAttribute("receive", receive);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("sums", sums);
        model.addAttribute("size", ssize);

        return "admin/bonus/differential";
    }

    @RequestMapping(value = {"/admin/bonus/differential/event/{id}"}, method = RequestMethod.GET)
    public String viewEvent(Model model, HttpServletRequest request, @PathVariable Long id) {
        List<Differential> diff = differentialBisRepository.findByEventIdAndMatchingOrderByReceiveUserIdDesc(id, false);
        List<Differential> matchs = differentialBisRepository.findByEventIdAndMatchingOrderByReceiveUserIdDesc(id, true);
        model.addAttribute("diffs", diff);
        model.addAttribute("matchs", matchs);

        return "admin/bonus/differential_event";
    }

    @RequestMapping(value = {"/admin/bonus/matching"}, method = RequestMethod.GET)
    public String getMatching(Model model, HttpServletRequest request) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String offer = request.getParameter("offer");
        String receive = request.getParameter("receive");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));

        Page<Differential> items = differentialBisRepository.findAll(DifferentialSpecs.differentialSearch(offer, receive, f, t, true), pr);
        DifferentialSums sums = differentialDAO.find(offer, receive, f, t, true);

        PageWrapper<Differential> pageWrapper = new PageWrapper<>(items, "admin/bonus/matching");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("offer", offer);
        model.addAttribute("receive", receive);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("sums", sums);
        model.addAttribute("size", ssize);

        return "admin/bonus/matching";
    }

    @RequestMapping(value = {"/admin/bonus/direct-indirect"}, method = RequestMethod.GET)
    public String getDirectIndirect(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(q, f, t, OrderType.product.name(), type, null, null), pr);
        BalanceSums sums = balanceDAO.find(q, f, t, OrderType.product.name(), type, null, null);

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/admin/bonus/direct-indirect");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);
        model.addAttribute("type", type);

        return "admin/bonus/direct-indirect";
    }

    @RequestMapping(value = {"/admin/bonus/residual"}, method = RequestMethod.GET)
    public String getResidual(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(q, f, t, OrderType.service.name(), null, null, null), pr);
        BalanceSums sums = balanceDAO.find(q, f, t, OrderType.service.name(), null, null, null);

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/admin/bonus/residual");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);

        return "admin/bonus/residual";
    }

    @RequestMapping(value = {"/admin/bonus/speed"}, method = RequestMethod.GET)
    public String viewSpeedBonus(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(q, f, t, "speed", null, null, null), pr);
        BalanceSums sums = balanceDAO.find(q, f, t, "speed", null, null, null);

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/admin/bonus/speed");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);

        return "admin/bonus/speed";
    }

}
