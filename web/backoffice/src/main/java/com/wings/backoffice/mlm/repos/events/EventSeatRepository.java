/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventSeat;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface EventSeatRepository extends JpaRepository<EventSeat, Long> {

    Page<EventSeat> findAllByEventId(Long eventId, Pageable page);

    Page<EventSeat> findAllByEventIdAndTakenAndPoolIdIn(Long eventId, Boolean taken, List<Long> poolIds, Pageable page);

    int countByEventId(Long eventId);

    @Transactional
    int deleteByPoolId(Long poolId);

}
