/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api.store;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.services.ApiAuthService;
import com.wings.backoffice.services.OrdersService;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller("apiOrdersController")
@RequestMapping(value = "api/v1/")
public class OrdersController {

    @Autowired
    private ApiAuthService apiAuthService;
    @Autowired
    private OrdersService ordersService;

    @RequestMapping(value = "orders/{id}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, String>> getCategories(HttpServletRequest request, @PathVariable("id") Long orderId) {

        Map<String, String> ret = new HashMap<>();
        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        Order order = ordersService.getOrder(orderId);
        ret.put("orderId", String.valueOf(orderId));

        if (order == null) {
            ret.put("error", "true");
            ret.put("status", "notfound");
            return new ResponseEntity<>(ret, HttpStatus.NOT_FOUND);
        }

        ret.put("error", "false");
        ret.put("status", order.getStatus());
        return new ResponseEntity<>(ret, HttpStatus.OK);

    }

}
