/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.ecuador;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class EcuadorTrackingResponseItem {

    @JsonProperty("numero_guia")
    private Integer numeroGuia;
    @JsonProperty("tipo_logistica")
    private String tipoLogistica;
    @JsonProperty("tipo_guia")
    private String tipoGuia;
    @JsonProperty("cliente")
    private String cliente;
    @JsonProperty("sucursal")
    private String sucursal;
    @JsonProperty("dependencia")
    private String dependencia;
    @JsonProperty("detalle1")
    private Object detalle1;
    @JsonProperty("detalle2")
    private String detalle2;
    @JsonProperty("detalle3")
    private Object detalle3;
    @JsonProperty("destino")
    private String destino;
    @JsonProperty("origen")
    private String origen;
    @JsonProperty("remitente")
    private String remitente;
    @JsonProperty("razon_social_remitente")
    private String razonSocialRemitente;
    @JsonProperty("direccion_remitente")
    private String direccionRemitente;
    @JsonProperty("sector_remitente")
    private Object sectorRemitente;
    @JsonProperty("telefono1_remitente")
    private String telefono1Remitente;
    @JsonProperty("telefono2_remitente")
    private Object telefono2Remitente;
    @JsonProperty("destinatario")
    private Object destinatario;
    @JsonProperty("razon_social_destinatario")
    private String razonSocialDestinatario;
    @JsonProperty("direccion_destinatario")
    private String direccionDestinatario;
    @JsonProperty("sector_destinatario")
    private Object sectorDestinatario;
    @JsonProperty("producto")
    private String producto;
    @JsonProperty("contenido")
    private String contenido;
    @JsonProperty("cantidad")
    private Integer cantidad;
    @JsonProperty("valor_mercancia")
    private Double valorMercancia;
    @JsonProperty("valor_asegurado")
    private Double valorAsegurado;
    @JsonProperty("largo")
    private Double largo;
    @JsonProperty("ancho")
    private Double ancho;
    @JsonProperty("alto")
    private Double alto;
    @JsonProperty("peso_volumetrico")
    private Double pesoVolumetrico;
    @JsonProperty("peso_fisico")
    private Double pesoFisico;
    @JsonProperty("fecha")
    private String fecha;
    @JsonProperty("estado")
    private String estado;
    @JsonProperty("registrado_por")
    private String registradoPor;

    public Integer getNumeroGuia() {
        return numeroGuia;
    }

    public void setNumeroGuia(Integer numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    public String getTipoLogistica() {
        return tipoLogistica;
    }

    public void setTipoLogistica(String tipoLogistica) {
        this.tipoLogistica = tipoLogistica;
    }

    public String getTipoGuia() {
        return tipoGuia;
    }

    public void setTipoGuia(String tipoGuia) {
        this.tipoGuia = tipoGuia;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public Object getDetalle1() {
        return detalle1;
    }

    public void setDetalle1(Object detalle1) {
        this.detalle1 = detalle1;
    }

    public String getDetalle2() {
        return detalle2;
    }

    public void setDetalle2(String detalle2) {
        this.detalle2 = detalle2;
    }

    public Object getDetalle3() {
        return detalle3;
    }

    public void setDetalle3(Object detalle3) {
        this.detalle3 = detalle3;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public String getRazonSocialRemitente() {
        return razonSocialRemitente;
    }

    public void setRazonSocialRemitente(String razonSocialRemitente) {
        this.razonSocialRemitente = razonSocialRemitente;
    }

    public String getDireccionRemitente() {
        return direccionRemitente;
    }

    public void setDireccionRemitente(String direccionRemitente) {
        this.direccionRemitente = direccionRemitente;
    }

    public Object getSectorRemitente() {
        return sectorRemitente;
    }

    public void setSectorRemitente(Object sectorRemitente) {
        this.sectorRemitente = sectorRemitente;
    }

    public String getTelefono1Remitente() {
        return telefono1Remitente;
    }

    public void setTelefono1Remitente(String telefono1Remitente) {
        this.telefono1Remitente = telefono1Remitente;
    }

    public Object getTelefono2Remitente() {
        return telefono2Remitente;
    }

    public void setTelefono2Remitente(Object telefono2Remitente) {
        this.telefono2Remitente = telefono2Remitente;
    }

    public Object getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Object destinatario) {
        this.destinatario = destinatario;
    }

    public String getRazonSocialDestinatario() {
        return razonSocialDestinatario;
    }

    public void setRazonSocialDestinatario(String razonSocialDestinatario) {
        this.razonSocialDestinatario = razonSocialDestinatario;
    }

    public String getDireccionDestinatario() {
        return direccionDestinatario;
    }

    public void setDireccionDestinatario(String direccionDestinatario) {
        this.direccionDestinatario = direccionDestinatario;
    }

    public Object getSectorDestinatario() {
        return sectorDestinatario;
    }

    public void setSectorDestinatario(Object sectorDestinatario) {
        this.sectorDestinatario = sectorDestinatario;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getValorMercancia() {
        return valorMercancia;
    }

    public void setValorMercancia(Double valorMercancia) {
        this.valorMercancia = valorMercancia;
    }

    public Double getValorAsegurado() {
        return valorAsegurado;
    }

    public void setValorAsegurado(Double valorAsegurado) {
        this.valorAsegurado = valorAsegurado;
    }

    public Double getLargo() {
        return largo;
    }

    public void setLargo(Double largo) {
        this.largo = largo;
    }

    public Double getAncho() {
        return ancho;
    }

    public void setAncho(Double ancho) {
        this.ancho = ancho;
    }

    public Double getAlto() {
        return alto;
    }

    public void setAlto(Double alto) {
        this.alto = alto;
    }

    public Double getPesoVolumetrico() {
        return pesoVolumetrico;
    }

    public void setPesoVolumetrico(Double pesoVolumetrico) {
        this.pesoVolumetrico = pesoVolumetrico;
    }

    public Double getPesoFisico() {
        return pesoFisico;
    }

    public void setPesoFisico(Double pesoFisico) {
        this.pesoFisico = pesoFisico;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getRegistradoPor() {
        return registradoPor;
    }

    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

}
