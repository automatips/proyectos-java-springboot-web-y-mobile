package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItemI18n;
import java.math.BigInteger;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author lucas
 */
public interface StoreItemI18nRepository extends JpaRepository<StoreItemI18n, Long> {

    List<StoreItemI18n> findByStoreItemId(Long storeItemId);

}
