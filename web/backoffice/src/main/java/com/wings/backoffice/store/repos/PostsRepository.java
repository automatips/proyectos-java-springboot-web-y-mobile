/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.repos;

import com.wings.backoffice.store.domain.Post;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface PostsRepository extends JpaRepository<Post, Long> {

    List<Post> findByPostType(String postType);

    List<Post> findByPostTypeAndPostStatus(String postType, String postStatus);

}
