/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.events.enums;

/**
 *
 * @author seba
 */
public enum EventTicketStatus {

    /**
     * creado, nadi ha comprado un ticket
     */
    created,
    /**
     * asinado, alguien ya compro este ticket
     */
    assigned,
    /**
     * invitado, se envio la invitacion del ticket
     */
    invited,
    /**
     * alguien visito la pagina del ticket
     */
    visited,
    /**
     * reclamado, alguien ya se registro para usar este ticket
     */
    reclaimed,
    /**
     * Liberado, estaba reclamado y ahora esta liberado para volver a usar
     */
    released,
    /**
     * completado, ya usado para entrar al evento
     */
    completed,
    /**
     * evento finalizao y la persona no entro al evento
     */
    expired,
}
