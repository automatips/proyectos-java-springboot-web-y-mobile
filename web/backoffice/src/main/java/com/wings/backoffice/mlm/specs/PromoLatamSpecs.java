/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class PromoLatamSpecs {

    public static Specification<PromoLatam> search(String q, String country, Boolean qualify) {

        return (Root<PromoLatam> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("firstName"), search),
                                builder.like(root.get("lastName"), search),
                                builder.like(root.get("username"), search),
                                builder.like(root.get("email"), search),
                                builder.like(builder.concat(builder.concat(root.get("firstName"), " "), root.get("lastName")), search)
                        )
                );
            }

            if (qualify != null) {
                predicates.add(builder.equal(root.get("qualify"), qualify));
            }

            if (country != null) {
                predicates.add(builder.equal(root.get("country"), country));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
