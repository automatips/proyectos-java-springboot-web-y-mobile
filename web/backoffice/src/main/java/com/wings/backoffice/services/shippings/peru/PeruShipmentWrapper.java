/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.peru;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author seba
 */
public class PeruShipmentWrapper {

    @JsonProperty("numero_pedido")
    private String nroPedido;
    @JsonProperty("identificador_destina")
    private String identificador;
    @JsonProperty("nombre_destina")
    private String nombre;
    @JsonProperty("telefono_destina")
    private String telefono;
    @JsonProperty("direccion_destina")
    private String direccion;
    @JsonProperty("departamento_destina")
    private String departamento;
    @JsonProperty("provincia_destina")
    private String provincia;
    @JsonProperty("distrito_destina")
    private String distrito;
    @JsonProperty("bultos")
    private int bultos;
    @JsonProperty("items")
    private List<PeruShipmentWrapperItem> items;

    public String getNroPedido() {
        return nroPedido;
    }

    public void setNroPedido(String nroPedido) {
        this.nroPedido = nroPedido;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public int getBultos() {
        return bultos;
    }

    public void setBultos(int bultos) {
        this.bultos = bultos;
    }

    public List<PeruShipmentWrapperItem> getItems() {
        return items;
    }

    public void setItems(List<PeruShipmentWrapperItem> items) {
        this.items = items;
    }

}
