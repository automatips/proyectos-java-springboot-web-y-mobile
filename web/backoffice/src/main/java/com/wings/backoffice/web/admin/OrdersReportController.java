/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.services.ReportService;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class OrdersReportController {

    @Autowired
    private ReportService reportService;

    @Deprecated
    @RequestMapping(value = {"/admin/reports/orders/productsByCountry"}, method = RequestMethod.GET)
    public String productsByCountry(Model model, HttpServletRequest request) {

        String country = request.getParameter("country");
        if (country == null) {
            country = "all";
        } else {
            Map<String, Object> reports = reportService.getDeliveriesPerCountry(country);
            model.addAttribute("reports", reports);
        }

        model.addAttribute("country", country);

        return "admin/reports/orders/productsByCountry";
    }
}