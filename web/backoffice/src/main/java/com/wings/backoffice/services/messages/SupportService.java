/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.messages;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.messages.SupportMessage;
import com.wings.backoffice.mlm.repos.messages.SupportMessageAttachmentRepository;
import com.wings.backoffice.mlm.repos.messages.SupportMessageRepository;
import com.wings.backoffice.mlm.specs.SupportMessagesSpecs;
import com.wings.backoffice.utils.PageWrapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import com.wings.backoffice.services.Result;
import com.wings.backoffice.utils.InvoiceUtils;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Service
public class SupportService {

    @Autowired
    private SupportMessageRepository supportMessageRepository;
    @Autowired
    private SupportMessageAttachmentRepository supportMessageAttachmentRepository;

    private final Map<String, String> mimeTypes = new HashMap<String, String>() {
        {
            put("image/png", ".png");
            put("image/jpeg", ".jpg");
            put("application/pdf", ".pdf");
            put("text/plain", ".txt");
            put("video/mp4", ".mp4");
        }
    };

    /**
     *
     * @param message
     * @return
     */
    public void save(SupportMessage message) {
        supportMessageRepository.save(message);
    }

    /**
     *
     * @param memberId
     * @param page
     * @param size
     * @return
     */
    public Page<SupportMessage> getByMember(Long memberId, int page, int size) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate", "id"));
        return supportMessageRepository.findByMemberId(memberId, pr);
    }

    /**
     *
     * @return
     */
    public List<SupportMessage> getAll() {
        return supportMessageRepository.findAll();
    }

    /**
     *
     * @param q
     * @param spage
     * @param ssize
     * @param status
     * @param country
     * @param assignation
     * @param user
     * @return
     */
    public PageWrapper<SupportMessage> search(UserDetails user, String q, String spage, String ssize, String status, String country, String assignation) {
        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;
        Sort sort = new Sort(user instanceof AdminUser ? Sort.Direction.ASC : Sort.Direction.DESC, "creationDate", "id");
        PageRequest pr = new PageRequest(page, size, sort);
        Page<SupportMessage> list = user instanceof AdminUser
                ? supportMessageRepository.findAll(SupportMessagesSpecs.search(q, status, country, assignation, null), pr)
                : supportMessageRepository.findByMemberId(((Member) user).getId(), pr);
        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > list.getTotalElements()) {
            hasta = list.getTotalElements();
        }
        PageWrapper<SupportMessage> pageWrapper = new PageWrapper<>(list, "support/incidences");
        String summary = "Mostrando " + desde + " a " + hasta + " de " + list.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        return pageWrapper;
    }

    /**
     *
     * @param id
     * @return
     */
    public SupportMessage getOne(Long id) {
        return supportMessageRepository.findOne(id);
    }

    public Result<String> save(UserDetails user, String action, String title, String description, MultipartFile attachment) {
        Result<String> result = new Result<>();
        try {
            if (title == null || title.trim().isEmpty()) {
                result.addErrorMessage("El título no puede estar vacio");
            }

            if (description == null || description.trim().isEmpty()) {
                result.addErrorMessage("La descripcón no puede estar vacia");
            }
            String attachmentFile = null;
            if (attachment != null && !attachment.isEmpty()) {
                try {
                    if (mimeTypes.containsKey(attachment.getContentType())) {
                        String prefix = InvoiceUtils.getRandomString(12);
                        String suffix = mimeTypes.get(attachment.getContentType());
                        String fileName = "/var/wings/bo_media/attachments/" + prefix + suffix;
                        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                        stream.write(attachment.getBytes());
                        attachmentFile = prefix + suffix;
                    } else {
                        result.addErrorMessage("Formato de archivo no soportado");
                    }
                } catch (Exception ex) {
                    result.addErrorMessage("Error subiendo el adjunto! -> " + ex.getMessage());
                }
            }

            if (result.isOk()) {
                boolean isAdmin = user instanceof AdminUser;
                SupportMessage m = new SupportMessage();
                m.setCreationDate(new Date());
                m.setStatus("open");
                m.setTitle(title.trim());
                m.setDescription(description.trim());
                m.setAttachment(attachmentFile);
                m.setAssignedTo(isAdmin ? "admin" : "support");

                if (isAdmin && "admin".equals(action)) {
                    AdminUser admin = (AdminUser) user;
                    m.setMemberId(-1l);
                    m.setUsername(admin.getEmail());
                    m.setFirstName(admin.getUsername());
                    m.setLastName("");
                } else {
                    Member member = (Member) user;
                    m.setMemberId(member.getId());
                    m.setCountry(member.getCountry());
                    m.setLastName(member.getLastName());
                    m.setFirstName(member.getFirstName());
                    m.setEmail(member.getEmail());
                    m.setUsername(member.getUsername());
                }

                save(m);

                result.addMessage("Incidencia creada con éxito");
            }
        } catch (ConstraintViolationException e) {
            result.addErrorMessage("El título o la descripción tienen una longitud no permitida.");
        }

        return result;
    }

    public Result<String> update(AdminUser admin, Long incidenceId, String action, String closeMessage, String status, String asignation) {
        Result<String> result = new Result<>();
        SupportMessage message = getOne(incidenceId);
        action = action == null? "" : action;

        switch (action) {
            case "close":
                message.setStatus("closed");
                message.setClosedDate(new Date());
                message.setClosedMessage(closeMessage);
                message.setClosedUser(admin.getUsername());
                break;
            case "update-status":
                message.setClosedDate(null);
                message.setClosedMessage(null);
                message.setClosedUser(null);
                message.setStatus(status);
                break;
            case "change-asignation":
                message.setAssignedTo(asignation);
                break;
            default:
                result.addErrorMessage("Acción no reconocida!");
                break;
        }

        if (result.isOk()) {
            save(message);
            result.addMessage("Incidencia modificada con éxito.");
        }
        return result;
    }
}
