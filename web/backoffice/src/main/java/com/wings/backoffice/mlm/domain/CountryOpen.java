/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

/**
 *
 * @author seba
 */
public class CountryOpen {

    private String country;
    private int level;
    private long golden;
    private long diamond;
    private long membersCount;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public long getGolden() {
        return golden;
    }

    public void setGolden(long golden) {
        this.golden = golden;
    }

    public long getDiamond() {
        return diamond;
    }

    public void setDiamond(long diamond) {
        this.diamond = diamond;
    }

    public void setMembersCount(long membersCount) {
        this.membersCount = membersCount;
    }

    public long getMembersCount() {
        return membersCount;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "g: " + getGoldenPercent() + "% d: " + getDiamondPercent() + "% m1: " + getMembersPercentLevel1() + "%  m2: " + getMembersPercentLevel2() + "%";
    }

    public String getGoldenText() {
        return (golden > 200 ? 200 : golden) + "/200";
    }

    public int getGoldenPercent() {
        int ret;
        ret = (int) (golden * 0.5);
        if (ret > 100) {
            ret = 100;
        }
        return ret;
    }

    public String getDiamondText() {
        return (diamond > 100 ? 100 : diamond) + "/100";
    }

    public int getDiamondPercent() {
        int ret;
        ret = (int) diamond;
        if (ret > 100) {
            ret = 100;
        }
        return ret;
    }

    public int getMembersPercentLevel1() {
        int ret;
        int top = 1000;
        switch (level) {
            case 1:
                top = 1000;
                break;
            case 2:
                top = 2000;
                break;
            case 3:
                top = 3000;
                break;
        }

        ret = (int) ((membersCount * 100) / top);
        if (ret > 100) {
            ret = 100;
        }

        return ret;
    }

    public String getMembersLevelString1() {
        int top = 1000;
        switch (level) {
            case 1:
                top = 1000;
                break;
            case 2:
                top = 2000;
                break;
            case 3:
                top = 3000;
                break;
        }

        return (membersCount > top ? top : membersCount) + "/" + top;
    }

    public int getMembersPercentLevel2() {
        int ret;
        int top = 3000;
        switch (level) {
            case 1:
                top = 3000;
                break;
            case 2:
                top = 5000;
                break;
            case 3:
                top = 7000;
                break;
        }

        ret = (int) ((membersCount * 100) / top);
        if (ret > 100) {
            ret = 100;
        }

        return ret;
    }

    public String getMembersLevelString2() {
        int top = 3000;
        switch (level) {
            case 1:
                top = 3000;
                break;
            case 2:
                top = 5000;
                break;
            case 3:
                top = 7000;
                break;
        }

        return (membersCount > top ? top : membersCount) + "/" + top;
    }
}
