/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Formula;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bsc_customers")
public class Customer implements Serializable {

    private static final long serialVersionUID = 6366686791139770916L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date datelastorder;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateregister;
    private String payerEmail;
    private String password;
    private String pssw;
    private String resetHash;
    private String firstName;
    private String lastName;
    private String addressName;
    private String addressCountry;
    private String addressCountryCode;
    private String addressZip;
    private String addressState;
    private String addressCity;
    private String addressStreet;
    private String addressCountry2;
    private String addressCountryCode2;
    private String addressZip2;
    private String addressState2;
    private String addressCity2;
    private String addressStreet2;
    private String dni;
    private String phone;
    private String storeOwner;

    @Formula(value = "(select (count(1) = 0) from members m where m.email = payer_email)")
    private boolean customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatelastorder() {
        return datelastorder;
    }

    public void setDatelastorder(Date datelastorder) {
        this.datelastorder = datelastorder;
    }

    public Date getDateregister() {
        return dateregister;
    }

    public void setDateregister(Date dateregister) {
        this.dateregister = dateregister;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPssw() {
        return pssw;
    }

    public void setPssw(String pssw) {
        this.pssw = pssw;
    }

    public String getResetHash() {
        return resetHash;
    }

    public void setResetHash(String resetHash) {
        this.resetHash = resetHash;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getAddressCountryCode() {
        return addressCountryCode;
    }

    public void setAddressCountryCode(String addressCountryCode) {
        this.addressCountryCode = addressCountryCode;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressCountry2() {
        return addressCountry2;
    }

    public void setAddressCountry2(String addressCountry2) {
        this.addressCountry2 = addressCountry2;
    }

    public String getAddressCountryCode2() {
        return addressCountryCode2;
    }

    public void setAddressCountryCode2(String addressCountryCode2) {
        this.addressCountryCode2 = addressCountryCode2;
    }

    public String getAddressZip2() {
        return addressZip2;
    }

    public void setAddressZip2(String addressZip2) {
        this.addressZip2 = addressZip2;
    }

    public String getAddressState2() {
        return addressState2;
    }

    public void setAddressState2(String addressState2) {
        this.addressState2 = addressState2;
    }

    public String getAddressCity2() {
        return addressCity2;
    }

    public void setAddressCity2(String addressCity2) {
        this.addressCity2 = addressCity2;
    }

    public String getAddressStreet2() {
        return addressStreet2;
    }

    public void setAddressStreet2(String addressStreet2) {
        this.addressStreet2 = addressStreet2;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStoreOwner() {
        return storeOwner;
    }

    public void setStoreOwner(String storeOwner) {
        this.storeOwner = storeOwner;
    }

    public boolean isCustomer() {
        return customer;
    }

    public void setCustomer(boolean customer) {
        this.customer = customer;
    }
    
}
