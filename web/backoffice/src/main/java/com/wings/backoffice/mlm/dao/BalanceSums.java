/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.utils.NumberUtils;
import java.util.Locale;

/**
 *
 * @author seba
 */
public class BalanceSums {

    private Double total;
    private Double effective;
    private Long records;
    private Locale locale;

    public BalanceSums(Double sum, Long records, Double effective) {
        this.total = sum != null ? sum : 0;
        this.effective = effective != null ? effective : 0;
        this.records = records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public Long getRecords() {
        return records;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTotal() {
        return total;
    }

    public void setEffective(Double effective) {
        this.effective = effective;
    }

    public Double getEffective() {
        return effective;
    }

    public String getTotalText() {
        if (locale != null) {
            return NumberUtils.formatMoney(total, locale);
        }
        return NumberUtils.formatMoney(total);
    }

    public String getEffectiveText() {
        if (locale != null) {
            return NumberUtils.formatMoney(effective, locale);
        }
        return NumberUtils.formatMoney(effective);
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

}
