/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.enums;

/**
 *
 * @author seba
 */
public enum LogType {

    member_profile_edit,
    member_login,
    admin_login,
    generic_log,
    login_error,

}
