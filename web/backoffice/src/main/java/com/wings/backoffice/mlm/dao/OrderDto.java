/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.mlm.domain.bis.orders.FoundersView;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author lucas
 */
public class OrderDto {

    private Long id;
    private Date creationDate;
    private Date activationDate;
    private Long itemId;
    private Double paymentAmount;
    private String paymentCurrency;
    private String paymentType;
    private String billingCountry;
    private String storeTitle;
    private String localCurrency;
    private String currency;
    private String payerEmail;
    private String orderItem;
    private String attachment;
    private String ticketNumber;
    private String client;
    private String type;
    private String item;
    private Integer installmentNumber;
    private String founder;
    private String mark;
    private String status;
    private Long memberId;

    public OrderDto() {
        this(new Order());
    }

    public OrderDto(Long id, Date creationDate, Date activationDate, Long itemId, Double paymentAmount, String paymentCurrency, String paymentType,
            String billingCountry, String storeTitle, String payerEmail, String orderItem, String attachment, String ticketNumber, String client, String type) {
        this.id = id;
        this.creationDate = creationDate;
        this.activationDate = activationDate;
        this.itemId = itemId;
        this.paymentAmount = paymentAmount;
        this.paymentCurrency = paymentCurrency;
        this.paymentType = paymentType;
        this.billingCountry = billingCountry;
        this.storeTitle = storeTitle;
        this.payerEmail = payerEmail;
        this.orderItem = orderItem;
        this.localCurrency = CountryUtils.getCurrencyForCountry(this.billingCountry);
        this.attachment = attachment;
        this.ticketNumber = ticketNumber;
        this.client = client;
        this.type = type;
    }

    public OrderDto(Long id, Date creationDate, Date activationDate, Long itemId, Double paymentAmount, String paymentCurrency, String paymentType,
            String billingCountry, String storeTitle, String payerEmail, String orderItem) {
        this.id = id;
        this.creationDate = creationDate;
        this.activationDate = activationDate;
        this.itemId = itemId;
        this.paymentAmount = paymentAmount;
        this.paymentCurrency = paymentCurrency;
        this.paymentType = paymentType;
        this.billingCountry = billingCountry;
        this.storeTitle = storeTitle;
        this.payerEmail = payerEmail;
        this.orderItem = orderItem;
        this.localCurrency = CountryUtils.getCurrencyForCountry(this.billingCountry);
    }
    
    public OrderDto(Long id, Date creationDate, Date activationDate, Long itemId, Double paymentAmount, String paymentCurrency, String paymentType,
            String billingCountry, String storeTitle, String payerEmail, String orderItem, String founder, String mark) {
        this.id = id;
        this.creationDate = creationDate;
        this.activationDate = activationDate;
        this.itemId = itemId;
        this.paymentAmount = paymentAmount;
        this.paymentCurrency = paymentCurrency;
        this.paymentType = paymentType;
        this.billingCountry = billingCountry;
        this.storeTitle = storeTitle;
        this.payerEmail = payerEmail;
        this.orderItem = orderItem;
        this.localCurrency = CountryUtils.getCurrencyForCountry(this.billingCountry);
        this.founder = founder;
        this.mark = mark;
    }

    public OrderDto(Order order) {
        this(order.getId(),
                order.getCreationDate(),
                order.getActivationDate(),
                order.getItemId(),
                order.getPaymentAmount(),
                order.getPaymentCurrency(),
                order.getPaymentType(),
                order.getBillingCountry(),
                order.getStoreTitle(),
                order.getPayerEmail(),
                order.getOrderItem().getName(),
                order.getAttachment(),
                order.getTicketNumber(),
                order.getFullName(),
                order.getType());
        this.installmentNumber = order.getInstallmentNumber();
        this.item = order.getOrderItem() != null? order.getOrderItem().getName() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Double getPaymentAmount() {
        Double temp = 0D, amount = 0D;
        if (getPaymentCurrency().equals(localCurrency)) {
            temp = paymentAmount;
        } else if (getPaymentCurrency().equals("EUR")) {
            temp = NumberUtils.convertMoneyFromEuros(paymentAmount, CountryUtils.getLocaleForCountry(billingCountry));
        }

        if (currency != null) {
            Double euros = NumberUtils.convertMoneyToEuros(temp, CountryUtils.getLocaleForCountry(billingCountry));
            amount = NumberUtils.convertMoneyFromEuros(euros, currency);
        }

        return currency != null ? amount : temp;
    }

    public String getPaymentAmountStr() {
        Double total = getPaymentAmount();
        String str = currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, billingCountry);
        return str;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getStoreTitle() {
        return storeTitle;
    }

    public void setStoreTitle(String storeTitle) {
        this.storeTitle = storeTitle;
    }

    public String getLocalCurrency() {
        return localCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmountByCurrency(String currency) {
        return currency != null ? NumberUtils.formatMoney(paymentAmount, currency) : NumberUtils.formatMoneyForCountryIso(paymentAmount, billingCountry);
    }

    public Double getAmountInEuros() {
        return NumberUtils.convertMoneyFromEuros(paymentAmount, CountryUtils.getLocaleForCountry(billingCountry));
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(String orderItem) {
        this.orderItem = orderItem;
    }

    public Integer getMonth() {
        Calendar c = new GregorianCalendar();
        c.setTime(activationDate);
        return c.get(Calendar.MONTH) + 1;
    }

    public Integer getYear() {
        Calendar c = new GregorianCalendar();
        c.setTime(activationDate);
        return c.get(Calendar.YEAR);
    }
    
    public Integer getCreationYear() {
        Calendar c = new GregorianCalendar();
        c.setTime(creationDate);
        return c.get(Calendar.YEAR);
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "OrderDto{" + "id=" + id + ", creationDate=" + creationDate + ", activationDate=" + activationDate + ", itemId=" + itemId + ", paymentAmount=" + paymentAmount + ", paymentCurrency=" + paymentCurrency + ", paymentType=" + paymentType + ", billingCountry=" + billingCountry + ", storeTitle=" + storeTitle + ", localCurrency=" + localCurrency + '}';
    }
}
