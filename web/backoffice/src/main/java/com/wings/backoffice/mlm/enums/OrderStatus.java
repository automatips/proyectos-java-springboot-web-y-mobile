/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.enums;

/**
 *
 * @author seba
 */
public enum OrderStatus {

    paid,
    pending,
    /**
     * Not used
     */
    unpaid,
    /**
     * not used
     */
    review,
    /**
     * In transit from cuorier, only for shipment
     */
    shipped,
    /**
     * Prepared for shipment, use only on shopment status
     */
    prepared,
    /**
     * order delivered, only for shipment
     */
    delivered,
    /**
     * order completely delivered, only for shipment
     */
    complete,

}
