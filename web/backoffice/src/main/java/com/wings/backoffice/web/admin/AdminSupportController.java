/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.messages.SupportMessage;
import com.wings.backoffice.services.messages.SupportService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.PageWrapper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class AdminSupportController {

    @Autowired
    private SupportService supportService;

    private final Map<String, String> mimeTypes = new HashMap<String, String>() {
        {
            put("image/png", ".png");
            put("image/jpeg", ".jpg");
            put("application/pdf", ".pdf");
            put("text/plain", ".txt");
            put("video/mp4", ".mp4");
        }
    };

    /**
     *
     * @param model
     * @param request
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.getIncidencesList(Model, HttpServletRequest))}
     * @see com.wings.backoffice.web.commons.SupportController.getIncidencesList(Model, HttpServletRequest)
     */
    @RequestMapping(value = {"admin/support/incidences"}, method = {RequestMethod.GET})
    public String getIncidencesList(Model model, HttpServletRequest request) {
        AdminUser adminUser = AuthUtils.getAdminUser();

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String country = request.getParameter("country");
        String asignation = request.getParameter("asignation");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        PageWrapper<SupportMessage> pageWrapper = supportService.search(adminUser, q, spage, ssize, status, country, asignation);
        int size = 20;
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }
        model.addAttribute("page", pageWrapper);
        model.addAttribute("country", country);
        model.addAttribute("asignation", asignation);
        model.addAttribute("status", status);
        model.addAttribute("size", size);

        return "admin/support/incidences";
    }

    /**
     *
     * @param model
     * @param request
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.getNewIncidente()}
     * @see com.wings.backoffice.web.commons.SupportController.getNewIncidente()
     * @return
     */
    @RequestMapping(value = {"admin/support/incidences/create"}, method = {RequestMethod.GET})
    public String getNewIncidences(Model model, HttpServletRequest request) {
        return "admin/support/incidences-create";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @param attachment
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.postNewIncidences()}
     * @see com.wings.backoffice.web.commons.SupportController.postNewIncidences()
     */
    @RequestMapping(value = {"admin/support/incidences/create"}, method = {RequestMethod.POST})
    public String postNewIncidences(Model model, HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "attachment", required = false) MultipartFile attachment
    ) {

        AdminUser admin = AuthUtils.getAdminUser();
        String title = request.getParameter("title");
        String description = request.getParameter("description");

        boolean error = false;
        if (title == null || title.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "El título no puede estar vacio");
            redirectAttributes.addFlashAttribute("messageType", "error");
            error = true;
        }

        if (description == null || description.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "La descripcón no puede estar vacia");
            redirectAttributes.addFlashAttribute("messageType", "error");
            error = true;
        }

        String attachmentFile = null;
        if (attachment != null && !attachment.isEmpty()) {
            try {
                if (mimeTypes.containsKey(attachment.getContentType())) {
                    String prefix = InvoiceUtils.getRandomString(12);
                    String suffix = mimeTypes.get(attachment.getContentType());
                    String fileName = "/var/wings/bo_media/attachments/" + prefix + suffix;
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                    stream.write(attachment.getBytes());
                    attachmentFile = prefix + suffix;
                } else {
                    redirectAttributes.addFlashAttribute("message", "Formato de archivo no soportado");
                    redirectAttributes.addFlashAttribute("messageType", "error");
                    error = true;
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("message", "Error subiendo el adjunto!");
                redirectAttributes.addFlashAttribute("messageType", "error");
                error = true;
            }
        }

        if (!error) {

            SupportMessage m = new SupportMessage();
            m.setAssignedTo("admin");
            m.setCreationDate(new Date());
            m.setStatus("open");
            m.setTitle(title);
            m.setDescription(description);

            m.setMemberId(-1l);
            m.setUsername(admin.getEmail());
            m.setFirstName(admin.getUsername());
            m.setLastName("");
            m.setAttachment(attachmentFile);

            supportService.save(m);

            redirectAttributes.addFlashAttribute("message", "Incidencia creada con éxito");
            redirectAttributes.addFlashAttribute("messageType", "success");

        }

        return "redirect:/admin#admin/support/incidences";

    }

    /**
     *
     * @param model
     * @param request
     * @param incidenceId
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.viewIncidence()}
     * @see com.wings.backoffice.web.commons.SupportController.viewIncidence()
     */
    @RequestMapping(value = {"admin/support/incidences/{id}"}, method = {RequestMethod.GET})
    public String getIncidence(Model model, HttpServletRequest request, @PathVariable("id") Long incidenceId) {
        SupportMessage message = supportService.getOne(incidenceId);
        model.addAttribute("msg", message);
        return "admin/support/incidences-view";
    }

    /**
     *
     * @param model
     * @param request
     * @param incidenceId
     * @param redirectAttributes
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.updateIncidence()}
     * @see com.wings.backoffice.web.commons.SupportController.updateIncidence()
     */
    @RequestMapping(value = {"admin/support/incidences/{id}"}, method = {RequestMethod.POST})
    public String postIncidence(Model model, HttpServletRequest request, @PathVariable("id") Long incidenceId, RedirectAttributes redirectAttributes) {

        AdminUser admin = AuthUtils.getAdminUser();
        SupportMessage message = supportService.getOne(incidenceId);
        
        boolean error = false;
        String action = request.getParameter("action");
        if (action == null || action.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Accion no reconocida!");
            redirectAttributes.addFlashAttribute("messageType", "error");
            error = true;
        }

        if (!error) {
            if (action.equals("close")) {

                String closeMessage = request.getParameter("closeMessage");

                message.setStatus("closed");
                message.setClosedDate(new Date());
                message.setClosedMessage(closeMessage);
                message.setClosedUser(admin.getUsername());

                supportService.save(message);

                redirectAttributes.addFlashAttribute("message", "Incidencia cerrada con éxito");
                redirectAttributes.addFlashAttribute("messageType", "success");

            } else if (action.equals("update-status")) {

                String status = request.getParameter("status");
                message.setClosedDate(null);
                message.setClosedMessage(null);
                message.setClosedUser(null);
                message.setStatus(status);
                supportService.save(message);

                redirectAttributes.addFlashAttribute("message", "Incidencia modificada con éxito");
                redirectAttributes.addFlashAttribute("messageType", "success");

            } else if (action.equals("change-asignation")) {

                String asignation = request.getParameter("asignation");
                message.setAssignedTo(asignation);
                supportService.save(message);

                redirectAttributes.addFlashAttribute("message", "Incidencia modificada con éxito");
                redirectAttributes.addFlashAttribute("messageType", "success");

            }
        }

        return "redirect:/admin#support/incidences?action=admin";
    }

    /**
     *
     * @param model
     * @param request
     * @param response
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.getAttachment()}
     * @see com.wings.backoffice.web.commons.SupportController.getAttachment()
     */
    @RequestMapping(value = {"admin/support/attachment/view"}, method = {RequestMethod.GET})
    public void getAttachment(Model model, HttpServletRequest request, HttpServletResponse response) {

        String name = request.getParameter("file");
        FileOutputStream os = null;
        try {

            File outputFile = new File("/var/wings/bo_media/attachments/" + name);
            FileInputStream fis = new FileInputStream(outputFile);

            response.setContentType(URLConnection.guessContentTypeFromName(outputFile.getName()));
            if (name.contains(".mp4")) {
                response.setContentType("video/mp4");
            }

            response.setContentLengthLong(outputFile.length());
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Content-Disposition", "filename=" + name);

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
