//REPORTS CONTROLLERS
package com.wings.backoffice.web.admin.reports;

import com.wings.backoffice.mlm.dao.CountryDto;
import com.wings.backoffice.mlm.dao.OrderDto;
import com.wings.backoffice.mlm.dao.ReportDto;
import com.wings.backoffice.mlm.domain.bis.orders.FoundersView;
import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.ReportService;
import com.wings.backoffice.services.TestService;
import com.wings.backoffice.services.TestServiceDto;
import com.wings.backoffice.services.promos.PromoLatamService;
import com.wings.backoffice.utils.DateUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lucas
 */
@Controller
public class ReportsController {

    @Autowired
    private ReportService reportService;
    @Autowired
    private PromoLatamService promoLatamService;
    @Autowired
    private CountriesService countriesService;
    @Autowired
    private TestService testService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = {"/admin/reports"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {
        return "admin/reports/index";
    }

    @RequestMapping(value = {"/admin/reports/ordersByCountries"}, method = RequestMethod.GET)
    public String ordersByCountries(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = getCountries(scountry);
        List<Integer> months = getMonths(smonth);
        List<Integer> years = getYears(syear);
        String currency = getCurrency(scurrency);
        Date f = getDate(from);
        Date t = getDate(to);

        if (to == null && from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getOrdersByCountryDto(countries, months, years, f, t, currency);

        model.addAttribute("countries", reportDto.getCountryDtos());
        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("currency", scurrency);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/reports/orders/ordersByCountry";
    }

    @RequestMapping(value = {"/admin/reports/ordersByCountries2"}, method = RequestMethod.GET)
    public String ordersByCountries2(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String click = request.getParameter("click");

        List<String> countries = getCountries(scountry);
        List<Integer> months = getMonths(smonth);
        List<Integer> years = getYears(syear);
        String currency = getCurrency(scurrency);
        if (click != null) {
            Date f = getDate(from);
            Date t = getDate(to);

            if (to == null && from != null) {
                t = new Date();
                DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                to = format2.format(t);
                t = DateUtils.getLastHourOfDay(t);
            }

            if (f != null) {
                years = getYearsBetween(f, t);
                months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            }

            List<TestServiceDto> list = testService.getOrdersByCountryDto(countries, months, years, f, t, currency);
            model.addAttribute("list", list);
        }

        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("currency", scurrency);
        model.addAttribute("countriesCombo", countriesService.getCountriesIsos());
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/reports/orders/ordersByCountry2";
    }

    @RequestMapping(value = {"/admin/reports/ordersByBacs"}, method = RequestMethod.GET)
    public String ordersByBacs(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String click = request.getParameter("click");

        List<String> countries = getCountries(scountry);
        List<Integer> months = getMonths(smonth);
        List<Integer> years = getYears(syear);
        String currency = getCurrency(scurrency);
        if (click != null) {
            Date f = getDate(from);
            Date t = getDate(to);

            if (to == null && from != null) {
                t = new Date();
                DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                to = format2.format(t);
                t = DateUtils.getLastHourOfDay(t);
            }

            if (f != null) {
                years = getYearsBetween(f, t);
                months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            }

            List<TestServiceDto> list = testService.getOrdersByBacs(countries, months, years, f, t, currency);
            model.addAttribute("list", list);
        }

        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("currency", scurrency);
        model.addAttribute("countriesCombo", countriesService.getCountriesIsos());
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/reports/orders/ordersByCountry2";
    }

    @RequestMapping(value = {"/admin/reports/ordersProducts"}, method = RequestMethod.GET)
    public String ordersByProductos(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String click = request.getParameter("click");
        String payment = request.getParameter("payment");

        List<String> countries = getCountries(scountry);
        List<Integer> months = getMonths(smonth);
        List<Integer> years = getYears(syear);
        String currency = getCurrency(scurrency);
        if (click != null) {
            Date f = getDate(from);
            Date t = getDate(to);

            if (to == null && from != null) {
                t = new Date();
                DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                to = format2.format(t);
                t = DateUtils.getLastHourOfDay(t);
            }

            if (f != null) {
                years = getYearsBetween(f, t);
                months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            }

            List<TestServiceDto> list = testService.getOrdersByProducts(countries, months, years, f, t, payment);
            model.addAttribute("list", list);
        }

        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("payment", payment);
        model.addAttribute("currency", scurrency);
        model.addAttribute("countriesCombo", countriesService.getCountriesIsos());
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/reports/orders/productsByCountry2";
    }

    @RequestMapping(value = {"/admin/reports/fundersByCountries"}, method = RequestMethod.GET)
    public String fundersByCountries(Model model, HttpServletRequest request) {
        List<FoundersView> fundersComplete = reportService.getFundersComplete();
        List<FoundersView> fundersFirst = reportService.getFundersFirst();
        List<FoundersView> fundersSecond = reportService.getFundersSecond();

        List<FoundersView> completeDiamond = fundersComplete.stream().filter(view -> view.getFounder().equals("Diamond")).collect(Collectors.toList());
        List<FoundersView> completeGolden = fundersComplete.stream().filter(view -> view.getFounder().equals("Golden")).collect(Collectors.toList());
        List<FoundersView> firstDiamond = fundersFirst.stream().filter(view -> view.getFounder().equals("Diamond")).collect(Collectors.toList());
        List<FoundersView> firstGolden = fundersFirst.stream().filter(view -> view.getFounder().equals("Golden")).collect(Collectors.toList());
        List<FoundersView> secondDiamond = fundersSecond.stream().filter(view -> view.getFounder().equals("Diamond")).collect(Collectors.toList());
        List<FoundersView> secondGolden = fundersSecond.stream().filter(view -> view.getFounder().equals("Golden")).collect(Collectors.toList());

        int totalCompleteDiamond = 0;
        for (FoundersView foundersView : completeDiamond) {
            totalCompleteDiamond += foundersView.getSize();
        }
        int totalCompleteGolden = 0;
        for (FoundersView foundersView : completeGolden) {
            totalCompleteGolden += foundersView.getSize();
        }
        int totalFirstDiamond = 0;
        for (FoundersView foundersView : firstDiamond) {
            totalFirstDiamond += foundersView.getSize();
        }
        int totalFirstGolden = 0;
        for (FoundersView foundersView : firstGolden) {
            totalFirstGolden += foundersView.getSize();
        }
        int totalSecondDiamond = 0;
        for (FoundersView foundersView : secondDiamond) {
            totalSecondDiamond += foundersView.getSize();
        }
        int totalSecondGolden = 0;
        for (FoundersView foundersView : secondGolden) {
            totalSecondGolden += foundersView.getSize();
        }

        model.addAttribute("completeDiamond", completeDiamond);
        model.addAttribute("completeGolden", completeGolden);
        model.addAttribute("firstDiamond", firstDiamond);
        model.addAttribute("firstGolden", firstGolden);
        model.addAttribute("secondDiamond", secondDiamond);
        model.addAttribute("secondGolden", secondGolden);
        model.addAttribute("totalCompleteDiamond", totalCompleteDiamond);
        model.addAttribute("totalCompleteGolden", totalCompleteGolden);
        model.addAttribute("totalFirstDiamond", totalFirstDiamond);
        model.addAttribute("totalFirstGolden", totalFirstGolden);
        model.addAttribute("totalSecondDiamond", totalSecondDiamond);
        model.addAttribute("totalSecondGolden", totalSecondGolden);

        return "admin/reports/orders/foundersByCountry";
    }

    @RequestMapping(value = {"/admin/reports/totalProductsByCountries"}, method = RequestMethod.GET)
    public String totalProductsByCountries(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String installment = request.getParameter("installment");
        if (installment == null) {
            installment = "1";
        }

        List<String> countries = getCountries(scountry);

        List<Integer> months = getMonths(smonth);

        List<Integer> years = getYears(syear);

        Date f = getDate(from);

        Date t = getDate(to);

        if (to == null && from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getProductsByCountryDto(countries, months, years, f, t, installment);
        //List<String> columnas = Arrays.asList("W2", "W3", "W5", "W6", "WingsONEFOUR", "WINGS STORE BASE", "WINGS STORE CORNER LIGHT", "WINGS STORE CORNER FULL", "WINGS STORE FULL");
        List<String> columnas = Arrays.asList("W3", "W4", "W6", "W7", "WingsONEFOUR", "Wings Scooter", "Smartwatch Executive", "Computador All In One",
                "WINGS STORE BASE", "WINGS STORE CORNER LIGHT", "WINGS STORE CORNER FULL", "WINGS STORE FULL");

        model.addAttribute("countries", reportDto.getCountryDtos());
        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("installment", installment);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("columnas", columnas);

        return "admin/reports/orders/totalProductsByCountry";
    }

    @RequestMapping(value = {"/admin/reports/membersByCountries"}, method = RequestMethod.GET)
    public String membersByCountries(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = getCountries(scountry);
        if (scountry == null || scountry.equals("all")) {
            countries = new ArrayList<>(countries);
            countries.add("OT");
        }

        List<Integer> months = getMonths(smonth);

        List<Integer> years = getYears(syear);

        Date f = getDate(from);

        Date t = getDate(to);

        if (to == null && from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getMembersByCountryDto(countries, months, years, f, t);

        model.addAttribute("countries", reportDto);
        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/reports/orders/membersByCountry";
    }

    @RequestMapping(value = {"admin/reports/members/viewDetails"}, method = RequestMethod.GET)
    public String viewReportMembersDetails(Model model, HttpServletRequest request) {
        String country = request.getParameter("country");
        String year = request.getParameter("year");
        String month = request.getParameter("month");
        String action = request.getParameter("action");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = new ArrayList<>();
        countries.add(country);
        List<Integer> years = new ArrayList<>();
        years.add(Integer.valueOf(year));
        List<Integer> months = new ArrayList<>();
        months.add(Integer.valueOf(month));

        Date f = null, t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);

                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        } else if (from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getMembersByCountryDto(countries, months, years, f, t);
        model.addAttribute("members", reportDto.getCountryDtos().get(0));
        model.addAttribute("action", action);
        model.addAttribute("country", country);
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("smonth", DateUtils.getMonthName(Integer.valueOf(month)));

        return "admin/reports/orders/viewMembersDetails";
    }

    @RequestMapping(value = {"/admin/reports/ordersPending"}, method = RequestMethod.GET)
    public String ordersPendingByCountries(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String currency = getCurrency(scurrency);

        List<String> countries = getCountries(scountry);
        if (scountry == null || scountry.equals("all")) {
            countries = new ArrayList<>(countries);
            countries.add("OT");
        }

        List<Integer> years = getYears(syear);

        ReportDto reportDto = reportService.getOrdersIncompleteByCountryDto(countries, years, currency);
        reportDto.setCurrency(currency);
        Map<Long, String> products = getProductsNames(reportDto.getCountryDtos());

        model.addAttribute("countries", reportDto);
        model.addAttribute("country", scountry);
        model.addAttribute("products", products);
        model.addAttribute("currency", scurrency);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);

        return "admin/reports/orders/pendingByCountry";
    }

    @RequestMapping(value = {"admin/reports/orders/viewPendingDetails"}, method = RequestMethod.GET)
    public String viewReportPendingsDetails(Model model, HttpServletRequest request) {
        String country = request.getParameter("country");
        String year = request.getParameter("year");
        String scurrency = request.getParameter("currency");
        String itemId = request.getParameter("itemId");
        String currency = getCurrency(scurrency);
        Long id = Long.parseLong(itemId);
        List<String> countries = new ArrayList<>();
        countries.add(country);
        List<Integer> years = new ArrayList<>();
        years.add(Integer.valueOf(year));

        ReportDto reportDto = reportService.getOrdersIncompleteByCountryDto(countries, years, currency);
        List<OrderDto> orders = reportDto.getCountryDtos().get(0).getOrders().stream().filter(order -> order.getItemId().equals(id)).collect(Collectors.toList());

        model.addAttribute("orders", orders);
        model.addAttribute("country", country);
        model.addAttribute("year", year);
        model.addAttribute("itemId", id);
        model.addAttribute("currency", currency);

        return "admin/reports/orders/viewPendingsDetails";
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos de exportación">
    @RequestMapping(value = {"admin/reports/export/orderPending"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportPendings(Model model, HttpServletRequest request) {
        String country = request.getParameter("country");
        String year = request.getParameter("year");
        String itemId = request.getParameter("itemId");
        String scurrency = request.getParameter("currency");
        String currency = getCurrency(scurrency);
        Long id = 0L;
        if (itemId != null && !itemId.equals("null")) {
            id = Long.parseLong(itemId);
        }

        List<String> countries = getCountries(country);
        List<Integer> years = getYears(year);

        ReportDto reportDto = reportService.getOrdersIncompleteByCountryDto(countries, years, currency);

        List<OrderDto> orders = new ArrayList<>();

        if (id > 0) {
            for (OrderDto order : reportDto.getCountryDtos().get(0).getOrders()) {
                if (order.getItemId().equals(id)) {
                    orders.add(order);
                }
            }
        } else {
            reportDto.getCountryDtos().forEach((countryDto) -> {
                countryDto.getOrders().forEach((order) -> {
                    orders.add(order);
                });
            });
        }

        String ret = reportService.getOrderPendingCsv(orders);

        return buildFileToExport("listado_emails", ret);
    }

    @RequestMapping(value = {"admin/reports/export/products"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportProducts(Model model, HttpServletRequest request) {
        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String installment = request.getParameter("installment");
        if (installment == null) {
            installment = "1";
        }

        List<String> countries = getCountries(scountry);

        List<Integer> months = getMonths(smonth);

        List<Integer> years = getYears(syear);

        Date f = getDate(from);

        Date t = getDate(to);

        if (to == null && from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getProductsByCountryDto(countries, months, years, f, t, installment);
        List<String> columnas = Arrays.asList("W3", "W4", "W6", "W7", "WingsONEFOUR", "Wings Scooter", "Smartwatch Executive", "Computador All In One",
                "WINGS STORE BASE", "WINGS STORE CORNER LIGHT", "WINGS STORE CORNER FULL", "WINGS STORE FULL");

        String ret = reportService.getProductsCsv(reportDto, columnas, years, months, installment);

        return buildFileToExport("listado_productos", ret);
    }

    @RequestMapping(value = {"admin/reports/export/members"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportMembers(Model model, HttpServletRequest request) {
        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = getCountries(scountry);
        if (scountry == null || scountry.equals("all")) {
            countries = new ArrayList<>(countries);
            countries.add("OT");
        }

        List<Integer> months = getMonths(smonth);

        List<Integer> years = getYears(syear);

        Date f = getDate(from);

        Date t = getDate(to);

        if (to == null && from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getMembersByCountryDto(countries, months, years, f, t);

        String ret = reportService.getMembersCsv(reportDto, years, months);

        return buildFileToExport("listado_afiliados", ret);
    }

    @RequestMapping(value = {"admin/reports/export/ordersByCountry"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportOrdersByCountry(Model model, HttpServletRequest request) {
        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = getCountries(scountry);

        List<Integer> months = getMonths(smonth);

        List<Integer> years = getYears(syear);

        String currency = getCurrency(scurrency);

        Date f = getDate(from);

        Date t = getDate(to);

        if (to == null && from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getOrdersByCountryDto(countries, months, years, f, t, currency);

        String ret = reportService.getOrdersByCountryCsv(reportDto, years, months);

        return buildFileToExport("listado_afiliados", ret);
    }

    @RequestMapping(value = {"admin/reports/export/founders"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportFounders(Model model, HttpServletRequest request) {
        List<FoundersView> fundersComplete = reportService.getFundersComplete();
        List<FoundersView> fundersFirst = reportService.getFundersFirst();
        List<FoundersView> fundersSecond = reportService.getFundersSecond();

        List<FoundersView> completeDiamond = fundersComplete.stream().filter(view -> view.getFounder().equals("Diamond")).collect(Collectors.toList());
        List<FoundersView> completeGolden = fundersComplete.stream().filter(view -> view.getFounder().equals("Golden")).collect(Collectors.toList());
        List<FoundersView> firstDiamond = fundersFirst.stream().filter(view -> view.getFounder().equals("Diamond")).collect(Collectors.toList());
        List<FoundersView> firstGolden = fundersFirst.stream().filter(view -> view.getFounder().equals("Golden")).collect(Collectors.toList());
        List<FoundersView> secondDiamond = fundersSecond.stream().filter(view -> view.getFounder().equals("Diamond")).collect(Collectors.toList());
        List<FoundersView> secondGolden = fundersSecond.stream().filter(view -> view.getFounder().equals("Golden")).collect(Collectors.toList());

        int totalCompleteDiamond = 0;
        for (FoundersView foundersView : completeDiamond) {
            totalCompleteDiamond += foundersView.getSize();
        }
        int totalCompleteGolden = 0;
        for (FoundersView foundersView : completeGolden) {
            totalCompleteGolden += foundersView.getSize();
        }
        int totalFirstDiamond = 0;
        for (FoundersView foundersView : firstDiamond) {
            totalFirstDiamond += foundersView.getSize();
        }
        int totalFirstGolden = 0;
        for (FoundersView foundersView : firstGolden) {
            totalFirstGolden += foundersView.getSize();
        }
        int totalSecondDiamond = 0;
        for (FoundersView foundersView : secondDiamond) {
            totalSecondDiamond += foundersView.getSize();
        }
        int totalSecondGolden = 0;
        for (FoundersView foundersView : secondGolden) {
            totalSecondGolden += foundersView.getSize();
        }

        StringBuilder ret = reportService.getFounderCsv(completeDiamond, "Todo Pago Diamond", totalCompleteDiamond);
        ret.append(reportService.getFounderCsv(completeGolden, "Todo Pago Golden", totalCompleteGolden));
        ret.append(reportService.getFounderCsv(firstDiamond, "Pago Cuota 1 Diamond", totalFirstDiamond));
        ret.append(reportService.getFounderCsv(firstGolden, "Pago Cuota 1 Golden", totalFirstGolden));
        ret.append(reportService.getFounderCsv(secondDiamond, "Pago Cuota 1 y 2 Diamond", totalSecondDiamond));
        ret.append(reportService.getFounderCsv(secondGolden, "Pago Cuota 1 y 2 Golden", totalSecondGolden));

        return buildFileToExport("listado_afiliados", ret.toString());
    }

    @RequestMapping(value = {"admin/reports/export/promoLatam"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getPromoLatam(Model model, HttpServletRequest request) {
        String country = request.getParameter("country");
        String qualify = request.getParameter("qualify");

        if (qualify != null && qualify.equals("all")) {
            qualify = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        Boolean bq = true; //Quitar hardcodeo en caso de necesitar exportar otros
        List<PromoLatam> items = promoLatamService.getPromoLatam(country, bq);

        String ret = reportService.getPromoLatamCsv(items);

        return buildFileToExport("listado_promoLatam", ret);
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos de soporte para exportación">
    private ResponseEntity<InputStreamResource> buildFileToExport(String nameFile, String resources) {
        HttpHeaders headers = new HttpHeaders();
        FileInputStream fis;
        try {
            File file = File.createTempFile(nameFile, "");
            FileWriter fw = new FileWriter(file);
            fw.append(resources);
            fw.close();

            fis = new FileInputStream(file);

            headers.setContentType(MediaType.parseMediaType("application/csv"));
            headers.add("Access-Control-Allow-Origin", "*");
            headers.add("Content-Disposition", "filename=" + file.getName() + ".csv");
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos de soporte para reportes">
    private List<Integer> getYearsBetween(Date from, Date to) {
        final List<Integer> years = new ArrayList<>();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(from);
        int f = cal.get(Calendar.YEAR);
        years.add(f++);
        cal.setTime(to);
        final int t = cal.get(Calendar.YEAR);
        for (; f <= t; f++) {
            years.add(f);
        }

        return years;
    }

    private List<String> getCountries(String scountry) {
        List<String> countries = countriesService.getCountriesIsos();
        if (scountry != null && !scountry.equals("all")) {
            countries = Arrays.asList(scountry);
        }
        return countries;
    }

    private List<Integer> getMonths(String smonth) {
        List<Integer> months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        if (smonth != null && !smonth.equals("-1")) {
            months = Arrays.asList(Integer.valueOf(smonth));
        }

        return months;
    }

    private List<Integer> getYears(String syear) {
        //List<Integer> years = Arrays.asList(2017,2018, 2019, 2020, 2021);
        List<Integer> years = Arrays.asList(2017, 2018, 2019, 2020);
        if (syear != null && !syear.equals("-1")) {
            years = Arrays.asList(Integer.valueOf(syear));
        }

        return years;
    }

    private String getCurrency(String scurrency) {
        return scurrency != null && !scurrency.equals("all") ? scurrency : null;
    }

    private Date getDate(String sdate) {
        Date f = null;
        if (sdate != null) {
            try {
                f = sdf.parse(sdate);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        return f;
    }

    private Map<Long, String> getProductsNames(List<CountryDto> countryDtos) {
        Map<Long, String> products = new HashMap<>();
        for (CountryDto country : countryDtos) {
            for (OrderDto order : country.getOrders()) {
                Long itemId = order.getItemId();
                String product = products.get(itemId);
                if (product == null) {
                    products.put(itemId, order.getStoreTitle());
                }
            }
        }

        return products;
    }//</editor-fold>    

}
