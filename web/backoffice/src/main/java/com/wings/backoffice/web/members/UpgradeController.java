/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class UpgradeController {

    @Autowired
    private StoreItemsRepository storeItemsRepository;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private LogsService logsService;

    private final Logger log = LoggerFactory.getLogger(UpgradeController.class);

    /**
     * GET show Upgrade for this id
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"views/upgrade/{id}"}, method = RequestMethod.GET)
    public String getUpgradeInfo(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        StoreItem item = storeItemsRepository.findOne(id);

        Member m = AuthUtils.getMemberUser();
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        Double price = NumberUtils.convertMoneyFromEuros(item.getPrice(), locale);

        model.addAttribute("kit", item);
        model.addAttribute("price", NumberUtils.formatMoney(item.getPrice(), locale));

        return "members/store/upgrade";
    }

    /**
     * users confirm this upgrade, we add it to users orders
     *
     * @param model
     * @param request
     * @param upgradeId
     * @return
     */
    @RequestMapping(value = {"views/upgrade/confirm/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> getUpgradeConfirm(Model model, HttpServletRequest request, @PathVariable("id") Long upgradeId) {

        StoreItem item = storeItemsRepository.findOne(upgradeId);
        if (item == null) {
            return ResponseEntity.badRequest().body("No se encontro elemento para añadir el upgrade");
        }
        Member member = AuthUtils.getMemberUser();

        //reload - sometimes jpa session has expired
        member = membersRepository.findOne(member.getId());

        try {
            String ip = AuthUtils.getIp(request);
            String modUser = member.getUsername();

            ordersService.addUpgrade(member, upgradeId.toString(), modUser);
            logsService.addKitUpgradeLog(ip, modUser, member, upgradeId);

        } catch (Exception e) {
            log.error("Error addind upgrade", e);
            return ResponseEntity.badRequest().body("Ocurrió un error al añadir el upgrade");
        }

        return ResponseEntity.ok("Upgrade añadido correctamente");

    }
}
