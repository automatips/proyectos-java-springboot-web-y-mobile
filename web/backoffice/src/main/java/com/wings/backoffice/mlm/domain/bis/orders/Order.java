/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis.orders;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.utils.NumberUtils;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentDate;
    private Long itemId;
    private boolean active;
    private String status;
    private String comment;
    private String paymentStatus;
    private Double paymentAmount;
    private String paymentCurrency;
    private String paymentType;
    private String payerEmail;
    private String firstName;
    private String lastName;
    private String billingName;
    private String billingCompany;
    private String billingAddress;
    private String billingCity;
    private String billingPostalCode;
    private String billingState;
    private String billingCountry;
    private String storeId;
    private Long memberId;
    private Long externalId;
    private String type;
    private String typeStatus;
    private boolean deleted;
    private String storeTitle;
    private String shippingStatus;
    private String message;
    private String source;
    private boolean multiple;
    private boolean withPrize;
    private String prizeText;
    private String dni;
    private Double priceForComs;
    private Integer installmentNumber;
    private boolean installmentComplete;
    @Size(max = 200)
    @Column(name = "payment_attachment")
    private String attachment;
    @Size(max = 200)
    @Column(name = "payment_ticket_number")
    private String ticketNumber;
    @Size(max = 200)
    @Column(name = "shipping_name")
    private String shippingName;
    @Size(max = 200)
    @Column(name = "shipping_address")
    private String shippingAddress;
    @Size(max = 200)
    @Column(name = "shipping_city")
    private String shippingCity;
    @Size(max = 200)
    @Column(name = "shipping_postal_code")
    private String shippingPostalCode;
    @Size(max = 200)
    @Column(name = "shipping_state")
    private String shippingState;
    @Size(max = 200)
    @Column(name = "shipping_country")
    private String shippingCountry;
    @Size(max = 500)
    @Column(name = "shipping_extra")
    private String shippingExtra;
    @Size(max = 50)
    @Column(name = "dni_type")
    private String dniType;
    @Size(max = 150)
    @Column(name = "phone")
    private String phone;
    @Size(max = 250)
    @Column(name = "payment_bacs")
    private String paymentBacs;

    @Transient
    private String image;

    @OneToOne
    @JoinColumn(name = "itemId", updatable = false, insertable = false)
    private StoreItem orderItem;

    @OneToMany
    @JoinColumn(name = "orderId", updatable = false, insertable = false, nullable = true)
    private List<OrderItem> items = new LinkedList<>();

    @OneToOne
    @JoinColumn(name = "memberId", insertable = false, updatable = false, unique = false)
    private Member member;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public String getPaymentAmountText() {
        return NumberUtils.formatMoney(paymentAmount, paymentCurrency);
    }

    public String getPaymentAmountTextFormat() {
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(2);
        return df.format(paymentAmount);
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public StoreItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(StoreItem orderItem) {
        this.orderItem = orderItem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(String typeStatus) {
        this.typeStatus = typeStatus;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getActive() {
        return active;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public Long getExternalId() {
        return externalId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getStoreTitle() {
        return storeTitle;
    }

    public void setStoreTitle(String storeTitle) {
        this.storeTitle = storeTitle;
    }

    public String getFullName() {
        return firstName.trim() + " " + lastName.trim();
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public boolean getWithPrize() {
        return withPrize;
    }

    public void setWithPrize(boolean withPrize) {
        this.withPrize = withPrize;
    }

    public String getPrizeText() {
        return prizeText;
    }

    public void setPrizeText(String prizeText) {
        this.prizeText = prizeText;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public void setBillingCompany(String billingCompany) {
        this.billingCompany = billingCompany;
    }

    public String getBillingCompany() {
        return billingCompany;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDni() {
        return dni;
    }

    public Double getPriceForComs() {
        return priceForComs;
    }

    public void setPriceForComs(Double priceForComs) {
        this.priceForComs = priceForComs;
    }

    public String getPriceForComsText() {
        return NumberUtils.formatMoney(priceForComs, paymentCurrency);
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingPostalCode() {
        return shippingPostalCode;
    }

    public void setShippingPostalCode(String shippingPostalCode) {
        this.shippingPostalCode = shippingPostalCode;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getShippingExtra() {
        return shippingExtra;
    }

    public void setShippingExtra(String shippingExtra) {
        this.shippingExtra = shippingExtra;
    }

    public String getDniType() {
        return dniType;
    }

    public void setDniType(String dniType) {
        this.dniType = dniType;
    }

    public boolean getInstallmentComplete() {
        return installmentComplete;
    }

    public boolean isInstallmentComplete() {
        return installmentComplete;
    }

    public void setInstallmentComplete(boolean installmentComplete) {
        this.installmentComplete = installmentComplete;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setPaymentBacs(String paymentBacs) {
        this.paymentBacs = paymentBacs;
    }

    public String getPaymentBacs() {
        return paymentBacs;
    }

}
