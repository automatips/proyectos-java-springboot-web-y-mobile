/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.utils;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.MapUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_member_snapshots")
public class MemberSnapshot implements Serializable {

    private static final long serialVersionUID = -6329619034514303916L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long eventId;
    private Double eventPoints;
    private Long memberId;
    private Boolean isFounder;
    private Long nextRankId;
    private String nextRankName;
    private Long nextRankRequired;
    private Long nextRankRequiredSB;
    @Temporal(TemporalType.TIMESTAMP)
    private Date snapshotDate;
    private Long mostValuableBranch;
    private Boolean speedBonus;
    @Temporal(TemporalType.TIMESTAMP)
    private Date speedBonusEndDate;
    private Double kitsPv;
    private Double productsPv;
    private Double servicesPv;
    private Double personalServicePv;
    private Double sum6040;
    private String sumsPerBranch;
    private String pointsPerChild;
    private Long referers;
    private Long clientCount;
    private String comment;

    @Transient
    private Map<Long, Double> sumsPerBranchMap;
    @Transient
    private Map<Long, Double> pointsPerChildMap;
    @Transient
    private Map<Long, List<Long>> childIds = new HashMap<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nextRankId", updatable = false, insertable = false)
    private RankBis nextRank;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", updatable = false, insertable = false)
    private Member member;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Boolean getIsFounder() {
        return isFounder;
    }

    public void setIsFounder(Boolean isFounder) {
        this.isFounder = isFounder;
    }

    public Long getNextRankId() {
        return nextRankId;
    }

    public void setNextRankId(Long nextRankId) {
        this.nextRankId = nextRankId;
    }

    public String getNextRankName() {
        return nextRankName;
    }

    public void setNextRankName(String nextRankName) {
        this.nextRankName = nextRankName;
    }

    public Long getNextRankRequired() {
        return nextRankRequired;
    }

    public void setNextRankRequired(Long nextRankRequired) {
        this.nextRankRequired = nextRankRequired;
    }

    public Long getNextRankRequiredSB() {
        return nextRankRequiredSB;
    }

    public void setNextRankRequiredSB(Long nextRankRequiredSB) {
        this.nextRankRequiredSB = nextRankRequiredSB;
    }

    public Date getSnapshotDate() {
        return snapshotDate;
    }

    public void setSnapshotDate(Date snapshotDate) {
        this.snapshotDate = snapshotDate;
    }

    public Long getMostValuableBranch() {
        return mostValuableBranch;
    }

    public void setMostValuableBranch(Long mostValuableBranch) {
        this.mostValuableBranch = mostValuableBranch;
    }

    public Boolean getSpeedBonus() {
        return speedBonus;
    }

    public void setSpeedBonus(Boolean speedBonus) {
        this.speedBonus = speedBonus;
    }

    public Date getSpeedBonusEndDate() {
        return speedBonusEndDate;
    }

    public void setSpeedBonusEndDate(Date speedBonusEndDate) {
        this.speedBonusEndDate = speedBonusEndDate;
    }

    public Double getKitsPv() {
        return kitsPv;
    }

    public void setKitsPv(Double kitsPv) {
        this.kitsPv = kitsPv;
    }

    public Double getProductsPv() {
        return productsPv;
    }

    public void setProductsPv(Double productsPv) {
        this.productsPv = productsPv;
    }

    public Double getServicesPv() {
        return servicesPv;
    }

    public void setServicesPv(Double servicesPv) {
        this.servicesPv = servicesPv;
    }

    public Double getPersonalServicePv() {
        return personalServicePv;
    }

    public void setPersonalServicePv(Double personalServicePv) {
        this.personalServicePv = personalServicePv;
    }

    public Double getSum6040() {
        return sum6040;
    }

    public void setSum6040(Double sum6040) {
        this.sum6040 = sum6040;
    }

    public String getSumsPerBranch() {
        if (sumsPerBranch == null) {
            sumsPerBranch = sumsPerBranchMap.toString();
        }
        return sumsPerBranch;
    }

    public void setSumsPerBranch(String sumsPerBranch) {
        this.sumsPerBranch = sumsPerBranch;
    }

    public String getPointsPerChild() {
        if (pointsPerChild == null) {
            pointsPerChild = pointsPerChildMap.toString();
        }
        return pointsPerChild;
    }

    public void setPointsPerChild(String pointsPerChild) {
        this.pointsPerChild = pointsPerChild;
    }

    public Map<Long, Double> getSumsPerBranchMap() {
        if (sumsPerBranchMap == null) {
            sumsPerBranchMap = MapUtils.stringToMap(sumsPerBranch);
        }
        return sumsPerBranchMap;
    }

    public void setSumsPerBranchMap(Map<Long, Double> sumsPerBranchMap) {
        this.sumsPerBranchMap = sumsPerBranchMap;
    }

    public Map<Long, Double> getPointsPerChildMap() {
        if (pointsPerChildMap == null) {
            pointsPerChildMap = MapUtils.stringToMap(pointsPerChild);
        }
        return pointsPerChildMap;
    }

    public void setPointsPerChildMap(Map<Long, Double> pointsPerChildMap) {
        this.pointsPerChildMap = pointsPerChildMap;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void setNextRank(RankBis nextRank) {
        this.nextRank = nextRank;
    }

    public RankBis getNextRank() {
        return nextRank;
    }

    public Map<Long, List<Long>> getChildIds() {
        return childIds;
    }

    public void setChildIds(Map<Long, List<Long>> childIds) {
        this.childIds = childIds;
    }

    public void setEventPoints(Double eventPoints) {
        this.eventPoints = eventPoints;
    }

    public Double getEventPoints() {
        return eventPoints;
    }

    public Long getReferers() {
        return referers;
    }

    public void setReferers(Long referers) {
        this.referers = referers;
    }

    public Long getClientCount() {
        return clientCount;
    }

    public void setClientCount(Long clientCount) {
        this.clientCount = clientCount;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();

        b.append("<pre>");
        b.append("Afiliado: ").append(member).append(" (#").append(member.getId()).append(")").append(System.lineSeparator());
        b.append("Fecha: ").append(DateUtils.formatNormalDateTime(snapshotDate)).append(System.lineSeparator());
        b.append("Puntos Evento: ").append(eventPoints).append("(#").append(eventId).append(")").append(System.lineSeparator());
        b.append("Puntos Totales: ").append(kitsPv + productsPv + servicesPv + personalServicePv).append(System.lineSeparator());
        b.append("Kits: ").append(kitsPv).append(System.lineSeparator());
        b.append("Productos: ").append(productsPv).append(System.lineSeparator());
        b.append("Servicios: ").append(servicesPv).append(System.lineSeparator());
        b.append("Personal PV: ").append(personalServicePv).append(System.lineSeparator());
        b.append("Referidos: ").append(referers == null ? 0 : referers).append(System.lineSeparator());
        b.append("Clientes: ").append(clientCount == null ? 0 : clientCount).append(System.lineSeparator());

        b.append("Ramas:").append(System.lineSeparator());
        for (Map.Entry<Long, Double> entry : getSumsPerBranchMap().entrySet()) {
            Long key = entry.getKey();
            Double value = entry.getValue();
            b.append("\t ID: ").append(key).append("->").append(value).append(System.lineSeparator());
        }
        b.append("Rama mas valiosa: ").append(mostValuableBranch).append(System.lineSeparator());
        b.append("Rango siguiente: ").append(nextRank.getName()).append(" ").append(getNextRank().getQuantity())
                .append("/")
                .append(getNextRank().getSbQuantity())
                .append(System.lineSeparator());
        b.append("Regla 60/40: ").append(sum6040).append(System.lineSeparator());
        b.append("Speed Bonus: ").append(speedBonus).append(System.lineSeparator());
        b.append("Speed Bonus Hasta: ").append(DateUtils.formatNormalDate(member.getSpeedBonusEndDate())).append(System.lineSeparator());
        b.append("Comentario: ").append(comment).append(System.lineSeparator());
        b.append("</pre>");

        return b.toString();
    }

    public MemberSnapshot getClone() {

        MemberSnapshot clon = new MemberSnapshot();
        clon.setEventId(eventId);
        clon.setEventPoints(eventPoints);
        clon.setIsFounder(isFounder);
        clon.setKitsPv(kitsPv);
        clon.setMemberId(memberId);
        clon.setMostValuableBranch(mostValuableBranch);
        clon.setNextRankId(nextRankId);
        clon.setNextRankName(nextRankName);
        clon.setNextRankRequired(nextRankRequired);
        clon.setNextRankRequiredSB(nextRankRequiredSB);
        clon.setPersonalServicePv(personalServicePv);

        clon.setPointsPerChild(getPointsPerChild());
        clon.setProductsPv(productsPv);
        clon.setServicesPv(servicesPv);
        clon.setSnapshotDate(snapshotDate);
        clon.setSpeedBonus(speedBonus);
        clon.setSpeedBonusEndDate(speedBonusEndDate);
        clon.setSum6040(sum6040);
        clon.setSumsPerBranch(getSumsPerBranch());
        clon.setReferers(referers);
        clon.setClientCount(clientCount);
        clon.setComment(comment);

        return clon;

    }

    public String getNextRankPercentage() {
        int percent = (int) ((sum6040 * 100) / nextRankRequired);
        return percent + "%";
    }

    public String getNextRankPercentageText() {
        return (sum6040) + " (60/40) / " + nextRank.getQuantity();
    }
}
