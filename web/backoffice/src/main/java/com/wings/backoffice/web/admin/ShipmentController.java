/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Country;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import com.wings.backoffice.mlm.domain.bis.orders.ShipmentActivity;
import com.wings.backoffice.mlm.enums.LogReference;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.ShipmentsRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.backoffice.mlm.repos.CountriesRepository;
import com.wings.backoffice.mlm.repos.ShipmentActivitiesRepository;
import com.wings.backoffice.mlm.specs.ShipmentsSpecs;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.ShipmentsService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.MapUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class ShipmentController {

    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private ShipmentsRepository shipmentsRepository;
    @Autowired
    private ShipmentsService shipmentsService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private CountriesRepository countryiesRepository;
    @Autowired
    private ShipmentActivitiesRepository shipmentActivitiesRepository;
    @Autowired
    private LogsService logsService;
    @Autowired
    private CountriesService countriesService;

    private final Logger log = LoggerFactory.getLogger(ShipmentController.class);

    @RequestMapping(value = {"admin/orders/shipments"}, method = RequestMethod.GET)
    public String getShippingList(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String deleted = request.getParameter("deleted");
        String country = request.getParameter("country");
        String sort = request.getParameter("sort");

        if (deleted == null) {
            deleted = "false";
        }

        if (country == null || country.equals("all")) {
            country = null;
        }

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        Sort.Direction direction = sort == null || sort.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        PageRequest pr = new PageRequest(page, size, new Sort(direction, "order.activationDate"));
        Page<Shipment> items = shipmentsRepository.findAll(ShipmentsSpecs.search(q, status, deleted, country), pr);

        PageWrapper<Shipment> pageWrapper = new PageWrapper<>(items, "admin/orders/shipments");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("status", status);
        model.addAttribute("deleted", deleted);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());
        model.addAttribute("sort", sort);

        return "admin/orders/shipments";
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/generate"}, method = RequestMethod.POST)
    public ResponseEntity<String> generateShipments(Model model, HttpServletRequest request) {
        String ret;
        try {
            if (shipmentsService.isRunning()) {
                ret = "El proceso esta corriendo, espere por favor";
            } else {
                shipmentsService.createShipmemts();
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("ok");
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/send-email-ec"}, method = RequestMethod.POST)
    public ResponseEntity<String> postSendEmailEc(Model model, HttpServletRequest request) {
        String ret;
        try {
            shipmentsService.sendEmailEcuador();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("ok");
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/send-email-co"}, method = RequestMethod.POST)
    public ResponseEntity<String> postSendEmailCo(Model model, HttpServletRequest request) {
        String ret;
        try {
            shipmentsService.sendEmailColombia();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("ok");
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/send-selected"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendSelected(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        if (ids.isEmpty()) {
            return ResponseEntity.badRequest().body("orders.none.selected");
        }

        String ret;
        try {
            shipmentsService.sendToCurier(ids);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Ok");
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/tracking"}, method = RequestMethod.POST)
    public ResponseEntity<String> reprocessProcesed(Model model, HttpServletRequest request) {
        String ret;
        try {
            shipmentsService.checkTracking();
            ret = "ok";
        } catch (Exception e) {
            log.error("Exception checking tracking ", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    @RequestMapping(value = {"admin/orders/shipments/edit-dest/{id}"}, method = RequestMethod.GET)
    public String getShipmentEdit(Model model, HttpServletRequest request, @PathVariable("id") Long shipmentId) {

        Shipment shipment = shipmentsRepository.findOne(shipmentId);
        model.addAttribute("shipment", shipment);

        return "admin/orders/shipment-edit-dest";
    }

    @RequestMapping(value = {"admin/orders/shipments/edit-dest/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postShipmentEdit(Model model, HttpServletRequest request, @PathVariable("id") Long shipmentId) {

        Shipment shipment = shipmentsRepository.findOne(shipmentId);
        if (shipment == null) {
            return ResponseEntity.badRequest().body("No se encontró el envío");
        }

        String before = ReflectionToStringBuilder.toString(shipment);

        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String zip = request.getParameter("zip");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String status = request.getParameter("status");
        String tracking = request.getParameter("tracking");

        shipment.setShippingName(name);
        shipment.setShippingAddress(address);
        shipment.setShippingCity(city);
        shipment.setShippingState(state);
        shipment.setShippingPostalCode(zip);
        shipment.setPhoneNumber(phone);
        shipment.setEmail(email);
        shipment.setStatus(status);
        shipment.setTrackingNumber(tracking);

        /*if (shipment.getStatus().equals(ShipmentStatus.error.name())) {
            shipment.setStatus(ShipmentStatus.created.name());
        }*/
        //save
        shipmentsRepository.save(shipment);

        //log
        String after = ReflectionToStringBuilder.toString(shipment);
        String diff = MapUtils.getDiff(before, after);
        String ip = AuthUtils.getIp(request);
        AdminUser admin = AuthUtils.getAdminUser();
        String referenceName = LogReference.shipment.name();
        logsService.addGenericLog(ip, admin.getUsername(), shipmentId, referenceName, "shipment modification", diff);

        return ResponseEntity.ok("Envio modificado correctamente");
    }

    @RequestMapping(value = {"admin/shipments/tracking/{id}/view"}, method = RequestMethod.GET)
    public String getOrderActivity(Model model, HttpServletRequest request, @PathVariable("id") Long shipmentId) {
        Shipment shipment = shipmentsService.getOne(shipmentId);
        List<ShipmentActivity> activities = shipmentActivitiesRepository.findByTrackingCode(shipment.getTrackingNumber(), new Sort(Sort.Direction.DESC, "activityDate"));
        model.addAttribute("activities", activities);
        model.addAttribute("shipment", shipment);
        model.addAttribute("trackingNumber", shipment.getTrackingNumber());
        return "common/order-activity";
    }

    //--------------------------------------------------------------------------------
    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/{id}/confirm"}, method = RequestMethod.POST)
    public ResponseEntity<String> markConfirm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        try {
            Shipment s = shipmentsRepository.findOne(id);
            if (s == null) {
                return ResponseEntity.badRequest().body("shipment.notfound");
            }
            s.setConfirmed(!s.isConfirmed());
            shipmentsRepository.save(s);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("shipment.saved");
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/{id}/deliverable"}, method = RequestMethod.POST)
    public ResponseEntity<String> markDeliverable(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        try {
            Shipment s = shipmentsRepository.findOne(id);
            if (s == null) {
                return ResponseEntity.badRequest().body("shipment.notfound");
            }
            s.setDeliverable(!s.isDeliverable());
            shipmentsRepository.save(s);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("shipment.saved");
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/generate-store"}, method = RequestMethod.POST)
    public ResponseEntity<String> generateShipmentsStore(Model model, HttpServletRequest request) {
        String ret;
        try {
            if (shipmentsService.isRunning()) {
                ret = "E proceso esta corriendo, espere por favor";
            } else {
                ret = shipmentsService.generateStoreShipment();
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/process"}, method = RequestMethod.POST)
    public ResponseEntity<String> processColors(Model model, HttpServletRequest request) {
        String ret;
        try {
            if (shipmentsService.isRunning()) {
                ret = "El proceso esta corriendo, espere por favor";
            } else {
                ret = shipmentsService.processMembersColors();
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/status"}, method = RequestMethod.POST)
    public ResponseEntity<String> checkStatus(Model model, HttpServletRequest request) {
        try {
            shipmentsService.checkUpsStatus(null);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("success");
    }

    @ResponseBody
    @RequestMapping(value = {"api/shipments/tracking/{trackingNumber}"}, method = RequestMethod.POST)
    public ResponseEntity getOrderActivityUps(Model model, HttpServletRequest request, @PathVariable("trackingNumber") String trackingNumber) {
        Shipment or = shipmentsRepository.findByTrackingNumber(trackingNumber);
        shipmentsService.checkShipmentStatus(or);
        return ResponseEntity.ok().build();
    }

    @ResponseBody
    @RequestMapping(value = {"admin/orders/shipments/send"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendAll(Model model, HttpServletRequest request) {
        String ret;
        try {
            ret = shipmentsService.sendAll();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    //temporal
    @RequestMapping(value = {"/members/shipment/confirm/{username}"}, method = RequestMethod.GET)
    public String getShipmentForm(Model model, HttpServletRequest request, @PathVariable("username") String username) {

        Member member = membersRepository.findByUsername(username);
        List<Order> order = ordersService.findByActiveAndPayerEmail(true, member.getEmail());
        List<Shipment> shipment = shipmentsRepository.findByOrderId(order.get(0).getId());
        List<Country> countries = countryiesRepository.findAll();

        model.addAttribute("shipment", shipment.get(0));
        model.addAttribute("countries", countries);

        return "members/shipment/shipment";
    }

    //temporal
    @ResponseBody
    @RequestMapping(value = {"/members/shipment/confirm"}, method = RequestMethod.POST)
    public ResponseEntity<String> postShipmentForm(Model model, HttpServletRequest request) {

        String id = request.getParameter("shipmentId");
        String name = request.getParameter("shippingName");
        String address = request.getParameter("shippingAddress");
        String city = request.getParameter("shippingCity");
        String state = request.getParameter("shippingState");
        String cp = request.getParameter("shippingPostalCode");
        String country = request.getParameter("shippingCountry");
        String phone = request.getParameter("phoneNumber");
        String email = request.getParameter("email");

        try {
            Long lid = Long.parseLong(id);
            Shipment ship = shipmentsRepository.findOne(lid);
            if (ship == null) {
                return ResponseEntity.badRequest().body("No se encontró el envio");
            }

            ship.setShippingName(name);
            ship.setShippingAddress(address);
            ship.setShippingCity(city);
            ship.setShippingState(state);
            ship.setShippingPostalCode(cp);
            ship.setPhoneNumber(phone);
            ship.setEmail(email);
            ship.setShippingCountry(country);

            shipmentsRepository.save(ship);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error guardando los cambios:" + e.getMessage());
        }

        return ResponseEntity.ok("success");
    }

}
