/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Topup;
import com.wings.backoffice.mlm.domain.TopupProvider;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.repos.TopUpsProviderRepository;
import com.wings.backoffice.mlm.repos.TopUpsRepository;
import com.wings.backoffice.mlm.specs.TopupSpecs;
import java.io.File;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class TopUpsService {

    @Autowired
    private SumaProviderService sumaProviderService;
    @Autowired
    private TopUpsRepository topUpsRepository;
    @Autowired
    private TopUpsProviderRepository topUpsProviderRepository;

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param provider
     * @param from
     * @param to
     * @return
     */
    public Page<Topup> searchItems(int page, int size, String q, String provider, Date from, Date to) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        return topUpsRepository.findAll(TopupSpecs.searchTopup(q, provider, from, to), pr);
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param provider
     * @param from
     * @param to
     * @return
     */
    public Page<TopupProvider> searchProviderItems(int page, int size, String q, String provider, Date from, Date to) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        return topUpsProviderRepository.findAll(TopupSpecs.searchTopupProvider(q, provider, from, to), pr);
    }

    /**
     *
     * @param topup
     * @return
     */
    public boolean addTopUp(Topup topup) {
        boolean ret = false;
        if (topup.getProvider().equals("suma")) {
            if (sumaProviderService.addTopUp(topup)) {
                topup.setStatus("success");
                ret = true;
            } else {
                topup.setStatus("error");
            }
        }
        save(topup);
        return ret;
    }

    /**
     *
     * @param msisdn
     * @param order
     * @return
     */
    public boolean redeemPackage(String msisdn, Order order) {
        return sumaProviderService.addPackage(msisdn, order.getOrderItem().getCode(), order.getId().toString());
    }

    /**
     *
     * @param topup
     */
    public void save(Topup topup) {
        topUpsRepository.save(topup);
    }

    /**
     *
     * @param topupProvider
     */
    public void saveTopUpProvider(TopupProvider topupProvider) {
        topUpsProviderRepository.save(topupProvider);
    }

    /**
     *
     * @param provider
     * @return
     */
    public List<String> getProviderFiles(String provider) {
        return sumaProviderService.getFileList();
    }

    /**
     *
     * @param fileName
     * @return
     */
    public File getFile(String fileName) {
        return sumaProviderService.getFile(fileName);
    }

    /**
     *
     * @return
     */
    public List<TopupProvider> getAllTopupProvider() {
        return topUpsProviderRepository.findAll(new Sort(Sort.Direction.ASC, "creationDate"));
    }
}
