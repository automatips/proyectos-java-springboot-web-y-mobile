/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.MemberMedia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface MembersMediaRepository extends JpaRepository<MemberMedia, Long> {

    MemberMedia findByCode(String code);

    List<MemberMedia> findByMemberId(Long memberId);

    MemberMedia findByMemberIdAndType(Long memberId, String type);

    List<MemberMedia> findByStatus(String status);

    List<MemberMedia> findByMemberIdAndStatus(Long memberId, String status);

    List<MemberMedia> findByTypeIn(List<String> types);

    List<MemberMedia> findByMemberIdAndTypeIn(Long memberId, List<String> types);

    List<MemberMedia> findByTypeInAndStatus(List<String> types, String status);

}
