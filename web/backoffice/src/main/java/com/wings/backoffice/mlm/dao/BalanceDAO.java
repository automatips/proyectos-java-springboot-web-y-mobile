/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class BalanceDAO {

    @Autowired
    @Qualifier(value = "entityManagerFactory")
    private EntityManager em;

    private final SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public BalanceSums find(String q, Date from, Date to, String type, String bonusType, Long memberId, String country) {

        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.backoffice.mlm.dao.BalanceSums(sum(c.amount) as total,");
        b.append("count(1) as records, ");
        b.append("sum(CASE WHEN (effective = 1) THEN c.amount ELSE 0 END) as effective) ");
        b.append("FROM Balance as c, Member as m1 ");
        b.append("WHERE m1.id = c.memberId ");

        if (q != null) {
            b.append(" AND (");
            b.append(" m1.firstName like '%").append(q).append("%'");
            b.append(" OR m1.lastName like '%").append(q).append("%'");
            b.append(" OR m1.email like '%").append(q).append("%'");
            b.append(" OR m1.username like '%").append(q).append("%'");
            b.append(" OR concat(m1.firstName,' ',m1.lastName) like '%").append(q).append("%')");
        }

        if (memberId != null) {
            b.append(" AND (c.memberId = ").append(memberId).append(")");
        }

        if (from != null) {
            Date withHours = DateUtils.getFirstHourOfDay(from);
            b.append(" AND (c.creationDate >= '").append(sdfFrom.format(withHours)).append("')");
        }

        if (to != null) {
            Date withHours = DateUtils.getLastHourOfDay(to);
            b.append(" AND (c.creationDate <= '").append(sdfFrom.format(withHours)).append("')");
        }

        if (type != null) {
            b.append(" AND (c.type = '").append(type).append("')");
        }

        if (bonusType != null) {
            b.append(" AND (c.bonusType = '").append(bonusType).append("')");
        }

        if (country != null) {
            if (country.equals("other")) {
                String list = "('CO','ES','EC','PE')";
                b.append(" AND (m1.country not in ").append(list).append(")");
            } else {
                b.append(" AND (m1.country = '").append(country).append("')");
            }
        }

        Query query = em.createQuery(b.toString(), BalanceSums.class);
        BalanceSums ret = (BalanceSums) query.getSingleResult();

        return ret;
    }

}
