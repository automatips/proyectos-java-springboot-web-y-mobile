$(document).ready(function () {

    new InputMask().Initialize(document.querySelectorAll('#time'), {
        mask: InputMaskDefaultMask.Time,
        placeHolder: 'HH:mm:ss'
    });

    $('#save').click(function (e) {
        e.preventDefault();

        var id = $('#orderId').val();
        var url = '/admin/order/edit/' + id;
        var formElement = document.getElementById("edit-order-form");

        var request = new XMLHttpRequest();
        var notif = notifyAndWait('Guardando, espere por favor...');
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                notif.remove();
                notifySuccess(request.responseText);
                $('#search-button').click();
                $('.bootbox.modal').modal('hide');
            } else if (request.readyState === 4 && request.status === 400) {
                notif.remove();
                notifyError(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(new FormData(formElement));
    });

});
