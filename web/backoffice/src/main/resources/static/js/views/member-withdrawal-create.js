$(document).ready(function () {

    $('#save').click(function () {

        var notif = notifyAndWait("Creando solicitud, espere por favor...");
        $.post('/members/financial/withdrawal/create', $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            showAlert(xhr.responseText, "alert-error");
        });

    });

    $('#profile').click(function () {
        window.location.href = "/members#members/profile";
        window.location.reload();
    });


    $('#amount').blur(function () {
        calcular();
    });
    
    $('#calc').click(function () {
        calcular();
    });

    function calcular() {

        var data = {};
        var amount = $('#amount').val();
        data.amount = amount;

        $.post('/members/financial/withdrawal/calc', data, function (response) {            
            $('#calc-info').html(response);
        }).fail(function (xhr, status, error) {
            $('#calc-info').html(xhr.responseText);
        });

    }

});
