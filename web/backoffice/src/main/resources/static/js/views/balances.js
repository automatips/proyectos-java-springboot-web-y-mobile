$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'country', $('#country').val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {
        e.preventDefault();

        var action = $(e.target).attr('id');

        if (action === 'insert-balance') {

            loadInDialog('/admin/financial/balance/create', 'Insertar balance ', false, false);

        } else if (action === 'delete-balance') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('¿Eliminar el balance?', name, function (result) {
                if (result) {
                    var notif = notifyAndWait("Eliminando balance, espere por favor...");
                    $.post('/admin/financial/balance/delete/' + id, null, function (response) {
                        notif.remove();
                        notifySuccess(response);
                        $('#search-button').click();
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });

        } else if (action === 'edit-balance') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            loadInDialog('/admin/financial/balance/edit/' + id, 'Editar balance ' + name, false, false);

        }

    });


    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });

});
