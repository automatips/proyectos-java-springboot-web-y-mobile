$(document).ready(function ($) {

     $('#save').click(function (e) {

        e.preventDefault();
        var url = '/admin/store/addtopup';

        var notif = notifyAndWait('Guardando, espere por favor...');
        $.post(url, $('#add-topup-form').serialize(), function (response) {
            notif.remove();
            notifySuccess(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (response) {
            notif.remove();
            notifyError(response.responseText);
        });
    });

});
