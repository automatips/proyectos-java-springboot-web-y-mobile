$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'status', $('#status').val());
        url = addParam(url, 'asignation', $('#asignation').val());
        url = addParam(url, 'size', $('#size').val());
        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });
    
    $('#create-form').click(function (e) {
        e.preventDefault();
        var url = '/admin#support/incidences/create';
        url = addParam(url, 'action', $('#action').val());
        setUpUrl(url);
    });
});
