$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'status', $('#status').val());
        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'add-event') {

            bootbox.prompt({
                title: 'Ingrese el nombre corto del nuevo evento (solo uso interno)',
                inputType: 'text',
                callback: function (result) {
                    if (result) {
                        var postData = {};
                        postData.eventName = result;
                        $.post('/admin/events/create/', postData, function (response) {
                            notifySuccess(response);
                            location.reload();
                        }).fail(function (response) {
                            notifyError(response.responseText);
                        });
                    }
                }
            });

        } else if (action === 'delete-event') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Eliminar Evento', 'Esto eliminará todos los datos relacionados, tickets, compras, productos, etc.\n ¿Esta seguro?', function (result) {
                if (result) {
                    var notif = notifyAndWait("Eliminando, espere por favor...");
                    $.post('/admin/events/delete/' + id, null, function (response) {
                        notif.remove();
                        notifySuccess(response);
                        $('#event-' + id).remove();
                    }).error(function (response) {
                        notifyError(response.responseText);
                    });
                }
            });
        }

    });





});
