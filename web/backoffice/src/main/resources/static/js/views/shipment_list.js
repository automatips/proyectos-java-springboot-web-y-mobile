$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'status', $('#status').val());
        url = addParam(url, 'deleted', $('#deleted').val());
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'sort', $('#sort').val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        switch (action) {
            case 'generate-shipments':
                var notif = notifyAndWait('Generando envios, espere por favor...');
                $.post('/admin/orders/shipments/generate', null, function (response) {
                    notif.remove();
                    notify(response);
                }).fail(function (response) {
                    notif.remove();
                    notify(response.responseText);
                });
                break;
            case 'generate-shipments-store':

                var notif = notifyAndWait('Generando envios tienda, espere por favor...');
                $.post('/admin/orders/shipments/generate-store', null, function (response) {
                    notif.remove();
                    notify(response);
                }).fail(function (response) {
                    notif.remove();
                    notify(response.responseText);
                });
                break;
            case 'check-tracking':
                var notif = notifyAndWait('Verificando envios, espere por favor...');
                $.post('admin/orders/shipments/tracking', null, function (response) {
                    notif.remove();
                    notify(response);
                }).fail(function (response) {
                    notif.remove();
                    notify(response.responseText);
                });
                break;
            case 'send-ups':
                var notif = notifyAndWait('Enviando a ups, espere por favor...');
                $.post('/admin/orders/shipments/send', null, function (response) {
                    notif.remove();
                    notify(response);
                }).fail(function (response) {
                    notif.remove();
                    notify(response.responseText);
                });
                break;
            case 'mark-confirmed':
                var notif = notifyAndWait('Marcando orden, espere por favor...');
                var id = $(e.target).data('id');
                $.post('/admin/orders/shipments/' + id + '/confirm', null, function (response) {
                    notif.remove();
                    notifySuccess("Orden marcada como confirmada!");
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'mark-deliverable':
                var notif = notifyAndWait('Marcando orden, espere por favor...');
                var id = $(e.target).data('id');
                $.post('/admin/orders/shipments/' + id + '/deliverable', null, function (response) {
                    notif.remove();
                    location.reload();
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'send-selected':
                var ids = getCheckedItems();
                var postData = {};
                postData.ids = ids;

                var notif = notifyAndWait('Enviando seleccionados, espere por favor...');
                $.post('/admin/orders/shipments/send-selected/', postData, function (response) {
                    notif.remove();
                    notifySuccess(response);
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'check-ups':
                var notif = notifyAndWait('Controlando ups, espere por favor...');
                $.post('/admin/orders/shipments/status', null, function (response) {
                    notif.remove();
                    notifySuccess(response);
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'process-color':
                var notif = notifyAndWait('Arreglando colores, espere por favor...');
                $.post('/admin/orders/shipments/process', null, function (response) {
                    notif.remove();
                    notifySuccess(response);
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'send-email-ec':
                var notif = notifyAndWait('Enviando email de pedidos, espere por favor...');
                $.post('/admin/orders/shipments/send-email-ec', null, function (response) {
                    notif.remove();
                    notifySuccess(response);
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'send-email-co':
                var notif = notifyAndWait('Enviando email de pedidos, espere por favor...');
                $.post('/admin/orders/shipments/send-email-co', null, function (response) {
                    notif.remove();
                    notifySuccess(response);
                }).fail(function (response) {
                    notif.remove();
                    notifyError(response.responseText);
                });
                break;
            case 'edit-dest':

                var id = $(e.target).data('id');
                var url = "/admin/orders/shipments/edit-dest/" + id;
                loadInDialog(url, "Editar datos destinatario", true, true);

                break;
        }
    });


    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });

});
