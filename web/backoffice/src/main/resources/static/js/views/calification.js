$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');
        if (action === 'view-snap') {
            var id = $(e.target).data('id');
            var title = 'Snapshot para ' + $(e.target).data('name');
            var url = '/admin/members/view-snap/point/' + id;
            loadInDialog(url, title, false);
        }
    });

});
