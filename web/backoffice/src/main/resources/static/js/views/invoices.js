$(document).ready(function ($) {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            buscar();
        }
    });

    $('#search-button').click(function (e) {
        e.preventDefault();
        buscar();
    });

    function buscar() {
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'from', $("#from").val());
        url = addParam(url, 'to', $("#to").val());
        url = addParam(url, 'country', $("#country").val());

        setUpUrl(url);
    }


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'edit-invoice') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            loadInDialog('/admin/financial/invoices/edit/' + id, 'Editando factura ' + name, false, false);

        } else if (action === 'view-invoice') {

            var id = $(e.target).data('id');
            var url = 'admin/financial/invoices/' + id;
            window.open(url, '_blank');

        } else if (action === 'view-external-invoice') {

            var id = $(e.target).data('id');
            var url = 'http://movil.wingsmobile.net/invoices/' + id;
            window.open(url, '_blank');

        } else if (action === 'invoice-generate') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/financial/invoices/generate', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        }
    });


    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });


});
