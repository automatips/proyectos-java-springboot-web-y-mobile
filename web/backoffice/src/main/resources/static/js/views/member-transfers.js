$(document).ready(function () {

    $('#recipient').blur(function (e) {
        search();
    });

    $('#search-recipient').click(function (e) {
        e.preventDefault();
        search();
    });


    $('#transfer-button').click(function (e) {

        e.preventDefault();

        var amount = $('#amount').val();
        var recipient = $('#recipient').val();
        var recipientName = $('#recipientName').val();

        var data = {};
        data.amount = amount;
        data.recipient = recipient;

        confirmDialogDanger('Transferir fondos', 'Confirma la transferencia de ' + amount + ' a ' + recipientName, function (result) {
            if (result) {
                var notif = notifyAndWait("Efectuando transferencia, espere por favor...");
                $.post('/members/financial/transfers/', data, function (response) {
                    notif.remove();
                    notifySuccess(response);
                    location.reload();
                }).fail(function (xhr, status, error) {
                    notif.remove();
                    notifyError(xhr.responseText);
                });
            }
        });
    });

    function search() {
        var recipient = $('#recipient').val();
        var data = {};
        data.recipient = recipient;
        $.post('/members/financial/transfers/search/', data, function (response) {
            $('#memberName').html(response);
            $('#recipientName').val(response);
            $('#transfer-button').removeAttr('disabled');
        }).fail(function (response) {
            $('#memberName').html(response.responseText);
            $('#transfer-button').addAttr('disabled');
        });
    }


});


