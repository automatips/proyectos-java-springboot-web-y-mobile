$(document).ready(function () {


    $('#toggle-enabled').click(function (e) {
        e.preventDefault();

        var id = $(this).data("id");
        var url = "/admin/store/item/enable/" + id;

        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $.post(url, null, function (response) {
            notif.remove();
            notifySuccess(response);

            location.reload();
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });

    });

});

