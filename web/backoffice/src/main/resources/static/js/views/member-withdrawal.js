$(document).ready(function () {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('#search-button').click(function (e) {
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'status', $('#status').val());

        setUpUrl(url);
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        switch (action) {
            case 'withdrawal-request':
                loadInDialog('/members/financial/withdrawal/create', 'Crear solicitud de retiro', false, false);
                break;
            case 'delete-request':

                var title = 'Confirmar eliminación';
                var message = '¿Desea eliminar la solicitud?';
                confirmDialogDanger(title, message, function (result) {
                    if (result) {
                        var id = $(e.target).data('id');
                        var notif = notifyAndWait("Eliminando solicitud, espere por favor...");
                        $.post('/members/financial/withdrawal/delete/' + id, null, function (response) {
                            notif.remove();
                            notifySuccess(response);
                            $('#search-button').click();
                        }).fail(function (xhr, status, error) {
                            notif.remove();
                            notifyError(xhr.responseText);
                        });
                    }
                });
                break;
            case 'add-account':
                loadInDialog('/members/financial/account/create', 'Crear cuenta para retiro', false, false);
                break;
            case 'delete-account':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');

                var title = 'Confirmar eliminación';
                var message = '¿Desea eliminar la cuenta <b>' + name + '</b>?';
                confirmDialogDanger(title, message, function (result) {
                    if (result) {
                        $.post('/members/financial/account/delete/' + id, null, function (response) {
                            notifySuccess(response);
                            location.reload();
                        }).fail(function (response) {
                            notifyError(response.responseText);
                        });
                    }
                });
                break;

            case 'edit-account':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');
                loadInDialog('/members/financial/account/edit/' + id, 'Editar cuenta ' + name, false, false);
                break;
                
            case 'upload-bac':

                var name = $(e.target).data('name');
                var id = $(e.target).data('id');
                var title = 'Añadir comprobante para cuenta bancaria';

                var url = '/members/profile/media/add/' + name + '/' + id;
                var dialog = loadInDialog(url, title, false, false);

                dialog.on('hidden.bs.modal', function (e) {
                    location.reload();
                });
                break;
        }

    });

});
