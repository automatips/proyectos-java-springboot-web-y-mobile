$(document).ready(function ($) {

    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A', size: 'small'
            });
        });
    }

    $('.action-button').click(function (e) {
        e.preventDefault();
        var action = $(e.target).val();
        var code = $("#code").val();
        var oldCode = $("#oldCode").val();
        var name = $("#name").val();
        var status = $("#status").is(':checked');

        $.post('/admin/settings/countries/create', {action: action, code: code, name: name, status: status, oldCode: oldCode}, function (response) {
            notify(response);
            var url = "admin/settings/countries";
            setUpUrl(url);
        }).error(function (response) {
            notifyError(response.responseText);
        });
    });
});
