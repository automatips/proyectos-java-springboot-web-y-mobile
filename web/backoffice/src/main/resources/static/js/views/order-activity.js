$(document).ready(function ($) {

    /**acciones de los botones **/
    $('.action-item').click(function (e) {

        var trackingNumber = $(this).data('tracking-number');
        e.preventDefault();
        var action = $(e.target).attr('id');
        
        if (action === 'action-refresh-order-status') {
            
            $body = $('#viewModal').find('.modal-body');
            $body.html('<div class="text-center"><i class="fa fa-circle-o-notch fa-lg mt-4 fa-spin"></i></div>');
                        
            $.post('api/shipments/tracking/' + trackingNumber, null, function (data, status) {
            
                if (status === 'success') {
                    var url = 'admin/shipments/tracking/'+trackingNumber+'/view';
                    $body.load(url);
                } else {
                    $.notify('Ocurrio un error!', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        }
    });

});
