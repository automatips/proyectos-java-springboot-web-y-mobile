$(document).ready(function () {

    $('#save').click(function (e) {

        e.preventDefault();

        var notif = notifyAndWait("Agregando sims, espere por favor...");
        $.post('/members/tools/sims-add', $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

});
