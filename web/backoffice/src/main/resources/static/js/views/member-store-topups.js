$(document).ready(function () {

    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');

        if (action === 'buy-packs') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            
            //Antes de esto necesito asignar el nro de linea

            confirmDialog('Comprar Paquete', 'Confirma la compra del paquete ' + name, function (result) {
                if (result) {
                    var notif = notifyAndWait("Efectuando compra, espere por favor...");
                    $.post('/members/store/buy/' + id, null, function (response) {
                        notif.remove();
                        window.location = response;
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });
        } 
    });


});

