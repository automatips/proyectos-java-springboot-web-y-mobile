$(document).ready(function () {

    $("#orders").hide();

    new InputMask().Initialize(document.querySelectorAll('#time'), {
        mask: InputMaskDefaultMask.Time,
        placeHolder: 'HH:mm:ss'
    });

    $('#saveReview').click(function (e) {
        e.preventDefault();

        var id = $('#orderId').val();
        var url = '/admin/orders/review/' + id;
        var formElement = document.getElementById("edit-order-form");

        var request = new XMLHttpRequest();
        var notif = notifyAndWait('Guardando, espere por favor...');
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                notif.remove();
                notifySuccess(request.responseText);
                $('#search-button').click();
                $('.bootbox.modal').modal('hide');
            } else if (request.readyState === 4 && request.status === 400) {
                notif.remove();
                notifyError(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(new FormData(formElement));

    });

    $('#savePaid').click(function (e) {
        e.preventDefault();

        var id = $('#orderId').val();
        var url = '/admin/orders/paid/' + id + "/save";

        //esto no anda con los archivos
        /*var notif = notifyAndWait('Guardando, espere por favor...');
         $.post(url, $('#edit-order-form').serialize(), function (response) {
         notif.remove();
         notifySuccess(response);
         $('#search-button').click();
         $('.bootbox.modal').modal('hide');
         }).fail(function (response) {
         notif.remove();
         notifyError(response.responseText);
         });*/

        var formElement = document.getElementById("edit-order-form");
        var request = new XMLHttpRequest();
        var notif = notifyAndWait('Guardando, espere por favor...');
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                notif.remove();
                notifySuccess(request.responseText);
                $('#search-button').click();
                $('.bootbox.modal').modal('hide');
            } else if (request.readyState === 4 && request.status === 400) {
                notif.remove();
                notifyError(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(new FormData(formElement));
    });

    $(('#time')).on('focusout', function () {

        var date = $("#date").val();
        var time = $("#time").val();
        var url = "/admin/orders/validate";

        if (date != null && date != "" && time != null && time != "") {
            var notif = notifyAndWait('Validando fecha...');
            $.post(url, {date: date, time: time}, function (response) {
                notif.remove();

                if (response.length > 0) {
                    var table = "<table border='1' class='table table-condensed table-striped jambo_table'>";
                    table += "<tr><th>Pedido N°</th><th>Email</th><th>Cliente</th><th>Comprobante</th></tr><tbody>";
                    for (i = 0; i < response.length; i++) {
                        var order = response[i];
                        var row = "<tr>";
                        row += "<td><a href='/admin#admin/orders/list?q=" + order.id + "' target='_blank'>" + order.id + "</a></td>";
                        row += "<td>" + order.payerEmail + "</td>";
                        row += "<td>" + order.client + "</td>";
                        if (order.attachment != null) {
                            row += "<td><a href='/admin/orders/attachment/view?file=" + order.attachment + "' target='_blank'>Ver comprobante</a></td>";
                        } else {
                            row += "<td></td></tr>";
                        }

                        table += row;
                    }
                    table += "</tbody></table>";

                    $("#orders-paid").html(table);
                    $("#orders").show();
                    $("#alert").html("Un comprobante con esa fecha exacta ya existe en el sistema");
                } else {
                    $("#orders").hide();
                }
            }).fail(function (response) {
                notif.remove();
                notifyError(response.responseText);
                var r = response;
            });
        }
    });

    $('#paymentReview').change(function () {
        var val = $(this).val();
        if (val === 'bacs') {
            $('#bankReview').show();
            $('#saveReview').show();
            $('#savePaid').hide();
        } else {
            $('#bankReview').hide();
            $('#saveReview').hide();
            $('#savePaid').show();
        }
    });

});
