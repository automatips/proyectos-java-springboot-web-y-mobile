$(document).ready(function ($) {

    var chkElement;
    var inputElement;
    var buttonElement;
    var xreq;


    $('#content-country').chosen();
    $('#content-rank').chosen();
    $('#content-file-chk').hide();
    $('#thumb-file-chk').hide();

    $('input[type=file]').click(function () {

        $('#content-file-progress').css("width", 0);
        if ($(this).attr("id") === 'content-file') {
            $('#content-file-chk').hide();
            $('#btn-upload-content').show();
        } else {
            $('#thumb-file-chk').hide();
            $('#btn-upload-thumb').show();
        }

    });

    $('#save-content').click(function (e) {
        e.preventDefault();

        if (!validateForm()) {
            return;
        }

        showAlertAutoHide("Guardando, espere por favor...");
        $.post('/admin/tools/content-add', $('#create-form').serialize(), function (response) {

            showAlert(response.message, "alert-success");
            $('#content-id').val(response.contentId);

        }).fail(function (xhr, status, error) {
            showAlert(xhr.responseText, "alert-error");
        });

    });

    function validateForm() {

        if ($('#title').val() === '') {
            showAlert("El título es obligatorio", "alert-error");
            return false;
        }
        if ($('#content-id').val() === '' && $('#content-file-hidden').val() === '') {
            showAlert("El archivo de material es obligatorio", "alert-error");
            return false;
        }

        if ($('#content-id').val() === '' && $('#thumb-file-hidden').val() === '') {
            showAlert("Las imágen miniatura es obligatoria", "alert-error");
            return false;
        }

        return true;
    }

    $('#btn-upload-content').click(function () {
        if ($('#content-file')[0].files.length === 0) {
            showAlertAutoHide("Seleccione un archivo", "alert-error");
            return;
        }

        uploadFile('file', $('#content-file'));
        chkElement = $('#content-file-chk');
        inputElement = $('#content-file-hidden');
        buttonElement = $(this);
    });

    $('#btn-upload-thumb').click(function () {
        if ($('#thumb-file')[0].files.length === 0) {
            showAlertAutoHide("Seleccione un archivo", "alert-error");
            return;
        }

        uploadFile('thumb', $('#thumb-file'));
        chkElement = $('#thumb-file-chk');
        inputElement = $('#thumb-file-hidden');
        buttonElement = $(this);
    });

    function uploadFile(name, inputElement) {
        xreq = new XMLHttpRequest();
        var formData = new FormData();
        formData.set(name, inputElement[0].files[0]);
        xreq.upload.addEventListener('progress', updateProgress, false);
        xreq.addEventListener('load', uploadDone, false);
        xreq.open('POST', '/admin/tools/content-add/file');
        xreq.send(formData);
    }

    function updateProgress(e) { // upload process in progress
        if (e.lengthComputable) {
            var percentComplete = Math.round(e.loaded * 100 / e.total);
            $('#content-file-progress').css("width", percentComplete.toString() + '%');
        }
    }


    function uploadDone() { // upload process in progress        
        if (xreq.status === 200) {
            chkElement.show();
            buttonElement.hide();
            inputElement.val(xreq.responseText);
        } else {
            showAlert(xreq.responseText, "alert-error");
        }
    }

});
