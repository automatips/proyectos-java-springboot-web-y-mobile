$(document).ready(function () {

    $('#save').click(function () {
        showAlert("Modificando kit, espere por favor...");
        $.post($('#request-form').attr('action'), $('#request-form').serialize(), function (response) {
            showAlert(response, "alert-success");
        }).fail(function (xhr, status, error) {
            showAlert(xhr.responseText, "alert-error");
        });
    });

});
