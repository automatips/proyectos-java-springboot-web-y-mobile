alter table bis_balance add column `reference_id` bigint(20) null;
alter table bis_balance add column `effective` tinyint(1) not null default 0;
alter table bis_balance add column `status_text` varchar(200) null;

