alter table bis_withdrawals add column account_type varchar(250) default null;
alter table bis_withdrawals add column details varchar(250) default null;
alter table bis_withdrawals add column currency varchar(5) default null;
alter table bis_withdrawals MODIFY amount decimal(24,6);
alter table bis_withdrawals MODIFY money_base decimal(24,6);
alter table bis_withdrawals MODIFY money_tax decimal(24,6);
alter table bis_withdrawals MODIFY money_retention decimal(24,6);
alter table bis_withdrawals MODIFY money_payable decimal(24,6);
alter table bis_member_bacs add column account_type varchar(250) default null;



