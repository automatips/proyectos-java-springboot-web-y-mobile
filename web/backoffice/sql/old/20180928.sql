alter table bis_store_items add column `source` varchar(250) default 'store';

--Precios
-- Starter         249
-- Starter Pro     549
-- Combo           649
-- Combo Pro       699
-- Mini Founder    999
-- Golden Founder  999
-- Diamond Founder 1999
-- 
-- Wings POP           499
-- Wings RedPassion    549
-- Wings OneFour       549
-- Wings XL            599
-- 
-- W2+     249
-- W5+     549


--FALTANNN

--Wings Store Full
--Wings Store Corner Full
--Wings Store Corner Light
--Wings Store Base


INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock, `source`) 
	VALUES ('STRT', 'STARTER', '', 1, 249, 25, 125, -2, 'store-es,store-co,store-it'),
         ('STRTPRO', 'STARTER PRO', '', 1, 549, 55, 300, -2, 'store-es,store-co,store-it'),
	 ('CMB', 'COMBO', '', 1, 649, 65, 325, -2, 'store-es,store-co,store-it'),
	 ('CMBPRO', 'COMBO PRO', '', 1, 699, 70, 350, -2, 'store-es,store-co,store-it'),
	 ('MINFND', 'MINI FOUNDER', '', 1, 999, 100, 500, -2, 'store-es,store-co,store-it'),
	 ('GLDFND', 'GOLDEN FOUNDER', '', 1, 999, 100, 500, -2, 'store-es,store-co,store-it'),
	 ('DMDFND', 'DIAMOND FOUNDER', '', 1, 1999, 200, 1000, -2, 'store-es,store-co,store-it'),
	 ('WINPOP', 'Wings POP', '', 2, 499, 50, 250, -2, 'store-es,store-co,store-it'),
	 ('WINREP', 'Wings REDPASSION', '', 2, 549, 55, 275, -2, 'store-es,store-co,store-it'),
	 ('WINOFR', 'Wings ONEFOUR', '', 2, 549, 55, 275, -2, 'store-es,store-co,store-it'),
	 ('WINXL', 'Wings XL', '', 2, 599, 60, 300, -2, 'store-es,store-co,store-it'),
	 ('PHONEW2+', 'Telefono W2+', '', 2, 249, 25, 125, -2, 'store-es,store-co,store-it'),
	 ('PHONEW5+', 'Telefono W5+', '', 2, 549, 55, 275, -2, 'store-es,store-co,store-it');

select * from MLM.bis_store_items;

--Ids de lo insertado arriba
-- 69  STARTER
-- 70  STARTER PRO
-- 71  COMBO
-- 72  COMBO PRO
-- 73  MINI FOUNDER
-- 74  GOLDEN FOUNDER
-- 75  DIAMOND FOUNDER
-- 76  Wings POP
-- 77  Wings REDPASSION
-- 78  Wings ONEFOUR
-- 79  Wings XL
-- 80  Telefono W2+
-- 81  Telefono W5+

--eliminino la constrain de unique para el name
ALTER TABLE MLM.bis_store_item_names DROP KEY bis_store_item_names_UN ;
--nuevos nombres para es, co e it
INSERT INTO MLM.bis_store_item_names (item_id, item_name) 
	VALUES 
        (69, 'STARTER'),
        (69, 'STARTER - RED'),
        (69, 'STARTER - BLACK'),
        (70, 'STARTER PRO'),
        (71, 'COMBO'),
        (71, 'COMBO - RED'),
        (71, 'COMBO - BLACK'),
        (72, 'COMBO PRO'),
        (73, 'Mini FOUNDER'),
        (73, 'Mini Founder'),
        (73, 'Mini FOUNDER - RED'),
        (73, 'Mini FOUNDER - BLACK'),
        (74, 'GOLDEN FOUNDER'),
        (74, 'Golden FOUNDER'),
        (75, 'DIAMOND FOUNDER'),
        (75, 'Diamond FOUNDER'),
        (76, 'Wings POP'),
        (76, 'WINGS POP'),
        (77, 'WINGS REDPASSION'),
        (77, 'Wings REDPASSION'),
        (78, 'Wings ONEFOUR'),
        (78, 'WINGS ONEFOUR'),
        (79, 'WINGS XL'),
        (79, 'Wings XL'),
        (80, 'Teléfono W2+ - RED'),
        (80, 'Teléfono W2+ - BLACK'),
        (80, 'Teléfono W2+'),
        (80, 'W2+'),
        (81, 'Teléfono W5+'),
        (81, 'W5+')
;
        
