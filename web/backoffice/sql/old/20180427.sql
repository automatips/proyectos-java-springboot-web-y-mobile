alter table bis_invoices add column token varchar(250) default null;
alter table bis_invoices add column client_country varchar(250) default null;
alter table bis_invoices add column member_id bigint(20) default null;
