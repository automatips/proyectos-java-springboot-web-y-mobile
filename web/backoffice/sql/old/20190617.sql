alter table bis_members add column idt varchar(100) default null;
alter table bis_members add column idt_type varchar(50) default '0';
alter table bis_member_media add column status varchar(50) default null;
alter table bis_member_media add column status_audit text default null;
alter table bis_member_media add column status_comment text default null;

DROP TABLE bis_member_bacs;
CREATE TABLE `bis_member_bacs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20),
  `creation_date` TIMESTAMP NOT NULL,
  `account_name` varchar(250) default NULL,
  `bank_name` varchar(250) default NULL,
  `account_number` varchar(250) default NULL,
  `account_iban` varchar(250) default NULL,
  `account_swift` varchar(250) default NULL,
  `description` text default null,
  `verified` tinyint(1) default 0,
  `media_id` bigint(20),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


alter table bis_withdrawals add column account_bank varchar(250);
alter table bis_withdrawals add column account_iban varchar(250);
alter table bis_withdrawals add column account_swift varchar(250);
alter table bis_withdrawals add column account_description text;
ALTER TABLE bis_withdrawals MODIFY amount decimal(10,6);
ALTER TABLE bis_withdrawals MODIFY money_base decimal(10,6);
ALTER TABLE bis_withdrawals MODIFY money_tax decimal(10,6);
ALTER TABLE bis_withdrawals MODIFY money_retention decimal(10,6);
ALTER TABLE bis_withdrawals MODIFY money_payable decimal(10,6);
update bis_members set contract_signed = false;