DROP TABLE `bis_member_media`;
CREATE TABLE `bis_member_media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,  
  `code` varchar(200) default null,
  `path` varchar(250) default null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `bis_tokens` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    token VARCHAR(250) NOT NULL,
    token_type VARCHAR(100),
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL,
    expire_date DATETIME DEFAULT NULL,
    reference_id BIGINT,
    active TINYINT(1) DEFAULT 1, 
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bis_logs` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,    
    `creation_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL,
    `type` VARCHAR(100),
    `reference` VARCHAR(100),
    `reference_id` BIGINT,
    `comment` VARCHAR(250),
    `message` text,
    `mod_user` VARCHAR(250),
    `mod_ip` VARCHAR(100),    
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
