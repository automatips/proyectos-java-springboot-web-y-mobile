CREATE TABLE `MLM`.`bis_store_item_images` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `store_item_id` BIGINT(20) NOT NULL,
  `image` VARCHAR(255) NULL,
  `name` VARCHAR(100) NULL,
  `main` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_store_item_images_store_items_idx` (`store_item_id` ASC),
  CONSTRAINT `fk_store_item_images_store_items`
    FOREIGN KEY (`store_item_id`)
    REFERENCES `MLM`.`bis_store_items` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
