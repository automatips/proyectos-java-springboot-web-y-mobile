INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) VALUES 
('upgrade-stf','Upgrade STARTER a FOUNDER',NULL,4,1750,175,875,-1),
('upgrade-sx2tf','Upgrade STARTERX2 a FOUNDER',NULL,4,1600,160,800,-1),
('upgrade-s2tf','Upgrade STARTER2 a FOUNDER',NULL,4,1500,150,750,-1),
('upgrade-ctf','Upgrade COMBO a FOUNDER',NULL,4,1400,140,700,-1),
('upgrade-cptf','Upgrade COMBO PRO a FOUNDER',NULL,4,1000,100,500,-1),
('upgrade-cbtf','Upgrade COMBO BUSINESS a FOUNDER',NULL,4,600,60,300,-1),
('upgrade-stmf','Upgrade STARTER a MiniFOUNDER',NULL,4,750,75,375,-1),
('upgrade-sx2tmf','Upgrade STARTERX2 a MiniFOUNDER',NULL,4,600,60,300,-1),
('upgrade-s2tmf','Upgrade STARTER2 a MiniFOUNDER',NULL,4,500,50,250,-1),
('upgrade-ctmf','Upgrade COMBO a MiniFOUNDER',NULL,4,400,40,200,-1);


delete from MLM.bis_store_item_names where item_id = 48;
INSERT INTO MLM.bis_store_item_names (item_id, item_name) VALUES
(54,'Upgrade to Founder - Desde STARTER a FOUNDER'),
(55,'Upgrade to Founder - Desde STARTER2 a FOUNDER'),
(56,'Upgrade to Founder - Desde STARTERx2 a FOUNDER'),
(57,'Upgrade to Founder - Desde COMBO a FOUNDER'),
(58,'Upgrade to Founder - Desde COMBO PRO a FOUNDER'),
(59,'Upgrade to Founder - Desde COMBO BUSINESS a FOUNDER'),
(54,'Upgrade STARTER a FOUNDER'),
(55,'Upgrade STARTER2 a FOUNDER'),
(56,'Upgrade STARTERx2 a FOUNDER'),
(57,'Upgrade COMBO a FOUNDER'),
(58,'Upgrade COMBO PRO a FOUNDER'),
(59,'Upgrade COMBO BUSINESS a FOUNDER'),
(60,'Upgrade to Founder - Desde STARTER a MiniFOUNDER'),
(62,'Upgrade to Founder - Desde STARTER2 a MiniFOUNDER'),
(61,'Upgrade to Founder - Desde STARTERx2 a MiniFOUNDER'),
(63,'Upgrade to Founder - Desde COMBO a MiniFOUNDER'),
(60,'Upgrade STARTER a MiniFOUNDER'),
(62,'Upgrade STARTER2 a MiniFOUNDER'),
(61,'Upgrade STARTERx2 a MiniFOUNDER'),
(63,'Upgrade COMBO a MiniFOUNDER');

alter table bis_members add column founder_date DATETIME;