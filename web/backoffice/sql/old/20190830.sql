alter table bis_shipment_items add column store_item_id bigint(20);
alter table bis_shipments modify column status_text text;

CREATE TABLE `bis_countries_states` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(20) NOT NULL,
  `state` varchar(200) NOT NULL,
  `custom1` varchar(150),
  `custom2` varchar(150),
  `custom3` varchar(150),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bis_countries_cities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(20),
  `state_id` bigint(20),
  `city` varchar(200) NOT NULL,
  `custom1` varchar(150),
  `custom2` varchar(150),
  `custom3` varchar(150),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

