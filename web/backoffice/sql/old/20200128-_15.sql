CREATE TABLE `MLM`.`bis_member_sims` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `member_id` BIGINT NULL,
  `iccid_id` BIGINT NULL,
  `iccid` VARCHAR(100) NULL,
  `creation_date` datetime NULL,
  `activation_date` datetime NULL,
  `order_id` BIGINT NULL,
  `status` VARCHAR(45) NULL,
  `deleted` TINYINT NULL,
  `deleted_date` datetime NULL,
  PRIMARY KEY (`id`));
