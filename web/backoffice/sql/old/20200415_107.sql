create or replace
algorithm = UNDEFINED view `mlm`.`vw_ordersactives` as
select
    `o`.`id` as `order_Id`,
    (case
        when (`o`.`type` = 'upgrade') then `si`.`final_item_id`
        else `o`.`item_id` end) as `item_id`,
    `o`.`type` as `type`,
    `o`.`store_title` as `store_title`,
    `o`.`billing_country` as `billing_country`,
    `o`.`activation_date` as `activation_date`,
    `o`.`member_id` as `member_id`,
    `o`.`installment_complete` as `installment_complete`,
    `o`.`installment_number` as `installment_number`
from
    (`mlm`.`bis_orders` `o`
join `mlm`.`bis_store_items` `si` on
    ((`o`.`item_id` = `si`.`id`)))
where
    ((`o`.`status` = 'paid')
    and (`o`.`deleted` = 0)
    and (`o`.`item_id` <> 15)
    and ((`o`.`installment_number` is null)
    or (`o`.`installment_number` in (0,
    1))))
order by
    `o`.`activation_date`;

