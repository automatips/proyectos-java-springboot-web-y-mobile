drop table `bis_speed_bonus`;
CREATE TABLE `bis_speed_bonus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `valid_from` DATETIME DEFAULT NULL,
  `valid_to` DATETIME DEFAULT NULL,
  `duration_to` DATETIME DEFAULT NULL,
  `duration_to_founder` DATETIME DEFAULT NULL,
  `duration_days` int default null,
  `duration_days_founder` int default null, 
  `price_rank1` decimal(10,2) default null,
  `price_rank2` decimal(10,2) default null,
  `price_rank3` decimal(10,2) default null,
  `price_rank4` decimal(10,2) default null,
  `price_rank5` decimal(10,2) default null,
  `price_rank6` decimal(10,2) default null,
  `price_rank7` decimal(10,2) default null,
  `price_rank8` decimal(10,2) default null,
  `price_rank9` decimal(10,2) default null,    
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table `bis_members` add column `speed_bonus_id` bigint(20) default null;
