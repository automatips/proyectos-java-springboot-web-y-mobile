CREATE TABLE `bis_shipment_activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_date` timestamp NULL DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `detail` tinyint(4) DEFAULT NULL,
  `tracking_code` varchar(100) DEFAULT NULL,
  `address_city` varchar(200) DEFAULT NULL,
  `address_postal_code` varchar(200) DEFAULT NULL,
  `address_country_code` varchar(200) DEFAULT NULL,
  `location_code` varchar(200) DEFAULT NULL,
  `location_description` varchar(200) DEFAULT NULL,
  `signed_for_by_name` varchar(200) DEFAULT NULL,
  `package_address_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_activities_activity_date_IDX` (`activity_date`,`tracking_code`) USING BTREE,
  KEY `order_activities_tracking_code_IDX` (`tracking_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `k` varchar(100) DEFAULT NULL,
  `v` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_UN` (`k`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
