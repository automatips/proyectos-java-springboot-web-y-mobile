alter table bis_invoices add column `type` varchar(100) not null;
alter table bis_invoices add column `creation_date` TIMESTAMP not null default current_timestamp;
alter table bis_invoices add column `type_number` bigint(20) not null default 1;
alter table bis_invoices add column `type_id` bigint(20) not null default 0;
