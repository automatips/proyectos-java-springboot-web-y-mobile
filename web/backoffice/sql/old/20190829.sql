
--APLICADO
--Add W3 options
--W3 - black
INSERT INTO MLM.bis_store_items_options (store_item_id, ean_code, `name`, `value`, in_stock, stock, option_name, product_name) 
	VALUES (158, '1010108337', 'color-w3', 'black', true, 1800, 'color', 'W3');
--W3 - red
INSERT INTO MLM.bis_store_items_options (store_item_id, ean_code, `name`, `value`, in_stock, stock, option_name, product_name) 
	VALUES (158, '1010108338', 'color-w3', 'red', true, 0, 'color', 'W3');
--W3 - blue
INSERT INTO MLM.bis_store_items_options (store_item_id, ean_code, `name`, `value`, in_stock, stock, option_name, product_name) 
	VALUES (158, '1010108339', 'color-w3', 'blue', true, 0, 'color', 'W3');
