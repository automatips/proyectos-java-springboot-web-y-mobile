
alter table bis_store_items add column `reference` varchar(100) default null COMMENT 'cadena de referencia cuando el item esta ligado a otro, no se usa por ahora';
alter table bis_store_items add column `price_for_coms` decimal(24,6) default null COMMENT 'precio descuento  pago comisiones';
alter table bis_orders add column price_for_coms decimal(24,6) default null COMMENT 'precio descuento  pago comisiones';
alter table bis_members add column payments_enabled tinyint(1) NOT NULL DEFAULT 0 COMMENT 'permite uso de comisiones en la pasarela';


DROP TABLE `bis_events`;
CREATE TABLE `bis_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_date` TIMESTAMP NOT NULL,
  `event_date` DATETIME DEFAULT NULL,
  `name` varchar(250) default NULL,
  `short_name` varchar(50) NOT NULL,
  `date` varchar(150) default NULL COMMENT 'fecha del evento en formato dd/MM/yyyy',
  `hour` varchar(150) default NULL COMMENT 'Hora del evento en formato HH:mm',
  `place` varchar(250) default NULL COMMENT 'Lugar del evento',
  `place_address` varchar(250) default NULL COMMENT 'Lugar del evento',
  `image` varchar(250) default NULL COMMENT 'imagen o logo del evento',
  `ticket_background` varchar(250) default NULL COMMENT 'imagen de background del ticket',
  `ticket_color` varchar(100) default NULL COMMENT 'color de fondo si no tiene especificada imagen',
  `generate_register` tinyint(1) default 0 COMMENT 'si true, cuando la persona asiste al evento se genera un afiliado en modo silencioso, permitir opt-in cuando la persona llena los datos',
  `ticket_numbers_from` integer default 0 COMMENT 'Intervalo de generacion de nros de tickets, desde',
  `ticket_numbers_to` integer default 0 COMMENT 'Intervalo de generacion de nros de tickets, hasta',
  `ticket_numbers_pattern` varchar(20) default NULL COMMENT 'Patron para la generacion de ticket numbers, ej ###-##### o ########',
  `available_countries` varchar(200) default NULL COMMENT 'lista separada por comas, de los isos de los paises a los cuales ofertar el evento',
  `seats_numbers` tinyint(1) default 0 COMMENT 'si true, permite generar numeracion de asientos',
  `disclaimer` varchar(250) default NULL COMMENT 'texto que sale al final del ticket',
  `status` varchar(250) default NULL COMMENT 'estado del evento, creado, en curso (publicado), finalizado',
  `show_name` tinyint(1) default 1 COMMENT 'mostrar el nombre del evento en el ticket',
  `show_date` tinyint(1) default 1 COMMENT 'mostrar la fecha del evento',
  `show_hour` tinyint(1) default 1 COMMENT 'mosdtrar la hora del evento',
  `show_place` tinyint(1) default 1 COMMENT 'mostrar el lugar',
  `show_place_address` tinyint(1) default 1 COMMENT 'mostrar la direccion',
  `text_color` varchar(100) default '#000000' COMMENT 'color del texto',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `bis_events_seats`;
CREATE TABLE `bis_events_seats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `pool_id` bigint(20) NOT NULL,
  `creation_date` TIMESTAMP NOT NULL,
  `taken_date` DATETIME NULL COMMENT 'fecha cuando se tomo el asiento',
  `seat_number` varchar(150) default NULL COMMENT 'numero de asiento en formato texto',
  `taken` tinyint(1) default 0 COMMENT 'si true, el asiento esta tomado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'si seat_numbers es true en eventos, se genera un pool de nros de asientos, se pueden generar varios pools, por ejemplo AA-100 hasta AA-200, AB-100 hasta AB -100';


DROP TABLE `bis_events_seats_pool`;
CREATE TABLE `bis_events_seats_pool` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `creation_date` TIMESTAMP NOT NULL,
  `name` varchar(150) default NULL COMMENT 'Nombre del pool de asientos',
  `seats_quantity` integer default 0 COMMENT 'Cantidad del pool de asientos',
  `pattern` varchar(150) default NULL COMMENT 'patron usado para generar el pool de asientos ejemplos A-###, AA#### C-#####',
  `private_pool` tinyint(1) default 0 COMMENT 'si es private, asientos que no se pueden asignar, publico los asientos se puede asignar a los clientes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Pool de asientos, permite generar los asientos en distintos pools para poder rastrearlos, eliminar por bloques, etc';

CREATE TABLE `bis_events_extras` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `extra` varchar(100) NOT NULL COMMENT 'texto de no mas de 100 caracteres',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'informacion extra para mostrar en el ticket';

DROP TABLE `bis_events_info`;
CREATE TABLE `bis_events_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT 'nombre para mostrar en el formulario, texto de no mas de 100 caracteres',
  `type` varchar(100) NOT NULL COMMENT 'tipo, puede ser string,list,int boolean, etc',
  `enum_value` varchar(250) DEFAULT NULL COMMENT 'si es tipo lista, una lista separadas por comas',
  `default_value` varchar(250) DEFAULT NULL COMMENT 'valor por defecto si lo lleva',
  `mandatory` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'si el campo es obligatorio',
  `pattern` varchar(255) DEFAULT NULL COMMENT 'patron del campo si lo lleva ej:  ##:##, email, etc',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'informacion que debe llenar el usuario al momento de registrarse';


DROP TABLE `bis_events_products`;
CREATE TABLE `bis_events_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `short_name` varchar(100) NOT NULL COMMENT 'Nombre interno del producto, ej: PEREG10, combo 10 tickets, Super Combo Primavera!',
  `name` varchar(100) NOT NULL COMMENT 'Nombre del producto, ej: ticket, combo 10 tickets, Super Combo Primavera!',
  `price` double(24,6) DEFAULT 0 COMMENT 'Precio de este producto',
  `country` varchar(100) NOT NULL COMMENT 'Pais al cual esta destinado este producto',
  `quantity` integer DEFAULT 0 COMMENT 'cantidad de tickets de este producto',
  `member_discount` tinyint(1) DEFAULT 0 COMMENT 'Si tiene descuento para pagos por comisiones',
  `discount_price` double(24,6) DEFAULT NULL COMMENT 'Precio descuento',
  `status` varchar(50) DEFAULT NULL COMMENT 'Estado del producto, pendiente, publico, privado',  
  `seats_available` tinyint(1) DEFAULT 0 COMMENT 'Si permite seleccionar asiento, platea, campo, etc',
  `store_item_id` bigint(20) DEFAULT 0 COMMENT 'Si permite seleccionar asiento, platea, campo, etc',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Permite crear mas productos para este evento en particular';


--Registros de las compras de productos, desde el backoffice o desde open 
--de este, se puede generar una orden y se sigue el proceso de pago de ordenes
--comprobar si es mas facil crear una orden y al final, generar esta o solo usar
--las ordenes, puede ser util para estadisticas
--preguntar si hace falta facturar la venta de tickets
CREATE TABLE `bis_events_products_purchase` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `event_product_id` bigint(20) NOT NULL,
  `member_id` bigint(20) DEFAULT NULL COMMENT 'Id del afiliado, si lo existe',
  `first_name` varchar(200) NOT NULL COMMENT 'Nombre del comprador',
  `last_name` varchar(200) NOT NULL COMMENT 'Apellido del comprador',
  `email` varchar(200) NOT NULL COMMENT 'Email del comprador',
  `dni` varchar(200) NOT NULL COMMENT 'DNI del comprador',
  `address` varchar(500) NOT NULL COMMENT 'Domicilio del comprador',
  `city` varchar(150) NOT NULL COMMENT 'Ciudad del comprador',
  `state` varchar(150) NOT NULL COMMENT 'Estado del comprador',
  `zip` varchar(100) NOT NULL COMMENT 'Código postal del comprador',
  `country` varchar(150) NOT NULL COMMENT 'País del comprador',
  `creation_date` TIMESTAMP NOT NULL COMMENT 'Fecha de compra',
  `payment_date` DATETIME DEFAULT NULL COMMENT 'Fecha de pago',
  `processor` VARCHAR(150) NOT NULL COMMENT 'Nombre de la pasarela utilizada',
  `amount_paid` decimal(24,6) DEFAULT NULL COMMENT 'Monto pagado',
  `status` varchar(50) NOT NULL COMMENT 'Estado de la compra, pendiente, completa, rechazada, etc',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'Id de la orden asignada',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Registros de las compras de productos';



CREATE TABLE `bis_events_tickets_assignation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_id` bigint(20) NOT NULL COMMENT 'Id de la orden asociada a esta compra' ,
  `ticket_id` bigint(20) DEFAULT NULL COMMENT 'Id del ticket asignado a esta compra',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Permite relacionar las compras con los tickets, no hay tickets flotantes';


DROP TABLE `bis_events_tickets`;
CREATE TABLE `bis_events_tickets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `creation_date` TIMESTAMP NOT NULL COMMENT 'Fecha de creacion de ticket',
  `reclaim_date` DATETIME DEFAULT NULL COMMENT 'Fecha de reclamacion de ticket',
  `release_date` DATETIME DEFAULT NULL COMMENT 'Fecha de liberacion de ticket, si estaba reclamado y esa persona no puede ir',
  `attendance_date` DATETIME DEFAULT NULL COMMENT 'Fecha y hora de asistencia al evento',
  `ticket_number` varchar(200) NOT NULL COMMENT 'Número simbolico del ticket',
  `seat_number` varchar(200) DEFAULT NULL COMMENT 'Número de asiento si lo hubiere, relacionado con la tabla seats_numbers',
  `seat_number_id` bigint(20) DEFAULT NULL COMMENT 'id del Número de asiento',
  `invite_code` varchar(250) NOT NULL COMMENT 'Hash utilizado para los enlaces de invitacion',
  `ticket_code` varchar(250) NOT NULL COMMENT 'Hash utilizado para manejo del ticket, marcar asistencia, imprimir y demas',
  `first_name` varchar(200) DEFAULT NULL COMMENT 'Nombre del asistente',
  `last_name` varchar(200) DEFAULT NULL COMMENT 'Apellido del asistente',
  `email` varchar(300) DEFAULT NULL COMMENT 'EMAIL del asistente',
  `address` varchar(500) DEFAULT NULL COMMENT 'Domicilio del asistente',
  `city` varchar(150) DEFAULT NULL COMMENT 'Ciudad del asistente',
  `state` varchar(150) DEFAULT NULL COMMENT 'Estado del asistente',
  `zip` varchar(100) DEFAULT NULL COMMENT 'Código postal del asistente',
  `country` varchar(150) DEFAULT NULL COMMENT 'País del asistente',
  `status` varchar(50) NOT NULL COMMENT 'Estado del ticket, creado, reclamado, released, completed',
  `status_message` varchar(150) DEFAULT NULL COMMENT 'Mensaje de Estado del ticket, visible para el usuario',
  `status_audit` text DEFAULT NULL COMMENT 'Registro de todos los movimientos de este ticket',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'el ticket propiamente dicho, cuando algien manda la invitacion se crea, luego se llenan los datos cuando alguien lo reclama, se usa para generar el ticket de verdad';

CREATE TABLE `bis_events_tickets_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ticket_id` bigint(20) NOT NULL COMMENT 'Id del ticket relacionado',
  `event_info_id` bigint(20) NOT NULL COMMENT 'ID de la info extra relacionada',
  `info_name` varchar(200) DEFAULT NULL COMMENT 'Nombre de la info relacionada',
  `info_Value` text DEFAULT NULL COMMENT 'Valor ingresado por el usuario',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Informacion de formulario relacionada al ticket, se crea con los datos de bis_event_info';

CREATE TABLE `bis_events_hosts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL COMMENT 'Nombre del host',
  `email` varchar(250) NOT NULL COMMENT 'email del host, sirve para el login',
  `password` varchar(250) NOT NULL COMMENT 'contraseña para la app',
  `admin` tinyint(1) DEFAULT 0 COMMENT 'Si es admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Informacion de los hosts del evento';

CREATE TABLE `bis_events_hosts_event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `host_id` bigint(20) NOT NULL,
  `event_id` bigint(20) NOT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Relacion de los hosts con respecto a un evento';

alter table bis_events add column sold_out tinyint(1) not null default 0 comment 'if event has been sold out';
