alter table bis_members add column `contract_signed` tinyint(1) not null default 0;
alter table bis_members add column `contract_signed_date` DATETIME DEFAULT NULL;
alter table bis_members add column `birth_date` DATE DEFAULT NULL;
alter table bis_members add column `company_dni` varchar(50) DEFAULT NULL;
alter table bis_members add column `company_dni_type` varchar(50) DEFAULT NULL;
alter table bis_members add column `contract_url` varchar(250) DEFAULT NULL;
alter table bis_members add column `is_company` tinyint(1) NOT NULL DEFAULT 0;

update bis_members set is_company = 1 where company_name is not null and LENGTH(company_name) > 1;
update bis_members set dni_type = null where dni_type = "0";
update bis_members set company_dni_type = null where company_dni_type = "0";


