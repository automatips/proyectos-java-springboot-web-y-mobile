DROP TABLE bis_corpo_content;
CREATE TABLE `bis_corpo_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_date` TIMESTAMP NOT NULL,
  `title` varchar(50) default NULL,
  `description` text default null,
  `published` tinyint(1) default 0,
  `path_file` varchar(250) DEFAULT NULL,  
  `path_thumb` varchar(250) DEFAULT NULL,  
  `hash_file` varchar(250) DEFAULT NULL,  
  `hash_thumb` varchar(250) DEFAULT NULL,  
  `rank_available` varchar(250) DEFAULT NULL,  
  `country_available` varchar(250) DEFAULT NULL,  
  `mime_type` varchar(150) DEFAULT NULL,
  `downloaded` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;