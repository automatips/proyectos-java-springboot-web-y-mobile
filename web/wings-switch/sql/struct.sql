DROP TABLE IF EXISTS config;
CREATE TABLE `config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prop_name` varchar(255) default NULL,
  `prop_value` varchar(255) default null,
  `prop_type` varchar(50) default null,
  `prop_enum` varchar(255) default null,
  `prop_default` varchar(255) default null,
  `prop_description` text default null,  
  `prop_category` varchar(150) default null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

truncate table wings_switch.config;
INSERT INTO wings_switch.config (prop_name, prop_value, prop_type, prop_enum, prop_default, prop_description, prop_category) 
	VALUES 
        ('sip.codecString', 'OPUS,PCMU,PCMA,G729', 'string', NULL, 'OPUS,PCMU,PCMA,G729', 'Codecs used by phone, order in string states priority, if codec is missing, codec is disabled', 'provisioning'),
	('sip.dnsServers', '8.8.8.8,8.8.4.4', 'string', NULL, '8.8.8.8,8.8.4.4', 'Dns servers used by the library to resolve names', 'provisioning'),
	('sip.sipServer', 'sip.wingsmobile.net', 'string', NULL, 'sip.wingsmobile.net', 'Freeswitch Domain', 'provisioning'),
	('sip.sipStunServer', 'stun.wingsmobile.net', 'string', NULL, 'stun.wingsmobile.net', 'Stun server to use', 'provisioning'),
	('sip.registrationExpire', '60', 'int', NULL, '60', 'Registration expiration in secods', 'provisioning'),
	('sip.enableAdaptativeRateControl', 'true', 'boolean', 'true,false', 'true', 'Enable adaptative rate control', 'provisioning'),
	('sip.enableAudioAdaptiveJittcomp', 'true', 'boolean', 'true,false', 'true', 'Enable or disable the audio adaptive jitter compensation.', 'provisioning'),
	('sip.adaptativeRateControlAlgo', 'advanced', 'string', 'advanced,basic', 'advanced', 'Algorithm for adaptative rate control', 'provisioning'),
	('sip.audioJitterComp', '60', 'int', null, '60', 'Nominal audio jitter buffer size in milliseconds', 'provisioning'),
	('sip.videoJitterComp', '60', 'int', null, '60', 'Nominal video jitter buffer size in milliseconds', 'provisioning'),
	('sip.audioPortRange', '6000,8000', 'string', '', '6000,8000', 'UDP port range from which to randomly select the port used for audio streaming', 'provisioning'),
	('sip.enableEchoCancelation', 'true', 'boolean', 'true,false', 'true', 'Enable echo cancelation', 'provisioning'),
	('sip.enableEchoLimiter', 'true', 'boolean', 'true,false', 'true', 'Enable echo limiter', 'provisioning'),
	('sip.playbackGainDb', '0.0', 'float', null, '0.0', 'Gain used for playback', 'provisioning'),
	('sip.mainGain', '0.0', 'float', null, '0.0', 'Main gain', 'provisioning'),
	('sip.udpPort', '0', 'int', NULL, '0', 'UDP port for transport, 0 is transport disabled', 'provisioning'),
	('sip.tcpPort', '0', 'int', NULL, '0', 'TCP port for transport, 0 is transport disabled', 'provisioning'),
	('sip.tlsPort', '5061', 'int', NULL, '5061', 'TLS port for transport, 0 is transport disabled', 'provisioning'),
	('sip.enableStun', 'true', 'boolean', 'true,false', 'true', 'Enable Stun use', 'provisioning'),
	('sip.enableIce', 'false', 'boolean', 'true,false', 'false', 'Enable ICE', 'provisioning'),
	('sip.enableTurn', 'true', 'boolean', 'true,false', 'true', 'Enable Turn', 'provisioning'),
	('sip.enableUpnp', 'false', 'boolean', 'true,false', 'false', 'Enable Upnp', 'provisioning'),
	('config.enableLogCollection', 'false', 'boolean', 'true,false', 'false', 'Enable SIP library log collection', 'provisioning'),
	('config.logCollectionUploadServer', 'http://switch.wingsmobile.net/api/logs/upload', 'string', null, 'http://switch.wingsmobile.net/api/logs/upload', 'Server url to upload log collection if enabled', 'provisioning');

CREATE TABLE `clients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) default NULL,
  `token` varchar(255) default null,
  `creation_date` TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table clients drop column `balance`;
alter table clients add column `balance` decimal(24,5) not null default 0;
alter table clients add column `country_iso` varchar(5);
alter table clients add column `country_prefix` varchar(10);
alter table clients add column `locale` varchar(50);



DROP TABLE `client_devices`;
CREATE TABLE `client_devices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,  
  `creation_date` TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,  
  `device_serial_number` varchar(150),
  `device_name` varchar(150),
  `device_model` varchar(150),
  `device_id` varchar(200),
  `device_udid` varchar(200),
  `imei1` varchar(200),
  `imei2` varchar(200),
  `push_token` varchar(250),
  `push_instance_id` varchar(250),
  `device_os` varchar(250),
  `app_version` varchar(250),
  `active` tinyint(1) not null default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `cdr`;
CREATE TABLE `cdr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(200) NOT NULL,
  `creation_date` DATETIME,  
  `origination` varchar(150),
  `origination_caller_id` varchar(150),
  `origination_ip` varchar(150),
  `destination` varchar(150),
  `destination_ip` varchar(150),
  `domain_name` varchar(255),
  `bridge_hangup_cause` varchar(150),
  `hangup_cause` varchar(150),
  `duration` INTEGER,
  `billsec` INTEGER,
  `progresssec` INTEGER,
  `answersec` INTEGER,
  `waitsec` INTEGER,
  `secure` tinyint(1) not null default 0,
  `wings_plan` varchar(50),
  `wings_internal` tinyint(1) not null default 0,
  `user_agent` varchar(250),
  `nat_detected` tinyint(1) not null default 0,
  `cost` decimal(24,5),
  `real_cost` decimal(24,5),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `client_contacts`;
CREATE TABLE `client_contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,
  `display_name` varchar(255),
  `number` varchar(200),
  `raw` varchar(200),
  `formatted` varchar(200),
  `label` varchar(200),
  `free` tinyint(1) not null default 0,
  `secure` tinyint(1) not null default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `services`;
CREATE TABLE `services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255),
  `service_code` varchar(255),
  `description` text,
  `priority` integer,
  `service_type` varchar(100),
  `renovation` varchar(100),
  `asignation` varchar(100),
  `cost` decimal(24,5),
  `bonus_minutes` integer,
  `bonus_money` decimal(24,5),
  `price_list_id` bigint(20),
  `duration` integer,
  `valid_from` DATETIME,
  `valid_to` DATETIME,
  `valid_countries` text,
  `active` tinyint(1) not null default 0,
  `sort_order` int not null default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `services_localization`;
CREATE TABLE `services_localization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20),
  `display_name` varchar(255),
  `description` text,
  `image_url` varchar(250),
  `loc_country` varchar(20),
  `loc_language` varchar(20),
  `locale` varchar(20),
  `currency` varchar(20),
  `cost` decimal(24,5),
  `localized_cost` decimal(24,5),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `localization_countries`;
CREATE TABLE `localization_countries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(20),
  `currency` varchar(20),
  `exchange_rate` decimal(24,5),
  `default_locale` varchar(40),
  `other_locale` varchar(400),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `price_list`;
CREATE TABLE `price_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `description` text,
  `valid_from` DATETIME,
  `valid_to` DATETIME,
  `active` tinyint(1) not null default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `price_list_destination`;
CREATE TABLE `price_list_destination` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `price_list_id` bigint(20),
  `name` varchar(255),
  `country_code` varchar(10),
  `area_code` integer,
  `price` decimal(24,5),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `client_services`;
CREATE TABLE `client_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20),
  `client_id` bigint(20),
  `creation_date` DATETIME,
  `expiration_date` DATETIME,
  `renewal_date` DATETIME,
  `remaining_minutes` integer,
  `remaining_money` decimal(24,5),
  `active` tinyint(1) not null default 0,
  `did_number` varchar(30),
  `status` varchar(30),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `client_balances`;
CREATE TABLE `client_balances` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20),
  `creation_date` DATETIME,
  `amount` decimal(24,5),
  `balance_type` varchar(50),
  `description` varchar(250),
  `comment` varchar(150),
  `reference_id` bigint(20),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `client_pending_actions`;
create table client_pending_actions(
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `client_id` bigint(20),
    `login` varchar(255),
    `send_count` integer,
    `completed` tinyint(1) not null default 0,
    `creation_date` DATETIME,
    `completion_date` DATETIME,
    `action` varchar(100),
    `push_token` varchar(250),
    `failure_count` int,
    `disabled` tinyint(1) not null default 0,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `client_dids`;
CREATE TABLE `client_dids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20),
  `client_service_id` bigint(20),
  `did_number` varchar(50),
  `country` varchar(40),
  `city` varchar(40),
  `creation_date` DATETIME,
  `expiration_date` DATETIME,
  `local_status` varchar(50),
  `remote_status` varchar(50),
  `remote_id` varchar(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE `did_countries`;
CREATE TABLE `did_countries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_id` bigint(20),
  `remote_id` varchar(150),
  `name` varchar(150),
  `prefix` varchar(50),
  `iso` varchar(40),
  `enabled` tinyint(1) not null default 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE `did_cities`;
CREATE TABLE `did_cities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` bigint(20),
  `remote_id` varchar(150),
  `name` varchar(200),
  `enabled` tinyint(1) not null default 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
