--info de las cuentas de sip
select * from clientsshared;
--recargas de credito
select * from clientsshared_credit;
--info del credito del cliente
select * from accounts;
--info de los planes, wings y secure
select * from plans;
--dids de los clientes
select * from portal_clientdids;

--check foreing keys
SELECT 
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_SCHEMA = 'vsm_voipswitch' AND
  REFERENCED_TABLE_NAME = 'clientsshared' AND
  REFERENCED_COLUMN_NAME = 'id_client';

--query para saber que planes tiene el usuario
SELECT 
    `em_aps_schedule`.`id` as Id ,
    `em_aps_schedule`.`id_client` as IdClient ,
    `em_aps_schedule`.`client_type` as ClientType ,
    `clienttypes`.`client_type_name` as ClientTypeName ,
    (CASE `em_aps_schedule`.`client_type` 
        WHEN 0 THEN IFNULL(`clientsip`.`login`,'N/A') 
        WHEN 4 THEN IFNULL(`clientscallback`.`login`,'N/A') 
        WHEN 8 THEN IFNULL(`clientspin`.`login`,'N/A') 
        WHEN 16 THEN IFNULL(`clientscallshop`.`login`,'N/A') 
        WHEN 32 THEN IFNULL(`clientsretail`.`login`,'N/A') 
        WHEN 128 THEN IFNULL(`clientspbx`.`login`,'N/A') 
        ELSE 'N/A' END) 
    as ClientLogin ,
    CAST((CASE `em_aps_schedule`.`client_type` 
        WHEN 0 THEN IFNULL(`clientsip`.`account_state`,-(1)) 
        WHEN 4 THEN IFNULL(`clientscallback`.`account_state`,-(1)) 
        WHEN 8 THEN IFNULL(`clientspin`.`account_state`,-(1)) 
        WHEN 16 THEN IFNULL(`clientscallshop`.`account_state`,-(1)) 
        WHEN 32 THEN IFNULL(`clientsretail`.`account_state`,-(1)) 
        WHEN 128 THEN IFNULL(`clientspbx`.`account_state`,-(1)) 
        ELSE 0 END) 
        AS DECIMAL(12,4))  
    as ClientAccountState ,
    `em_aps_schedule`.`next_date` as DateOfPayment ,
    IFNULL((SELECT MAX(`em_aps_history`.`created_at`) FROM em_aps_history WHERE (`em_aps_history`.`id_aps_schedule` = `em_aps_schedule`.`id`)),
    `em_aps_schedule`.`next_date`) as DateOfLastUpdate ,
    `em_aps_schedule`.`start_date` as StartDate , 
    `em_aps_schedule`.`end_date` as EndDate , 
    `em_aps_schedule`.`aps_type` as PaymentType ,
     IF(`em_aps_schedule`.`aps_type` = 5, 'Plan payment', `em_aps_types`.`name`) as PaymentTypeName ,
     (CASE `em_aps_schedule`.`aps_type` 
        WHEN 1 THEN IFNULL(`plans_packs`.`period_cost`, -( 1 )) 
        WHEN 2 THEN IFNULL(`em_aps_schedule`.`payment_amount`, -( 1 )) 
        WHEN 5 THEN IFNULL(`plans`.`period_cost`, -( 1 )) 
        WHEN 6 THEN CAST(IFNULL(ExtractValue(`paypalrequest`.`ShopData`,'/ShopData/SubscriptionPeriodAmount'), -( 1 )) AS DECIMAL(12,4)) 
        ELSE `em_aps_types`.`amount` END) 
    as PaymentAmount , 
    (CASE 
        WHEN (`em_aps_schedule`.`aps_type` = 1) THEN CONVERT(IFNULL(`plans_packs`.`name`,'N/A') USING utf8) 
        WHEN (`em_aps_schedule`.`aps_type` = 2) THEN IFNULL(`em_aps_schedule`.`aps_data`,'N/A') 
        WHEN (`em_aps_schedule`.`aps_type` = 5) THEN IFNULL(`plans`.`name`,'N/A') 
        WHEN (`em_aps_schedule`.`aps_type` = 6) THEN IFNULL(`em_aps_types`.`description`,'N/A') 
        WHEN (`em_aps_schedule`.`aps_type` > 100) THEN CONVERT(IFNULL(`em_aps_types`.`description`,'N/A') USING utf8) 
        ELSE 'N/A' END) 
    AS PaymentName ,
    `em_aps_schedule`.`aps_status` as PaymentStatus ,
    `em_aps_schedule`.`aps_data` as PaymentData ,
    `em_aps_schedule`.`transaction_id` as TransactionId 
FROM em_aps_schedule 
    JOIN clienttypes ON `clienttypes`.`id_client_type` = `em_aps_schedule`.`client_type` 
    LEFT OUTER JOIN em_aps_types ON `em_aps_types`.`id` = `em_aps_schedule`.`aps_type` 
    LEFT OUTER JOIN plans_packs ON `plans_packs`.`id_plan_pack` = `em_aps_schedule`.`aps_data` AND `em_aps_schedule`.`aps_type` = 1 
    LEFT OUTER JOIN plans ON `plans`.`id_plan` = `em_aps_schedule`.`aps_data` AND `em_aps_schedule`.`aps_type` = 5 
    LEFT OUTER JOIN clientsip ON `clientsip`.`id_client` = `em_aps_schedule`.`id_client` AND `em_aps_schedule`.`client_type` = 0 
    LEFT OUTER JOIN clientscallback ON `clientscallback`.`id_client` = `em_aps_schedule`.`id_client` AND `em_aps_schedule`.`client_type` = 4 
    LEFT OUTER JOIN clientspin ON `clientspin`.`id_client` = `em_aps_schedule`.`id_client` AND `em_aps_schedule`.`client_type` = 8 
    LEFT OUTER JOIN clientscallshop ON `clientscallshop`.`id_client` = `em_aps_schedule`.`id_client` AND `em_aps_schedule`.`client_type` = 16 
    LEFT OUTER JOIN clientsretail ON `clientsretail`.`id_client` = `em_aps_schedule`.`id_client` AND `em_aps_schedule`.`client_type` = 32 
    LEFT OUTER JOIN clientspbx ON `clientspbx`.`id_company` = `em_aps_schedule`.`id_client` AND `em_aps_schedule`.`client_type` = 128 
    LEFT OUTER JOIN paypalrequest ON `em_aps_schedule`.`transaction_id` = `paypalrequest`.`TransactionID` 
WHERE 
    `em_aps_schedule`.`client_type` = 32 AND 
    `em_aps_schedule`.`id_client` = 172 AND 
    IFNULL((SELECT MAX(`em_aps_history`.`created_at`) FROM em_aps_history WHERE (`em_aps_history`.`id_aps_schedule` = `em_aps_schedule`.`id`)),`em_aps_schedule`.`next_date`) BETWEEN '0001-01-01 00:00:00' AND '9999-12-31 23:59:59';


select * from em_aps_history;
select * from em_aps_types;