alter table services add column default_image varchar(250) default null;
update services s set default_image = (select image_url from services_localization where service_id = s.id limit 1);
UPDATE wings_switch.services SET `valid_countries` = 'CO,ES,PE,EC,AR';
alter table clients modify balance decimal(24,6) not null default 0;
alter table client_balances modify amount decimal(24,6) not null default 0;
alter table roaming_free_operators add column enabled tinyint(1) default 0;
