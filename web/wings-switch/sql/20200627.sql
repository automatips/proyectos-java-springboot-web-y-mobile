alter table wings_switch.cdr add column destination_id bigint(20);
alter table wings_switch.cdr add column service_id bigint(20);
alter table wings_switch.cdr add column client_service_id bigint(20);
alter table wings_switch.cdr add column minutes integer default 0;