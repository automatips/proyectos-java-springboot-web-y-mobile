/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.services.ApiAuthService;
import com.wings.voip.services.ClientService;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class VerifyController {

    private final Logger log = LoggerFactory.getLogger(VerifyController.class);

    @Autowired
    private ClientService clientService;
    @Autowired
    private ApiAuthService apiAuthService;

    @RequestMapping(value = "/api/validate", method = RequestMethod.POST)
    public ResponseEntity<String> validateNumber(HttpServletRequest request, @RequestBody Map<String, String> body) {

        try {
            String auth = request.getHeader("auth");
            if (!apiAuthService.isTokenValid(auth)) {
                return ResponseEntity.badRequest().body("unauth");
            }

            String number = body.get("login");
            if (number == null || number.trim().isEmpty()) {
                return ResponseEntity.badRequest().body("error");
            }
            number = number.replaceAll("\\+", "").trim();

            Client client = clientService.getOne(number);
            if (client != null) {
                return ResponseEntity.ok("success");
            } else {
                return ResponseEntity.ok("error");
            }
        } catch (Exception e) {
            log.error(" Error on validate number ", e);
            return ResponseEntity.ok("error");
        }
    }

}
