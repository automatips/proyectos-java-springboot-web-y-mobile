/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "roaming_free_operators")
public class RoamingFreeOperator implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 150)
    @Column(name = "operator_name")
    private String operatorName;
    @Size(max = 20)
    @Column(name = "mcc")
    private String mcc;
    @Size(max = 20)
    @Column(name = "mnc")
    private String mnc;
    @Size(max = 20)
    @Column(name = "country")
    private String country;
    @Size(max = 60)
    @Column(name = "dial_string_enable")
    private String dialStringEnable;
    @Size(max = 60)
    @Column(name = "dial_string_disable")
    private String dialStringDisable;
    @Column(name = "enabled")
    private boolean enabled;

    public RoamingFreeOperator() {
    }

    public RoamingFreeOperator(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDialStringEnable() {
        return dialStringEnable;
    }

    public void setDialStringEnable(String dialStringEnable) {
        this.dialStringEnable = dialStringEnable;
    }

    public String getDialStringDisable() {
        return dialStringDisable;
    }

    public void setDialStringDisable(String dialStringDisable) {
        this.dialStringDisable = dialStringDisable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoamingFreeOperator)) {
            return false;
        }
        RoamingFreeOperator other = (RoamingFreeOperator) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.voip.domain.sw.entities.RoamingFreeOperator[ id=" + id + " ]";
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
