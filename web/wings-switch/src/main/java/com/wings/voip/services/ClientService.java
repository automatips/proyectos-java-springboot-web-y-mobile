/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientBalance;
import com.wings.voip.domain.sw.entities.ClientContact;
import com.wings.voip.domain.sw.enums.ServiceTypes;
import com.wings.voip.domain.sw.repos.ClientBalanceRepository;
import com.wings.voip.domain.sw.repos.ClientContactRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientServiceRepository;
import com.wings.voip.domain.sw.specs.ClientServicesSpecs;
import com.wings.voip.domain.sw.specs.ClientsSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author seba
 */
@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientServiceRepository clientServiceRepository;

    @Autowired
    private ClientBalanceRepository clientBalanceRepository;

    @Autowired
    private ClientContactRepository clientContactRepository;

    /**
     * getOne client by phone number
     *
     * @param login
     * @return
     */
    public Client getOne(String login) {
        return clientRepository.findByLogin(login);
    }

    @Transactional
    public List<ClientContact> findByClientId(Long id) {
        return clientContactRepository.findByClientId(id);
    }

    @Transactional
    public Client findOneClient(Long id) {
        return clientRepository.findOne(id);
    }

    /**
     * Given login, calculate how many minutes the user can talk according to
     * price
     *
     * @param login
     * @param price
     * @return
     */
    public int calculateTimeout(String login, double price) {
        Client client = getOne(login);
        double credit = client.getBalance();
        int duration = (int) (credit / price);
        return duration * 60;
    }

    public void getClientPlans(String login) {

        Client client = getOne(login);
        Sort sort = new Sort(Sort.Direction.DESC, "priority");
        List<com.wings.voip.domain.sw.entities.ClientService> clientServices
                = clientServiceRepository.findAll(ClientServicesSpecs.searchServiceByType(client.getId(), ServiceTypes.external.name()), sort);

        if (!clientServices.isEmpty()) {
            //user firts, it's ordered by priority
            com.wings.voip.domain.sw.entities.ClientService chooseService = clientServices.get(0);
            chooseService.getService().getPriceListId();
        }
    }

    public Page<Client> findAll(PageRequest pr) {
        return clientRepository.findAll(pr);
    }

    public Page<Client> findAll(Pageable pgbl, String login, Date dateFrom, Date dateTo) {
        return clientRepository.findAll(ClientsSpecs.searchByLoginAndDates(login, dateFrom, dateTo), pgbl);
    }

    public Client findOne(String login) {
        return clientRepository.findByLogin(login);
    }

    @Transactional
    public void getAddCredit(Long id, String amount, HttpServletRequest request) {
        try {
            Client client = clientRepository.findOne(id);
            Double balanceBefore = client.getBalance();
            ClientBalance clientBalance =clientBalanceRepository.findByClientId(id);
            if(clientBalance == null) {
                clientBalance = new ClientBalance();
            }
            clientBalanceSetting(amount, client, balanceBefore, clientBalance);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clientBalanceSetting(String amount, Client client, Double balanceBefore, ClientBalance clientBalance) {
        clientBalance.setClientId(client.getId());
        clientBalance.setAmount(Double.valueOf(amount));
        clientBalanceRepository.save(clientBalance);
        client.setBalance(balanceBefore + clientBalance.getAmount());
        clientRepository.save(client);
    }
}
