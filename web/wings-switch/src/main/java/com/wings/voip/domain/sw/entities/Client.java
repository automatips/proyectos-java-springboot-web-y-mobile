/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.entities;

import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "clients")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 255)
    @Column(name = "login")
    private String login;
    @Size(max = 255)
    @Column(name = "token")
    private String token;
    @Basic(optional = false)
    @NotNull
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Column(name = "balance")
    private Double balance;
    @Size(max = 5)
    @Column(name = "country_iso")
    private String countryIso;
    @Size(max = 10)
    @Column(name = "country_prefix")
    private String countryPrefix;
    @Column(name = "locale")
    private String locale;
    @Column(name = "mcc")
    private int mcc;
    @Column(name = "mnc")
    private int mnc;
    @Column(name = "web_pwd")
    private String webPwd;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private List<ClientService> clientServices;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = true, updatable = false, insertable = false)
    private List<ClientDevice> devices = new LinkedList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = true, updatable = false, insertable = false)
    private List<ClientBalance> clientBalances = new LinkedList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = true, updatable = false, insertable = false)
    private List<ClientContact> clientContacts;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = true, updatable = false, insertable = false)
    private List<ClientDid> clientDids;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = true, updatable = false, insertable = false)
    private List<ClientPendingAction> clientPendingActions;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = true, updatable = false, insertable = false)
    private List<ClientRoamingFree> clientRoamingFrees;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "origination", referencedColumnName = "login")
    private List<CdrPersistent> cdrPersistents;

    public List<ClientDid> getClientDids() {
        return clientDids;
    }

    public void setClientDids(List<ClientDid> clientDids) {
        this.clientDids = clientDids;
    }

    public List<ClientContact> getClientContacts() {
        return clientContacts;
    }

    public void setClientContacts(List<ClientContact> clientContacts) {
        this.clientContacts = clientContacts;
    }

    public void setCdrPersistents(List<CdrPersistent> cdrPersistents) {
        this.cdrPersistents = cdrPersistents;
    }

    public List<ClientService> getClientServices() {
        return clientServices;
    }

    public void setClientServices(List<ClientService> clientServices) {
        this.clientServices = clientServices;
    }

    public List<ClientBalance> getClientBalances() {
        return clientBalances;
    }

    public void setClientBalances(List<ClientBalance> clientBalances) {
        this.clientBalances = clientBalances;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getCountryIso() {
        return countryIso;
    }

    public void setCountryIso(String countryIso) {
        this.countryIso = countryIso;
    }

    public String getCountryPrefix() {
        return countryPrefix;
    }

    public void setCountryPrefix(String countryPrefix) {
        this.countryPrefix = countryPrefix;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public List<ClientDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<ClientDevice> devices) {
        this.devices = devices;
    }

    public String getWebPwd() {
        return webPwd;
    }

    public void setWebPwd(String webPwd) {
        this.webPwd = webPwd;
    }

    public Locale getLocaleReal() {
        try {
            if (locale != null) {
                return new Locale(locale.split("_")[0], locale.split("_")[1]);
            } else {
                return new Locale("es", "ES");
            }
        } catch (Exception e) {
            return new Locale("es", "ES");
        }
    }

}
