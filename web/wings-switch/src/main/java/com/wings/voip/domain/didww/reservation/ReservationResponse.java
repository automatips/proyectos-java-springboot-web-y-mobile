/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww.reservation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 *
 * @author seba
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationResponse {

    private GenericData data;
    private List<GenericData> included;

    public GenericData getData() {
        return data;
    }

    public void setData(GenericData data) {
        this.data = data;
    }

    public void setIncluded(List<GenericData> included) {
        this.included = included;
    }

    public List<GenericData> getIncluded() {
        return included;
    }

}
