/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "config")
public class Property implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(max = 255)
    @Column(name = "prop_name")
    private String propName;
    @Size(max = 255)
    @Column(name = "prop_value")
    private String propValue;
    @Size(max = 50)
    @Column(name = "prop_type")
    private String propType;
    @Size(max = 255)
    @Column(name = "prop_enum")
    private String propEnum;
    @Size(max = 255)
    @Column(name = "prop_default")
    private String propDefault;
    @Lob
    @Size(max = 65535)
    @Column(name = "prop_description")
    private String propDescription;
    @Size(max = 150)
    @Column(name = "prop_category")
    private String propCategory;

    public Property() {
    }

    public Property(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public String getPropType() {
        return propType;
    }

    public void setPropType(String propType) {
        this.propType = propType;
    }

    public String getPropEnum() {
        return propEnum;
    }

    public void setPropEnum(String propEnum) {
        this.propEnum = propEnum;
    }

    public String getPropDefault() {
        return propDefault;
    }

    public void setPropDefault(String propDefault) {
        this.propDefault = propDefault;
    }

    public String getPropDescription() {
        return propDescription;
    }

    public void setPropDescription(String propDescription) {
        this.propDescription = propDescription;
    }

    public String getPropCategory() {
        return propCategory;
    }

    public void setPropCategory(String propCategory) {
        this.propCategory = propCategory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Property)) {
            return false;
        }
        Property other = (Property) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.voip.domain.sw.entities.Property[ id=" + id + " ]";
    }

}
