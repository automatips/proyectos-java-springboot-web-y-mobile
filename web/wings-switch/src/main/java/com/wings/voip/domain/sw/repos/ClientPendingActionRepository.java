/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.ClientPendingAction;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ClientPendingActionRepository extends JpaRepository<ClientPendingAction, Long>, JpaSpecificationExecutor<ClientPendingAction> {

    List<ClientPendingAction> findByCompletedAndDisabled(Boolean completed,Boolean disabled);

    List<ClientPendingAction> findByLoginAndCompletedAndAction(String login, Boolean completed, String action);

}
