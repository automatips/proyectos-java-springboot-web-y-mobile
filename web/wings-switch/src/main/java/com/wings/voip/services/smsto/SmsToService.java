/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.smsto;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class SmsToService {

    @Autowired
    private Environment environment;

    private final Logger log = LoggerFactory.getLogger(SmsToService.class);
    private final String clientId = "";
    private final String secret = "";
    private final String authUrl = "https://auth.sms.to/oauth/token";
    private final String sendUrl = "https://api.sms.to/sms/send";
    private AuthHolder authHolder;

    /**
     *
     * @return
     */
    private String getAuthToken() {

        String token = null;
        try {
            boolean dev = false;
            String[] profiles = environment.getActiveProfiles();
            for (String profile : profiles) {
                if (profile.contains("dev")) {
                    dev = true;
                    break;
                }
            }

            Map<String, String> map = new HashMap<>();
            map.put("client_id", clientId);
            map.put("secret", secret);

            RequestEntity<Map<String, String>> request = RequestEntity.post(new URI(authUrl))
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .body(map);

            RestTemplate template = new RestTemplate();
            ResponseEntity<AuthResponse> response = template.exchange(request, AuthResponse.class);
            authHolder = new AuthHolder(response.getBody());

            token = authHolder.getToken();
        } catch (Exception e) {
            log.error("Error sending SMS", e);
        }
        return token;
    }

    /**
     *
     * @param to
     * @param message
     */
    public void sendSms(String to, String message) {

        String authToken;
        if (authHolder.isExpired()) {
            authToken = getAuthToken();
        } else {
            authToken = authHolder.getToken();
        }

        try {

            boolean dev = false;
            String[] profiles = environment.getActiveProfiles();
            for (String profile : profiles) {
                if (profile.contains("dev")) {
                    dev = true;
                    break;
                }
            }

            Map<String, String> map = new HashMap<>();
            map.put("message", message);
            map.put("to", to);
            map.put("sender_id", "wings-switch");
            map.put("callback_url", null);

            RequestEntity<Map<String, String>> request = RequestEntity.post(new URI(sendUrl))
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Bearer " + authToken)
                    .body(map);

            RestTemplate template = new RestTemplate();
            ResponseEntity<SmsSendResponse> response = template.exchange(request, SmsSendResponse.class);
            response.getBody();

            //TODO: hacer algo con la respuesta
        } catch (Exception e) {
            log.error("Error sending SMS", e);
        }
    }
}
