/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww.reservation;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author seba
 */
public class DidTrunkUptadeData {

    private String id;
    private String type;

    private Map<String, Map<String, Map<String, String>>> relationships;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setRelationships(Map<String, Map<String, Map<String, String>>> relationships) {
        this.relationships = relationships;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Map<String, Map<String, String>>> getRelationships() {
        return relationships;
    }

    public void setTrunk(String trunkId) {
        Map<String, String> data = new HashMap<>();
        data.put("type", "trunks");
        data.put("id", trunkId);
        Map<String, Map<String, String>> trunk = new HashMap<>();
        trunk.put("data", data);
        relationships = new HashMap<>();
        relationships.put("trunk", trunk);
    }
}
