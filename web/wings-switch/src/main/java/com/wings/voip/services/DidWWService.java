/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.didww.DidBuyResponse;
import com.wings.voip.domain.didww.DidCreateOrder;
import com.wings.voip.domain.didww.DidData;
import com.wings.voip.domain.didww.DidDoubleData;
import com.wings.voip.domain.didww.DidOrderData;
import com.wings.voip.domain.didww.DidOrderDataAttributes;
import com.wings.voip.domain.didww.DidOrderDataAttributesItem;
import com.wings.voip.domain.didww.DidReservation;
import com.wings.voip.domain.didww.DidReservationData;
import com.wings.voip.domain.didww.DidResponse;
import com.wings.voip.domain.didww.DidSpecs;
import com.wings.voip.domain.didww.reservation.DidListResponse;
import com.wings.voip.domain.didww.reservation.DidOrderResponse;
import com.wings.voip.domain.didww.reservation.DidTrunkUptade;
import com.wings.voip.domain.didww.reservation.DidTrunkUptadeData;
import com.wings.voip.domain.didww.reservation.GenericData;
import com.wings.voip.domain.didww.reservation.ReservationResponse;
import com.wings.voip.domain.sw.entities.ClientDid;
import com.wings.voip.domain.sw.entities.DidCity;
import com.wings.voip.domain.sw.entities.DidCountry;
import com.wings.voip.domain.sw.repos.DidCityRepository;
import com.wings.voip.domain.sw.repos.DidCountryRepository;
import com.wings.voip.model.DidNumberAvailableDTO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

/**
 *
 * @author seba
 */
@Service
public class DidWWService {

    private final Logger log = LoggerFactory.getLogger(DidWWService.class);

    private final String url = "https://api.didww.com/v3/";
    private final String apiKey = "94a058zsfcbbvevrrxedve1mo3hhw2br";
    //private final String url = "https://sandbox-api.didww.com/v3/";
    //private final String apiKey = "94a058zsfcbbvevrrxedve1mo3hhw2br";

    @Autowired
    private DidCountryRepository didCountryRepository;

    @Autowired
    private DidCityRepository didCityRepository;

    public void sync() {

        if (didCountryRepository.count() > 0) {
            return;
        }

        List<DidCountry> countries = getCountries();
        didCountryRepository.save(countries);
        for (DidCountry country : countries) {
            List<DidCity> cities = getCities(country.getId(), country.getRemoteId());
            didCityRepository.save(cities);
        }
    }

    public List<DidCountry> getCountries() {

        List<DidCountry> ret = new ArrayList<>();
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Api-Key", apiKey);
            httpHeaders.set("Content-Type", "application/vnd.api+json");
            httpHeaders.set("Accept", "application/vnd.api+json");

            HttpEntity entity = new HttpEntity<>(httpHeaders);
            ResponseEntity<DidResponse> response = restTemplate.exchange(url + "countries", HttpMethod.GET, entity, DidResponse.class, new ArrayList<>());

            DidResponse didResponse = response.getBody();
            for (DidData data : didResponse.getData()) {

                DidCountry c = new DidCountry();
                c.setEnabled(false);
                c.setIso(data.getAttributes().get("iso"));
                c.setName(data.getAttributes().get("name"));
                c.setPrefix(data.getAttributes().get("prefix"));
                c.setProviderId(1l);
                c.setRemoteId(data.getId());
                ret.add(c);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return ret;
    }

    public List<DidCity> getCities(Long countryId, String countryRemoteId) {

        List<DidCity> ret = new ArrayList<>();
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Api-Key", apiKey);
            httpHeaders.set("Content-Type", "application/vnd.api+json");
            httpHeaders.set("Accept", "application/vnd.api+json");

            Map<String, String> params = new HashMap<>();
            String uri = url + "cities?filter[country.id]=" + countryRemoteId;

            HttpEntity entity = new HttpEntity<>(httpHeaders);
            ResponseEntity<DidResponse> response = restTemplate.exchange(uri, HttpMethod.GET, entity, DidResponse.class, params);

            DidResponse didResponse = response.getBody();
            for (DidData data : didResponse.getData()) {

                DidCity c = new DidCity();
                c.setEnabled(true);
                c.setCountryId(countryId);
                c.setName(data.getAttributes().get("name"));
                c.setRemoteId(data.getId());
                ret.add(c);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return ret;
    }

    /**
     *
     * @param countryRemoteId
     * @param cityRemoteId
     * @return
     */
    public List<DidNumberAvailableDTO> getAvailableDids(String countryRemoteId, String cityRemoteId) {

        List<DidNumberAvailableDTO> ret = new ArrayList<>();
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Api-Key", apiKey);
            httpHeaders.set("Content-Type", "application/vnd.api+json");
            httpHeaders.set("Accept", "application/vnd.api+json");

            Map<String, String> params = new HashMap<>();
            String uri = url + "available_dids";

            //only without registrations
            uri += "?filter[did_group.needs_registration]=false";
            if (countryRemoteId != null) {
                uri += "&filter[country.id]=" + countryRemoteId;
            }

            if (cityRemoteId != null) {
                uri += "&filter[city.id]=" + cityRemoteId;
            }

            log.error("Requesting did " + uri);

            HttpEntity entity = new HttpEntity<>(httpHeaders);
            ResponseEntity<DidResponse> response = restTemplate.exchange(uri, HttpMethod.GET, entity, DidResponse.class, params);

            DidResponse didResponse = response.getBody();
            for (DidData data : didResponse.getData()) {
                DidNumberAvailableDTO dto = new DidNumberAvailableDTO();
                dto.setNumber(data.getAttributes().get("number"));
                dto.setRemoteId(data.getId());
                ret.add(dto);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return ret;
    }

    public ClientDid postDidReservation(String remoteDidId, ClientDid did) throws Exception {

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Api-Key", apiKey);
        httpHeaders.set("Content-Type", "application/vnd.api+json");
        httpHeaders.set("Accept", "application/vnd.api+json");

        Map<String, String> params = new HashMap<>();
        String uri = url + "did_reservations";

        log.error("Requesting reservation " + uri + " for did id " + remoteDidId);

        DidReservationData data = new DidReservationData(remoteDidId);
        DidReservation didReservation = new DidReservation(data);
        Map<String, DidReservation> body = new HashMap<>();
        body.put("data", didReservation);

        HttpEntity<Map<String, DidReservation>> entity = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<DidBuyResponse> response = restTemplate.exchange(uri, HttpMethod.POST, entity, DidBuyResponse.class, params);

        log.error("Response status code " + response.getStatusCode());
        log.error("Response body " + response.getBody().getData().getId());
        log.error("Response body " + response.getBody().getData().getType());
        log.error("Response body " + response.getBody().getData().getAttributes());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");

        DidBuyResponse didBuyResponse = response.getBody();
        DidData dataResponse = didBuyResponse.getData();
        String created = dataResponse.getAttributes().get("created_at");
        String expires = dataResponse.getAttributes().get("expire_at");

        did.setCreationDate(sdf.parse(created));
        did.setExpirationDate(sdf.parse(expires));
        did.setReservationId(dataResponse.getId());
        did.setRemoteId(remoteDidId);
        did.setRemoteStatus("created");

        //get reservation in order to get SKU
        uri = uri + "/" + dataResponse.getId() + "?include=available_did.did_group.stock_keeping_units";
        log.error("Requesting reservation " + uri);
        HttpEntity reservationEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ReservationResponse> reservationResponse = restTemplate.exchange(uri, HttpMethod.GET, reservationEntity, ReservationResponse.class, params);

        GenericData reservationData = reservationResponse.getBody().getData();
        List<GenericData> included = reservationResponse.getBody().getIncluded();
        String skuId = null;
        for (GenericData genericData : included) {
            if (genericData.getType().equals("stock_keeping_units")) {
                Map<String, Object> attrs = genericData.getAttributes();
                if (attrs.containsKey("channels_included_count") && attrs.get("channels_included_count").toString().equals("2")) {
                    skuId = genericData.getId();
                    break;
                }
            }
        }
        if (skuId != null) {
            did.setRemoteSku(skuId);
        }

        //place order using SKU                        
        uri = url + "orders";
        Map<String, String> attrs = new HashMap<>();
        attrs.put("did_reservation_id", did.getReservationId());
        attrs.put("sku_id", did.getRemoteSku());

        DidOrderDataAttributesItem item = new DidOrderDataAttributesItem();
        item.setType("did_order_items");
        item.setAttributes(attrs);

        DidOrderDataAttributes attributes = new DidOrderDataAttributes();
        attributes.setAllow_back_ordering(Boolean.FALSE);
        attributes.setItems(Arrays.asList(item));

        DidOrderData didOrderData = new DidOrderData();
        didOrderData.setType("orders");
        didOrderData.setAttributes(attributes);

        DidCreateOrder order = new DidCreateOrder();
        order.setData(didOrderData);

        log.error("Requesting order " + uri);
        HttpEntity<DidCreateOrder> orderEntity = new HttpEntity<>(order, httpHeaders);
        ResponseEntity<DidOrderResponse> responseOrder = restTemplate.exchange(uri, HttpMethod.POST, orderEntity, DidOrderResponse.class, params);

        //get DID id
        uri = url + "dids?filter[number]=" + did.getDidNumber();
        log.error("Requesting did " + uri);
        ResponseEntity<DidListResponse> responseDids = restTemplate.exchange(uri, HttpMethod.GET, reservationEntity, DidListResponse.class);
        GenericData didData = responseDids.getBody().getData().get(0);
        String didId = didData.getId();
        did.setRemoteId(didId);

        //get did ID and set TRUNK TO DID
        DidTrunkUptade trunkUptade = new DidTrunkUptade();
        DidTrunkUptadeData trunkUptadeData = new DidTrunkUptadeData();
        trunkUptadeData.setId(didId);
        trunkUptadeData.setType("dids");
        //trunk ID -- 4840ec48-f46c-4441-a6da-0decdd8f45a9
        trunkUptadeData.setTrunk("4840ec48-f46c-4441-a6da-0decdd8f45a9");
        trunkUptade.setData(trunkUptadeData);

        uri = url + "dids/" + did.getRemoteId();
        log.error("Requesting did PATH" + uri);
        HttpEntity<DidTrunkUptade> trunkEntity = new HttpEntity<>(trunkUptade, httpHeaders);
        responseOrder = restTemplate.exchange(uri, HttpMethod.PATCH, trunkEntity, DidOrderResponse.class);

        /*try {
            postOrder(did);
        } catch (Exception e) {
            uri = uri + "/" + dataResponse.getId();
            restTemplate.delete(uri);
            log.error("deleting reservation " + uri, e);
        }*/
        return did;
    }

    public void postOrder(ClientDid did) {

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Api-Key", apiKey);
        httpHeaders.set("Content-Type", "application/vnd.api+json");
        httpHeaders.set("Accept", "application/vnd.api+json");

        Map<String, String> params = new HashMap<>();
        String uri = url + "orders";

        //log.error("Requesting order " + uri + " for reservation id " + reservationId);
        Map<String, String> attrs = new HashMap<>();
        attrs.put("did_reservation_id", did.getReservationId());
        attrs.put("sku_id", did.getRemoteSku());

        DidOrderDataAttributesItem item = new DidOrderDataAttributesItem();
        item.setType("did_order_items");
        item.setAttributes(attrs);

        DidOrderDataAttributes attributes = new DidOrderDataAttributes();
        attributes.setAllow_back_ordering(Boolean.FALSE);
        attributes.setItems(Arrays.asList(item));

        DidOrderData data = new DidOrderData();
        data.setType("orders");
        data.setAttributes(attributes);

        DidCreateOrder order = new DidCreateOrder();
        order.setData(data);

        HttpEntity<DidCreateOrder> entity = new HttpEntity<>(order, httpHeaders);
        ResponseEntity<DidBuyResponse> response = template.exchange(uri, HttpMethod.POST, entity, DidBuyResponse.class, params);

        String resp = ReflectionToStringBuilder.reflectionToString(response);

        log.error("Respuesta a la compra " + resp);

    }

    /**
     *
     * @param clientDid
     * @return
     */
    public boolean cancelSubscription(ClientDid clientDid) {

        String remoteId = clientDid.getRemoteId();
        if (remoteId == null) {
            log.error("Error cancel subscription, did remote is null client did id = " + clientDid.getId());
            return true;
        }

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //RestTemplate template = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Api-Key", apiKey);
        httpHeaders.set("Content-Type", "application/vnd.api+json");
        httpHeaders.set("Accept", "application/vnd.api+json");

        Map<String, String> params = new HashMap<>();
        String uri = url + "dids/" + remoteId;

        DidDoubleData didDoubleData = new DidDoubleData();
        DidData data = new DidData();
        data.setId(remoteId);
        data.setType("dids");

        Map<String, String> attrs = new HashMap<>();
        attrs.put("terminated", "false");
        attrs.put("pending_removal", "true");

        data.setAttributes(attrs);
        didDoubleData.setData(data);

        try {
            HttpEntity<DidDoubleData> entity = new HttpEntity<>(didDoubleData, httpHeaders);
            ResponseEntity<DidData> response = template.exchange(uri, HttpMethod.PATCH, entity, DidData.class, params);

            String resp = ReflectionToStringBuilder.reflectionToString(response.getBody());
            if (response.getStatusCodeValue() == 200) {
                log.error("Respuesta cancel subscription " + resp);
                return true;
            } else {
                log.error("Respuesta cancel subscription ERROR " + resp);
                return false;
            }
        } catch (Exception e) {
            log.error("Respuesta cancel subscription ERROR " + e.getMessage());
            return true;
        }

        //PATCH https://api.didww.com/v3/dids/46e129f1-deaa-44db-8915-2646de4d4c70 HTTP/1.1
        //Content-Type: application/vnd.api+json
        //Accept: application/vnd.api+json
        //Api-Key: [API token]
        /*{
            "data" : {
                "id" : "46e129f1-deaa-44db-8915-2646de4d4c70",
                "type": "dids",
                "attributes": {
                  "terminated" : false,
                  "pending_removal": false,
                  "description": "string",
                  "capacity_limit": 1
                }
          }
        }*/
    }

    @Transactional
    public Page<DidCountry> findAll(String q, Pageable pageable) {
        return didCountryRepository.findAll(DidSpecs.search(q), pageable);
    }
}
