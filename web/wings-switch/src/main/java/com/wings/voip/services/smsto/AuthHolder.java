/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.smsto;

import java.util.Date;

/**
 *
 * @author seba
 */
public class AuthHolder {

    private AuthResponse authResponse;
    private Date creationDate;

    public AuthHolder(AuthResponse authResponse) {
        this.authResponse = authResponse;
        this.creationDate = new Date();
    }

    public AuthResponse getAuthResponse() {
        return authResponse;
    }

    public void setAuthResponse(AuthResponse authResponse) {
        this.authResponse = authResponse;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getToken() {
        if (authResponse == null) {
            return null;
        }
        return authResponse.getJwt();
    }

    public boolean isExpired() {
        if (authResponse == null) {
            return true;
        }
        long seconds = authResponse.getExpires();
        long mili = System.currentTimeMillis() - creationDate.getTime();
        long elapsed = mili / 1000;
        //16 minutes to expire
        return ((seconds - elapsed) < 1000);
    }
}
