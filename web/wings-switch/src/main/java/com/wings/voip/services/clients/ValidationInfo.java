/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.clients;

/**
 *
 * @author seba
 */
public class ValidationInfo {
    
    private int country_code;
    private String country_iso_code;
    private String carrier;
    private boolean is_mobile;
    private String e164_format;
    private String formatting;

    public int getCountry_code() {
        return country_code;
    }

    public void setCountry_code(int country_code) {
        this.country_code = country_code;
    }

    public String getCountry_iso_code() {
        return country_iso_code;
    }

    public void setCountry_iso_code(String country_iso_code) {
        this.country_iso_code = country_iso_code;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public boolean isIs_mobile() {
        return is_mobile;
    }

    public void setIs_mobile(boolean is_mobile) {
        this.is_mobile = is_mobile;
    }

    public String getE164_format() {
        return e164_format;
    }

    public void setE164_format(String e164_format) {
        this.e164_format = e164_format;
    }

    public String getFormatting() {
        return formatting;
    }

    public void setFormatting(String formatting) {
        this.formatting = formatting;
    }
    
    
    
}
