/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;

/**
 *
 * @author seba
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DidDoubleData {

    private DidData data;

    public DidData getData() {
        return data;
    }

    public void setData(DidData data) {
        this.data = data;
    }

}
