/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.utils;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author seba
 */
public class RequestUtils {

    public static void debugRequestParams(HttpServletRequest request) {
        System.out.println("--------------------------------------------------");
        Map<String, String[]> params = request.getParameterMap();
        params.entrySet().forEach((entry) -> {
            String key = entry.getKey();
            String[] value = entry.getValue();
            System.out.println("Key : " + key + " -> " + Arrays.toString(value));
        });
    }
    
     public static void debugRequestBody(HttpServletRequest request) {
        System.out.println("--------------------------------------------------");
        try {
            String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            System.out.println(body);
        } catch (Exception e) {
        }
    }

    public static Map<String, String> getRequestParams(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Map<String, String> ret = new HashMap<>();
        params.entrySet().forEach((entry) -> {
            String key = entry.getKey();
            String[] value = entry.getValue();
            ret.put(key, Arrays.toString(value).replaceAll("\\]", "").replaceAll("\\[", ""));
        });
        return ret;
    }

    public static void debugRequestHeaders(HttpServletRequest request) {
        System.out.println("--------------------------------------------------");
        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String nextElement = headers.nextElement();
            System.out.println("Header Name " + nextElement);
            System.out.println("Header Value " + request.getHeader(nextElement));
        }
    }

}
