/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.fs;

import com.wings.voip.domain.cdr.Cdr;
import com.wings.voip.domain.sw.entities.CdrPersistent;
import com.wings.voip.domain.sw.repos.CdrRepository;
import com.wings.voip.services.ClientService;
import com.wings.voip.services.PriceListService;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class CdrPostController {

    @Autowired
    private CdrRepository cdrRepository;
    @Autowired
    private PriceListService priceListService;
    @Autowired
    private ClientService clientService;

    /**
     * This is posted from freeswitch CDR
     *
     * @param request
     * @param cdr
     * @return
     */
    @RequestMapping(value = {"/fs/cdr"}, method = RequestMethod.POST, produces = {MediaType.TEXT_XML_VALUE}, consumes = {MediaType.TEXT_XML_VALUE})
    public ResponseEntity<String> register(HttpServletRequest request, @RequestBody(required = false) Cdr cdr) {

        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = request.getRemoteAddr();
        }

        if (ip == null || (!ip.equals("79.143.182.239") && !ip.equals("127.0.0.1") && !ip.equals("10.10.39.4"))) {
            return ResponseEntity.ok().build();
        }

        //TODO: guarda en cache y procesar despues, una vez insertado, agregar balance negativo al cliente
        CdrPersistent persistent = createPersistent(cdr);
        cdrRepository.save(persistent);

        //add balance to client
        if (persistent.getOrigination() != null && persistent.getOrigination().equals("5493518149914")) {
            clientService.addCdrBalance(persistent);
        }

        return ResponseEntity.ok().build();
    }

    /**
     * Parse from CDR
     *
     * @param cdr
     * @return
     */
    private CdrPersistent createPersistent(Cdr cdr) {

        CdrPersistent persistent = new CdrPersistent();

        persistent.setDuration(Integer.valueOf(cdr.getVariables().getDuration()));
        persistent.setBillsec(Integer.valueOf(cdr.getVariables().getBillsec()));
        persistent.setProgresssec(Integer.valueOf(cdr.getVariables().getProgresssec()));
        persistent.setAnswersec(Integer.valueOf(cdr.getVariables().getAnswersec()));
        persistent.setWaitsec(Integer.valueOf(cdr.getVariables().getWaitsec()));

        persistent.setUuid(cdr.getVariables().getUuid());
        persistent.setCreationDate(transformUsecToDate(cdr.getCallflow().getTimes().getCreated_time()));

        String origination = cdr.getCallflow().getCaller_profile().getUsername();
        if (origination == null || origination.isEmpty()) {
            origination = cdr.getCallflow().getCaller_profile().getCaller_id_name();
        }
        persistent.setOrigination(origination);

        persistent.setOriginationCallerId(cdr.getCallflow().getCaller_profile().getCaller_id_number());
        persistent.setOriginationIp(cdr.getCallflow().getCaller_profile().getNetwork_addr());
        persistent.setDestination(cdr.getCallflow().getCaller_profile().getDestination_number());

        try {
            persistent.setDestinationIp(cdr.getCallflow().getCaller_profile().getOriginatee().getOriginatee_caller_profile().getNetwork_addr());
        } catch (Exception e) {
        }

        persistent.setDomainName(cdr.getVariables().getDomain_name());
        persistent.setBridgeHangupCause(cdr.getVariables().getBridge_hangup_cause());
        persistent.setHangupCause(cdr.getVariables().getHangup_cause());
        persistent.setSecure(cdr.getVariables().getZrtp_passthru_active() != null);
        persistent.setWingsPlan("");
        persistent.setWingsInternal(true);
        persistent.setUserAgent(cdr.getVariables().getSip_user_agent());
        try {
            persistent.setNatDetected(cdr.getVariables().getSip_nat_detected().equals("true"));
        } catch (Exception e) {
        }

        persistent.setCost(0.0);
        persistent.setRealCost(0.0);

        //saco el costo si existe la variable
        String sid = cdr.getVariables().getWings_dest_id();
        if (sid != null) {
            Long destId = Long.parseLong(sid);
            Double cost = 0d;
            if (persistent.getBillsec() != 0) {

                int minutes = 0;
                if (persistent.getBillsec() != 0) {
                    minutes = (int) Math.ceil(persistent.getBillsec() / 60.0);
                }
                persistent.setMinutes(minutes);
                cost = priceListService.calculateCost(minutes, destId);
            }
            persistent.setCost(cost);
            persistent.setDestinationId(destId);
        }
        String sServiceId = cdr.getVariables().getWings_service_id();
        try {
            persistent.setServiceId(Long.valueOf(sServiceId));
        } catch (Exception e) {
        }

        String sClientServiceId = cdr.getVariables().getWings_client_service_id();
        try {
            persistent.setClientServiceId(Long.valueOf(sClientServiceId));
        } catch (Exception e) {
        }

        return persistent;

    }

    /**
     *
     * @param usec
     * @return
     */
    private Date transformUsecToDate(String usec) {
        long val = Long.valueOf(usec);
        val = val / 1000;
        return new Date(val);
    }
}
