/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.CdrPersistent;
import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientBalance;
import com.wings.voip.domain.sw.entities.ClientContact;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author seba
 */
@Repository
public interface CdrRepository extends JpaRepository<CdrPersistent, Long>, JpaSpecificationExecutor<CdrPersistent> {

    @Query
    List<CdrPersistent> findFirst10ByOrigination(String origination);

}
