/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.ClientDid;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ClientDidRepository extends JpaRepository<ClientDid, Long>, JpaSpecificationExecutor<ClientDid> {

    List<ClientDid> findByClientId(Long clientId);

    List<ClientDid> findByLocalStatus(String localStatus);

    ClientDid findFirstByClientId(Long clientId);

    ClientDid findByClientIdAndClientServiceId(Long clientId, Long clientServiceId);

    ClientDid findByDidNumber(String didNumber);

}
