/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

/**
 *
 * @author seba
 */
public class DidOrderData {

    private String type;
    private DidOrderDataAttributes attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAttributes(DidOrderDataAttributes attributes) {
        this.attributes = attributes;
    }

    public DidOrderDataAttributes getAttributes() {
        return attributes;
    }

}
