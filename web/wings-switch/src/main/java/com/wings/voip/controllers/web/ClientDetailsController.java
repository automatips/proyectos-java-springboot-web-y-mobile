package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.CdrPersistent;
import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.repos.CdrRepository;
import com.wings.voip.services.ClientService;
import com.wings.voip.services.PushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ClientDetailsController {

    @Autowired
    private ClientService clientService;
    @Autowired
    private CdrRepository cdrRepository;
    @Autowired
    private PushService pushService;

    /**
     *
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"view/clientDetails/{id}"}, method = RequestMethod.GET)
    public String getClientDetails(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Client client = clientService.findOneClient(id);
        if (client != null) {
            model.addAttribute("item", client);
        } else {
            model.addAttribute("item", new Client());
        }

        List<CdrPersistent> listCdrPersistent;
        listCdrPersistent = cdrRepository.findFirst10ByOrigination(String.valueOf(client.getId()));
        model.addAttribute("cdr1", listCdrPersistent);

        return "html/client-details";
    }

    /**
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = {"views/clientDetails/balance/{id}/credit"}, method = RequestMethod.GET)
    public String getAddCredit(Model model, @PathVariable("id") Long id) {
        model.addAttribute("id", id);
        return "html/add_credit_details";
    }

    @RequestMapping(value = {"views/clientDetails/balance/{id}/update"}, method = RequestMethod.POST)
    public ResponseEntity<String> postOrderAddCredit(HttpServletRequest request, @PathVariable("id") Long id) {

        String amount = request.getParameter("credit");
        if (amount != null && amount.length() > 0) {
            //clientService.getAddCredit(id, amount, request);
        } else {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok("Balance NOT modified");
    }

    /**
     *
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"view/clientDetails/{id}/push/"}, method = RequestMethod.GET)
    public String getClientDetailsPush(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        model.addAttribute("deviceId", id);
        return "html/client-details-push";
    }

    @RequestMapping(value = {"view/clientDetails/{id}/push/"}, method = RequestMethod.POST)
    public ResponseEntity<String> postClientDetailsPush(Model model, HttpServletRequest request, @PathVariable("id") Long clientDeviceId) {

        String title = request.getParameter("title");
        String body = request.getParameter("body");
        pushService.sendGenericPush(title, body, clientDeviceId);

        return null;
    }

}
