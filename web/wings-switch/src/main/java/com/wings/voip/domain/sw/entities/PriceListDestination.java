package com.wings.voip.domain.sw.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "price_list_destination")
public class PriceListDestination implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "price_list_id")
    private Long priceListId;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 25)
    @Column(name = "country_code")
    private String countryCode;
    @Column(name = "area_code")
    private Integer areaCode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "price_list_id", updatable = false, insertable = false, nullable = true)
    private PriceList priceList;

    @Transient
    private Long serviceId;
    @Transient
    private Long clientServiceId;

    public PriceListDestination() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Long priceListId) {
        this.priceListId = priceListId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PriceList getPriceList() {
        return priceList;
    }

    public void setPriceList(PriceList priceList) {
        this.priceList = priceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PriceListDestination)) {
            return false;
        }
        PriceListDestination other = (PriceListDestination) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.voip.domain.sw.entities.PriceListDestination[ id=" + id + " ]";
    }

    /**
     * use to send to CDR which service was used
     *
     * @return
     */
    public Long getServiceId() {
        return serviceId;
    }

    /**
     * use to send to CDR which service was used
     *
     * @param serviceId
     */
    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Long getClientServiceId() {
        return clientServiceId;
    }

    public void setClientServiceId(Long clientServiceId) {
        this.clientServiceId = clientServiceId;
    }

}
