/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.web;

import com.wings.voip.services.LocalizedCountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class LocalizationController {

    @Autowired
    private LocalizedCountriesService localizedCountriesService;

    @RequestMapping(value = "views/localization")
    public String syncCountries() {
        return "html/localization";
    }

    @RequestMapping(value = "views/localization/resync", method = RequestMethod.POST)
    public String postSynCountries() {
        localizedCountriesService.syncCountries();
        return "redirect:/#views/localization";
    }

}
