/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import com.wings.voip.sip.MissedCallResponse;
import java.net.URI;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class TwilioClient {

    // Find your Account Sid and Token at twilio.com/console
    // DANGER! This is insecure. See http://twil.io/secure
    //prod
    public static final String ACCOUNT_SID = "AC312a4ac7448883a30bf7c12bf9ff5f63";
    public static final String AUTH_TOKEN = "e465651a78664be0fe6620f36b60fe17";
    //test
    //public static final String ACCOUNT_SID = "AC3bbb2fd3e04e49eeae24c42199b26040";
    //public static final String AUTH_TOKEN = "58f0a266da5e0c1b72a045d2e5264ec6";

    private String[] origin = {
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363",
        "+12056493363"
    };

    public MissedCallResponse makeMissedCall(String to) {

        int index = 0;
        try {
            String last = to.substring(to.length() - 1);
            index = Integer.parseInt(last);
        } catch (Exception e) {
        }

        String from = origin[index];
        String uuid = makeCall(from, to);
        return new MissedCallResponse(from, uuid);

    }

    public String makeCall(String from, String to) {

        String ret = "";

        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Call call = Call.creator(
                new com.twilio.type.PhoneNumber("+" + to),
                new com.twilio.type.PhoneNumber(from),
                URI.create("https://wingsmobile.com/voice.xml"))
                .create();

        ret = call.getSid();

        return ret;
    }
}
