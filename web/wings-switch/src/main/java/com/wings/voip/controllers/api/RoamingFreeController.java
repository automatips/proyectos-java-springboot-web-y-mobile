/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientRoamingFree;
import com.wings.voip.domain.sw.entities.RoamingFreeOperator;
import com.wings.voip.domain.sw.entities.RoamingFreeOperatorsNumber;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientRoamingFreeRepository;
import com.wings.voip.domain.sw.repos.RoamingFreeOperatorNumberRepository;
import com.wings.voip.domain.sw.repos.RoamingFreeOperatorRepository;
import com.wings.voip.model.secondNumber.RoamingFreeResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class RoamingFreeController {

    @Autowired
    private RoamingFreeOperatorRepository roamingFreeOperatorRepository;

    @Autowired
    private RoamingFreeOperatorNumberRepository roamingFreeOperatorNumberRepository;

    @Autowired
    private ClientRoamingFreeRepository clientRoamingFreeRepository;

    @Autowired
    private ClientRepository clientRepository;

    private Logger log = LoggerFactory.getLogger(RoamingFreeController.class);

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/roaming/action"}, method = {RequestMethod.POST})
    public ResponseEntity<RoamingFreeResponse> enableSecondNumber(HttpServletRequest request) {

        String mcc = request.getParameter("mcc");
        String mnc = request.getParameter("mnc");
        String login = request.getParameter("login");
        String enable = request.getParameter("enable");

        log.error("Roamin free requested  mcc " + mcc + " mnc " + mnc + " number " + login);

        RoamingFreeResponse response = new RoamingFreeResponse();

        //find client
        Client client = clientRepository.findByLogin(login);
        if (client == null) {
            response.setEnabled(Boolean.FALSE);
            response.setMessage("roaming_free_client_not_found");
            return ResponseEntity.ok(response);
        }

        //find roaming free asignation
        ClientRoamingFree clientRoamingFree = clientRoamingFreeRepository.findByClientId(client.getId());
        if (clientRoamingFree == null) {
            clientRoamingFree = new ClientRoamingFree();
        }

        //find operator
        RoamingFreeOperator operator = roamingFreeOperatorRepository.findByMccAndMncAndEnabled(mcc, mnc, Boolean.TRUE);
        if (operator == null) {
            response.setEnabled(Boolean.FALSE);
            response.setMessage("roaming_free_operator_not_supported");
            return ResponseEntity.ok(response);
        }

        //find operator number
        RoamingFreeOperatorsNumber number = roamingFreeOperatorNumberRepository.findFirstByOperatorIdOrderByAsignationsAsc(operator.getId());
        if (enable.equals("true")) {

            String redirectNumber = number.getDidNumber();

            response.setDialString(operator.getDialStringEnable().replaceAll("NUMBER", redirectNumber));
            response.setEnabled(Boolean.TRUE);
            response.setRedirectNumber(redirectNumber);
            number.setAsignations(number.getAsignations() + 1);

            //save client asignation
            clientRoamingFree.setClientId(client.getId());
            clientRoamingFree.setClientNumber(login);
            clientRoamingFree.setOperatorNumber(number.getDidNumber());

            clientRoamingFreeRepository.save(clientRoamingFree);

        } else {
            response.setDialString(operator.getDialStringDisable());
            response.setEnabled(Boolean.FALSE);
            number.setAsignations(number.getAsignations() - 1);

            //delete client asignation
            clientRoamingFreeRepository.delete(clientRoamingFree);

        }

        roamingFreeOperatorNumberRepository.save(number);

        return ResponseEntity.ok(response);

    }

}
