/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Origination {

    private Caller_profile origination_caller_profile;

    public Caller_profile getOrigination_caller_profile() {
        return origination_caller_profile;
    }

    public void setOrigination_caller_profile(Caller_profile origination_caller_profile) {
        this.origination_caller_profile = origination_caller_profile;
    }

}
