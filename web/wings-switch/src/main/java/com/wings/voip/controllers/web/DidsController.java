package com.wings.voip.controllers.web;

import com.wings.voip.SwitchApplication;
import com.wings.voip.domain.sw.entities.DidCountry;
import com.wings.voip.services.DidWWService;
import com.wings.voip.utils.PageWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * @author seba
 */
@Controller
public class DidsController {

    private static final Logger log = LoggerFactory.getLogger(SwitchApplication.class);
    public static final String ID = "id";
    public static final String SSIZE = "20";
    public static final String SPAGE = "0";
    public static final int PAGE = 0;
    public static final int SIZE = 20;
    
    @Autowired
    private DidWWService didWWService;

    @RequestMapping(value = "/views/dids/sync")
    public String syncCountries() {

        didWWService.sync();

        return "redirect:/views/dids";
    }

    @RequestMapping(value = "/views/dids", method = RequestMethod.GET)
    public String getClients(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = SSIZE;
        }

        if (spage == null) {
            spage = SPAGE;
        }

        int page = PAGE;
        int size = SIZE;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, ID));

        Page<DidCountry> didCountries = didWWService.findAll(q, pr);

        PageWrapper<DidCountry> pageWrapper = new PageWrapper<>(didCountries, "/views/dids");

        model.addAttribute("list", didCountries);
        model.addAttribute("page", pageWrapper);
        return "html/dids";
    }
}
