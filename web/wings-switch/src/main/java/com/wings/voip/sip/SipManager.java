/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.sip;

import com.wings.voip.utils.NumberUtils;
import io.freeswitch.codec.FreeSwitchMessageHeaders;
import io.freeswitch.event.EslEvent;
import io.freeswitch.event.IEventsListener;
import io.freeswitch.outbound.FreeSwitchClient;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SipManager {

    private final FreeSwitchClient client = new FreeSwitchClient();
    private final Logger log = LoggerFactory.getLogger(SipManager.class);
    private final String host = "79.143.182.239";
    private final int port = 8021;
    private final String password = "JZx9hGQFXZyLhByT";
    public boolean connected = false;

    public SipManager() {
        connect();
    }

    public MissedCallResponse makeMissedCall(String to) {

        int length = to.length();
        String prefix = to.substring(0, 4);
        String suffix = NumberUtils.getNumberRandomString(length - 4);
        String from = prefix + suffix;
        String uuid = makeCall(from, to);
        //String uuid = UUID.randomUUID().toString();

        return new MissedCallResponse(from, uuid);
    }

    public String makeCall(String from, String to) {

        if (!connected) {
            connect();
        }

        if (!client.canSend()) {
            connect();
        }

        //IDT
        String idtHost = "213.166.103.1";
        to = "99900" + to;

        String id = null;
        try {
            //UUID uuid = client.bgApi("originate", "{origination_caller_id_number=" + from + ",fail_on_single_reject=true,originate_retries=false,return_ring_ready=false}sofia/external/" + to + "@79.143.183.86 XML default validation_api " + from);
            log.error("ORIGINATE " + "{origination_caller_id_number=" + from + ",fail_on_single_reject=true,originate_retries=false,ignore_early_media=true,originate_timeout=30}sofia/external/" + to + "@" + idtHost + " XML default " + from + " " + from);
            UUID uuid = client.bgApi("originate", "{origination_caller_id_name=missed_call_api,origination_caller_id_number=" + from + ",fail_on_single_reject=true,originate_retries=false,ignore_early_media=true,originate_timeout=30}sofia/external/" + to + "@" + idtHost + " XML default " + from + " " + from);
            id = uuid.toString();
        } catch (Exception e) {
            connected = false;
            throw e;
        }
        return id;
    }

    public void done(String callId) {

        if (!connected) {
            connect();
        }

        if (!client.canSend()) {
            connect();
        }

        try {
            client.bgApi("uuid_kill", callId);
        } catch (Exception e) {
            connected = false;
            throw e;
        }
    }

    private void connect() {

        client.removeEnetListeners();
        client.addEventListener(new IEventsListener() {
            //Event received [EslEvent: name=[CHANNEL_CREATE] headers=2, eventHeaders=78, eventBody=0 lines.]
            //Background job result received [EslEvent: name=[BACKGROUND_JOB] headers=2, eventHeaders=17, eventBody=1 lines.]
            //Event received [EslEvent: name=[CHANNEL_DESTROY] headers=2, eventHeaders=228, eventBody=0 lines.]            
            public void eventReceived(EslEvent event) {
                //log.error("Event received [{}]", event);
                //if (event.eventName().equals("CUSTOM")) {
                //    printHeaders(event);
                //}
                //log.error(event.headers().toString());
                //log.error(event.eventHeaders().toString());
            }

            public void backgroundJobEventReceived(EslEvent event) {
                //log.error("Background job result received [{}]", event);
                //log.error(event.headers().toString());
                //log.error(event.eventHeaders().toString());
            }
        });

        log.error("Client connecting ..");
        try {
            client.connect(host, port, password, 20);
            //client.event("plain", "CHANNEL_CREATE CHANNEL_DESTROY HEARTBEAT CHANNEL_STATE CUSTOM BACKGROUND_JOB");
            client.event("plain", "all");
            //client.filter("Event-HeaderName", "heartbeat");
            //client.filter("Event-HeaderName", "channel_create");
            //client.filter("Event-HeaderName", "BACKGROUND_JOB");

            log.error("Client connected ..");
            connected = true;
        } catch (Exception e) {
            log.error("Connect failed", e);
        }
    }

    private void printHeaders(EslEvent event) {

        log.error("-----------Headers " + event.eventName() + "----------------------");
        Map<FreeSwitchMessageHeaders.HeaderName, String> headers = event.headers();
        for (Map.Entry<FreeSwitchMessageHeaders.HeaderName, String> entry : headers.entrySet()) {
            FreeSwitchMessageHeaders.HeaderName key = entry.getKey();
            String value = entry.getValue();
            log.error("Header-name: " + key.name() + "-> " + value);
        }

        log.error("----------- Event Headers ----------------------");
        Map<String, String> eventHeaders = event.eventHeaders();
        eventHeaders.entrySet().forEach((entry) -> {
            String key = entry.getKey();
            String value = entry.getValue();
            log.error("Header-name: " + key + "-> " + value);
        });
        log.error("--------------------------------------------");
        log.error("                          ");
        log.error("                          ");
        log.error("                          ");
    }
}
