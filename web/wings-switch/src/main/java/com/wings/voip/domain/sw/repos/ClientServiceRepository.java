/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.ClientService;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ClientServiceRepository extends JpaRepository<ClientService, Long>, JpaSpecificationExecutor<ClientService> {

    ClientService findByClientIdAndServiceId(Long clientId, Long serviceId);

    List<ClientService> findByClientId(Long clientId);
    
    List<ClientService> findByClientIdAndActiveOrderByIdAscExpirationDateDesc(Long clientId, Boolean active);
    
    List<ClientService> findByClientIdAndActiveOrderByExpirationDateDescIdAsc(Long clientId, Boolean active);

    List<ClientService> findByClientIdAndActiveAndRemainingMoneyGreaterThanOrderByIdAsc(Long clientId, Boolean active, double remainingMoney);
    
    List<ClientService> findByActive(Boolean active);

}
