/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientContact;
import com.wings.voip.domain.sw.repos.ClientContactRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.requests.SyncContactRequest;
import com.wings.voip.model.requests.SyncContactResponse;
import com.wings.voip.services.ContactsService;
import com.wings.voip.services.PendinActionsService;
import com.wings.voip.utils.EncryptUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author seba
 */
@Controller
public class ContactsController {

    private final Logger log = LoggerFactory.getLogger(ContactsController.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientContactRepository clientContactRepository;

    @Autowired
    private ContactsService contactsService;

    @Autowired
    private PendinActionsService pendinActionsService;

    /**
     *
     * @param request
     * @param appId
     * @param accessToken
     * @param syncRequest
     * @return
     */
    @RequestMapping(value = {"/api/contacts/sync"}, method = RequestMethod.POST)
    public ResponseEntity<SyncContactResponse> contactSync(HttpServletRequest request,
            @RequestParam(required = false, value = "app_id") String appId,
            @RequestParam(required = false, value = "access_token") String accessToken,
            @RequestBody SyncContactRequest syncRequest) {

        String login = syncRequest.getLogin();
        String password = EncryptUtils.decrypt(syncRequest.getPassword());

        //cancel push sends
        pendinActionsService.markAsCompleted("update-contacts", login);

        //Authorize login
        Client client = clientRepository.findByLogin(login);
        if (client == null) {
            log.error("no se encotnro el cliente sync " + login);
            return ResponseEntity.badRequest().build();
        }

        if (!client.getToken().equals(password)) {
            log.error("error autenticando");
            return ResponseEntity.badRequest().build();
        }

        //authorize app
        if (!(appId.equals("a2dc3b1c57d64fe9840c5cb") && accessToken.equals("6f4390a15e9165f37ad6d96e0c63f68fb1cc1f13"))) {
            log.error("error autenticando");
            return ResponseEntity.badRequest().build();
        }

        //last and not least, authorize request
        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("Basic YW5kcm9pZF9hcHA6d0c2REVzUzM0clQ1OUE5Vg==")) {
            log.error("error autenticando");
            return ResponseEntity.badRequest().build();
        }

        //get all client contacts and build a map from numbers
        Map<String, ClientContact> existing = new HashMap<>();
        List<ClientContact> clientContacts = clientContactRepository.findByClientId(client.getId());
        clientContacts.forEach((clientContact) -> {
            existing.put(clientContact.getNumber() + clientContact.getRaw(), clientContact);
        });

        //compare remote to local
        List<ClientContact> toSave = new ArrayList<>();

        List<Contact> contacts = syncRequest.getContacts();
        for (Contact contact : contacts) {

            for (com.wings.voip.model.contact.Number number : contact.getNumbers()) {
                if ((number.getNumber() == null || number.getNumber().isEmpty()) && !number.getRawNumber().isEmpty()) {
                    number.setNumber(number.getRawNumber().replaceAll("\\+", "").replaceAll(" ", ""));
                }
            }
        }
        contactsService.parseContactServices(contacts);

        contacts.forEach((contact) -> {
            List<com.wings.voip.model.contact.Number> numbers = contact.getNumbers();
            numbers.forEach((number) -> {

                if ((number.getNumber() == null || number.getNumber().isEmpty()) && !number.getRawNumber().isEmpty()) {
                    //log.error("no number for " + contact.getName() );
                    //return;
                    number.setNumber(number.getRawNumber().replaceAll("\\+", "").replaceAll(" ", ""));
                }

                //if local existes check for name update
                if (existing.containsKey(number.getNumber() + number.getRawNumber())) {

                    ClientContact e = existing.remove(number.getNumber());
                    if (e != null && !e.getDisplayName().equals(contact.getName())) {
                        e.setDisplayName(contact.getName());
                        toSave.add(e);
                    }

                } else {
                    //if not, create new local one                    
                    ClientContact e = new ClientContact();
                    e.setClientId(client.getId());
                    e.setDisplayName(contact.getName());
                    e.setNumber(number.getNumber());
                    e.setRaw(number.getRawNumber());
                    e.setFormatted(number.getFormattedNumber());
                    e.setFree(number.isFree());
                    e.setSecure(number.isSecure());
                    e.setLabel(number.getLabel());
                    toSave.add(e);
                }
            });
        });

        for (ClientContact clientContact : toSave) {
            try {
                clientContactRepository.save(clientContact);
            } catch (Exception e) {
            }
        }
        //save ones in the phone

        SyncContactResponse resp = new SyncContactResponse();
        resp.setLogin(login);
        resp.setContacts(contacts);

        return ResponseEntity.ok(resp);
    }
}
