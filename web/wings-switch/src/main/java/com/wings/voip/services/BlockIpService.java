/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class BlockIpService {

    @Autowired
    private SshClientService sshClientService;

    private final Logger log = LoggerFactory.getLogger(BlockIpService.class);
    private final Map<String, Integer> toBlock = new HashMap<>();

    /**
     *
     * @param ip
     */
    public void addBlockIp(String ip) {
        log.error("Marking ip as banneable " + ip);
        //put on block list
        if (toBlock.containsKey(ip)) {
            //increment
            int cant = toBlock.get(ip) + 1;
            toBlock.put(ip, cant);
            if (cant > 1) {
                blockIp(ip);
                toBlock.remove(ip);
            }
        } else {
            toBlock.put(ip, 1);
        }
    }

    public void blockIp(String ip) {
        sshClientService.sendCommands(
                "root",
                "sip.wingsmobile.com",
                Arrays.asList("iptables -I INPUT -s " + ip + " -j DROP"));

        log.error("Blocking ip " + ip);
    }

}
