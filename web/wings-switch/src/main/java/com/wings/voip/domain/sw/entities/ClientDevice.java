/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "client_devices")
public class ClientDevice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "client_id")
    private long clientId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Size(max = 150)
    @Column(name = "device_serial_number")
    private String deviceSerialNumber;
    @Size(max = 150)
    @Column(name = "device_name")
    private String deviceName;
    @Size(max = 150)
    @Column(name = "device_model")
    private String deviceModel;
    @Size(max = 200)
    @Column(name = "device_id")
    private String deviceId;
    @Size(max = 200)
    @Column(name = "device_udid")
    private String deviceUdid;
    @Size(max = 200)
    @Column(name = "imei1")
    private String imei1;
    @Size(max = 200)
    @Column(name = "imei2")
    private String imei2;
    @Size(max = 250)
    @Column(name = "push_token")
    private String pushToken;
    @Size(max = 250)
    @Column(name = "push_instance_id")
    private String pushinstanceId;
    @Size(max = 250)
    @Column(name = "device_os")
    private String deviceOs;
    @Size(max = 250)
    @Column(name = "app_version")
    private String appVersion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", updatable = false, insertable = false, nullable = true)
    private Client client;

    public ClientDevice() {
    }

    public ClientDevice(Long id) {
        this.id = id;
    }

    public ClientDevice(Long id, long clientId, Date creationDate, boolean active) {
        this.id = id;
        this.clientId = clientId;
        this.creationDate = creationDate;
        this.active = active;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceUdid() {
        return deviceUdid;
    }

    public void setDeviceUdid(String deviceUdid) {
        this.deviceUdid = deviceUdid;
    }

    public String getImei1() {
        return imei1;
    }

    public void setImei1(String imei1) {
        this.imei1 = imei1;
    }

    public String getImei2() {
        return imei2;
    }

    public void setImei2(String imei2) {
        this.imei2 = imei2;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getPushInstanceId() {
        return pushinstanceId;
    }

    public void setPushInstanceId(String pushInstanceId) {
        this.pushinstanceId = pushInstanceId;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientDevice)) {
            return false;
        }
        ClientDevice other = (ClientDevice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.voip.domain.sw.entities.ClientDevice[ id=" + id + " ]";
    }
    
}
