/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.ClientContact;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface ClientContactRepository extends JpaRepository<ClientContact, Long>, JpaSpecificationExecutor<ClientContact> {

    List<ClientContact> findByClientId(Long clientId);

    @Modifying
    @Transactional
    @Query("update ClientContact c set c.free = 1 where number = ?1")
    void setMeFreeOnContacts(String login);
    
    @Modifying
    @Transactional
    @Query("update ClientContact c set c.secure = 1 where number = ?1")
    void setMeSecureOnContacts(String login);
    
    @Modifying
    @Transactional
    @Query("update ClientContact c set c.secure = 0 where number = ?1")
    void setMeUnSecureOnContacts(String login);
    
}
