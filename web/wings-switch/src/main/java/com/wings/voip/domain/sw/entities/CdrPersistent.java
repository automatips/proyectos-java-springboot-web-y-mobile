/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "cdr")
public class CdrPersistent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Size(max = 150)
    @Column(name = "origination")
    private String origination;
    @Size(max = 150)
    @Column(name = "origination_caller_id")
    private String originationCallerId;
    @Size(max = 150)
    @Column(name = "origination_ip")
    private String originationIp;
    @Size(max = 150)
    @Column(name = "destination")
    private String destination;
    @Size(max = 150)
    @Column(name = "destination_ip")
    private String destinationIp;
    @Size(max = 255)
    @Column(name = "domain_name")
    private String domainName;
    @Size(max = 150)
    @Column(name = "bridge_hangup_cause")
    private String bridgeHangupCause;
    @Size(max = 150)
    @Column(name = "hangup_cause")
    private String hangupCause;
    @Column(name = "duration")
    private Integer duration;
    @Column(name = "billsec")
    private Integer billsec;
    @Column(name = "progresssec")
    private Integer progresssec;
    @Column(name = "answersec")
    private Integer answersec;
    @Column(name = "waitsec")
    private Integer waitsec;
    @Basic(optional = false)
    @NotNull
    @Column(name = "secure")
    private boolean secure;
    @Size(max = 50)
    @Column(name = "wings_plan")
    private String wingsPlan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "wings_internal")
    private boolean wingsInternal;
    @Size(max = 250)
    @Column(name = "user_agent")
    private String userAgent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nat_detected")
    private boolean natDetected;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Double cost;
    @Column(name = "real_cost")
    private Double realCost;
    @Column(name = "destination_id")
    private Long destinationId;
    @Column(name = "service_id")
    private Long serviceId;
    @Column(name = "client_service_id")
    private Long clientServiceId;
    @Column(name = "minutes")
    private int minutes = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getOrigination() {
        return origination;
    }

    public void setOrigination(String origination) {
        this.origination = origination;
    }

    public String getOriginationCallerId() {
        return originationCallerId;
    }

    public void setOriginationCallerId(String originationCallerId) {
        this.originationCallerId = originationCallerId;
    }

    public String getOriginationIp() {
        return originationIp;
    }

    public void setOriginationIp(String originationIp) {
        this.originationIp = originationIp;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationIp() {
        return destinationIp;
    }

    public void setDestinationIp(String destinationIp) {
        this.destinationIp = destinationIp;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getBridgeHangupCause() {
        return bridgeHangupCause;
    }

    public void setBridgeHangupCause(String bridgeHangupCause) {
        this.bridgeHangupCause = bridgeHangupCause;
    }

    public String getHangupCause() {
        return hangupCause;
    }

    public void setHangupCause(String hangupCause) {
        this.hangupCause = hangupCause;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getBillsec() {
        return billsec;
    }

    public void setBillsec(Integer billsec) {
        this.billsec = billsec;
    }

    public Integer getProgresssec() {
        return progresssec;
    }

    public void setProgresssec(Integer progresssec) {
        this.progresssec = progresssec;
    }

    public Integer getAnswersec() {
        return answersec;
    }

    public void setAnswersec(Integer answersec) {
        this.answersec = answersec;
    }

    public Integer getWaitsec() {
        return waitsec;
    }

    public void setWaitsec(Integer waitsec) {
        this.waitsec = waitsec;
    }

    public boolean getSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public String getWingsPlan() {
        return wingsPlan;
    }

    public void setWingsPlan(String wingsPlan) {
        this.wingsPlan = wingsPlan;
    }

    public boolean getWingsInternal() {
        return wingsInternal;
    }

    public void setWingsInternal(boolean wingsInternal) {
        this.wingsInternal = wingsInternal;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public boolean getNatDetected() {
        return natDetected;
    }

    public void setNatDetected(boolean natDetected) {
        this.natDetected = natDetected;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getRealCost() {
        return realCost;
    }

    public void setRealCost(Double realCost) {
        this.realCost = realCost;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public Long getClientServiceId() {
        return clientServiceId;
    }

    public void setClientServiceId(Long clientServiceId) {
        this.clientServiceId = clientServiceId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getMinutes() {
        return minutes;
    }

}
