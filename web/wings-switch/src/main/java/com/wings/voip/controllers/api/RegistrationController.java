/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.model.requests.RegisterRequest;
import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientDevice;
import com.wings.voip.domain.sw.entities.ClientPendingAction;
import com.wings.voip.domain.sw.entities.LocalizationCountry;
import com.wings.voip.domain.sw.entities.Property;
import com.wings.voip.domain.sw.enums.PropertyCategories;
import com.wings.voip.domain.sw.repos.ClientContactRepository;
import com.wings.voip.domain.sw.repos.ClientDeviceRepository;
import com.wings.voip.domain.sw.repos.ClientPendingActionRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.PropertiesRepository;
import com.wings.voip.model.GenericResponse;
import com.wings.voip.model.requests.RegisterResponse;
import com.wings.voip.services.ClientServiceService;
import com.wings.voip.utils.EncryptUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author seba
 */
@Controller
public class RegistrationController {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientDeviceRepository clientDeviceRepository;

    @Autowired
    private PropertiesRepository propertiesRepository;

    @Autowired
    private ClientServiceService clientServiceService;

    @Autowired
    private ClientPendingActionRepository clientPendingActionRepository;

    @Autowired
    private ClientContactRepository clientContactRepository;

    private final Logger log = LoggerFactory.getLogger(RegistrationController.class);

    /**
     *
     * @param request
     * @param appId
     * @param accessToken
     * @param registerRequest
     * @return
     */
    @RequestMapping(value = {"/api/clients/register"}, method = RequestMethod.POST)
    public ResponseEntity<RegisterResponse> register(HttpServletRequest request,
            @RequestParam(required = false, value = "app_id") String appId,
            @RequestParam(required = false, value = "access_token") String accessToken,
            @RequestBody RegisterRequest registerRequest) {

        LocalizationCountry country = clientServiceService.getCountry(registerRequest.getLogin());
        if (country == null) {
            return ResponseEntity.badRequest().header("error", "Country not available").build();
        }

        Client client = clientRepository.findByLogin(registerRequest.getLogin());
        if (client == null) {
            client = new Client();
            client.setBalance(0.0);
            client.setLogin(registerRequest.getLogin());
            client.setCreationDate(new Date());
        }

        try {
            client.setMcc(Integer.parseInt(registerRequest.getMcc()));
            client.setMnc(Integer.parseInt(registerRequest.getMnc()));
        } catch (Exception e) {
        }
        client.setCountryIso(country.getCountryCode());
        client.setCountryPrefix(country.getCountryPrefix());

        client.setLocale(registerRequest.getLocale());
        client.setToken(EncryptUtils.decrypt(registerRequest.getToken()));

        clientRepository.save(client);

        //set me free on every client that has me as contact
        clientContactRepository.setMeFreeOnContacts(client.getLogin());

        List<ClientDevice> devices = clientDeviceRepository.findByClientId(client.getId());
        //no tiene dispositivos, creamos uno nuevo
        if (devices.isEmpty()) {

            ClientDevice device = fillDevice(new ClientDevice(), registerRequest);
            device.setClientId(client.getId());

            clientDeviceRepository.save(device);

        } else {

            boolean updated = false;
            for (ClientDevice device : devices) {

                //si ya existe actualizamos y marcamos el resto como inactivos
                if (device.getDeviceSerialNumber().equals(registerRequest.getSerialNumber())) {

                    device = fillDevice(device, registerRequest);
                    clientDeviceRepository.save(device);
                    updated = true;

                } else {
                    if (device.getDeviceOs().equals("and")) {
                        device.setActive(false);
                    }
                    clientDeviceRepository.save(device);
                }
            }

            //si no existia, insertamos
            if (!updated) {
                ClientDevice device = fillDevice(new ClientDevice(), registerRequest);
                device.setClientId(client.getId());
                clientDeviceRepository.save(device);
            }
        }

        //Inicializo servicios del cliente
        clientServiceService.initClient(client.getId());

        //Armo el response
        RegisterResponse response = new RegisterResponse();
        response.setAccountStatus(clientServiceService.getAccountStatus(client.getId(), null));

        //provisioning
        Map<String, String> provisioning = new TreeMap<>();
        List<Property> props = propertiesRepository.findByPropCategory(PropertyCategories.provisioning.name());
        props.forEach((prop) -> {
            provisioning.put(prop.getPropName(), prop.getPropValue());
        });
        response.setProvisioning(provisioning);

        //send push to all my contacts to update contacts info
        ClientPendingAction clientPendingAction = new ClientPendingAction();
        clientPendingAction.setAction("register");
        clientPendingAction.setCompleted(false);
        clientPendingAction.setCompletionDate(null);
        clientPendingAction.setCreationDate(new Date());
        clientPendingAction.setDisabled(false);
        clientPendingAction.setFailureCount(0);
        clientPendingAction.setLogin(client.getLogin());
        clientPendingAction.setPushToken(null);
        clientPendingAction.setSendCount(0);
        clientPendingAction.setClientId(client.getId());
        clientPendingActionRepository.save(clientPendingAction);

        return ResponseEntity.ok(response);
    }

    /**
     *
     * @param request
     * @param appId
     * @param accessToken
     * @param registerRequest
     * @return
     */
    @RequestMapping(value = {"/api/clients/updateToken"}, method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> updateUserInfo(HttpServletRequest request,
            @RequestParam(required = false, value = "app_id") String appId,
            @RequestParam(required = false, value = "access_token") String accessToken,
            @RequestBody RegisterRequest registerRequest) {

        GenericResponse resp = new GenericResponse();

        /*LocalizationCountry country = clientServiceService.getCountry(registerRequest.getLogin());
        if (country == null) {
            resp.setCode("400");
            resp.setError("Country not available");
            log.error("Error updating token, country not available");
            return ResponseEntity.ok(resp);
        }*/

        Client client = clientRepository.findByLogin(registerRequest.getLogin());
        if (client == null) {
            resp.setCode("400");
            resp.setError("Client not found");
            log.error("Error updating token, client not found " + registerRequest.getLogin());
            return ResponseEntity.ok(resp);
        }

        ClientDevice device = clientDeviceRepository.findFirstByClientIdAndDeviceSerialNumber(client.getId(), registerRequest.getSerialNumber());
        if (device == null) {
            resp.setCode("400");
            resp.setError("Device not found");
            log.error("Error updating token, device not found " + registerRequest.getSerialNumber());
            return ResponseEntity.ok(resp);
        }
        device.setPushToken(registerRequest.getPushToken());
        clientDeviceRepository.save(device);

        resp.setCode("200");
        resp.setMessage("Token update success");
        return ResponseEntity.ok(resp);
    }

    /**
     *
     * @param device
     * @param registerRequest
     * @return
     */
    private ClientDevice fillDevice(ClientDevice device, RegisterRequest registerRequest) {

        device.setActive(true);
        device.setAppVersion(registerRequest.getVersionCode() + " " + registerRequest.getVersionName());
        device.setDeviceId(registerRequest.getDeviceId());
        device.setDeviceModel(registerRequest.getDeviceModel());
        device.setDeviceName(registerRequest.getDeviceName());
        device.setDeviceOs(registerRequest.getOs());
        device.setDeviceSerialNumber(registerRequest.getSerialNumber());
        device.setDeviceUdid(registerRequest.getUdid());
        device.setImei1(registerRequest.getImei1());
        device.setImei2(registerRequest.getImei2());
        device.setPushToken(registerRequest.getPushToken());
        device.setPushInstanceId(registerRequest.getInstanceId());
        if (device.getCreationDate() == null) {
            device.setCreationDate(new Date());
        }
        return device;
    }

}
