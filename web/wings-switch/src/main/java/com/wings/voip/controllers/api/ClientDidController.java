/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientDid;
import com.wings.voip.domain.sw.entities.ClientService;
import com.wings.voip.domain.sw.entities.DidCity;
import com.wings.voip.domain.sw.entities.DidCountry;
import com.wings.voip.domain.sw.repos.ClientDidRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientServiceRepository;
import com.wings.voip.domain.sw.repos.DidCityRepository;
import com.wings.voip.domain.sw.repos.DidCountryRepository;
import com.wings.voip.domain.sw.repos.ServiceRepository;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.DidCityDTO;
import com.wings.voip.model.DidCountryDTO;
import com.wings.voip.model.DidNumberAvailableDTO;
import com.wings.voip.services.ClientServiceService;
import com.wings.voip.services.DidWWService;
import com.wings.voip.utils.EncryptUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ClientDidController {

    private final Logger log = LoggerFactory.getLogger(ClientDidController.class);

    @Autowired
    private DidCountryRepository didCountryRepository;

    @Autowired
    private DidCityRepository didCityRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private DidWWService didWWService;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientServiceRepository clientServiceRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ClientDidRepository clientDidRepository;

    @Autowired
    private ClientServiceService clientServiceService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/did/countries"}, method = {RequestMethod.GET})
    public ResponseEntity<List<DidCountryDTO>> getCountries(Model model, HttpServletRequest request) {

        List<DidCountryDTO> ret = new LinkedList<>();
        List<DidCountry> countries = didCountryRepository.findByEnabled(Boolean.TRUE);

        countries.forEach((country) -> {
            DidCountryDTO dto = new DidCountryDTO();
            String name = country.getName();
            try {
                name = messageSource.getMessage(country.getIso(), null, request.getLocale());
            } catch (Exception e) {
            }
            dto.setName(name);
            dto.setId(country.getId());
            ret.add(dto);
        });

        //sort after translation
        Collections.sort(ret, new Comparator<DidCountryDTO>() {
            @Override
            public int compare(DidCountryDTO o1, DidCountryDTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        //empty one to prevent firing event on android
        DidCountryDTO choose = new DidCountryDTO();
        String nameFake = "Seleccione...";
        try {
            nameFake = messageSource.getMessage("choose", null, request.getLocale());
        } catch (Exception e) {
        }
        choose.setName(nameFake);
        choose.setId(-1l);
        ((LinkedList) ret).addFirst(choose);

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/did/cities"}, method = {RequestMethod.GET})
    public ResponseEntity<List<DidCityDTO>> getCities(Model model, HttpServletRequest request) {

        String countryId = request.getParameter("countryId");

        List<DidCityDTO> ret = new LinkedList<>();
        List<DidCity> cities = didCityRepository.findByCountryIdAndEnabled(Long.valueOf(countryId), Boolean.TRUE);

        //empty one to prevent firing event on android
        DidCityDTO choose = new DidCityDTO();
        String nameFake = "Seleccione...";
        try {
            nameFake = messageSource.getMessage("choose", null, request.getLocale());
        } catch (Exception e) {
        }
        choose.setName(nameFake);
        choose.setId(-1l);
        ret.add(choose);

        cities.forEach((city) -> {
            DidCityDTO dto = new DidCityDTO();
            dto.setName(city.getName());
            dto.setId(city.getId());
            ret.add(dto);
        });

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/did/numbers/available"}, method = {RequestMethod.GET})
    public ResponseEntity<List<DidNumberAvailableDTO>> getAvailableNumbers(Model model, HttpServletRequest request) {

        String countryId = request.getParameter("countryId");
        String cityId = request.getParameter("cityId");

        if (countryId != null) {
            if (countryId.equals("-1")) {
                countryId = null;
            } else {
                DidCountry c = didCountryRepository.findOne(Long.valueOf(countryId));
                if (c != null) {
                    countryId = c.getRemoteId();
                }
            }
        }

        if (cityId != null) {
            if (cityId.equals("-1")) {
                cityId = null;
            } else {
                DidCity c = didCityRepository.findOne(Long.valueOf(cityId));
                if (c != null) {
                    cityId = c.getRemoteId();
                }
            }
        }

        if (countryId == null && cityId == null) {
            List<DidNumberAvailableDTO> ret = new ArrayList<>();
            DidNumberAvailableDTO dto = new DidNumberAvailableDTO();
            String name = "No available numbers";
            try {
                name = messageSource.getMessage("noNumberAvailable", null, request.getLocale());
            } catch (Exception e) {
            }
            dto.setNumber(name);
            dto.setRemoteId("-1");
            ret.add(dto);
            return ResponseEntity.ok(ret);
        }

        List<DidNumberAvailableDTO> ret = didWWService.getAvailableDids(countryId, cityId);

        if (ret.isEmpty()) {
            DidNumberAvailableDTO dto = new DidNumberAvailableDTO();

            String name = "No available numbers";
            try {
                name = messageSource.getMessage("noNumberAvailable", null, request.getLocale());
            } catch (Exception e) {
            }
            dto.setNumber(name);
            dto.setRemoteId("-1");
            ret.add(dto);
        }

        return ResponseEntity.ok(ret);
    }

    @RequestMapping(value = {"/api/did/numbers/setup"}, method = {RequestMethod.POST})
    public ResponseEntity<AccountStatus> postDidNumberBuy(Model model, HttpServletRequest request) {

        String login = request.getParameter("login");
        String password = request.getParameter("token");
        String appId = request.getParameter("app_id");
        String accessToken = request.getParameter("access_token");
        password = EncryptUtils.decrypt(password);

        String didNumber = request.getParameter("didNumber");
        String didId = request.getParameter("didId");
        String didRemoteId = request.getParameter("didRemoteId");

        //Authorize login
        Client client = clientRepository.findByLogin(login);
        if (client == null) {
            return ResponseEntity.badRequest().build();
        }

        if (!client.getToken().equals(password)) {
            return ResponseEntity.badRequest().build();
        }

        //authorize app
        if (!(appId.equals("a2dc3b1c57d64fe9840c5cb") && accessToken.equals("6f4390a15e9165f37ad6d96e0c63f68fb1cc1f13"))) {
            return ResponseEntity.badRequest().build();
        }

        //last and not least, authorize request
        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("Basic YW5kcm9pZF9hcHA6d0c2REVzUzM0clQ1OUE5Vg==")) {
            return ResponseEntity.badRequest().build();
        }

        ClientDid did = clientDidRepository.findOne(Long.parseLong(didId));
        if (did == null) {
            return ResponseEntity.badRequest().build();
        }

        //check if the user has the service
        ClientService clientService = clientServiceRepository.findOne(did.getClientServiceId());

        did.setDidNumber(didNumber);
        did.setLocalStatus("setup");
        did.setRemoteStatus("pending");
        did.setCity(null);
        did.setCountry(null);

        try {
            did = didWWService.postDidReservation(didRemoteId, did);
        } catch (Exception e) {
            log.error("Error reserving did ", e);
            return ResponseEntity.badRequest().header("error", "not_created").build();
        }

        if (did.getCreationDate() == null) {
            return ResponseEntity.badRequest().header("error", "not_created").build();
        }

        if (clientService.getDidNumber() == null || clientService.getDidNumber().isEmpty()) {
            clientService.setDidNumber(did.getDidNumber());
        } else {
            clientService.setDidNumber(clientService.getDidNumber() + "," + did.getDidNumber());
        }

        clientDidRepository.save(did);
        clientServiceRepository.save(clientService);

        AccountStatus status = clientServiceService.getAccountStatus(client.getId(), client.getLocale());

        return ResponseEntity.ok(status);
    }

}
