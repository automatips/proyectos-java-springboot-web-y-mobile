/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.clients;

/**
 *
 * @author seba
 */
public class ValidationRequest {

    private String id;
    private String type;
    private String pin_hash;
    private String cli_prefix;
    private ValidationInfo validation_info;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

   
    public String getCli_prefix() {
        return cli_prefix;
    }

    public void setCli_prefix(String cli_prefix) {
        this.cli_prefix = cli_prefix;
    }

    public String getPin_hash() {
        return pin_hash;
    }

    public void setPin_hash(String pin_hash) {
        this.pin_hash = pin_hash;
    }

    public ValidationInfo getValidation_info() {
        return validation_info;
    }

    public void setValidation_info(ValidationInfo validation_info) {
        this.validation_info = validation_info;
    }

  

}
