/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.fs;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.services.BlockIpService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author seba
 */
@Controller
public class DirectoryController {

    private final Logger log = LoggerFactory.getLogger(DirectoryController.class);

    @Autowired
    private TemplateEngine xmlTemplateEngine;
    @Autowired
    private ClientRepository clientsRepository;
    @Autowired
    private BlockIpService blockIpService;

    /**
     * Freeswitch authutorization
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/fs/directory"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String register(HttpServletRequest request) {

        String clientIp = request.getParameter("ip");
        Context context = new Context();

        //RequestUtils.debugRequestParams(request);

        //Check fs ip
        String fsIp = request.getParameter("FreeSWITCH-IPv4");
        if (fsIp == null || !fsIp.equals("10.10.39.4") && !fsIp.equals("79.143.182.239")) {
            log.error("Rejecting request - ip does not match " + fsIp);
            blockIpService.addBlockIp(clientIp);
            return xmlTemplateEngine.process("xml/directory", context);
        }

        String user = request.getParameter("user");
        String domain = request.getParameter("domain");

        //TEST!!!!
        /*if (user.equals("test1") || user.equals("test2")) {
            context.setVariable("user", user);
            context.setVariable("domain", domain);
            context.setVariable("password", user.replaceAll("test", "pass"));
            return xmlTemplateEngine.process("xml/directory", context);
        }*/

        //check user agent
        String userAgent = request.getParameter("sip_user_agent");
        if (userAgent != null && userAgent.contains("DIDWW SBC node") && user.equals("didww")) {

            context.setVariable("user", user);
            context.setVariable("domain", domain);
            context.setVariable("ha1", DigestUtils.md5Hex(user + ":" + domain + ":7EQUB2FnQhUHrAXN"));
            return xmlTemplateEngine.process("xml/directory", context);

        } else if (userAgent != null && !userAgent.contains("WingsPhone")) {
            
            log.error("Rejecting request - user agent does not match " + userAgent);
            blockIpService.addBlockIp(clientIp);
            return xmlTemplateEngine.process("xml/directory", context);
        }

        Client client = clientsRepository.findByLogin(user);
        if (client == null) {
            log.error("Rejecting request - user does not exists " + user);
            blockIpService.addBlockIp(clientIp);
        } else {
            context.setVariable("user", user);
            context.setVariable("domain", domain);
            context.setVariable("ha1", DigestUtils.md5Hex(user + ":" + domain + ":" + client.getToken()));
        }

        //log.error("Directory request user " + user + " domain " + domain + " client ip " + clientIp);

        String response = xmlTemplateEngine.process("xml/directory", context);

        return response;
    }

}
