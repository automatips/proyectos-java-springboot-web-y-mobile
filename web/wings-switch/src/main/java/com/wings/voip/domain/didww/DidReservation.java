/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author seba
 */
public class DidReservation {

    private String type = "did_reservations";
    private Map<String, DidReservationData> relationships = new HashMap<>();

    public DidReservation(DidReservationData data) {
        relationships.put("available_did", data);
    }

    public void setRelationships(Map<String, DidReservationData> relationships) {
        this.relationships = relationships;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, DidReservationData> getRelationships() {
        return relationships;
    }

    public String getType() {
        return type;
    }

}
