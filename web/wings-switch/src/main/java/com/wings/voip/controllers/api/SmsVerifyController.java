/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.services.SmsService;
import com.wings.voip.utils.NumberUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
@RequestMapping(value = "api/")
public class SmsVerifyController {

    @Autowired
    private SmsService smsService;

    @Autowired
    private MessageSource messageSource;

    private final Map<String, Integer> counters = new HashMap<>();
    private final Map<String, String> codes = new HashMap<>();
    private final Logger log = LoggerFactory.getLogger(SmsVerifyController.class);

    @RequestMapping(value = {"/verify/sms"}, method = RequestMethod.POST)
    public ResponseEntity<String> requestSmsVerification(Model model, HttpServletRequest request) {

        String login = request.getParameter("login");
        String lang = request.getParameter("lang");

        log.error("Requesting sms to " + login + " using " + lang);
        int current = 1;
        if (counters.containsKey(login)) {
            current = counters.get(login);
            current = current + 1;
            counters.put(login, current);
        } else {
            counters.put(login, current);
        }

        if (current >= 5) {
            log.error("Rejecting request, too many request " + login);
            return ResponseEntity.badRequest().header("error", "too many requests").build();
        }

        //send sms
        String random = NumberUtils.getNumberRandomString(6);
        String message = messageSource.getMessage("sms.validationMessage", new String[]{random}, new Locale(lang));
        smsService.sendSms(login, message);
        codes.put(login, random);

        return ResponseEntity.ok(random);
    }

    @RequestMapping(value = {"/verify/sms/confirm"}, method = RequestMethod.POST)
    public ResponseEntity<String> confirmSmsVerification(Model model, HttpServletRequest request) {

        String login = request.getParameter("login");
        String code = request.getParameter("code");

        if (codes.containsKey(login) && codes.get(login).equals(code)) {
            return ResponseEntity.ok("verified");
        }

        return ResponseEntity.ok("error");
    }

    @Scheduled(initialDelay = 10000, fixedRate = 600000)
    public void cleanUpCounters() {

        List<String> toClean = new ArrayList<>();
        counters.entrySet().forEach((entry) -> {
            String key = entry.getKey();
            int value = entry.getValue();
            if (value >= 3) {
                toClean.add(key);
            }
        });

        toClean.forEach((string) -> {
            counters.remove(string);
        });
    }

}
