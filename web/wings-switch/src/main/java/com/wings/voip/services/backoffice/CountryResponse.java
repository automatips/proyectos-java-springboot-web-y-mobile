package com.wings.voip.services.backoffice;

import java.util.List;

/**
 *
 * @author lucas
 */
public class CountryResponse {

    private List<CountryDto> countries;

    public List<CountryDto> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryDto> countries) {
        this.countries = countries;
    }

}
