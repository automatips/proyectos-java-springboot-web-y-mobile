$(document).ready(function ($) {

    $('#form-submit').click(function (e) {
        e.preventDefault();
        var id = $("#item").val();
        var url = "/views/clients/balance/" + id + "/update";
        var credit = $("#credit").val();


        $.post(url, {id: id, credit: credit}, function (response) {

            $('#search-button').click();

            location.reload();
        });

    });
});
