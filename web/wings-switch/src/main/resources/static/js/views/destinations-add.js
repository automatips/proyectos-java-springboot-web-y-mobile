$(document).ready(function ($) {
    
    $('#destination-add-save').click(function (e) {
        e.preventDefault();
        var url = "/views/destinations/add";
        $.post(url, $('#add-destination-form').serialize(), function (response) {
            location.reload();
        });
    });
});