$(document).ready(function ($) {


    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'comment', $('#comment').val());
        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());
        setUpUrl(url);
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'view-contacts') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var url = "/views/clients/contacts/" + id;

            loadInDialog(url, "Contacts for " + name, false, true);

        } else if (action === 'add-credit') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Añadir credito a ' + name;
            var url = '/views/clients/balance/' + id + "/credit";

            loadInDialog(url, title, false, false);
        } else if (action === 'add-credit-details') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Añadir credito a ' + name;
            var url = '/views/clientDetails/balance/' + id + "/credit";

            loadInDialog(url, title, false, false);
        } else if (action === 'view-details') {

            var url = $(e.target).attr('href');            
            location.href = url;
            location.reload();
            
            
        } else if(action==='view-clients') {

            var url = $(e.target).attr('href');
            location.href = url;
            location.reload();
        }
    });


});
