$(document).ready(function ($) {

    $('#form-submit-list').click(function (e) {
        e.preventDefault();
        var url = "/views/servicesDestinationList/post";
        var name = $("#name").val();
        var countryCode = $("#countryCode").val();
        var areaCode = $("#areaCode").val();
        var priceDestination = $("#priceDestination").val();
        var priceListDestinationSelected = $('#priceListDestination').val();

        $.post(url, {
            name: name,
            countryCode: countryCode,
            areaCode: areaCode,
            priceDestination: priceDestination,
            priceListDestinationSelected: priceListDestinationSelected
        }, function () {

            $('#search-button-destination').click();

            location.reload();
        });

    });
});
