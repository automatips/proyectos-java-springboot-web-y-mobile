$(document).ready(function ($) {
    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        setUpUrl(url);
    });
    $('#form-submit-list-price-edit').click(function (e) {
        e.preventDefault();
        var url = "/views/servicesListPriceEdit/post";
        var name = $("#name").val();
        var description = $("#description").val();
        var validFrom = $("#validFrom").val();
        var validTo = $("#validTo").val();
        var active =$("#active").is(':checked');
        var id = $(e.target).data('id');

        $.post(url, {
            name: name,
            description: description,
            validFrom: validFrom,
            validTo: validTo,
            active: active,
            id:id
        }, function (response) {

            $('#search-button-destination').click();

            location.reload();
        });

    });
});