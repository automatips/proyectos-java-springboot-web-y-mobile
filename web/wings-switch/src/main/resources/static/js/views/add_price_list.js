$(document).ready(function ($) {
    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        setUpUrl(url);
    });
    $('#form-submit-list-price').click(function (e) {
        e.preventDefault();
        var url = "/views/servicesListPrice/post";
        var name = $("#name").val();
        var description = $("#description").val();
        var validFrom = $("#validFrom").val();
        var validTo = $("#validTo").val();
        var active =$("#active").is(':checked');

        $.post(url, {
            name: name,
            description: description,
            validFrom: validFrom,
            validTo: validTo,
            active: active
        }, function (response) {

            $('#search-button-destination').click();

            location.reload();
        });

    });
});