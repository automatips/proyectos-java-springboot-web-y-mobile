$(document).ready(function ($) {

    $('#send-push-form-button').click(function (e) {

        e.preventDefault();

        var id = $('#deviceId').val();
        var url = 'view/clientDetails/' + id + '/push/';

        $.post(url, $('#send-push-form').serialize(), function (response) {
            location.reload();
        }).fail(function (xhr, status, error) {
            notifyError(xhr.responseText);
        });

    });
});

