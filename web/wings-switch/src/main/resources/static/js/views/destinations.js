$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'priceListId', $('#priceListId').val());
        setUpUrl(url);
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'destination-add') {

            var url = "/views/destinations/add";
            loadInDialog(url, "Crear nuevo destino", null, null);

        } else if (action === 'destination-import') {

            var url = "/views/destinations/import";
            loadInDialog(url, "Importar nuevos destinos", null, null);

        } else if (action === 'edit-destination') {
            bootbox.alert({title: "nada", message: "nada todavia"});
        } else if (action === 'delete-destination') {
            bootbox.alert({title: "nada", message: "nada todavia"});
        }
    });

});
