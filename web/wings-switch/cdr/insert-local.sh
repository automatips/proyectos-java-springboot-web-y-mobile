#!/bin/bash

cd /home/seba/projects/wings/java/voip/switch/cdr

#for CDR in $(find . -name "*.cdr.xml"); do
#    echo "Resubmitting $(basename $CDR .cdr.xml)..."
#    echo -n 'cdr='$(cat $CDR) | \
#        curl -sS -X POST -f -H "Content-type: text/xml" -d @- http://localhost:8098/api/cdr > /dev/null \
#            && echo "OK"
#done


for CDR in $(find $CDRDIR -name "*.cdr.xml"); do
        echo "Resubmitting $(basename $CDR .cdr.xml)..."
        curl -sS --request POST -f -H "Content-type: text/xml" -data cdr@$CDR http://localhost:8098/api/cdr  -vvv > /dev/null && echo "OK" 
done