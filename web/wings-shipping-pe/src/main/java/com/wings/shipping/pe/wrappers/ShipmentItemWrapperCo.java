/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.shipping.pe.wrappers;

/**
 *
 * @author seba
 */
public class ShipmentItemWrapperCo {

    private Long id;
    private Long shipmentId;
    private String itemEan;
    private String itemName;
    private String itemDescription;
    private Integer weigth;
    private Integer width;
    private Integer height;
    private Integer length;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getItemEan() {
        return itemEan;
    }

    public void setItemEan(String itemEan) {
        this.itemEan = itemEan;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getWeigth() {
        return weigth;
    }


    public void setWeigth(Integer weigth) {
        this.weigth = weigth;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

}
