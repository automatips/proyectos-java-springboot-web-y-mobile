/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.androidrat.web;

import com.wings.androidrat.service.SerialService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class IndexController {

    @Autowired
    private SerialService serialService;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String getIndex(Model model, HttpServletRequest request) {

        String license = (String) request.getSession().getAttribute("license");

        boolean esnulo = license == null;

        model.addAttribute("license", license);
        model.addAttribute("showForm", esnulo);

        return "index";
    }

    @RequestMapping(value = {"/postCode"}, method = RequestMethod.POST)
    public String postCode(Model model, HttpServletRequest request) {
        String pinCode = request.getParameter("pinCode");
        if (pinCode != null) {
            pinCode = pinCode.toUpperCase();
        }
        String license = serialService.getSerialByCode(pinCode);
        request.getSession().setAttribute("license", license);
        return "redirect:/activacionwingsbook/";
    }

    @RequestMapping(value = {"/cleanCode"}, method = RequestMethod.POST)
    public String cleanCode(Model model, HttpServletRequest request) {
        request.getSession().removeAttribute("license");
        return "redirect:/activacionwingsbook/";
    }

}
