package com.wings.androidrat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AndroidratApplication {

	public static void main(String[] args) {
		SpringApplication.run(AndroidratApplication.class, args);
	}

}
