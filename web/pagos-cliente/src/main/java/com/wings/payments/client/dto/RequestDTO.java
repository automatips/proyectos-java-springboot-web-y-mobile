/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.client.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seba
 */
public class RequestDTO {

    private String requesterName;
    private long orderNumber;
    private String title;
    private String description;
    private Double amount;
    private Double comsDiscountAmount;
    private String currency;
    private String country;
    private String callbackUrl;
    private String cancelUrl;
    private String image;
    private String preferredGateway;
    private boolean excludeBacs;
    private String paymentType;
    private String billingFirstName;
    private String billingLastName;
    private String billingEmail;
    private String billingAddress;
    private String billingCity;
    private String billingCountry;
    private String billingState;

    private List<RequestDTOExtra> extras = new ArrayList<>();

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCancelUrl() {
        return cancelUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPreferredGateway() {
        return preferredGateway;
    }

    public void setPreferredGateway(String preferredGateway) {
        this.preferredGateway = preferredGateway;
    }

    public boolean isExcludeBacs() {
        return excludeBacs;
    }

    public void setExcludeBacs(boolean excludeBacs) {
        this.excludeBacs = excludeBacs;
    }
   
    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public List<RequestDTOExtra> getExtras() {
        return extras;
    }

    public void setExtras(List<RequestDTOExtra> extras) {
        this.extras = extras;
    }

    public Double getComsDiscountAmount() {
        return comsDiscountAmount;
    }

    public void setComsDiscountAmount(Double comsDiscountAmount) {
        this.comsDiscountAmount = comsDiscountAmount;
    }

    
}
