/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.cdr.repo;

import com.wings.cdr.entity.Cdr;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface CdrRepository extends JpaRepository<Cdr, Long> {

}
