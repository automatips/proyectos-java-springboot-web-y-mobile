
package com.wings.api.models;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ContactsPacketDto {
    
    ArrayList<ContactDto> list;
    
//    public ContactsPacket() {
//		
//    }
//
//    public ContactsPacket(ArrayList<Contact> ar) {
//            list = ar;
//    }

    public byte[] build() {
            try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bos);
                    out.writeObject(list);
                    return bos.toByteArray();
            } catch (IOException e) {
                    return null;
            }
    }

    public void parse(byte[] packet) {
            ByteArrayInputStream bis = new ByteArrayInputStream(packet);
            ObjectInputStream in;
            try {
                    in = new ObjectInputStream(bis);
                    list = (ArrayList<ContactDto>) in.readObject();
            } catch (Exception e) {
            }
    }

}
