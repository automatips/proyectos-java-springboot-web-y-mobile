
package com.wings.api.models;

import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ContactDto {
    
    private static final long serialVersionUID = -744071613945933264L;
    private long id;
    private int times_contacted;
    private long last_time_contacted;
    private String display_name;
    private int starred;
    private ArrayList<String> phones;
    private ArrayList<String> emails;
    private ArrayList<String> notes;
    private String street;
    private String city;
    private String region;
    private String postalcode;
    private String country;
    private int type_addr;
    private ArrayList<String> messaging;
    private String OrganisationName;
    private String OrganisationStatus; //manager ..
    private byte[] photo;
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the times_contacted
     */
    public int getTimes_contacted() {
        return times_contacted;
    }

    /**
     * @param times_contacted the times_contacted to set
     */
    public void setTimes_contacted(int times_contacted) {
        this.times_contacted = times_contacted;
    }

    /**
     * @return the last_time_contacted
     */
    public long getLast_time_contacted() {
        return last_time_contacted;
    }

    /**
     * @param last_time_contacted the last_time_contacted to set
     */
    public void setLast_time_contacted(long last_time_contacted) {
        this.last_time_contacted = last_time_contacted;
    }

    /**
     * @return the display_name
     */
    public String getDisplay_name() {
        return display_name;
    }

    /**
     * @param display_name the display_name to set
     */
    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    /**
     * @return the starred
     */
    public int getStarred() {
        return starred;
    }

    /**
     * @param starred the starred to set
     */
    public void setStarred(int starred) {
        this.starred = starred;
    }

    /**
     * @return the phones
     */
    public ArrayList<String> getPhones() {
        return phones;
    }

    /**
     * @param phones the phones to set
     */
    public void setPhones(ArrayList<String> phones) {
        this.phones = phones;
    }

    /**
     * @return the emails
     */
    public ArrayList<String> getEmails() {
        return emails;
    }

    /**
     * @param emails the emails to set
     */
    public void setEmails(ArrayList<String> emails) {
        this.emails = emails;
    }

    /**
     * @return the notes
     */
    public ArrayList<String> getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(ArrayList<String> notes) {
        this.notes = notes;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the postalcode
     */
    public String getPostalcode() {
        return postalcode;
    }

    /**
     * @param postalcode the postalcode to set
     */
    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the type_addr
     */
    public int getType_addr() {
        return type_addr;
    }

    /**
     * @param type_addr the type_addr to set
     */
    public void setType_addr(int type_addr) {
        this.type_addr = type_addr;
    }

    /**
     * @return the messaging
     */
    public ArrayList<String> getMessaging() {
        return messaging;
    }

    /**
     * @param messaging the messaging to set
     */
    public void setMessaging(ArrayList<String> messaging) {
        this.messaging = messaging;
    }

    /**
     * @return the OrganisationName
     */
    public String getOrganisationName() {
        return OrganisationName;
    }

    /**
     * @param OrganisationName the OrganisationName to set
     */
    public void setOrganisationName(String OrganisationName) {
        this.OrganisationName = OrganisationName;
    }

    /**
     * @return the OrganisationStatus
     */
    public String getOrganisationStatus() {
        return OrganisationStatus;
    }

    /**
     * @param OrganisationStatus the OrganisationStatus to set
     */
    public void setOrganisationStatus(String OrganisationStatus) {
        this.OrganisationStatus = OrganisationStatus;
    }

    /**
     * @return the photo
     */
    public byte[] getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}
