package com.wings.payments.services;

import com.wings.payments.domain.login.ApplicationUser;
import com.wings.payments.domain.login.Role;
import com.wings.payments.domain.repos.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role findOne(ApplicationUser user) {
        return roleRepository.findByUserId(user.getId());
    }

    @Transactional
    public void save(Role role) {
        roleRepository.save(role);
    }
}
