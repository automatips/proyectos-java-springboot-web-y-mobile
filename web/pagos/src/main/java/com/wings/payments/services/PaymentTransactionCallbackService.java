package com.wings.payments.services;

import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.repos.PaymentTransactionCallbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentTransactionCallbackService{

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    public Page<PaymentTransactionCallback> findByPaymentTransactionId(Long id, Pageable pageable) {
        return paymentTransactionCallbackRepository.findByPaymentTransactionId(id, PageRequest.of(pageable.getPageNumber(),pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "id")));
    }

    public List<PaymentTransactionCallback> findByPaymentTransactionId(Long id) {
        return paymentTransactionCallbackRepository.findByPaymentTransactionId(id);
    }
}
