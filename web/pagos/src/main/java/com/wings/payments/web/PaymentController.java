/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.web;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.enums.GatewayTypes;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import com.wings.payments.gateways.PaymentGateway;
import com.wings.payments.gateways.PaymentGatewayFactory;
import com.wings.payments.gateways.impl.payu.PayUService;
import com.wings.payments.services.CallbackService;
import com.wings.payments.utils.RequestUtils;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class PaymentController {

    private final Logger log = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentGatewayFactory paymentGatewayService;

    @Autowired
    private PayUService payUService;

    @Autowired
    private CallbackService callbackService;

    /**
     * GET for selected gateway, this usually redirect to GW page
     *
     * @param model
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/t", method = RequestMethod.GET)
    public String getPaymentTransaction(Model model, HttpServletRequest request, HttpServletResponse response) {

        //prevent go back
        response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Expires", "0");

        PaymentTransaction transac = (PaymentTransaction) model.asMap().get("transac");
        if (transac == null) {
            return "redirect:/";
        }

        //get transacion elements
        Long giId = transac.getGatewayImplementantionId();
        Long prId = transac.getPaymentRequestId();
        GatewayImplementation gi = gatewayImplementationRepository.getOne(giId);
        PaymentRequest pr = paymentRequestRepository.getOne(prId);

        //if bacs, mark as pending, and show info
        if (gi.getGateway().getType().equals(GatewayTypes.bacs.name())) {
            transac.setLastStatus(PaymentStatus.PENDING.name());
            paymentTransactionRepository.save(transac);

            pr.setStatus(PaymentStatus.PENDING.name());
            pr.setStatusMessage("Transacción pendiente, esperando comprobante de transferencia");
            pr.setStatusShown(false);
            pr.setStatusDate(new Date());
            pr.setProcessor("bacs");
            paymentRequestRepository.save(pr);

            //send callback
            callbackService.postCallback(pr);
        }

        //aca pido que gateway es
        PaymentGateway pg = paymentGatewayService.getImplementation(gi);
        Map<String, String> formData = pg.getPaymentFormData(pr);
        model.addAttribute("template", pg.getTemplateName());
        model.addAttribute("formData", formData);

        request.getSession().removeAttribute("transac");

        //add attributes
        model.addAttribute("transac", transac);
        model.addAttribute("req", pr);
        model.addAttribute("gi", gi);

        return "transac_page";
    }

    /**
     * Post for selected GW, this redirect to GET page, we get here when user
     * chooses a GW
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/t", method = RequestMethod.POST)
    public String postPaymentTransaction(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        String requestId = request.getParameter("req_id");
        String gatewayId = request.getParameter("req_pg");

        Long gatewayImplementationId = -1l;
        try {
            gatewayImplementationId = Long.parseLong(gatewayId);
        } catch (NumberFormatException e) {
        }

        Long paymentRequestId = -1l;
        try {
            paymentRequestId = Long.parseLong(requestId);
        } catch (NumberFormatException e) {
        }

        //check if request id it's already on transactions
        PaymentTransaction transac = null;
        if (paymentRequestId != -1) {
            transac = paymentTransactionRepository.findByPaymentRequestId(paymentRequestId);
        }

        //if not exists create new one
        if (transac == null) {
            transac = new PaymentTransaction();
            transac.setPaymentRequestId(paymentRequestId);
        }

        transac.setGatewayImplementantionId(gatewayImplementationId);
        transac.setLastStatus(PaymentStatus.PROCESSING.name());
        paymentTransactionRepository.save(transac);

        //redirect
        redirectAttributes.addFlashAttribute("transac", transac);

        return "redirect:/t";
    }

    /**
     * This page is to get back from GW and redirect to /
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/tr", method = {RequestMethod.GET, RequestMethod.POST})
    public String getResponsePageRedirect(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        RequestUtils.debugRequestParams(request);
        RequestUtils.debugRequestHeaders(request);
        RequestUtils.debugRequestBody(request);

        Map<String, String> params = RequestUtils.getRequestParams(request);
        //payU
        if (params.get("referenceCode") != null && !params.get("referenceCode").isEmpty()) {
            payUService.processPayUResponse(request);
        }
        return "redirect:/";
    }

}
