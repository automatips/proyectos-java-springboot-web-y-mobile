/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.culqi;

import com.culqi.Culqi;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackValueRepository;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import com.wings.payments.gateways.PaymentGatewayFactory;
import com.wings.payments.services.CallbackService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CulqiService {

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentGatewayFactory paymentGatewayService;

    @Autowired
    private CallbackService callbackService;

    private final Logger log = LoggerFactory.getLogger(CulqiService.class);

    /**
     *
     * @param token
     * @param reqId
     */
    public void createCulqiCharge(String token, String reqId) {

        //get paymentRequest
        PaymentRequest payReq = paymentRequestRepository.getOne(Long.parseLong(reqId));

        //get tx
        PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(payReq.getId());

        //get implementation
        Long giId = ptx.getGatewayImplementantionId();
        GatewayImplementation gi = gatewayImplementationRepository.getOne(giId);
        CulqiPaymentGateway culqiPaymentGateway = (CulqiPaymentGateway) paymentGatewayService.getImplementation(gi);
        Map<String, String> formData = culqiPaymentGateway.getPaymentFormData(payReq);

        Culqi culqi = new Culqi();
        culqi.public_key = formData.get("public_key");
        culqi.secret_key = formData.get("private_key");

        try {
            Map<String, Object> charge = new HashMap<>();
            Map<String, Object> antifraudDetails = new HashMap<>();

            String address = payReq.getBillingAddress();
            if (address != null && address.length() > 100) {
                address = address.substring(0, 90);
            }
            antifraudDetails.put("address", address);
            antifraudDetails.put("address_city", payReq.getBillingCity());
            antifraudDetails.put("country_code", payReq.getBillingCountry());
            antifraudDetails.put("first_name", payReq.getBillingFirstName());
            antifraudDetails.put("last_name", payReq.getBillingLastName());
            //antifraudDetails.put("phone_number", payReq.getBillingAddress());

            Map<String, Object> metadata = new HashMap<>();
            metadata.put("order_id", payReq.getId());
            metadata.put("tx_id", ptx.getId());
            charge.put("amount", (int) (payReq.getAmount() * 100));
            charge.put("capture", true);
            charge.put("currency_code", payReq.getCurrency());

            String description = payReq.getDescription();
            if (description == null || description.isEmpty() || description.length() < 5) {
                description = payReq.getTitle() + " - " + payReq.getId() + "-" + payReq.getOrderNumber();
            }

            if (description.length() >= 80) {
                description = description.substring(0, 70) + "...";
            }

            charge.put("description", description);
            charge.put("email", payReq.getBillingEmail());
            charge.put("installments", 0);
            charge.put("antifraud_details", antifraudDetails);
            charge.put("metadata", metadata);
            charge.put("source_id", token);
            Map<String, Object> culqiResponse = culqi.charge.create(charge);

            if (culqiResponse.get("object") != null) {

                String status;
                String statusMessage;

                if (culqiResponse.get("object").toString().equals("error")) {
                    status = PaymentStatus.FAILED.name();

                    if (culqiResponse.get("user_message") != null) {
                        statusMessage = culqiResponse.get("user_message").toString().replaceAll("\\{", "").replaceAll("\\}", "");
                    } else if (culqiResponse.get("merchant_message") != null) {
                        statusMessage = culqiResponse.get("merchant_message").toString().replaceAll("\\{", "").replaceAll("\\}", "");
                    } else {
                        statusMessage = "Error desconocido";
                    }

                } else {

                    ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                    String badJson = culqiResponse.get("outcome").toString().replaceAll("=", ":");
                    badJson = badJson.replaceAll("\\}", "\\\"\\}");
                    badJson = badJson.replaceAll(":", ":\\\"");
                    badJson = badJson.replaceAll(",", "\\\",");

                    HashMap<String, Object> result = mapper.readValue(badJson, HashMap.class);
                    if (result.get("type").toString().equals("venta_exitosa")) {
                        status = PaymentStatus.COMPLETED.name();
                        statusMessage = result.get("user_message").toString();
                    } else {
                        status = PaymentStatus.PENDING.name();
                        statusMessage = result.get("user_message").toString();
                    }
                }

                try {
                    String medatada = culqiResponse.get("metadata").toString();
                    ObjectMapper mapper = new ObjectMapper();
                    Map<String, String> meta = mapper.readValue(medatada, Map.class);

                    meta.get("tx_id");
                    meta.get("order_id");

                } catch (Exception e) {
                }

                PaymentTransactionCallback ptxCallback = new PaymentTransactionCallback();
                ptxCallback.setCallbackDate(new Date());
                ptxCallback.setReportedDate(new Date());
                ptxCallback.setStatus(status);
                ptxCallback.setPaymentTransactionId(ptx.getId());

                paymentTransactionCallbackRepository.save(ptxCallback);

                //get other info
                List<PaymentTransactionCallbackValue> values = new ArrayList<>();
                culqiResponse.entrySet().stream().map((entry) -> {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    PaymentTransactionCallbackValue val = new PaymentTransactionCallbackValue();
                    val.setCallbackId(ptxCallback.getId());
                    val.setValName(key);
                    val.setValValue(value != null ? value.toString() : null);
                    return val;
                }).forEachOrdered((val) -> {
                    values.add(val);
                });
                paymentTransactionCallbackValueRepository.saveAll(values);

                //get payment request 
                payReq.setStatus(status);
                payReq.setStatusMessage(statusMessage);
                payReq.setStatusShown(false);
                payReq.setStatusDate(new Date());
                payReq.setProcessor("culqi");

                paymentRequestRepository.save(payReq);

                //send callback
                callbackService.postCallback(payReq);

            }

        } catch (Exception e) {
            log.error("Error creating culqi charge", e);
        }
    }
}
