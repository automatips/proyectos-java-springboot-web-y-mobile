/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.blockpays;

import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.gateways.impl.blockpays.dto.CurrenciesAvailable;
import com.wings.payments.gateways.impl.blockpays.dto.Currency;
import com.wings.payments.gateways.impl.blockpays.dto.RequestPayment;
import com.wings.payments.gateways.impl.blockpays.dto.ResponsePayment;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class BlockPaysService {

    private final Logger log = LoggerFactory.getLogger(BlockPaysService.class);

    /**
     *
     * @return
     */
    public List<Currency> getCryptoCurrencies() {

        List<Currency> ret = new ArrayList<>();
        try {
            //Si necesito loguear uso esto
            HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
            RestTemplate template = new RestTemplate(clientHttpRequestFactory);
            //RestTemplate template = new RestTemplate();
            RequestEntity requestEntity = RequestEntity
                    .post(new URI("https://my.business.blockpays.io/api/v1/getCurrencies"))
                    .header("accountId", "5BOkMdJGtqcJo7uqARv1k1584732156620")
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE).build();

            ResponseEntity<CurrenciesAvailable> response = template.exchange(requestEntity, CurrenciesAvailable.class);

            if (response.getBody() != null
                    && response.getBody().getStatus() != null
                    && response.getBody().getStatus().equals(200)) {

                return response.getBody().getData();
            }

        } catch (Exception e) {
            log.error("Error making callback", e);
        }

        return ret;
    }

    /**
     *
     * @param req
     * @param currencyId
     * @return
     */
    public ResponsePayment postCharge(PaymentRequest req, Long currencyId) {

        try {
            //Si necesito loguear uso esto
            HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
            RestTemplate template = new RestTemplate(clientHttpRequestFactory);
            //RestTemplate template = new RestTemplate();

            RequestPayment requestPayment = new RequestPayment();
            requestPayment.setAmount(req.getAmount());
            requestPayment.setCurrencyISO(req.getCurrency());
            requestPayment.setCriptoId(currencyId.intValue());
            requestPayment.setAddressTo("");
            requestPayment.setReference("W" + req.getId());
            requestPayment.setTimeActive(60000l);
            requestPayment.setUrlConfirmation("https://dev.pagos.wingsmobile.com/blockpays/callbacks");

            RequestEntity<RequestPayment> requestEntity = RequestEntity
                    .post(new URI("https://my.business.blockpays.io/api/v1/requestPayment"))
                    .header("accountId", "5BOkMdJGtqcJo7uqARv1k1584732156620")
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .body(requestPayment);

            ResponseEntity<ResponsePayment> response = template.exchange(requestEntity, ResponsePayment.class);
            return response.getBody();

        } catch (Exception e) {
            log.error("Error making callback", e);
        }
        return null;
    }

}
