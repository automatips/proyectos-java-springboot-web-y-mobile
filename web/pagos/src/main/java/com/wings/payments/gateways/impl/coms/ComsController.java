/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.coms;

import com.wings.payments.services.CallbackService;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Locale;

/**
 *
 * @author seba
 */
@Controller
public class ComsController {

    @Autowired
    private ComsService comsService;

    @Autowired
    private MessageSource messageSource;
    
    private final Logger log = LoggerFactory.getLogger(ComsService.class);

    @RequestMapping(value = {"coms/charge"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postCharge(Model model, HttpServletRequest request, Locale locale) {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return ResponseEntity.badRequest().body(messageSource.getMessage("coms.controller.bad.credentials", null, locale));
        }

        String reqId = request.getParameter("reqId");
        long requestId;
        try {
            requestId = Long.parseLong(reqId);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(messageSource.getMessage("coms.controller.req.not.valid", null, locale));
        }

        try {
            String statusMessage = comsService.createComsCharge(username, password, requestId);
            if (statusMessage.equals("Transaccion aceptada")) {
                return ResponseEntity.ok(statusMessage);
            } else {
                return ResponseEntity.badRequest().body(statusMessage);
            }
        } catch (Exception e) {
            log.error("Error con coms charge", e);
            return ResponseEntity.badRequest().body("Error: " + e.getMessage());
        }
    }

}
