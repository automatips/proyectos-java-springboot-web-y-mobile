package com.wings.payments.domain.specs;

import com.wings.payments.domain.bo.GatewayImplementation;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class GatewaySpec {

    public static Specification<GatewayImplementation> search(String country, String implementation, int enabled) {
        return (Root<GatewayImplementation> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(country != null) {
                predicates.add(builder.equal(root.get("country").as(String.class), country));
            }
            if(implementation != null) {
                predicates.add(builder.equal(root.get("name").as(String.class), implementation));
            }
            if(enabled >= 0) {
                predicates.add(builder.equal(root.get("enabled").as(Boolean.class), enabled));
            }
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
