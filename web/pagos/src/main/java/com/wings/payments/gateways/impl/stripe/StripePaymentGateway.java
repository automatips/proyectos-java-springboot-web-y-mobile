/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.stripe;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.GatewayImplementationProperty;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.gateways.PaymentGateway;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author seba
 */
public class StripePaymentGateway implements PaymentGateway {

    private final Map<String, String> config = new HashMap<>();

    @Override
    public void init(GatewayImplementation gatewayImplementation) {
        List<GatewayImplementationProperty> properties = gatewayImplementation.getTest() ? gatewayImplementation.getTestProperties() : gatewayImplementation.getProperties();
        properties.forEach((property) -> {
            config.put(property.getPropertyName(), property.getPropertyValue());
        });
    }

    @Override
    public Map<String, String> getPaymentFormData(PaymentRequest request) {
        return config;
    }

    @Override
    public String getTemplateName() {
        return "pays/stripe/index";
    }

}
