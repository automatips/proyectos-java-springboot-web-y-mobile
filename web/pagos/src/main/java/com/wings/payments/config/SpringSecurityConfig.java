/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.config;

import com.wings.payments.config.auth.handler.LoginSuccessHandler;
import com.wings.payments.services.JpaApplicationUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author maurib
 */
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{
    
    @Autowired
    public BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    private JpaApplicationUserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/**","/assets/**", "/fonts/**", "/img/**", "/webfonts/**","/css/**", "/js/**").permitAll()
            .anyRequest().authenticated()
            .and()
                .formLogin()
                .successHandler(loginSuccessHandler)
                .loginPage("/login")
                .defaultSuccessUrl("/admin",true)
            .permitAll()
                .and()
                .csrf().ignoringAntMatchers("/**")
            .and()
            .logout().permitAll()
            .and()
            .exceptionHandling().accessDeniedPage("/admin/error_403");
    }

    @Autowired
    public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception{
        build.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }
}
