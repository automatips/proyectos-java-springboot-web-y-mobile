package com.wings.payments.services.gateways;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.specs.GatewaySpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GatewayImplementationService {

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    public Page<GatewayImplementation> findAll(Pageable p) {
        return gatewayImplementationRepository.findAll(p);
    }

    public List<GatewayImplementation> findAll() {
        return gatewayImplementationRepository.findAll();
    }

    public Page<GatewayImplementation> findByParams(String country, String implementation, int enabled, Pageable p) {
        return gatewayImplementationRepository.findAll(GatewaySpec.search(country, implementation, enabled), p);
    }

    public GatewayImplementation findById(Long id) {
        return gatewayImplementationRepository.findById(id).orElse(null);
    }

    public GatewayImplementation save(GatewayImplementation gi) {
        return gatewayImplementationRepository.save(gi);
    }

    public void deleteById(Long id) {
        gatewayImplementationRepository.deleteById(id);
    }

    @Transactional
    public void update(GatewayImplementation gi) {
        gatewayImplementationRepository.update(gi.getEnabled(), gi.getTest(), gi.getId());
    }

    public List<GatewayImplementation> findByGatewayId(Long id) {
        return gatewayImplementationRepository.findByGatewayId(id);
    }
}
