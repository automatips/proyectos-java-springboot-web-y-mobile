package com.wings.payments.domain.bo;

public class GatewayImplementationAndPropertiesWrapper {

    private GatewayImplementation gi;
    private GatewayImplementationProperty[] properties;

    public GatewayImplementation getGi() {
        return gi;
    }

    public void setGi(GatewayImplementation gi) {
        this.gi = gi;
    }

    public GatewayImplementationProperty[] getProperties() {
        return properties;
    }

    public void setGipList(GatewayImplementationProperty[] properties) {
        this.properties = new GatewayImplementationProperty[properties.length];
        this.properties = properties;
    }
}