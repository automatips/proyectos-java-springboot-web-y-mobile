/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.blockpays.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 * @author seba
 */
@JsonInclude(Include.NON_NULL)
public class RequestPayment {

    private Double amount;
    private String currencyISO;
    private Integer criptoId;
    private String addressTo;
    private String reference;
    private Long timeActive;
    private String urlConfirmation;
    private String urlPayment;
    private String paymentId;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setCurrencyISO(String currencyISO) {
        this.currencyISO = currencyISO;
    }

    public String getCurrencyISO() {
        return currencyISO;
    }

    public Integer getCriptoId() {
        return criptoId;
    }

    public void setCriptoId(Integer criptoId) {
        this.criptoId = criptoId;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getTimeActive() {
        return timeActive;
    }

    public void setTimeActive(Long timeActive) {
        this.timeActive = timeActive;
    }

    public String getUrlConfirmation() {
        return urlConfirmation;
    }

    public void setUrlConfirmation(String urlConfirmation) {
        this.urlConfirmation = urlConfirmation;
    }

    public String getUrlPayment() {
        return urlPayment;
    }

    public void setUrlPayment(String urlPayment) {
        this.urlPayment = urlPayment;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

}
