/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.payu;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackValueRepository;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import com.wings.payments.gateways.PaymentGatewayFactory;
import com.wings.payments.services.CallbackService;
import com.wings.payments.utils.DateUtils;
import com.wings.payments.utils.RequestUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PayUService {

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentGatewayFactory paymentGatewayService;

    @Autowired
    private CallbackService callbackService;

    private final static Map<String, String> TRANSALTIONS = new HashMap<>();

    static {
        TRANSALTIONS.put("APPROVED", "Transacción aprobada");
        TRANSALTIONS.put("PAYMENT_NETWORK_REJECTED", "Transacción rechazada por entidad financiera");
        TRANSALTIONS.put("ENTITY_DECLINED", "Transacción rechazada por el banco");
        TRANSALTIONS.put("INSUFFICIENT_FUNDS", "Fondos insuficientes");
        TRANSALTIONS.put("INVALID_CARD", "Tarjeta inválida");
        TRANSALTIONS.put("CONTACT_THE_ENTITY", "Contactar entidad financiera");
        TRANSALTIONS.put("BANK_ACCOUNT_ACTIVATION_ERROR", "Débito automático no permitido");
        TRANSALTIONS.put("BANK_ACCOUNT_NOT_AUTHORIZED_FOR_AUTOMATIC_DEBIT", "Débito automático no permitido");
        TRANSALTIONS.put("INVALID_AGENCY_BANK_ACCOUNT", "Débito automático no permitido");
        TRANSALTIONS.put("INVALID_BANK_ACCOUNT", "Débito automático no permitido");
        TRANSALTIONS.put("INVALID_BANK", "Débito automático no permitido");
        TRANSALTIONS.put("EXPIRED_CARD", "Tarjeta vencida");
        TRANSALTIONS.put("RESTRICTED_CARD", "Tarjeta restringida");
        TRANSALTIONS.put("INVALID_EXPIRATION_DATE_OR_SECURITY_CODE", "Fecha de expiración o código de seguridadinválidos");
        TRANSALTIONS.put("REPEAT_TRANSACTION", "Reintentar pago");
        TRANSALTIONS.put("INVALID_TRANSACTION", "Transacción inválida");
        TRANSALTIONS.put("EXCEEDED_AMOUNT", "El valor excede el máximo permitido por la entidad");
        TRANSALTIONS.put("ABANDONED_TRANSACTION", "Transacción abandonada por el pagador");
        TRANSALTIONS.put("CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS", "Tarjeta no autorizada para comprar por internet");
        TRANSALTIONS.put("ANTIFRAUD_REJECTED", "Transacción rechazada por sospecha de fraude");
        TRANSALTIONS.put("DIGITAL_CERTIFICATE_NOT_FOUND", "Certificado digital no encotnrado");
        TRANSALTIONS.put("BANK_UNREACHABLE", "Error tratando de cominicarse con el banco");
        TRANSALTIONS.put("PAYMENT_NETWORK_NO_CONNECTION", "No fue posible establecer comunicación con la entidad financiera");
        TRANSALTIONS.put("PAYMENT_NETWORK_NO_RESPONSE", "No se recibió respuesta de la entidad financiera");
        TRANSALTIONS.put("ENTITY_MESSAGING_ERROR", "Error comunicándose con la entidad financiera");
        TRANSALTIONS.put("NOT_ACCEPTED_TRANSACTION", "Transacción no permitida");
        TRANSALTIONS.put("INTERNAL_PAYMENT_PROVIDER_ERROR", "Error");
        TRANSALTIONS.put("INACTIVE_PAYMENT_PROVIDER", "Error");
        TRANSALTIONS.put("ERROR", "Error");
        TRANSALTIONS.put("ERROR_CONVERTING_TRANSACTION_AMOUNTS", "Error");
        TRANSALTIONS.put("BANK_ACCOUNT_ACTIVATION_ERROR", "Error");
        TRANSALTIONS.put("FIX_NOT_REQUIRED", "Error");
        TRANSALTIONS.put("AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL", "Error");
        TRANSALTIONS.put("AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL", "Error");
        TRANSALTIONS.put("AUTOMATIC_FIXED_NOT_SUPPORTED", "Error");
        TRANSALTIONS.put("NOT_FIXED_FOR_ERROR_STATE", "Error");
        TRANSALTIONS.put("ERROR_FIXING_AND_REVERSING", "Error");
        TRANSALTIONS.put("ERROR_FIXING_INCOMPLETE_DATA", "Error");
        TRANSALTIONS.put("PAYMENT_NETWORK_BAD_RESPONSE", "Error");
        TRANSALTIONS.put("EXPIRED_TRANSACTION", "Transacción expirada");
        TRANSALTIONS.put("DECLINED_TEST_MODE_NOT_ALLOWED", "Transacción rechazada, modo de pruebas activo");
    }

    /**
     * PayU callbacks
     *
     * @param request
     */
    public void processPayUCallback(HttpServletRequest request) {

        Map<String, String> params = RequestUtils.getRequestParams(request);

        String paymentRequestId = params.get("reference_sale");
        paymentRequestId = paymentRequestId.replaceAll("W", "");

        //get paymentRequest
        PaymentRequest payReq = paymentRequestRepository.getOne(Long.parseLong(paymentRequestId));

        //get tx
        PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(payReq.getId());

        //create new callback
        PaymentTransactionCallback ptxCallback = new PaymentTransactionCallback();
        ptxCallback.setCallbackDate(new Date());
        ptxCallback.setPaymentTransactionId(ptx.getId());

        //get date
        String sreportedDate = params.get("transaction_date");
        Date reportedDate = new Date();
        try {
            reportedDate = DateUtils.getDateTimeFromMysqlFormat(sreportedDate);
        } catch (Exception e) {
        }
        //get status
        String sstatus = params.get("state_pol");
        String status = fromPayUToWingsStatus(sstatus);

        //get status message
        String sstatusMessage = params.get("response_message_pol");
        String statusMessage = translateStatus(sstatusMessage);

        Long giId = 5l;
        if (payReq.getCountry().equals("CO")) {
            giId = 6l;
        }

        //TODO: verificar esto que no lo tome de la trax por que puede cambiar 
        //antes de que llegue el callback
        //check signature        
        //Long giId = ptx.getGatewayImplementantionId();
        GatewayImplementation gi = gatewayImplementationRepository.getOne(giId);
        PayUPaymentGateway payu = (PayUPaymentGateway) paymentGatewayService.getImplementation(gi);
        String mySignature = payu.getConfirmationSignature(
                params.get("merchant_id"),
                params.get("reference_sale"),
                params.get("value"),
                params.get("currency"),
                params.get("state_pol"));

        String theySignature = params.get("sign");
        if (!mySignature.equals(theySignature)) {
            //is signature don't match put failed status
            status = PaymentStatus.FAILED_WRONG_SIGNATURE.name();
            statusMessage = "Las firmas no coinciden";
        }

        //put values
        ptxCallback.setReportedDate(reportedDate);
        ptxCallback.setStatus(status);

        //save
        paymentTransactionCallbackRepository.save(ptxCallback);

        //refresh status
        ptx.setLastStatus(status);
        ptx.setGatewayImplementantionId(2l);
        paymentTransactionRepository.save(ptx);

        //get other info
        List<PaymentTransactionCallbackValue> values = new ArrayList<>();
        params.entrySet()
                .forEach((entry) -> {
                    String key = entry.getKey();
                    String valu = entry.getValue();
                    PaymentTransactionCallbackValue val = new PaymentTransactionCallbackValue();
                    val.setCallbackId(ptxCallback.getId());
                    val.setValName(key);
                    val.setValValue(valu);
                    values.add(val);
                }
                );
        paymentTransactionCallbackValueRepository.saveAll(values);

        //update payment request 
        payReq.setStatus(status);
        payReq.setStatusMessage(statusMessage);
        payReq.setStatusShown(false);
        payReq.setStatusDate(new Date());
        payReq.setProcessor("payu");

        paymentRequestRepository.save(payReq);

        //send callback
        callbackService.postCallback(payReq);
    }

    /**
     * PayU responseUrl
     *
     * @param request
     */
    public void processPayUResponse(HttpServletRequest request) {

        Map<String, String> params = RequestUtils.getRequestParams(request);
        String paymentRequestId = params.get("referenceCode");
        paymentRequestId = paymentRequestId.replaceAll("W", "");

        //get payment request
        PaymentRequest payReq = paymentRequestRepository.getOne(Long.parseLong(paymentRequestId));

        //get tx
        PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(payReq.getId());

        //create new callback
        PaymentTransactionCallback ptxCallback = new PaymentTransactionCallback();
        ptxCallback.setCallbackDate(new Date());
        ptxCallback.setPaymentTransactionId(ptx.getId());

        //get date
        String sreportedDate = params.get("processingDate");
        Date reportedDate = new Date();
        try {
            reportedDate = DateUtils.getDateFromMysqlFormat(sreportedDate);
        } catch (Exception e) {
        }

        //get status
        String sstatus = params.get("lapTransactionState");
        String status = fromPayUToWingsStatus(sstatus);

        //get status message
        String sstatusMessage = params.get("lapResponseCode");
        String statusMessage = translateStatus(sstatusMessage);

        //check signature
        Long giId = ptx.getGatewayImplementantionId();
        GatewayImplementation gi = gatewayImplementationRepository.getOne(giId);
        PayUPaymentGateway payu = (PayUPaymentGateway) paymentGatewayService.getImplementation(gi);
        String mySignature = payu.getResponseSignature(
                params.get("merchantId"),
                params.get("referenceCode"),
                params.get("TX_VALUE"),
                params.get("currency"),
                params.get("transactionState"));

        String theySignature = params.get("signature");
        if (!mySignature.equals(theySignature)) {
            //is signature don't match put failed status
            status = PaymentStatus.FAILED_WRONG_SIGNATURE.name();
            statusMessage = "Las firmas no coinciden";
        }

        //put values
        ptxCallback.setReportedDate(reportedDate);
        ptxCallback.setStatus(status);

        //save
        paymentTransactionCallbackRepository.save(ptxCallback);

        //refresh status
        ptx.setLastStatus(status);
        paymentTransactionRepository.save(ptx);

        //get other info
        List<PaymentTransactionCallbackValue> values = new ArrayList<>();
        params.entrySet()
                .forEach((entry) -> {
                    String key = entry.getKey();
                    String valu = entry.getValue();
                    PaymentTransactionCallbackValue val = new PaymentTransactionCallbackValue();
                    val.setCallbackId(ptxCallback.getId());
                    val.setValName(key);
                    val.setValValue(valu);
                    values.add(val);
                }
                );
        paymentTransactionCallbackValueRepository.saveAll(values);

        //solo si NO es aceptada
        if (!status.equals(PaymentStatus.COMPLETED.name())) {
            payReq.setStatus(status);
            payReq.setStatusMessage(statusMessage);
            payReq.setStatusShown(false);
            payReq.setStatusDate(new Date());
            payReq.setProcessor("payu");
            paymentRequestRepository.save(payReq);
        }

        //send callback
        callbackService.postCallback(payReq);
    }

    /**
     * Get payment status from gw to wings
     *
     * @param payUStatus
     * @return
     */
    public static String fromPayUToWingsStatus(String payUStatus) {
        switch (payUStatus) {
            case "4":
                return PaymentStatus.COMPLETED.name();
            case "6":
                return PaymentStatus.FAILED.name();
            case "104":
                return PaymentStatus.FAILED.name();
            case "5":
                return PaymentStatus.CANCELED.name();
            case "7":
                return PaymentStatus.PENDING.name();
            default:
                return PaymentStatus.FAILED.name();
        }
    }

    /**
     * Translate to spanish the status code
     *
     * @param payUStatus
     * @return
     */
    public String translateStatus(String payUStatus) {
        return TRANSALTIONS.get(payUStatus);
    }

}
