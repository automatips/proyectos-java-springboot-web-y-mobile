/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.blockpays.dto;

/**
 *
 * @author seba
 */
public class ResponsePayment {

    private String status;
    private RequestPayment data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RequestPayment getData() {
        return data;
    }

    public void setData(RequestPayment data) {
        this.data = data;
    }

}
