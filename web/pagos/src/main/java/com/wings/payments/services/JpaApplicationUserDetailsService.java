package com.wings.payments.services;

import com.wings.payments.domain.login.PasswordResetToken;
import com.wings.payments.domain.login.Role;
import com.wings.payments.domain.login.ApplicationUser;
import com.wings.payments.domain.repos.ApplicationUserRepository;
import com.wings.payments.domain.repos.PasswordTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Service("jpaUserDetailsService")
public class JpaApplicationUserDetailsService implements UserDetailsService {

    @Autowired
    private ApplicationUserRepository userRepository;

    @Autowired
    private PasswordTokenRepository passwordTokenRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private Logger logger = LoggerFactory.getLogger(JpaApplicationUserDetailsService.class);

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = userRepository.findByUsername(username);
        if(applicationUser == null) {
            logger.error("Usuario no encontrado");
            throw new UsernameNotFoundException("El usuario ".concat(username).concat(" no existe."));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();

        for(Role role: applicationUser.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
        }

        if(authorities.isEmpty()) {
            logger.error("Error login: usuario '".concat(username).concat("' no tiene roles asignado."));
            throw new UsernameNotFoundException("Error login: usuario '".concat(username).concat("' no tiene roles asignado."));
        }

        return new User(applicationUser.getUsername(),
                applicationUser.getPassword(),
                applicationUser.getEnabled(), true, true, true, authorities);
    }

    @Transactional(readOnly = true)
    public List<ApplicationUser> findAllEnabled() {
        return userRepository.findAllEnabled();
    }

    @Transactional(readOnly = true)
    public ApplicationUser findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public ApplicationUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public ApplicationUser save(ApplicationUser user) {
        return userRepository.save(user);
    }

    @Transactional
    public void saveRole(Long id, String role) {
        userRepository.saveRole(id, role);
    }

    @Transactional(readOnly = true)
    public boolean usernameAlreadyExists(ApplicationUser user) {
        if(user.getId() == null) {
            return userRepository.existsByUsername(user.getUsername());
        }
        return userRepository.existsByUsernameAndIdNot(user.getUsername(), user.getId());
    }

    @Transactional(readOnly = true)
    public ApplicationUser findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void createPasswordResetTokenForUser(ApplicationUser user, String token) {
        PasswordResetToken myToken = new PasswordResetToken(token, user.getEmail());
        passwordTokenRepository.save(myToken);
    }

    public String validatePasswordResetToken(String token) {
        final PasswordResetToken passToken = passwordTokenRepository.findByToken(token);
        return !isTokenFound(passToken) ? "invalidToken"
                : isTokenExpired(passToken) ? "expired"
                : null;
    }

    private boolean isTokenFound(PasswordResetToken passToken) {
        return passToken != null;
    }

    private boolean isTokenExpired(PasswordResetToken passToken) {
        final Calendar cal = Calendar.getInstance();
        return passToken.getExpiryDate().before(cal.getTime());
    }

    public ApplicationUser getUserByPasswordResetToken(String token) {
        PasswordResetToken pswToken = passwordTokenRepository.findEmailByToken(token);
        return userRepository.findByEmail(pswToken.getUserEmail());
    }

    public void changeUserPassword(ApplicationUser user, String newPassword) {
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }
}
