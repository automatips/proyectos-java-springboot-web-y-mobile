package com.wings.payments.email;

import com.wings.payments.domain.login.ApplicationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class EmailService {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Environment environment;

    @Value("${spring.mail.username}")
    private String fromEmail;

    public SimpleMailMessage constructResetTokenEmail(
            String contextPath, Locale locale, String token, ApplicationUser user) {
        String url = contextPath + "/user/changePassword?token=" + token;
        StringBuilder body = new StringBuilder();
        body.append(messageSource.getMessage("message.rp.email.message", null, locale));
        body.append(System.getProperty("line.separator"));
        body.append(System.getProperty("line.separator"));
        body.append(messageSource.getMessage("message.rp.email.steps", null, locale));
        body.append(System.getProperty("line.separator"));
        body.append(System.getProperty("line.separator"));
        body.append("\u2022 "+messageSource.getMessage("message.rp.email.step.1", null, locale)).append(": ").append("http://"+url);
        body.append(System.getProperty("line.separator"));
        body.append("\u2022 "+messageSource.getMessage("message.rp.email.step.2", null, locale)).append(": ").append(token);
        body.append(System.getProperty("line.separator"));
        body.append(System.getProperty("line.separator"));
        body.append(messageSource.getMessage("message.rp.email.username", null, locale)).append(": ").append(user.getUsername());
        body.append(System.getProperty("line.separator"));
        body.append(System.getProperty("line.separator"));
        body.append(messageSource.getMessage("message.rp.email.regards", null, locale));
        return constructEmail(messageSource.getMessage("password.reset.request", null, locale), body.toString(), user);
    }

    private SimpleMailMessage constructEmail(String subject, String body,
                                             ApplicationUser user) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getEmail());
        email.setFrom(fromEmail);
        return email;
    }
}