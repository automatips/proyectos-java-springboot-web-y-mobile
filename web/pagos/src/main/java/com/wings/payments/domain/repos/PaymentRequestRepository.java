/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.PaymentRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface PaymentRequestRepository extends JpaRepository<PaymentRequest, Long>, JpaSpecificationExecutor {

    PaymentRequest findByOrderNumberAndRequesterName(Long orderNumber, String requesterName);

    PaymentRequest findByCode(String code);

    PaymentRequest findByPayCode(String payCode);

    int countByPayCode(String payCode);

    PaymentRequest findByOrderNumber(Long orderNumber);

}
