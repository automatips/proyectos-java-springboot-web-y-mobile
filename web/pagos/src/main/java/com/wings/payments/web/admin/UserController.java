package com.wings.payments.web.admin;

import com.wings.payments.domain.dto.PasswordDto;
import com.wings.payments.domain.login.ApplicationUser;
import com.wings.payments.domain.login.Role;
import com.wings.payments.email.EmailService;
import com.wings.payments.services.JpaApplicationUserDetailsService;
import com.wings.payments.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Controller
@SessionAttributes("applicationUser")
public class UserController {

    @Autowired
    private JpaApplicationUserDetailsService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    public BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private EmailService emailService;

    @Autowired
    private Environment environment;

    @Value("${wings.payment.dev.domain}")
    private String devDomain;

    @Value("${wings.payment.domain}")
    private String domain;

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String getUsers(Model model) {
        List<ApplicationUser> users =  userService.findAllEnabled();
        model.addAttribute("users", users);
        return "view-users";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value="/admin/user", method = RequestMethod.GET)
    public String createUserPage(Model model, Locale locale) {
           model.addAttribute("mainTitle", messageSource.getMessage("create.user.title", null, locale));
           model.addAttribute("cardTitle", messageSource.getMessage("create.user.card.title", null, locale));
           ApplicationUser user = new ApplicationUser();
           model.addAttribute("applicationUser", user);
        return "create-user";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value="/admin/user/{id}", method = RequestMethod.GET)
    public String editionUserPage(Model model, @PathVariable(value = "id", required = false) Long id, Locale locale) {
        ApplicationUser user = userService.findById(id);
        model.addAttribute("mainTitle", messageSource.getMessage("edit.user.title", null, locale));
        model.addAttribute("cardTitle", messageSource.getMessage("edit.user.card.title", null, locale));
        model.addAttribute("applicationUser", user);
        return "edit-user";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value="/user/password/{name}", method = RequestMethod.GET)
    public String userPassword(Model model, @PathVariable(value = "name") String name) {
        ApplicationUser applicationUser = userService.findByUsername(name);
        model.addAttribute("applicationUser", applicationUser);
        return "changePassword";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value="/user/password", method = RequestMethod.PUT)
    public String changePassword(@Valid ApplicationUser applicationUser, BindingResult result, Model model,
                                 @RequestParam(value = "id") Long id,
                                 @RequestParam(value="newPassword", required = false) String newPassword,
                                 @RequestParam(value="passwordConfirmation") String pswConfirm,
                                 SessionStatus status, Locale locale) {

        ApplicationUser user = userService.findById(id);

       if(result.hasErrors()) {
            if(newPassword == null) {
                model.addAttribute("mainTitle", messageSource.getMessage("edit.user.title", null, locale));
                model.addAttribute("cardTitle", messageSource.getMessage("edit.user.card.title", null, locale));
                return "edit-user";
            } else if(newPassword.trim().length() == 0 || pswConfirm.trim().length() == 0){
                model.addAttribute("error", messageSource.getMessage("psw.validation.error.msg", null, locale));
                return "changePassword";
            }
        }

        if(newPassword == null) {
            if (!applicationUser.getPassword().equals(pswConfirm)) {
                model.addAttribute("error", messageSource.getMessage("password.dont.match", null, locale));
                return "edit-user";
            }
            String bcryptPassword = passwordEncoder.encode(applicationUser.getPassword());
            user.setPassword(bcryptPassword);
            user = userService.save(user);
            status.setComplete();
            model.addAttribute("applicationUser", user);
            return "redirect:/admin/users";
        }
        if(passwordEncoder.matches(applicationUser.getPassword(), user.getPassword())) {
            if(newPassword.equals(pswConfirm)) {
                String bcryptPassword = passwordEncoder.encode(pswConfirm);
                user.setPassword(bcryptPassword);
                userService.save(user);
                status.setComplete();
                return "changePassword";
            }
        }
        model.addAttribute("error", messageSource.getMessage("wrong.password", null, locale));
        return "changePassword";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value="/admin/user", method = RequestMethod.POST)
    public String addUser(@Valid ApplicationUser user,
                          BindingResult result,
                          Model model, @RequestParam("role") String role,
                          @RequestParam(value = "passwordConfirmation", required = false) String pswConfirm,
                          SessionStatus status, Locale locale) {

        model.addAttribute("mainTitle", messageSource.getMessage("create.user.title", null, locale));
        model.addAttribute("cardTitle", messageSource.getMessage("create.user.card.title", null, locale));

        if(result.hasErrors()) {
            return "create-user";
        }

        if(userService.usernameAlreadyExists(user)) {
            model.addAttribute("error", messageSource.getMessage("user.already.exists", null, locale));
            return "create-user";
        }

        if(!user.getPassword().equals(pswConfirm)) {
            model.addAttribute("error", messageSource.getMessage("password.dont.match", null, locale));
            return "create-user";
        }

        String bcryptPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(bcryptPassword);

        user = userService.save(user);
        userService.saveRole(user.getId(), role);

        status.setComplete();
        return "redirect:/admin/users";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value="/admin/user", method = RequestMethod.PUT)
    public String saveEditedUser(@Valid ApplicationUser user,
                          BindingResult result,
                          Model model, @RequestParam("role") String role,
                          SessionStatus status, Locale locale) {

        model.addAttribute("mainTitle", messageSource.getMessage("edit.user.title", null, locale));
        model.addAttribute("cardTitle", messageSource.getMessage("edit.user.card.title", null, locale));

        if(result.hasErrors()) {
            return "edit-user";
        }

        if(userService.usernameAlreadyExists(user)) {
            model.addAttribute("error", messageSource.getMessage("user.already.exists", null, locale));
            return "edit-user";
        }

        user = userService.save(user);

        Role applicationRole = roleService.findOne(user);
        applicationRole.setAuthority(role);
        roleService.save(applicationRole);

        status.setComplete();
        return "redirect:/admin/users";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value="/admin/user/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity disableUser(@PathVariable Long id) {

        if(id == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        ApplicationUser user = userService.findById(id);
        if(user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        user.setDisabled(true);
        userService.save(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping("/passwordRecovery")
    public String passwordRecovery(Model model) {

        model.addAttribute("error", false);
        model.addAttribute("emailSent", false);
        return "forgot-password";
    }

    @RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
    public String resetPassword(@RequestParam("email") String userEmail, Locale locale, Model model) {
        ApplicationUser user = userService.findByEmail(userEmail);
        if (user != null) {

            String[] profiles = environment.getActiveProfiles();
            for (String profile : profiles) {
                if (profile.contains("desa") || profile.contains("default")) {
                    domain = devDomain;
                    break;
                }
            }

            String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);
            mailSender.send(emailService.constructResetTokenEmail(domain, locale, token, user));
            model.addAttribute("emailSent", true);
            model.addAttribute("userEmail", user.getEmail());
            return "forgot-password";
        }
        model.addAttribute("error", true);
        return "forgot-password";
    }

    @GetMapping("/user/changePassword")
    public String showChangePasswordPage(Model model, @RequestParam("token") String token) {
        String result = userService.validatePasswordResetToken(token);
        if(result != null) {
            return "redirect:/admin";
        } else {
            model.addAttribute("token", token);
            return "updatePassword";
        }
    }

    @RequestMapping(value="/user/savePassword", method = RequestMethod.POST)
    public String savePassword(final Locale locale, @Valid PasswordDto passwordDto, Model model) {

        if(!passwordDto.getNewPassword().equals(passwordDto.getConfirmPassword())) {
            model.addAttribute("error", messageSource.getMessage("update.psw.error", null, locale));
            model.addAttribute("errorMessage", messageSource.getMessage("update.psw.not.equals", null, locale));
            return "updatePassword";
        }

        String result = userService.validatePasswordResetToken(passwordDto.getToken().trim());
        if(result != null) {
            model.addAttribute("error", messageSource.getMessage("update.psw.token.error.title", null, locale));
            model.addAttribute("errorMessage", messageSource.getMessage("update.psw.token.error.message", null, locale));
            return "updatePassword";
        }
        ApplicationUser user = userService.getUserByPasswordResetToken(passwordDto.getToken().trim());
        if(user != null) {
            userService.changeUserPassword(user, passwordDto.getNewPassword());
            model.addAttribute("success", messageSource.getMessage("update.psw.user.success.title", null, locale));
            model.addAttribute("successMessage", messageSource.getMessage("update.psw.user.success.message", null, locale));
        } else {
            model.addAttribute("error", messageSource.getMessage("update.psw.user.error.title", null, locale));
            model.addAttribute("errorMessage", messageSource.getMessage("update.psw.user.error.message", null, locale));
        }
        return "updatePassword";
    }
}
