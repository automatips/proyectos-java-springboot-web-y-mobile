/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author seba
 */
public interface PaymentTransactionCallbackValueRepository extends JpaRepository<PaymentTransactionCallbackValue, Long> {

    PaymentTransactionCallbackValue findByValNameAndValValue(String valName, String valValue);

    Page<PaymentTransactionCallbackValue> findByCallbackId(Long id, Pageable pageable);

    List<PaymentTransactionCallbackValue> findByCallbackId(Long id);

}