/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author seba
 */
public class DateUtils {

    private final static SimpleDateFormat SDF_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy-MM-dd");

    public static Date getDateTimeFromMysqlFormat(String stringDate) throws ParseException {
        return SDF_DATETIME.parse(stringDate);
    }

    public static Date getDateFromMysqlFormat(String stringDate) throws ParseException {
        return SDF_DATE.parse(stringDate);
    }

}
