package com.wings.payments.domain.dto;

import javax.validation.constraints.NotEmpty;

public class PasswordDto {

    @NotEmpty
    private String confirmPassword;

    @NotEmpty
    private  String token;

    @NotEmpty
    private String newPassword;

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}