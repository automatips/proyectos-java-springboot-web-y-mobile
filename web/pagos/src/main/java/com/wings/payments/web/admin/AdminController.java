package com.wings.payments.web.admin;

import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import com.wings.payments.services.*;
import com.wings.payments.utils.PageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 *
 * @author seba
 */
@Secured({"ROLE_ADMIN", "ROLE_USER"})
@Controller
public class AdminController {

    @Autowired
    private PaymentRequestService paymentRequestService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private PaymentTransactionCallbackService paymentTransactionCallbackService;

    @Autowired
    private PaymentTransactionCallbacksValuesService paymentTransactionCallbacksValuesService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value={"/admin","/admin/"}, method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest request) {

        int page = 0;
        int size = 12;

        try {
            page = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) { }
        try {
            size = Integer.parseInt(request.getParameter("size"));
        } catch (NumberFormatException e) { }

        page = page == 0 ? page : page - 1;

        PageRequest pr = PageRequest.of(page, size, new Sort(Sort.Direction.ASC, "id"));

        Page<PaymentRequest> paymentRequests = paymentRequestService.findAll(pr);

        PageWrapper<PaymentRequest> pageWrapper = new PageWrapper<>(paymentRequests, "/admin");

        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", size);
        model.addAttribute("paymentRequests", paymentRequests);
        return "home";
    }

    @RequestMapping(value="/admin/request-details", method = RequestMethod.GET)
    public String showDetails(@RequestParam(value="searchParams") String params,
                              Model model, HttpServletRequest request, Locale locale) {

        if(params == null || params.isEmpty()) {
            return "redirect:/admin/";
        }

        int page = 0;
        int size = 12;

        try {
            page = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) { }
        try {
            size = Integer.parseInt(request.getParameter("size"));
        } catch (NumberFormatException e) { }

        page = page == 0 ? page : page - 1;

        PageRequest pr = PageRequest.of(page, size, new Sort(Sort.Direction.ASC, "id"));

        Page<PaymentRequest> paymentRequests = paymentRequestService.findByParams(params.trim(), pr);

        if(paymentRequests != null && paymentRequests.getNumberOfElements() == 1) {

            PaymentRequest paymentRequest = paymentRequests.iterator().next();
            PaymentTransaction paymentTransaction = paymentTransactionService.findByPaymentRequestId(paymentRequest.getId());
            model.addAttribute("paymentRequest", paymentRequest);
            model.addAttribute("paymentTransaction", paymentTransaction);

            return "request-details";
        } else if (paymentRequests.getNumberOfElements() > 1){
            PageWrapper<PaymentRequest> pageWrapper = new PageWrapper<>(paymentRequests, "/admin/request-details?searchParams=".concat(params));
            model.addAttribute("page", pageWrapper);
            model.addAttribute("size", size);
            model.addAttribute("paymentRequests", paymentRequests);
            return "home";
        } else {
            model.addAttribute("message", messageSource.getMessage("search.not.found", null, locale));
            model.addAttribute("paymentRequests", paymentRequests);
            return "home";
        }
    }

    @RequestMapping(value="/admin/request-details/{orderNumber}", method = RequestMethod.GET)
    public String getPaymentRequestByOrderNumber(@PathVariable(value="orderNumber") String order,
                                                 Model model, Locale locale) {

        if(order == null || order.isEmpty()) {
            return "redirect:/admin/";
        }

        Long orderNumber = 0l;
        try {
            orderNumber = Long.parseLong(order);
        } catch (NumberFormatException e) {}
        PaymentRequest paymentRequest = paymentRequestService.findByOrderNumber(orderNumber);
        if(paymentRequest != null) {
            PaymentTransaction paymentTransaction = paymentTransactionService.findByPaymentRequestId(paymentRequest.getId());
            model.addAttribute("paymentRequest", paymentRequest);
            model.addAttribute("paymentTransaction", paymentTransaction);
            return "request-details";
        } else {
            model.addAttribute("message", messageSource.getMessage("search.not.found", null, locale));
            return "home";
        }
    }

    @RequestMapping(value="/admin/callback-transactions/{ptId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String getPaymentTransactionCallbacks(@PathVariable("ptId") String ptId, Model model, HttpServletRequest request) {
        if(ptId != null) {
            int size =  5;
            int page = 0;
            try{
                page = (request.getParameter("page") == null) ? 0 : (Integer.parseInt(request.getParameter("page")) -1);
            } catch (NumberFormatException e) { }

            Long id = null;
            try {
                id = Long.parseLong(ptId);
            } catch (Exception e) {}

            PageRequest pr = PageRequest.of(page, size, new Sort(Sort.Direction.ASC, "id"));
            Page<PaymentTransactionCallback> paymentTransactionCallbackList = paymentTransactionCallbackService.findByPaymentTransactionId(id, pr);
            PageWrapper<PaymentTransactionCallback> pageWrapper = new PageWrapper<>(paymentTransactionCallbackList, "/admin/callback-transactions/".concat(ptId));

            if(paymentTransactionCallbackList.getTotalElements() == 0) {
                return null;
            }
            model.addAttribute("page", pageWrapper);
            model.addAttribute("size", size);
            model.addAttribute("paymentTransactionCallbackList", paymentTransactionCallbackList);
            return "payment-callbacks";
        }
        return null;
    }

    @RequestMapping(value="/admin/callback-details/{vid}")
    public String showCallbackDetails(@PathVariable String vid, Model model, HttpServletRequest request) {

        int size =  5;
        int page = 0;
        try{
            page = (request.getParameter("page") == null) ? 0 : (Integer.parseInt(request.getParameter("page")) -1);
        } catch (NumberFormatException e) { }

        Long id = 0L;
        try { id = Long.parseLong(vid); } catch (Exception e) {}

        PageRequest pr = PageRequest.of(page, size, new Sort(Sort.Direction.ASC, "id"));
        Page<PaymentTransactionCallbackValue> callbackValuesList = paymentTransactionCallbacksValuesService.findByCallbackId(id, pr);
        PageWrapper<PaymentTransactionCallbackValue> pageWrapper = new PageWrapper<>(callbackValuesList, "/admin/callback-details/".concat(vid));

        if (callbackValuesList.getContent().size() == 0) {
            model.addAttribute("noCallbackValues", true);
        } else {
            model.addAttribute("page", pageWrapper);
            model.addAttribute("size", size);
            model.addAttribute("callbackValuesList", callbackValuesList);
        }
        return "callback-values";
    }
}