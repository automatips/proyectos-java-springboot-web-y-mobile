/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.PaymentTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface PaymentTransactionRepository extends JpaRepository<PaymentTransaction, Long> {

    PaymentTransaction findByPaymentRequestId(Long paymentRequestId);

    PaymentTransaction findFirstById(Long transacId);

}
