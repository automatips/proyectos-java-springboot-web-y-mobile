package com.wings.payments.web.admin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.payments.domain.bo.*;
import com.wings.payments.services.CountryService;
import com.wings.payments.services.gateways.GatewayImplementationPropertyService;
import com.wings.payments.services.gateways.GatewayImplementationService;
import com.wings.payments.services.gateways.GatewayPropertyService;
import com.wings.payments.services.gateways.GatewayService;
import com.wings.payments.utils.PageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Secured("ROLE_ADMIN")
@Controller
public class GatewayController {

    @Autowired
    private GatewayImplementationPropertyService gatewayImplementationPropertyService;

    @Autowired
    private GatewayImplementationService gatewayImplementationService;

    @Autowired
    private GatewayPropertyService gatewayPropertyService;

    @Autowired
    private GatewayService gatewayService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/admin/gateways", method = RequestMethod.GET)
    public String viewGatewaysInformation(Model model, HttpServletRequest request) {

        int page = 0;
        int size = 12;

        try {
            page = Integer.parseInt(request.getParameter("page")) -1;
        } catch (NumberFormatException e) { }
        try {
            size = Integer.parseInt(request.getParameter("size"));
        } catch (NumberFormatException e) { }

        List<String> countries = countryService.findAll();
        List<String> implementationsList = new ArrayList<>();
        gatewayImplementationService.findAll().stream().forEach( x -> {
            if(!implementationsList.contains(x.getName())) {
                implementationsList.add(x.getName());
            }
        });

        PageRequest pr = PageRequest.of(page, size, new Sort(Sort.Direction.ASC, "id"));

        String gatewayEnabled = request.getParameter("gatewayEnabled");
        String gatewayGw = request.getParameter("gatewayGw");
        String gatewayCountry = request.getParameter("gatewayCountry");

        Map<String, String> params = new HashMap<>();

        if(gatewayEnabled != null) { params.put("gatewayEnabled", gatewayEnabled);}
        if(gatewayGw != null) { params.put("gatewayGw", gatewayGw);}
        if(gatewayCountry != null) { params.put("gatewayCountry", gatewayCountry);}
        
        Page<GatewayImplementation> gatewayImplementationList;
        PageWrapper<GatewayImplementation> pageWrapper;

        if(params.size() == 0) {
            gatewayImplementationList = gatewayImplementationService.findAll(pr);
            pageWrapper = new PageWrapper<>(gatewayImplementationList, "/admin/gateways");
        } else {
            int isEnabled = -1;
            try {
                isEnabled = Integer.parseInt(gatewayEnabled);
            } catch (NumberFormatException e) {}
            gatewayImplementationList = gatewayImplementationService.findByParams(gatewayCountry, gatewayGw, isEnabled, pr);
            StringBuilder sb = new StringBuilder("/admin/gateways?");

            Map.Entry<String, String> param = params.entrySet().stream().findFirst().orElse(null);
            sb.append(param.getKey()).append("=").append(params.get(param.getKey()));
            params.remove(param.getKey());

            for ( String key : params.keySet() ) {
                sb.append("&").append(key).append("=").append(params.get(key));
            }
            pageWrapper = new PageWrapper<>(gatewayImplementationList, sb.toString());
        }

        model.addAttribute("countries", countries);
        model.addAttribute("implementationsList", implementationsList);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", size);

        model.addAttribute("gatewayImplementationList", gatewayImplementationList);
        return "gateways/gatewaysView";
    }

    @RequestMapping(value = "/admin/gateway-details", method = RequestMethod.GET)
    public String viewGatewayDetails() {
        return "gateways/gateway-details";
    }

    @RequestMapping(value = "/admin/gateway-details/{id}")
    public String viewGatewayDetails(@PathVariable(value = "id") Long id, Model model, Locale locale) {

        GatewayImplementation gatewayImplementation;
        List<GatewayProperty> gatewayProperties;
        List<GatewayImplementationProperty> gatewayImplementationProperties;
        List<GatewayImplementationProperty> gipProd = new ArrayList<>();
        List<GatewayImplementationProperty> gipTest = new ArrayList<>();
        if (id > 0) {
            gatewayImplementation = gatewayImplementationService.findById(id);

            gatewayProperties = gatewayPropertyService.findByGatewayId(gatewayImplementation.getGatewayId());
            gatewayImplementationProperties = gatewayImplementationPropertyService.findByGatewayImplementationId(gatewayImplementation.getId());

            for (int i = 0; i < gatewayProperties.size(); i++) {
                GatewayProperty gp = gatewayProperties.get(i);
                gipProd.add(new GatewayImplementationProperty(gp.getId(), gp.getGatewayId(), gp.getId(), gp.getPropertyName(), "", false));
                gipTest.add(new GatewayImplementationProperty(gp.getId(), gp.getGatewayId(), gp.getId(), gp.getPropertyName(), "", true));
            }

            for (int i = 0; i < gatewayImplementationProperties.size(); i++) {
                for (int j = 0; j < gipProd.size(); j++) {
                    if (gatewayImplementationProperties.get(i).getPropertyId().equals(gipProd.get(j).getPropertyId())) {
                        if (!gatewayImplementationProperties.get(i).getPropertyTest()) {
                            gipProd.get(j).setId(gatewayImplementationProperties.get(i).getId());
                            gipProd.get(j).setPropertyValue(gatewayImplementationProperties.get(i).getPropertyValue());
                            break;
                        } else {
                            gipTest.get(j).setId(gatewayImplementationProperties.get(i).getId());
                            gipTest.get(j).setPropertyValue(gatewayImplementationProperties.get(i).getPropertyValue());
                            break;
                        }
                    }
                }
            }
        } else {
            return "redirect:/admin/gateways";
        }
        model.addAttribute("titulo", messageSource.getMessage("requests.gateway.details", null, locale));
        model.addAttribute("gatewayImplementation", gatewayImplementation);
        model.addAttribute("gipTest", gipTest);
        model.addAttribute("gipProd", gipProd);

        return "gateways/gateway-details";
    }

    @RequestMapping(value = "/admin/gateway-form", method = RequestMethod.GET)
    public String gatewayForm(Model model, Locale locale) {

        List<Gateway> gateways = gatewayService.findAllWithFilters();
        model.addAttribute("titulo", messageSource.getMessage("gateway.form.create", null, locale));
        model.addAttribute("gateways", gateways);

        return "gateways/gateway-form";
    }

    @RequestMapping(value = "/admin/save-gateway", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public String saveGateway(@RequestBody String json) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            GatewayImplementationAndPropertiesWrapper gipw = mapper.readValue(json, GatewayImplementationAndPropertiesWrapper.class);

            GatewayImplementation gi = gatewayImplementationService.findById(gipw.getGi().getId());
            gi.setEnabled(gipw.getGi().getEnabled());
            gi.setTest(gipw.getGi().getTest());
            gi.setProperties(Arrays.asList(gipw.getProperties()));

            List<GatewayProperty> gatewayProperties;
            List<GatewayImplementationProperty> gipProd = new ArrayList<>();

            gatewayProperties = gatewayPropertyService.findByGatewayId(gi.getGatewayId());
            for (int i = 0; i < gipw.getProperties().length; i++) {
                for (GatewayProperty g : gatewayProperties) {
                    if (gipw.getProperties()[i].getPropertyName().contains(g.getPropertyName())) {
                        gipProd.add(new GatewayImplementationProperty(gipw.getProperties()[i].getId(),
                                gipw.getProperties()[i].getGatewayImplementationId(),
                                g.getId(),
                                g.getPropertyName(),
                                gipw.getProperties()[i].getPropertyValue(),
                                gipw.getProperties()[i].getPropertyTest()));
                    }
                }
            }
            gatewayImplementationService.update(gi);
            gatewayImplementationPropertyService.saveAll(gipProd);

        } catch (Exception e) {
        }
        return "redirect:/admin/gateways";
    }

    @RequestMapping(value = "/admin/gateway-properties/{id}", method = RequestMethod.GET)
    public String gatewayForm(@PathVariable(value = "id") Long gatewayId, Model model) {

        List<GatewayProperty> gatewayProperties = gatewayPropertyService.findByGatewayId(gatewayId);
        List<GatewayImplementation> gatewayImplementations = gatewayImplementationService.findByGatewayId(gatewayId);
        List<String> countries = new LinkedList<>(countryService.findAll());

        gatewayImplementations.stream().forEach( x -> {
            if(countries.contains(x.getCountry())) {
                countries.remove(x.getCountry());
            }
        });
        model.addAttribute("countries", countries);
        model.addAttribute("gp", gatewayProperties);

        return "gateways/gateway-properties";
    }

    @RequestMapping(value = "/admin/delete-implementation/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteImplementation(@PathVariable(value = "id") Long id) {
        gatewayImplementationPropertyService.deleteByGatewayImplementationId(id);
        gatewayImplementationService.deleteById(id);
    }

    @RequestMapping(value = "/admin/create-gateway", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public void saveNewGateway(@RequestBody String json) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            GatewayImplementationAndPropertiesWrapper gipw = mapper.readValue(json, GatewayImplementationAndPropertiesWrapper.class);

            GatewayImplementation gi = gipw.getGi();
            gi.setProperties(Arrays.asList(gipw.getProperties()));

            gi = gatewayImplementationService.save(gi);

            for (GatewayImplementationProperty gip : gipw.getProperties()) {
                gip.setGatewayImplementationId(gi.getId());
            }
            gatewayImplementationPropertyService.saveAll(Arrays.asList(gipw.getProperties()));
            List<GatewayImplementationProperty> gipList = gatewayImplementationPropertyService.findByGatewayImplementationId(gi.getId());
            for (GatewayImplementationProperty giproperty : gi.getProperties()) {
                for (GatewayImplementationProperty property : gipList) {
                    if (giproperty.getPropertyId().equals(property.getPropertyId())) {
                        giproperty.setId(property.getId());
                    }
                }
            }
            gatewayImplementationService.save(gi);
        } catch (Exception e) {
        }
    }
}
