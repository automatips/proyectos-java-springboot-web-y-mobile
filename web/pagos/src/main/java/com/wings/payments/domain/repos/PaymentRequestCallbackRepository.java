/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.PaymentRequestsCallback;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface PaymentRequestCallbackRepository extends JpaRepository<PaymentRequestsCallback, Long> {

    
}
