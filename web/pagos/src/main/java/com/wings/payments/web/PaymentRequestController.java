/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.web;

import com.wings.payments.domain.bo.*;
import com.wings.payments.domain.dto.RequestDTO;
import com.wings.payments.domain.dto.RequestDTOExtra;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.repos.PaymentRequestExtraRepository;
import com.wings.payments.services.*;
import com.wings.payments.utils.StringUtils;

import java.security.Principal;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author seba
 */
@Controller
public class PaymentRequestController {

    @Autowired
    private PaymentRequestExtraRepository paymentRequestExtraRepository;

    @Autowired
    private PaymentRequestService paymentRequestService;

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    @Autowired
    private Environment environment;

    /**
     *
     * get with code in session
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request, HttpServletResponse response) {

        //prevent go back
        response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Expires", "0");

        String code;
        try {
            code = request.getSession().getAttribute("code").toString();
        } catch (Exception e) {
            return "pay-code";
        }

        PaymentRequest req = paymentRequestService.findByCode(code);
        if (req == null) {
            return "pay-code";
        }

        String country = req.getCountry();
        List<GatewayImplementation> gateways = gatewayImplementationRepository.findByCountryAndEnabled(country, Boolean.TRUE);

        //exclude bacs
        if (req.isExcludeBacs()) {
            List<GatewayImplementation> temp = new LinkedList<>();
            for (GatewayImplementation gi : gateways) {
                if (!gi.getGateway().getType().equals("bacs")) {
                    temp.add(gi);
                }
            }
            gateways.clear();
            gateways.addAll(temp);
        }

        model.addAttribute("req", req);
        model.addAttribute("gateways", gateways);
        model.addAttribute("showStatus", false);

        //marco el estado como mostrado para no volver a mostrar
        if (!req.isStatusShown() || req.getStatus().equals(PaymentStatus.COMPLETED.name())) {
            req.setStatusShown(true);
            paymentRequestService.save(req);
            model.addAttribute("showStatus", true);
        }

        return "index";
    }

    /**
     * get with code, put in session and redirect to /
     *
     * @param model
     * @param request
     * @param code
     * @return
     */
    @RequestMapping(value = {"/{code}"}, method = RequestMethod.GET)
    public String indexByCode(Model model, HttpServletRequest request, @PathVariable("code") String code) {

        if (code == null) {
            return "redirect:/";
        }

        PaymentRequest req;
        if (code.length() == 8) {
            req = paymentRequestService.findByPayCode(code);
        } else {
            req = paymentRequestService.findByCode(code);
        }

        if (req == null) {
            return "redirect:/";
        }

        request.getSession().setAttribute("code", req.getCode());

        String country = req.getCountry();
        List<GatewayImplementation> gateways = gatewayImplementationRepository.findByCountryAndEnabled(country, Boolean.TRUE);

        model.addAttribute("req", req);
        model.addAttribute("gateways", gateways);

        return "redirect:/";
    }

    /**
     * Post via form a pay code
     *
     * @param model
     * @param request
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String requestPayment(Model model, HttpServletRequest request) {

        String payCode = request.getParameter("payCode");
        if (payCode == null) {
            return "redirect:/";
        }

        PaymentRequest req = paymentRequestService.findByPayCode(payCode);

        if (req == null) {
            return "redirect:/";
        }

        request.getSession().setAttribute("code", req.getCode());

        String country = req.getCountry();
        List<GatewayImplementation> gateways = gatewayImplementationRepository.findByCountryAndEnabled(country, Boolean.TRUE);

        model.addAttribute("req", req);
        model.addAttribute("gateways", gateways);

        return "redirect:/";
    }

    /**
     * Post a new Payment request and return has code for that payment used by
     * API calls or client, no post directly
     *
     * @param dto
     * @return
     */
    @RequestMapping(value = "/b", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> requestPaymentWithBody(@RequestBody RequestDTO dto) {

        boolean test = false;
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (profile.contains("dev") || profile.contains("default")) {
                test = true;
                break;
            }
        }

        String requesterName = dto.getRequesterName();
        Long orderNumber = dto.getOrderNumber();
        String title = dto.getTitle();
        String description = dto.getDescription();
        Double amount = dto.getAmount();
        String currency = dto.getCurrency();
        String country = dto.getBillingCountry();
        String callbackUrl = dto.getCallbackUrl();
        String cancelUrl = dto.getCancelUrl();
        String image = dto.getImage();

        //parse extras
        List<PaymentRequestExtra> extras = new ArrayList<>();
        List<RequestDTOExtra> exs = dto.getExtras();
        for (RequestDTOExtra ex : exs) {
            PaymentRequestExtra extra = new PaymentRequestExtra();
            extra.setExtraName(ex.getExtraName());
            extra.setExtraValue(ex.getExtraValue());
            extras.add(extra);
        }

        //check if it's new request
        PaymentRequest paymentRequest = paymentRequestService.findByOrderNumberAndRequesterName(orderNumber, requesterName);
        if (paymentRequest == null) {
            paymentRequest = new PaymentRequest();
            paymentRequest.setOrderNumber(orderNumber);
            paymentRequest.setRequesterName(requesterName);
            paymentRequest.setStatus("PENDING");
            paymentRequest.setStatusMessage(null);
            paymentRequest.setStatusShown(true);
            paymentRequest.setPayCode(getUniquePayCode());
        }

        //set values
        paymentRequest.setAmount(amount);
        paymentRequest.setCallbackUrl(callbackUrl);
        paymentRequest.setCancelUrl(cancelUrl);
        paymentRequest.setCountry(country);
        paymentRequest.setCurrency(currency);
        paymentRequest.setDescription(description);
        paymentRequest.setImage(image);
        paymentRequest.setTitle(title);
        paymentRequest.setBillingAddress(dto.getBillingAddress());
        paymentRequest.setBillingCity(dto.getBillingCity());
        paymentRequest.setBillingCountry(dto.getBillingCountry());
        paymentRequest.setBillingEmail(dto.getBillingEmail());
        paymentRequest.setBillingFirstName(dto.getBillingFirstName());
        paymentRequest.setBillingLastName(dto.getBillingLastName());
        paymentRequest.setBillingState(dto.getBillingState());
        paymentRequest.setBillingState(dto.getBillingState());
        paymentRequest.setComsDiscountAmount(dto.getComsDiscountAmount());
        paymentRequest.setExcludeBacs(dto.isExcludeBacs());

        if (test) {
            paymentRequest.setBillingEmail("test@wingsmobile.com");
            paymentRequest.setBillingFirstName("test");
            paymentRequest.setBillingLastName("wings");
        }

        if (paymentRequest.getPayCode() == null) {
            paymentRequest.setPayCode(getUniquePayCode());
        }

        //save
        paymentRequestService.save(paymentRequest);
        final Long requestId = paymentRequest.getId();

        //set hash code
        String code = DigestUtils.sha256Hex(System.currentTimeMillis() + "dQxLF8ua" + requestId.toString());
        paymentRequest.setCode(code);
        paymentRequestService.save(paymentRequest);

        //delete all extras and insert them again
        paymentRequestExtraRepository.deleteByRequestId(requestId);
        extras.forEach((extra) -> {
            extra.setRequestId(requestId);
        });
        paymentRequestExtraRepository.saveAll(extras);

        //create response
        Map<String, String> resp = new HashMap<>();
        resp.put("status", "success");
        resp.put("message", code);

        return ResponseEntity.ok(resp);
    }

    /**
     * get with code in session
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/change"}, method = RequestMethod.GET)
    public String change(HttpServletRequest request) {
        request.getSession().removeAttribute("code");
        return "redirect:/";
    }

    /**
     * function to generate unique pay code
     *
     * @return
     */
    private String getUniquePayCode() {
        String ret = StringUtils.genPaymentCode();
        boolean loop = true;
        while (loop) {
            int count = paymentRequestService.countByPayCode(ret);
            if (count == 0) {
                loop = false;
            } else {
                ret = StringUtils.genPaymentCode();
            }
        }
        return ret;
    }

    @RequestMapping(value = "/regenerate-order-number/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void regenerateOrderNumber(@PathVariable Long id, Principal principal) {
        if (id != null && id > 0) {
            PaymentRequest pr = paymentRequestService.findById(id);
            if (pr != null) {
                pr.setOrderNumber(pr.getOrderNumber() * (-1));
                pr.setRegeneratedBy(principal.getName());
                pr.setRegeneratedAt(new Date());
                paymentRequestService.save(pr);
            }
        }
    }
}
