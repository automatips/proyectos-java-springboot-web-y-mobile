/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.PaymentRequestExtra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface PaymentRequestExtraRepository extends JpaRepository<PaymentRequestExtra, Long> {

    @Transactional
    int deleteByRequestId(Long requestId);
    
}
