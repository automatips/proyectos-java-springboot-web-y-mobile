package com.wings.payments.services.gateways;

import com.wings.payments.domain.bo.GatewayProperty;
import com.wings.payments.domain.repos.GatewayPropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GatewayPropertyService {

    @Autowired
    private GatewayPropertiesRepository gatewayPropertiesRepository;

    public List<GatewayProperty> findAll() {
        return gatewayPropertiesRepository.findAll();
    }

    public List<GatewayProperty> findByIdIn(List<Long> id) {
        return gatewayPropertiesRepository.findByIdIn(id);
    }

    public List<GatewayProperty> findByGatewayId(Long id) {
        return gatewayPropertiesRepository.findByGatewayId(id);
    }
}
