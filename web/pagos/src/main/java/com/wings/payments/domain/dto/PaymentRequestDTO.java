package com.wings.payments.domain.dto;

import java.util.List;

public class PaymentRequestDTO {

    private Long paymentRequestId;
    private Long orderNumber;
    private String gatewayImplementation;
    private String lastStatus;
    private List<TransactionDTO> transactions;

    public Long getPaymentRequestId() {
        return paymentRequestId;
    }

    public void setPaymentRequestId(Long paymentRequestId) {
        this.paymentRequestId = paymentRequestId;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getGatewayImplementation() {
        return gatewayImplementation;
    }

    public void setGatewayImplementation(String gatewayImplementation) {
        this.gatewayImplementation = gatewayImplementation;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public List<TransactionDTO> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDTO> transactions) {
        this.transactions = transactions;
    }
}
