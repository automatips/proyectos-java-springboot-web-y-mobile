(() =>{
    let repPt = document.querySelector("#rep-pt");
    let divElement = document.querySelector('#divCallbackValues');

    const ptId     = document.querySelector('#pt-Id');

    const renderCallback = async (htmlElement, endpoint) => {
        const resp = await fetch(endpoint);
        if( resp.ok) {
            htmlElement.innerHTML = await resp.text();
            setCallbackPaginator(htmlElement);
        }
    }

    const setCallbackPaginator = (htmlElement) => {
        const paginatorRedirect = htmlElement.querySelectorAll("[name^=pg-value]");
        paginatorRedirect.forEach( x => {
            x.addEventListener('click', async () =>{
                const endpoint = x.getAttribute('value');
                await renderCallback(htmlElement, endpoint).then( () => {
                    if(htmlElement === repPt) {
                        divElement.innerHTML = '';
                        callbacksStyles();
                    }
                });
            })
        })
    }

    const callbacksStyles = () => {
        const forms = document.querySelectorAll("[name^=callbackValues-]");

        const callbackStatus = document.querySelectorAll('[name="callbackStatus"]');
        callbackStatus.forEach( x => {
            if(x.innerHTML == 'FAILED' || x.innerHTML == 'FAILED_WRONG_SIGNATURE') {
                x.classList.add('label-danger');
            }
            if(x.innerHTML == 'SESSION_CREATED') {
                x.classList.add('label-warning');
            }
            if(x.innerHTML == 'COMPLETED') {
                x.classList.add('label-success');
            }
        });

        forms.forEach( x => {
            x.addEventListener(('click'), async () =>{
                forms.forEach( e => {
                    if( x !== e) {e.firstElementChild.setAttribute("class", "fa fa-folder");}
                });
                x.firstElementChild.setAttribute("class", "fa fa-folder-open");

                const endpoint = `/admin/callback-details/` +x.dataset.id;
                await renderCallback(divElement, endpoint);
            })
        });
    };

    const regenerate = () => {
        const btnRegenerate = document.querySelector(".regenerate-order");
        if(btnRegenerate != null ) {
            const popupTitle   = document.querySelector("#i18n_regenerate_order_number_title");
            const popupMessage = document.querySelector("#i18n_regenerate_order_number");
            const popupSuccessMessage = document.querySelector("#regeneratedOrder");
            const popupErrorMessage = document.querySelector('#i18n_regenerate_order_number_error');
            btnRegenerate.addEventListener("click", () => {
                swal({
                    title: popupTitle.innerHTML,
                    text: popupMessage.innerHTML,
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(async (willRegenerate) => {

                    if (willRegenerate) {
                        const prId = document.querySelector("#prId").dataset.identifier;
                        const orderNumber = document.querySelector("#prId").value;
                        const endpoint = `/regenerate-order-number/${prId}`;
                        const resp = await fetch(endpoint, {method:'POST'});
                        return (resp.ok) ? swal(popupSuccessMessage.innerHTML, {
                            icon: "success",
                            timer: 2000,
                            button: false
                        }).then( async () =>{
                            if (this.getState === undefined) {
                                window.location = `/admin/request-details/${orderNumber * -1}`;
                            }
                        }) : swal(popupErrorMessage.innerHTML, {
                            icon: "error",
                            button: false,
                            timer: 2000,
                        }).then( () =>{
                            if (this.getState === undefined) {
                                window.location = `/admin/request-details/${orderNumber}`;
                            }
                        });
                    }
                });
            });
        }};
    regenerate();

    if(ptId != null) {
        const firstCTendpoint = `/admin/callback-transactions/${ptId.getAttribute('value')}`;
        renderCallback(repPt, firstCTendpoint).then(callbacksStyles);
    }
})();