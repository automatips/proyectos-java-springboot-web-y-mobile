const addEvents = () => {
    const btnEdit = document.querySelectorAll('[name="editUser"]');
    const btnDelete = document.querySelectorAll('[name="deleteUser"]');

    for(let btn of btnEdit) {
        btn.addEventListener('mouseover', () =>{
            btn.setAttribute("style", "color: Dodgerblue;");
        })
        btn.addEventListener('mouseout', () =>{
            btn.removeAttribute("style");
        })

        btn.addEventListener('click', async () => {
            window.location.href=`/admin/user/${btn.getAttribute('value')}`;
        })
    }

    for(let btn of btnDelete) {
        btn.addEventListener('mouseover', () =>{
            btn.setAttribute("style", "color: Tomato;");
        })

        btn.addEventListener('mouseout', () =>{
            btn.removeAttribute("style");
        })

        btn.addEventListener('click', () => {

            const deleteUserTitle = document.querySelector('#deleteUserTitle').innerText;
            const deleteUserMessage = document.querySelector('#deleteUserMessage').innerText;
            const deleteUserConfirm = document.querySelector('#deleteUserConfirm').innerText;
            const deleteUserCancel = document.querySelector('#deleteUserCancel').innerText;
            const deleteUserSuccess = document.querySelector('#deleteUserSuccess').innerText;
            const deleteUserError = document.querySelector('#deleteUserError').innerText;

            swal({
                title: deleteUserTitle,
                text: deleteUserMessage,
                icon: 'warning',
                buttons: [deleteUserCancel, deleteUserConfirm],
                dangerMode: true,

            }).then(async (result) => {
                if (result) {
                    const resp = await fetch(`/admin/user/${btn.getAttribute('value')}`, {
                        method: 'PUT'
                    })
                    if(resp.ok) {
                        await swal(deleteUserSuccess, {
                            icon: "success",
                            button: false,
                            timer: 2000
                        });
                        window.location = '/admin/users';
                    } else {
                        swal(deleteUserError, {
                            icon: "error",
                            button: false,
                            timer: 2000
                        })
                    }
                }
            })
        })
    }
}

addEvents();