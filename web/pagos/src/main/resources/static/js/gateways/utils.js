const init = () => {
    const btnEdit = document.querySelector('#gateway-details-edit');
    const modified = document.querySelectorAll(".modified");
    const btnSave = document.querySelector("#save");
    const btnDelete = document.querySelector("#delete-implementation");

    btnEdit.addEventListener('click', () =>{
        let enabled = document.getElementById("gatewayImplEnabled");
        if (enabled) {
            enabled.className += enabled.className ? ' border' : 'border';
        }
        enabled.removeAttribute("disabled");
        enabled.setAttribute("style", "color: black;");

        let testImplementation = document.getElementById("gatewayImplTest");
        if (testImplementation) {
            testImplementation.className += enabled.className ? ' border' : 'border';
        }
        testImplementation.removeAttribute("disabled");
        testImplementation.setAttribute("style", "color: black;");

        let propertiesNames = document.querySelectorAll("[name^=property-valuename-]");
        propertiesNames.forEach(x => {
            x.setAttribute("contenteditable", "true");
            x.setAttribute("style", "color: black; background-color: #FAFAFA;");

            x.addEventListener('paste', function (event) {
                event.preventDefault();
                document.execCommand('inserttext', false, event.clipboardData.getData('text/plain'));
            });
        });

        let propertiesTestValues = document.querySelectorAll("[name^=property-test-]");
        propertiesTestValues.forEach(x => {
                if (x) {
                    x.className += x.className ? ' border' : 'border';
                }
                x.removeAttribute("disabled");
                x.setAttribute("style", "color: black;");
            }
        )
    });

    btnEdit.addEventListener('mouseover', () =>{
        btnEdit.setAttribute("style", "color: Dodgerblue;");
    })

    btnEdit.addEventListener('mouseout', () =>{
        btnEdit.removeAttribute("style");
    })

    btnSave.addEventListener('mouseover', () =>{
        btnSave.setAttribute("style", "color: Dodgerblue;");
    })

    btnSave.addEventListener('mouseout', () =>{
        btnSave.removeAttribute("style");
    })

    btnDelete.addEventListener('mouseover', () =>{
        btnDelete.setAttribute("style", "color: Tomato;");
    })

    btnDelete.addEventListener('mouseout', () =>{
        btnDelete.removeAttribute("style");
    })

    modified.forEach( e => {
        e.addEventListener('change', () =>{
            btnSave.removeAttribute("style");
        });
        e.addEventListener('keydown', () =>{
            btnSave.removeAttribute("style");
        })
    });

    btnSave.setAttribute("style", "opacity: 0.5; cursor: not-allowed; pointer-events: none;");

    const addGatewayCountry = () => {
        const gwCountry = document.querySelector('#gatewayFlag');

        const countries = [
            ['AR', 'argentina'],
            ['BO', 'bolivia'],
            ['PE', 'peru'],
            ['BR', 'brazil'],
            ['CL', 'chile'],
            ['CO', 'colombia'],
            ['CR', 'costa-rica'],
            ['DO', 'dominican-republic'],
            ['EC', 'ehcuador'],
            ['ES', 'spain'],
            ['GT', 'guatemala'],
            ['HN', 'honduras'],
            ['IT', 'italy'],
            ['MX', 'mexico'],
            ['OT', 'cancel-2'],
            ['PA', 'panama'],
            ['SV', 'el-salvador'],
            ['US', 'usa']
        ];

        const flags = 'https://img.icons8.com/color/40/000000/';
        let img = document.createElement('img');
        countries.forEach( country => {
            if(gwCountry.innerText === country[0]) {
                gwCountry.innerHTML = '';
                img.src = flags.concat(country[1].concat('.png'));
                gwCountry.appendChild(img);
            }
        })
    }

    addGatewayCountry();
}

init();