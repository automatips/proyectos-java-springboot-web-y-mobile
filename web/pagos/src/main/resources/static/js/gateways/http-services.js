const btnDeleteImpl = document.querySelector("#delete-implementation");
btnDeleteImpl.addEventListener('click', () =>{
    const popupTitle = document.querySelector("#i18n_delete_impl_title");
    const popupMessage = document.querySelector("#i18n_delete_impl_msg");
    const successDeleteImpl = document.querySelector("#i18n_delete_impl_msg_success");
    const errorDeleteImpl = document.querySelector("#i18n_delete_impl_msg_error");

    swal({
        title: popupTitle.innerHTML,
        text: popupMessage.innerHTML,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(async (willDelete) => {
        if (willDelete) {
            let id = $("#id").val();
            const resp = await fetch(`/admin/delete-implementation/${id}`,{
                method:'DELETE'
            });

            return (resp.ok) ? swal(successDeleteImpl.innerHTML, {
                icon: "success",
                button: false,
                timer: 2000,
            }).then( () =>{
                if (this.getState === undefined) {
                    window.location = '/admin/gateways';
                }
            }) : swal(errorDeleteImpl.innerHTML, {
                icon: "error",
                button: false,
                timer: 2000,
            }).then( () =>{
                if (this.getState === undefined) {
                    window.location = `/admin/gateway-details/${id}`;
                }
            });
        }
    });
})

const saveBtn = document.querySelector('#save');
saveBtn.addEventListener('click', () =>{
    const popupTitle = document.querySelector("#i18n_save_fields_title");
    const popupMessage = document.querySelector("#i18n_save_fields_message");
    const successSave = document.querySelector("#i18n_save_msg_success");
    const errorSave = document.querySelector("#i18n_save_msg_error");

    swal({
        title: popupTitle.innerHTML,
        text: popupMessage.innerHTML,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(async (willSave) => {
        if (willSave) {
            const id = $("#id").val();
            const enabled = document.querySelector("#gatewayImplEnabled").value == "1"? "true":"false";
            const testImplementation = document.querySelector("#gatewayImplTest").value == "1"? "true":"false";

            const gi = {
                id: id,
                enabled: enabled,
                test: testImplementation
            };

            const propertiesProdList = document.querySelectorAll("[name^=property-valuename-prod]");
            const propertiesTestList = document.querySelectorAll(("[name^=property-valuename-test-]"));
            let dataProperties = [];

            for (let i= 0; i < propertiesProdList.length; i++) {
                const gipID = propertiesProdList[i].dataset.id;
                const propertyName = propertiesProdList[i].getAttribute("name");
                const propertyValue = propertiesProdList[i].textContent;
                const obj = {id: gipID, gatewayImplementationId: id, propertyName: propertyName, propertyValue: propertyValue, propertyTest: false};
                dataProperties.push(obj);
            }

            for (let i= 0; i < propertiesTestList.length; i++) {
                const gipID = propertiesTestList[i].dataset.id;
                const propertyName = propertiesTestList[i].getAttribute("name");
                const propertyValue = propertiesTestList[i].textContent;
                const obj = {id: gipID, gatewayImplementationId: id, propertyName: propertyName, propertyValue: propertyValue, propertyTest: true};
                dataProperties.push(obj);
            }

            const finalObject = {
                gi: gi,
                properties: dataProperties
            };

            const endpoint = "/admin/save-gateway";

            const resp = await fetch(endpoint, {
                method:'PUT',
                body: JSON.stringify(finalObject),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })

            return (resp.ok) ? swal(successSave.innerHTML, {
                icon: "success",
                button: false,
                timer: 2000,
            }).then( () =>{
                if (this.getState === undefined) {
                    window.location.reload();
                }
            }) : swal(errorSave.innerHTML, {
                icon: "error",
                button: false,
                timer: 2000,
            }).then( () =>{
                if (this.getState === undefined) {
                    window.location.reload();
                }
            });
        }
    })
});