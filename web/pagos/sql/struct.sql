
--create database wings_payments;

DROP TABLE payment_requests;
CREATE TABLE `payment_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `requester_name` varchar(255) default  NULL,
  `order_number` bigint(20) NOT NULL,
  `title` varchar(250) default NULL,
  `description` text default null,
  `amount` decimal(24,5) default 0,
  `currency` varchar(5) DEFAULT 'EUR',
  `country` varchar(5) DEFAULT 'ES',
  `callback_url` varchar(255) DEFAULT NULL,
  `cancel_url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `preferred_gateway` varchar(255) DEFAULT NULL,
  `exclude_bacs` tinyint(1) DEFAULT 0,
  `status` varchar(150) DEFAULT NULL,
  `status_message` text DEFAULT NULL,    
  `status_shown` tinyint(1) DEFAULT 0,
  `status_date` DATETIME,
  `processor` varchar(150) DEFAULT NULL,
  `payment_type` varchar(50) default null,
  `billing_first_name` varchar(250) default NULL,
  `billing_last_name` varchar(250) default NULL,
  `billing_email` varchar(250) default NULL,
  `billing_address` varchar(250) default NULL,
  `billing_city` varchar(250) default NULL,
  `billing_country` varchar(250) default NULL,
  `billing_state` varchar(250) default NULL,
  `hash_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `payment_request_extras` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` bigint(20) NOT NULL,
  `extra_name` varchar(255) default  NULL,
  `extra_value` varchar(255) NOT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `gateways` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) default  NULL,
  `code` varchar(255) default  NULL,
  `image` varchar(255) DEFAULT NULL,
  `available_countries` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL, -- redirect - info
  `service_class_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `gateway_properties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gateway_id` bigint(20),
  `property_name` varchar(255) default  NULL,
  `property_type` varchar(50) default  NULL, 
  `property_enum` varchar(255) default  NULL,
  `property_default` varchar(255) default  NULL,
  `property_description` varchar(255) default  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `gateway_implementation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gateway_id` bigint(20),
  `country` varchar(5) default  NULL,
  `name` varchar(50) default  NULL, 
  `enabled` tinyint(1) default  0,
  `test` tinyint(1) default  0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `payment_requests_callbacks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_request_id` bigint(20) NOT NULL,
  `response_status` int DEFAULT NULL,
  `response_status_message` text DEFAULT NULL,
  `tries` int DEFAULT NULL,
  `resend` tinyint(1) DEFAULT 0,      
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


drop TABLE `gateway_implementation_properties`;
CREATE TABLE `gateway_implementation_properties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gateway_implementation_id` bigint(20),
  `property_id` bigint(20) default  NULL,
  `property_name` varchar(50) default  NULL, 
  `property_value` text default  NULL,
  `property_test` tinyint(1) default  0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE `payment_transaction`;
CREATE TABLE `payment_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_request_id` bigint(20),
  `gateway_implementantion_id` bigint(20),
  `last_status` varchar(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `payment_transaction_callbacks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_transaction_id` bigint(20),
  `callback_date` DATETIME,
  `reported_date` DATETIME,
  `status` varchar(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `payment_transaction_callbacks_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `callback_id` bigint(20),
  `val_name` varchar(150),
  `val_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--Gateways
INSERT INTO wings_payments.gateways (`name`, code, image, available_countries, `type`) 
	VALUES 
            ('Transferencia Bancaria Directa', 'bacs', 'fa fa-university', 'ES,CO,EC,PE', 'bacs'),
            ('PayU Latam', 'payu', 'fa fa-credit-card', 'CO,PE', 'card'),
            ('Culqi', 'culqi', 'fa fa-credit-card', 'PE', 'card'),
            ('TefPay', 'tefpay', 'fa fa-credit-card', 'ES', 'card'),
            ('Stripe', 'stripe', 'fa fa-credit-card', 'ES', 'card'),
            ('Paymentez', 'paymentez', 'fa fa-credit-card', 'EC', 'card'),
            ('Paypal', 'paypal', 'fa fa-paypal', 'ES', 'other'),
            ('Bitwings', 'bwn', 'fa fa-wallet', 'ES', 'crypto'),
            ('Bitcoins', 'bcn', 'fa fa-wallet', 'ES', 'crypto'),
            ('Ethereum', 'eth', 'fa fa-wallet', 'ES', 'crypto');


--PROPERTIES

--BACS
INSERT INTO wings_payments.gateway_properties (gateway_id, property_name, property_type, property_enum, property_default, property_description) 
	VALUES (1, 'bank_name', 'string', NULL, NULL, 'Nombre del banco'),
	(1, 'account_number', 'string', NULL, NULL, 'Número de cuenta'),
	(1, 'account_iban', 'string', NULL, NULL, 'Número IBAN'),
	(1, 'account_swift', 'string', NULL, NULL, 'Código Swift'),
	(1, 'extra_information', 'string', NULL, NULL, 'Informacion extra para el cliente'),
	(1, 'account_name', 'string', NULL, NULL, 'Nombre de la cuenta');

--PayU
INSERT INTO wings_payments.gateway_properties (gateway_id, property_name, property_type, property_enum, property_default, property_description) 
	VALUES (2, 'apiKey', 'string', NULL, NULL, 'API key, se toma del backoffice de PayU'),
               (2, 'merchantId', 'int', NULL, NULL, 'MerchantId, se toma del backoffice de PayU'),
               (2, 'accountId', 'int', NULL, NULL, 'AccountId, se toma del backoffice de PayU'),
               (2, 'test', 'boolean', NULL, NULL, 'test, si es una request de test o no'),
               (2, 'postUrl', 'string', NULL, NULL, 'La url a donde hacer POST');

---------implementaciones------------------------
--BACS - ALL
INSERT INTO wings_payments.gateway_implementation (gateway_id, country, `name`, enabled) 
	VALUES 
            (1, 'PE', 'Transferencia Bancaria Directa', true),
            (1, 'ES', 'Transferencia Bancaria Directa', true),
            (1, 'CO', 'Transferencia Bancaria Directa', true),
            (1, 'EC', 'Transferencia Bancaria Directa', true);
--PERU
INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES (1, '1', 'bank_name', 'SCOTIABANK', false),
	VALUES (1, '2', 'account_number', 'Cuenta de Depósito 036-7588316', false);
	VALUES (1, '5', 'extra_information', 'Por favor, usa el número del pedido como referencia de pago. 
Tu pedido no se procesará hasta que se haya recibido el importe en nuestra cuenta. 
Una vez realizado el depósito por favor enviar copia del comprobante a  <a href="mailto:atencionalcliente@wingsmobile.es">atencionalcliente@wingsmobile.es</a>
 indicando el número de pedido.', false),
	VALUES (1, '6', 'account_name', 'WINGSMOBILE PERÚ SAC', false);

--ESPAÑA
INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES (2, '5', 'extra_information', 'Por favor, usa el número del pedido como referencia de pago. 
Tu pedido no se procesará hasta que se haya recibido el importe en nuestra cuenta. 
Una vez realizado el depósito por favor enviar copia del comprobante a  <a href="mailto:atencionalcliente@wingsmobile.es">atencionalcliente@wingsmobile.es</a>
 indicando el número de pedido.', false),
            (2, '6', 'account_name', 'Wings Mobile Telecom SL', false),
            (2, '3', 'account_iban', 'ES27 0075 6002 3406 0036 6201', false);
--COLOMBIA
INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES (3, '5', 'extra_information', 'Por favor, usa el número del pedido como referencia de pago. 
Tu pedido no se procesará hasta que se haya recibido el importe en nuestra cuenta. 
Una vez realizado el depósito por favor enviar copia del comprobante a  <a href="mailto:atencionalcliente@wingsmobile.es">atencionalcliente@wingsmobile.es</a>
 indicando el número de pedido.', false),
            (3, '6', 'account_name', 'Wings Mobile Colombia', false),
            (3, '2', 'account_number', '76096183026', false),
            (3, '1', 'bank_name', 'Bancolombia', false);
--Ecuador
INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES (4, '5', 'extra_information', 'Por favor, usa el número del pedido como referencia de pago. 
Tu pedido no se procesará hasta que se haya recibido el importe en nuestra cuenta. 
Una vez realizado el depósito por favor enviar copia del comprobante a  <a href="mailto:atencionalcliente@wingsmobile.es">atencionalcliente@wingsmobile.es</a>
 indicando el número de pedido.', false),
            (4, '6', 'account_name', 'WingsMobile SA', false),
            (4, '2', 'account_number', '130063271-2', false),
            (4, '1', 'bank_name', 'Banco Internacional', false);

--PayU PE
INSERT INTO wings_payments.gateway_implementation (gateway_id, country, `name`, enabled) 
	VALUES (2, 'PE', 'PayU Latam', true);

INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES 
            (5, '7', 'apiKey', '4Vj8eK4rloUd272L48hsrarnUA', true),
            (5, '8', 'merchantId', '508029', true),
            (5, '9', 'accountId', '512323', true),
            (5, '11', 'postUrl', 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/', true),
            (5, '7', 'apiKey', 'xYU8FlKeW3BJ8o7Nd33z46paQ1', false),
            (5, '8', 'merchantId', '763599', false),
            (5, '9', 'accountId', '770185', false),
            (5, '11', 'postUrl', 'https://checkout.payulatam.com/ppp-web-gateway-payu/', false);

--PayU COLOMBIA
INSERT INTO wings_payments.gateway_implementation (gateway_id, country, `name`, enabled) 
	VALUES (2, 'CO', 'PayU Latam', true);

INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES 
            (6, '7', 'apiKey', '4Vj8eK4rloUd272L48hsrarnUA', true),
            (6, '8', 'merchantId', '508029', true),
            (6, '9', 'accountId', '512321', true),
            (6, '11', 'postUrl', 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/', true),
            (6, '7', 'apiKey', 'i3qje6rEZQ3lctu0XrfckkrBzT', false),
            (6, '8', 'merchantId', '760816', false),
            (6, '9', 'accountId', '767371', false),
            (6, '11', 'postUrl', 'https://checkout.payulatam.com/ppp-web-gateway-payu/', false);


