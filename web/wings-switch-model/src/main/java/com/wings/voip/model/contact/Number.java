 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model.contact;

import java.util.Objects;

/**
 *
 * @author seba
 */
public class Number {

    private String label;
    /**
     * 5493518039844
     */
    private String number;
    /**
     * +54 9 351 803-9844
     */
    private String formattedNumber;
    /**
     * 8039844
     */
    private String rawNumber;

    /**
     * version
     */
    private int version;

    /**
     * used by server to tell phone
     * to add mime types
     */
    private boolean free = false;
    private boolean secure = false;

    /**
     * Use by phone to tell the server
     * if it's sync or not, should delete, etc,
     */
    private boolean inPhoneAccount = false;
    private boolean inWingsAccount = false;

    /**
     * user by phone and sync adapter if we have to create mimes
     */
    private boolean hasMimeFree = false;
    private boolean hasMimeWings = false;
    private boolean hasMimeSecure = false;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFormattedNumber() {
        return formattedNumber;
    }

    public String getRawNumber() {
        return rawNumber;
    }

    public void setRawNumber(String rawNumber) {
        this.rawNumber = rawNumber;
    }

    public long getLongNumber() {
        if (getNumber() != null) {
            try {
                return Long.valueOf(getNumber());                
            } catch (Exception e) {
            }
        }
        return -1;
    }

    public void setFormattedNumber(String formattedNumber) {
        this.formattedNumber = formattedNumber;
    }

    public String getNumberForSip() {
        if (getNumber() != null) {
            return getNumber()
                    .replaceAll("\\+", "")
                    .replace(" ", "")
                    .replace("-", "");
        }
        return null;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public boolean isFree() {
        return free;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public boolean isSecure() {
        return secure;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isInPhoneAccount() {
        return inPhoneAccount;
    }

    public void setInPhoneAccount(boolean inPhoneAccount) {
        this.inPhoneAccount = inPhoneAccount;
    }

    public boolean isInWingsAccount() {
        return inWingsAccount;
    }

    public void setInWingsAccount(boolean inWingsAccount) {
        this.inWingsAccount = inWingsAccount;
    }

    public boolean isHasMimeFree() {
        return hasMimeFree;
    }

    public void setHasMimeFree(boolean hasMimeFree) {
        this.hasMimeFree = hasMimeFree;
    }

    public boolean isHasMimeWings() {
        return hasMimeWings;
    }

    public void setHasMimeWings(boolean hasMimeWings) {
        this.hasMimeWings = hasMimeWings;
    }

    public boolean isHasMimeSecure() {
        return hasMimeSecure;
    }

    public void setHasMimeSecure(boolean hasMimeSecure) {
        this.hasMimeSecure = hasMimeSecure;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.label);
        hash = 53 * hash + Objects.hashCode(this.number);
        hash = 53 * hash + Objects.hashCode(this.formattedNumber);
        hash = 53 * hash + Objects.hashCode(this.rawNumber);
        hash = 53 * hash + this.version;
        hash = 53 * hash + (this.free ? 1 : 0);
        hash = 53 * hash + (this.secure ? 1 : 0);
        hash = 53 * hash + (this.inPhoneAccount ? 1 : 0);
        hash = 53 * hash + (this.inWingsAccount ? 1 : 0);
        hash = 53 * hash + (this.hasMimeFree ? 1 : 0);
        hash = 53 * hash + (this.hasMimeWings ? 1 : 0);
        hash = 53 * hash + (this.hasMimeSecure ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Number other = (Number) obj;
        if (this.version != other.version) {
            return false;
        }
        if (this.free != other.free) {
            return false;
        }
        if (this.secure != other.secure) {
            return false;
        }
        if (this.inPhoneAccount != other.inPhoneAccount) {
            return false;
        }
        if (this.inWingsAccount != other.inWingsAccount) {
            return false;
        }
        if (this.hasMimeFree != other.hasMimeFree) {
            return false;
        }
        if (this.hasMimeWings != other.hasMimeWings) {
            return false;
        }
        if (this.hasMimeSecure != other.hasMimeSecure) {
            return false;
        }
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        if (!Objects.equals(this.formattedNumber, other.formattedNumber)) {
            return false;
        }
        if (!Objects.equals(this.rawNumber, other.rawNumber)) {
            return false;
        }
        return true;
    }

    

   
}
