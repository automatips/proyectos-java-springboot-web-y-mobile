/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model.requests;

import com.wings.voip.model.contact.Contact;
import java.util.List;

/**
 *
 * @author seba
 */
public class SyncContactResponse {
    
    private String login;
    private List<Contact> contacts;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
    
}
