/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model.requests;

import com.wings.voip.model.AccountStatus;
import java.util.Map;

/**
 *
 * @author seba
 */
public class RegisterResponse {
    
    private Map<String, String> provisioning;
    private AccountStatus accountStatus;

    
    public void setProvisioning(Map<String, String> provisioning) {
        this.provisioning = provisioning;
    }

    public Map<String, String> getProvisioning() {
        return provisioning;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    
}
