
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="num_Guia" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="num_GuiaFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ide_CodFacturacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sFormatoImpresionGuia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Id_ArchivoCargar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bytesReport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numGuia",
    "numGuiaFinal",
    "ideCodFacturacion",
    "sFormatoImpresionGuia",
    "idArchivoCargar",
    "interno",
    "bytesReport"
})
@XmlRootElement(name = "GenerarGuiaSticker")
public class GenerarGuiaSticker {

    @XmlElement(name = "num_Guia", required = true)
    protected BigDecimal numGuia;
    @XmlElement(name = "num_GuiaFinal", required = true)
    protected BigDecimal numGuiaFinal;
    @XmlElement(name = "ide_CodFacturacion")
    protected String ideCodFacturacion;
    protected int sFormatoImpresionGuia;
    @XmlElement(name = "Id_ArchivoCargar")
    protected String idArchivoCargar;
    protected boolean interno;
    protected byte[] bytesReport;

    /**
     * Obtiene el valor de la propiedad numGuia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuia() {
        return numGuia;
    }

    /**
     * Define el valor de la propiedad numGuia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuia(BigDecimal value) {
        this.numGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad numGuiaFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuiaFinal() {
        return numGuiaFinal;
    }

    /**
     * Define el valor de la propiedad numGuiaFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuiaFinal(BigDecimal value) {
        this.numGuiaFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad ideCodFacturacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeCodFacturacion() {
        return ideCodFacturacion;
    }

    /**
     * Define el valor de la propiedad ideCodFacturacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeCodFacturacion(String value) {
        this.ideCodFacturacion = value;
    }

    /**
     * Obtiene el valor de la propiedad sFormatoImpresionGuia.
     * 
     */
    public int getSFormatoImpresionGuia() {
        return sFormatoImpresionGuia;
    }

    /**
     * Define el valor de la propiedad sFormatoImpresionGuia.
     * 
     */
    public void setSFormatoImpresionGuia(int value) {
        this.sFormatoImpresionGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivoCargar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivoCargar() {
        return idArchivoCargar;
    }

    /**
     * Define el valor de la propiedad idArchivoCargar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivoCargar(String value) {
        this.idArchivoCargar = value;
    }

    /**
     * Obtiene el valor de la propiedad interno.
     * 
     */
    public boolean isInterno() {
        return interno;
    }

    /**
     * Define el valor de la propiedad interno.
     * 
     */
    public void setInterno(boolean value) {
        this.interno = value;
    }

    /**
     * Obtiene el valor de la propiedad bytesReport.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBytesReport() {
        return bytesReport;
    }

    /**
     * Define el valor de la propiedad bytesReport.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBytesReport(byte[] value) {
        this.bytesReport = value;
    }

}
