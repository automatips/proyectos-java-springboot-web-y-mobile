
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTrazaAuditoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuarioTrazaAuditoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="urlTrazaAuditoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mensaje",
    "idTrazaAuditoria",
    "usuarioTrazaAuditoria",
    "urlTrazaAuditoria"
})
@XmlRootElement(name = "ConsultaRangosGuias")
public class ConsultaRangosGuias {

    @XmlElement(name = "Mensaje")
    protected String mensaje;
    protected String idTrazaAuditoria;
    protected String usuarioTrazaAuditoria;
    protected String urlTrazaAuditoria;

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad idTrazaAuditoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTrazaAuditoria() {
        return idTrazaAuditoria;
    }

    /**
     * Define el valor de la propiedad idTrazaAuditoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTrazaAuditoria(String value) {
        this.idTrazaAuditoria = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioTrazaAuditoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioTrazaAuditoria() {
        return usuarioTrazaAuditoria;
    }

    /**
     * Define el valor de la propiedad usuarioTrazaAuditoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioTrazaAuditoria(String value) {
        this.usuarioTrazaAuditoria = value;
    }

    /**
     * Obtiene el valor de la propiedad urlTrazaAuditoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlTrazaAuditoria() {
        return urlTrazaAuditoria;
    }

    /**
     * Define el valor de la propiedad urlTrazaAuditoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlTrazaAuditoria(String value) {
        this.urlTrazaAuditoria = value;
    }

}
