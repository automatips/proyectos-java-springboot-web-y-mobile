
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EnviosExterno complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EnviosExterno">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Num_Guia" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Sobreporte" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_SobreCajaPorte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fec_Creacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fec_TiempoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Doc_Relacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Piezas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_TipoTrayecto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Ide_Producto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Ide_Destinatarios" type="{http://microsoft.com/wsdl/types/}guid"/>
 *         &lt;element name="Ide_CodFacturacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_Manifiesto" type="{http://microsoft.com/wsdl/types/}guid"/>
 *         &lt;element name="Des_FormaPago" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_MedioTransporte" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Num_PesoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_ValorDeclaradoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_VolumenTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_BolsaSeguridad" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Precinto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Des_TipoDuracionTrayecto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_Telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_Ciudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_Direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nom_Contacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_VlrCampoPersonalizado1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_ValorLiquidado" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Des_DiceContener" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_TipoGuia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_CiudadRemitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_VlrSobreflete" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_VlrFlete" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Descuento" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Des_DireccionRecogida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_TelefonoRecogida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_CiudadRecogida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idePaisOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idePaisDestino" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_IdArchivoOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_Num_Identific_Dest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_Num_Referencia_Dest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_DireccionRemitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_PesoFacturado" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Nom_TipoTrayecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Est_CanalMayorista" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Nom_Remitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_IdentiRemitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_TelefonoRemitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Alto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Num_Ancho" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Num_Largo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_CiudadOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_DiceContenerSobre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_DepartamentoDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_DepartamentoOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Gen_Cajaporte" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Gen_Sobreporte" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Nom_UnidadEmpaque" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nom_RemitenteCanal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_UnidadLongitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_UnidadPeso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_ValorDeclaradoSobreTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Factura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_CorreoElectronico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Celular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Id_ArchivoCargar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Recaudo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id_zonificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_codigopostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rem_codigopostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CentroCosto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID_CANAL_DISTRIBUCION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CASILLERO_POSTAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="objEnviosUnidadEmpaqueCargue" type="{http://tempuri.org/}ArrayOfEnviosUnidadEmpaqueCargue" minOccurs="0"/>
 *         &lt;element name="Est_EnviarCorreo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Recoleccion_Esporadica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo_Doc_Destinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombrecontacto_remitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celular_remitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="correo_remitente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Serv_Externo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Retorno_Digital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnviosExterno", propOrder = {
    "numGuia",
    "numSobreporte",
    "numSobreCajaPorte",
    "fecCreacion",
    "fecTiempoEntrega",
    "docRelacionado",
    "numPiezas",
    "desTipoTrayecto",
    "ideProducto",
    "ideDestinatarios",
    "ideCodFacturacion",
    "ideManifiesto",
    "desFormaPago",
    "desMedioTransporte",
    "numPesoTotal",
    "numValorDeclaradoTotal",
    "numVolumenTotal",
    "numBolsaSeguridad",
    "numPrecinto",
    "desTipoDuracionTrayecto",
    "desTelefono",
    "desCiudad",
    "desDireccion",
    "nomContacto",
    "desVlrCampoPersonalizado1",
    "numValorLiquidado",
    "desDiceContener",
    "desTipoGuia",
    "desCiudadRemitente",
    "numVlrSobreflete",
    "numVlrFlete",
    "numDescuento",
    "desDireccionRecogida",
    "desTelefonoRecogida",
    "desCiudadRecogida",
    "idePaisOrigen",
    "idePaisDestino",
    "desIdArchivoOrigen",
    "ideNumIdentificDest",
    "ideNumReferenciaDest",
    "desDireccionRemitente",
    "numPesoFacturado",
    "nomTipoTrayecto",
    "estCanalMayorista",
    "nomRemitente",
    "numIdentiRemitente",
    "numTelefonoRemitente",
    "numAlto",
    "numAncho",
    "numLargo",
    "desCiudadOrigen",
    "desDiceContenerSobre",
    "desDepartamentoDestino",
    "desDepartamentoOrigen",
    "genCajaporte",
    "genSobreporte",
    "nomUnidadEmpaque",
    "nomRemitenteCanal",
    "desUnidadLongitud",
    "desUnidadPeso",
    "numValorDeclaradoSobreTotal",
    "numFactura",
    "desCorreoElectronico",
    "numCelular",
    "idArchivoCargar",
    "numRecaudo",
    "idZonificacion",
    "desCodigopostal",
    "remCodigopostal",
    "centroCosto",
    "idcanaldistribucion",
    "casilleropostal",
    "objEnviosUnidadEmpaqueCargue",
    "estEnviarCorreo",
    "recoleccionEsporadica",
    "tipoDocDestinatario",
    "nombrecontactoRemitente",
    "celularRemitente",
    "correoRemitente",
    "servExterno",
    "retornoDigital"
})
public class EnviosExterno {

    @XmlElement(name = "Num_Guia", required = true)
    protected BigDecimal numGuia;
    @XmlElement(name = "Num_Sobreporte", required = true)
    protected BigDecimal numSobreporte;
    @XmlElement(name = "Num_SobreCajaPorte")
    protected String numSobreCajaPorte;
    @XmlElement(name = "Fec_Creacion")
    protected String fecCreacion;
    @XmlElement(name = "Fec_TiempoEntrega")
    protected String fecTiempoEntrega;
    @XmlElement(name = "Doc_Relacionado")
    protected String docRelacionado;
    @XmlElement(name = "Num_Piezas")
    protected int numPiezas;
    @XmlElement(name = "Des_TipoTrayecto")
    protected int desTipoTrayecto;
    @XmlElement(name = "Ide_Producto")
    protected int ideProducto;
    @XmlElement(name = "Ide_Destinatarios", required = true)
    protected String ideDestinatarios;
    @XmlElement(name = "Ide_CodFacturacion")
    protected String ideCodFacturacion;
    @XmlElement(name = "Ide_Manifiesto", required = true)
    protected String ideManifiesto;
    @XmlElement(name = "Des_FormaPago")
    protected int desFormaPago;
    @XmlElement(name = "Des_MedioTransporte")
    protected int desMedioTransporte;
    @XmlElement(name = "Num_PesoTotal", required = true)
    protected BigDecimal numPesoTotal;
    @XmlElement(name = "Num_ValorDeclaradoTotal", required = true)
    protected BigDecimal numValorDeclaradoTotal;
    @XmlElement(name = "Num_VolumenTotal", required = true)
    protected BigDecimal numVolumenTotal;
    @XmlElement(name = "Num_BolsaSeguridad", required = true)
    protected BigDecimal numBolsaSeguridad;
    @XmlElement(name = "Num_Precinto", required = true)
    protected BigDecimal numPrecinto;
    @XmlElement(name = "Des_TipoDuracionTrayecto")
    protected int desTipoDuracionTrayecto;
    @XmlElement(name = "Des_Telefono")
    protected String desTelefono;
    @XmlElement(name = "Des_Ciudad")
    protected String desCiudad;
    @XmlElement(name = "Des_Direccion")
    protected String desDireccion;
    @XmlElement(name = "Nom_Contacto")
    protected String nomContacto;
    @XmlElement(name = "Des_VlrCampoPersonalizado1")
    protected String desVlrCampoPersonalizado1;
    @XmlElement(name = "Num_ValorLiquidado", required = true)
    protected BigDecimal numValorLiquidado;
    @XmlElement(name = "Des_DiceContener")
    protected String desDiceContener;
    @XmlElement(name = "Des_TipoGuia")
    protected int desTipoGuia;
    @XmlElement(name = "Des_CiudadRemitente")
    protected String desCiudadRemitente;
    @XmlElement(name = "Num_VlrSobreflete", required = true)
    protected BigDecimal numVlrSobreflete;
    @XmlElement(name = "Num_VlrFlete", required = true)
    protected BigDecimal numVlrFlete;
    @XmlElement(name = "Num_Descuento", required = true)
    protected BigDecimal numDescuento;
    @XmlElement(name = "Des_DireccionRecogida")
    protected String desDireccionRecogida;
    @XmlElement(name = "Des_TelefonoRecogida")
    protected String desTelefonoRecogida;
    @XmlElement(name = "Des_CiudadRecogida")
    protected String desCiudadRecogida;
    protected int idePaisOrigen;
    protected int idePaisDestino;
    @XmlElement(name = "Des_IdArchivoOrigen")
    protected String desIdArchivoOrigen;
    @XmlElement(name = "Ide_Num_Identific_Dest")
    protected String ideNumIdentificDest;
    @XmlElement(name = "Ide_Num_Referencia_Dest")
    protected String ideNumReferenciaDest;
    @XmlElement(name = "Des_DireccionRemitente")
    protected String desDireccionRemitente;
    @XmlElement(name = "Num_PesoFacturado", required = true)
    protected BigDecimal numPesoFacturado;
    @XmlElement(name = "Nom_TipoTrayecto")
    protected String nomTipoTrayecto;
    @XmlElement(name = "Est_CanalMayorista")
    protected boolean estCanalMayorista;
    @XmlElement(name = "Nom_Remitente")
    protected String nomRemitente;
    @XmlElement(name = "Num_IdentiRemitente")
    protected String numIdentiRemitente;
    @XmlElement(name = "Num_TelefonoRemitente")
    protected String numTelefonoRemitente;
    @XmlElement(name = "Num_Alto")
    protected int numAlto;
    @XmlElement(name = "Num_Ancho")
    protected int numAncho;
    @XmlElement(name = "Num_Largo")
    protected int numLargo;
    @XmlElement(name = "Des_CiudadOrigen")
    protected String desCiudadOrigen;
    @XmlElement(name = "Des_DiceContenerSobre")
    protected String desDiceContenerSobre;
    @XmlElement(name = "Des_DepartamentoDestino")
    protected String desDepartamentoDestino;
    @XmlElement(name = "Des_DepartamentoOrigen")
    protected String desDepartamentoOrigen;
    @XmlElement(name = "Gen_Cajaporte")
    protected boolean genCajaporte;
    @XmlElement(name = "Gen_Sobreporte")
    protected boolean genSobreporte;
    @XmlElement(name = "Nom_UnidadEmpaque")
    protected String nomUnidadEmpaque;
    @XmlElement(name = "Nom_RemitenteCanal")
    protected String nomRemitenteCanal;
    @XmlElement(name = "Des_UnidadLongitud")
    protected String desUnidadLongitud;
    @XmlElement(name = "Des_UnidadPeso")
    protected String desUnidadPeso;
    @XmlElement(name = "Num_ValorDeclaradoSobreTotal")
    protected String numValorDeclaradoSobreTotal;
    @XmlElement(name = "Num_Factura")
    protected String numFactura;
    @XmlElement(name = "Des_CorreoElectronico")
    protected String desCorreoElectronico;
    @XmlElement(name = "Num_Celular")
    protected String numCelular;
    @XmlElement(name = "Id_ArchivoCargar")
    protected String idArchivoCargar;
    @XmlElement(name = "Num_Recaudo")
    protected String numRecaudo;
    @XmlElement(name = "id_zonificacion")
    protected String idZonificacion;
    @XmlElement(name = "Des_codigopostal")
    protected String desCodigopostal;
    @XmlElement(name = "Rem_codigopostal")
    protected String remCodigopostal;
    @XmlElement(name = "CentroCosto")
    protected String centroCosto;
    @XmlElement(name = "ID_CANAL_DISTRIBUCION")
    protected String idcanaldistribucion;
    @XmlElement(name = "CASILLERO_POSTAL")
    protected String casilleropostal;
    protected ArrayOfEnviosUnidadEmpaqueCargue objEnviosUnidadEmpaqueCargue;
    @XmlElement(name = "Est_EnviarCorreo")
    protected boolean estEnviarCorreo;
    @XmlElement(name = "Recoleccion_Esporadica")
    protected String recoleccionEsporadica;
    @XmlElement(name = "Tipo_Doc_Destinatario")
    protected String tipoDocDestinatario;
    @XmlElement(name = "nombrecontacto_remitente")
    protected String nombrecontactoRemitente;
    @XmlElement(name = "celular_remitente")
    protected String celularRemitente;
    @XmlElement(name = "correo_remitente")
    protected String correoRemitente;
    @XmlElement(name = "Serv_Externo")
    protected String servExterno;
    @XmlElement(name = "Retorno_Digital")
    protected String retornoDigital;

    /**
     * Obtiene el valor de la propiedad numGuia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuia() {
        return numGuia;
    }

    /**
     * Define el valor de la propiedad numGuia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuia(BigDecimal value) {
        this.numGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad numSobreporte.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumSobreporte() {
        return numSobreporte;
    }

    /**
     * Define el valor de la propiedad numSobreporte.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumSobreporte(BigDecimal value) {
        this.numSobreporte = value;
    }

    /**
     * Obtiene el valor de la propiedad numSobreCajaPorte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSobreCajaPorte() {
        return numSobreCajaPorte;
    }

    /**
     * Define el valor de la propiedad numSobreCajaPorte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSobreCajaPorte(String value) {
        this.numSobreCajaPorte = value;
    }

    /**
     * Obtiene el valor de la propiedad fecCreacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecCreacion() {
        return fecCreacion;
    }

    /**
     * Define el valor de la propiedad fecCreacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecCreacion(String value) {
        this.fecCreacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecTiempoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecTiempoEntrega() {
        return fecTiempoEntrega;
    }

    /**
     * Define el valor de la propiedad fecTiempoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecTiempoEntrega(String value) {
        this.fecTiempoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad docRelacionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocRelacionado() {
        return docRelacionado;
    }

    /**
     * Define el valor de la propiedad docRelacionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocRelacionado(String value) {
        this.docRelacionado = value;
    }

    /**
     * Obtiene el valor de la propiedad numPiezas.
     * 
     */
    public int getNumPiezas() {
        return numPiezas;
    }

    /**
     * Define el valor de la propiedad numPiezas.
     * 
     */
    public void setNumPiezas(int value) {
        this.numPiezas = value;
    }

    /**
     * Obtiene el valor de la propiedad desTipoTrayecto.
     * 
     */
    public int getDesTipoTrayecto() {
        return desTipoTrayecto;
    }

    /**
     * Define el valor de la propiedad desTipoTrayecto.
     * 
     */
    public void setDesTipoTrayecto(int value) {
        this.desTipoTrayecto = value;
    }

    /**
     * Obtiene el valor de la propiedad ideProducto.
     * 
     */
    public int getIdeProducto() {
        return ideProducto;
    }

    /**
     * Define el valor de la propiedad ideProducto.
     * 
     */
    public void setIdeProducto(int value) {
        this.ideProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad ideDestinatarios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeDestinatarios() {
        return ideDestinatarios;
    }

    /**
     * Define el valor de la propiedad ideDestinatarios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeDestinatarios(String value) {
        this.ideDestinatarios = value;
    }

    /**
     * Obtiene el valor de la propiedad ideCodFacturacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeCodFacturacion() {
        return ideCodFacturacion;
    }

    /**
     * Define el valor de la propiedad ideCodFacturacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeCodFacturacion(String value) {
        this.ideCodFacturacion = value;
    }

    /**
     * Obtiene el valor de la propiedad ideManifiesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeManifiesto() {
        return ideManifiesto;
    }

    /**
     * Define el valor de la propiedad ideManifiesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeManifiesto(String value) {
        this.ideManifiesto = value;
    }

    /**
     * Obtiene el valor de la propiedad desFormaPago.
     * 
     */
    public int getDesFormaPago() {
        return desFormaPago;
    }

    /**
     * Define el valor de la propiedad desFormaPago.
     * 
     */
    public void setDesFormaPago(int value) {
        this.desFormaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad desMedioTransporte.
     * 
     */
    public int getDesMedioTransporte() {
        return desMedioTransporte;
    }

    /**
     * Define el valor de la propiedad desMedioTransporte.
     * 
     */
    public void setDesMedioTransporte(int value) {
        this.desMedioTransporte = value;
    }

    /**
     * Obtiene el valor de la propiedad numPesoTotal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumPesoTotal() {
        return numPesoTotal;
    }

    /**
     * Define el valor de la propiedad numPesoTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumPesoTotal(BigDecimal value) {
        this.numPesoTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad numValorDeclaradoTotal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumValorDeclaradoTotal() {
        return numValorDeclaradoTotal;
    }

    /**
     * Define el valor de la propiedad numValorDeclaradoTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumValorDeclaradoTotal(BigDecimal value) {
        this.numValorDeclaradoTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad numVolumenTotal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumVolumenTotal() {
        return numVolumenTotal;
    }

    /**
     * Define el valor de la propiedad numVolumenTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumVolumenTotal(BigDecimal value) {
        this.numVolumenTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad numBolsaSeguridad.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumBolsaSeguridad() {
        return numBolsaSeguridad;
    }

    /**
     * Define el valor de la propiedad numBolsaSeguridad.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumBolsaSeguridad(BigDecimal value) {
        this.numBolsaSeguridad = value;
    }

    /**
     * Obtiene el valor de la propiedad numPrecinto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumPrecinto() {
        return numPrecinto;
    }

    /**
     * Define el valor de la propiedad numPrecinto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumPrecinto(BigDecimal value) {
        this.numPrecinto = value;
    }

    /**
     * Obtiene el valor de la propiedad desTipoDuracionTrayecto.
     * 
     */
    public int getDesTipoDuracionTrayecto() {
        return desTipoDuracionTrayecto;
    }

    /**
     * Define el valor de la propiedad desTipoDuracionTrayecto.
     * 
     */
    public void setDesTipoDuracionTrayecto(int value) {
        this.desTipoDuracionTrayecto = value;
    }

    /**
     * Obtiene el valor de la propiedad desTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesTelefono() {
        return desTelefono;
    }

    /**
     * Define el valor de la propiedad desTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesTelefono(String value) {
        this.desTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad desCiudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCiudad() {
        return desCiudad;
    }

    /**
     * Define el valor de la propiedad desCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCiudad(String value) {
        this.desCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad desDireccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDireccion() {
        return desDireccion;
    }

    /**
     * Define el valor de la propiedad desDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDireccion(String value) {
        this.desDireccion = value;
    }

    /**
     * Obtiene el valor de la propiedad nomContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomContacto() {
        return nomContacto;
    }

    /**
     * Define el valor de la propiedad nomContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomContacto(String value) {
        this.nomContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad desVlrCampoPersonalizado1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesVlrCampoPersonalizado1() {
        return desVlrCampoPersonalizado1;
    }

    /**
     * Define el valor de la propiedad desVlrCampoPersonalizado1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesVlrCampoPersonalizado1(String value) {
        this.desVlrCampoPersonalizado1 = value;
    }

    /**
     * Obtiene el valor de la propiedad numValorLiquidado.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumValorLiquidado() {
        return numValorLiquidado;
    }

    /**
     * Define el valor de la propiedad numValorLiquidado.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumValorLiquidado(BigDecimal value) {
        this.numValorLiquidado = value;
    }

    /**
     * Obtiene el valor de la propiedad desDiceContener.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDiceContener() {
        return desDiceContener;
    }

    /**
     * Define el valor de la propiedad desDiceContener.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDiceContener(String value) {
        this.desDiceContener = value;
    }

    /**
     * Obtiene el valor de la propiedad desTipoGuia.
     * 
     */
    public int getDesTipoGuia() {
        return desTipoGuia;
    }

    /**
     * Define el valor de la propiedad desTipoGuia.
     * 
     */
    public void setDesTipoGuia(int value) {
        this.desTipoGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad desCiudadRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCiudadRemitente() {
        return desCiudadRemitente;
    }

    /**
     * Define el valor de la propiedad desCiudadRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCiudadRemitente(String value) {
        this.desCiudadRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad numVlrSobreflete.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumVlrSobreflete() {
        return numVlrSobreflete;
    }

    /**
     * Define el valor de la propiedad numVlrSobreflete.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumVlrSobreflete(BigDecimal value) {
        this.numVlrSobreflete = value;
    }

    /**
     * Obtiene el valor de la propiedad numVlrFlete.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumVlrFlete() {
        return numVlrFlete;
    }

    /**
     * Define el valor de la propiedad numVlrFlete.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumVlrFlete(BigDecimal value) {
        this.numVlrFlete = value;
    }

    /**
     * Obtiene el valor de la propiedad numDescuento.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumDescuento() {
        return numDescuento;
    }

    /**
     * Define el valor de la propiedad numDescuento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumDescuento(BigDecimal value) {
        this.numDescuento = value;
    }

    /**
     * Obtiene el valor de la propiedad desDireccionRecogida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDireccionRecogida() {
        return desDireccionRecogida;
    }

    /**
     * Define el valor de la propiedad desDireccionRecogida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDireccionRecogida(String value) {
        this.desDireccionRecogida = value;
    }

    /**
     * Obtiene el valor de la propiedad desTelefonoRecogida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesTelefonoRecogida() {
        return desTelefonoRecogida;
    }

    /**
     * Define el valor de la propiedad desTelefonoRecogida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesTelefonoRecogida(String value) {
        this.desTelefonoRecogida = value;
    }

    /**
     * Obtiene el valor de la propiedad desCiudadRecogida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCiudadRecogida() {
        return desCiudadRecogida;
    }

    /**
     * Define el valor de la propiedad desCiudadRecogida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCiudadRecogida(String value) {
        this.desCiudadRecogida = value;
    }

    /**
     * Obtiene el valor de la propiedad idePaisOrigen.
     * 
     */
    public int getIdePaisOrigen() {
        return idePaisOrigen;
    }

    /**
     * Define el valor de la propiedad idePaisOrigen.
     * 
     */
    public void setIdePaisOrigen(int value) {
        this.idePaisOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad idePaisDestino.
     * 
     */
    public int getIdePaisDestino() {
        return idePaisDestino;
    }

    /**
     * Define el valor de la propiedad idePaisDestino.
     * 
     */
    public void setIdePaisDestino(int value) {
        this.idePaisDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad desIdArchivoOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesIdArchivoOrigen() {
        return desIdArchivoOrigen;
    }

    /**
     * Define el valor de la propiedad desIdArchivoOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesIdArchivoOrigen(String value) {
        this.desIdArchivoOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad ideNumIdentificDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeNumIdentificDest() {
        return ideNumIdentificDest;
    }

    /**
     * Define el valor de la propiedad ideNumIdentificDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeNumIdentificDest(String value) {
        this.ideNumIdentificDest = value;
    }

    /**
     * Obtiene el valor de la propiedad ideNumReferenciaDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeNumReferenciaDest() {
        return ideNumReferenciaDest;
    }

    /**
     * Define el valor de la propiedad ideNumReferenciaDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeNumReferenciaDest(String value) {
        this.ideNumReferenciaDest = value;
    }

    /**
     * Obtiene el valor de la propiedad desDireccionRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDireccionRemitente() {
        return desDireccionRemitente;
    }

    /**
     * Define el valor de la propiedad desDireccionRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDireccionRemitente(String value) {
        this.desDireccionRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad numPesoFacturado.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumPesoFacturado() {
        return numPesoFacturado;
    }

    /**
     * Define el valor de la propiedad numPesoFacturado.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumPesoFacturado(BigDecimal value) {
        this.numPesoFacturado = value;
    }

    /**
     * Obtiene el valor de la propiedad nomTipoTrayecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomTipoTrayecto() {
        return nomTipoTrayecto;
    }

    /**
     * Define el valor de la propiedad nomTipoTrayecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomTipoTrayecto(String value) {
        this.nomTipoTrayecto = value;
    }

    /**
     * Obtiene el valor de la propiedad estCanalMayorista.
     * 
     */
    public boolean isEstCanalMayorista() {
        return estCanalMayorista;
    }

    /**
     * Define el valor de la propiedad estCanalMayorista.
     * 
     */
    public void setEstCanalMayorista(boolean value) {
        this.estCanalMayorista = value;
    }

    /**
     * Obtiene el valor de la propiedad nomRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomRemitente() {
        return nomRemitente;
    }

    /**
     * Define el valor de la propiedad nomRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomRemitente(String value) {
        this.nomRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad numIdentiRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumIdentiRemitente() {
        return numIdentiRemitente;
    }

    /**
     * Define el valor de la propiedad numIdentiRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumIdentiRemitente(String value) {
        this.numIdentiRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad numTelefonoRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTelefonoRemitente() {
        return numTelefonoRemitente;
    }

    /**
     * Define el valor de la propiedad numTelefonoRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTelefonoRemitente(String value) {
        this.numTelefonoRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad numAlto.
     * 
     */
    public int getNumAlto() {
        return numAlto;
    }

    /**
     * Define el valor de la propiedad numAlto.
     * 
     */
    public void setNumAlto(int value) {
        this.numAlto = value;
    }

    /**
     * Obtiene el valor de la propiedad numAncho.
     * 
     */
    public int getNumAncho() {
        return numAncho;
    }

    /**
     * Define el valor de la propiedad numAncho.
     * 
     */
    public void setNumAncho(int value) {
        this.numAncho = value;
    }

    /**
     * Obtiene el valor de la propiedad numLargo.
     * 
     */
    public int getNumLargo() {
        return numLargo;
    }

    /**
     * Define el valor de la propiedad numLargo.
     * 
     */
    public void setNumLargo(int value) {
        this.numLargo = value;
    }

    /**
     * Obtiene el valor de la propiedad desCiudadOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCiudadOrigen() {
        return desCiudadOrigen;
    }

    /**
     * Define el valor de la propiedad desCiudadOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCiudadOrigen(String value) {
        this.desCiudadOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad desDiceContenerSobre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDiceContenerSobre() {
        return desDiceContenerSobre;
    }

    /**
     * Define el valor de la propiedad desDiceContenerSobre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDiceContenerSobre(String value) {
        this.desDiceContenerSobre = value;
    }

    /**
     * Obtiene el valor de la propiedad desDepartamentoDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDepartamentoDestino() {
        return desDepartamentoDestino;
    }

    /**
     * Define el valor de la propiedad desDepartamentoDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDepartamentoDestino(String value) {
        this.desDepartamentoDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad desDepartamentoOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDepartamentoOrigen() {
        return desDepartamentoOrigen;
    }

    /**
     * Define el valor de la propiedad desDepartamentoOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDepartamentoOrigen(String value) {
        this.desDepartamentoOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad genCajaporte.
     * 
     */
    public boolean isGenCajaporte() {
        return genCajaporte;
    }

    /**
     * Define el valor de la propiedad genCajaporte.
     * 
     */
    public void setGenCajaporte(boolean value) {
        this.genCajaporte = value;
    }

    /**
     * Obtiene el valor de la propiedad genSobreporte.
     * 
     */
    public boolean isGenSobreporte() {
        return genSobreporte;
    }

    /**
     * Define el valor de la propiedad genSobreporte.
     * 
     */
    public void setGenSobreporte(boolean value) {
        this.genSobreporte = value;
    }

    /**
     * Obtiene el valor de la propiedad nomUnidadEmpaque.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomUnidadEmpaque() {
        return nomUnidadEmpaque;
    }

    /**
     * Define el valor de la propiedad nomUnidadEmpaque.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomUnidadEmpaque(String value) {
        this.nomUnidadEmpaque = value;
    }

    /**
     * Obtiene el valor de la propiedad nomRemitenteCanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomRemitenteCanal() {
        return nomRemitenteCanal;
    }

    /**
     * Define el valor de la propiedad nomRemitenteCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomRemitenteCanal(String value) {
        this.nomRemitenteCanal = value;
    }

    /**
     * Obtiene el valor de la propiedad desUnidadLongitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesUnidadLongitud() {
        return desUnidadLongitud;
    }

    /**
     * Define el valor de la propiedad desUnidadLongitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesUnidadLongitud(String value) {
        this.desUnidadLongitud = value;
    }

    /**
     * Obtiene el valor de la propiedad desUnidadPeso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesUnidadPeso() {
        return desUnidadPeso;
    }

    /**
     * Define el valor de la propiedad desUnidadPeso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesUnidadPeso(String value) {
        this.desUnidadPeso = value;
    }

    /**
     * Obtiene el valor de la propiedad numValorDeclaradoSobreTotal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumValorDeclaradoSobreTotal() {
        return numValorDeclaradoSobreTotal;
    }

    /**
     * Define el valor de la propiedad numValorDeclaradoSobreTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumValorDeclaradoSobreTotal(String value) {
        this.numValorDeclaradoSobreTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad numFactura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumFactura() {
        return numFactura;
    }

    /**
     * Define el valor de la propiedad numFactura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumFactura(String value) {
        this.numFactura = value;
    }

    /**
     * Obtiene el valor de la propiedad desCorreoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCorreoElectronico() {
        return desCorreoElectronico;
    }

    /**
     * Define el valor de la propiedad desCorreoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCorreoElectronico(String value) {
        this.desCorreoElectronico = value;
    }

    /**
     * Obtiene el valor de la propiedad numCelular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCelular() {
        return numCelular;
    }

    /**
     * Define el valor de la propiedad numCelular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCelular(String value) {
        this.numCelular = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivoCargar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivoCargar() {
        return idArchivoCargar;
    }

    /**
     * Define el valor de la propiedad idArchivoCargar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivoCargar(String value) {
        this.idArchivoCargar = value;
    }

    /**
     * Obtiene el valor de la propiedad numRecaudo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRecaudo() {
        return numRecaudo;
    }

    /**
     * Define el valor de la propiedad numRecaudo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRecaudo(String value) {
        this.numRecaudo = value;
    }

    /**
     * Obtiene el valor de la propiedad idZonificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdZonificacion() {
        return idZonificacion;
    }

    /**
     * Define el valor de la propiedad idZonificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdZonificacion(String value) {
        this.idZonificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad desCodigopostal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCodigopostal() {
        return desCodigopostal;
    }

    /**
     * Define el valor de la propiedad desCodigopostal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCodigopostal(String value) {
        this.desCodigopostal = value;
    }

    /**
     * Obtiene el valor de la propiedad remCodigopostal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemCodigopostal() {
        return remCodigopostal;
    }

    /**
     * Define el valor de la propiedad remCodigopostal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemCodigopostal(String value) {
        this.remCodigopostal = value;
    }

    /**
     * Obtiene el valor de la propiedad centroCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentroCosto() {
        return centroCosto;
    }

    /**
     * Define el valor de la propiedad centroCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentroCosto(String value) {
        this.centroCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad idcanaldistribucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCANALDISTRIBUCION() {
        return idcanaldistribucion;
    }

    /**
     * Define el valor de la propiedad idcanaldistribucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCANALDISTRIBUCION(String value) {
        this.idcanaldistribucion = value;
    }

    /**
     * Obtiene el valor de la propiedad casilleropostal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCASILLEROPOSTAL() {
        return casilleropostal;
    }

    /**
     * Define el valor de la propiedad casilleropostal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCASILLEROPOSTAL(String value) {
        this.casilleropostal = value;
    }

    /**
     * Obtiene el valor de la propiedad objEnviosUnidadEmpaqueCargue.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEnviosUnidadEmpaqueCargue }
     *     
     */
    public ArrayOfEnviosUnidadEmpaqueCargue getObjEnviosUnidadEmpaqueCargue() {
        return objEnviosUnidadEmpaqueCargue;
    }

    /**
     * Define el valor de la propiedad objEnviosUnidadEmpaqueCargue.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEnviosUnidadEmpaqueCargue }
     *     
     */
    public void setObjEnviosUnidadEmpaqueCargue(ArrayOfEnviosUnidadEmpaqueCargue value) {
        this.objEnviosUnidadEmpaqueCargue = value;
    }

    /**
     * Obtiene el valor de la propiedad estEnviarCorreo.
     * 
     */
    public boolean isEstEnviarCorreo() {
        return estEnviarCorreo;
    }

    /**
     * Define el valor de la propiedad estEnviarCorreo.
     * 
     */
    public void setEstEnviarCorreo(boolean value) {
        this.estEnviarCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad recoleccionEsporadica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecoleccionEsporadica() {
        return recoleccionEsporadica;
    }

    /**
     * Define el valor de la propiedad recoleccionEsporadica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecoleccionEsporadica(String value) {
        this.recoleccionEsporadica = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocDestinatario() {
        return tipoDocDestinatario;
    }

    /**
     * Define el valor de la propiedad tipoDocDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocDestinatario(String value) {
        this.tipoDocDestinatario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombrecontactoRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombrecontactoRemitente() {
        return nombrecontactoRemitente;
    }

    /**
     * Define el valor de la propiedad nombrecontactoRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombrecontactoRemitente(String value) {
        this.nombrecontactoRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad celularRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelularRemitente() {
        return celularRemitente;
    }

    /**
     * Define el valor de la propiedad celularRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelularRemitente(String value) {
        this.celularRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad correoRemitente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoRemitente() {
        return correoRemitente;
    }

    /**
     * Define el valor de la propiedad correoRemitente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoRemitente(String value) {
        this.correoRemitente = value;
    }

    /**
     * Obtiene el valor de la propiedad servExterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServExterno() {
        return servExterno;
    }

    /**
     * Define el valor de la propiedad servExterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServExterno(String value) {
        this.servExterno = value;
    }

    /**
     * Obtiene el valor de la propiedad retornoDigital.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoDigital() {
        return retornoDigital;
    }

    /**
     * Define el valor de la propiedad retornoDigital.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoDigital(String value) {
        this.retornoDigital = value;
    }

}
