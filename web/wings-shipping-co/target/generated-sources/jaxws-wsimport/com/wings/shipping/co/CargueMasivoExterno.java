
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="envios" type="{http://tempuri.org/}ArrayOfCargueMasivoExternoDTO" minOccurs="0"/>
 *         &lt;element name="arrayGuias" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "envios",
    "arrayGuias"
})
@XmlRootElement(name = "CargueMasivoExterno")
public class CargueMasivoExterno {

    protected ArrayOfCargueMasivoExternoDTO envios;
    protected ArrayOfString arrayGuias;

    /**
     * Obtiene el valor de la propiedad envios.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCargueMasivoExternoDTO }
     *     
     */
    public ArrayOfCargueMasivoExternoDTO getEnvios() {
        return envios;
    }

    /**
     * Define el valor de la propiedad envios.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCargueMasivoExternoDTO }
     *     
     */
    public void setEnvios(ArrayOfCargueMasivoExternoDTO value) {
        this.envios = value;
    }

    /**
     * Obtiene el valor de la propiedad arrayGuias.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getArrayGuias() {
        return arrayGuias;
    }

    /**
     * Define el valor de la propiedad arrayGuias.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setArrayGuias(ArrayOfString value) {
        this.arrayGuias = value;
    }

}
