
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerarManifiestoResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadenaBytes" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Des_Error" type="{http://tempuri.org/}ArrayOfErrorGeneradoPorGuia" minOccurs="0"/>
 *         &lt;element name="Num_manifiesto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generarManifiestoResult",
    "cadenaBytes",
    "desError",
    "numManifiesto",
    "mensaje"
})
@XmlRootElement(name = "GenerarManifiestoResponse")
public class GenerarManifiestoResponse {

    @XmlElement(name = "GenerarManifiestoResult")
    protected boolean generarManifiestoResult;
    protected byte[] cadenaBytes;
    @XmlElement(name = "Des_Error")
    protected ArrayOfErrorGeneradoPorGuia desError;
    @XmlElement(name = "Num_manifiesto")
    protected String numManifiesto;
    @XmlElement(name = "Mensaje")
    protected String mensaje;

    /**
     * Obtiene el valor de la propiedad generarManifiestoResult.
     * 
     */
    public boolean isGenerarManifiestoResult() {
        return generarManifiestoResult;
    }

    /**
     * Define el valor de la propiedad generarManifiestoResult.
     * 
     */
    public void setGenerarManifiestoResult(boolean value) {
        this.generarManifiestoResult = value;
    }

    /**
     * Obtiene el valor de la propiedad cadenaBytes.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getCadenaBytes() {
        return cadenaBytes;
    }

    /**
     * Define el valor de la propiedad cadenaBytes.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setCadenaBytes(byte[] value) {
        this.cadenaBytes = value;
    }

    /**
     * Obtiene el valor de la propiedad desError.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfErrorGeneradoPorGuia }
     *     
     */
    public ArrayOfErrorGeneradoPorGuia getDesError() {
        return desError;
    }

    /**
     * Define el valor de la propiedad desError.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfErrorGeneradoPorGuia }
     *     
     */
    public void setDesError(ArrayOfErrorGeneradoPorGuia value) {
        this.desError = value;
    }

    /**
     * Obtiene el valor de la propiedad numManifiesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumManifiesto() {
        return numManifiesto;
    }

    /**
     * Define el valor de la propiedad numManifiesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumManifiesto(String value) {
        this.numManifiesto = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

}
