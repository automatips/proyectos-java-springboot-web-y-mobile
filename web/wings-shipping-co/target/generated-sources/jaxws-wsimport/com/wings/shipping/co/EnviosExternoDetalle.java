
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EnviosExternoDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EnviosExternoDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Numero_palet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Numero_Suborden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Numero_F12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Numero_segundo_F12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fecha_F12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo_Documento_F11F12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ciudad_Origen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Departamento_Origen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ciudad_Destino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Departamento_Destino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo_producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cantidad_Bultos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo_Documento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo_Contenido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Monto_Declarado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rut_Destinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Digito_verificador_Rut_Destinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comuna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="objEnviosUnidadEmpaqueCargueDetalle" type="{http://tempuri.org/}ArrayOfEnviosUnidadEmpaqueCargueDetalle" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnviosExternoDetalle", propOrder = {
    "numeroPalet",
    "numeroSuborden",
    "numeroF12",
    "numeroSegundoF12",
    "fechaF12",
    "tipoDocumentoF11F12",
    "ciudadOrigen",
    "departamentoOrigen",
    "ciudadDestino",
    "departamentoDestino",
    "tipoProducto",
    "cantidadBultos",
    "tipoDocumento",
    "tipoContenido",
    "montoDeclarado",
    "rutDestinatario",
    "digitoVerificadorRutDestinatario",
    "nombres",
    "telefono",
    "calle",
    "comuna",
    "region",
    "objEnviosUnidadEmpaqueCargueDetalle"
})
public class EnviosExternoDetalle {

    @XmlElement(name = "Numero_palet")
    protected String numeroPalet;
    @XmlElement(name = "Numero_Suborden")
    protected String numeroSuborden;
    @XmlElement(name = "Numero_F12")
    protected String numeroF12;
    @XmlElement(name = "Numero_segundo_F12")
    protected String numeroSegundoF12;
    @XmlElement(name = "Fecha_F12")
    protected String fechaF12;
    @XmlElement(name = "Tipo_Documento_F11F12")
    protected String tipoDocumentoF11F12;
    @XmlElement(name = "Ciudad_Origen")
    protected String ciudadOrigen;
    @XmlElement(name = "Departamento_Origen")
    protected String departamentoOrigen;
    @XmlElement(name = "Ciudad_Destino")
    protected String ciudadDestino;
    @XmlElement(name = "Departamento_Destino")
    protected String departamentoDestino;
    @XmlElement(name = "Tipo_producto")
    protected String tipoProducto;
    @XmlElement(name = "Cantidad_Bultos")
    protected String cantidadBultos;
    @XmlElement(name = "Tipo_Documento")
    protected String tipoDocumento;
    @XmlElement(name = "Tipo_Contenido")
    protected String tipoContenido;
    @XmlElement(name = "Monto_Declarado")
    protected String montoDeclarado;
    @XmlElement(name = "Rut_Destinatario")
    protected String rutDestinatario;
    @XmlElement(name = "Digito_verificador_Rut_Destinatario")
    protected String digitoVerificadorRutDestinatario;
    @XmlElement(name = "Nombres")
    protected String nombres;
    @XmlElement(name = "Telefono")
    protected String telefono;
    @XmlElement(name = "Calle")
    protected String calle;
    @XmlElement(name = "Comuna")
    protected String comuna;
    @XmlElement(name = "Region")
    protected String region;
    protected ArrayOfEnviosUnidadEmpaqueCargueDetalle objEnviosUnidadEmpaqueCargueDetalle;

    /**
     * Obtiene el valor de la propiedad numeroPalet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPalet() {
        return numeroPalet;
    }

    /**
     * Define el valor de la propiedad numeroPalet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPalet(String value) {
        this.numeroPalet = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSuborden.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSuborden() {
        return numeroSuborden;
    }

    /**
     * Define el valor de la propiedad numeroSuborden.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSuborden(String value) {
        this.numeroSuborden = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroF12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroF12() {
        return numeroF12;
    }

    /**
     * Define el valor de la propiedad numeroF12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroF12(String value) {
        this.numeroF12 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSegundoF12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSegundoF12() {
        return numeroSegundoF12;
    }

    /**
     * Define el valor de la propiedad numeroSegundoF12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSegundoF12(String value) {
        this.numeroSegundoF12 = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaF12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaF12() {
        return fechaF12;
    }

    /**
     * Define el valor de la propiedad fechaF12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaF12(String value) {
        this.fechaF12 = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumentoF11F12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumentoF11F12() {
        return tipoDocumentoF11F12;
    }

    /**
     * Define el valor de la propiedad tipoDocumentoF11F12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumentoF11F12(String value) {
        this.tipoDocumentoF11F12 = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    /**
     * Define el valor de la propiedad ciudadOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadOrigen(String value) {
        this.ciudadOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad departamentoOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartamentoOrigen() {
        return departamentoOrigen;
    }

    /**
     * Define el valor de la propiedad departamentoOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartamentoOrigen(String value) {
        this.departamentoOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDestino() {
        return ciudadDestino;
    }

    /**
     * Define el valor de la propiedad ciudadDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDestino(String value) {
        this.ciudadDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad departamentoDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartamentoDestino() {
        return departamentoDestino;
    }

    /**
     * Define el valor de la propiedad departamentoDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartamentoDestino(String value) {
        this.departamentoDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProducto() {
        return tipoProducto;
    }

    /**
     * Define el valor de la propiedad tipoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProducto(String value) {
        this.tipoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadBultos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadBultos() {
        return cantidadBultos;
    }

    /**
     * Define el valor de la propiedad cantidadBultos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadBultos(String value) {
        this.cantidadBultos = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContenido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContenido() {
        return tipoContenido;
    }

    /**
     * Define el valor de la propiedad tipoContenido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContenido(String value) {
        this.tipoContenido = value;
    }

    /**
     * Obtiene el valor de la propiedad montoDeclarado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoDeclarado() {
        return montoDeclarado;
    }

    /**
     * Define el valor de la propiedad montoDeclarado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoDeclarado(String value) {
        this.montoDeclarado = value;
    }

    /**
     * Obtiene el valor de la propiedad rutDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutDestinatario() {
        return rutDestinatario;
    }

    /**
     * Define el valor de la propiedad rutDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutDestinatario(String value) {
        this.rutDestinatario = value;
    }

    /**
     * Obtiene el valor de la propiedad digitoVerificadorRutDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitoVerificadorRutDestinatario() {
        return digitoVerificadorRutDestinatario;
    }

    /**
     * Define el valor de la propiedad digitoVerificadorRutDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitoVerificadorRutDestinatario(String value) {
        this.digitoVerificadorRutDestinatario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * Define el valor de la propiedad nombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombres(String value) {
        this.nombres = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Define el valor de la propiedad telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Obtiene el valor de la propiedad calle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalle() {
        return calle;
    }

    /**
     * Define el valor de la propiedad calle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalle(String value) {
        this.calle = value;
    }

    /**
     * Obtiene el valor de la propiedad comuna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComuna() {
        return comuna;
    }

    /**
     * Define el valor de la propiedad comuna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComuna(String value) {
        this.comuna = value;
    }

    /**
     * Obtiene el valor de la propiedad region.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Define el valor de la propiedad region.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Obtiene el valor de la propiedad objEnviosUnidadEmpaqueCargueDetalle.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEnviosUnidadEmpaqueCargueDetalle }
     *     
     */
    public ArrayOfEnviosUnidadEmpaqueCargueDetalle getObjEnviosUnidadEmpaqueCargueDetalle() {
        return objEnviosUnidadEmpaqueCargueDetalle;
    }

    /**
     * Define el valor de la propiedad objEnviosUnidadEmpaqueCargueDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEnviosUnidadEmpaqueCargueDetalle }
     *     
     */
    public void setObjEnviosUnidadEmpaqueCargueDetalle(ArrayOfEnviosUnidadEmpaqueCargueDetalle value) {
        this.objEnviosUnidadEmpaqueCargueDetalle = value;
    }

}
