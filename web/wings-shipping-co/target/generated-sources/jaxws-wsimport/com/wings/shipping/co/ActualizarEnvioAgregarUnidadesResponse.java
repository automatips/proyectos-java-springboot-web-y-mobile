
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActualizarEnvioAgregarUnidadesResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actualizarEnvioAgregarUnidadesResult"
})
@XmlRootElement(name = "ActualizarEnvioAgregarUnidadesResponse")
public class ActualizarEnvioAgregarUnidadesResponse {

    @XmlElement(name = "ActualizarEnvioAgregarUnidadesResult")
    protected boolean actualizarEnvioAgregarUnidadesResult;

    /**
     * Obtiene el valor de la propiedad actualizarEnvioAgregarUnidadesResult.
     * 
     */
    public boolean isActualizarEnvioAgregarUnidadesResult() {
        return actualizarEnvioAgregarUnidadesResult;
    }

    /**
     * Define el valor de la propiedad actualizarEnvioAgregarUnidadesResult.
     * 
     */
    public void setActualizarEnvioAgregarUnidadesResult(boolean value) {
        this.actualizarEnvioAgregarUnidadesResult = value;
    }

}
