
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CargueMasivoExternoDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CargueMasivoExternoDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="objEnvios" type="{http://tempuri.org/}ArrayOfEnviosExterno" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CargueMasivoExternoDTO", propOrder = {
    "objEnvios"
})
public class CargueMasivoExternoDTO {

    protected ArrayOfEnviosExterno objEnvios;

    /**
     * Obtiene el valor de la propiedad objEnvios.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEnviosExterno }
     *     
     */
    public ArrayOfEnviosExterno getObjEnvios() {
        return objEnvios;
    }

    /**
     * Define el valor de la propiedad objEnvios.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEnviosExterno }
     *     
     */
    public void setObjEnvios(ArrayOfEnviosExterno value) {
        this.objEnvios = value;
    }

}
