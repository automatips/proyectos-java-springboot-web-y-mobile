
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="num_GuiaArreglo" type="{http://tempuri.org/}ArrayOfLong" minOccurs="0"/>
 *         &lt;element name="ide_CodFacturacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="des_TipoGuia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Id_ArchivoCargar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numGuiaArreglo",
    "ideCodFacturacion",
    "desTipoGuia",
    "idArchivoCargar",
    "interno"
})
@XmlRootElement(name = "GenerarGuiaStikerData")
public class GenerarGuiaStikerData {

    @XmlElement(name = "num_GuiaArreglo")
    protected ArrayOfLong numGuiaArreglo;
    @XmlElement(name = "ide_CodFacturacion")
    protected String ideCodFacturacion;
    @XmlElement(name = "des_TipoGuia")
    protected int desTipoGuia;
    @XmlElement(name = "Id_ArchivoCargar")
    protected String idArchivoCargar;
    protected boolean interno;

    /**
     * Obtiene el valor de la propiedad numGuiaArreglo.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLong }
     *     
     */
    public ArrayOfLong getNumGuiaArreglo() {
        return numGuiaArreglo;
    }

    /**
     * Define el valor de la propiedad numGuiaArreglo.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLong }
     *     
     */
    public void setNumGuiaArreglo(ArrayOfLong value) {
        this.numGuiaArreglo = value;
    }

    /**
     * Obtiene el valor de la propiedad ideCodFacturacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeCodFacturacion() {
        return ideCodFacturacion;
    }

    /**
     * Define el valor de la propiedad ideCodFacturacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeCodFacturacion(String value) {
        this.ideCodFacturacion = value;
    }

    /**
     * Obtiene el valor de la propiedad desTipoGuia.
     * 
     */
    public int getDesTipoGuia() {
        return desTipoGuia;
    }

    /**
     * Define el valor de la propiedad desTipoGuia.
     * 
     */
    public void setDesTipoGuia(int value) {
        this.desTipoGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivoCargar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivoCargar() {
        return idArchivoCargar;
    }

    /**
     * Define el valor de la propiedad idArchivoCargar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivoCargar(String value) {
        this.idArchivoCargar = value;
    }

    /**
     * Obtiene el valor de la propiedad interno.
     * 
     */
    public boolean isInterno() {
        return interno;
    }

    /**
     * Define el valor de la propiedad interno.
     * 
     */
    public void setInterno(boolean value) {
        this.interno = value;
    }

}
