
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="num_Guia" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="num_GuiaFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ide_CodFacturacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sFormatoImpresionGuia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Id_ArchivoCargar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consumoClienteExterno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bytesReport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="correoElectronico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solicitudRecoleccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Remision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numGuia",
    "numGuiaFinal",
    "ideCodFacturacion",
    "sFormatoImpresionGuia",
    "idArchivoCargar",
    "consumoClienteExterno",
    "bytesReport",
    "correoElectronico",
    "solicitudRecoleccion",
    "numRemision"
})
@XmlRootElement(name = "GenerarGuiaStickerTiendasVirtuales")
public class GenerarGuiaStickerTiendasVirtuales {

    @XmlElement(name = "num_Guia", required = true)
    protected BigDecimal numGuia;
    @XmlElement(name = "num_GuiaFinal", required = true)
    protected BigDecimal numGuiaFinal;
    @XmlElement(name = "ide_CodFacturacion")
    protected String ideCodFacturacion;
    protected int sFormatoImpresionGuia;
    @XmlElement(name = "Id_ArchivoCargar")
    protected String idArchivoCargar;
    protected boolean consumoClienteExterno;
    protected byte[] bytesReport;
    protected String correoElectronico;
    protected String solicitudRecoleccion;
    @XmlElement(name = "Num_Remision")
    protected String numRemision;

    /**
     * Obtiene el valor de la propiedad numGuia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuia() {
        return numGuia;
    }

    /**
     * Define el valor de la propiedad numGuia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuia(BigDecimal value) {
        this.numGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad numGuiaFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuiaFinal() {
        return numGuiaFinal;
    }

    /**
     * Define el valor de la propiedad numGuiaFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuiaFinal(BigDecimal value) {
        this.numGuiaFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad ideCodFacturacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeCodFacturacion() {
        return ideCodFacturacion;
    }

    /**
     * Define el valor de la propiedad ideCodFacturacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeCodFacturacion(String value) {
        this.ideCodFacturacion = value;
    }

    /**
     * Obtiene el valor de la propiedad sFormatoImpresionGuia.
     * 
     */
    public int getSFormatoImpresionGuia() {
        return sFormatoImpresionGuia;
    }

    /**
     * Define el valor de la propiedad sFormatoImpresionGuia.
     * 
     */
    public void setSFormatoImpresionGuia(int value) {
        this.sFormatoImpresionGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivoCargar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivoCargar() {
        return idArchivoCargar;
    }

    /**
     * Define el valor de la propiedad idArchivoCargar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivoCargar(String value) {
        this.idArchivoCargar = value;
    }

    /**
     * Obtiene el valor de la propiedad consumoClienteExterno.
     * 
     */
    public boolean isConsumoClienteExterno() {
        return consumoClienteExterno;
    }

    /**
     * Define el valor de la propiedad consumoClienteExterno.
     * 
     */
    public void setConsumoClienteExterno(boolean value) {
        this.consumoClienteExterno = value;
    }

    /**
     * Obtiene el valor de la propiedad bytesReport.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBytesReport() {
        return bytesReport;
    }

    /**
     * Define el valor de la propiedad bytesReport.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBytesReport(byte[] value) {
        this.bytesReport = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Define el valor de la propiedad correoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudRecoleccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSolicitudRecoleccion() {
        return solicitudRecoleccion;
    }

    /**
     * Define el valor de la propiedad solicitudRecoleccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSolicitudRecoleccion(String value) {
        this.solicitudRecoleccion = value;
    }

    /**
     * Obtiene el valor de la propiedad numRemision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRemision() {
        return numRemision;
    }

    /**
     * Define el valor de la propiedad numRemision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRemision(String value) {
        this.numRemision = value;
    }

}
