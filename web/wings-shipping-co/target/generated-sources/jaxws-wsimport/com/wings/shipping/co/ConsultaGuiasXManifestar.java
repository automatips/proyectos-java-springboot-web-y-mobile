
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaGuiasXManifestar complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaGuiasXManifestar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumGuia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaGuiasXManifestar", propOrder = {
    "numGuia"
})
public class ConsultaGuiasXManifestar {

    @XmlElement(name = "NumGuia")
    protected int numGuia;

    /**
     * Obtiene el valor de la propiedad numGuia.
     * 
     */
    public int getNumGuia() {
        return numGuia;
    }

    /**
     * Define el valor de la propiedad numGuia.
     * 
     */
    public void setNumGuia(int value) {
        this.numGuia = value;
    }

}
