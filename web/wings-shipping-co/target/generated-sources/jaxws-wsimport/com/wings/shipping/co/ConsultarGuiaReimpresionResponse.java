
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarGuiaReimpresionResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarGuiaReimpresionResult"
})
@XmlRootElement(name = "ConsultarGuiaReimpresionResponse")
public class ConsultarGuiaReimpresionResponse {

    @XmlElement(name = "ConsultarGuiaReimpresionResult")
    protected boolean consultarGuiaReimpresionResult;

    /**
     * Obtiene el valor de la propiedad consultarGuiaReimpresionResult.
     * 
     */
    public boolean isConsultarGuiaReimpresionResult() {
        return consultarGuiaReimpresionResult;
    }

    /**
     * Define el valor de la propiedad consultarGuiaReimpresionResult.
     * 
     */
    public void setConsultarGuiaReimpresionResult(boolean value) {
        this.consultarGuiaReimpresionResult = value;
    }

}
