
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnularGuiasResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interno" type="{http://tempuri.org/}ArrayOfResultadoAnulacionGuias" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "anularGuiasResult",
    "interno"
})
@XmlRootElement(name = "AnularGuiasResponse")
public class AnularGuiasResponse {

    @XmlElement(name = "AnularGuiasResult")
    protected String anularGuiasResult;
    protected ArrayOfResultadoAnulacionGuias interno;

    /**
     * Obtiene el valor de la propiedad anularGuiasResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnularGuiasResult() {
        return anularGuiasResult;
    }

    /**
     * Define el valor de la propiedad anularGuiasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnularGuiasResult(String value) {
        this.anularGuiasResult = value;
    }

    /**
     * Obtiene el valor de la propiedad interno.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResultadoAnulacionGuias }
     *     
     */
    public ArrayOfResultadoAnulacionGuias getInterno() {
        return interno;
    }

    /**
     * Define el valor de la propiedad interno.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResultadoAnulacionGuias }
     *     
     */
    public void setInterno(ArrayOfResultadoAnulacionGuias value) {
        this.interno = value;
    }

}
