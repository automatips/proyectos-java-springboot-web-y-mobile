/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.shipping.co;

import com.wings.shipping.co.wrappers.ShipmentItemWrapperCo;
import com.wings.shipping.co.wrappers.ShipmentWraperCo;
import com.wings.shipping.co.consulta.InformacionGuiaMov;
import com.wings.shipping.co.consulta.InformacionMov;
import com.wings.shipping.co.consulta.MovimientoSrPack;
import com.wings.shipping.co.consulta.WsRastreoEnvios;
import com.wings.shipping.co.consulta.WsRastreoEnviosSoap;
import com.wings.shipping.co.wrappers.InformationMovSrWrapper;
import com.wings.shipping.co.wrappers.InformationMovWrapper;
import com.wings.shipping.co.wrappers.ShipmentInformationWrapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author seba
 */
public class ColombiaShippingService {

    public static ColombiaShippingService INSTANCE;

    /**
     *
     * @param debug
     * @return
     */
    public static synchronized ColombiaShippingService getInstance(boolean debug) {
        if (INSTANCE == null) {
            INSTANCE = new ColombiaShippingService();
            init(debug);
        }
        return INSTANCE;
    }

    /**
     *
     * @param debug
     */
    private static void init(boolean debug) {

    }

    /**
     *
     * @param shipment
     * @return
     */
    public List<String> sendToCurier(ShipmentWraperCo shipment) {

        try { // Call Web Service Operation
            GeneracionGuias service = new GeneracionGuias();
            GeneracionGuiasSoap port = service.getGeneracionGuiasSoap();

            // TODO initialize WS operation arguments here
            javax.xml.ws.Holder<ArrayOfCargueMasivoExternoDTO> envios = new javax.xml.ws.Holder<>();

            List<EnviosExterno> enviosExternos = createEnvioEnviosExterno(shipment);

            ArrayOfEnviosExterno arrayOfEnviosExterno = new ArrayOfEnviosExterno();
            arrayOfEnviosExterno.enviosExterno = enviosExternos;

            CargueMasivoExternoDTO cargueMasivoExternoDTO = new CargueMasivoExternoDTO();
            cargueMasivoExternoDTO.setObjEnvios(arrayOfEnviosExterno);

            List<CargueMasivoExternoDTO> cargueMasivoExternoDTOs = new ArrayList<>();
            cargueMasivoExternoDTOs.add(cargueMasivoExternoDTO);

            ArrayOfCargueMasivoExternoDTO arrayOfCargueMasivoExternoDTO = new ArrayOfCargueMasivoExternoDTO();
            arrayOfCargueMasivoExternoDTO.cargueMasivoExternoDTO = cargueMasivoExternoDTOs;

            envios.value = arrayOfCargueMasivoExternoDTO;

            javax.xml.ws.Holder<ArrayOfString> arrayGuias = new javax.xml.ws.Holder<>();

            List<String> stringList = new ArrayList<>();
            stringList.add("0");
            ArrayOfString arrayOfString = new ArrayOfString();
            arrayOfString.string = stringList;
            arrayGuias.value = arrayOfString;

            javax.xml.ws.Holder<Boolean> cargueMasivoExternoResult = new javax.xml.ws.Holder<>();
            cargueMasivoExternoResult.value = Boolean.TRUE;

            port.cargueMasivoExterno(envios, arrayGuias, cargueMasivoExternoResult);

            //System.out.println(arrayGuias.value.string);
            return arrayGuias.value.string;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     *
     * @param trackingNumber
     * @return
     */
    public ShipmentInformationWrapper checkTracking(String trackingNumber) {
        try { // Call Web Service Operation
            WsRastreoEnvios service = new WsRastreoEnvios();
            WsRastreoEnviosSoap port = service.getWsRastreoEnviosSoap();
            InformacionGuiaMov result = port.consultarGuiaExterno(trackingNumber);
            return transform(result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param result
     * @return
     */
    private ShipmentInformationWrapper transform(InformacionGuiaMov result) {

        ShipmentInformationWrapper ret = new ShipmentInformationWrapper();

        ret.setBytesImagen(result.getBytesImagen());
        ret.setCiuDes(result.getCiuDes());
        ret.setCiuRem(result.getCiuRem());
        ret.setDirDes(result.getDirDes());
        ret.setDirRem(result.getDirRem());
        ret.setEstAct(result.getEstAct());
        ret.setFecEnv(result.getFecEnv());
        ret.setFecEst(result.getFecEst());
        ret.setFechaProbable(result.getFechaProbable());
        ret.setFormPago(result.getFormPago());
        ret.setIdDocumentoCliente(result.getIdDocumentoCliente());
        ret.setIdDocumentoCliente2(result.getIdDocumentoCliente2());
        ret.setIdEstAct(result.getIdEstAct());
        ret.setIdGPS(result.getIdGPS());
        ret.setImagen(result.getImagen());
        ret.setMensajeBuscarImagen(result.getMensajeBuscarImagen());
        ret.setMensajesValidacion(result.getMensajesValidacion());
        ret.setMotivoDevolucion(result.getMotivoDevolucion());
        ret.setNomDes(result.getNomDes());
        ret.setNomProducto(result.getNomProducto());
        ret.setNomRec(result.getNomRec());
        ret.setNomRem(result.getNomRem());
        ret.setNumCun(result.getNumCun());
        ret.setNumGui(result.getNumGui());
        ret.setNumPie(result.getNumPie());
        ret.setPlaca(result.getPlaca());
        ret.setRegime(result.getRegime());

        //movs
        List<InformationMovWrapper> movs = new LinkedList<>();
        ret.setMov(movs);

        if (result.getMov() != null && result.getMov().getInformacionMov() != null) {
            List<InformacionMov> rmovs = result.getMov().getInformacionMov();
            for (InformacionMov rmov : rmovs) {
                InformationMovWrapper mov = new InformationMovWrapper();
                mov.setDesMov(rmov.getDesMov());
                mov.setDesTipoMov(rmov.getDesTipoMov());
                mov.setFecMov(rmov.getFecMov());
                mov.setFechaProb(rmov.getFechaProb());
                mov.setIdConc(rmov.getIdConc());
                mov.setIdProc(rmov.getIdProc());
                mov.setIdViewCliente(rmov.getIdViewCliente());
                mov.setNomConc(rmov.getNomConc());
                mov.setNomMov(rmov.getNomMov());
                mov.setObservacion(rmov.getObservacion());
                mov.setOriMov(rmov.getOriMov());
                mov.setTipoMov(rmov.getTipoMov());

                movs.add(mov);
            }
        }

        //movSr
        List<InformationMovSrWrapper> movSr = new LinkedList<>();
        ret.setMovSrPack(movSr);
        if (result.getMovSrPack() != null && result.getMovSrPack().getMovimientoSrPack() != null) {
            List<MovimientoSrPack> srmovs = result.getMovSrPack().getMovimientoSrPack();
            for (MovimientoSrPack srmov : srmovs) {
                InformationMovSrWrapper imov = new InformationMovSrWrapper();
                imov.setDescMov(srmov.getDescMov());
                imov.setDestMov(srmov.getDestMov());
                imov.setFechMov(srmov.getFechMov());
                imov.setOriMov(srmov.getOriMov());
                movSr.add(imov);
            }
        }

        return ret;
    }

    /**
     *
     * @return
     */
    private List<EnviosExterno> createEnvioEnviosExterno(ShipmentWraperCo shipment) {

        List<EnviosExterno> ret = new LinkedList<>();

        ObjectFactory factory = new ObjectFactory();
        List<ShipmentItemWrapperCo> items = shipment.getItems();

        EnviosExterno ee = factory.createEnviosExterno();

        //default
        ee.numGuia = BigDecimal.ZERO;
        ee.desTipoTrayecto = 1;
        ee.idePaisOrigen = 1;
        ee.idePaisDestino = 1;
        ee.ideProducto = 2;
        ee.numPesoTotal = BigDecimal.ONE;
        ee.numVolumenTotal = BigDecimal.TEN;
        ee.numBolsaSeguridad = BigDecimal.ZERO;
        ee.desTipoDuracionTrayecto = 1;
        ee.numValorLiquidado = BigDecimal.ZERO;
        ee.desFormaPago = 2;
        ee.desMedioTransporte = 1;
        ee.desTipoGuia = 1;
        ee.desIdArchivoOrigen = "1";
        ee.nomUnidadEmpaque = "generica";
        ee.desUnidadLongitud = "cm";
        ee.desUnidadPeso = "Kg";

        //shipment
        ee.docRelacionado = shipment.getShipmentNumber();
        ee.desTelefono = shipment.getPhoneNumber();
        ee.nomContacto = shipment.getShippingName();
        ee.desDireccion = shipment.getShippingAddress();
        ee.desCodigopostal = shipment.getShippingPostalCode();
        ee.desCorreoElectronico = shipment.getEmail();

        ee.desCiudad = shipment.getShippingCity();
        ee.desDepartamentoDestino = shipment.getShippingState();
        ee.numPiezas = items.size();
        ee.numValorDeclaradoTotal = new BigDecimal(250000);
        ee.desVlrCampoPersonalizado1 = shipment.getShipmentNumber();
        ee.desDiceContener = shipment.getDescription();
        ee.numPiezas = items.size();
        ee.numPesoTotal = new BigDecimal(3);
        ee.numAlto = 3;
        ee.numAncho = 3;
        ee.numLargo = 3;

        //one item
        if (items.size() == 1) {
            //item            
            ShipmentItemWrapperCo item = items.get(0);
            ee.desVlrCampoPersonalizado1 = item.getItemEan();
            ee.desDiceContener = item.getItemDescription();
            ee.setNumAlto(3);
            ee.setNumAncho(3);
            ee.setNumLargo(3);
            ee.setNumPesoTotal(new BigDecimal(3));

        } else {
            List<EnviosUnidadEmpaqueCargue> unidades = new LinkedList<>();
            for (ShipmentItemWrapperCo item : items) {

                EnviosUnidadEmpaqueCargue i = new EnviosUnidadEmpaqueCargue();
                i.setDesDiceContener(item.getItemName());
                i.setNomUnidadEmpaque("generica");
                i.setDesUnidadLongitud("cm");
                i.setDesUnidadPeso("Kg");
                i.setNumAlto(new BigDecimal(3));
                i.setNumAncho(new BigDecimal(3));
                i.setNumLargo(new BigDecimal(3));
                i.setNumPeso(new BigDecimal(3));
                i.setDesIdArchivoOrigen("1");

                unidades.add(i);
            }

            ArrayOfEnviosUnidadEmpaqueCargue a = new ArrayOfEnviosUnidadEmpaqueCargue();
            a.enviosUnidadEmpaqueCargue = unidades;
            ee.objEnviosUnidadEmpaqueCargue = a;
        }

        ret.add(ee);

        return ret;
    }

}
