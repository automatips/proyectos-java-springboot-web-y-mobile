/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.services;

import com.wings.servicios.domain.entities.PriceListDestination;
import com.wings.servicios.domain.repos.PriceListDestinationRepository;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PriceListService {

    @Autowired
    private PriceListDestinationRepository priceListDestinationRepository;

    /**
     *
     * @param priceListId
     * @return
     */
    public List<PriceListDestination> getAll(Long priceListId) {
        return priceListDestinationRepository.findByPriceListId(priceListId);
    }

    /**
     *
     * @param priceListId
     * @return
     */
    public List<Map<String, Double>> getAllDistinct(Long priceListId) {
        return priceListDestinationRepository.returnNamePrice(priceListId);
    }

}
