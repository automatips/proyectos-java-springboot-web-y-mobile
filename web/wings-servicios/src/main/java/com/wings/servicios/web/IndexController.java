/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.web;

import com.wings.servicios.domain.entities.Client;
import com.wings.servicios.domain.entities.LocalizationCountry;
import com.wings.servicios.domain.repos.LocalizationCountryRepository;
import com.wings.servicios.services.ClientsService;
import com.wings.servicios.util.AuthUtils;
import com.wings.servicios.util.NumberUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class IndexController {

    @Autowired
    private ClientsService clientsService;
    @Autowired
    private LocalizationCountryRepository localizationCountryRepository;

    @RequestMapping(value = {"/"}, method = {RequestMethod.GET})
    public String getIndex(Model model, HttpServletRequest request) {

        String login = AuthUtils.getLogin();
        Client client = clientsService.getOne(login);

        LocalizationCountry localizationCountry = localizationCountryRepository.findByCountryCode(client.getCountryIso());
        if (localizationCountry == null) {
            localizationCountry = localizationCountryRepository.findByDefaultLocale("es_ES");
        }

        Double balance = client.getBalance() * localizationCountry.getExchangeRate();
        String textBal = NumberUtils.formatMoney(balance, localizationCountry.getCurrency());

        model.addAttribute("balance", textBal);

        return "index";
    }

}
