/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.repos;

import com.wings.servicios.domain.entities.RoamingFreeOperator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface RoamingFreeOperatorRepository extends JpaRepository<RoamingFreeOperator, Long>, JpaSpecificationExecutor<RoamingFreeOperator> {

    RoamingFreeOperator findByMccAndMnc(String mcc, String mnc);
    
    RoamingFreeOperator findByMccAndMncAndEnabled(String mcc, String mnc, Boolean enabled);

}
