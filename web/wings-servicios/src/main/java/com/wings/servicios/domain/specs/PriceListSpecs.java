package com.wings.servicios.domain.specs;

import com.wings.servicios.domain.entities.PriceListDestination;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PriceListSpecs {

//    /**
//     *
//     * @param q
//     * @return
//     */
//    public static Specification<PriceList> searchPriceList(String q) {
//
//        return (Root<PriceList> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
//
//            List<Predicate> predicates = new ArrayList<>();
//            if (q != null) {
//                String search = "%" + q + "%";
//                predicates.add(
//                        builder.or(
//                                builder.like(root.get("name"), search)
//                        )
//                );
//            }
//
//            return builder.and(predicates.toArray(new Predicate[]{}));
//        };
//    }
    /**
     *
     * @param q
     * @param priceListId
     * @return
     */
    public static Specification<PriceListDestination> searchPriceListDestination(String q, Long priceListId) {

        return (Root<PriceListDestination> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("name"), search)
                        )
                );
            }

            if (priceListId != null) {
                predicates.add(builder.equal(root.get("priceListId"), priceListId));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
