/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.repos;

import com.wings.servicios.domain.entities.DidCountry;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface DidCountryRepository extends JpaRepository<DidCountry, Long>, JpaSpecificationExecutor<DidCountry> {

    List<DidCountry> findByEnabled(Boolean enabled);

    DidCountry findByRemoteId(String remoteId);

}
