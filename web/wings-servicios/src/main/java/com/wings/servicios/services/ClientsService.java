/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.services;

import com.wings.servicios.domain.entities.Client;
import com.wings.servicios.domain.entities.ClientDevice;
import com.wings.servicios.domain.entities.ClientService;
import com.wings.servicios.domain.push.Notification;
import com.wings.servicios.domain.push.PushNotification;
import com.wings.servicios.domain.repos.ClientDeviceRepository;
import com.wings.servicios.domain.repos.ClientRepository;
import com.wings.servicios.domain.repos.ClientServiceRepository;
import com.wings.servicios.util.NumberUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ClientsService {

    @Autowired
    private ClientDeviceRepository clientDeviceRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private PushService pushService;
    @Autowired
    private ClientServiceRepository clientServiceRepository;

    private final Map<String, String> codes = new HashMap<>();

    /**
     *
     * @param login
     */
    public void sendAuthenticationCode(String login) {

        Client client = clientRepository.findByLogin(login);
        ClientDevice device = clientDeviceRepository.findFirstByClientIdAndActive(client.getId(), Boolean.TRUE);
        String code = NumberUtils.getNumberRandomString(3) + " " + NumberUtils.getNumberRandomString(3);
        PushNotification push = new PushNotification();
        Notification notif = new Notification();
        notif.setTitle("Servicios - Wings Mobile");
        notif.setBody("Código de autenticación " + code);
        notif.setVisibility(0);
        push.setNotification(notif);
        push.setPriority("high");
        push.setTimeToLive(0);
        push.setTo(device.getPushToken());
        push.setVisibility("PRIVATE");

        codes.put(login, code.replaceAll(" ", "").trim());

        pushService.sendPushNotification(push);

    }

    /**
     *
     * @param login
     * @param code
     * @return
     */
    public boolean validateAuthenticationCode(String login, String code) {
        boolean ret = codes.get(login) != null && codes.get(login).equals(code);
        if (ret) {
            codes.remove(login);
        }
        return ret;
    }

    /**
     *
     * @param login
     * @param password
     * @return
     */
    public boolean authenticate(String login, String password) {

        Client client = clientRepository.findByLogin(login);
        if (client == null) {
            return false;
        }
        return client.getWebPwd().equals(DigestUtils.sha256Hex(password));

    }

    public Client getOne(String login) {
        return clientRepository.findByLogin(login);
    }

    public Client save(Client client) {
        return clientRepository.save(client);
    }

    /**
     *
     * @param clientId
     * @return
     */
    public List<ClientService> getServices(Long clientId) {
        return clientServiceRepository.findByClientId(clientId);
    }

    /**
     *
     * @param clientId
     * @param active
     * @return
     */
    public List<ClientService> getServicesActive(Long clientId, boolean active) {
        return clientServiceRepository.findByClientIdAndActiveOrderByExpirationDateAsc(clientId, active);
    }

}
