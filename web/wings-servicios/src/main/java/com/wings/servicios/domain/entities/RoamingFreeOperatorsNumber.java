/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "roaming_free_operators_numbers")
public class RoamingFreeOperatorsNumber implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "operator_id")
    private Long operatorId;
    @Size(max = 60)
    @Column(name = "did_number")
    private String didNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignations")
    private int asignations;

    public RoamingFreeOperatorsNumber() {
    }

    public RoamingFreeOperatorsNumber(Long id) {
        this.id = id;
    }

    public RoamingFreeOperatorsNumber(Long id, int asignations) {
        this.id = id;
        this.asignations = asignations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getDidNumber() {
        return didNumber;
    }

    public void setDidNumber(String didNumber) {
        this.didNumber = didNumber;
    }

    public int getAsignations() {
        return asignations;
    }

    public void setAsignations(int asignations) {
        this.asignations = asignations;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoamingFreeOperatorsNumber)) {
            return false;
        }
        RoamingFreeOperatorsNumber other = (RoamingFreeOperatorsNumber) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.voip.domain.sw.entities.RoamingFreeOperatorsNumber[ id=" + id + " ]";
    }

}
