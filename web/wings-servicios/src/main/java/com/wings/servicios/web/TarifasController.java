/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.web;

import com.wings.servicios.services.PriceListService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class TarifasController {

    @Autowired
    private PriceListService priceListService;

    @RequestMapping(value = {"/tarifas"}, method = {RequestMethod.GET})
    public String getIndex(Model model, HttpServletRequest request) {
        model.addAttribute("tarifas", priceListService.getAllDistinct(2l));
        return "tarifas";
    }
    
    @RequestMapping(value = {"/601000"}, method = {RequestMethod.GET})
    public String getIndexDestinos(Model model, HttpServletRequest request) {
        model.addAttribute("tarifas", priceListService.getAllDistinct(5l));
        return "601000";
    }

}
