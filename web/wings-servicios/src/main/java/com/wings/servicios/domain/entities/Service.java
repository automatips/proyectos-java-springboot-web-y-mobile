/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "services")
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 255)
    @Column(name = "service_name")
    private String serviceName;
    @Size(max = 255)
    @Column(name = "service_code")
    private String serviceCode;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Size(max = 100)
    @Column(name = "service_type")
    private String serviceType;
    @Size(max = 100)
    @Column(name = "renovation")
    private String renovation;
    @Size(max = 100)
    @Column(name = "asignation")
    private String asignation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Double cost;
    @Column(name = "price_list_id")
    private Long priceListId;
    @Column(name = "duration")
    private Integer duration;
    @Column(name = "valid_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Column(name = "valid_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @Lob
    @Size(max = 65535)
    @Column(name = "valid_countries")
    private String validCountries;
    @Column(name = "active")
    private boolean active;
    @Column(name = "bonus_minutes")
    private Integer bonusMinutes;
    @Column(name = "bonus_money")
    private Double bonusMoney;
    @Column(name = "priority")
    private Integer priority;
    @Column(name = "sort_order")
    private Integer sortOrder;
    @Column(name = "default_image")
    private String defaultImage;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", nullable = true, updatable = false, insertable = false)
    private List<ServiceLocalization> localizations = new LinkedList<>();

    @Transient
    private final Map<String, ServiceLocalization> localizationMaps = new HashMap<String, ServiceLocalization>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getRenovation() {
        return renovation;
    }

    public void setRenovation(String renovation) {
        this.renovation = renovation;
    }

    public String getAsignation() {
        return asignation;
    }

    public void setAsignation(String asignation) {
        this.asignation = asignation;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Long getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Long priceListId) {
        this.priceListId = priceListId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public String getValidCountries() {
        return validCountries;
    }

    public void setValidCountries(String validCountries) {
        this.validCountries = validCountries;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getBonusMinutes() {
        return bonusMinutes;
    }

    public void setBonusMinutes(Integer bonusMinutes) {
        this.bonusMinutes = bonusMinutes;
    }

    public Double getBonusMoney() {
        return bonusMoney;
    }

    public void setBonusMoney(Double bonusMoney) {
        this.bonusMoney = bonusMoney;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public List<ServiceLocalization> getLocalizations() {
        return localizations;
    }

    public void setLocalizations(List<ServiceLocalization> localizations) {
        this.localizations = localizations;
    }

    public String getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(String defaultImage) {
        this.defaultImage = defaultImage;
    }

    public ServiceLocalization getLocalization(String countryCode) {
        if (localizationMaps.isEmpty()) {
            localizations.forEach(localization -> {
                localizationMaps.put(localization.getLocCountry(), localization);
            });
        }
        return localizationMaps.get(countryCode);
    }

}
