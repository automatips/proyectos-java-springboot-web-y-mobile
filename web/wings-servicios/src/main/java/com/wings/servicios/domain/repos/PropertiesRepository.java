/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.repos;

import com.wings.servicios.domain.entities.Property;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface PropertiesRepository extends JpaRepository<Property, Long>{
    
    List<Property> findByPropCategory(String propCategory);
    
}
